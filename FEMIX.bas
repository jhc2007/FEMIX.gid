*#TESTE
*#
*#Set Var numDimension = 3
*#include .\BasFiles\NumericalIntegration.bas
*#include .\BasFiles\WriteLineSprings.bas
*##loop intervals
*#include .\BasFiles\EdgeLoads.bas
*#end intervals
*#
*#include .\BasFiles\Teste.bas
*#include .\BasFiles\ElementNodes.bas
*#Set Var TESTE=0
*#if(TESTE > 0)
*#
*#TESTE
*#
*#
*Set Var TRASH=tcl(IsFemixModelEditable)
*if(TRASH == 1)
*#
*#
*#Set Var UseFemixOriginalFile=tcl(GetUseFemixFile)
*#
*#
*#if(UseFemixOriginalFile == 0)
*#
<FEMIX_DATA_FILE_V4.0>
*#
*#realformat "%18.8e"
*realformat "%20.10e"
*#
*Set Var COUNTLAYERS = 0
*#
*loop layers
*Set Var COUNTLAYERS = COUNTLAYERS + 1
*end layers
*#
*#
*if(COUNTLAYERS == 0)
*MessageBox error: Layers not defined
*endif
*#
*Set Var TRASH=tcl(CreateLayerOfInterfaceElementsList)
*Set Cond Element_Properties *layers
*#
*if(CondNumEntities != COUNTLAYERS)
*MessageBox error: Define Element Properties for all Layers
*endif
*#
*loop layers *OnlyInCond
*#
*if(Cond(Non_Linear_Problem,int) == 0)
*#
*if(loopVar == 1)
*#
*if(strcasecmp(Cond(Element_Type),"CABLE_2D") == 0 || strcasecmp(Cond(Element_Type),"EMB_CABLE_2D") == 0 || strcasecmp(Cond(Element_Type),"FRAME_2D") == 0 || strcasecmp(Cond(Element_Type),"PLANE_STRESS_QUAD") == 0 || strcasecmp(Cond(Element_Type),"PLANE_STRESS_QUAD_THERMAL") == 0 || strcasecmp(Cond(Element_Type),"TIMOSHENKO_BEAM_2D") == 0 || strcasecmp(Cond(Element_Type),"TRUSS_2D") == 0 || strcasecmp(Cond(Element_Type),"INTERFACE_LINE_2D") == 0)
*#
*Set Var numDimension = 2
*#
*else
*#
*Set Var numDimension = 3
*#
*endif
*#
*endif
*#
*if(strcasecmp(Cond(Element_Type),"INTERFACE_LINE_2D") == 0)
*Set Var TRASH=tcl(AddLayerOfInterfaceElements *LayerNum)
*endif
*#
*else # Cond(Non_Linear_Problem,int) == 1
*#
*if(loopVar == 1)
*#
*if(strcasecmp(Cond(_Element_Type),"CABLE_2D") == 0 || strcasecmp(Cond(_Element_Type),"EMB_CABLE_2D") == 0 || strcasecmp(Cond(_Element_Type),"FRAME_2D") == 0 || strcasecmp(Cond(_Element_Type),"PLANE_STRESS_QUAD") == 0 || strcasecmp(Cond(_Element_Type),"PLANE_STRESS_QUAD_THERMAL") == 0 || strcasecmp(Cond(_Element_Type),"TIMOSHENKO_BEAM_2D") == 0 || strcasecmp(Cond(_Element_Type),"TRUSS_2D") == 0 || strcasecmp(Cond(_Element_Type),"INTERFACE_LINE_2D") == 0)
*#
*Set Var numDimension = 2
*#
*else
*#
*Set Var numDimension = 3
*#
*endif
*#
*endif
*#
*if(strcasecmp(Cond(_Element_Type),"INTERFACE_LINE_2D") == 0)
*Set Var TRASH=tcl(AddLayerOfInterfaceElements *LayerNum)
*endif
*#
*endif
*#
*#break
*#
*end layers
*#

<MAIN_PARAMETERS>

*#  MAIN_TITLE = FEMIX_JOB ;
  MAIN_TITLE = *tcl(GetMainTitle) ;

  ANALYSIS_TYPE = _*GenData(Analysis_Type) ;
  MATERIALLY_NONLINEAR_PROBLEM = *\
*if(GenData(Materially_Nonlinear_Problem,int) == 0)
_N ;
*else
_Y ;
*endif
  GEOMETRICALLY_NONLINEAR_PROBLEM = *\
*if(GenData(Geometrically_Nonlinear_Problem,int) == 0)
_N ;
*else
_Y ;
*endif

  NUMBER_OF_PHASES = *GenData(Number_of_Phases) ;
*#if(GenData(Phase_Analysis,int) == 0)
*#0 ;
*#else
*#
*#endif
*# NUMBER_OF_LOAD_CASES = *GenData(Number_of_Load_Cases) ;
  NUMBER_OF_LOAD_CASES = *nintervals ;
*# NUMBER_OF_COMBINATIONS = *GenData(Number_of_Combinations) ;
  NUMBER_OF_COMBINATIONS = *tcl(NumOfCombinations) ;

  STIFFNESS_MATRIX_STORAGE_TECHNIQUE = _*GenData(Stiffness_Matrix_Storage_Technique) ;
*#
*if(strcasecmp(GenData(Stiffness_Matrix_Storage_Technique),"SYMMETRIC_NONCONSTANT_SEMIBAND") == 0)
*#
  SYSTEM_LIN_EQ_ALGORITHM = _*GenData(System_of_Linear_Eq._Algorithm) ;
*#
*else
*#
  SYSTEM_LIN_EQ_ALGORITHM = _*GenData(_System_of_Linear_Eq._Algorithm) ;
  PRECONDITIONED_RESIDUAL_DECAY = *GenData(Preconditioned_Residual_Decay) ;
*#
*endif
*#
*if(strcasecmp(GenData(Analysis_Type),"THERMAL_TRANSIENT") == 0)
  THERMAL_TRANSIENT_TIME_STEPPING_METHOD = _*GenData(Thermal_Transient_Time_Stepping_Method) ;
*endif
*#
*#//PRECONDITIONED_RESIDUAL_DECAY =  *\
*#//if(strcmp(GenData(Stiffness_Matrix_Storage_Technique),"SYMMETRIC_NONCONSTANT_SEMIBAND") == 0)
*#//0 ;
*#//else
*#if(strcmp(GenData(Stiffness_Matrix_Storage_Technique),"SYMMETRIC_SPARSE") == 0)
*#PRECONDITIONED_RESIDUAL_DECAY = *GenData(Preconditioned_Residual_Decay) ;
*#endif

*if(GenData(Materially_Nonlinear_Problem,int) == 1)
*#
  ITERATIVE_ALGORITHM = _*GenData(Iterative_Algorithm) ;
  CONVERGENCE_CRITERION = _*GenData(Convergence_Criterion) ; 
  TOLERANCE_IN_EACH_COMBINATION = *GenData(Tolerance_in_Each_Combination) ;
  MAXIMUM_NUMBER_OF_ITERATIONS_IN_EACH_COMBINATION = *GenData(Maximum_Number_of_Iterations_in_Each_Combination) ;
  NUMBER_OF_COMBINATIONS_BEFORE_RESTART = *GenData(Number_of_Combinations_Before_Restart) ;
  ADD_THE_UNBALANCED_FORCES_IN_EACH_COMBINATION = *\
*if(GenData(Add_the_Unbalanced_Forces_in_Each_Combination,int) == 0)
_N ;
*else
_Y ;
*endif

  ARC_LENGTH = *\
*if(GenData(Arc_Length,int) == 0)
_N ;
*else
_Y ;
  MAXIMUM_NUMBER_OF_ARC_LENGTH_COMBINATIONS = *GenData(Maximum_Number_of_Arc_Length_Combinations) ;
*endif
*#

  LINE_SEARCH = *\
*if(GenData(Line_Search,int) == 0)
_N ;
*else
_Y ;
*endif

  PATH_BEHAVIOR = _*GenData(Path_Behavior) ;
*#
*endif
*#

</MAIN_PARAMETERS>

*#
*Set Var TRASH=tcl(SetLengthUnit *Units(LENGTH))
*Set Var TRASH=tcl(SetStrengthUnit *Units(STRENGTH))
*Set Var TRASH=tcl(SetStressUnit *Units(STRESS))
*Set Var TRASH=tcl(SetNumDimension *numDimension)
*#
<MESH>

*#
*Set Var COUNTGROUPS = 0
*#
*loop groups
*Set Var COUNTGROUPS = COUNTGROUPS + 1
*end groups
*#
*if(COUNTGROUPS == 0)
*Set Var COUNTGROUPS=tcl(CreateDefaultGroup)
*endif
*#
<GROUP_NAMES>

## Declaration of the names of the groups
   COUNT = *operation(COUNTLAYERS+COUNTGROUPS) ; # N. of groups
*#
*include .\BasFiles\GroupNames.bas
*#
*loop intervals
*Set Var TRASH=tcl(AddPointLoads *loopVar)
*include .\BasFiles\BoundaryWithFixedTemperaturesFromPrescribedTemperatures.bas
*include .\BasFiles\SupportsFromPrescribedDisplacements.bas
*include .\BasFiles\AuxiliaryVectorsFromEdgeLoads.bas
*include .\BasFiles\AuxiliaryVectorsFromFaceLoads.bas
*end intervals
*#
*include .\BasFiles\PointCoordinates.bas
*include .\BasFiles\ElementNodes.bas
*include .\BasFiles\CrossSectionAreas.bas
*include .\BasFiles\LaminateThicknesses.bas
*include .\BasFiles\Frame2DCrossSections.bas
*include .\BasFiles\Frame3DCrossSections.bas
*include .\BasFiles\GeometryPatterns.bas
*include .\BasFiles\LinearIsotropicMaterials.bas
*include .\BasFiles\NLMM101.bas
*include .\BasFiles\NLMM104.bas
*include .\BasFiles\NLMM111.bas
*include .\BasFiles\NLMM141.bas
*include .\BasFiles\NLMM151.bas
*include .\BasFiles\NLMM201.bas
*include .\BasFiles\NLMM301.bas
*include .\BasFiles\NLMM305.bas
*include .\BasFiles\LinearInterfaceLine2DProperties.bas
*include .\BasFiles\LinearInterfaceLine3DProperties.bas
*include .\BasFiles\LinearInterfaceSurfaceProperties.bas
*#
*include .\BasFiles\AuxiliaryVectorsFromCoordinateSystems.bas
*include .\BasFiles\DefinePointSprings.bas
*#Set Var TRASH=tcl(AddAuxiliaryVectorFromSurfaceSpringsFromFemixFile)
*include .\BasFiles\LinearSpringMaterials.bas
*include .\BasFiles\NLSMM101.bas
*include .\BasFiles\NLSMM102.bas
*#
*include .\BasFiles\ThermalIsotropicMaterials.bas
*include .\BasFiles\NLMM401.bas
*#
*include .\BasFiles\LayerProperties.bas
*include .\BasFiles\LayerPatterns.bas
*include .\BasFiles\NumericalIntegration.bas
*include .\BasFiles\ElementProperties.bas
*#
*# *loop intervals estava aqui
*#
*include .\BasFiles\Supports.bas
*include .\BasFiles\BoundaryWithFixedTemperatures.bas
*include .\BasFiles\AuxiliaryVectors.bas
*include .\BasFiles\CoordinateSystems.bas
*include .\BasFiles\WritePointSprings.bas
*include .\BasFiles\WriteLineSprings.bas
*include .\BasFiles\WriteSurfaceSprings.bas
*include .\BasFiles\PointsWithSpecialCoordinateSystem.bas
</MESH>
*#

*include .\BasFiles\ArcLengthParameters.bas
*include .\BasFiles\ThermalTransientInitialConditions.bas
*#
<LOAD>

*# CODE FOR <LOAD_CASE>
*#
*#
*loop intervals
*#
*#Set Var TRASH=tcl(SetLoadCaseNumber *loopVar)
<LOAD_CASE>

<LOAD_CASE_PARAMETERS>

  LOAD_CASE_NUMBER = *loopVar ;
  LOAD_CASE_TITLE = *tcl(ReplaceSpaceWithUnderscore *IntvData(Load_Title)) ;
  LOAD_CASE_GROUP = *IntvData(Group_Name) ;

*if(IntvData(New_Numerical_Integration,int) == 0)
*#
  INTEGRATION_TYPE = *\
*if(strcasecmp(IntvData(5),"GAUSS-LEGENDRE") == 0)
_GLEG ;
*elseif(strcasecmp(IntvData(5),"GAUSS-LOBATTO") == 0)
_GLOB ;
*elseif(strcasecmp(IntvData(5),"NEWTON-COTES") == 0)
_NCOTES ;
*else
_NONE ;
*endif
  INTEGRATION_NAME = *\
*if(strcasecmp(IntvData(6),"DEFAULT") == 0)
_DEFAULT *\
*else
*tcl(ReplaceSpaceWithUnderscore *IntvData(6)) *\
*endif
; # Defined in the <NUMERICAL_INTEGRATION> block or _DEFAULT
*#
*else
*#
  INTEGRATION_TYPE = *\
*if(strcasecmp(IntvData(8),"GAUSS-LEGENDRE") == 0)
_GLEG ;
*elseif(strcasecmp(IntvData(8),"GAUSS-LOBATTO") == 0)
_GLOB ;
*elseif(strcasecmp(IntvData(8),"NEWTON-COTES") == 0)
_NCOTES ;
*else
_NONE ;
*endif
  INTEGRATION_NAME = *\
*if(strcasecmp(IntvData(7),"DEFAULT") == 0)
_DEFAULT *\
*else
*tcl(ReplaceSpaceWithUnderscore *IntvData(7)) *\
*endif
; # Defined in the <NUMERICAL_INTEGRATION> block or _DEFAULT
*#
*#
*endif

</LOAD_CASE_PARAMETERS>

*#if(IntvData(X-Acceleration,real) != 0 || IntvData(Y-Acceleration,real) != 0 || IntvData(Z-Acceleration,real) != 0)
*#
*if(IntvData(Gravity_Load,int) == 1)
<GRAVITY_LOAD>

## Gravity acceleration (global coordinate system)
## Content of each column:
#  A -> Acceleration - XG1
#  B -> Acceleration - XG2
#  C -> Acceleration - XG3
#             A               B               C
*if(numDimension == 2)
*IntvData(Z-Acceleration,real) *IntvData(X-Acceleration,real) *IntvData(Y-Acceleration,real) ;
*else
*IntvData(X-Acceleration,real) *IntvData(Y-Acceleration,real) *IntvData(Z-Acceleration,real) ;
*endif

</GRAVITY_LOAD>
*endif
*#
*include .\BasFiles\PrescribedDisplacements.bas
*include .\BasFiles\PointLoads.bas
*include .\BasFiles\EdgeLoads.bas
*include .\BasFiles\FaceLoads.bas
*#include .\BasFiles\TemperatureVariation.bas ### ja nao se usa?
*include .\BasFiles\PrescribedPointTemperatures.bas
*include .\BasFiles\VolumetricInternalHeatGeneration.bas
*include .\BasFiles\SurfaceHeatFlux.bas
*#
</LOAD_CASE>

*end intervals
*#
</LOAD>
*#
*tcl(WriteCombinations)
*#
</FEMIX_DATA_FILE_V4.0>
*#
*#
*#endif
*#
*#
*endif
*#
*#
*# TESTE
*#endif
*# TESTE
