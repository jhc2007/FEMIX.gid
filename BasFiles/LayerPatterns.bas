*# CODE TO <LAYER_PATTERNS>
*#
*#
*#
*Set Var COUNT=tcl(NumOfLayerPatterns)
*#
*#
*if(COUNT > 0)
*#
<LAYER_PATTERNS>

## Keyword: _LAY_PATT
## Layer patterns
   COUNT = *COUNT ; # N. of layer patterns

## Content of each column:
#  A -> Counter
#  B -> Pattern name
#  C -> N. of layers
#  D -> Layer (or layer range)
#  E -> Name of the layer properties in the <LAYER_PROPERTIES> block
#
#   A  B                    C    D    E 

*tcl(WriteLayerPatterns)
</LAYER_PATTERNS>

*#
*endif