*# CODE FOR <COORDINATE_SYSTEMS>
*#
*#
*if(NLocalAxes > 0)
*#

<COORDINATE_SYSTEMS>

## Coordinate systems defined with 3 vectors
   COUNT = *NLocalAxes ; # N. of coordinate systems

## Content of each column:
#  A -> Counter
#  B -> Coordinate system name
#  C -> Name of the auxiliary vector - 1
#  D -> Name of the auxiliary vector - 2
#  E -> Name of the auxiliary vector - 3
#     A     B     C     D     E

*loop localaxes
*#
*format  "%7i"
*loopVar  *\
*#
*tcl(GetLocalAxesName *loopVar) *\
*#
*for(i=1; i<4; i=i+1)
*#
VECTOR_*tcl(GetAuxiliaryVectorIndexFromComponents *LocalAxesDef(*i) *LocalAxesDef(*operation(3+i)) *LocalAxesDef(*operation(6+i))) *\
*#
*endfor
*#
;
*#
*end localaxes

*#
</COORDINATE_SYSTEMS>

*#
*endif
*#
*# END CODE FOR <COORDINATE_SYSTEMS>