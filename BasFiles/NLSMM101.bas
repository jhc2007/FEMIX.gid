*# CODE FOR <NLSMM101>
*#
*Set Var COUNT=tcl(CountSpringMaterials NLSMM101)
*#
*if(COUNT > 0)

<NLSMM101>

## Keyword: _NLSMM101
## Properties of the NLMM361 material model
   COUNT = *COUNT ; # N. of NLSMM101 materials

## Content of each column:
#  A -> Counter
#  B -> Name of the material model
#  C -> Initial stiffness modulus
#  D -> Compression strength
#  E -> Ratio between the compression stress of the linear branch and the compression strength
#  F -> Type of unloading/reloading diagram
#       - Available keywords: _SECANT or _ELASTIC 
#     A     B     C     D     E     F

*tcl(WriteNLSMM101)
*#
</NLSMM101>

*#
*endif