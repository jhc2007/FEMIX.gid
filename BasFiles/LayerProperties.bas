*# CODE TO <LAYER_PROPERTIES>
*#
*#
*#
*Set Var COUNT=tcl(NumOfLayerProperties)
*#
*#
*if(COUNT > 0)
*#
<LAYER_PROPERTIES>

## Layer properties
   COUNT = *COUNT ; # N. of layer properties

## Content of each column:
#  A -> Counter
#  B -> Layer name
#  C -> Material type
#  D -> Material name
#  E -> Geometry type
#  F -> Geometry name
#  A  B              C            D            E           F
*#

*tcl(WriteLayerProperties)
</LAYER_PROPERTIES>

*#
*endif