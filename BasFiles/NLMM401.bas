*# CODE FOR <NLMM401>
*#
*Set Var COUNT = 0
*#
*loop materials
*#
*if(strcasecmp(MatProp(Material_Type),"NLMM401") == 0)
*Set Var COUNT = COUNT + 1
*endif
*#
*end materials
*#
*if(COUNT > 0)

<NLMM401>
## Keyword: _NLMM401
## Properties of the NLMM401 material model
   COUNT = *COUNT ; # N. of NLMM401 materials

## Content of each column:
#  A -> Counter
#  B -> Name of the material model
#  C -> Mass per unit volume
#  D -> Thermal conductivity and specific heat dependence
#       - Available keywords: _NONE, _ALPHAT
#  E -> Thermal conductivity
#  F -> Specific heat
#  G -> Volumetric cement content in the concrete mix
#  H -> Proportionality constant for the Arrhenius law (AT)
#  I -> Apparent activation energy  for the Arrhenius law (Ea)
#  J -> Total accumulated heat generation
#  K -> Initial degree of heat development
#  L -> N. of set of values defining the normalized heat generation function
#  M -> Values of normalized heat generation rate function
#     A     B     C     D     E     F     G     H     I     J     K     L     M

*Set Var INDEX = 1
*#
*loop materials
*#
*if(strcasecmp(MatProp(1),"NLMM401") == 0)
*format  "%7i"
*INDEX  *tcl(ReplaceSpaceWithUnderscore *MatProp(0))  *MatProp(2,real)  *\
*#
*if(strcasecmp(MatProp(3),"NONE") == 0)
_NONE*\
*else
_ALPHAT*\
*endif
*#
*MatProp(4,real)*MatProp(5,real)*MatProp(6,real)*MatProp(7,real)*MatProp(8,real)*MatProp(9,real)*MatProp(10,real)  *MatProp(13)
*#
*if(strcasecmp(MatProp(11),"User_Specified") == 0)
*tcl(WriteValuesOfNormalizedHeatGenerationRateFunction *MatProp(0) 1)
*else
*tcl(WriteValuesOfNormalizedHeatGenerationRateFunction *MatProp(12) 0)
*endif
*#
*Set Var INDEX = INDEX + 1
*endif
*#
*end materials

</NLMM401>

*endif