*# CODE FOR <FRAME_2D_CROSS_SECTIONS>
*#
*#
*Set Var COUNT = 0
*#
*Set Cond Element_Properties *layers
*#
*loop layers *OnlyInCond
*#
*if(strcasecmp(Cond(Geometry_Type),"FRAME_2D_CROSS_SECTIONS") == 0)
*Set Var COUNT = COUNT + 1
*elseif(strcasecmp(Cond(Geometry_Type),"GEOMETRY_PATTERNS") == 0 && strcasecmp(Cond(Geometry_Patterns_Type),"FRAME_2D_CROSS_SECTIONS") == 0)
*Set Var COUNT = COUNT + 2
*endif
*#
*end layers
*#
*#
*if(COUNT > 0)
*#
*#realformat "%15.5e"
*#
*#

<FRAME_2D_CROSS_SECTIONS>

## Keyword: _CS_2D
## Cross section properties of the 2D frame elements
   COUNT = *COUNT ; # N. of 2D frame cross sections

## Content of each column:
#  A -> Counter
#  B -> Name of the cross section
#  C -> Area
#  D -> Shear factor
#  E -> Moment of inertia
#  F -> Cross section height (used with differential temperature only)
#      A  B             C           D            E         F

*#
*Set Var INDEX = 1
*#
*Set Cond Element_Properties *layers
*#
*loop layers *OnlyInCond
*#
*if(strcasecmp(Cond(Geometry_Type),"FRAME_2D_CROSS_SECTIONS") == 0)
*#
*format "%7i"
*INDEX  CS_2D_*tcl(ReplaceSpaceWithUnderscore *LayerName)  *Cond(Area,real) *Cond(Shear_Factor,real) *Cond(Moment_of_Inertia,real) *Cond(Cross_Section_Height,real) ;
*Set Var INDEX = INDEX + 1
*#
*elseif(strcasecmp(Cond(Geometry_Type),"GEOMETRY_PATTERNS") == 0 && strcasecmp(Cond(Geometry_Patterns_Type),"FRAME_2D_CROSS_SECTIONS") == 0)
*#
*format "%7i"
*INDEX  CS_2D_1_GEOM_PATT_*tcl(ReplaceSpaceWithUnderscore *LayerName)  *Cond(Area_[1],real) *Cond(Shear_Factor_[1],real) *Cond(Moment_of_Inertia_[1],real) *Cond(Cross_Section_Height_[1],real) ;
*Set Var INDEX = INDEX + 1
*#
*format "%7i"
*INDEX  CS_2D_2_GEOM_PATT_*tcl(ReplaceSpaceWithUnderscore *LayerName)  *Cond(Area_[2],real) *Cond(Shear_Factor_[2],real) *Cond(Moment_of_Inertia_[2],real) *Cond(Cross_Section_Height_[2],real) ;
*Set Var INDEX = INDEX + 1
*#
*endif
*#
*end layers

</FRAME_2D_CROSS_SECTIONS>

*#
*#
*endif
*#
*#
*# END CODE FOR <FRAME_2D_CROSS_SECTIONS>