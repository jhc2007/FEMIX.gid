*# CODE FOR <NLMM301>
*#
*Set Var COUNT=tcl(NumOfNLMM301InLayerProperties)
*#
*loop materials
*#
*Set Var usedNLMM301=tcl(ExistsNLMM301InLayerProperties *MatProp(0))
*#
*if(strcasecmp(MatProp(Material_Type),"NLMM301") == 0 && usedNLMM301 == 0)
*Set Var COUNT = COUNT + 1
*endif
*#
*end materials
*#
*if(COUNT > 0)

<NLMM301>

## Properties of the NLMM301 material model
## Keyword: _NLMM301
   COUNT = *COUNT ; # N. of NLMM301 materials

## Content of each column:
#  A -> Counter
#  B -> Name of the material model
#  C -> Slip at the end of the linear bond stress-slip relationship
#  D -> Slip at peak bond stress
#  E -> Peak bond stress
#  F -> Parameter used in the definition of the pre-peak bond stress-slip relationship
#  G -> Parameter used in the definition of the post-peak bond stress-slip relationship
#  H -> Normal stiffness
*#
#     A     B     C     D     E     F     G     H

*Set Var INDEX = 1
*#
*loop materials
*#
*Set Var usedNLMM301=tcl(ExistsNLMM301InLayerProperties *MatProp(0))
*#
*if(strcasecmp(MatProp(1),"NLMM301") == 0 && usedNLMM301 == 0)

*format  "%7i"
*INDEX  *tcl(ReplaceSpaceWithUnderscore *MatProp(0)) *MatProp(2,real)*MatProp(3,real)*MatProp(4,real)*MatProp(5,real)*MatProp(6,real)*MatProp(7,real) ;
*#
*Set Var INDEX = INDEX + 1
*endif
*#
*end materials
*#
*tcl(WriteNLMM301InLayerProperties *INDEX)
*#
</NLMM301>
*endif
*#
*#
*# END CODE FOR <NLMM301>