*# CODE TO WRITE <POINT_SPRINGS>
*#
*Set Cond _Point_Springs *nodes *CanRepeat
*#
*if(CondNumEntities > 0)

<POINT_SPRINGS>

## Points with springs
   COUNT = *CondNumEntities ; # N. of point springs

## Content of each column:
#  A -> Counter (or counter range)
#  B -> Group name
#  C -> Phase (or phase range)
#  D -> Point number (or point number range)
#  E -> Direction vector:
#       - Global: _XG1, _XG2 or _XG3
#       - Specified: vector from the <AUXILIARY_VECTORS> block
#  F -> Spring location:
#       - Direction vector from the structure (S) to the ground (G): _SG
#       - Direction vector from the ground (G) to the structure (S): _GS
#         Example:
#           Direction vector: -----> _XG1
#                                            E     F
#           Structure - Spring - Ground  =>  _XG1  _SG
#           Ground - Spring - Structure  =>  _XG1  _GS
#  G -> Dof type (_TRANS - Translational; _ROT - Rotational)
#  H -> Spring material type
#  I -> Spring material name(s)
#     A     B     C     D     E     F     G     H     I

*tcl(WritePointSpringsList)
*#
</POINT_SPRINGS>
*endif