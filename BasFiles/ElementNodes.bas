<ELEMENT_NODES>

## Nodes defining the elements
   COUNT = *nelem ; # N. of elements

## Content of each column:
#  A -> Counter
#  B -> N. of nodes of the element
#  C -> Nodes of the element
#     A      B      C

*Set Var INTELEM=tcl(CreateExtraNodesList)
*#
*loop elems
*#
*format "%9i"
*ElemsNum*\
*#
*if(ElemsNnode == 2)
*#
*Set Var INTELEM=tcl(IsLayerOfInterfaceElements *ElemsLayerNum)
*#
*if(INTELEM == 1) *# is interface element
*#
*#Set Var INTELEM = ElemsNum
*Set Var CONEC1 = ElemsConec(1)
*Set Var CONEC2 = ElemsConec(2)
*Set Var NUMNODES = 0
*#
*loop nodes
*#
*if(NodesNum != CONEC1 && NodesNum != CONEC2)
*#
*Set Var SAMECOORDINATES=tcl(CompareNodeCoordinatesWithConec12 *NodesNum *CONEC1 *CONEC2)
*#
*if(SAMECOORDINATES > 0)
*#
*if(SAMECOORDINATES == 1)
*Set Var NODE1 = NodesNum
*else
*Set Var NODE2 = NodesNum
*endif
*#
*Set Var NUMNODES = NUMNODES + 1
*#
*endif
*#
*endif
*#
*if(NUMNODES == 2)
*break
*endif
*#
*end nodes
*#
*tcl(Write2NodesLinearInterfaceElemsConec *CONEC1 *CONEC2 *NODE1 *NODE2) ;
*#
*else *# is not interface element
*#
*format "%9i"
*ElemsNnode*\
*if(ElemsConec(2) < ElemsConec(1))
*format "%9i%9i"
*ElemsConec(2)*ElemsConec(1) ;
*else
*format "%9i%9i"
*ElemsConec ;
*endif
*#
*#Set Var TRASH=tcl(RemovePointLoads ElemsConec(1))
*#Set Var TRASH=tcl(RemovePointLoads ElemsConec(2))
*#
*#Set Var TRASH=tcl(RemoveInternalPointLoadsInsideLinearElement ElemsConec(1) ElemsConec(2))
*#
*endif
*#
*elseif(ElemsNnode == 3)
*#
*Set Var INTELEM=tcl(IsLayerOfInterfaceElements *ElemsLayerNum)
*#
*if(INTELEM == 1) *# is interface element
*#
*#Set Var INTELEM = ElemsNum
*Set Var CONEC1 = ElemsConec(1)
*Set Var CONEC2 = ElemsConec(2)
*Set Var CONEC3 = ElemsConec(3)
*Set Var NUMNODES = 0
*#
*loop nodes
*#
*if(NodesNum != CONEC1 && NodesNum != CONEC2 && NodesNum != CONEC3)
*#
*Set Var SAMECOORDINATES=tcl(CompareNodeCoordinatesWithConec123 *NodesNum *CONEC1 *CONEC2 *CONEC3)
*#
*if(SAMECOORDINATES > 0)
*#
*if(SAMECOORDINATES == 1)
*Set Var NODE1 = NodesNum
*elseif(SAMECOORDINATES == 2)
*Set Var NODE2 = NodesNum
*else
*Set Var NODE3 = NodesNum
*endif
*#
*Set Var NUMNODES = NUMNODES + 1
*#
*endif
*#
*endif
*#
*if(NUMNODES == 3)
*break
*endif
*#
*end nodes
*#
*tcl(Write3NodesLinearInterfaceElemsConec *CONEC1 *CONEC3 *CONEC2 *NODE1 *NODE3 *NODE2) ;
*#
*else *# is not interface element
*#
*format  "%9i%9i%9i%9i"
*ElemsNnode*ElemsConec(1)*ElemsConec(3)*ElemsConec(2) ;
*#
*endif
*#
*elseif(ElemsNnode == 4)
*#
*Set Var INTELEM=tcl(IsLayerOfInterfaceElements *ElemsLayerNum)
*#
*if(INTELEM == 1)
*#
*#Set Var INTELEM = ElemsNum
*Set Var CONEC1 = ElemsConec(1)
*Set Var CONEC2 = ElemsConec(2)
*Set Var INOTHERELEM = 0
*#
*loop elems
*#
*Set Var INTELEM=tcl(IsLayerOfInterfaceElements *ElemsLayerNum)
*#
*if(INTELEM == 0)
*#
*Set Var NumNodesFound = 0
*#
*for(i=1; i<=ElemsNnode; i=i+1)
*#
*if(ElemsConec(*i) == CONEC1 || ElemsConec(*i) == CONEC2)
*#
*Set Var NumNodesFound = NumNodesFound + 1
*Set Var INOTHERELEM = 1
*#
*endif
*#
*if(NumNodesFound == 2)
*break
*endif
*#
*endfor
*#
*endif
*#
*if(NumNodesFound == 2)
*break
*endif
*#
*end elems
*#
*format "%9i"
*ElemsNnode*\
*#
*if(INOTHERELEM == 0 || NumNodesFound == 2)
*#
*tcl(Write4NodesQuadInterfaceElemsConec *ElemsConec(1) *ElemsConec(2) *ElemsConec(4) *ElemsConec(3)) ;
*#
*else
*#
*tcl(Write4NodesQuadInterfaceElemsConec *ElemsConec(2) *ElemsConec(3) *ElemsConec(1) *ElemsConec(4)) ;
*#
*endif
*#
*else *# is not interface element
*#
*format "%9i%9i%9i%9i%9i"
*ElemsNnode*ElemsConec ;
*#
*endif
*#
*elseif(ElemsNnode == 8)
*#
*Set Var INTELEM=tcl(IsLayerOfInterfaceElements *ElemsLayerNum)
*#
*if(INTELEM) == 1)
*#
*Set Var CONEC1 = ElemsConec(1)
*Set Var CONEC2 = ElemsConec(2)
*Set Var CONEC3 = ElemsConec(3)
*Set Var CONEC4 = ElemsConec(4)
*Set Var CONEC5 = ElemsConec(5)
*Set Var CONEC6 = ElemsConec(6)
*Set Var CONEC7 = ElemsConec(7)
*Set Var CONEC8 = ElemsConec(8)
*#
*loop elems
*#
*Set Var NumNodesFound = 0
*#
*Set Var INTELEM=tcl(IsLayerOfInterfaceElements *ElemsLayerNum)
*#
*if(INTELEM == 0)
*#
*for(i=1; i<=ElemsNnode; i=i+1)
*#
*if(ElemsConec(*i) == CONEC1)
*#
*if(NumNodesFound == 0)
*Set Var NODE1 = 1
*elseif(NumNodesFound == 1)
*Set Var NODE2 = 1
*else
*Set Var NODE3 = 1
*endif
*#
*Set Var NumNodesFound = NumNodesFound + 1
*#
*elseif(ElemsConec(*i) == CONEC2)
*#
*if(NumNodesFound == 0)
*Set Var NODE1 = 3
*elseif(NumNodesFound == 1)
*Set Var NODE2 = 3
*else
*Set Var NODE3 = 3
*endif
*#
*Set Var NumNodesFound = NumNodesFound + 1
*#
*elseif(ElemsConec(*i) == CONEC3)
*#
*if(NumNodesFound == 0)
*Set Var NODE1 = 5
*elseif(NumNodesFound == 1)
*Set Var NODE2 = 5
*else
*Set Var NODE3 = 5
*endif
*#
*Set Var NumNodesFound = NumNodesFound + 1
*#
*elseif(ElemsConec(*i) == CONEC4)
*#
*if(NumNodesFound == 0)
*Set Var NODE1 = 7
*elseif(NumNodesFound == 1)
*Set Var NODE2 = 7
*else
*Set Var NODE3 = 7
*endif
*#
*Set Var NumNodesFound = NumNodesFound + 1
*#
*elseif(ElemsConec(*i) == CONEC5)
*#
*if(NumNodesFound == 0)
*Set Var NODE1 = 2
*elseif(NumNodesFound == 1)
*Set Var NODE2 = 2
*else
*Set Var NODE3 = 2
*endif
*#
*Set Var NumNodesFound = NumNodesFound + 1
*#
*elseif(ElemsConec(*i) == CONEC6)
*#
*if(NumNodesFound == 0)
*Set Var NODE1 = 4
*elseif(NumNodesFound == 1)
*Set Var NODE2 = 4
*else
*Set Var NODE3 = 4
*endif
*#
*Set Var NumNodesFound = NumNodesFound + 1
*#
*elseif(ElemsConec(*i) == CONEC7)
*#
*if(NumNodesFound == 0)
*Set Var NODE1 = 6
*elseif(NumNodesFound == 1)
*Set Var NODE2 = 6
*else
*Set Var NODE3 = 6
*endif
*#
*Set Var NumNodesFound = NumNodesFound + 1
*#
*elseif(ElemsConec(*i) == CONEC8)
*#
*if(NumNodesFound == 0)
*Set Var NODE1 = 8
*elseif(NumNodesFound == 1)
*Set Var NODE2 = 8
*else
*Set Var NODE3 = 8
*endif
*#
*Set Var NumNodesFound = NumNodesFound + 1
*#
*endif
*#
*if(NumNodesFound == 3)
*break
*endif
*#
*endfor
*#
*endif
*#
*if(NumNodesFound == 3)
*break
*endif
*#
*end elems
*#
        6*\
*#
*if(NumNodesFound == 3)
*#
*# Sort Number of Nodes
*if(NODE2 < NODE1)
*Set Var TMPNODE1 = NODE1
*Set Var NODE1 = NODE2
*Set Var NODE2 = TMPNODE1
*endif
*#
*if(NODE3 < NODE1)
*Set Var TMPNODE1 = NODE1
*Set Var TMPNODE2 = NODE2
*Set Var NODE1 = NODE3
*Set Var NODE2 = TMPNODE1
*Set Var NODE3 = TMPNODE2
*elseif(NODE3 < NODE2)
*Set Var TMPNODE2 = NODE2
*Set Var NODE2 = NODE3
*Set Var NODE3 = TMPNODE2
*endif
*#
*if(NODE1 == 1)
*#
*if(NODE2 == 7)
*Set Var NODE1 = 7
*Set Var NODE2 = 8
*Set Var NODE3 = 1
*elseif(NODE2 == 2 && NODE3 == 8)
*Set Var NODE1 = 8
*Set Var NODE2 = 1
*Set Var NODE3 = 2
*endif
*#
*endif
*#
*Set Var NODE4 = operation((NODE1+6)%8)
*if(NODE4 == 0)
*Set Var NODE4 = 8
*endif
*#
*Set Var NODE5 = operation((NODE2+4)%8)
*if(NODE5 == 0)
*Set Var NODE5 = 8
*endif
*#
*Set Var NODE6 = operation((NODE3+2)%8)
*if(NODE6 == 0)
*Set Var NODE6 = 8
*endif
*#
*Set Var NODE1=tcl(ConvertNodeNumFromFemixToGidOrder *NODE1)
*Set Var NODE2=tcl(ConvertNodeNumFromFemixToGidOrder *NODE2)
*Set Var NODE3=tcl(ConvertNodeNumFromFemixToGidOrder *NODE3)
*Set Var NODE4=tcl(ConvertNodeNumFromFemixToGidOrder *NODE4)
*Set Var NODE5=tcl(ConvertNodeNumFromFemixToGidOrder *NODE5)
*Set Var NODE6=tcl(ConvertNodeNumFromFemixToGidOrder *NODE6)
*#
*tcl(Write6NodesQuadInterfaceElemsConec *ElemsConec(*NODE1) *ElemsConec(*NODE2) *ElemsConec(*NODE3) *ElemsConec(*NODE4) *ElemsConec(*NODE5) *ElemsConec(*NODE6)) ;
*#
*Set Var INTELEM=tcl(AddExtraNodes *NODE1 *NODE4 *ElemsConec(1) *ElemsConec(2) *ElemsConec(3) *ElemsConec(4) *ElemsConec(5) *ElemsConec(6) *ElemsConec(7) *ElemsConec(8))
*#
*endif
*#
*#############################################################################
*#
*else *# is not interface element
*#
*format "%9i"
*ElemsNnode*\
*#
*if(strcasecmp(ElemsTypeName,"Quadrilateral") == 0)
*#
*format "%9i%9i%9i%9i%9i%9i%9i%9i"
*ElemsConec(1)*ElemsConec(5)*ElemsConec(2)*ElemsConec(6)*ElemsConec(3)*ElemsConec(7)*ElemsConec(4)*ElemsConec(8) ;
*#
*else *# ElemsTypeName == Hexahedra
*#
*format "%9i%9i%9i%9i%9i%9i%9i%9i"
*ElemsConec ;
*#
*endif
*#
*endif
*#
*elseif(ElemsNnode == 9)
*#
*format "%9i%9i%9i%9i%9i%9i%9i%9i%9i%9i"
*ElemsNnode*ElemsConec(1)*ElemsConec(5)*ElemsConec(2)*ElemsConec(6)*ElemsConec(3)*ElemsConec(7)*ElemsConec(4)*ElemsConec(8)*ElemsConec(9) ;
*#
*elseif(ElemsNnode == 20)
*#
*format "%9i%9i%9i%9i%9i%9i%9i%9i%9i%9i%9i%9i%9i%9i%9i%9i%9i%9i%9i%9i%9i"
*ElemsNnode*ElemsConec(1)*ElemsConec(9)*ElemsConec(2)*ElemsConec(10)*ElemsConec(3)*ElemsConec(11)*ElemsConec(4)*ElemsConec(12)*ElemsConec(13)*ElemsConec(14)*ElemsConec(15)*ElemsConec(16)*ElemsConec(5)*ElemsConec(17)*ElemsConec(6)*ElemsConec(18)*ElemsConec(7)*ElemsConec(19)*ElemsConec(8)*ElemsConec(20) ;
*#
*endif
*#
*for(i=1; i<=ElemsNnode; i=i+1)
*Set Var TRASH=tcl(RemovePointLoads *ElemsConec(*i))
*endfor
*#
*end elems
*#Set Var INTELEM=tcl(WriteExtraNodes)

</ELEMENT_NODES>
