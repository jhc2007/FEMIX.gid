*# CODE FOR <GEOMETRY_PATTERNS>
*#
*#
*Set Var COUNT=tcl(NumOfGeometryPatternsInLayerProperties)
*#
*Set Cond Element_Properties *layers
*#
*loop layers *OnlyInCond
*#
*if(strcasecmp(Cond(Geometry_Type),"GEOMETRY_PATTERNS") == 0)
*Set Var COUNT = COUNT + 1
*endif
*#
*end layers
*#
*#
*if(COUNT > 0)
*#
*#
<GEOMETRY_PATTERNS>

## Keyword: _GEOM_PATT
## Geometry patterns of several types of properties
   COUNT = *COUNT ; # N. of geometry patterns

## Content of each column:
#  A -> Counter
#  B -> Pattern name
#  C -> Type of geometry description
#       - Available keywords: _CS_AREA, _CS_2D, _CS_3D or _LAM_THICK
#  D -> N. of nodes of the pattern
#  E -> Names of geometry descriptions
#      A   B          C                D  E

*#
*Set Var INDEX = 1
*#
*Set Cond Element_Properties *layers
*#
*loop layers *OnlyInCond
*#
*if(strcasecmp(Cond(Geometry_Type),"GEOMETRY_PATTERNS") == 0)
*format "%7i"
*INDEX  GEOM_PATT_*tcl(ReplaceSpaceWithUnderscore *LayerName)  *\
*#
*if(strcasecmp(Cond(Geometry_Patterns_Type),"CROSS_SECTION_AREAS") == 0)
_CS_AREA    2  CS_AREA_1_GEOM_PATT_*tcl(ReplaceSpaceWithUnderscore *LayerName)  *\  
CS_AREA_2_GEOM_PATT_*tcl(ReplaceSpaceWithUnderscore *LayerName) ;
*elseif(strcasecmp(Cond(Geometry_Patterns_Type),"FRAME_2D_CROSS_SECTIONS") == 0)
_CS_2D      2  CS_2D_1_GEOM_PATT_*tcl(ReplaceSpaceWithUnderscore *LayerName)  *\
CS_2D_2_GEOM_PATT_*tcl(ReplaceSpaceWithUnderscore *LayerName) ;
*elseif(strcasecmp(Cond(Geometry_Patterns_Type),"FRAME_3D_CROSS_SECTIONS") == 0)
_CS_3D      2  CS_3D_1_GEOM_PATT_*tcl(ReplaceSpaceWithUnderscore *LayerName)  *\
CS_3D_2_GEOM_PATT_*tcl(ReplaceSpaceWithUnderscore *LayerName) ;
*else
_LAM_THICK  *Cond(Number_of_Nodes)  *\
*for(i=1; i<=Cond(Number_of_Nodes,int); i=i+1)
LAM_THICK_*i_GEOM_PATT_*tcl(ReplaceSpaceWithUnderscore *LayerName)  *\
*endfor
;
*endif
*#
*Set Var INDEX = INDEX + 1
*endif
*#
*end layers
*#
*tcl(WriteGeometryPatternsInLayerProperties *INDEX)
*#
</GEOMETRY_PATTERNS>

*#
*endif
*#
*# END CODE FOR <GEOMETRY_PATTERNS>