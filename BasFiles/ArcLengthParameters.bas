*if(GenData(Arc_Length,int) == 1)
*#
<ARC_LENGTH_PARAMETERS>

*if(strcasecmp(GenData(Displacement_Control),"DEFAULT") == 0)
*#
	DISPLACEMENT_CONTROL = _Y ;
	POINT_NUMBER = *GenData(Node_Number) ;
	DISPLACEMENT_INCREMENT = *GenData(Displacement_Increment) ;
*#
*else
*#
	RELATIVE_DISPLACEMENT_CONTROL = _Y ;
	POINT_A_NUMBER = *GenData(Node_Number_[A]) ;
	POINT_B_NUMBER = *GenData(Node_Number_[B]) ;
	RELATIVE_DISPLACEMENT_INCREMENT = *GenData(Relative_Displacement_Increment) ;
*#
*endif
*#
	DEGREE_OF_FREEDOM = *\
*#
*if(numDimension == 2)
*#
*if(strcasecmp(GenData(Degree_of_Freedom),"X") == 0)
_D2 ;
*elseif(strcasecmp(GenData(Degree_of_Freedom),"Y") == 0)
_D3 ;
*else
_D1 ;
*endif
*#	
*else *# numDimension == 3
*#
*if(strcasecmp(GenData(Degree_of_Freedom),"X") == 0)
_D1 ;
*elseif(strcasecmp(GenData(Degree_of_Freedom),"Y") == 0)
_D2 ;
*else
_D3 ;
*endif
*#
*endif
*#
*if(strcasecmp(GenData(Arc_Length_Parameters),"CUSTOM") == 0)
*#
*if(GenData(Constant_Radius,int) == 0)
	CONSTANT_RADIUS = _N ;
	LOAD_FACTOR = *GenData(Load_Factor) ;
*else
	CONSTANT_RADIUS = _Y ;
	RADIUS_FACTOR = *GenData(Radius_Factor) ;
*endif
*#
	FORCE_DISPLACEMENT_SCALING_FACTOR = *GenData(Force_Displacement_Scaling_Factor) ;
    MOMENT_ROTATION_SCALING_FACTOR = *GenData(Moment_Rotation_Scaling_Factor) ;
*#
*endif

</ARC_LENGTH_PARAMETERS>

*#
*endif