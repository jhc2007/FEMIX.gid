*# CODE FOR <LINEAR_INTERFACE_LINE_2D_PROPERTIES>
*#
*Set Var COUNT=tcl(NumOfLinIntLine2DInLayerProperties)
*#
*loop materials
*#
*Set Var usedLinIntLine2D=tcl(ExistsLinIntLine2DInLayerProperties *MatProp(0))
*#
*if(strcasecmp(MatProp(Material_Type),"LIN_INT_LINE_2D") == 0 && usedLinIntLine2D == 0)
*Set Var COUNT = COUNT + 1
*endif
*#
*end materials
*#
*if(COUNT > 0)

<LINEAR_INTERFACE_LINE_2D_PROPERTIES>

## Keyword: _LIN_INT_LINE_2D
## Properties of linear interface elements (2D only)
   COUNT = *COUNT ; # N. of types of line interface elements
 
## Content of each column:
#  A -> Counter
#  B -> Name of the type of line interface element
#  C -> Normal stiffness
#  D -> Tangential stiffness
#     A     B     C     D

*Set Var INDEX = 1
*#
*loop materials
*#
*Set Var usedLinIntLine2D=tcl(ExistsLinIntLine2DInLayerProperties *MatProp(0))
*#
*if(strcasecmp(MatProp(1),"LIN_INT_LINE_2D") == 0 && usedLinIntLine2D == 0)

*format  "%7i"
*INDEX  *tcl(ReplaceSpaceWithUnderscore *MatProp(0)) *MatProp(2,real)*MatProp(3,real) ;
*#
*Set Var INDEX = INDEX + 1
*endif
*#
*end materials
*#
*tcl(WriteLinIntLine2DInLayerProperties *INDEX)
*#
</LINEAR_INTERFACE_LINE_2D_PROPERTIES>
*endif
*#
*#
*# END CODE FOR </LINEAR_INTERFACE_LINE_2D_PROPERTIES>