*# CODE FOR <LINEAR_SPRING_MATERIALS>
*#
*Set Var COUNT=tcl(CountSpringMaterials LIN_SPRING)
*#
*if(COUNT > 0)

<LINEAR_SPRING_MATERIALS>

## Keyword: _LIN_SPRING
## Properties of the linear spring materials
   COUNT = *COUNT ; # N. of linear spring materials

## Content of each column:
#  A -> Counter
#  B -> Material name
#  C -> Stiffness
#     A  B  C

*tcl(WriteLinearSpringMaterials)
*#
</LINEAR_SPRING_MATERIALS>

*#
*endif