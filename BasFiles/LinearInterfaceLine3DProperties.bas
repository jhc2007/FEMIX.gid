*# CODE FOR <LINEAR_INTERFACE_LINE_3D_PROPERTIES>
*#
*Set Var COUNT=tcl(NumOfLinIntLine3DInLayerProperties)
*#
*loop materials
*#
*Set Var usedLinIntLine3D=tcl(ExistsLinIntLine3DInLayerProperties *MatProp(0))
*#
*if(strcasecmp(MatProp(Material_Type),"LIN_INT_LINE_3D") == 0 && usedLinIntLine3D == 0)
*Set Var COUNT = COUNT + 1
*endif
*#
*end materials
*#
*if(COUNT > 0)

<LINEAR_INTERFACE_LINE_3D_PROPERTIES>

## Keyword: _LIN_INT_LINE_3D
## Properties of linear interface (3D only)
   COUNT = *COUNT ; # N. of types of line interface elements
 
## Content of each column:
#  A -> Counter
#  B -> Name of the type of line interface element
#  C -> Normal stiffness (l2)
#  D -> Normal stiffness (l3)
#  E -> Tangential stiffness
#     A     B     C     D     E

*Set Var INDEX = 1
*#
*loop materials
*#
*Set Var usedLinIntLine3D=tcl(ExistsLinIntLine3DInLayerProperties *MatProp(0))
*#
*if(strcasecmp(MatProp(1),"LIN_INT_LINE_3D") == 0 && usedLinIntLine3D == 0)

*format  "%7i"
*INDEX  *tcl(ReplaceSpaceWithUnderscore *MatProp(0)) *MatProp(2,real)*MatProp(3,real)*MatProp(4,real) ;
*#
*Set Var INDEX = INDEX + 1
*endif
*#
*end materials
*#
*tcl(WriteLinIntLine3DInLayerProperties *INDEX)
*#
</LINEAR_INTERFACE_LINE_3D_PROPERTIES>
*endif
*#
*#
*# END CODE FOR </LINEAR_INTERFACE_LINE_3D_PROPERTIES>