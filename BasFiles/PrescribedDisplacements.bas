*# CODE FOR <PRESCRIBED_DISPLACEMENTS>
*#
*Set Cond _Prescribed_Displacements *nodes
*#
*Set Var COUNTPRESCRIBEDDISPLACEMENTS = 0
*#
*loop nodes *OnlyInCond
*#
*for(i=1; i<7; i=i+1)
*#
*if(Cond(*i,real) != 0)
*Set Var COUNTPRESCRIBEDDISPLACEMENTS = COUNTPRESCRIBEDDISPLACEMENTS + 1
*endif
*#
*endfor
*#
*end nodes
*#
*#
*#
*#*if(loopVar == 1)
*#*#
*#*Set Var FIRSTNODE = NodesNum
*#*Set Var LASTNODE = NodesNum
*#*Set Var XDISPLACEMENT = Cond(1,real)
*#*Set Var YDISPLACEMENT = Cond(2,real)
*#*Set Var ZDISPLACEMENT = Cond(3,real)
*#*Set Var XROTATION = Cond(4,real)
*#*Set Var YROTATION = Cond(5,real)
*#*Set Var ZROTATION = Cond(6,real)
*#*#
*#*else *#// loopVar != 1
*#*#
*#*if(XDISPLACEMENT == Cond(1,real) && YDISPLACEMENT == Cond(2,real) && ZDISPLACEMENT == Cond(3,real) && XROTATION == Cond(4,real) && YROTATION == Cond(5,real) && ZROTATION == Cond(6,real) && NodesNum == LASTNODE + 1)
*#*#
*#*Set Var LASTNODE = NodesNum
*#*#
*#*else
*#*#
*#*if(XDISPLACEMENT != 0)
*#*Set Var COUNTPRESCRIBEDDISPLACEMENTS = COUNTPRESCRIBEDDISPLACEMENTS + 1
*#*endif
*#*if(YDISPLACEMENT != 0)
*#*Set Var COUNTPRESCRIBEDDISPLACEMENTS = COUNTPRESCRIBEDDISPLACEMENTS + 1
*#*endif
*#*if(ZDISPLACEMENT != 0)
*#*Set Var COUNTPRESCRIBEDDISPLACEMENTS = COUNTPRESCRIBEDDISPLACEMENTS + 1
*#*endif
*#*if(XROTATION != 0)
*#*Set Var COUNTPRESCRIBEDDISPLACEMENTS = COUNTPRESCRIBEDDISPLACEMENTS + 1
*#*endif
*#*if(YROTATION != 0)
*#*Set Var COUNTPRESCRIBEDDISPLACEMENTS = COUNTPRESCRIBEDDISPLACEMENTS + 1
*#*endif
*#*if(ZROTATION != 0)
*#*Set Var COUNTPRESCRIBEDDISPLACEMENTS = COUNTPRESCRIBEDDISPLACEMENTS + 1
*#*endif
*#*#
*#*Set Var FIRSTNODE = NodesNum
*#*Set Var LASTNODE = NodesNum
*#*Set Var XDISPLACEMENT = Cond(1,real)
*#*Set Var YDISPLACEMENT = Cond(2,real)
*#*Set Var ZDISPLACEMENT = Cond(3,real)
*#*Set Var XROTATION = Cond(4,real)
*#*Set Var YROTATION = Cond(5,real)
*#*Set Var ZROTATION = Cond(6,real)
*#*#
*#*endif
*#*#
*#*endif
*#*#
*#*if(loopVar == CondNumEntities)
*#*#
*#*for(i=1; i<7; i=i+1)
*#*#
*#*if(Cond(*i,real) != 0)
*#*Set Var COUNTPRESCRIBEDDISPLACEMENTS = COUNTPRESCRIBEDDISPLACEMENTS + 1
*#*endif
*#*#
*#*endfor
*#*#
*#*endif
*#*#
*#*end nodes
*#
*#
*#
*if(COUNTPRESCRIBEDDISPLACEMENTS > 0)
*#
<PRESCRIBED_DISPLACEMENTS>

## Points with prescribed displacement
   COUNT = *COUNTPRESCRIBEDDISPLACEMENTS ; # N. of prescribed displacements

## Content of each column:
#  A -> Counter (or counter range)
#  B -> Point number (or point number range)
#  C -> Prescribed degree of freedom
#       - Available keywords: _D1, _D2, _D3, _R1, _R2 or _R3
#  D -> Value of the prescribed displacement
#     A         B     C             D

*#
*Set Var INDEX = 1
*#
*loop nodes *OnlyInCond
*#
*if(loopVar == 1)
*#
*Set Var FIRSTNODE = NodesNum
*Set Var LASTNODE = NodesNum
*Set Var XDISPLACEMENT = Cond(1,real)
*Set Var YDISPLACEMENT = Cond(2,real)
*Set Var ZDISPLACEMENT = Cond(3,real)
*Set Var XROTATION = Cond(4,real)
*Set Var YROTATION = Cond(5,real)
*Set Var ZROTATION = Cond(6,real)
*#
*else *#// loopVar != 1
*#
*if(XDISPLACEMENT == Cond(1,real) && YDISPLACEMENT == Cond(2,real) && ZDISPLACEMENT == Cond(3,real) && XROTATION == Cond(4,real) && YROTATION == Cond(5,real) && ZROTATION == Cond(6,real) && NodesNum == LASTNODE + 1)
*#
*Set Var LASTNODE = NodesNum
*#
*else
*#
*if(numDimension == 2)
*#
*if(XDISPLACEMENT != 0)
*#
*if(FIRSTNODE == LASTNODE)
*format "%7i"
*INDEX  *FIRSTNODE  *\
*Set Var INDEX = INDEX + 1
*else
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  [*FIRSTNODE-*LASTNODE]  *\
*Set Var INDEX = INDEX + LASTNODE - FIRSTNODE + 1
*endif
*#
_D2 *XDISPLACEMENT(real) ;
*#
*endif
*#
*if(YDISPLACEMENT != 0)
*#
*if(FIRSTNODE == LASTNODE)
*format "%7i"
*INDEX  *FIRSTNODE  *\
*Set Var INDEX = INDEX + 1
*else
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  [*FIRSTNODE-*LASTNODE]  *\
*Set Var INDEX = INDEX + LASTNODE - FIRSTNODE + 1
*endif
*#
_D3 *YDISPLACEMENT(real) ;
*#
*endif
*#
*if(ZDISPLACEMENT != 0)
*#
*if(FIRSTNODE == LASTNODE)
*format "%7i"
*INDEX  *FIRSTNODE  *\
*Set Var INDEX = INDEX + 1
*else
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  [*FIRSTNODE-*LASTNODE]  *\
*Set Var INDEX = INDEX + LASTNODE - FIRSTNODE + 1
*endif
*#
_D1 *ZDISPLACEMENT(real) ;
*#
*endif
*#
*if(XROTATION != 0)
*#
*if(FIRSTNODE == LASTNODE)
*format "%7i"
*INDEX  *FIRSTNODE  *\
*Set Var INDEX = INDEX + 1
*else
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  [*FIRSTNODE-*LASTNODE]  *\
*Set Var INDEX = INDEX + LASTNODE - FIRSTNODE + 1
*endif
*#
_R2 *XROTATION(real) ;
*#
*endif
*#
*if(YROTATION != 0)
*#
*if(FIRSTNODE == LASTNODE)
*format "%7i"
*INDEX  *FIRSTNODE  *\
*Set Var INDEX = INDEX + 1
*else
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  [*FIRSTNODE-*LASTNODE]  *\
*Set Var INDEX = INDEX + LASTNODE - FIRSTNODE + 1
*endif
*#
_R3 *YROTATION(real) ;
*#
*endif
*#
*if(ZROTATION != 0)
*#
*if(FIRSTNODE == LASTNODE)
*format "%7i"
*INDEX  *FIRSTNODE  *\
*Set Var INDEX = INDEX + 1
*else
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  [*FIRSTNODE-*LASTNODE]  *\
*Set Var INDEX = INDEX + LASTNODE - FIRSTNODE + 1
*endif
*#
_R1 *ZROTATION(real) ;
*#
*endif
*#
*else *# numDimension != 2
*#
*if(XDISPLACEMENT != 0)
*#
*if(FIRSTNODE == LASTNODE)
*format "%7i"
*INDEX  *FIRSTNODE  *\
*Set Var INDEX = INDEX + 1
*else
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  [*FIRSTNODE-*LASTNODE]  *\
*Set Var INDEX = INDEX + LASTNODE - FIRSTNODE + 1
*endif
*#
_D1 *XDISPLACEMENT(real) ;
*#
*endif
*#
*if(YDISPLACEMENT != 0)
*#
*if(FIRSTNODE == LASTNODE)
*format "%7i"
*INDEX  *FIRSTNODE  *\
*Set Var INDEX = INDEX + 1
*else
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  [*FIRSTNODE-*LASTNODE]  *\
*Set Var INDEX = INDEX + LASTNODE - FIRSTNODE + 1
*endif
*#
_D2 *YDISPLACEMENT(real) ;
*#
*endif
*#
*if(ZDISPLACEMENT != 0)
*#
*if(FIRSTNODE == LASTNODE)
*format "%7i"
*INDEX  *FIRSTNODE  *\
*Set Var INDEX = INDEX + 1
*else
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  [*FIRSTNODE-*LASTNODE]  *\
*Set Var INDEX = INDEX + LASTNODE - FIRSTNODE + 1
*endif
*#
_D3 *ZDISPLACEMENT(real) ;
*#
*endif
*#
*if(XROTATION != 0)
*#
*if(FIRSTNODE == LASTNODE)
*format "%7i"
*INDEX  *FIRSTNODE  *\
*Set Var INDEX = INDEX + 1
*else
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  [*FIRSTNODE-*LASTNODE]  *\
*Set Var INDEX = INDEX + LASTNODE - FIRSTNODE + 1
*endif
*#
_R1 *XROTATION(real) ;
*#
*endif
*#
*if(YROTATION != 0)
*#
*if(FIRSTNODE == LASTNODE)
*format "%7i"
*INDEX  *FIRSTNODE  *\
*Set Var INDEX = INDEX + 1
*else
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  [*FIRSTNODE-*LASTNODE]  *\
*Set Var INDEX = INDEX + LASTNODE - FIRSTNODE + 1
*endif
*#
_R2 *YROTATION(real) ;
*#
*endif
*#
*if(ZROTATION != 0)
*#
*if(FIRSTNODE == LASTNODE)
*format "%7i"
*INDEX  *FIRSTNODE  *\
*Set Var INDEX = INDEX + 1
*else
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  [*FIRSTNODE-*LASTNODE]  *\
*Set Var INDEX = INDEX + LASTNODE - FIRSTNODE + 1
*endif
*#
_R3 *ZROTATION(real) ;
*#
*endif
*#
*endif
*#
*Set Var FIRSTNODE = NodesNum
*Set Var LASTNODE = NodesNum
*Set Var XDISPLACEMENT = Cond(1,real)
*Set Var YDISPLACEMENT = Cond(2,real)
*Set Var ZDISPLACEMENT = Cond(3,real)
*Set Var XROTATION = Cond(4,real)
*Set Var YROTATION = Cond(5,real)
*Set Var ZROTATION = Cond(6,real)
*#
*endif
*#
*endif
*#
*if(loopVar == CondNumEntities)
*#
*if(numDimension == 2)
*#
*if(XDISPLACEMENT != 0)
*#
*if(FIRSTNODE == LASTNODE)
*format "%7i"
*INDEX  *FIRSTNODE  *\
*Set Var INDEX = INDEX + 1
*else
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  [*FIRSTNODE-*LASTNODE]  *\
*Set Var INDEX = INDEX + LASTNODE - FIRSTNODE + 1
*endif
*#
_D2 *XDISPLACEMENT(real) ;
*#
*endif
*#
*if(YDISPLACEMENT != 0)
*#
*if(FIRSTNODE == LASTNODE)
*format "%7i"
*INDEX  *FIRSTNODE  *\
*Set Var INDEX = INDEX + 1
*else
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  [*FIRSTNODE-*LASTNODE]  *\
*Set Var INDEX = INDEX + LASTNODE - FIRSTNODE + 1
*endif
*#
_D3 *YDISPLACEMENT(real) ;
*#
*endif
*#
*if(ZDISPLACEMENT != 0)
*#
*if(FIRSTNODE == LASTNODE)
*format "%7i"
*INDEX  *FIRSTNODE  *\
*Set Var INDEX = INDEX + 1
*else
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  [*FIRSTNODE-*LASTNODE]  *\
*Set Var INDEX = INDEX + LASTNODE - FIRSTNODE + 1
*endif
*#
_D1 *ZDISPLACEMENT(real) ;
*#
*endif
*#
*if(XROTATION != 0)
*#
*if(FIRSTNODE == LASTNODE)
*format "%7i"
*INDEX  *FIRSTNODE  *\
*Set Var INDEX = INDEX + 1
*else
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  [*FIRSTNODE-*LASTNODE]  *\
*Set Var INDEX = INDEX + LASTNODE - FIRSTNODE + 1
*endif
*#
_R2 *XROTATION(real) ;
*#
*endif
*#
*if(YROTATION != 0)
*#
*if(FIRSTNODE == LASTNODE)
*format "%7i"
*INDEX  *FIRSTNODE  *\
*Set Var INDEX = INDEX + 1
*else
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  [*FIRSTNODE-*LASTNODE]  *\
*Set Var INDEX = INDEX + LASTNODE - FIRSTNODE + 1
*endif
*#
_R3 *YROTATION(real) ;
*#
*endif
*#
*if(ZROTATION != 0)
*#
*if(FIRSTNODE == LASTNODE)
*format "%7i"
*INDEX  *FIRSTNODE  *\
*Set Var INDEX = INDEX + 1
*else
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  [*FIRSTNODE-*LASTNODE]  *\
*Set Var INDEX = INDEX + LASTNODE - FIRSTNODE + 1
*endif
*#
_R1 *ZROTATION(real) ;
*#
*endif
*#
*else *# numDimension != 2
*#
*if(XDISPLACEMENT != 0)
*#
*if(FIRSTNODE == LASTNODE)
*format "%7i"
*INDEX  *FIRSTNODE  *\
*Set Var INDEX = INDEX + 1
*else
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  [*FIRSTNODE-*LASTNODE]  *\
*Set Var INDEX = INDEX + LASTNODE - FIRSTNODE + 1
*endif
*#
_D1 *XDISPLACEMENT(real) ;
*#
*endif
*#
*if(YDISPLACEMENT != 0)
*#
*if(FIRSTNODE == LASTNODE)
*format "%7i"
*INDEX  *FIRSTNODE  *\
*Set Var INDEX = INDEX + 1
*else
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  [*FIRSTNODE-*LASTNODE]  *\
*Set Var INDEX = INDEX + LASTNODE - FIRSTNODE + 1
*endif
*#
_D2 *YDISPLACEMENT(real) ;
*#
*endif
*#
*if(ZDISPLACEMENT != 0)
*#
*if(FIRSTNODE == LASTNODE)
*format "%7i"
*INDEX  *FIRSTNODE  *\
*Set Var INDEX = INDEX + 1
*else
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  [*FIRSTNODE-*LASTNODE]  *\
*Set Var INDEX = INDEX + LASTNODE - FIRSTNODE + 1
*endif
*#
_D3 *ZDISPLACEMENT(real) ;
*#
*endif
*#
*if(XROTATION != 0)
*#
*if(FIRSTNODE == LASTNODE)
*format "%7i"
*INDEX  *FIRSTNODE  *\
*Set Var INDEX = INDEX + 1
*else
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  [*FIRSTNODE-*LASTNODE]  *\
*Set Var INDEX = INDEX + LASTNODE - FIRSTNODE + 1
*endif
*#
_R1 *XROTATION(real) ;
*#
*endif
*#
*if(YROTATION != 0)
*#
*if(FIRSTNODE == LASTNODE)
*format "%7i"
*INDEX  *FIRSTNODE  *\
*Set Var INDEX = INDEX + 1
*else
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  [*FIRSTNODE-*LASTNODE]  *\
*Set Var INDEX = INDEX + LASTNODE - FIRSTNODE + 1
*endif
*#
_R2 *YROTATION(real) ;
*#
*endif
*#
*if(ZROTATION != 0)
*#
*if(FIRSTNODE == LASTNODE)
*format "%7i"
*INDEX  *FIRSTNODE  *\
*Set Var INDEX = INDEX + 1
*else
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  [*FIRSTNODE-*LASTNODE]  *\
*Set Var INDEX = INDEX + LASTNODE - FIRSTNODE + 1
*endif
*#
_R3 *ZROTATION(real) ;
*#
*endif
*#
*endif
*#
*endif
*#
*end nodes

</PRESCRIBED_DISPLACEMENTS>
*endif
