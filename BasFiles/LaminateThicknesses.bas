*# CODE FOR <LAMINATE_THICKNESSES>
*#
*#
*#
*Set Var COUNTLAMINATETHICK=tcl(NumOfLaminateThicknessesInLayerProperties)
*#
*Set Cond Element_Properties *layers
*#
*loop layers *OnlyInCond
*#
*if(Cond(Layered_Configuration,int) == 0)
*#
*if(strcasecmp(Cond(Geometry_Type),"LAMINATE_THICKNESSES") == 0)
*Set Var COUNTLAMINATETHICK = COUNTLAMINATETHICK + 1
*elseif(strcasecmp(Cond(Geometry_Type),"GEOMETRY_PATTERNS") == 0 && strcasecmp(Cond(Geometry_Patterns_Type),"LAMINATE_THICKNESSES") == 0)
*Set Var COUNTLAMINATETHICK = operation(COUNTLAMINATETHICK+Cond(Number_of_Nodes,int))
*endif
*#
*endif
*#
*end layers
*#
*if(COUNTLAMINATETHICK > 0)
*#

<LAMINATE_THICKNESSES>

## Keyword: _LAM_THICK
## Thickness of the laminate elements
   COUNT = *COUNTLAMINATETHICK ; # N. of nodal thicknesses

## Content of each column:
#  A -> Counter
#  B -> Name of the thickness
#  C -> Thickness
#     A  B                C

*#
*Set Var INDEX = 1
*#
*Set Cond Element_Properties *layers
*#
*loop layers *OnlyInCond
*#
*if(Cond(Layered_Configuration,int) == 0)
*#
*if(strcasecmp(Cond(Geometry_Type),"LAMINATE_THICKNESSES") == 0)
*#
*format "%7i"
*INDEX  LAM_THICK_*tcl(ReplaceSpaceWithUnderscore *LayerName)  *Cond(Thickness,real) ;
*Set Var INDEX = INDEX + 1
*#
*elseif(strcasecmp(Cond(Geometry_Type),"GEOMETRY_PATTERNS") == 0 && strcasecmp(Cond(Geometry_Patterns_Type),"LAMINATE_THICKNESSES") == 0)
*#
*for(i=1; i<=Cond(Number_of_Nodes,int); i=i+1)
*#
*format "%7i"
*INDEX  LAM_THICK_*i_GEOM_PATT_*tcl(ReplaceSpaceWithUnderscore *LayerName)  *Cond(*operation(51+i),real) ;
*Set Var INDEX = INDEX + 1
*#
*endfor
*#
*endif
*#
*endif
*#
*end layers
*#
*tcl(WriteLaminateThicknessesInLayerProperties *INDEX) 
*#
</LAMINATE_THICKNESSES>

*endif