*# CODE FOR <NLMM101>
*#
*Set Var COUNT=tcl(NumOfNLMM101InLayerProperties)
*#
*loop materials
*#
*Set Var usedNLMM101=tcl(ExistsNLMM101InLayerProperties *MatProp(0))
*#
*if(strcasecmp(MatProp(Material_Type),"NLMM101") == 0 && usedNLMM101 == 0)
*Set Var COUNT = COUNT + 1
*endif
*#
*end materials
*#
*if(COUNT > 0)

<NLMM101>

## Keyword: _NLMM101
## Properties of the NLMM101 material model
   COUNT = *COUNT ; # N. of NLMM101 materials

## Content of each column:
#  A -> Counter
#  B -> Name of the material model
#  C -> Mass per unit volume
#  D -> Temperature coefficient
#  E -> Poisson's coefficient
#  F -> Young's modulus
#  G -> Compressive strength
#  H -> Tensile strength
#  I -> Type of tensile-softening diagram
#       - Available keywords: _TRILINEAR or _CORNELISSEN
#  J -> Ratio between the strain at the first post-peak point and the
#       ultimate strain of the trilinear tensile-softening diagram (fracture mode I)
#  K -> Ratio between the stress at the first post-peak point and the
#       tensile strength of the trilinear tensile-softening diagram (fracture mode I)
#  L -> Ratio between the strain at the second post-peak point and the
#       ultimate strain of the trilinear tensile-softening diagram (fracture mode I)
#  M -> Ratio between the stress at the second post-peak point and the
#       tensile strength of the trilinear tensile-softening diagram (fracture mode I)
#  N -> Mode I fracture energy (Gf)
#  O -> Parameter to define the mode I fracture energy available to the new crack
#  P -> Type of shear retention factor law (keyword or constant value)
#       - Available keywords: _LINEAR, _QUADRATIC or _CUBIC
#  Q -> Type of unloading/reloading diagram
#       - Available keywords: _SECANT or _ELASTIC
#  R -> Crack band width for the fracture mode I (keyword or constant value)
#       - Available keywords: _SQRT_ELEMENT or _SQRT_IP
#  S -> Maximum n. of cracks
#  T -> Threshold angle (degress)

*Set Var INDEX = 1
*#
*loop materials
*#
*Set Var usedNLMM101=tcl(ExistsNLMM101InLayerProperties *MatProp(0))
*#
*if(strcasecmp(MatProp(1),"NLMM101") == 0 && usedNLMM101 == 0)

#     A     B     C     D     E     F     G
*format  "%7i"
*INDEX  *tcl(ReplaceSpaceWithUnderscore *MatProp(0))*MatProp(2,real)*MatProp(3,real)*MatProp(4,real)*MatProp(5,real)*MatProp(6,real)
#     H     I     J     K     L     M    
  *MatProp(7,real)  _*MatProp(8)*MatProp(9,real)*MatProp(10,real)*MatProp(11,real)*MatProp(12,real)
#     N     O     P     Q     R     S     T  
*#
*if(strcasecmp(MatProp(8),"TRILINEAR") == 0)
  *MatProp(13,real)*\
*else
  *MatProp(14,real)*\
*endif
*#
*MatProp(15,real)  _*MatProp(16)  _*MatProp(17)*\
*#
*if(strcasecmp(MatProp(18),"VALUE")==0)
*MatProp(19,real)  *\ 
*else
  _*MatProp(18)  *\ 
*endif
*#
*MatProp(20)  *MatProp(21) ;

*#
*#
*Set Var INDEX = INDEX + 1
*endif
*#
*end materials
*#
*tcl(WriteNLMM101InLayerProperties *INDEX)

</NLMM101>
*endif
*#
*#
*# END CODE FOR <NLMM101>