*# CODE FOR <PRESCRIBED_POINT_TEMPERATURES>
*#
*Set Cond _Prescribed_Temperatures *nodes
*#
*#Set Var COUNT = 0
*#
*#loop nodes *OnlyInCond
*#Set Var COUNT = COUNT + 1
*#end nodes
*#
*#if(COUNT > 0)
*#
*if(CondNumEntities > 0)

<PRESCRIBED_POINT_TEMPERATURES>
## Points with prescribed temperature
   COUNT = *CondNumEntities ; # N. of prescribed point temperatures

## Content of each column:
#  A -> Counter (or counter range)
#  B -> Point number (or point number range)
#  C -> Value of the prescribed temperature
#     A     B     C

*Set Var INDEX = 1
*#
*loop nodes *OnlyInCond
*#
*if(loopVar == 1)
*#
*Set Var FIRSTNODE = NodesNum
*Set Var LASTNODE = NodesNum
*Set Var TEMPERATURE = Cond(1,real)
*#
*else *#// loopVar != 1
*#
*if(TEMPERATURE == Cond(1,real) && NodesNum == LASTNODE + 1)
*#
*Set Var LASTNODE = NodesNum
*#
*else
*#
*if(FIRSTNODE == LASTNODE)
*format "%7i"
*INDEX  *FIRSTNODE  *\
*Set Var INDEX = INDEX + 1
*else
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  [*FIRSTNODE-*LASTNODE]  *\
*Set Var INDEX = INDEX + LASTNODE - FIRSTNODE + 1
*endif
*#
*TEMPERATURE ;
*#
*Set Var FIRSTNODE = NodesNum
*Set Var LASTNODE = NodesNum
*Set Var TEMPERATURE = Cond(1,real)
*#
*endif
*#
*endif
*#
*end nodes
*#
*if(FIRSTNODE == LASTNODE)
*format "%7i"
*INDEX  *FIRSTNODE  *\
*else
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  [*FIRSTNODE-*LASTNODE]  *\
*endif
*#
*TEMPERATURE ;

</PRESCRIBED_POINT_TEMPERATURES>

*endif