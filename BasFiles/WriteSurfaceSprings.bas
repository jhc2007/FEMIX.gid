*# CODE TO WRITE <SURFACE_SPRINGS>
*#
*#Set Cond _Surface_Springs *nodes
*#
*#if(CondNumEntities > 0)
*#
*Set Var COUNT=tcl(CountSurfaceSprings)
*Set Var CountFromFemixFile=tcl(CountSurfaceSpringsFromFemixFile)
*#
*if(operation(COUNT+CountFromFemixFile) > 0)
*#
<SURFACE_SPRINGS>

## Surface with springs
   COUNT = *operation(COUNT+CountFromFemixFile) ; # N. of surface springs

## Content of each column:
#  A -> Counter (or counter range)
#  B -> Group name
#  C -> Phase (or phase range)
#  D -> Element number (or element number range)
#  E -> Face number
#  F -> Direction vector:
#       - Global: _XG1, _XG2 or _XG3
#       - Solid/Shell: _L1, _L2 or _N
#       - Specified: vector from the <AUXILIARY_VECTORS> block
#  G -> Spring location:
#       - Direction vector from the structure (S) to the ground (G): _SG
#       - Direction vector from the ground (G) to the structure (S): _GS
#         Example:
#           Direction vector: -----> _XG1
#                                            F     G
#           Structure - Spring - Ground  =>  _XG1  _SG
#           Ground - Spring - Structure  =>  _XG1  _GS
#  H -> Integration type (spring stiffness matrix)
#  I -> Integration name (spring stiffness matrix)
#  J -> Spring material type
#  K -> N. of nodes of the face of the element
#  L -> Spring material name(s)
#
#  Notes:
#         when K is equal to 1 the spring is constant
#         Only translational stiffness is available
#
#  Direction vector keywords:
#     _L1 - tangential to the face and following the l1 local axis
#     _L2 - tangential to the face and following the l2 local axis
#     _N  - normal to the face
#
#     A     B     C     D     E     F     G     H     I     J     K     L

*tcl(WriteSurfaceSpringsList)
*tcl(WriteSurfaceSpringsFromFemixFile *COUNT)
*#
</SURFACE_SPRINGS>

*endif
*#endif
