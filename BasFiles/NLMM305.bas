*# CODE FOR <NLMM305>
*#
*Set Var COUNT=tcl(NumOfNLMM305InLayerProperties)
*#
*loop materials
*#
*Set Var usedNLMM305=tcl(ExistsNLMM305InLayerProperties *MatProp(0))
*#
*if(strcasecmp(MatProp(Material_Type),"NLMM305") == 0 && usedNLMM305 == 0)
*Set Var COUNT = COUNT + 1
*endif
*#
*end materials
*#
*if(COUNT > 0)

<NLMM305>

## Properties of the NLMM305 material model
## Keyword: _NLMM305
COUNT = *COUNT ; # N. of NLMM305 materials

## Content of each column:
#  A -> Counter
#  B -> Name of the material model
#  C -> Slip at the end of the linear bond stress-slip relationship
#  D -> Slip at peak bond stress
#  E -> Material cohesion
#  F -> Friction angle between materials in contact
#  G -> Parameter used in the definition of the pre-peak bond stress-slip relationship
#  H -> Parameter used in the definition of the post-peak bond stress-slip relationship
#  I -> Normal stiffness
#     A     B     C     D     E     F     G     H     I

*Set Var INDEX = 1
*#
*loop materials
*#
*Set Var usedNLMM305=tcl(ExistsNLMM305InLayerProperties *MatProp(0))
*#
*if(strcasecmp(MatProp(1),"NLMM305") == 0 && usedNLMM305 == 0)

*format  "%7i"
*INDEX  *tcl(ReplaceSpaceWithUnderscore *MatProp(0)) *MatProp(2,real)*MatProp(3,real)*MatProp(4,real)*MatProp(5,real)*MatProp(6,real)*MatProp(7,real)*MatProp(8,real) ;
*#
*Set Var INDEX = INDEX + 1
*endif
*#
*end materials
*#
*tcl(WriteNLMM305InLayerProperties *INDEX)
*#
</NLMM305>
*endif
*#
*#
*# END CODE FOR <NLMM305>