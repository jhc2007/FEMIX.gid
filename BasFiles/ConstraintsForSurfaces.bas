*# CODE TO CONSTRAINTS FOR SURFACES
*#
*#Set Var INDEX = 1
*#
*Set Cond Constraints_for_Surfaces *nodes
*#
*loop nodes *OnlyInCond
*#
*if(loopVar == 1)
*#
*Set Var LAYERNUMBER = NodesLayerNum
*Set Var FIRSTNODE = NodesNum
*Set Var LASTNODE = NodesNum
*Set Var XDISPLACEMENT = Cond(1,int)
*Set Var YDISPLACEMENT = Cond(2,int)
*Set Var ZDISPLACEMENT = Cond(3,int)
*Set Var XROTATION = Cond(4,int)
*Set Var YROTATION = Cond(5,int)
*Set Var ZROTATION = Cond(6,int)
*#
*else *#// loopVar != 1
*#
*if(XDISPLACEMENT == Cond(1,int) && YDISPLACEMENT == Cond(2,int) && ZDISPLACEMENT == Cond(3,int) && XROTATION == Cond(4,int) && YROTATION == Cond(5,int) && ZROTATION == Cond(6,int) && NodesNum == LASTNODE + 1)
*#
*Set Var LASTNODE = NodesNum
*#
*else
*#
*if(FIRSTNODE == LASTNODE)
*INDEX  *tcl(GetFirstGroupName)  1  *FIRSTNODE  *\
*else
[*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  *tcl(GetFirstGroupName)  1  [*FIRSTNODE-*LASTNODE]  *\
*endif
*#
*operation(XDISPLACEMENT+YDISPLACEMENT+ZDISPLACEMENT+XROTATION+YROTATION+ZROTATION)  *\
*#
*if(numDimension == 2)
*#
*if(XDISPLACEMENT == 1)
_D2  *\
*endif
*if(YDISPLACEMENT == 1)
_D3  *\
*endif
*if(ZDISPLACEMENT == 1)
_D1  *\
*endif
*if(XROTATION == 1)
_R2  *\
*endif
*if(YROTATION == 1)
_R3  *\
*endif
*if(ZROTATION == 1)
_R1  *\
*endif
*#
*else *# numDimension == 3
*#
*if(XDISPLACEMENT == 1)
_D1  *\
*endif
*if(YDISPLACEMENT == 1)
_D2  *\
*endif
*if(ZDISPLACEMENT == 1)
_D3  *\
*endif
*if(XROTATION == 1)
_R1  *\
*endif
*if(YROTATION == 1)
_R2  *\
*endif
*if(ZROTATION == 1)
_R3  *\
*endif
*#
*endif
*#
;
*#
*Set Var INDEX = operation(INDEX+1+LASTNODE-FIRSTNODE)
*Set Var FIRSTNODE = NodesNum
*Set Var LASTNODE = NodesNum
*Set Var XDISPLACEMENT = Cond(1,int)
*Set Var YDISPLACEMENT = Cond(2,int)
*Set Var ZDISPLACEMENT = Cond(3,int)
*Set Var XROTATION = Cond(4,int)
*Set Var YROTATION = Cond(5,int)
*Set Var ZROTATION = Cond(6,int)
*#
*endif
*#
*endif
*#
*if(loopVar == CondNumEntities)
*#
*if(FIRSTNODE == LASTNODE)
*INDEX  *tcl(GetFirstGroupName)  1  *FIRSTNODE  *\
*else
[*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  *tcl(GetFirstGroupName)   1  [*FIRSTNODE-*LASTNODE]  *\
*endif
*#
*#Set Var COUNTDEGREESFREEDOM = 0
*#for(i=1; i<7; i=i+1)
*#if(Cond(*i,int) == 1)
*#Set Var COUNTDEGREESFREEDOM = COUNTDEGREESFREEDOM + 1
*#endif
*#endfor
*#
*#COUNTDEGREESFREEDOM  *\
*#
*operation(XDISPLACEMENT+YDISPLACEMENT+ZDISPLACEMENT+XROTATION+YROTATION+ZROTATION)  *\
*#
*if(numDimension == 2)
*#
*if(XDISPLACEMENT == 1)
_D2  *\
*endif
*if(YDISPLACEMENT == 1)
_D3  *\
*endif
*if(ZDISPLACEMENT == 1)
_D1  *\
*endif
*if(XROTATION == 1)
_R2  *\
*endif
*if(YROTATION == 1)
_R3  *\
*endif
*if(ZROTATION == 1)
_R1  *\
*endif
*#
*else *# numDimension != 2
*#
*if(XDISPLACEMENT == 1)
_D1  *\
*endif
*if(YDISPLACEMENT == 1)
_D2  *\
*endif
*if(ZDISPLACEMENT == 1)
_D3  *\
*endif
*if(XROTATION == 1)
_R1  *\
*endif
*if(YROTATION == 1)
_R2  *\
*endif
*if(ZROTATION == 1)
_R3  *\
*endif
*#
*endif
*#
;
*#
*endif
*#
*end nodes