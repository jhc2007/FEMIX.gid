*# CODE TO <THERMAL_TRANSIENT_INITIAL_CONDITIONS>
*#
*Set Cond _Thermal_Transient_Initial_Conditions *nodes
*#
*if(CondNumEntities > 0)
*#
<THERMAL_TRANSIENT_INITIAL_CONDITIONS>
## Points with thermal transient initial conditions
   COUNT = *CondNumEntities ; # N. of thermal transient initial conditions

## Content of each column:
#  A -> Counter (or counter range)
#  B -> Point number (or point number range)
#  C -> Value of the initial temperature
#     A     B     C

*Set Var INDEX = 1
*#
*loop nodes *OnlyInCond
*#
*if(loopVar == 1)
*#
*Set Var FIRSTNODE = NodesNum
*Set Var LASTNODE = NodesNum
*Set Var TEMPERATURE = Cond(1,real)
*#
*else *#// loopVar > 1
*#
*if(TEMPERATURE == Cond(1,real) && NodesNum == LASTNODE + 1)
*#
*Set Var LASTNODE = NodesNum
*#
*else
*#
*if(FIRSTNODE == LASTNODE)
*format "%7i"
*INDEX  *FIRSTNODE  *\
*Set Var INDEX = INDEX + 1
*else
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  [*FIRSTNODE-*LASTNODE]  *\
*Set Var INDEX = INDEX + LASTNODE - FIRSTNODE + 1
*endif
*#
*TEMPERATURE ;
*#
*Set Var FIRSTNODE = NodesNum
*Set Var LASTNODE = NodesNum
*Set Var TEMPERATURE = Cond(1,real)
*#
*endif
*#
*endif
*#
*end nodes
*#
*if(FIRSTNODE == LASTNODE)
*format "%7i"
*INDEX  *FIRSTNODE  *\
*else
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  [*FIRSTNODE-*LASTNODE]  *\
*endif
*#
*TEMPERATURE ;

</THERMAL_TRANSIENT_INITIAL_CONDITIONS>

*endif