*# CODE FOR <EDGE_LOADS>
*#
*Set Var COUNT1=tcl(CreateEdgeLoadsList *loopVar)
*Set Var COUNT2=tcl(CountEdgeLoadsFromFemixFile *loopVar)
*Set Var COUNT = COUNT1 + COUNT2
*#
*if(COUNT > 0)

<EDGE_LOADS>

## Load on the edge of an element
   COUNT = *COUNT ; # N. of edge loads

## Content of each column:
#  A -> Counter (or counter range)
#  B -> Loaded element (or loaded element range)
#  C -> Loaded face
#  D -> Loaded edge
#  E -> Direction vector:
#       - Global: _XG1, _XG2 or _XG3
#       - Frame: _L1, _L2 or _L3
#       - Solid/Shell: _T, _NN or _NT
#       - Plane stress/Plane strain/Axisymmetry: _T or _NT
#       - Specified: vector from the <AUXILIARY_VECTORS> block
#  F -> Type of generalized force (_FORC - Force; _MOM - Moment)
#  G -> N. of nodes of the edge of the element
#  H -> Load values
#
#  Note: when G is equal to 1 the load is constant
#
#  Direction vector keywords:
#     _L1 - tangential to the bar
#     _L2 - normal to the bar and following axis l2
#     _L3 - normal to the bar and following axis l3
#     _T  - tangential to the edge
#     _NN - normal to the edge and normal to the element face
#     _NT - normal to the edge and tangential to the element face
#
#     A     B     C     D     E     F     G     H

*tcl(WriteEdgeLoadsList)
*tcl(WriteEdgeLoadsFromFemixFile *loopVar *COUNT1)
*#
</EDGE_LOADS>
*#
*endif