*# CODE FOR <FRAME_3D_CROSS_SECTIONS>
*#
*#
*Set Var COUNT = 0
*#
*Set Cond Element_Properties *layers
*#
*loop layers *OnlyInCond
*#
*if(strcasecmp(Cond(Geometry_Type),"FRAME_3D_CROSS_SECTIONS") == 0)
*Set Var COUNT = COUNT + 1
*elseif(strcasecmp(Cond(Geometry_Type),"GEOMETRY_PATTERNS") == 0 && strcasecmp(Cond(Geometry_Patterns_Type),"FRAME_3D_CROSS_SECTIONS") == 0)
*Set Var COUNT = COUNT + 2
*endif
*#
*end layers
*#
*#
*if(COUNT > 0)
*#
*#realformat "%15.5e"
*#
*#

<FRAME_3D_CROSS_SECTIONS>

## Keyword: _CS_3D
## Cross section properties of the 3D frame elements
   COUNT = *COUNT ; # N. of 3D frame cross sections

## Content of each column:
#  A -> Counter
#  B -> Name of the cross section
#  C -> Area
#  D -> Shear factor (l2)
#  E -> Shear factor (l3)
#  F -> Torsional constant
#  G -> Moment of inertia (l2)
#  H -> Moment of inertia (l3)
#  I -> Local coordinate of the shear center (l2)
#  J -> Local coordinate of the shear center (l3)
#  K -> Cross section height (l2) (used with differential temperature only)
#  L -> Cross section height (l3) (used with differential temperature only)
#  M -> Angle (degrees)
# A  B           C    D    E       F        G    H    I    J    K    L     M

*#
*Set Var INDEX = 1
*#
*Set Cond Element_Properties *layers
*#
*loop layers *OnlyInCond
*#
*if(strcasecmp(Cond(Geometry_Type),"FRAME_3D_CROSS_SECTIONS") == 0)
*#
*format "%7i"
*INDEX  CS_3D_*tcl(ReplaceSpaceWithUnderscore *LayerName)  *Cond(Area,real) *Cond(9,real) *Cond(10,real) *Cond(11,real) *Cond(13,real) *Cond(14,real) *Cond(15,real) *Cond(16,real) *Cond(18,real) *Cond(19,real) *Cond(20,real) ;
*Set Var INDEX = INDEX + 1
*#
*elseif(strcasecmp(Cond(Geometry_Type),"GEOMETRY_PATTERNS") == 0 && strcasecmp(Cond(Geometry_Patterns_Type),"FRAME_3D_CROSS_SECTIONS") == 0)
*#
*format "%7i"
*INDEX  CS_3D_1_GEOM_PATT_*tcl(ReplaceSpaceWithUnderscore *LayerName)  *Cond(Area_[1],real) *\
*for(i=28; i<=37; i=i+1)
*Cond(*i,real) *\
*endfor
; 
*Set Var INDEX = INDEX + 1
*#
*format "%7i"
*INDEX  CS_3D_2_GEOM_PATT_*tcl(ReplaceSpaceWithUnderscore *LayerName)  *Cond(Area_[2],real) *\
*for(i=42; i<=51; i=i+1)
*Cond(*i,real) *\
*endfor
;
*Set Var INDEX = INDEX + 
*#
*endif
*#
*end layers

</FRAME_3D_CROSS_SECTIONS>

*#
*#
*endif
*#
*#
*# END CODE FOR <FRAME_3D_CROSS_SECTIONS>