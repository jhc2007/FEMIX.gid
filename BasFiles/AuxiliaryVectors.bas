*# CODE FOR <AUXILIARY_VECTORS>
*#
*#if(NLocalAxes > 0)
*#
*#loop localaxes
*#
*#for(i=1; i<4; i=i+1)
*#
*#Set Var TRASH=tcl(AddAuxiliaryVector *LocalAxesDef(*i) *LocalAxesDef(*operation(3+i)) *LocalAxesDef(*operation(6+i)))
*#
*#endfor
*#
*#end localaxes
*#
*#endif
*#
*Set Var COUNT=tcl(CountAuxiliaryVectors)
*#
*if(COUNT > 0)
*#

<AUXILIARY_VECTORS>

## Auxiliary vectors (global coordinate system)
## All vectors are normalized immediately after input
   COUNT = *COUNT ; # N. of auxiliary vectors

## Content of each column:
#  A -> Counter
#  B -> Name of the auxiliary vector
#  C -> Component of the auxiliary vector - 1
#  D -> Component of the auxiliary vector - 2
#  E -> Component of the auxiliary vector - 3
#     A     B     C     D     E

*tcl(WriteAuxiliaryVectorsList)
*#
</AUXILIARY_VECTORS>

*endif
*#
*# END CODE FOR <AUXILIARY_VECTORS>