*# CODE FOR <POINT_COORDINATES>
*#
*#
<POINT_COORDINATES>

## Point coordinates (global coordinate system)
   COUNT = *npoin ; # N. of points

## Content of each column:
#  A -> Counter
#  B -> Coordinate - XG1
#  C -> Coordinate - XG2
#  D -> Coordinate - XG3
#     A                    B                    C                    D

*if(numDimension == 2)
*#
*loop nodes
*format  "%7i"
*loopVar *NodesCoord(3,real) *NodesCoord(1,real) *NodesCoord(2,real) ;
*end nodes
*#
*else
*#
*loop nodes
*format  "%7i"
*loopVar *NodesCoord(1,real) *NodesCoord(2,real) *NodesCoord(3,real) ;
*end nodes
*#
*endif

</POINT_COORDINATES>

*# END CODE FOR <POINT_COORDINATES>