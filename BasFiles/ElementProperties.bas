*# CODE FOR <ELEMENT_PROPERTIES>
*#
*#
<ELEMENT_PROPERTIES>
*#
*Set Cond Element_Properties *layers
*Set Var INDEX=tcl(CreateElementPropertiesList)
*#
*loop layers *OnlyInCond
*#
*#Set Var FIRSTMATERIALNUM = 0
*#Set Var LASTMATERIALNUM = 0
*#				
*Set Layer *LayerName *elems
*#
*Set Var FIRSTELEMENT = 0
*#
*loop elems *OnlyInLayer
*#
*if(loopVar == 1)
*#
*Set Var FIRSTELEMENT = ElemsNum
*Set Var LASTELEMENT = ElemsNum
*Set Var FIRSTMATERIALNUM = ElemsMat
*Set Var LASTMATERIALNUM = ElemsMat
*#
*else *# loopVar != 1
*#
*Set Var NEWLASTELEMENT = LASTELEMENT + 1
*#
*if(ElemsNum == NEWLASTELEMENT)
*#
*Set Var LASTELEMENT = ElemsNum
*Set Var LASTMATERIALNUM = ElemsMat
*#
*if(Cond(Layered_Configuration,int) == 0)
*#
*if(LASTMATERIALNUM == 0 || FIRSTMATERIALNUM == 0)
*MessageBox error: Define material for all elements
*endif
*if(LASTMATERIALNUM != FIRSTMATERIALNUM)
*MessageBox error: The same material must be defined for whole layer
*endif
*#
*endif
*#
*else *# ElemsNum != NEWLASTELEMENT
*#
*Set Var TRASH=tcl(CreateElementProperties *LayerName)
*#
*Set Var INDEX = INDEX + 1
*#            
*Set Var TRASH=tcl(AddElementPropertiesElements *FIRSTELEMENT *LASTELEMENT)
*#
*Set Var FIRSTELEMENT = ElemsNum
*Set Var LASTELEMENT = ElemsNum
*Set Var FIRSTMATERIALNUM = ElemsMat
*Set Var LASTMATERIALNUM = ElemsMat
*#
*if(Cond(Non_Linear_Problem,int) == 0)
*#
*if(strcasecmp(Cond(Element_Type),"Select") == 0)
*MessageBox error: Define Element Properties for all Layers
*endif
*#
*Set Var TRASH=tcl(AddElementPropertiesValue *Cond(Element_Type) 1)
*#
*else *# Cond(Non_Linear_Problem,int) == 1
*#
*if(strcasecmp(Cond(_Element_Type),"Select") == 0)
*MessageBox error: Define Element Properties for all Layers
*endif
*#
*Set Var TRASH=tcl(AddElementPropertiesValue *Cond(_Element_Type) 1)
*#
*endif
*#
*if(Cond(Layered_Configuration,int) == 0)
*#
*loop materials
*#
*if(MatNum == FIRSTMATERIALNUM)
*#
*Set Var TRASH=tcl(AddElementPropertiesValue *MatProp(1) 1)
*#
*Set Var TRASH=tcl(AddElementPropertiesValue *MatProp(0) 0)
*#
*break
*#
*endif
*#
*end materials
*#
*if(strcasecmp(Cond(Geometry_Type),"CROSS_SECTION_AREAS") == 0 && Cond(Area,real) != 0)
*Set Var TRASH=tcl(AddElementPropertiesValue CS_AREA 1)
*elseif(strcasecmp(Cond(Geometry_Type),"FRAME_2D_CROSS_SECTIONS") == 0)
*Set Var TRASH=tcl(AddElementPropertiesValue CS_2D 1)
*elseif(strcasecmp(Cond(Geometry_Type),"FRAME_3D_CROSS_SECTIONS") == 0)
*Set Var TRASH=tcl(AddElementPropertiesValue CS_3D 1)
*elseif(strcasecmp(Cond(Geometry_Type),"LAMINATE_THICKNESSES") == 0)
*Set Var TRASH=tcl(AddElementPropertiesValue LAM_THICK 1)
*elseif(strcasecmp(Cond(Geometry_Type),"GEOMETRY_PATTERNS") == 0)
*Set Var TRASH=tcl(AddElementPropertiesValue GEOM_PATT 1)
*else
*Set Var TRASH=tcl(AddElementPropertiesValue NONE 1)
*endif
*#
*else *# Cond(Layered_Configuration,int) == 1
*#
*Set Var TRASH=tcl(AddElementPropertiesValue LAY_PATT 1)
*#
*Set Var TRASH=tcl(AddElementPropertiesValue *Cond(Layer_Pattern_Name) 0)
*#
*Set Var TRASH=tcl(AddElementPropertiesValue NONE 1)
*#
*endif
*#
*#
*if(Cond(Number_of_Keywords,int) == 0)
*#
*Set Var TRASH=tcl(AddElementPropertiesValue NONE 1)
*Set Var TRASH=tcl(AddElementPropertiesValue NONE 1)
*#
*else
*#
*if(Cond(Layered_Configuration,int) == 0)
*#
*if(strcasecmp(Cond(Integration_Type),"GAUSS-LEGENDRE") == 0)
*Set Var TRASH=tcl(AddElementPropertiesValue GLEG 1)
*elseif(strcasecmp(Cond(Integration_Type),"GAUSS-LOBATTO") == 0)
*Set Var TRASH=tcl(AddElementPropertiesValue GLOB 1)
*elseif(strcasecmp(Cond(Integration_Type),"NEWTON-COTES") == 0)
*Set Var TRASH=tcl(AddElementPropertiesValue NCOTES 1)
*endif
*Set Var TRASH=tcl(AddElementPropertiesValue *Cond(Integration_Name) 0)
*#
*else *# Cond(Layered_Configuration,int) == 1
*#
*if(strcasecmp(Cond(_Integration_Type),"GAUSS-LEGENDRE") == 0)
*Set Var TRASH=tcl(AddElementPropertiesValue GLEG 1)
*elseif(strcasecmp(Cond(_Integration_Type),"GAUSS-LOBATTO") == 0)
*Set Var TRASH=tcl(AddElementPropertiesValue GLOB 1)
*elseif(strcasecmp(Cond(_Integration_Type),"NEWTON-COTES") == 0)
*Set Var TRASH=tcl(AddElementPropertiesValue NCOTES 1)
*endif
*Set Var TRASH=tcl(AddElementPropertiesValue *Cond(_Integration_Name) 0)
*#
*endif
*#
*endif
*#
*Set Var TRASH=tcl(AddElementPropertiesList)
*#
*endif
*#
*endif
*#
*end elems
*#
*if(FIRSTELEMENT == 0)
*MessageBox error: There's a layer without elements
*endif
*#
*Set Var TRASH=tcl(CreateElementProperties *LayerName)
*#
*Set Var INDEX = INDEX + 1
*#
*Set Var TRASH=tcl(AddElementPropertiesElements *FIRSTELEMENT *LASTELEMENT)
*#
*if(Cond(Non_Linear_Problem,int) == 0)
*#
*if(strcasecmp(Cond(Element_Type),"Select") == 0)
*MessageBox error: Define Element Properties for all Layers
*endif
*#
*Set Var TRASH=tcl(AddElementPropertiesValue *Cond(Element_Type) 1)
*#
*else *# Cond(Non_Linear_Problem,int) == 1
*#
*if(strcasecmp(Cond(_Element_Type),"Select") == 0)
*MessageBox error: Define Element Properties for all Layers
*endif
*#
*Set Var TRASH=tcl(AddElementPropertiesValue *Cond(_Element_Type) 1)
*#
*endif
*#
*if(Cond(Layered_Configuration,int) == 0)
*#
*loop materials
*#
*if(MatNum == FIRSTMATERIALNUM)
*#
*Set Var TRASH=tcl(AddElementPropertiesValue *MatProp(1) 1)
*#
*Set Var TRASH=tcl(AddElementPropertiesValue *MatProp(0) 0)
*#
*break
*#
*endif
*#
*end materials
*#
*if(strcasecmp(Cond(Geometry_Type),"CROSS_SECTION_AREAS") == 0 && Cond(Area,real) != 0)
*Set Var TRASH=tcl(AddElementPropertiesValue CS_AREA 1)
*elseif(strcasecmp(Cond(Geometry_Type),"FRAME_2D_CROSS_SECTIONS") == 0)
*Set Var TRASH=tcl(AddElementPropertiesValue CS_2D 1)
*elseif(strcasecmp(Cond(Geometry_Type),"FRAME_3D_CROSS_SECTIONS") == 0)
*Set Var TRASH=tcl(AddElementPropertiesValue CS_3D 1)
*elseif(strcasecmp(Cond(Geometry_Type),"LAMINATE_THICKNESSES") == 0)
*Set Var TRASH=tcl(AddElementPropertiesValue LAM_THICK 1)
*elseif(strcasecmp(Cond(Geometry_Type),"GEOMETRY_PATTERNS") == 0)
*Set Var TRASH=tcl(AddElementPropertiesValue GEOM_PATT 1)
*else
*Set Var TRASH=tcl(AddElementPropertiesValue NONE 1)
*endif
*#
*else
*#
*Set Var TRASH=tcl(AddElementPropertiesValue LAY_PATT 1)
*#
*Set Var TRASH=tcl(AddElementPropertiesValue *Cond(Layer_Pattern_Name) 0)
*#
*Set Var TRASH=tcl(AddElementPropertiesValue NONE 1)
*#
*endif
*#
*#
*if(Cond(Number_of_Keywords,int) == 0)
*#
*Set Var TRASH=tcl(AddElementPropertiesValue NONE 1)
*Set Var TRASH=tcl(AddElementPropertiesValue NONE 1)
*#
*else
*#
*if(Cond(Layered_Configuration,int) == 0)
*#
*if(strcasecmp(Cond(Integration_Type),"GAUSS-LEGENDRE") == 0)
*Set Var TRASH=tcl(AddElementPropertiesValue GLEG 1)
*elseif(strcasecmp(Cond(Integration_Type),"GAUSS-LOBATTO") == 0)
*Set Var TRASH=tcl(AddElementPropertiesValue GLOB 1)
*elseif(strcasecmp(Cond(Integration_Type),"NEWTON-COTES") == 0)
*Set Var TRASH=tcl(AddElementPropertiesValue NCOTES 1)
*endif
*Set Var TRASH=tcl(AddElementPropertiesValue *Cond(Integration_Name) 0)
*#
*else
*#
*if(strcasecmp(Cond(_Integration_Type),"GAUSS-LEGENDRE") == 0)
*Set Var TRASH=tcl(AddElementPropertiesValue GLEG 1)
*elseif(strcasecmp(Cond(_Integration_Type),"GAUSS-LOBATTO") == 0)
*Set Var TRASH=tcl(AddElementPropertiesValue GLOB 1)
*elseif(strcasecmp(Cond(_Integration_Type),"NEWTON-COTES") == 0)
*Set Var TRASH=tcl(AddElementPropertiesValue NCOTES 1)
*endif
*Set Var TRASH=tcl(AddElementPropertiesValue *Cond(_Integration_Name) 0)
*#
*endif
*#
*endif
*#
*Set Var TRASH=tcl(AddElementPropertiesList)
*#
*end layers
*#

## Specification of the element properties
   COUNT = *tcl(CountElementProperties) ; # N. of specification

## Content of each column:
#  A -> Counter
#  B -> Group name
#  C -> Phase (or phase range)
#  D -> Element (or element range)
#  E -> Element type
#  F -> Material type
#  G -> Material name
#  H -> Geometry type
#  I -> Geometry name
#  J -> Integration type (stiffness matrix)
#  K -> Integration name (stiffness matrix)
#     A     B     C     D     E     F     G     H     I     J     K

*tcl(WriteElementPropertiesList)
*#
</ELEMENT_PROPERTIES>

*#