*# CODE FOR <SUPPORTS>
*#
*Set Cond Constraints_for_Points *nodes
*#
*Set Var COUNT = CondNumEntities
*#
*Set Cond Constraints_for_Lines *nodes
*#
*Set Var COUNT = operation(COUNT+CondNumEntities)
*#
*Set Cond Constraints_for_Surfaces *nodes
*#
*Set Var COUNT = operation(COUNT+CondNumEntities)
*#
*if(COUNT > 0)
*#
<SUPPORTS>
## Points with fixed degrees of freedom
   COUNT = *COUNT ; # N. of points with fixed degrees of freedom

## Content of each column:
#  A -> Counter (or counter range)
#  B -> Group name
#  C -> Phase (or phase range)
#  D -> Point number (or point number range)
#  E -> N. of fixed degrees of freedom in the current point(s)
#  F -> Fixed degrees of freedom:
#       - Available keywords: _D1, _D2, _D3, _R1, _R2 or _R3
#     A     B     C     D     E     F

*include .\ConstraintsForPoints.bas
*include .\ConstraintsForLines.bas
*include .\ConstraintsForSurfaces.bas

</SUPPORTS>
*#
*endif