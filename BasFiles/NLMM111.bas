*# CODE FOR <NLMM111>
*#
*Set Var COUNT=tcl(NumOfNLMM111InLayerProperties)
*#
*loop materials
*#
*Set Var usedNLMM111=tcl(ExistsNLMM111InLayerProperties *MatProp(0))
*#
*if(strcasecmp(MatProp(Material_Type),"NLMM111") == 0 && usedNLMM111 == 0)
*Set Var COUNT = COUNT + 1
*endif
*#
*end materials
*# 

<NLMM111>

## Keyword: _NLMM111
## Properties of the NLMM111 material model
   COUNT = *COUNT ; # N. of NLMM111 materials

## Content of each column:
#  A -> Counter
#  B -> Name of the material model
#  C -> Mass per unit volume
#  D -> Temperature coefficient
#  E -> Poisson's coefficient
#  F -> Young's modulus
#  G -> Compressive strength
#  H -> Tensile strength
#  I -> Type of tensile-softening diagram
#       - Available keywords: _TRILINEAR, _QUADRILINEAR or _CORNELISSEN
#  J -> Ratio between the strain at the first post-peak point and the
#       ultimate strain of the trilinear/quadrilinear tensile-softening diagram (fracture mode I)
#  K -> Ratio between the stress at the first post-peak point and the
#       tensile strength of the trilinear/quadrilinear tensile-softening diagram (fracture mode I)
#  L -> Ratio between the strain at the second post-peak point and the
#       ultimate strain of the trilinear/quadrilinear tensile-softening diagram (fracture mode I)
#  M -> Ratio between the stress at the second post-peak point and the
#       tensile strength of the trilinear/quadrilinear tensile-softening diagram (fracture mode I)
#  N -> Ratio between the strain at the third post-peak point and the
#       ultimate strain of the quadrilinear tensile-softening diagram (fracture mode I)
#  O -> Ratio between the stress at the third post-peak point and the
#       tensile strength of the quadrilinear tensile-softening diagram (fracture mode I)
#  P -> Mode I fracture energy (Gf)
#  Q -> Parameter to define the mode I fracture energy available to the new crack
#  R -> Type of shear-softening diagram
#       - Available keywords: _NONE, _LINEAR, _TRILINEAR or _CORNELISSEN
#  S -> Ratio between the (shear strain minus peak shear strain) at the first post-peak point and
#       the (ultimate shear strain minus peak shear strain) of the trilinear shear-softening diagram
#  T -> Ratio between the the shear stress at the first post-peak point and the
#       shear strength of the trilinear shear-softening diagram
#  U -> Ratio between the (shear strain minus peak shear strain) at the second post-peak point and
#       the (ultimate shear strain minus peak shear strain) of the trilinear shear-softening diagram
#  V -> Ratio between the the shear stress at the second post-peak point and the
#       shear strength of the trilinear shear-softening diagram
#  W -> Type of shear retention factor law (keyword or constant value)
#       - Available keywords: _LINEAR, _QUADRATIC or _CUBIC
#  X -> Type of crack shear modelling appoach (used in shear retention model)
#       - Available keywords: _INCREMENTAL or _TOTAL
#  Y -> Crack shear strength
#  Z -> Shear fracture energy (Gfs=GfII=GfIII)
# AA -> Type of unloading/reloading diagram
#       - Available keywords: _SECANT or _ELASTIC
# AB -> Crack band width for the fracture mode I (keyword or constant value)
#       - Available keywords: _SQRT_ELEMENT or _SQRT_IP
# AC -> Maximum n. of cracks
# AD -> Threshold angle (degress)
*#
*Set Var INDEX = 1
*#
*loop materials
*#
*#Set Var usedNLMM111=tcl(ExistsNLMM111InLayerProperties *MatProp(0))
*#
*if(strcasecmp(MatProp(1),"NLMM111") == 0 && usedNLMM111 == 0)

#     A     B     C     D     E     F     G
*#
*format  "%7i"
*INDEX  *tcl(ReplaceSpaceWithUnderscore *MatProp(0))*MatProp(2,real)*MatProp(3,real)*MatProp(4,real)*MatProp(5,real)*MatProp(6,real)
*#
#     H     I     J     K     L     M     N     O
*#
*format "%20.8e"
*MatProp(7)  _*MatProp(8)*MatProp(9,real)*MatProp(10,real)*MatProp(11,real)*MatProp(12,real)*MatProp(13,real)*MatProp(14,real)
*#
#     P     Q     R     S     T     U     V
*#
*format "%20.8e"
*if(strcasecmp(MatProp(8),"TRILINEAR") == 0)
*MatProp(15) *\
*elseif(strcasecmp(MatProp(8),"QUADRILINEAR") == 0)
*MatProp(16) *\
*else
*MatProp(17) *\
*endif
*#
*MatProp(18,real)  _*MatProp(19)*MatProp(20,real)*MatProp(21,real)*MatProp(22,real)*MatProp(23,real)
*#
#     W     X     Y     Z
*#
*if(strcasecmp(MatProp(24),"VALUE")==0)
*format "%21.8e"
*MatProp(25,real) *\ 
*else
      _*MatProp(24)  *\
*endif
*#
_*MatProp(26)*MatProp(27,real)*\
*#
*if(strcasecmp(MatProp(19),"NONE")==0)
*MatProp(28,real)
*elseif(strcasecmp(MatProp(19),"LINEAR")==0)
*MatProp(28,real)
*elseif(strcasecmp(MatProp(19),"TRILINEAR")==0)
*MatProp(29,real)
*else
*MatProp(30,real)
*endif
*#
#     AA     AB     AC     AD
*#
      _*MatProp(31)*\
*#
*if(strcasecmp(MatProp(32),"VALUE")==0)
*MatProp(33,real)  *\
*else
  _*MatProp(32)  *\
*endif
*#
*MatProp(34) *MatProp(35) ;
*#
*Set Var INDEX = INDEX + 1
*#
*endif
*#
*end materials
*#

*tcl(WriteNLMM111InLayerProperties *INDEX)
*#
</NLMM111>
*endif
*#
*#
*# END CODE FOR <NLMM111>