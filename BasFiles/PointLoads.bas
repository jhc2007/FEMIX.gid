*# CODE FOR <POINT_LOADS> & <INTERNAL_POINT_LOADS>
*#
*Set Var ExistsInternalPointLoads=tcl(ExistsInternalPointLoads)
*#
*if(ExistsInternalPointLoads == 1)
*#
*loop elems 
*#
*if(ElemsType == 3) *# Quadrilateral
*Set Var TRASH=tcl(InternalPointLoadsInsideQualElement *ElemsNum *ElemsConec(1) *ElemsConec(2) *ElemsConec(3) *ElemsConec(4))
*elseif(ElemsType == 5) *# Hexahedra
*Set Var TRASH=tcl(InternalPointLoadsInsideHexaElement *ElemsNum *ElemsConec(1) *ElemsConec(2) *ElemsConec(3) *ElemsConec(4) *ElemsConec(5) *ElemsConec(6) *ElemsConec(7))
*endif
*#
*end elems
*#
*#Set Var TRASH=tcl(AssignInternalPointLoads)
*#
*endif
*#
*realformat "%18.8e"
*#
*Set Var COUNTPOINTLOADS = 0
*Set Var COUNTINTERNALPOINTLOADS = 0
*#
*Set Cond _Point_Loads *nodes
*#
*loop nodes *OnlyInCond 
*#
*if(Cond(Internal_Point,int) == 0)
*Set Var COUNTPOINTLOADS = COUNTPOINTLOADS + 1
*else
*Set Var COUNTINTERNALPOINTLOADS = COUNTINTERNALPOINTLOADS + 1
*endif
*#
*end nodes
*#
*if(COUNTPOINTLOADS > 0)
*#

<POINT_LOADS>

## Point loads (global coordinate system)
   COUNT = *COUNTPOINTLOADS ; # N. of point loads

## Content of each column:
#  A -> Counter (or counter range)
#  B -> Loaded point (or loaded point range)
#  C -> Force - XG1
#  D -> Force - XG2
#  E -> Force - XG3
#  F -> Moment - XG1
#  G -> Moment - XG2
#  H -> Moment - XG3
#     A     B     C     D     E     F     G     H

*#
*Set Var FIRSTPOINTLOAD = 1
*#
*loop nodes *OnlyInCond
*#
*if(Cond(Internal_Point,int) == 0)
*#
*if(FIRSTPOINTLOAD == 1)
*#
*Set Var FIRSTINDEX = 1
*Set Var LASTINDEX = 1
*Set Var FIRSTNODE = NodesNum
*Set Var LASTNODE = NodesNum
*Set Var XFORCE = Cond(1,real)
*Set Var YFORCE = Cond(2,real)
*Set Var ZFORCE = Cond(3,real)
*Set Var XMOMENT = Cond(4,real)
*Set Var YMOMENT = Cond(5,real)
*Set Var ZMOMENT = Cond(6,real)
*Set Var FIRSTPOINTLOAD = 0
*#
*else *#(FIRSTPOINTLOAD != 1)
*#
*if(XFORCE == Cond(1,real) && YFORCE == Cond(2,real) && ZFORCE == Cond(3,real) && XMOMENT == Cond(4,real) && YMOMENT == Cond(5,real) && ZMOMENT == Cond(6,real) && NodesNum == LASTNODE + 1)
*#
*Set Var LASTNODE = LASTNODE + 1
*Set Var LASTINDEX = LASTINDEX + 1
*#
*else
*#
*if(FIRSTNODE == LASTNODE)
*#
*FIRSTINDEX  *FIRSTNODE*\
*else
[*FIRSTINDEX-*LASTINDEX]  [*FIRSTNODE-*LASTNODE]*\
*endif
*#
*if(numDimension == 2)
*#
*ZFORCE(real)*XFORCE(real)*YFORCE(real)*ZMOMENT(real)*XMOMENT(real)*YMOMENT(real) ;
*#
*else
*#
*XFORCE(real)*YFORCE(real)*ZFORCE(real)*XMOMENT(real)*YMOMENT(real)*ZMOMENT(real) ;
*#
*endif
*#
*Set Var FIRSTNODE = NodesNum
*Set Var LASTNODE = NodesNum
*Set Var XFORCE = Cond(1,real)
*Set Var YFORCE = Cond(2,real)
*Set Var ZFORCE = Cond(3,real)
*Set Var XMOMENT = Cond(4,real)
*Set Var YMOMENT = Cond(5,real)
*Set Var ZMOMENT = Cond(6,real)
*Set Var FIRSTINDEX = LASTINDEX + 1
*Set Var LASTINDEX = FIRSTINDEX
*#
*endif
*#
*endif
*#
*#AQUI
*#
*endif
*#
*end nodes
*#
*#if(loopVar == CondNumEntities)
*#
*if(FIRSTNODE == LASTNODE)
*FIRSTINDEX  *FIRSTNODE*\
*else
[*FIRSTINDEX-*LASTINDEX]  [*FIRSTNODE-*LASTNODE]*\
*endif
*#
*if(numDimension == 2)
*#
*ZFORCE(real)*XFORCE(real)*YFORCE(real)*ZMOMENT(real)*XMOMENT(real)*YMOMENT(real) ;
*#
*else
*#
*XFORCE(real)*YFORCE(real)*ZFORCE(real)*XMOMENT(real)*YMOMENT(real)*ZMOMENT(real) ;
*#
*endif
*#
*#endif

</POINT_LOADS>

*endif
*#
*if(COUNTINTERNALPOINTLOADS > 0)
*#

<INTERNAL_POINT_LOADS>
## Load applied to a point inside an element (global coordinate system)
   COUNT = *COUNTINTERNALPOINTLOADS ; # N. of internal point loads

## Content of each column:
#  A -> Counter (or counter range)
#  B -> Loaded element
#  C -> Auxiliary point with the load (or auxiliary point range)
#  D -> Force - XG1
#  E -> Force - XG2
#  F -> Force - XG3
#  G -> Moment - XG1
#  H -> Moment - XG2
#  I -> Moment - XG3
#     A     B     C     D     E     F     G     H     I

*#
*Set Var FIRSTPOINTLOAD = 1
*#
*loop nodes *OnlyInCond
*#
*if(Cond(Internal_Point,int) == 1)
*#
*if(FIRSTPOINTLOAD == 1)
*#
*Set Var FIRSTINDEX = 1
*Set Var LASTINDEX = 1
*Set Var FIRSTNODE = NodesNum
*Set Var LASTNODE = NodesNum
*Set Var XFORCE = Cond(1,real)
*Set Var YFORCE = Cond(2,real)
*Set Var ZFORCE = Cond(3,real)
*Set Var XMOMENT = Cond(4,real)
*Set Var YMOMENT = Cond(5,real)
*Set Var ZMOMENT = Cond(6,real)
*Set Var ELEMENT = Cond(Element_Number,int)
*Set Var FIRSTPOINTLOAD = 0
*#
*else *#(FIRSTPOINTLOAD != 1)
*#
*if(ELEMENT == Cond(Element_Number,int) && XFORCE == Cond(1,real) && YFORCE == Cond(2,real) && ZFORCE == Cond(3,real) && XMOMENT == Cond(4,real) && YMOMENT == Cond(5,real) && ZMOMENT == Cond(6,real) && NodesNum == LASTNODE + 1)
*#
*Set Var LASTNODE = LASTNODE + 1
*Set Var LASTINDEX = LASTINDEX + 1
*#
*else
*#
*if(FIRSTNODE == LASTNODE)
*#
*FIRSTINDEX  *ELEMENT  *FIRSTNODE*\
*else
[*FIRSTINDEX-*LASTINDEX]  *ELEMENT  [*FIRSTNODE-*LASTNODE]*\
*endif
*#
*if(numDimension == 2)
*#
*ZFORCE(real)*XFORCE(real)*YFORCE(real)*ZMOMENT(real)*XMOMENT(real)*YMOMENT(real) ;
*#
*else
*#
*XFORCE(real)*YFORCE(real)*ZFORCE(real)*XMOMENT(real)*YMOMENT(real)*ZMOMENT(real) ;
*#
*endif
*#
*Set Var FIRSTNODE = NodesNum
*Set Var LASTNODE = NodesNum
*Set Var XFORCE = Cond(1,real)
*Set Var YFORCE = Cond(2,real)
*Set Var ZFORCE = Cond(3,real)
*Set Var XMOMENT = Cond(4,real)
*Set Var YMOMENT = Cond(5,real)
*Set Var ZMOMENT = Cond(6,real)
*Set Var ELEMENT = Cond(Element_Number,int)
*Set Var FIRSTINDEX = LASTINDEX + 1
*Set Var LASTINDEX = FIRSTINDEX
*#
*endif
*#
*endif
*#
*endif
*#
*end nodes
*#
*if(FIRSTNODE == LASTNODE)
*FIRSTINDEX  *ELEMENT  *FIRSTNODE*\
*else
[*FIRSTINDEX-*LASTINDEX]  *ELEMENT  [*FIRSTNODE-*LASTNODE]*\
*endif
*#
*if(numDimension == 2)
*#
*ZFORCE(real)*XFORCE(real)*YFORCE(real)*ZMOMENT(real)*XMOMENT(real)*YMOMENT(real) ;
*#
*else
*#
*XFORCE(real)*YFORCE(real)*ZFORCE(real)*XMOMENT(real)*YMOMENT(real)*ZMOMENT(real) ;
*#
*endif
*#
*#endif

</INTERNAL_POINT_LOADS>
*#
*endif