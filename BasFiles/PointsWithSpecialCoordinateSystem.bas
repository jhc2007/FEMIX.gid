*# CODE FOR <POINTS_WITH_SPECIAL_COORDINATE_SYSTEM>
*#
*#
*Set Cond Points_With_Special_Local_Axes *nodes
*#
*if(CondNumEntities > 0)
*#
<POINTS_WITH_SPECIAL_COORDINATE_SYSTEM>

## Points with special coordinate system
   COUNT = *CondNumEntities ; # N. of points with special coordinate system

## Content of each column:
#  A -> Counter (or counter range)
#  B -> Group name
#  C -> Phase (or phase range)
#  D -> Point number (or point number range)
#  E -> Name of the coordinate system
#     A     B     C     D     E

*#
*loop nodes *OnlyInCond
*#
*if(loopVar == 1) *# if#1
*#
*Set Var INDEX = 1
*Set Var FIRSTNODE = NodesNum
*Set Var LASTNODE = NodesNum
*Set Var NODESLAYERNUMBER = NodesLayerNum
*Set Var LOCALAXESNUMBER=tcl(GetLocalAxesNumber *Cond(1))
*#
*else *# (loopVar != 1)
*#
*Set Var LOCALAXESNUMBERAUX=tcl(GetLocalAxesNumber *Cond(1))
*#
*if(LOCALAXESNUMBERAUX == LOCALAXESNUMBER && NodesNum == operation(LASTNODE+1) && NodesLayerNum == NODESLAYERNUMBER) *# if#2
*#
*Set Var LASTNODE = NodesNum
*#
*else
*#
*if(LASTNODE == FIRSTNODE) *# if#4
*#
*format "%7i"
*INDEX  *\
*#
*loop layers
*if(LayerNum == NODESLAYERNUMBER)
*tcl(ReplaceSpaceWithUnderscore *LayerName)  *\
*break
*endif
*end layers
*#
1  *FIRSTNODE  *tcl(GetLocalAxesName *LOCALAXESNUMBER) ;
*Set Var INDEX = INDEX + 1
*#
*else
*#
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  *\
*#
*loop layers
*if(LayerNum == NODESLAYERNUMBER)
*tcl(ReplaceSpaceWithUnderscore *LayerName)  *\
*break
*endif
*end layers
*#
1  [*FIRSTNODE-*LASTNODE]  *tcl(GetLocalAxesName *LOCALAXESNUMBER) ;
*Set Var INDEX = operation(INDEX+LASTNODE-FIRSTNODE+1)
*#
*endif *# if#4
*#
*Set Var FIRSTNODE = NodesNum
*Set Var LASTNODE = NodesNum
*Set Var NODESLAYERNUMBER = NodesLayerNum
*Set Var LOCALAXESNUMBER=tcl(GetLocalAxesNumber *Cond(1))
*#
*endif *# if#2
*#
*endif *# if#1
*#
*if(loopVar == CondNumEntities)
*#
*if(LASTNODE == FIRSTNODE) *# if#4
*#
*format "%7i"
*INDEX  *\
*#
*loop layers
*if(LayerNum == NODESLAYERNUMBER)
*tcl(ReplaceSpaceWithUnderscore *LayerName)  *\
*break
*endif
*end layers
*#
1  *FIRSTNODE  *tcl(GetLocalAxesName *LOCALAXESNUMBER) ;
*#
*else
*#
  [*INDEX-*operation(INDEX+LASTNODE-FIRSTNODE)]  *\
*#
*loop layers
*if(LayerNum == NODESLAYERNUMBER)
*tcl(ReplaceSpaceWithUnderscore *LayerName)  *\
*break
*endif
*end layers
*#
1  [*FIRSTNODE-*LASTNODE]  *tcl(GetLocalAxesName *LOCALAXESNUMBER) ;
*#
*endif *# if#4
*#
*#
*endif
*#
*end nodes

</POINTS_WITH_SPECIAL_COORDINATE_SYSTEM>

*endif
*#
*#
*# END CODE FOR <POINTS_WITH_SPECIAL_COORDINATE_SYSTEM>