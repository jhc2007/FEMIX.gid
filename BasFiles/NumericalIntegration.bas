*# CODE FOR <NUMERICAL_INTEGRATION>
*#
*Set Var CountLayersAndIntervalsNumInt = 0
*#
*Set Var CountSpringsNumInt=tcl(CountSpringNumericalIntegration)
*Set Var CountSpringsNumIntFromFemixFile=tcl(CountSpringsNumericalIntegrationsFromFemixFile)
*#
*Set Cond Element_Properties *layers
*#
*loop layers *OnlyInCond
*#
*if(Cond(Number_of_Keywords,int) != 0)
*#
*if(Cond(Layered_Configuration,int) == 0))
*Set Var CountLayersAndIntervalsNumInt=tcl(AddNumericalIntegration *LayerNum *Cond(Integration_Name))
*else
*Set Var CountLayersAndIntervalsNumInt=tcl(AddNumericalIntegration *LayerNum *Cond(_Integration_Name))
*endif
*#
*endif
*#
*end layers
*#
*loop intervals
*#
*if(IntvData(New_Numerical_Integration,int) == 1)	   
*Set Var CountLayersAndIntervalsNumInt=tcl(AddNumericalIntegration *operation(-1*loopVar) *IntvData(7))
*endif
*#
*end intervals
*#
*Set Var COUNT = operation(CountLayersAndIntervalsNumInt+CountSpringsNumInt+CountSpringsNumIntFromFemixFile)
*#
*if(COUNT > 0)
*#
<NUMERICAL_INTEGRATION>

## Keywords: _GLEG, _GLOB or _NCOTES
## Numerical integration definition
   COUNT = *COUNT ; # N. of layouts of integration points

## Content of each column:
#  A -> Counter
#  B -> Name of the layout
#  C -> N. of keywords defining the layout
#  D -> Keywords defining the type of term and the n. of points in each direction
#
#  Valid syntax for D is the following:
#     _i_Sj_k
#
#     i: type of terms
#       G -> General use (stiffness matrix, springs and load vector)
#       M -> Membrane terms (stiffness matrix)
#       S -> Shear terms (stiffness matrix)
#       T -> Torsional terms (stiffness matrix)
#       B -> Bending terms (stiffness matrix)
#
#     j: direction
#       1 -> direction s1
#       2 -> direction s2
#       3 -> direction s3
#
#     k: n. of points in the specified direction
#
#     Note: k must be in the range [1,10]
#
#     Examples: _G_S1_5, _M_S2_1, _S_S3_2, _T_S1_7, _B_S2_10
#
#     A  B  C  D

*#Set Var INDEX = 1
*#
*for(j=0; j<CountLayersAndIntervalsNumInt; j=j+1)
*#
*format "%7i"
*operation(j+1)  *tcl(GetNumericalIntegrationName *j)  *\
*#
*Set Var LAYERNUMBER=tcl(GetNumericalIntegrationLayerNumber *j)
*#
*if(LAYERNUMBER > 0)
*#
*Set Cond Element_Properties *layers
*#
*loop layers *OnlyInCond
*#
*if(LAYERNUMBER == LayerNum)
*#
*if(Cond(Layered_Configuration,int) == 0))
*#
*Cond(Number_of_Keywords)  *\
*#
*for(i=0; i<Cond(Number_of_Keywords,int); i=i+1)
*#
*# QUESTION # 63 -> Type_of_Terms 
*#
*if(strcasecmp(Cond(*operation(63+i*3)),"General") == 0)
_G*\
*elseif(strcasecmp(Cond(*operation(63+i*3)),"Membrane") == 0)
_M*\
*elseif(strcasecmp(Cond(*operation(63+i*3)),"Shear") == 0)
_S*\
*elseif(strcasecmp(Cond(*operation(63+i*3)),"Torsional") == 0)
_T*\
*else
_B*\
*endif
*#
*# QUESTION # 64 -> Direction_[1]
*#
*if(strcasecmp(Cond(*operation(64+i*3)),"s1") == 0)
_S1*\
*elseif(strcasecmp(Cond(*operation(64+i*3)),"s2") == 0)
_S2*\
*else
_S3*\
*endif
*#
*# QUESTION # 65 -> Number_of_Points_[1]
*#
_*Cond(*operation(65+i*3))  *\
*#
*end for
*#
;
*#
*else *# Cond(Layered_Configuration,int) == 1
*#
2  _G_S1_2  _G_S2_2  ;
*#
*endif
*#
*break
*#
*endif
*#
*end layers
*#
*elseif(LAYERNUMBER < 0)
*#
*#format "%7i"
*#INDEX  *tcl(GetNumericalIntegrationName *j)  *\
*#Set Var INDEX = INDEX + 1
*#
*loop intervals
*#
*if(IntvData(New_Numerical_Integration,int) == 1 && operation(-1*LAYERNUMBER) == loopVar)
*#
*IntvData(Number_of_Keywords)  *\
*# 
*for(i=0; i<IntvData(Number_of_Keywords,int); i=i+1)
*#
*# QUESTION # 10 -> Type_of_Terms_[1]
*#
*if(strcasecmp(IntvData(*operation(10+i*3)),"General") == 0)
_G*\
*elseif(strcasecmp(IntvData(*operation(10+i*3)),"Membrane") == 0)
_M*\
*elseif(strcasecmp(IntvData(*operation(10+i*3)),"Shear") == 0)
_S*\
*elseif(strcasecmp(IntvData(*operation(10+i*3)),"Torsional") == 0)
_T*\
*else
_B*\
*endif
*#
*# QUESTION # 11 -> Direction_[1]
*#
*if(strcasecmp(IntvData(*operation(11+i*3)),"s1") == 0)
_S1*\
*elseif(strcasecmp(IntvData(*operation(11+i*3)),"s2") == 0)
_S2*\
*else
_S3*\
*endif
*#
*# QUESTION # 12 -> Number_of_Points_[1]
*#
_*IntvData(*operation(12+i*3))  *\
*#
*end for
*#
;
*#
*break
*#
*endif
*#
*end intervals
*#
*endif
*#
*endfor
*#
*if(CountSpringsNumInt > 0)
*tcl(WriteSpringsNumericalIntegration *CountLayersAndIntervalsNumInt)
*endif
*#
*if(CountSpringsNumIntFromFemixFile > 0)
*tcl(WriteSpringsNumericalIntegrationsFromFemixFile *operation(CountLayersAndIntervalsNumInt+CountSpringsNumInt))
*endif
</NUMERICAL_INTEGRATION>

*endif