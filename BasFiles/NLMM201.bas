*# CODE FOR <NLMM201>
*#
*Set Var COUNT=tcl(NumOfNLMM201InLayerProperties)
*#
*loop materials
*#
*Set Var usedNLMM201=tcl(ExistsNLMM201InLayerProperties *MatProp(0))
*#
*if(strcasecmp(MatProp(Material_Type),"NLMM201") == 0 && usedNLMM201 == 0)
*Set Var COUNT = COUNT + 1
*endif
*#
*end materials
*#
*if(COUNT > 0)

<NLMM201>

## Properties of the NLMM201 material model
## Keyword: _NLMM201
   COUNT = *COUNT ; # N. of NLMM201 materials

## Content of each column:
#  A -> Counter
#  B -> Name of the material model
#  C -> Mass per unit volume
#  D -> Temperature coefficient
#  E -> Strain at the end of the first branch
#  F -> Stress at the end of the first branch
#  G -> Strain at the end of the second branch
#  H -> Stress at the end of the second branch
#  I -> Strain at the end of the third branch
#  J -> Stress at the end of the third branch
#  K -> Third branch exponent
*#
*Set Var INDEX = 1
*#
*loop materials
*#
*Set Var usedNLMM201=tcl(ExistsNLMM201InLayerProperties *MatProp(0))
*#
*if(strcasecmp(MatProp(1),"NLMM201") == 0 && usedNLMM201 == 0)

#     A     B     C     D     E     F
*format  "%7i"
*INDEX  *tcl(ReplaceSpaceWithUnderscore *MatProp(0))*MatProp(2,real)*MatProp(3,real)*MatProp(4,real)*MatProp(5,real)
*#
#     G     H     I     J     K
*format "%20.8e"
*MatProp(6)*MatProp(7,real)*MatProp(8,real)*MatProp(9,real)*MatProp(10,real) ;
*#
*Set Var INDEX = INDEX + 1
*endif
*#
*end materials
*#

*tcl(WriteNLMM201InLayerProperties *INDEX)
*#
</NLMM201>
*endif
*#
*#
*# END CODE FOR <NLMM201                       >