*# CODE FOR <NLMM104>
*#
*Set Var COUNT=tcl(NumOfNLMM104InLayerProperties)
*#
*loop materials
*#
*Set Var usedNLMM104=tcl(ExistsNLMM104InLayerProperties *MatProp(0))
*#
*if(strcasecmp(MatProp(Material_Type),"NLMM104") == 0 && usedNLMM104 == 0)
*Set Var COUNT = COUNT + 1
*endif
*#
*end materials
*#
*if(COUNT > 0)

<NLMM104>

## Keyword: _NLMM104
## Properties of the NLMM104 material model
   COUNT = *COUNT ; # N. of NLMM104 materials

## Content of each column:
#  A -> Counter
#  B -> Name of the material model
#  C -> Mass per unit volume
#  D -> Temperature coefficient
#  E -> Poisson's coefficient
#  F -> Young's modulus
#  G -> Compressive strength
#  H -> Tensile strength
#  I -> Type of tensile-softening diagram
#       - Available keywords: _TRILINEAR, _QUADRILINEAR or _CORNELISSEN
#  J -> Ratio between the strain at the first post-peak point and the
#       ultimate strain of the trilinear/quadrilinear tensile-softening diagram (fracture mode I)
#  K -> Ratio between the stress at the first post-peak point and the
#       tensile strength of the trilinear/quadrilinear tensile-softening diagram (fracture mode I)
#  L -> Ratio between the strain at the second post-peak point and the
#       ultimate strain of the trilinear/quadrilinear tensile-softening diagram (fracture mode I)
#  M -> Ratio between the stress at the second post-peak point and the
#       tensile strength of the trilinear/quadrilinear tensile-softening diagram (fracture mode I)
#  N -> Ratio between the strain at the third post-peak point and the
#       ultimate strain of the quadrilinear tensile-softening diagram (fracture mode I)
#  O -> Ratio between the stress at the third post-peak point and the
#       tensile strength of the quadrilinear tensile-softening diagram (fracture mode I)
#  P -> Mode I fracture energy (Gf)
#  Q -> Type of mode I fracture energy update law
#       - Available keywords: _NONE, _LINEAR, _QUADRATIC or _CUBIC
#  R -> Ultimate crack shear sliding (keyword or constant value)
#       - Available keywords: _MAXIMUM_CRACKWIDTH
#  S -> Parameter to define the mode I fracture energy available to the new crack
#  T -> Type of shear-softening diagram
#       - Available keywords: _NONE or _LINEAR
#  U -> Type of shear retention factor law (keyword or constant value)
#       - Available keywords: _LINEAR, _QUADRATIC or _CUBIC
#  V -> Type of crack shear modelling appoach (used in shear retention model)
#       - Available keywords: _INCREMENTAL or _TOTAL
#  W -> Crack shear strength
#  X -> Shear fracture energy (Gfs=GfII) (in nonlinear Mindlin shell: In-plane shear fracture energy)
#  Y -> Type of unloading/reloading diagram
#       - Available keywords: _SECANT or _ELASTIC
#  Z -> Crack band width for the fracture mode I (keyword or constant value)
#       - Available keywords: _SQRT_ELEMENT or _SQRT_IP
# AA -> Maximum n. of cracks
# AB -> Threshold angle (degress)
#
# Out-of-plane shear data:
#    -> Used in nonlinear Mindlin shell
#
# AC -> Type of out-of-plane shear-softening diagram
#       - Available keywords: _NONE, _LINEAR, _TRILINEAR or _CORNELISSEN
# AD -> Minimum out-of-plane shear stress for softening behavior
# AE -> Out-of-plane shear fracture energy
# AF -> Ratio between the out-of-plane shear strain at the first
#       post-peak point and the ultimate out-of-plane shear strain
#       trilinear out-of-plane shear-softening diagram
# AG -> Ratio between the out-of-plane shear stress at the first
#       post-peak point and the out-of-plane shear strength of the
#       trilinear out-of-plane shear-softening diagram
# AH -> Ratio between the out-of-plane shear strain at the second
#       post-peak point and the ultimate out-of-plane shear strain
#       of the trilinear out-of-plane shear-softening diagram
# AI -> Ratio between the out-of-plane shear stress at the second
#       post-peak point and the out-of-plane shear strength of the
#       trilinear out-of-plane shear-softening diagram

*Set Var INDEX = 1
*#
*loop materials
*#
*Set Var usedNLMM104=tcl(ExistsNLMM104InLayerProperties *MatProp(0))
*#
*if(strcasecmp(MatProp(1),"NLMM104") == 0 && usedNLMM104 == 0)

#     A     B     C     D     E     F     G
*format  "%7i"
*INDEX  *tcl(ReplaceSpaceWithUnderscore *MatProp(0))*MatProp(2,real)*MatProp(3,real)*MatProp(4,real)*MatProp(5,real)*MatProp(6,real)
#     H     I     J     K     L     M     N     O
  *MatProp(7,real)  _*MatProp(8)*MatProp(9,real)*MatProp(10,real)*MatProp(11,real)*MatProp(12,real)*MatProp(13,real)*MatProp(14,real)
#     P     Q     R     S     T     U
*#
*if(strcasecmp(MatProp(8),"TRILINEAR") == 0)
  *MatProp(15,real)  *\
*elseif(strcasecmp(MatProp(8),"QUADRILINEAR") == 0)
  *MatProp(16,real)  *\
*else
  *MatProp(17,real)  *\
*endif
*#
_*MatProp(18)  *\
*#
*if(strcasecmp(MatProp(19),"VALUE") == 0)
*MatProp(20,real)*\
*else
_*MatProp(19)*\
*endif
*#
*MatProp(21,real)  _*MatProp(22)  *\
*#
*if(strcasecmp(MatProp(23),"VALUE")==0)
*MatProp(24,real)  
*else
_*MatProp(23)  
*endif
*#
#     V     W     X
      _*MatProp(25)*MatProp(26,real)*MatProp(27,real)
#     Y     Z     AA     AB     AC     AD     AE
      _*MatProp(28)*\
*#
*if(strcasecmp(MatProp(29),"VALUE")==0)
*MatProp(30,real)  *\ 
*else
  _*MatProp(29)  *\ 
*endif
*#
*#format "%7i"
*MatProp(31)  *MatProp(32)  _*MatProp(33)*MatProp(34,real) *\
*#
*if(strcasecmp(MatProp(33),"NONE")==0)
*MatProp(35,real)
*elseif(strcasecmp(MatProp(33),"LINEAR")==0)
*MatProp(36,real)
*elseif(strcasecmp(MatProp(33),"TRILINEAR")==0)
*MatProp(37,real)
*else
*MatProp(38,real)
*endif
*#
#     AF     AG     AH     AI
  *MatProp(39,real)*MatProp(40,real)*MatProp(41,real)*MatProp(42,real) ;

*#
*#
*Set Var INDEX = INDEX + 1
*endif
*#
*end materials
*#
*tcl(WriteNLMM104InLayerProperties *INDEX)

</NLMM104>
*endif
*#
*#
*# END CODE FOR <NLMM104>