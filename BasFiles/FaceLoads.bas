*Set Cond Face_Loads *elems *CanRepeat
*#
*if(CondNumEntities > 0)
*Set Var COUNT=tcl(CreateFaceLoadsList)
*endif
*#
*loop elems *OnlyInCond 
*#
*if(CondElemFace == 0)
*Set Var FACE = 1
*else
*Set Var FACE=tcl(GetFemixFace *CondElemFace)
*endif
*#
*if(strcasecmp(Cond(1),"Global") == 0)
*#
*if(strcasecmp(Cond(9),"All") == 0)
*#
*Set Var COUNT=tcl(AddFaceLoads *ElemsType *ElemsNum *FACE *Cond(2) *Cond(8) 1 *Cond(10))
*#
*elseif(Cond(9,int) == 4)
*#
*Set Var COUNT=tcl(AddFaceLoads *ElemsType *ElemsNum *FACE *Cond(2) *Cond(8) 4 *Cond(11) *Cond(12) *Cond(13) *Cond(14) *Cond(15) *Cond(16) *Cond(17) *Cond(18))
*#
*else
*#
*Set Var COUNT=tcl(AddFaceLoads *ElemsType *ElemsNum *FACE *Cond(2) *Cond(8) 8 *Cond(11) *Cond(12) *Cond(13) *Cond(14) *Cond(15) *Cond(16) *Cond(17) *Cond(18) *Cond(19) *Cond(20) *Cond(21) *Cond(22) *Cond(23) *Cond(24) *Cond(25) *Cond(26))
*#
*endif
*#
*elseif(strcasecmp(Cond(1),"Solid/Shell") == 0)
*#
*if(strcasecmp(Cond(9),"All") == 0)
*#
*Set Var COUNT=tcl(AddFaceLoads *ElemsType *ElemsNum *FACE *Cond(3) *Cond(8) 1 *Cond(10))
*#
*elseif(Cond(9,int) == 4)
*#
*Set Var COUNT=tcl(AddFaceLoads *ElemsType *ElemsNum *FACE *Cond(3) *Cond(8) 4 *Cond(11) *Cond(12) *Cond(13) *Cond(14) *Cond(15) *Cond(16) *Cond(17) *Cond(18))
*#
*else
*#
*Set Var COUNT=tcl(AddFaceLoads *ElemsType *ElemsNum *FACE *Cond(3) *Cond(8) 8 *Cond(11) *Cond(12) *Cond(13) *Cond(14) *Cond(15) *Cond(16) *Cond(17) *Cond(18) *Cond(19) *Cond(20) *Cond(21) *Cond(22) *Cond(23) *Cond(24) *Cond(25) *Cond(26))
*#
*endif
*#
*elseif(strcasecmp(Cond(1),"Plane_Stress/Strain/Axisymmetry") == 0)
*#
*if(strcasecmp(Cond(9),"All") == 0)
*#
*Set Var COUNT=tcl(AddFaceLoads *ElemsType *ElemsNum *FACE *Cond(4) *Cond(8) 1 *Cond(10))
*#
*elseif(Cond(9,int) == 4)
*#
*Set Var COUNT=tcl(AddFaceLoads *ElemsType *ElemsNum *FACE *Cond(4) *Cond(8) 4 *Cond(11) *Cond(12) *Cond(13) *Cond(14) *Cond(15) *Cond(16) *Cond(17) *Cond(18))
*#
*else
*#
*Set Var COUNT=tcl(AddFaceLoads *ElemsType *ElemsNum *FACE *Cond(4) *Cond(8) 8 *Cond(11) *Cond(12) *Cond(13) *Cond(14) *Cond(15) *Cond(16) *Cond(17) *Cond(18) *Cond(19) *Cond(20) *Cond(21) *Cond(22) *Cond(23) *Cond(24) *Cond(25) *Cond(26))
*#
*endif
*#
*else *# Specified ->  args[2] = 0
*#
*if(strcasecmp(Cond(9),"All") == 0)
*#
*Set Var COUNT=tcl(AddFaceLoads *ElemsType *ElemsNum *FACE 0 *Cond(8) 1 *Cond(10) *Cond(5) *Cond(6) *Cond(7))
*#
*elseif(Cond(9,int) == 4)
*#
*Set Var COUNT=tcl(AddFaceLoads *ElemsType *ElemsNum *FACE 0 *Cond(8) 4 *Cond(11) *Cond(12) *Cond(13) *Cond(14) *Cond(15) *Cond(16) *Cond(17) *Cond(18) *Cond(5) *Cond(6) *Cond(7))
*#
*else
*#
*Set Var COUNT=tcl(AddFaceLoads *ElemsType *ElemsNum *FACE 0 *Cond(8) 8 *Cond(11) *Cond(12) *Cond(13) *Cond(14) *Cond(15) *Cond(16) *Cond(17) *Cond(18) *Cond(19) *Cond(20) *Cond(21) *Cond(22) *Cond(23) *Cond(24) *Cond(25) *Cond(26) *Cond(5) *Cond(6) *Cond(7))
*#
*endif
*#
*endif
*#
*end elems
*#
*Set Var COUNT1=tcl(CountFaceLoads)
*Set Var COUNT2=tcl(CountFaceLoadsFromFemixFile *loopVar)
*Set Var COUNT = COUNT1 + COUNT2
*#
*if(COUNT > 0)
*#

<FACE_LOADS>

## Load on the face of an element
   COUNT = *COUNT ; # N. of face loads

## Content of each column:
#  A -> Counter (or counter range)
#  B -> Loaded element (or loaded element range)
#  C -> Loaded face
#  D -> Direction vector:
#       - Global: _XG1, _XG2 or _XG3
#       - Solid/Shell: _L1, _L2 or _N
#       - Plane stress/Plane strain/Axisymmetry: _L1 or _L2
#       - Specified: vector from the <AUXILIARY_VECTORS> block
#  E -> Type of generalized force (_FORC - Force; _MOM - Moment)
#  F -> N. of nodes of the face of the element
#  G -> Load values
#
#  Note: when F is equal to 1 the load is constant
#
#  Direction vector keywords:
#     _L1 - tangential to the face and following the l1 local axis
#     _L2 - tangential to the face and following the l2 local axis
#     _N  - normal to the face
#
#  G -> Load values
#
#     A     B     C     D     E     F     G

*tcl(WriteFaceLoadsList)
*tcl(WriteFaceLoadsFromFemixFile *loopVar *COUNT1)
*#
</FACE_LOADS>
*#
*endif