*# CODE FOR <NLSMM102>
*#
*Set Var COUNT=tcl(CountSpringMaterials NLSMM102)
*#
*if(COUNT > 0)

<NLSMM102>

## Keyword: _NLSMM102
## Properties of the NLSMM102 material model
   COUNT = *COUNT ; # N. of NLSMM102 materials

## Content of each column:
#  A -> Counter
#  B -> Name of the material model
#  C -> Unilateral spring behavior
#       - Available keywords: _TENSION or _COMPRESSION
#  D -> Displacement at the end of the first branch
#  E -> Force at the end of the first branch
#  F -> Displacement at the end of the second branch
#  G -> Force at the end of the second branch
#  H -> Displacement at the end of the third branch
#  I -> Force at the end of the third branch
#  J -> Third branch exponent
#     A     B     C     D     E     F     G     H     I     J 

*tcl(WriteNLSMM102)
*#
</NLSMM102>

*#
*endif