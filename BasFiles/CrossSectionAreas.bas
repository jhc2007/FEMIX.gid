*# CODE FOR <CROSS_SECTION_AREAS>
*#
*Set Var COUNTCROSSSECTIONAREAS = 0
*#
*Set Cond Element_Properties *layers
*#
*loop layers *OnlyInCond
*#
*if(strcasecmp(Cond(Geometry_Type),"CROSS_SECTION_AREAS") == 0 && Cond(Area,real) != 0)
*Set Var COUNTCROSSSECTIONAREAS = COUNTCROSSSECTIONAREAS + 1
*elseif(strcasecmp(Cond(Geometry_Type),"GEOMETRY_PATTERNS") == 0 && strcasecmp(Cond(Geometry_Patterns_Type),"CROSS_SECTION_AREAS") == 0)
*Set Var COUNTCROSSSECTIONAREAS = COUNTCROSSSECTIONAREAS + 2
*endif
*#
*end layers
*#
*#
*if(COUNTCROSSSECTIONAREAS > 0)
*#

<CROSS_SECTION_AREAS>

## Keyword: _CS_AREA
## Cross section areas
   COUNT = *COUNTCROSSSECTIONAREAS ; # N. of cross section areas

## Content of each column:
#  A -> Counter
#  B -> Name of the cross section
#  C -> Area
#      A  B                                         C

*#
*Set Var INDEX = 1
*#
*Set Cond Element_Properties *layers
*#
*loop layers *OnlyInCond
*#
*if(strcasecmp(Cond(Geometry_Type),"CROSS_SECTION_AREAS") == 0 && Cond(Area,real) != 0)
*#
*format "%7i"
*INDEX  CS_AREA_*tcl(ReplaceSpaceWithUnderscore *LayerName)  *Cond(Area,real) ;
*Set Var INDEX = INDEX + 1
*#
*elseif(strcasecmp(Cond(Geometry_Type),"GEOMETRY_PATTERNS") == 0 && strcasecmp(Cond(Geometry_Patterns_Type),"CROSS_SECTION_AREAS") == 0)
*#
*format "%7i"
*INDEX  CS_AREA_1_GEOM_PATT_*tcl(ReplaceSpaceWithUnderscore *LayerName)  *Cond(Area_[1],real) ;
*Set Var INDEX = INDEX + 1
*#
*format "%7i"
*INDEX  CS_AREA_2_GEOM_PATT_*tcl(ReplaceSpaceWithUnderscore *LayerName)  *Cond(Area_[2],real) ;
*Set Var INDEX = INDEX + 1
*#
*endif
*#
*end layers
*#

</CROSS_SECTION_AREAS>

*#
*endif
