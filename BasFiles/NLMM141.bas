*# CODE FOR <NLMM141>
*#
*Set Var COUNT=tcl(NumOfNLMM141InLayerProperties)
*#
*loop materials
*#
*Set Var usedNLMM141=tcl(ExistsNLMM141InLayerProperties *MatProp(0))
*#
*if(strcasecmp(MatProp(Material_Type),"NLMM141") == 0 && usedNLMM141 == 0)
*Set Var COUNT = COUNT + 1
*endif
*#
*end materials
*#
*if(COUNT > 0)

<NLMM141>

## Keyword: _NLMM141
## Properties of the NLMM141 material model
   COUNT = *COUNT ; # N. of NLMM141 materials

## Content of each column:
#  A -> Counter
#  B -> Name of the material model
#  C -> Mass per unit volume
#  D -> Temperature coefficient
#  E -> Poisson's coefficient
#  F -> Young's modulus
#  G -> Compressive strength
#  H -> Strain at compression peak stress
#  I -> Parameter defining the initial yield surface
#  J -> Compression/stiffness degradation parameter due to concrete cracking
#  K -> Tensile strength
#  L -> Type of tensile-softening diagram
#       - Available keywords: _TRILINEAR or _CORNELISSEN
#  M -> Ratio between the strain at the first post-peak point and the
#       ultimate strain of the trilinear tensile-softening diagram (fracture mode I)
#  N -> Ratio between the stress at the first post-peak point and the
#       tensile strength of the trilinear tensile-softening diagram (fracture mode I)
#  O -> Ratio between the strain at the second post-peak point and the
#       ultimate strain of the trilinear tensile-softening diagram (fracture mode I)
#  P -> Ratio between the stress at the second post-peak point and the
#       tensile strength of the trilinear tensile-softening diagram (fracture mode I)
#  Q -> Mode I fracture energy (Gf)
#  R -> Parameter to define the mode I fracture energy available to the new crack
#  S -> Type of shear retention factor law (keyword or constant value)
#       - Available keywords: _LINEAR, _QUADRATIC or _CUBIC
#  T -> Type of tensile unloading/reloading diagram
#       - Available keywords: _SECANT or _ELASTIC
#  U -> Crack band width for the fracture mode I (keyword or constant value)
#       - Available keywords: _SQRT_ELEMENT or _SQRT_IP
#  V -> Maximum n. of cracks
#  X -> Threshold angle (degrees)
*#
*Set Var INDEX = 1
*#
*loop materials
*#
*Set Var usedNLMM141=tcl(ExistsNLMM141InLayerProperties *MatProp(0))
*#
*if(strcasecmp(MatProp(1),"NLMM141") == 0 && usedNLMM141 == 0)

#     A     B     C     D     E     F     G     H
*format  "%7i"
*INDEX  *tcl(ReplaceSpaceWithUnderscore *MatProp(0))*MatProp(2,real)*MatProp(3,real)*MatProp(4,real)*MatProp(5,real)*MatProp(6,real)*MatProp(7,real)
#     I     J     K     L     M     N     O     P
*format "%20.8e"
*MatProp(8)*\
*MatProp(9,real)*MatProp(10,real)  _*MatProp(11)*MatProp(12,real)*MatProp(13,real)*MatProp(14,real)*MatProp(15,real)
#     Q     R     S     T     U     V     X
*#
*format "%20.8e"
*if(strcasecmp(MatProp(11),"TRILINEAR") == 0)
*MatProp(16)*\
*else
*MatProp(17)*\
*endif
*#
*MatProp(18,real)*\
*#
*if(strcasecmp(MatProp(19),"VALUE") == 0)
*MatProp(20,real)  *\
*else
  _*MatProp(19)  *\
*endif
*#
_*MatProp(21)*\
*#
*if(strcasecmp(MatProp(22),"VALUE") == 0)
*MatProp(23,real)  *\
*else
  _*MatProp(22)  *\
*endif
*#
*MatProp(24) *MatProp(25) ;
*#
*Set Var INDEX = INDEX + 1
*endif
*#
*end materials
*#

*tcl(WriteNLMM141InLayerProperties *INDEX)
*#
</NLMM141>

*endif
*#
*#
*# END CODE FOR <NLMM141>