*# CODE FOR <LINEAR_INTERFACE_SURFACE_PROPERTIES>
*#
*Set Var COUNT=tcl(NumOfLinIntSurfaceInLayerProperties)
*#
*loop materials
*#
*Set Var usedLinIntSurface=tcl(ExistsLinIntSurfaceInLayerProperties *MatProp(0))
*#
*if(strcasecmp(MatProp(Material_Type),"LIN_INT_SURF") == 0 && usedLinIntSurface == 0)
*Set Var COUNT = COUNT + 1
*endif
*#
*end materials
*#
*if(COUNT > 0)

<LINEAR_INTERFACE_SURFACE_PROPERTIES>

## Keyword: _LIN_INT_SURF
## Properties of linear interface elements that link a pair of faces (solid/shell only)
   COUNT = *COUNT ; # N. of types of surface interface elements
 
## Content of each column:
#  A -> Counter
#  B -> Name of the type of surface interface element
#  C -> Normal stiffness
#  D -> Tangential stiffness - s1
#  E -> Tangential stiffness - s2
#     A     B     C     D     E

*Set Var INDEX = 1
*#
*loop materials
*#
*Set Var usedLinIntSurface=tcl(ExistsLinIntSurfaceInLayerProperties *MatProp(0))
*#
*if(strcasecmp(MatProp(1),"LIN_INT_SURF") == 0 && usedLinIntSurface == 0)

*format  "%7i"
*INDEX  *tcl(ReplaceSpaceWithUnderscore *MatProp(0)) *MatProp(2,real)*MatProp(3,real)*MatProp(4,real) ;
*#
*Set Var INDEX = INDEX + 1
*endif
*#
*end materials
*#
*tcl(WriteLinIntSurfaceInLayerProperties *INDEX)
*#
</LINEAR_INTERFACE_SURFACE_PROPERTIES>
*endif
*#
*#
*# END CODE FOR </LINEAR_INTERFACE_SURFACE_PROPERTIES>