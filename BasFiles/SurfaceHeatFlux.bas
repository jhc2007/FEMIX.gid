*Set Cond _Heat_Flux *elems
*#
*if(CondNumEntities > 0)
*Set Var COUNT=tcl(CreateSurfaceHeatFluxList)
*endif
*#
*loop elems *OnlyInCond 
*#
*if(CondElemFace == 0)
*Set Var FACE = 1
*else
*Set Var FACE=tcl(GetFemixFace *CondElemFace)
*endif
*#
*if(strcasecmp(Cond(1),"All") == 0)
*#
*Set Var COUNT=tcl(AddSurfaceHeatFlux *ElemsType *ElemsNum *FACE 1 *Cond(2))
*#
*elseif(Cond(1,int) == 4)
*#
*Set Var COUNT=tcl(AddSurfaceHeatFlux *ElemsType *ElemsNum *FACE 4 *Cond(3) *Cond(4) *Cond(5) *Cond(6) *Cond(7) *Cond(8) *Cond(9) *Cond(10))
*#
*else
*#
*Set Var COUNT=tcl(AddSurfaceHeatFlux *ElemsType *ElemsNum *FACE 8 *Cond(3) *Cond(4) *Cond(5) *Cond(6) *Cond(7) *Cond(8) *Cond(9) *Cond(10) *Cond(11) *Cond(12) *Cond(13) *Cond(14) *Cond(15) *Cond(16) *Cond(17) *Cond(18))
*#
*endif
*#
*end elems
*#
*Set Var COUNT1=tcl(CountSurfaceHeatFlux)
*Set Var COUNT2=tcl(CountSurfaceHeatFluxFromFemixFile *loopVar)
*Set Var COUNT = COUNT1 + COUNT2
*#
*if(COUNT > 0)
*#

<SURFACE_HEAT_FLUX>
## Heat flux on the face of an element
   COUNT = *COUNT ; # N. of surface heat fluxes

## Content of each column:
#  A -> Counter (or counter range)
#  B -> Element number (or element number range)
#  C -> Face number
#  D -> N. of nodes of the face of the element
#  E -> Heat flux value(s)
#
#  Note: when D is equal to 1 the heat flux is constant
#
#
#     A     B     C     D     E

*if(COUNT1 > 0)
*tcl(WriteSurfaceHeatFluxList)
*endif
*if(COUNT2 > 0)
*tcl(WriteSurfaceHeatFluxFromFemixFile *loopVar *COUNT1)
*endif
*#
</SURFACE_HEAT_FLUX>
*#
*endif