*# CODE FOR <NLMM151>
*#
*Set Var COUNT=tcl(NumOfNLMM151InLayerProperties)
*#
*loop materials
*#
*Set Var usedNLMM151=tcl(ExistsNLMM151InLayerProperties *MatProp(0))
*#
*if(strcasecmp(MatProp(Material_Type),"NLMM151") == 0 && usedNLMM151 == 0)
*Set Var COUNT = COUNT + 1
*endif
*#
*end materials
*#
*if(COUNT > 0)

<NLMM151>

## Keyword: _NLMM151
## Properties of the NLMM151 material model
   COUNT = *COUNT ; # N. of NLMM151 materials

## Content of each column:
#  A -> Counter
#  B -> Name of the material model
#  C -> Mass per unit volume
#  D -> Temperature coefficient
#  E -> Poisson's coefficient
#  F -> Young's modulus
#  G -> Compressive strength
#  H -> Strain at compressive strength
#  I -> Parameter defining the linear part of the compressive strength
#  J -> Compressive fracture energy
#  K -> Type of hardening function after peak load
#       - Available keywords: _CEB_FIP93 or _PERFECT_PLASTIC
#  L -> Volume fraction of fibers (Vf)
#
#       Note: for plane concrete a Value of zero should be set for this parameter
#
#  M -> Aspect ratio of fibers (lf/Df)
#  N -> Length efficiency factor of fibers
#  O -> Orientation factor of fibers
#  P -> Ultimate bond strength
#  Q -> Tensile strength
#  R -> Type of tensile-softening diagram
#       - Available keywords: _TRILINEAR, _QUADRILINEAR or _CORNELISSEN
#  S -> Ratio between the strain at the first post-peak point and the
#       ultimate strain of the trilinear/quadrilinear tensile-softening diagram (fracture mode I)
#  T -> Ratio between the stress at the first post-peak point and the
#       tensile strength of the trilinear/quadrilinear tensile-softening diagram (fracture mode I)
#  U -> Ratio between the strain at the second post-peak point and the
#       ultimate strain of the trilinear/quadrilinear tensile-softening diagram (fracture mode I)
#  V -> Ratio between the stress at the second post-peak point and the
#       tensile strength of the trilinear/quadrilinear tensile-softening diagram (fracture mode I)
#  W -> Ratio between the strain at the third post-peak point and the
#       ultimate strain of the quadrilinear tensile-softening diagram (fracture mode I)
#  X -> Ratio between the stress at the third post-peak point and the
#       tensile strength of the quadrilinear tensile-softening diagram (fracture mode I)
#  Y -> Mode I fracture energy (Gf)
#  Z -> Parameter to define the mode I fracture energy available to the new crack
# AA -> Type of shear-softening diagram
#       - Available keywords: _NONE, _LINEAR, _TRILINEAR or _CORNELISSEN
# AB -> Ratio between the (shear strain minus peak shear strain) at the first post-peak point and
#       the (ultimate shear strain minus peak shear strain) of the trilinear shear-softening diagram
# AC -> Ratio between the the shear stress at the first post-peak point and the
#       shear strength of the trilinear shear-softening diagram
# AD -> Ratio between the (shear strain minus peak shear strain) at the second post-peak point and
#       the (ultimate shear strain minus peak shear strain) of the trilinear shear-softening diagram
# AE -> Ratio between the the shear stress at the second post-peak point and the
#       shear strength of the trilinear shear-softening diagram
# AF -> Type of shear retention factor law (keyword or constant value)
#       - Available keywords: _LINEAR, _QUADRATIC or _CUBIC
# AG -> Type of crack shear modelling approach (used in shear retention model)
#       - Available keywords: _INCREMENTAL or _TOTAL
# AH -> Crack shear strength
# AI -> Shear fracture energy (Gfs=GfII=GfIII)
# AJ -> Type of unloading/reloading diagram
#       - Available keywords: _SECANT or _ELASTIC
# AK -> Crack band width for the fracture mode I (keyword or constant value)
#       - Available keywords: _SQRT_ELEMENT or _SQRT_IP
# AL -> Maximum n. of cracks
# AM -> Threshold angle (degrees)
*#
*Set Var INDEX = 1
*#
*loop materials
*#
*Set Var usedNLMM151=tcl(ExistsNLMM151InLayerProperties *MatProp(0))
*#
*if(strcasecmp(MatProp(1),"NLMM151") == 0 && usedNLMM151 == 0)

#     A     B     C     D     E     F
*#
*format  "%7i"
*INDEX  *tcl(ReplaceSpaceWithUnderscore *MatProp(0))*MatProp(2,real)*MatProp(3,real)*MatProp(4,real)*MatProp(5,real)
*#
#     G     H     I     J     K
*#
*format "%20.8e"
*MatProp(6,real)*MatProp(7,real)*MatProp(8,real)*MatProp(9,real)  _*MatProp(10)
*#
#     L     M     N     O     P
      0.00    54.55   0.5    0.25     6.5
*#
#     Q     R     S     T     U     V     W     X     Y     Z
*#
*format "%20.8e"	
*MatProp(11,real)  _*MatProp(12)*MatProp(13,real)*MatProp(14,real)*MatProp(15,real)*MatProp(16,real)*MatProp(17,real)*MatProp(18,real) *\
*#
*if(strcasecmp(MatProp(13),"TRILINEAR") == 0)
*MatProp(19) *\
*elseif(strcasecmp(MatProp(13),"QUADRILINEAR") == 0)
*MatProp(20) *\
*else
*MatProp(21) *\
*endif
*#
*MatProp(22,real)  _*MatProp(23)
*#
#     AA     AB     AC	    AD     AE     AF     AG     AH     AI
*#
*format "%20.8e"
*MatProp(24,real)*MatProp(25,real)*MatProp(26,real)*MatProp(27,real) *\
*#
*if(strcasecmp(MatProp(28),"VALUE")==0)
*format "%21.8e"
*MatProp(29,real) *\ 
*else
      _*MatProp(28)  *\
*endif
*#
_*MatProp(30)*MatProp(31,real)*\
*#
*if(strcasecmp(MatProp(23),"NONE")==0)
*MatProp(32,real)
*elseif(strcasecmp(MatProp(23),"LINEAR")==0)
*MatProp(32,real)
*elseif(strcasecmp(MatProp(19),"TRILINEAR")==0)
*MatProp(33,real)
*else
*MatProp(34,real)
*endif
*#
#     AJ     AK     AL     AM
*#
      _*MatProp(35)*\
*#
*if(strcasecmp(MatProp(36),"VALUE")==0)
*MatProp(37,real)  *\
*else
  _*MatProp(36)  *\
*endif
*#
*MatProp(38) *MatProp(39) ;
*#
*Set Var INDEX = INDEX + 1
*#
*endif
*#
*end materials
*#

*tcl(WriteNLMM151InLayerProperties *INDEX)
*#
</NLMM151>
*endif
*#
*#
*# END CODE FOR <NLMM151>