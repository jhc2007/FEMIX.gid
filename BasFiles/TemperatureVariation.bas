*# CODE TO <TEMPERATURE_VARIATION>
*#
*Set Cond Temperature_Variation_for_Lines *elems
*#
*Set Var COUNT = CondNumEntities
*#
*Set Cond Temperature_Variation_for_Surfaces *elems
*#
*Set Var COUNT = operation(COUNT+CondNumEntities)
*#
*Set Cond Temperature_Variation_for_Volumes *elems
*#
*Set Var COUNT = operation(COUNT+CondNumEntities)
*#
*if(COUNT > 0)
*#
<TEMPERATURE_VARIATION>

## Elements with temperature variation
   COUNT = *COUNT ; # N. of elements with temperature variation

## Content of each column:
#  A -> Counter (or counter range)
#  B -> Loaded element (or loaded element range)
#  C -> N. of nodes of the element
#  D -> Temperature variation values
#
#  Note: when C is equal to 1 the temperature variation is the same for all the element nodes
#
#  A     B     C     D

*include .\TemperatureVariationForLines.bas
*include .\TemperatureVariationForSurfaces.bas
*include .\TemperatureVariationForVolumes.bas

</TEMPERATURE_VARIATION>

*#
*endif