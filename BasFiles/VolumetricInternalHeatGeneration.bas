*# CODE FOR <VOLUMETRIC_INTERNAL_HEAT_GENERATION>
*#
*Set Cond _Internal_Heat_Generation *elems
*#
*if(CondNumEntities > 0)
*#
<VOLUMETRIC_INTERNAL_HEAT_GENERATION>
## Internal heat generation per unit volume
   COUNT = *CondNumEntities ; # N. of volumetric internal heat generations

## Content of each column:
#  A -> Counter (or counter range)
#  B -> Element number (or element number range)
#  C -> Heat generation value
#     A     B     C

*Set Var INDEX = 1
*#
*loop elems *OnlyInCond
*#
*if(loopVar == 1)
*#
*Set Var FIRSTELEM = ElemsNum
*Set Var LASTELEM = ElemsNum
*Set Var HEAT = Cond(1,real)
*#
*else *#// loopVar != 1
*#
*if(HEAT == Cond(1,real) && ElemsNum == LASTELEM + 1)
*#
*Set Var LASTELEM = ElemsNum
*#
*else
*#
*if(FIRSTELEM == LASTELEM)
*format "%7i"
*INDEX  *FIRSTELEM  *\
*Set Var INDEX = INDEX + 1
*else
  [*INDEX-*operation(INDEX+LASTELEM-FIRSTELEM)]  [*FIRSTELEM-*LASTELEM]  *\
*Set Var INDEX = INDEX + LASTELEM - FIRSTELEM + 1
*endif
*#
*HEAT ;
*#
*Set Var FIRSTELEM = NodesNum
*Set Var LASTELEM = NodesNum
*Set Var HEAT = Cond(1,real)
*#
*endif
*#
*endif
*#
*end elems
*#
*if(FIRSTELEM == LASTELEM)
*format "%7i"
*INDEX  *FIRSTELEM  *\
*else
  [*INDEX-*operation(INDEX+LASTELEM-FIRSTELEM)]  [*FIRSTELEM-*LASTELEM]  *\
*endif
*#
*HEAT ;

</VOLUMETRIC_INTERNAL_HEAT_GENERATION>

*endif