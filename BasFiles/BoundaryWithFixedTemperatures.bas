*# CODE FOR <BOUNDARY_WITH_FIXED_TEMPERATURES>
*#
*Set Var COUNT=tcl(CountBoundaryPointsWithFixedTemperatures)
*#
*if(COUNT > 0)
*#

<BOUNDARY_WITH_FIXED_TEMPERATURES>
## Boundary points with fixed temperatures
   COUNT = *COUNT ; # N. of boundary points with fixed temperature

## Content of each column:
#  A -> Counter (or counter range)
#  B -> Group name
#  C -> Phase (or phase range)
#  D -> Point number (or point number range)
#     A     B     C     D

*tcl(WriteBoundaryPointsWithFixedTemperaturesList)
*#
</BOUNDARY_WITH_FIXED_TEMPERATURES>

*#
*endif