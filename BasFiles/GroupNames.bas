*# CODE FOR <GROUP_NAMES>
*#
*#

## Content of each column:
#  A -> Counter
#  B -> Name
#     A  B

*Set Var INDEX = 1
*loop layers
*format  "%7i"
*INDEX  *tcl(ReplaceSpaceWithUnderscore *LayerName) ;
*Set Var INDEX = INDEX + 1
*end layers
*#
*loop groups
*format  "%7i"
*INDEX  *tcl(ReplaceSpaceWithUnderscore *GroupName) ;
*Set Var INDEX = INDEX + 1
*end groups

</GROUP_NAMES>

*#
*# END CODE FOR <GROUP_NAMES>