*# CODE FOR <THERMAL_ISOTROPIC_MATERIALS>
*#
*Set Var COUNT = 0
*#
*loop materials
*#
*if(strcasecmp(MatProp(Material_Type),"THERMAL_ISO") == 0)
*Set Var COUNT = COUNT + 1
*endif
*#
*end materials
*#
*if(COUNT > 0)

<THERMAL_ISOTROPIC_MATERIALS>
## Keyword: _THERMAL_ISO
## Properties of the isotropic materials for thermal analysis
   COUNT = *COUNT ; # N. of linear thermal isotropic materials

## Content of each column:
#  A -> Counter
#  B -> Material name
#  C -> Mass per unit volume
#  D -> Thermal conductivity
#  E -> Specific heat
#     A     B     C     D     E

*Set Var INDEX = 1
*#
*loop materials
*#
*if(strcasecmp(MatProp(1),"THERMAL_ISO") == 0)
*format  "%7i"
*INDEX  *tcl(ReplaceSpaceWithUnderscore *MatProp(0))  *MatProp(2,real)*MatProp(3,real)*MatProp(4,real) ;
*Set Var INDEX = INDEX + 1
*endif
*#
*end materials

</THERMAL_ISOTROPIC_MATERIALS>

*endif