*# CODE TO WRITE <LINE_SPRINGS>
*#
*#Set Cond _Line_Springs *nodes
*#
*#if(CondNumEntities > 0)
*#
*Set Var COUNT=tcl(CountLineSprings)
*Set Var CountFromFemixFile=tcl(CountLineSpringsFromFemixFile)
*#
*if(operation(COUNT+CountFromFemixFile) > 0)
*#
<LINE_SPRINGS>

## Lines with springs
   COUNT = *operation(COUNT+CountFromFemixFile) ; # N. of line springs

## Content of each column:
#  A -> Counter (or counter range)
#  B -> Group name
#  C -> Phase (or phase range)
#  D -> Element number (or element number range)
#  E -> Face number
#  F -> Edge number
#  G -> Direction vector:
#       - Global: _XG1, _XG2 or _XG3
#       - Timoshenko beam: _L1, _L2 or _L3
#       - Solid/Shell: _T, _NN or _NT
#       - Plane stress/Plane strain/Axisymmetry: _T or _NT
#       - Specified: vector from the <AUXILIARY_VECTORS> block
#  H -> Spring location:
#       - Direction vector from the structure (S) to the ground (G): _SG
#       - Direction vector from the ground (G) to the structure (S): _GS
#         Example:
#           Direction vector: -----> _XG1
#                                            G     H
#           Structure - Spring - Ground  =>  _XG1  _SG
#           Ground - Spring - Structure  =>  _XG1  _GS
#  I -> Dof type (_TRANS - Translational; _ROT - Rotational)
#  J -> Integration type (spring stiffness matrix)
#  K -> Integration name (spring stiffness matrix)
#  L -> Spring material type
#  M -> N. of nodes of the edge of the element
#  N -> Spring material name(s)
#
#  Note: when M is equal to 1 the spring is constant
#
#  Direction vector keywords:
#     _L1 - tangential to the bar
#     _L2 - normal to the bar and following axis l2
#     _L3 - normal to the bar and following axis l3
#     _T  - tangential to the edge
#     _NN - normal to the edge and normal to the element face
#     _NT - normal to the edge and tangential to the element face
#
#     A     B     C     D     E     F     G     H     I     J     K     L     M     N

*tcl(WriteLineSpringsList)
*tcl(WriteLineSpringsFromFemixFile *COUNT)
*#
</LINE_SPRINGS>

*endif
*#endif
