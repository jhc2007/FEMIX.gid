*# CODE FOR <LINEAR_ISOTROPIC_MATERIALS>
*#
*#
*Set Var COUNT=tcl(NumOfLinIsoMaterialsInLayerProperties)
*#
*loop materials
*#
*Set Var usedLinIsoMat=tcl(ExistsLinIsoMaterialInLayerProperties *MatProp(0))
*#
*if(strcasecmp(MatProp(Material_Type),"LIN_ISO") == 0 && usedLinIsoMat == 0)
*Set Var COUNT = COUNT + 1
*endif
*#
*end materials
*#
*if(COUNT > 0)

*#realformat "%18.8e"
<LINEAR_ISOTROPIC_MATERIALS>

## Keyword: _LIN_ISO
## Properties of the linear isotropic materials
   COUNT = *COUNT ; # N. of linear isotropic materials

## Content of each column:
#  A -> Counter
#  B -> Material name
#  C -> Mass per unit volume
#  D -> Temperature coefficient
#  E -> Young's modulus
#  F -> Poisson's coefficient
#     A              B              C              D              E              F

*Set Var INDEX = 1
*#
*loop materials
*#
*Set Var usedLinIsoMat=tcl(ExistsLinIsoMaterialInLayerProperties *MatProp(0))
*#
*if(strcasecmp(MatProp(1),"LIN_ISO") == 0 && usedLinIsoMat == 0)
*format  "%7i"
*INDEX  *tcl(ReplaceSpaceWithUnderscore *MatProp(0))  *MatProp(2,real)*MatProp(3,real)*MatProp(4,real)*MatProp(5,real) ;
*Set Var INDEX = INDEX + 1
*endif
*#
*end materials
*#
*tcl(WriteLinIsoMaterialsInLayerProperties *INDEX)
*#
</LINEAR_ISOTROPIC_MATERIALS>

*endif
*#
*# END CODE FOR <LINEAR_ISOTROPIC_MATERIALS>