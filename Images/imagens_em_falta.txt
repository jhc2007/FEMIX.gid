Loads (directorio LoadImages)

Edge Loads Global (EdgeLoadsGlobal.png)
Edge Loads Frame (EdgeLoadsFrame.png)
Edge Loads Solid/Shell (EdgeLoadsSolid.png)
Edge Loads Plane Stress/Strain/Axisymmetry (EdgeLoadsPlane.png)
Edge Loads Specified (EdgeLoadsSpecified.png)

Face Loads Global (FaceLoadsGlobal.png)
Face Loads Solid/Shell (FaceLoadsSolid.png)
Face Loads Plane Stress/Strain/Axisymmetry (FaceLoadsPlane.png)
Face Loads Specified (FaceLoadsSpecified.png)

Springs (directorio SpringImages):

Point Springs Global (PointSpringsGlobal.png)
Point Springs Specified (PointSpringsSpecified.png)

Line Springs Global (LineSpringsGlobal.png)
Line Springs Frame (LineSpringsFrame.png)
Line Springs Solid/Shell (LineSpringsSolid.png)
Line Springs Plane Stress/Strain/Axisymmetry (LineSpringsPlane.png)
Line Springs Specified (LineSpringsSpecified.png)

Surface Springs Global (SurfaceSpringsGlobal.png)
Surface Springs Solid/Shell (SurfaceSpringsSolid.png)
Surface Springs Specified (SurfaceSpringsSpecified.png)

Materials (directorio MaterialImages)

NLMM305 (NLMM305.png)
Linear Spring (LinSpring.png)
Thermal Isotropic (ThermalIso.png)
NLMM401 (NLMM401.png)

