# TemperatureVariationFromFemixFile.tcl

proc AssignTemperatureVariation { temperatureVariationData conditionTypeList } {

	# global PROBLEMTYPEPATH 
	# set result [open "$PROBLEMTYPEPATH\\result_new.dat" "w"]

	if { [llength $conditionTypeList] == 0 } {
		return
	}

	set index 5

	while { $index < [llength $temperatureVariationData] } {
		
		set rangeList [GetFirstLastValuesFromRange [lindex $temperatureVariationData $index]]
		set firstElem [lindex $rangeList 0]
		set lastElem [lindex $rangeList 1]

		set index [expr $index + 1]
		set numNodes [lindex $temperatureVariationData $index]
		set temperatures [list 0 0 0 0]

		for {set i 0} {$i < $numNodes} {incr i} {
			set index [expr $index + 1]
			set temperatures [lreplace $temperatures $i $i [lindex $temperatureVariationData $index]]
		}

		set index [expr $index + 3]

		for {set i $firstElem} {$i <= $lastElem} {incr i} {
			# set condition [GetConditionType $conditionTypeList $i]
			GiD_Process Mescape Data Conditions AssignCond \
			Temperature_Variation_for_[GetConditionType $conditionTypeList $i] \
			Change \
	        $numNodes \
	        [lindex $temperatures 0] \
	        [lindex $temperatures 1] \
	        [lindex $temperatures 2] \
	        [lindex $temperatures 3] \
	        $i \
	        Mescape

	        # puts $result $i
	        # puts $result $condition
		}
	}
	# close $result
}

proc GetConditionType { conditionTypeList element } {

	for {set i 0} {$i < [llength $conditionTypeList]} {incr i} {
		if { $element >= [lindex [lindex $conditionTypeList $i] 0] && $element <= [lindex [lindex $conditionTypeList $i] 1] } {
			return [lindex [lindex $conditionTypeList $i] 2]
		}
	}
}