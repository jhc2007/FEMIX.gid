# StressStrainPostMshFile.tcl
#
# proc CreateStressStrainPostMshFile { basename }

proc CreateStressStrainPostMshFile { basename } {

    global PROJECTPATH numDimension

    set ums3dFileName $basename
    append ums3dFileName "_um.s3d"

    if { [file exists "$PROJECTPATH\\$ums3dFileName"] } {
        
        set stressStrainPostMshName $basename
        append stressStrainPostMshName "_stress_strain.post.msh"
        
        set ums3dFile [open "$PROJECTPATH\\$ums3dFileName" "r"]
        set ums3dData [read $ums3dFile]
        close $ums3dFile
        
        set index 0
        while { ![string is integer [lindex $ums3dData $index] ] } {
            set index [expr $index + 1]
        }
        set numElems [lindex $ums3dData $index]
        set numNodes [lindex $ums3dData [expr $index + 1]]
        set index [expr $index + 3]
        #set index 6
        #set index [expr $index + 6]
        set numNodesElem 0
        set numElemsType 0
        #################################
        #puts $result $numElems
        #puts $result $numNodes
        #################################
        # $numElems
        for {set i 0} {$i < $numElems} {incr i} {
            #puts $result $i
            #set indexData [lindex $ums3dData [expr $ind + 1]]
            #set indexData [lindex $ums3dData $ind]
            #if { [lindex $ums3dData [expr $index + 1]] == 5 || [lindex $ums3dData [expr $index + 1]] == 9 } {
            #    set tmpNumNodesElem [expr [lindex $ums3dData [expr $index + 1]] - 1]
            #} elseif {  } {
            set tmpNumNodesElem [lindex $ums3dData [expr $index + 1]]
            #} 
            if { $tmpNumNodesElem != 0 && $tmpNumNodesElem != $numNodesElem } {
                set numNodesElem $tmpNumNodesElem
                set numElemsType [expr $numElemsType + 1]
                if { $numNodesElem == 2 || $numNodesElem == 3 } {
                    #set elemType "Linear"
                    set elemsData($numElemsType) [list "Linear" $numNodesElem]
                } elseif { $numNodesElem == 5 || $numNodesElem == 9 } {
                    #set elemType "Quadrilateral"
                    set elemsData($numElemsType) [list "Quadrilateral" [expr $numNodesElem - 1]]
                    #set elemsData($numElemsType) [list Quadrilateral $numNodesElem]
                } else {
                    #set elemType "Hexahedra"
                    set elemsData($numElemsType) [list "Hexahedra" $numNodesElem]
                }
                #lappend elemsData($numElemsType) $numNodesElem 
                #set elemsData($numElemsType) [list $elemType $numNodesElem]
            }
            if { $tmpNumNodesElem != 0 } { 
                set data [list [lindex $ums3dData $index]]

                switch $numNodesElem {

                    3 {
                        lappend data [lindex $ums3dData [expr $index + 2]]
                        lappend data [lindex $ums3dData [expr $index + 4]]
                        lappend data [lindex $ums3dData [expr $index + 3]]
                    } 
                    5 {
                        for {set j 0} { $j < [expr $numNodesElem - 1] } {incr j} {
                            lappend data [lindex $ums3dData [expr $index + 2 + $j]]
                        }
                    } 
                    9 {
                        for {set j 2} { $j <= 8 } {incr j 2} {
                            lappend data [lindex $ums3dData [expr $index + $j]]
                        }
                        #lappend data [lindex $ums3dData [expr $index + 2]]
                        #lappend data [lindex $ums3dData [expr $index + 4]]
                        #lappend data [lindex $ums3dData [expr $index + 6]]
                        #lappend data [lindex $ums3dData [expr $index + 8]]
                        for {set j 3} { $j <= 9 } {incr j 2} {
                            lappend data [lindex $ums3dData [expr $index + $j]]
                        }
                        #lappend data [lindex $ums3dData [expr $index + 3]]
                        #lappend data [lindex $ums3dData [expr $index + 5]]
                        #lappend data [lindex $ums3dData [expr $index + 7]]
                        #lappend data [lindex $ums3dData [expr $index + 9]]
                    } 
                    20 { 
                        for {set j 2} { $j <= 8 } {incr j 2} {
                            lappend data [lindex $ums3dData [expr $index + $j]]
                        }
                        #lappend data [lindex $ums3dData [expr $index + 2]]
                        #lappend data [lindex $ums3dData [expr $index + 4]]
                        #lappend data [lindex $ums3dData [expr $index + 6]]
                        #lappend data [lindex $ums3dData [expr $index + 8]]
                        for {set j 14} { $j <= 20 } {incr j 2} {
                            lappend data [lindex $ums3dData [expr $index + $j]]
                        }
                        #lappend data [lindex $ums3dData [expr $index + 14]]
                        #lappend data [lindex $ums3dData [expr $index + 16]]
                        #lappend data [lindex $ums3dData [expr $index + 18]]
                        #lappend data [lindex $ums3dData [expr $index + 20]]
                        for {set j 3} { $j <= 9 } {incr j 2} {
                            lappend data [lindex $ums3dData [expr $index + $j]]
                        }
                        #lappend data [lindex $ums3dData [expr $index + 5]]
                        #lappend data [lindex $ums3dData [expr $index + 7]]
                        #lappend data [lindex $ums3dData [expr $index + 9]]
                        #lappend data [lindex $ums3dData [expr $index + 3]]
                        for {set j 10} { $j <= 13 } {incr j} {
                            lappend data [lindex $ums3dData [expr $index + $j]]
                        }
                        #lappend data [lindex $ums3dData [expr $index + 10]]
                        #lappend data [lindex $ums3dData [expr $index + 11]]
                        #lappend data [lindex $ums3dData [expr $index + 12]]
                        #lappend data [lindex $ums3dData [expr $index + 13]]
                        for {set j 15} { $j <= 21 } {incr j 2} {
                            lappend data [lindex $ums3dData [expr $index + $j]]
                        }
                        #lappend data [lindex $ums3dData [expr $index + 15]]
                        #lappend data [lindex $ums3dData [expr $index + 17]]
                        #lappend data [lindex $ums3dData [expr $index + 19]]
                        #lappend data [lindex $ums3dData [expr $index + 21]]
                    }
                    default {
                        for {set j 0} {$j < $numNodesElem} {incr j} {
                            lappend data [lindex $ums3dData [expr $index + 2 + $j]]
                        }
                    }
                }
                lappend elemsData($numElemsType) $data
            }
            #if { $numNodesElem == 5 || $numNodesElem == 9 } {
            #set index [expr $index + $numNodesElem + 3]
            #} elseif {  } {
            set index [expr $index + $numNodesElem + 2]
            #}
        }
        set stressStrainPostMshFile [open "$PROJECTPATH\\$stressStrainPostMshName" "w"]
        
        #for {set i 1} {$i <= $numElemsType} {incr i} {
        #    set dataSize [llength $elemsData($i)]
        #    for {set j 0} {$j < $dataSize} {incr j} {
        #        puts $postGidMsh [lindex $elemsData($i) $j]
        #    }
        #}
        set nodesData {}
        if { $numDimension == 2 } {
            for {set i 0} {$i < $numNodes} {incr i} {
                lappend nodesData [list [lindex $ums3dData $index] \
                                        [lindex $ums3dData [expr $index + 2]] \
                                        [lindex $ums3dData [expr $index + 3]] \
                                        [lindex $ums3dData [expr $index + 1]]]
                set index [expr $index + 4]                        
            }
        } else {
            # $numDimension == 3
            for {set i 0} {$i < $numNodes} {incr i} {
                lappend nodesData [list [lindex $ums3dData $index] \
                                        [lindex $ums3dData [expr $index + 1]] \
                                        [lindex $ums3dData [expr $index + 2]] \
                                        [lindex $ums3dData [expr $index + 3]]]
                set index [expr $index + 4]                        
            }
        }
        #for {set i 0} {$i < $numNodes} {incr i} {
        #    puts $postGidMsh [lindex $nodesData $i]
        #}

        puts $stressStrainPostMshFile "MESH dimension 3 ElemType [lindex $elemsData(1) 0] Nnode [lindex $elemsData(1) 1]"
        puts $stressStrainPostMshFile "\nCoordinates\n"
        for {set i 0} {$i < $numNodes} {incr i} {
            puts $stressStrainPostMshFile [lindex $nodesData $i]
        }
        puts $stressStrainPostMshFile "\nEnd Coordinates\n"
        for {set i 1} {$i <= $numElemsType} {incr i} {
            if {$i > 1} {
                puts $stressStrainPostMshFile "MESH dimension 3 ElemType [lindex $elemsData($i) 0] Nnode [lindex $elemsData($i) 1]"
                puts $stressStrainPostMshFile "\nCoordinates"
                puts $stressStrainPostMshFile "End Coordinates\n"
            }
            puts $stressStrainPostMshFile "Elements\n"
            set dataSize [llength $elemsData($i)]
            for {set j 2} {$j < $dataSize} {incr j} {
                puts $stressStrainPostMshFile [lindex $elemsData($i) $j]
            }      
            puts $stressStrainPostMshFile "\nEnd Elements\n"

        }
        #puts $postGidMsh [lindex $ums3dData $index]
        close $stressStrainPostMshFile
    }
}