# PrescribedPointTemperaturesFromFemixFile.tcl

proc AssignPrescribedPointTemperatures { loadCaseData } {

	set prescribedPointTemperaturesData [GetBlockData $loadCaseData "PRESCRIBED_POINT_TEMPERATURES"]
		
	set index 5

	# if { [llength $prescribedPointTemperaturesData] > 0 } {

		# set prescribedDisplacementsValuesList {}

		# global PROBLEMTYPEPATH 
		# set result [open "$PROBLEMTYPEPATH\\result_new.dat" "w"]

	while { $index < [llength $prescribedPointTemperaturesData] } {

		set rangeList [GetFirstLastValuesFromRange [lindex $prescribedPointTemperaturesData $index]]

		set firstPoint [lindex $rangeList 0] 
		set lastPoint [lindex $rangeList 1]

		incr index 

		GiD_Process Mescape Data Conditions AssignCond _Prescribed_Temperatures Change \
			[lindex $prescribedPointTemperaturesData $index] $firstPoint:$lastPoint \
    	Mescape

		incr index 3
	}
	# }
}