# VolumetricInternalHeatGenerationFromFemixFile.tcl

proc AssignVolumetricInternalHeatGeneration { loadCaseData } {

	set volIntHeatGenData [GetBlockData $loadCaseData "VOLUMETRIC_INTERNAL_HEAT_GENERATION"]
	
	set index 5

	while { $index < [llength $volIntHeatGenData] } {

		set rangeList [GetFirstLastValuesFromRange [lindex $volIntHeatGenData $index]]

		set firstElem [lindex $rangeList 0] 
		set lastElem [lindex $rangeList 1]

		incr index 

		GiD_Process Mescape Data Conditions AssignCond _Internal_Heat_Generation Change \
			[lindex $volIntHeatGenData $index] $firstElem:$lastElem \
    	Mescape

		incr index 3
	}
}