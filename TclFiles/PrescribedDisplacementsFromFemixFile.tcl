# PrescribedDisplacementsFromFemixFile.tcl
#
# AssignPrescribedDisplacements { loadCaseData }
# Set3DPrescribedDisplacementsValuesList { valuesList point keyword value }
# Set2DPrescribedDisplacementsValuesList { valuesList point keyword value }
# GetPrescribedDisplacementsValuesIndex { valuesList point }

proc AssignPrescribedDisplacements { loadCaseData } {

	global numDimension angleUnit
	# lengthUnit 

	set prescribedDisplacementsData [GetBlockData $loadCaseData "PRESCRIBED_DISPLACEMENTS"]

	if { [llength $prescribedDisplacementsData] > 0 } {

		set index 5
		set prescribedDisplacementsValuesList {}

		# global PROBLEMTYPEPATH 
		# set result [open "$PROBLEMTYPEPATH\\result_new.dat" "w"]

		if { $numDimension == 3 } {

			while { $index < [llength $prescribedDisplacementsData] } {

				set rangeList [GetFirstLastValuesFromRange [lindex $prescribedDisplacementsData $index]]

				set firstPoint [lindex $rangeList 0] 
				set lastPoint [lindex $rangeList 1]

				for {set i $firstPoint} {$i <= $lastPoint} {incr i} {
					set prescribedDisplacementsValuesList [Set3DPrescribedDisplacementsValuesList $prescribedDisplacementsValuesList $i [lindex $prescribedDisplacementsData [expr $index + 1]] [lindex $prescribedDisplacementsData [expr $index + 2]]]			
				}

				set index [expr $index + 5]
			}

		} else {
			# $numDimension == 2
			while { $index < [llength $prescribedDisplacementsData] } {

				set rangeList [GetFirstLastValuesFromRange [lindex $prescribedDisplacementsData $index]]

				set firstPoint [lindex $rangeList 0] 
				set lastPoint [lindex $rangeList 1]

				for {set i $firstPoint} {$i <= $lastPoint} {incr i} {
					set prescribedDisplacementsValuesList [Set2DPrescribedDisplacementsValuesList $prescribedDisplacementsValuesList $i [lindex $prescribedDisplacementsData [expr $index + 1]] [lindex $prescribedDisplacementsData [expr $index + 2]]]			
				}

				set index [expr $index + 5]
			}
		}

		for {set i 0} {$i < [llength $prescribedDisplacementsValuesList]} {incr i} {
			# GiD_Process Mescape Data Conditions AssignCond _Prescribed_Displacements Change \
			# 	[lindex [lindex $prescribedDisplacementsValuesList $i] 0]$lengthUnit \
			# 	[lindex [lindex $prescribedDisplacementsValuesList $i] 1]$lengthUnit \
			# 	[lindex [lindex $prescribedDisplacementsValuesList $i] 2]$lengthUnit \
			# 	[lindex [lindex $prescribedDisplacementsValuesList $i] 3]$angleUnit \
			# 	[lindex [lindex $prescribedDisplacementsValuesList $i] 4]$angleUnit \
			# 	[lindex [lindex $prescribedDisplacementsValuesList $i] 5]$angleUnit \
			# 	[lindex [lindex $prescribedDisplacementsValuesList $i] 6] \
            #  Mescape
   			GiD_Process Mescape Data Conditions AssignCond _Prescribed_Displacements Change \
				[lindex [lindex $prescribedDisplacementsValuesList $i] 0] \
				[lindex [lindex $prescribedDisplacementsValuesList $i] 1] \
				[lindex [lindex $prescribedDisplacementsValuesList $i] 2] \
				[lindex [lindex $prescribedDisplacementsValuesList $i] 3] \
				[lindex [lindex $prescribedDisplacementsValuesList $i] 4] \
				[lindex [lindex $prescribedDisplacementsValuesList $i] 5] \
				[lindex [lindex $prescribedDisplacementsValuesList $i] 6] \
        	Mescape
		}

		# puts $result $prescribedDisplacementsValuesList
		# close $result
	}
}

proc Set3DPrescribedDisplacementsValuesList { valuesList point keyword value } {

	set index [GetPrescribedDisplacementsValuesIndex $valuesList $point]

	if { $index == [llength $valuesList] } {
		#set values { 0 0 0 0 0 0 }
		#lappend values $point
		set values [list 0 0 0 0 0 0 $point]
		lappend valuesList $values
	} else {
		set values [lindex $valuesList $index]
	}

	switch $keyword {
		"_D1" {
			set values [lreplace $values 0 0 $value]
		}
		"_D2" {
			set values [lreplace $values 1 1 $value]
		}
		"_D3" {
			set values [lreplace $values 2 2 $value]
		}
		"_R1" {
			set values [lreplace $values 3 3 $value]
		}
		"_R2" {
			set values [lreplace $values 4 4 $value]
		}
		"_R3" {
			set values [lreplace $values 5 5 $value]
		}
	}

	set valuesList [lreplace $valuesList $index $index $values]

	# puts $result $point
	# puts $result $keyword
	# puts $result $value

	return $valuesList
}

proc Set2DPrescribedDisplacementsValuesList { valuesList point keyword value } {

	set index [GetPrescribedDisplacementsValuesIndex $valuesList $point]

	if { $index == [llength $valuesList] } {
		#set values { 0 0 0 0 0 0 }
		#lappend values $point
		set values [list 0 0 0 0 0 0 $point]
		lappend valuesList $values
	} else {
		set values [lindex $valuesList $index]
	}

	switch $keyword {

		"_D2" {
			set values [lreplace $values 0 0 $value]
		}
		"_D3" {
			set values [lreplace $values 1 1 $value]
		}
		"_D1" {
			set values [lreplace $values 2 2 $value]
		}
		"_R2" {
			set values [lreplace $values 3 3 $value]
		}
		"_R3" {
			set values [lreplace $values 4 4 $value]
		}
		"_R1" {
			set values [lreplace $values 5 5 $value]
		}
	}

	set valuesList [lreplace $valuesList $index $index $values]

	# puts $result $point
	# puts $result $keyword
	# puts $result $value

	return $valuesList
}

proc GetPrescribedDisplacementsValuesIndex { valuesList point } {

	for {set i 0} {$i < [llength $valuesList]} {incr i} {
		if { [lindex [lindex $valuesList $i] 6] == $point } {
			return $i
		}
	}
	return [llength $valuesList]
}