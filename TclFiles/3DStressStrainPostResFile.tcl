# 3DStressStrainPostResFile.tcl
#
# proc Create3DStressStrainPostResFile { basename }
# PutsLayeredElementStressStrainPostResFile { }
# proc PutsSolidHexaStressStrainPostResFile { }
# proc WriteStressStrainVectorData { type file1 file2 file3 }
# proc WriteStressData { firstSigmaPath secondSigmaPath tauPath }
# proc WriteMiddleStressData { firstTauPath secondTauPath }
# proc WriteStrainData { firstEpsPath secondEpsPath gammaPath }
# proc WriteMiddleStrainData { firstGammaPath secondGammaPath }

proc Create3DStressStrainPostResFile { basename } {

    global PROJECTPATH numCombinations
    global stressStrainPostResFile

    set elementType { "MindlinShellQuad" "SolidHexa" "AhmadShellQuad" }
    set pvaFiles [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *.pva]]
    set usedElementType {}

    for {set i 0} {$i < [llength $pvaFiles]} {incr i} {
        set j 0 
        while { $j < [llength $elementType] } {
            if { [string first [lindex $elementType $j] [lindex $pvaFiles $i]] >= 0 } {
                if { [lindex $usedElementType [expr [llength $usedElementType] - 1]] != [lindex $elementType $j] } {
                    lappend usedElementType [lindex $elementType $j]
                }
                set j [llength $elementType]
            } else {
                set j [expr $j + 1]
            }
        }
    }   

    set fileCreated no

    foreach elementType $usedElementType {

        if { $elementType == "MindlinShellQuad" || $elementType == "AhmadShellQuad" } {        

            foreach surface { "bottom" "top" "middle" } {
            
                switch $surface {

                    "bottom" {
                        set sigma1files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Sigma1_Bot_se.pva]]
                        set sigma2files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Sigma2_Bot_se.pva]]
                        set tau12files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH  *_Tau12_Bot_se.pva]]
                
                        set eps1files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH    *_Eps1_Bot_sa.pva]]
                        set eps2files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH    *_Eps2_Bot_sa.pva]]
                        set gamma12files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Gamma12_Bot_sa.pva]]

                        set stressLength [llength $sigma1files]
                        set strainLength [llength $eps1files]
                    }
                    "top" {
                        set sigma1files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Sigma1_Top_se.pva]]
                        set sigma2files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Sigma2_Top_se.pva]]
                        set tau12files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH  *_Tau12_Top_se.pva]]
                
                        set eps1files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH    *_Eps1_Top_sa.pva]]
                        set eps2files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH    *_Eps2_Top_sa.pva]]
                        set gamma12files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Gamma12_Top_sa.pva]]

                        set stressLength [llength $sigma1files]
                        set strainLength [llength $eps1files]
                    }
                    "middle" {
                        set tau23files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH  *_Tau23_Mid_se.pva]]
                        set tau31files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH  *_Tau31_Mid_se.pva]]

                        set gamma23files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Gamma23_Mid_sa.pva]]
                        set gamma31files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Gamma31_Mid_sa.pva]]

                        set stressLength [llength $tau23files]
                        set strainLength [llength $gamma23files]
                    }
                }

                if { $stressLength > 0 || $strainLength > 0 } {

                    if { !$fileCreated } {
                        set stressStrainPostResFileName $basename
                        append stressStrainPostResFileName "_stress_strain.post.res"
                        set stressStrainPostResFile [open "$PROJECTPATH\\$stressStrainPostResFileName" "w"]
                        puts $stressStrainPostResFile "GiD Post Results File 1.0\n"
                        set fileCreated yes
                    }
                    
                    set stressIndex 0
                    set strainIndex 0

                    while { $stressIndex < $stressLength && $strainIndex < $strainLength } {

                        if { $numCombinations > 0 } { 
                            if { $surface == "middle" } {
                                set stressCombNum [string range [lindex $tau23files $stressIndex] [expr [string last "_Comb" [lindex $tau23files $stressIndex]] + 5] [expr [string last "_Tau23" [lindex $tau23files $stressIndex]] - 1]]  
                                set strainCombNum [string range [lindex $gamma23files $strainIndex] [expr [string last "_Comb" [lindex $gamma23files $strainIndex]] + 5] [expr [string last "_Gamma23" [lindex $gamma23files $strainIndex]] - 1]]  
                            } else {
                                set stressCombNum [string range [lindex $sigma1files $stressIndex] [expr [string last "_Comb" [lindex $sigma1files $stressIndex]] + 5] [expr [string last "_Sigma1" [lindex $sigma1files $stressIndex]] - 1]]  
                                set strainCombNum [string range [lindex $eps1files $strainIndex] [expr [string last "_Comb" [lindex $eps1files $strainIndex]] + 5] [expr [string last "_Eps1" [lindex $eps1files $strainIndex]] - 1]]  
                            }
                        } else {
                            if { $surface == "middle" } {
                                set stressCombNum [string range [lindex $tau23files $stressIndex] [expr [string last "_LCase" [lindex $tau23files $stressIndex]] + 6] [expr [string last "_Tau23" [lindex $tau23files $stressIndex]] - 1]]  
                                set strainCombNum [string range [lindex $gamma23files $strainIndex] [expr [string last "_LCase" [lindex $gamma23files $strainIndex]] + 6] [expr [string last "_Gamma23" [lindex $gamma23files $strainIndex]] - 1]]      
                            } else {
                                set stressCombNum [string range [lindex $sigma1files $stressIndex] [expr [string last "_LCase" [lindex $sigma1files $stressIndex]] + 6] [expr [string last "_Sigma1" [lindex $sigma1files $stressIndex]] - 1]]  
                                set strainCombNum [string range [lindex $eps1files $strainIndex] [expr [string last "_LCase" [lindex $eps1files $strainIndex]] + 6] [expr [string last "_Eps1" [lindex $eps1files $strainIndex]] - 1]]      
                            }
                        }
                        if { $stressCombNum < $strainCombNum } {
                            if { $numCombinations > 0 } { 
                                puts $stressStrainPostResFile "Result \"Stress ($surface)\" \"Combinations\" $stressCombNum Vector OnNodes\n"
                            } else {
                                puts $stressStrainPostResFile "Result \"Stress ($surface)\" \"Load Cases\" $stressCombNum Vector OnNodes\n"
                            }
                            if { $surface == "middle" } {
                                WriteMiddleStressData [lindex $tau23files $stressIndex] [lindex $tau31files $stressIndex]    
                            } else {
                                WriteStressData [lindex $sigma1files $stressIndex] [lindex $sigma2files $stressIndex] [lindex $tau12files $stressIndex]
                            }
                            set stressIndex [expr $stressIndex + 1]
                        } elseif { $strainCombNum < $stressCombNum } {
                            if { $numCombinations > 0 } { 
                                puts $stressStrainPostResFile "Result \"Strain ($surface)\" \"Combinations\" $strainCombNum Vector OnNodes\n"
                            } else {
                                puts $stressStrainPostResFile "Result \"Strain ($surface)\" \"Load Cases\" $strainCombNum Vector OnNodes\n"
                            }
                            if { $surface == "middle" } {
                                WriteMiddleStrainData [lindex $gamma23files $strainIndex] [lindex $gamma31files $strainIndex]    
                            } else {
                                WriteStrainData [lindex $eps1files $strainIndex] [lindex $eps2files $strainIndex] [lindex $gamma12files $strainIndex]
                            }
                            set strainIndex [expr $strainIndex + 1]
                        } else {
                            if { $numCombinations > 0 } { 
                                if { $surface == "middle" } {
                                    puts $stressStrainPostResFile "Result \"Stress ($surface)\" \"Combinations\" $stressCombNum Vector OnNodes\n"
                                    WriteMiddleStressData [lindex $tau23files $stressIndex] [lindex $tau31files $stressIndex]
                                    puts $stressStrainPostResFile "Result \"Strain ($surface)\" \"Combinations\" $strainCombNum Vector OnNodes\n"
                                    WriteMiddleStrainData [lindex $gamma23files $strainIndex] [lindex $gamma31files $strainIndex] 
                                } else {
                                    puts $stressStrainPostResFile "Result \"Stress ($surface)\" \"Combinations\" $stressCombNum Vector OnNodes\n"
                                    WriteStressData [lindex $sigma1files $stressIndex] [lindex $sigma2files $stressIndex] [lindex $tau12files $stressIndex]
                                    puts $stressStrainPostResFile "Result \"Strain ($surface)\" \"Combinations\" $strainCombNum Vector OnNodes\n"
                                    WriteStrainData [lindex $eps1files $strainIndex] [lindex $eps2files $strainIndex] [lindex $gamma12files $strainIndex]
                                }
                            } else {
                                if { $surface == "middle" } {
                                    puts $stressStrainPostResFile "Result \"Stress ($surface)\" \"Load Cases\" $stressCombNum Vector OnNodes\n"
                                    WriteMiddleStressData [lindex $tau23files $stressIndex] [lindex $tau31files $stressIndex]
                                    puts $stressStrainPostResFile "Result \"Strain ($surface)\" \"Load Cases\" $strainCombNum Vector OnNodes\n"
                                    WriteMiddleStrainData [lindex $gamma23files $strainIndex] [lindex $gamma31files $strainIndex]
                                } else {
                                    puts $stressStrainPostResFile "Result \"Stress ($surface)\" \"Load Cases\" $stressCombNum Vector OnNodes\n"
                                    WriteStressData [lindex $sigma1files $stressIndex] [lindex $sigma2files $stressIndex] [lindex $tau12files $stressIndex]
                                    puts $stressStrainPostResFile "Result \"Strain ($surface)\" \"Load Cases\" $strainCombNum Vector OnNodes\n"
                                    WriteStrainData [lindex $eps1files $strainIndex] [lindex $eps2files $strainIndex] [lindex $gamma12files $strainIndex]
                                }
                            }
                            set stressIndex [expr $stressIndex + 1]
                            set strainIndex [expr $strainIndex + 1]
                        }
                    }    
                    
                    if { $numCombinations > 0 } { 
                        if { $surface == "middle" } {
                            for { set i $stressIndex } { $i < $stressLength } { incr i } {
                                set stressCombNum [string range [lindex $tau23files $stressIndex] [expr [string last "_Comb" [lindex $tau23files $stressIndex]] + 5] [expr [string last "_Tau23" [lindex $tau23files $stressIndex]] - 1]]  
                                puts $stressStrainPostResFile "Result \"Stress ($surface)\" \"Combinations\" $stressCombNum Vector OnNodes\n"
                                WriteMiddleStressData [lindex $tau23files $i] [lindex $tau31files $i]
                            }
                            for { set i $strainIndex } { $i < $strainLength } { incr i } {
                                set strainCombNum [string range [lindex $gamma23files $strainIndex] [expr [string last "_Comb" [lindex $gamma23files $strainIndex]] + 5] [expr [string last "_Gamma23" [lindex $gamma23files $strainIndex]] - 1]]  
                                puts $stressStrainPostResFile "Result \"Strain ($surface)\" \"Load Analysis\" $strainCombNum Vector OnNodes\n"            
                                WriteMiddleStrainData [lindex $gamma23files $i] [lindex $gamma31files $i]
                            }
                        } else {
                            for { set i $stressIndex } { $i < $stressLength } { incr i } {
                                set stressCombNum [string range [lindex $sigma1files $i] [expr [string last "_Comb" [lindex $sigma1files $i]] + 5] [expr [string last "_Sigma1" [lindex $sigma1files $i]] - 1]]  
                                puts $stressStrainPostResFile "Result \"Stress ($surface)\" \"Combinations\" $stressCombNum Vector OnNodes\n"
                                WriteStressData [lindex $sigma1files $i] [lindex $sigma2files $i] [lindex $tau12files $i]
                            }
                            for { set i $strainIndex } { $i < $strainLength } { incr i } {
                                set strainCombNum [string range [lindex $eps1files $i] [expr [string last "_Comb" [lindex $eps1files $i]] + 5] [expr [string last "_Eps1" [lindex $eps1files $i]] - 1]]  
                                puts $stressStrainPostResFile "Result \"Strain ($surface)\" \"Load Analysis\" $strainCombNum Vector OnNodes\n"            
                                WriteStrainData [lindex $eps1files $i] [lindex $eps2files $i] [lindex $gamma12files $i]
                            }
                        }
                    } else {
                        if { $surface == "middle" } { 
                            for { set i $stressIndex } { $i < $stressLength } { incr i } {
                                set stressCombNum [string range [lindex $tau23files $i] [expr [string last "_LCase" [lindex $tau23files $i]] + 6] [expr [string last "_Tau23" [lindex $tau23files $i]] - 1]]  
                                puts $stressStrainPostResFile "Result \"Stress ($surface)\" \"Load Cases\" $stressCombNum Vector OnNodes\n"
                                WriteMiddleStressData [lindex $tau23files $i] [lindex $tau31files $i] 
                            }
                            for { set i $strainIndex } { $i < $strainLength } { incr i } {
                                set strainCombNum [string range [lindex $gamma23files $i] [expr [string last "_LCase" [lindex $gamma23files $i]] + 6] [expr [string last "_Gamma23" [lindex $gamma23files $i]] - 1]]  
                                puts $stressStrainPostResFile "Result \"Strain ($surface)\" \"Load Cases\" $strainCombNum Vector OnNodes\n"            
                                WriteMiddleStrainData [lindex $gamma23files $i] [lindex $gamma31files $i]
                            }
                        } else {
                            for { set i $stressIndex } { $i < $stressLength } { incr i } {
                                set stressCombNum [string range [lindex $sigma1files $i] [expr [string last "_LCase" [lindex $sigma1files $i]] + 6] [expr [string last "_Sigma1" [lindex $sigma1files $i]] - 1]]  
                                puts $stressStrainPostResFile "Result \"Stress ($surface)\" \"Load Cases\" $stressCombNum Vector OnNodes\n"
                                WriteStressData [lindex $sigma1files $i] [lindex $sigma2files $i] [lindex $tau12files $i]
                            }
                            for { set i $strainIndex } { $i < $strainLength } { incr i } {
                                set strainCombNum [string range [lindex $eps1files $i] [expr [string last "_LCase" [lindex $eps1files $i]] + 6] [expr [string last "_Eps1" [lindex $eps1files $i]] - 1]]  
                                puts $stressStrainPostResFile "Result \"Strain ($surface)\" \"Load Cases\" $strainCombNum Vector OnNodes\n"            
                                WriteStrainData [lindex $eps1files $i] [lindex $eps2files $i] [lindex $gamma12files $i]
                            }
                        }
                    }
                }
            }
            if { !$fileCreated } {
                set stressStrainPostResFileName $basename
                append stressStrainPostResFileName "_stress_strain.post.res"
                set stressStrainPostResFile [open "$PROJECTPATH\\$stressStrainPostResFileName" "w"]
                puts $stressStrainPostResFile "GiD Post Results File 1.0\n"
                set fileCreated yes
            }
            if { [file exists "$PROJECTPATH\\femix_layer_patterns.dat"] } {
                PutsLayeredElementStressStrainPostResFile
            }
        } elseif { $elementType == "SolidHexa" } {

            if { !$fileCreated } {
                set stressStrainPostResFileName $basename
                append stressStrainPostResFileName "_stress_strain.post.res"
                set stressStrainPostResFile [open "$PROJECTPATH\\$stressStrainPostResFileName" "w"]
                puts $stressStrainPostResFile "GiD Post Results File 1.0\n"
                set fileCreated yes
            }
            PutsSolidHexaStressStrainPostResFile
        }
    }
    if { $fileCreated } {
        close $stressStrainPostResFile
    }
}

proc PutsLayeredElementStressStrainPostResFile { } {

    global PROJECTPATH numCombinations
    global stressStrainPostResFile

    set sigma1files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Sigma1_se.pva]]
    set sigma2files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Sigma2_se.pva]]
    #set sigma3files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Sigma3_se.pva]]

    set tau12files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Tau12_se.pva]]
    set tau23files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Tau23_se.pva]]
    set tau31files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Tau31_se.pva]]

    set eps1files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Eps1_sa.pva]]
    set eps2files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Eps2_sa.pva]]
    #set eps3files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Eps3_sa.pva]]

    set gamma12files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Gamma12_sa.pva]]
    set gamma23files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Gamma23_sa.pva]]
    set gamma31files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Gamma31_sa.pva]]

    set stressLength [llength $sigma1files]
    set strainLength [llength $eps1files]

    #set result [open "$PROJECTPATH\\result.dat" "w"]
    #puts $result $stressLength
    #puts $result $strainLength
    #close $result

    if { $stressLength > 0 || $strainLength > 0 } {
       
        set stressIndex 0
        set strainIndex 0

        if { $numCombinations > 0 } { 
            # Combinations
            while { $stressIndex < $stressLength && $strainIndex < $strainLength } { 
            
                set stressCombNum [string range [lindex $sigma1files $stressIndex] [expr [string last "_Comb" [lindex $sigma1files $stressIndex]] + 5] [expr [string last "_Sigma1" [lindex $sigma1files $stressIndex]] - 1]]  
                set strainCombNum [string range [lindex $eps1files $strainIndex] [expr [string last "_Comb" [lindex $eps1files $strainIndex]] + 5] [expr [string last "_Eps1" [lindex $eps1files $strainIndex]] - 1]]  
            
                if { $stressCombNum < $strainCombNum } {
                    puts $stressStrainPostResFile "Result \"Stress\" \"Combinations\" $stressCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "sigma" [lindex $sigma1files $stressIndex] [lindex $sigma2files $stressIndex] ""
                    puts $stressStrainPostResFile "Result \"Shear Stress\" \"Combinations\" $stressCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "tau" [lindex $tau12files $stressIndex] [lindex $tau23files $stressIndex] [lindex $tau31files $stressIndex]
                    set stressIndex [expr $stressIndex + 1]
                } elseif { $strainCombNum < $stressCombNum } {
                    puts $stressStrainPostResFile "Result \"Strain\" \"Combinations\" $strainCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "eps" [lindex $eps1files $strainIndex] [lindex $eps2files $strainIndex] ""
                    puts $stressStrainPostResFile "Result \"Shear Strain\" \"Combinations\" $strainCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "gamma" [lindex $gamma12files $strainIndex] [lindex $gamma23files $strainIndex] [lindex $gamma31files $strainIndex]
                    set strainIndex [expr $strainIndex + 1]           
                } else {
                    puts $stressStrainPostResFile "Result \"Stress\" \"Combinations\" $stressCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "sigma" [lindex $sigma1files $stressIndex] [lindex $sigma2files $stressIndex] ""
                    puts $stressStrainPostResFile "Result \"Shear Stress\" \"Combinations\" $stressCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "tau" [lindex $tau12files $stressIndex] [lindex $tau23files $stressIndex] [lindex $tau31files $stressIndex]
                    puts $stressStrainPostResFile "Result \"Strain\" \"Combinations\" $strainCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "eps" [lindex $eps1files $strainIndex] [lindex $eps2files $strainIndex] ""
                    puts $stressStrainPostResFile "Result \"Shear Strain\" \"Combinations\" $strainCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "gamma" [lindex $gamma12files $strainIndex] [lindex $gamma23files $strainIndex] [lindex $gamma31files $strainIndex]
                    set stressIndex [expr $stressIndex + 1]
                    set strainIndex [expr $strainIndex + 1]
                }
            }
            for { set i $stressIndex } { $i < $stressLength } { incr i } {
                set stressCombNum [string range [lindex $sigma1files $i] [expr [string last "_Comb" [lindex $sigma1files $i]] + 5] [expr [string last "_Sigma1" [lindex $sigma1files $i]] - 1]]  
                puts $stressStrainPostResFile "Result \"Stress\" \"Combinations\" $stressCombNum Vector OnNodes\n"
                WriteStressStrainVectorData "sigma" [lindex $sigma1files $i] [lindex $sigma2files $i] ""
                puts $stressStrainPostResFile "Result \"Shear Stress\" \"Combinations\" $stressCombNum Vector OnNodes\n"
                WriteStressStrainVectorData "tau" [lindex $tau12files $i] [lindex $tau23files $i] [lindex $tau31files $i]
            }
            for { set i $strainIndex } { $i < $strainLength } { incr i } {
                set strainCombNum [string range [lindex $eps1files $i] [expr [string last "_Comb" [lindex $eps1files $i]] + 5] [expr [string last "_Eps1" [lindex $eps1files $i]] - 1]]  
                puts $stressStrainPostResFile "Result \"Strain\" \"Combinations\" $strainCombNum Vector OnNodes\n"
                WriteStressStrainVectorData "eps" [lindex $eps1files $i] [lindex $eps2files $i] ""
                puts $stressStrainPostResFile "Result \"Shear Strain\" \"Combinations\" $strainCombNum Vector OnNodes\n"
                WriteStressStrainVectorData "gamma" [lindex $gamma12files $i] [lindex $gamma23files $i] [lindex $gamma31files $i]
            }
        } else {
            #Load Cases
            while { $stressIndex < $stressLength && $strainIndex < $strainLength } { 

                set stressCombNum [string range [lindex $sigma1files $stressIndex] [expr [string last "_LCase" [lindex $sigma1files $stressIndex]] + 6] [expr [string last "_Sigma1" [lindex $sigma1files $stressIndex]] - 1]]  
                set strainCombNum [string range [lindex $eps1files $strainIndex] [expr [string last "_LCase" [lindex $eps1files $strainIndex]] + 6] [expr [string last "_Eps1" [lindex $eps1files $strainIndex]] - 1]]      

                if { $stressCombNum < $strainCombNum } {
                    puts $stressStrainPostResFile "Result \"Stress\" \"Load Cases\" $stressCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "sigma" [lindex $sigma1files $stressIndex] [lindex $sigma2files $stressIndex] ""
                    puts $stressStrainPostResFile "Result \"Shear Stress\" \"Load Cases\" $stressCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "tau" [lindex $tau12files $stressIndex] [lindex $tau23files $stressIndex] [lindex $tau31files $stressIndex]
                    set stressIndex [expr $stressIndex + 1]
                } elseif { $strainCombNum < $stressCombNum } {
                    puts $stressStrainPostResFile "Result \"Strain\" \"Load Cases\" $strainCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "eps" [lindex $eps1files $stressIndex] [lindex $eps2files $stressIndex] ""
                    puts $stressStrainPostResFile "Result \"Shear Strain\" \"Load Cases\" $strainCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "gamma" [lindex $gamma12files $stressIndex] [lindex $gamma23files $stressIndex] [lindex $gamma31files $stressIndex]
                    set strainIndex [expr $strainIndex + 1]
                } else {
                    puts $stressStrainPostResFile "Result \"Stress\" \"Load Cases\" $stressCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "sigma" [lindex $sigma1files $stressIndex] [lindex $sigma2files $stressIndex] ""
                    puts $stressStrainPostResFile "Result \"Shear Stress\" \"Load Cases\" $stressCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "tau" [lindex $tau12files $stressIndex] [lindex $tau23files $stressIndex] [lindex $tau31files $stressIndex]
                    puts $stressStrainPostResFile "Result \"Strain\" \"Load Cases\" $strainCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "eps" [lindex $eps1files $strainIndex] [lindex $eps2files $strainIndex] ""
                    puts $stressStrainPostResFile "Result \"Shear Strain\" \"Load Cases\" $strainCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "gamma" [lindex $gamma12files $strainIndex] [lindex $gamma23files $strainIndex] [lindex $gamma31files $strainIndex]
                    set stressIndex [expr $stressIndex + 1]
                    set strainIndex [expr $strainIndex + 1]   
                }
            }

            for { set i $stressIndex } { $i < $stressLength } { incr i } {
                set stressCombNum [string range [lindex $sigma1files $i] [expr [string last "_LCase" [lindex $sigma1files $i]] + 6] [expr [string last "_Sigma1" [lindex $sigma1files $i]] - 1]]  
                puts $stressStrainPostResFile "Result \"Stress\" \"Load Cases\" $stressCombNum Vector OnNodes\n"
                WriteStressStrainVectorData "sigma" [lindex $sigma1files $i] [lindex $sigma2files $i] ""
                puts $stressStrainPostResFile "Result \"Shear Stress\" \"Load Cases\" $stressCombNum Vector OnNodes\n"
                WriteStressStrainVectorData "tau" [lindex $tau12files $i] [lindex $tau23files $i] [lindex $tau31files $i]
            }
            for { set i $strainIndex } { $i < $strainLength } { incr i } {
                set strainCombNum [string range [lindex $eps1files $i] [expr [string last "_LCase" [lindex $eps1files $i]] + 6] [expr [string last "_Eps1" [lindex $eps1files $i]] - 1]]  
                puts $stressStrainPostResFile "Result \"Strain\" \"Load Cases\" $strainCombNum Vector OnNodes\n"
                WriteStressStrainVectorData "eps" [lindex $eps1files $i] [lindex $eps2files $i] ""
                puts $stressStrainPostResFile "Result \"Shear Strain\" \"Load Cases\" $strainCombNum Vector OnNodes\n"
                WriteStressStrainVectorData "gamma" [lindex $gamma12files $i] [lindex $gamma23files $i] [lindex $gamma31files $i]
            }
        }
    }   
}

proc PutsSolidHexaStressStrainPostResFile { } {

    global PROJECTPATH numCombinations
    global stressStrainPostResFile

    set sigma1files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Sigma1_se.pva]]
    set sigma2files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Sigma2_se.pva]]
    set sigma3files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Sigma3_se.pva]]

    set tau12files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Tau12_se.pva]]
    set tau23files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Tau23_se.pva]]
    set tau31files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Tau31_se.pva]]

    set eps1files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Eps1_sa.pva]]
    set eps2files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Eps2_sa.pva]]
    set eps3files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Eps3_sa.pva]]

    set gamma12files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Gamma12_sa.pva]]
    set gamma23files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Gamma23_sa.pva]]
    set gamma31files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Gamma31_sa.pva]]

    set stressLength [llength $sigma1files]
    set strainLength [llength $eps1files]

    #set result [open "$PROJECTPATH\\result.dat" "w"]
    #puts $result $stressLength
    #puts $result $strainLength
    #close $result

    if { $stressLength > 0 || $strainLength > 0 } {
       
        set stressIndex 0
        set strainIndex 0

        if { $numCombinations > 0 } { 
            # Combinations
            while { $stressIndex < $stressLength && $strainIndex < $strainLength } { 
            
                set stressCombNum [string range [lindex $sigma1files $stressIndex] [expr [string last "_Comb" [lindex $sigma1files $stressIndex]] + 5] [expr [string last "_Sigma1" [lindex $sigma1files $stressIndex]] - 1]]  
                set strainCombNum [string range [lindex $eps1files $strainIndex] [expr [string last "_Comb" [lindex $eps1files $strainIndex]] + 5] [expr [string last "_Eps1" [lindex $eps1files $strainIndex]] - 1]]  
            
                if { $stressCombNum < $strainCombNum } {
                    puts $stressStrainPostResFile "Result \"Stress\" \"Combinations\" $stressCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "sigma" [lindex $sigma1files $stressIndex] [lindex $sigma2files $stressIndex] [lindex $sigma3files $stressIndex]
                    puts $stressStrainPostResFile "Result \"Shear Stress\" \"Combinations\" $stressCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "tau" [lindex $tau12files $stressIndex] [lindex $tau23files $stressIndex] [lindex $tau31files $stressIndex]
                    set stressIndex [expr $stressIndex + 1]
                } elseif { $strainCombNum < $stressCombNum } {
                    puts $stressStrainPostResFile "Result \"Strain\" \"Combinations\" $strainCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "eps" [lindex $eps1files $strainIndex] [lindex $eps2files $strainIndex] [lindex $eps3files $strainIndex]
                    puts $stressStrainPostResFile "Result \"Shear Strain\" \"Combinations\" $strainCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "gamma" [lindex $gamma12files $strainIndex] [lindex $gamma23files $strainIndex] [lindex $gamma31files $strainIndex]
                    set strainIndex [expr $strainIndex + 1]           
                } else {
                    puts $stressStrainPostResFile "Result \"Stress\" \"Combinations\" $stressCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "sigma" [lindex $sigma1files $stressIndex] [lindex $sigma2files $stressIndex] [lindex $sigma3files $stressIndex]
                    puts $stressStrainPostResFile "Result \"Shear Stress\" \"Combinations\" $stressCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "tau" [lindex $tau12files $stressIndex] [lindex $tau23files $stressIndex] [lindex $tau31files $stressIndex]
                    puts $stressStrainPostResFile "Result \"Strain\" \"Combinations\" $strainCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "eps" [lindex $eps1files $strainIndex] [lindex $eps2files $strainIndex] [lindex $eps3files $strainIndex]
                    puts $stressStrainPostResFile "Result \"Shear Strain\" \"Combinations\" $strainCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "gamma" [lindex $gamma12files $strainIndex] [lindex $gamma23files $strainIndex] [lindex $gamma31files $strainIndex]
                    set stressIndex [expr $stressIndex + 1]
                    set strainIndex [expr $strainIndex + 1]
                }
            }
            for { set i $stressIndex } { $i < $stressLength } { incr i } {
                set stressCombNum [string range [lindex $sigma1files $i] [expr [string last "_Comb" [lindex $sigma1files $i]] + 5] [expr [string last "_Sigma1" [lindex $sigma1files $i]] - 1]]  
                puts $stressStrainPostResFile "Result \"Stress\" \"Combinations\" $stressCombNum Vector OnNodes\n"
                WriteStressStrainVectorData "sigma" [lindex $sigma1files $i] [lindex $sigma2files $i] [lindex $sigma3files $i]
                puts $stressStrainPostResFile "Result \"Shear Stress\" \"Combinations\" $stressCombNum Vector OnNodes\n"
                WriteStressStrainVectorData "tau" [lindex $tau12files $i] [lindex $tau23files $i] [lindex $tau31files $i]
            }
            for { set i $strainIndex } { $i < $strainLength } { incr i } {
                set strainCombNum [string range [lindex $eps1files $i] [expr [string last "_Comb" [lindex $eps1files $i]] + 5] [expr [string last "_Eps1" [lindex $eps1files $i]] - 1]]  
                puts $stressStrainPostResFile "Result \"Strain\" \"Combinations\" $strainCombNum Vector OnNodes\n"
                WriteStressStrainVectorData "eps" [lindex $eps1files $i] [lindex $eps2files $i] [lindex $eps3files $i]
                puts $stressStrainPostResFile "Result \"Shear Strain\" \"Combinations\" $strainCombNum Vector OnNodes\n"
                WriteStressStrainVectorData "gamma" [lindex $gamma12files $i] [lindex $gamma23files $i] [lindex $gamma31files $i]
            }
        } else {
            #Load Cases
            while { $stressIndex < $stressLength && $strainIndex < $strainLength } { 

                set stressCombNum [string range [lindex $sigma1files $stressIndex] [expr [string last "_LCase" [lindex $sigma1files $stressIndex]] + 6] [expr [string last "_Sigma1" [lindex $sigma1files $stressIndex]] - 1]]  
                set strainCombNum [string range [lindex $eps1files $strainIndex] [expr [string last "_LCase" [lindex $eps1files $strainIndex]] + 6] [expr [string last "_Eps1" [lindex $eps1files $strainIndex]] - 1]]      

                if { $stressCombNum < $strainCombNum } {
                    puts $stressStrainPostResFile "Result \"Stress\" \"Load Cases\" $stressCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "sigma" [lindex $sigma1files $stressIndex] [lindex $sigma2files $stressIndex] [lindex $sigma3files $stressIndex]
                    puts $stressStrainPostResFile "Result \"Shear Stress\" \"Load Cases\" $stressCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "tau" [lindex $tau12files $stressIndex] [lindex $tau23files $stressIndex] [lindex $tau31files $stressIndex]
                    set stressIndex [expr $stressIndex + 1]
                } elseif { $strainCombNum < $stressCombNum } {
                    puts $stressStrainPostResFile "Result \"Strain\" \"Load Cases\" $strainCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "eps" [lindex $eps1files $stressIndex] [lindex $eps2files $stressIndex] [lindex $eps3files $stressIndex]
                    puts $stressStrainPostResFile "Result \"Shear Strain\" \"Load Cases\" $strainCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "gamma" [lindex $gamma12files $stressIndex] [lindex $gamma23files $stressIndex] [lindex $gamma31files $stressIndex]
                    set strainIndex [expr $strainIndex + 1]
                } else {
                    puts $stressStrainPostResFile "Result \"Stress\" \"Load Cases\" $stressCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "sigma" [lindex $sigma1files $stressIndex] [lindex $sigma2files $stressIndex] [lindex $sigma3files $stressIndex]
                    puts $stressStrainPostResFile "Result \"Shear Stress\" \"Load Cases\" $stressCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "tau" [lindex $tau12files $stressIndex] [lindex $tau23files $stressIndex] [lindex $tau31files $stressIndex]
                    puts $stressStrainPostResFile "Result \"Strain\" \"Load Cases\" $strainCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "eps" [lindex $eps1files $strainIndex] [lindex $eps2files $strainIndex] [lindex $eps3files $strainIndex]
                    puts $stressStrainPostResFile "Result \"Shear Strain\" \"Load Cases\" $strainCombNum Vector OnNodes\n"
                    WriteStressStrainVectorData "gamma" [lindex $gamma12files $strainIndex] [lindex $gamma23files $strainIndex] [lindex $gamma31files $strainIndex]
                    set stressIndex [expr $stressIndex + 1]
                    set strainIndex [expr $strainIndex + 1]   
                }
            }

            for { set i $stressIndex } { $i < $stressLength } { incr i } {
                set stressCombNum [string range [lindex $sigma1files $i] [expr [string last "_LCase" [lindex $sigma1files $i]] + 6] [expr [string last "_Sigma1" [lindex $sigma1files $i]] - 1]]  
                puts $stressStrainPostResFile "Result \"Stress\" \"Load Cases\" $stressCombNum Vector OnNodes\n"
                WriteStressStrainVectorData "sigma" [lindex $sigma1files $i] [lindex $sigma2files $i] [lindex $sigma3files $i]
                puts $stressStrainPostResFile "Result \"Shear Stress\" \"Load Cases\" $stressCombNum Vector OnNodes\n"
                WriteStressStrainVectorData "tau" [lindex $tau12files $i] [lindex $tau23files $i] [lindex $tau31files $i]
            }
            for { set i $strainIndex } { $i < $strainLength } { incr i } {
                set strainCombNum [string range [lindex $eps1files $i] [expr [string last "_LCase" [lindex $eps1files $i]] + 6] [expr [string last "_Eps1" [lindex $eps1files $i]] - 1]]  
                puts $stressStrainPostResFile "Result \"Strain\" \"Load Cases\" $strainCombNum Vector OnNodes\n"
                WriteStressStrainVectorData "eps" [lindex $eps1files $i] [lindex $eps2files $i] [lindex $eps3files $i]
                puts $stressStrainPostResFile "Result \"Shear Strain\" \"Load Cases\" $strainCombNum Vector OnNodes\n"
                WriteStressStrainVectorData "gamma" [lindex $gamma12files $i] [lindex $gamma23files $i] [lindex $gamma31files $i]
            }
        }
        #close $stressStrainPostResFile
    }   
}

proc WriteStressStrainVectorData { type file1 file2 file3 } {

    global stressStrainPostResFile
    # global stressUnit

    #global PROJECTPATH
    #set result [open "$PROJECTPATH\\result.dat" "w"]
    #puts $result "AQUI"
    #puts $result $file1
    #puts $result $file2
    #puts $result $file3
    #close $result 

    switch $type {

        "sigma" {
            if { $file3 != "" } {
                puts $stressStrainPostResFile "ComponentNames \"Stress-X\" \"Stress-Y\" \"Stress-Z\"\n"
                # puts $stressStrainPostResFile "ComponentNames \"Stress-X ($stressUnit)\" \"Stress-Y ($stressUnit)\" \"Stress-Z ($stressUnit)\"\n"
                puts $stressStrainPostResFile "Values"
                puts $stressStrainPostResFile [format %s%20s%20s%20s%s "#NodeNum" "Sigma-1" "Sigma-2" "Sigma-3" "\n"]
            } else {
                puts $stressStrainPostResFile "ComponentNames \"Stress-X\" \"Stress-Y\"\n"
                # puts $stressStrainPostResFile "ComponentNames \"Stress-X ($stressUnit)\" \"Stress-Y ($stressUnit)\"\n"
                puts $stressStrainPostResFile "Values"
                puts $stressStrainPostResFile [format %s%20s%20s%s "#NodeNum" "Sigma-1" "Sigma-2" "\n"]
            }
        }
        "tau" {
            puts $stressStrainPostResFile "ComponentNames \"ShearStress-XY\" \"ShearStress-YZ\" \"ShearStress-ZX\"\n"
            # puts $stressStrainPostResFile "ComponentNames \"ShearStress-XY ($stressUnit)\" \"ShearStress-YZ ($stressUnit)\" \"ShearStress-ZX ($stressUnit)\"\n"
            puts $stressStrainPostResFile "Values"
            puts $stressStrainPostResFile [format %s%20s%20s%20s%s "#NodeNum" "Tau-12" "Tau-23" "Tau-31" "\n"]
        }
        "eps" {
            if { $file3 != "" } {
                puts $stressStrainPostResFile "ComponentNames \"Strain-X\" \"Strain-Y\" \"Strain-Z\"\n"
                puts $stressStrainPostResFile "Values"
                puts $stressStrainPostResFile [format %s%20s%20s%20s%s "#NodeNum" "Eps-1" "Eps-2" "Eps-3" "\n"]
            } else {
                puts $stressStrainPostResFile "ComponentNames \"Strain-X\" \"Strain-Y\" \n"
                puts $stressStrainPostResFile "Values"
                puts $stressStrainPostResFile [format %s%20s%20s%s "#NodeNum" "Eps-1" "Eps-2" "\n"]
            }
        }
        "gamma" {
            puts $stressStrainPostResFile "ComponentNames \"ShearStrain-XY\" \"ShearStrain-YZ\" \"ShearStrain-ZX\"\n"
            puts $stressStrainPostResFile "Values"
            puts $stressStrainPostResFile [format %s%20s%20s%20s%s "#NodeNum" "Gamma-12" "Gamma-23" "Gamma-31" "\n"]
        }
    }
    
    set file1 [open $file1 "r"]
    set file1data [read $file1]
    close $file1

    set file2 [open $file2 "r"]
    set file2data [read $file2]
    close $file2

    if { $file3 != "" } {
        set file3 [open $file3 "r"]
        set file3data [read $file3]
        close $file3
        for {set i 0} {$i < [llength $file1data]} {incr i 2} {
            puts $stressStrainPostResFile [format %8d%20.8e%20.8e%20.8e \
                                          [lindex $file1data $i] \
                                          [lindex $file1data [expr $i + 1]] \
                                          [lindex $file2data [expr $i + 1]] \
                                          [lindex $file3data [expr $i + 1]]]
        }
    } else {
        for {set i 0} {$i < [llength $file1data]} {incr i 2} {
            puts $stressStrainPostResFile [format %8d%20.8e%20.8e \
                                          [lindex $file1data $i] \
                                          [lindex $file1data [expr $i + 1]] \
                                          [lindex $file2data [expr $i + 1]]]
        }
    }
    puts $stressStrainPostResFile "\nEnd Values\n"    
}

proc WriteStressData { firstSigmaPath secondSigmaPath tauPath } {

    global stressStrainPostResFile
    # global stressUnit

    puts $stressStrainPostResFile "ComponentNames \"Stress-X\" \"Stress-Y\" \"ShearStress-XY\"\n"
    # puts $stressStrainPostResFile "ComponentNames \"Stress-X ($stressUnit)\" \"Stress-Y ($stressUnit)\" \"ShearStress-XY ($stressUnit)\"\n"
    puts $stressStrainPostResFile "Values"
    puts $stressStrainPostResFile "#NodeNum             Sigma-X             Sigma-Y              Tau-XY\n"
    
    set firstSigmaFile [open $firstSigmaPath "r"]
    set firstSigmaData [read $firstSigmaFile]
    close $firstSigmaFile

    set secondSigmaFile [open $secondSigmaPath "r"]
    set secondSigmaData [read $secondSigmaFile]
    close $secondSigmaFile

    set tauFile [open $tauPath "r"]
    set tauData [read $tauFile]
    close $tauFile

    for {set i 0} {$i < [llength $firstSigmaData]} {incr i 2} {
        puts $stressStrainPostResFile [format %8d%20.8e%20.8e%20.8e \
                                              [lindex $firstSigmaData $i] \
                                              [lindex $firstSigmaData [expr $i + 1]] \
                                              [lindex $secondSigmaData [expr $i + 1]] \
                                              [lindex $tauData [expr $i + 1]]]
    }
    puts $stressStrainPostResFile "\nEnd Values\n"
}

proc WriteMiddleStressData { firstTauPath secondTauPath } {

    global stressStrainPostResFile
    # global stressUnit

    puts $stressStrainPostResFile "ComponentNames \"ShearStress-YZ\" \"ShearStress-ZX\"\n"
    # puts $stressStrainPostResFile "ComponentNames \"ShearStress-YZ ($stressUnit)\" \"ShearStress-ZX ($stressUnit)\"\n"
    puts $stressStrainPostResFile "Values"
    puts $stressStrainPostResFile "#NodeNum              Tau-YZ              Tau-ZX\n"
    
    set firstTauFile [open $firstTauPath "r"]
    set firstTauData [read $firstTauFile]
    close $firstTauFile

    set secondTauFile [open $secondTauPath "r"]
    set secondTauData [read $secondTauFile]
    close $secondTauFile

    for {set i 0} {$i < [llength $firstTauData]} {incr i 2} {
        puts $stressStrainPostResFile [format %8d%20.8e%20.8e \
                                              [lindex $firstTauData $i] \
                                              [lindex $firstTauData [expr $i + 1]] \
                                              [lindex $secondTauData [expr $i + 1]]]
    }
    puts $stressStrainPostResFile "\nEnd Values\n"
}

proc WriteStrainData { firstEpsPath secondEpsPath gammaPath } {

    global stressStrainPostResFile

    puts $stressStrainPostResFile "ComponentNames \"Strain-X\" \"Strain-Y\" \"ShearStrain-XY\"\n"
    puts $stressStrainPostResFile "Values"
    puts $stressStrainPostResFile "#NodeNum               Eps-X               Eps-Y            Gamma-XY\n"
    
    set firstEpsFile [open $firstEpsPath "r"]
    set firstEpsData [read $firstEpsFile]
    close $firstEpsFile

    set secondEpsFile [open $secondEpsPath "r"]
    set secondEpsData [read $secondEpsFile]
    close $secondEpsFile

    set gammaFile [open $gammaPath "r"]
    set gammaData [read $gammaFile]
    close $gammaFile

    for {set i 0} {$i < [llength $firstEpsData]} {incr i 2} {
        puts $stressStrainPostResFile [format %8d%20.8e%20.8e%20.8e \
                                              [lindex $firstEpsData $i] \
                                              [lindex $firstEpsData [expr $i + 1]] \
                                              [lindex $secondEpsData [expr $i + 1]] \
                                              [lindex $gammaData [expr $i + 1]]]
    }
    puts $stressStrainPostResFile "\nEnd Values\n"
}

proc WriteMiddleStrainData { firstGammaPath secondGammaPath } {

    global stressStrainPostResFile

    puts $stressStrainPostResFile "ComponentNames \"ShearStrain-YZ\" \"ShearStrain-ZX\"\n"
    puts $stressStrainPostResFile "Values"
    puts $stressStrainPostResFile "#NodeNum            Gamma-YZ            Gamma-ZX\n"
    
    set firstGammaFile [open $firstGammaPath "r"]
    set firstGammaData [read $firstGammaFile]
    close $firstGammaFile

    set secondGammaFile [open $secondGammaPath "r"]
    set secondGammaData [read $secondGammaFile]
    close $secondGammaFile

    for {set i 0} {$i < [llength $firstGammaData]} {incr i 2} {
        puts $stressStrainPostResFile [format %8d%20.8e%20.8e \
                                              [lindex $firstGammaData $i] \
                                              [lindex $firstGammaData [expr $i + 1]] \
                                              [lindex $secondGammaData [expr $i + 1]]]
    }
    puts $stressStrainPostResFile "\nEnd Values\n"
}