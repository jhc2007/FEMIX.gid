proc CreateSurfaceSprings { femixData } {

	global SurfaceSpringsFromFemixFile \
	       NUMERICALINTEGRATIONLIST \
	       SpringsNumericalIntegrationsFromFemixFile \
	       SpringMaterialsList

	set SurfaceSpringsFromFemixFile {}

	set surfaceSpringsData [GetBlockData $femixData "SURFACE_SPRINGS"]

	if { [llength $surfaceSpringsData] > 0 } {

		set index 4
		set firstSurfNumInt yes

		while { $index < [llength $surfaceSpringsData] } {
			
			incr index
			set groupName [lindex $surfaceSpringsData $index]
			if { ![ExistsListElement [GiD_Info layers] $groupName] && ![ExistsListElement [GiD_Groups list] $groupName] } {
				GiD_Process 'Groups Create $groupName Mescape
			}
			incr index 2
			set rangeList [GetFirstLastValuesFromRange [lindex $surfaceSpringsData $index]]
			set firstElem [lindex $rangeList 0]
			set lastElem [lindex $rangeList 1]
			incr index 
			set surfaceSpring [list $groupName $firstElem $lastElem [lindex $surfaceSpringsData $index]]
			incr index
			set key [lindex $surfaceSpringsData $index]
			if { [string index $key 0] != "_" } {
				set vector [GetAuxiliaryVectorFromFemixFile $key]
				foreach var $vector {
					lappend surfaceSpring $var
				}
			} else {
				lappend surfaceSpring $key
			}
			for {set i 0} {$i < 3} {incr i} {
				incr index
				lappend surfaceSpring [lindex $surfaceSpringsData $index]
			}

			set integrationName [lindex $surfaceSpringsData $index]

			if { $integrationName != "_DEFAULT" && ![ExistsListElement $NUMERICALINTEGRATIONLIST $integrationName] && ![ExistsSpringsNumericalIntegrationFromFemixFile $integrationName] } {
				if { $firstSurfNumInt } {
					set integrationData [GetBlockData $femixData "NUMERICAL_INTEGRATION"]
					set firstSurfNumInt no
				}
				set intIndex 5
				while { $intIndex < [llength $integrationData] && [lindex $integrationData $intIndex] != $integrationName } {
					incr intIndex
					set numKey [lindex $integrationData $intIndex]
					incr intIndex [expr $numKey + 3]
				}
				if { $intIndex < [llength $integrationData] } {
					incr intIndex
					set numKey [lindex $integrationData $intIndex]
					set surfNumInt [list $integrationName $numKey]
					for {set i 0} {$i < $numKey} {incr i} {
						incr intIndex
						lappend surfNumInt [lindex $integrationData $intIndex]
					}
					lappend SpringsNumericalIntegrationsFromFemixFile $surfNumInt
				}
			}
			
			incr index 
			set materialType [lindex $surfaceSpringsData $index]
			lappend surfaceSpring $materialType

			incr index
			set numNodes [lindex $surfaceSpringsData $index]
			lappend surfaceSpring $numNodes

			for {set i 0} {$i < $numNodes} {incr i} {
				incr index
				set materialName [lindex $surfaceSpringsData $index]
				lappend surfaceSpring $materialName
  				if { ![ExistsListElement $SpringMaterialsList [list [string range $materialType 1 end] $materialName]] } {
  				    lappend SpringMaterialsList [list [string range $materialType 1 end] $materialName]
  				}
			}
			lappend SurfaceSpringsFromFemixFile $surfaceSpring
			incr index 2
		}
	}
}

proc AddAuxiliaryVectorFromSurfaceSpringsFromFemixFile {} {

	global SurfaceSpringsFromFemixFile

	foreach var $SurfaceSpringsFromFemixFile {
		if { [string is double [lindex $var 4]] } {
			AddAuxiliaryVector [lindex $var 4] [lindex $var 5] [lindex $var 6]
		}
	}
	# return 0
}

proc CountSurfaceSpringsFromFemixFile {} {

	global SurfaceSpringsFromFemixFile

	set count 0

	for {set i 0} {$i < [llength $SurfaceSpringsFromFemixFile]} {incr i} {
		set count [expr $count + [lindex [lindex $SurfaceSpringsFromFemixFile $i] 2] - [lindex [lindex $SurfaceSpringsFromFemixFile $i] 1] + 1]
	}

	return $count
}

proc WriteSurfaceSpringsFromFemixFile { index } {

	global SurfaceSpringsFromFemixFile

	set returnString ""
	incr index 

  # global PROBLEMTYPEPATH
  # set result [open "$PROBLEMTYPEPATH\\result_new.dat" "w"]
  # puts $result "SurfSprings:"
  # foreach var $SurfaceSpringsFromFemixFile {
  # 	puts $result $var
  # }
  # close $result

	for {set i 0} {$i < [llength $SurfaceSpringsFromFemixFile]} {incr i} {
	
		set surfaceSpring [lindex $SurfaceSpringsFromFemixFile $i]

		if { [lindex $surfaceSpring 1] == [lindex $surfaceSpring 2] } {
            append returnString [format %7d $index]
            append returnString "  [lindex $surfaceSpring 0]  1  "
            append returnString [lindex $surfaceSpring 1]
            incr index
        } else {
            append returnString "  \[$index\-[expr $index + [lindex $surfaceSpring 2] - [lindex $surfaceSpring 1]]\]  "
            append returnString "[lindex $surfaceSpring 0]  1  "
            append returnString "\[[lindex $surfaceSpring 1]\-[lindex $surfaceSpring 2]\]"
            set index [expr $index + [lindex $surfaceSpring 2] - [lindex $surfaceSpring 1] + 1]
        }

        append returnString "  [lindex $surfaceSpring 3]"

        if { [string is double [lindex $surfaceSpring 4]] } {
        	append returnString "  VECTOR_[GetAuxiliaryVectorIndexFromComponents [lindex $surfaceSpring 4] [lindex $surfaceSpring 5] [lindex $surfaceSpring 6]]"
	        for {set j 7} {$j < [llength $surfaceSpring]} {incr j} {
	        	append returnString "  [lindex $surfaceSpring $j]"
	        }
        } else {
	        for {set j 4} {$j < [llength $surfaceSpring]} {incr j} {
	        	append returnString "  [lindex $surfaceSpring $j]"
	        }
        }

        append returnString " ;\n"
	}
	return $returnString
}

proc CreateFileSurfaceSpringsFromFemixFile {} {

	global PROJECTPATH \
		   SurfaceSpringsFromFemixFile

	if { [llength $SurfaceSpringsFromFemixFile] == 0 } {
		return
	}

	set surfaceSpringsFile [open "$PROJECTPATH\\femix_surface_springs.dat" "w"]
    for {set i 0} {$i < [llength $SurfaceSpringsFromFemixFile]} {incr i} {
    	foreach var [lindex $SurfaceSpringsFromFemixFile $i] {
    		puts $surfaceSpringsFile $var
    	}
    }
    close $surfaceSpringsFile
}

proc CreateFromFileSurfaceSpringsFromFemixFile {} {

	global PROJECTPATH \
		   SurfaceSpringsFromFemixFile \
		   SpringMaterialsList

	if { [file exists "$PROJECTPATH\\femix_surface_springs.dat"] } {
		
		set surfaceSpringsFile [open "$PROJECTPATH\\femix_surface_springs.dat" "r"]
  		set surfaceSpringsData [read $surfaceSpringsFile]
  		close $surfaceSpringsFile

  		set index 0

  		while { $index < [llength $surfaceSpringsData] } {

  			set surfaceSpring {}

  			for {set i $index} {$i < [expr $index + 4]} {incr i} {
  				lappend surfaceSpring [lindex $surfaceSpringsData $i]
  			}

  			incr index 4
  			if { [string is double [lindex $surfaceSpringsData $index]] } {
	  			for {set i $index} {$i < [expr $index + 6]} {incr i} {
	  				lappend surfaceSpring [lindex $surfaceSpringsData $i]
	  			}
	  			incr index 6
			} else {
				for {set i $index} {$i < [expr $index + 4]} {incr i} {
	  				lappend surfaceSpring [lindex $surfaceSpringsData $i]
	  			}
	  			incr index 4
			}

			set matType [lindex $surfaceSpringsData $index]
			lappend surfaceSpring $matType
  			set matType [string range $matType 1 end]

  			incr index
  			set numNodes [lindex $surfaceSpringsData $index]
  			lappend surfaceSpring $numNodes

  			for {set i 0} {$i < $numNodes} {incr i} {
  				incr index
  				set matName [lindex $surfaceSpringsData $index]
  				lappend surfaceSpring $matName
                if { ![ExistsListElement $SpringMaterialsList [list $matType $matName]] } {
                    lappend SpringMaterialsList [list $matType $matName]
                }
  			}
  			
  			incr index
  			lappend SurfaceSpringsFromFemixFile $surfaceSpring
  		}
	}
}