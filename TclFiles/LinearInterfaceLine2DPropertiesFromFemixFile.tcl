
proc CreateLinIntLine2D { femixData } {

    set linIntLine2DData [GetBlockData $femixData "LINEAR_INTERFACE_LINE_2D_PROPERTIES"]

    if { [llength $linIntLine2DData] > 0 } {

        global doubleFormatMatParameters
        
        set allMaterialsNameList [GiD_Info materials]
        set linIntLine2DNameList {}

        for {set i 0} {$i < [llength $allMaterialsNameList]} {incr i} {
            if { [lindex [GiD_Info materials [lindex $allMaterialsNameList $i]] 2] == "LIN_INT_LINE_2D" } {
                lappend linIntLine2DNameList [string toupper [lindex $allMaterialsNameList $i]]
            }
        }

    	set numMaterials [lindex $linIntLine2DData 2]

    	set index 4

    	for {set i 0} {$i < $numMaterials} {incr i} {

    		set materialName [lindex $linIntLine2DData [expr $index + 1]]

    		set materialData { LIN_INT_LINE_2D }

    		lappend materialData "[format $doubleFormatMatParameters [lindex $linIntLine2DData [expr $index + 2]]]"
	    	lappend materialData "[format $doubleFormatMatParameters [lindex $linIntLine2DData [expr $index + 3]]]"

	    	if { [ExistsListElement $linIntLine2DNameList [string toupper $materialName]] } {
	    		GiD_ModifyData materials $materialName $materialData
	    	} else {
	    		GiD_CreateData create material Linear_Interface_Line_2D $materialName $materialData
	    	}

        	set index [expr $index + 5]  
	    }
	}
}
