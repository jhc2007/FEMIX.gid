# NLMM401FromFemixFile.tcl
#
# CreateNLMM401 { femixData }

proc CreateNLMM401 { femixData } {

    global PROBLEMTYPEPATH \
           PROJECTPATH \

    set nlmm401Data [GetBlockData $femixData "NLMM401"]

    if { [llength $nlmm401Data] > 0 } {

        global doubleFormatMatParameters
    	set allMaterialsNameList [GiD_Info materials]
    	set nlmm401NameList {}

        for {set i 0} {$i < [llength $allMaterialsNameList]} {incr i} {
            if { [lindex [GiD_Info materials [lindex $allMaterialsNameList $i]] 2] == "NLMM401" } {
                lappend nlmm401NameList [string toupper [lindex $allMaterialsNameList $i]]
            }
        }

        set numMaterials [lindex $nlmm401Data 2]

        set index 4

        for {set i 0} {$i < $numMaterials} {incr i} {

            set materialName [lindex $nlmm401Data [expr $index + 1]]

            set materialData { NLMM401 }

            lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm401Data [expr $index + 2]]]"

            if { [lindex $nlmm401Data [expr $index + 3]] == "_NONE" } {
                lappend materialData "NONE"
            } else {
                lappend materialData "ALPHAT_"
            }

            for {set j 4} {$j < 11} {incr j} {
                lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm401Data [expr $index + $j]]]"
            }

            lappend materialData "User_Specified"
            lappend materialData $materialName

            set numSets [lindex $nlmm401Data [expr $index + 11]]

            lappend materialData $numSets

            if { [ExistsListElement $nlmm401NameList [string toupper $materialName]] } {
                GiD_ModifyData materials $materialName $materialData
            } else {
                GiD_CreateData create material NLMM401_Material $materialName $materialData
            }

            incr index 12

            if { $PROBLEMTYPEPATH == $PROJECTPATH } {
                set dbFile [open "$PROBLEMTYPEPATH\\TmpFiles\\$materialName.dat" "w"]
            } else {
                set dbFile [open "$PROJECTPATH\\$materialName.dat" "w"]
            }
            for {set i 0} {$i < $numSets} {incr i} {
                puts $dbFile "[lindex $nlmm401Data $index]\t[lindex $nlmm401Data [expr $index + 1]]"
                incr index 2
            }
            close $dbFile

            incr index
        }
    }
}