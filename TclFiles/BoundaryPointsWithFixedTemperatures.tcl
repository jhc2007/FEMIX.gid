proc AddBoundaryPointsWithFixedTemperature { node } {

	global BoundaryPointsWithFixedTemperaturesList

	if { ![ExistsListElement $BoundaryPointsWithFixedTemperaturesList $node] } {
		lappend BoundaryPointsWithFixedTemperaturesList $node
	}
	return 0
}

proc CountBoundaryPointsWithFixedTemperatures {} {

	global BoundaryPointsWithFixedTemperaturesList

	return [llength $BoundaryPointsWithFixedTemperaturesList]
}

proc WriteBoundaryPointsWithFixedTemperaturesList {} {

	global BoundaryPointsWithFixedTemperaturesList
	
	set BoundaryPointsWithFixedTemperaturesList [lsort $BoundaryPointsWithFixedTemperaturesList]
	
	set i 0
	set index 1
	set returnString ""

	while { $i < [llength $BoundaryPointsWithFixedTemperaturesList] } {	
		set firstNode [lindex $BoundaryPointsWithFixedTemperaturesList $i]
		set lastNode $firstNode
		incr i
		while { $i < [llength $BoundaryPointsWithFixedTemperaturesList] && [lindex $BoundaryPointsWithFixedTemperaturesList $i] == [expr $lastNode + 1] } {
			set lastNode [lindex $BoundaryPointsWithFixedTemperaturesList $i]
			incr i
		}
		if { $firstNode == $lastNode } {
			append returnString [format %7d $index]
			append returnString "  [GetFirstGroupName]  1  $firstNode ;\n"
			incr index
		} else {
			append returnString "  \[$index\-[expr $index + $lastNode - $firstNode]\]"
            append returnString "  [GetFirstGroupName]  1  \[$firstNode\-$lastNode\] ;\n"
            set index [expr $index + $lastNode - $firstNode + 1]
		}
	}
	return $returnString
}
