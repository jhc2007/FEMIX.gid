proc WriteValuesOfNormalizedHeatGenerationRateFunction { name flag } {

    # flag == 1 -> User_Specified -> name = MaterialName
    # flag == 0 -> Import_from_File -> name = FunctionName

	global PROJECTPATH \
           PROBLEMTYPEPATH 

    set returnString ""
    set name [ReplaceSpaceWithUnderscore $name]

    if { $flag == 1 } {
    	if { $PROJECTPATH == $PROBLEMTYPEPATH } {
    		set dbFile [open "$PROBLEMTYPEPATH\\TmpFiles\\$name.dat" "r"]
    	} else {
    		set dbFile [open "$PROJECTPATH\\$name.dat" "r"]
    	}
    } else {
    	set dbFile [open "$PROBLEMTYPEPATH\\MaterialDataBases\\NLMM401\\$name\_db.dat" "r"]
    }
    set dbData [read $dbFile]
    close $dbFile

    # set numSets [expr [llength $dbData] / 2]

    for {set i 0} {$i < [llength $dbData]} {incr i 2} {
    	append returnString "\n[lindex $dbData $i]\t[lindex $dbData [expr $i + 1]]"
    }
    append returnString "  ;"

    return $returnString
}