
proc CreateDefaultGroup {} {

	#global GroupNames
	#set GroupNames { Group0 }
	GiD_Process 'Groups Create FEMIXDefaultGroup Mescape
	return 1
}

proc GetFirstGroupName {} {

	if { [llength [GiD_Groups list]] == 0 } {
    	CreateDefaultGroup
    }
    return [ReplaceSpaceWithUnderscore [lindex [GiD_Groups list] 0]]
}

# proc WriteGroups {} {
# 	return [GiD_Groups list]
# }

# proc AfterCreateGroup { name } {

# 	  global GroupNames
# 	  lappend GroupNames $name

# }