    
proc NumOfLinIntLine2DInLayerProperties {} {

    global layerPropertiesList LinIntLine2DUsedInLayerProp

    set LinIntLine2DUsedInLayerProp {}

    for {set i 0} {$i < [llength $layerPropertiesList]} {incr i} { 
        if { [lindex [lindex $layerPropertiesList $i] 1] == "LIN_INT_LINE_2D" && ![ExistsLinIntLine2DInLayerProperties [lindex [lindex $layerPropertiesList $i] 2]] } {
            lappend LinIntLine2DUsedInLayerProp [lindex [lindex $layerPropertiesList $i] 2]
        }
    }

    return [llength $LinIntLine2DUsedInLayerProp]    
}

proc ExistsLinIntLine2DInLayerProperties { matName } {

    global LinIntLine2DUsedInLayerProp

    if { [ExistsListElement $LinIntLine2DUsedInLayerProp [ReplaceSpaceWithUnderscore $matName]] } {
        return 1
    }
    return 0
}

proc WriteLinIntLine2DInLayerProperties { index } {

    global LinIntLine2DUsedInLayerProp

    set linIntLine2D ""
    set materials [GiD_Info materials]

    #global PROJECTPATH
    #set result [open "$PROJECTPATH\\aaa_result.dat" "w"]
    ##puts $result $materials
    #set mat [GiD_Info materials [lindex $materials 5]]
    ##puts $result $mat
    #for {set i 1} {$i < [llength $mat]} {incr i} {
    # 	puts $result [lindex $mat $i]
    #}
    #close $result
    #return

    for {set i 0} {$i < [llength $materials]} {incr i} {

    	set material [GiD_Info materials [lindex $materials $i]]
        if { [lindex $material 2] == "LIN_INT_LINE_2D" && [ExistsLinIntLine2DInLayerProperties [ReplaceSpaceWithUnderscore [lindex $materials $i]]] } {

        	# append nlmm301 "#     A     B     C     D     E     F\n"

        	append linIntLine2D "[format %7d $index]  "
        	append linIntLine2D [ReplaceSpaceWithUnderscore [lindex $materials $i]]
        	for {set j 4} {$j <= 6} {incr j 2} {
                # append linIntLine2D [format %18.8e [RemoveUnitsFromParameterValue [lindex $material $j]]]
                append linIntLine2D [format %18.8e [lindex $material $j]]
            }
            append linIntLine2D " ;\n"

        	set index [expr $index + 1]
        }
    }
    return $linIntLine2D
}
