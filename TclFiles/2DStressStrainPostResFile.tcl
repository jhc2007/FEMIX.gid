# 2DStressStrainPostResFile.tcl
#
# proc Create2DStressStrainPostResFile { basename }

proc Create2DStressStrainPostResFile { basename } {

    global PROJECTPATH numCombinations

    set sigma2files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Sigma2_se.pva]]
    set sigma3files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Sigma3_se.pva]]
    set tau23files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Tau23_se.pva]]

    set eps2files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Eps2_sa.pva]]
    set eps3files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Eps3_sa.pva]]
    set gamma23files [lsort -dictionary [glob -nocomplain -directory $PROJECTPATH *_Gamma23_sa.pva]]

    set stressLength [llength $sigma2files]
    set strainLength [llength $eps2files]

    if { $stressLength > 0 || $strainLength > 0 } {

        global stressStrainPostResFile

        set stressStrainPostResFileName $basename
        append stressStrainPostResFileName "_stress_strain.post.res"
        set stressStrainPostResFile [open "$PROJECTPATH\\$stressStrainPostResFileName" "w"]
        puts $stressStrainPostResFile "GiD Post Results File 1.0\n"
        
        set stressIndex 0
        set strainIndex 0

        while { $stressIndex < $stressLength && $strainIndex < $strainLength } {

            if { $numCombinations > 0 } { 
                set stressCombNum [string range [lindex $sigma2files $stressIndex] [expr [string last "_Comb" [lindex $sigma2files $stressIndex]] + 5] [expr [string last "_Sigma2" [lindex $sigma2files $stressIndex]] - 1]]  
                set strainCombNum [string range [lindex $eps2files $strainIndex] [expr [string last "_Comb" [lindex $eps2files $strainIndex]] + 5] [expr [string last "_Eps2" [lindex $eps2files $strainIndex]] - 1]]  
            } else {
                set stressCombNum [string range [lindex $sigma2files $stressIndex] [expr [string last "_LCase" [lindex $sigma2files $stressIndex]] + 6] [expr [string last "_Sigma2" [lindex $sigma2files $stressIndex]] - 1]]  
                set strainCombNum [string range [lindex $eps2files $strainIndex] [expr [string last "_LCase" [lindex $eps2files $strainIndex]] + 6] [expr [string last "_Eps2" [lindex $eps2files $strainIndex]] - 1]]      
            }

            if { $stressCombNum < $strainCombNum } {
                if { $numCombinations > 0 } { 
                    puts $stressStrainPostResFile "Result \"Stress\" \"Combinations\" $stressCombNum Vector OnNodes\n"
                } else {
                    puts $stressStrainPostResFile "Result \"Stress\" \"Load Cases\" $stressCombNum Vector OnNodes\n"
                }
                WriteStressData [lindex $sigma2files $stressIndex] [lindex $sigma3files $stressIndex] [lindex $tau23files $stressIndex]
                set stressIndex [expr $stressIndex + 1]
            } elseif { $strainCombNum < $stressCombNum } {
                if { $numCombinations > 0 } { 
                    puts $stressStrainPostResFile "Result \"Strain\" \"Combinations\" $strainCombNum Vector OnNodes\n"
                } else {
                    puts $stressStrainPostResFile "Result \"Strain\" \"Load Cases\" $strainCombNum Vector OnNodes\n"
                }
                WriteStrainData [lindex $eps2files $strainIndex] [lindex $eps3files $strainIndex] [lindex $gamma23files $strainIndex]
                set strainIndex [expr $strainIndex + 1]
            } else {
                if { $numCombinations > 0 } { 
                    puts $stressStrainPostResFile "Result \"Stress\" \"Combinations\" $stressCombNum Vector OnNodes\n"
                    WriteStressData [lindex $sigma2files $stressIndex] [lindex $sigma3files $stressIndex] [lindex $tau23files $stressIndex]
                    puts $stressStrainPostResFile "Result \"Strain\" \"Combinations\" $strainCombNum Vector OnNodes\n"
                    WriteStrainData [lindex $eps2files $strainIndex] [lindex $eps3files $strainIndex] [lindex $gamma23files $strainIndex]
                } else {
                    puts $stressStrainPostResFile "Result \"Stress\" \"Load Cases\" $stressCombNum Vector OnNodes\n"
                    WriteStressData [lindex $sigma2files $stressIndex] [lindex $sigma3files $stressIndex] [lindex $tau23files $stressIndex]
                    puts $stressStrainPostResFile "Result \"Strain\" \"Load Cases\" $strainCombNum Vector OnNodes\n"
                    WriteStrainData [lindex $eps2files $strainIndex] [lindex $eps3files $strainIndex] [lindex $gamma23files $strainIndex]
                }
                set stressIndex [expr $stressIndex + 1]
                set strainIndex [expr $strainIndex + 1]
            }
        }
        if { $numCombinations > 0 } { 
            for { set i $stressIndex } { $i < $stressLength } { incr i } {
                set stressCombNum [string range [lindex $sigma2files $i] [expr [string last "_Comb" [lindex $sigma2files $i]] + 5] [expr [string last "_Sigma2" [lindex $sigma2files $i]] - 1]]  
                puts $stressStrainPostResFile "Result \"Stress\" \"Combinations\" $stressCombNum Vector OnNodes\n"
                WriteStressData [lindex $sigma2files $i] [lindex $sigma3files $i] [lindex $tau23files $i]
                #WriteStressData [lindex $sigma2files $stressIndex] [lindex $sigma3files $stressIndex] [lindex $tau23files $stressIndex]
            }
            for { set i $strainIndex } { $i < $strainLength } { incr i } {
                set strainCombNum [string range [lindex $eps2files $i] [expr [string last "_Comb" [lindex $eps2files $i]] + 5] [expr [string last "_Eps2" [lindex $eps2files $i]] - 1]]  
                puts $stressStrainPostResFile "Result \"Strain\" \"Load Analysis\" $strainCombNum Vector OnNodes\n"            
                WriteStrainData [lindex $eps2files $i] [lindex $eps3files $i] [lindex $gamma23files $i]
            }
        } else {
            for { set i $stressIndex } { $i < $stressLength } { incr i } {
                set stressCombNum [string range [lindex $sigma2files $i] [expr [string last "_LCase" [lindex $sigma2files $i]] + 6] [expr [string last "_Sigma2" [lindex $sigma2files $i]] - 1]]  
                puts $stressStrainPostResFile "Result \"Stress\" \"Load Cases\" $stressCombNum Vector OnNodes\n"
                WriteStressData [lindex $sigma2files $i] [lindex $sigma3files $i] [lindex $tau23files $i]
                #WriteStressData [lindex $sigma2files $stressIndex] [lindex $sigma3files $stressIndex] [lindex $tau23files $stressIndex]
            }
            for { set i $strainIndex } { $i < $strainLength } { incr i } {
                set strainCombNum [string range [lindex $eps2files $i] [expr [string last "_LCase" [lindex $eps2files $i]] + 6] [expr [string last "_Eps2" [lindex $eps2files $i]] - 1]]  
                puts $stressStrainPostResFile "Result \"Strain\" \"Load Cases\" $strainCombNum Vector OnNodes\n"            
                WriteStrainData [lindex $eps2files $i] [lindex $eps3files $i] [lindex $gamma23files $i]
            }
        }
        close $stressStrainPostResFile
    }
}