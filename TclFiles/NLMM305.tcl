# nlmm305.tcl
#
# NumOfNLMM305InLayerProperties {}
# ExistsNLMM305InLayerProperties { matName }
# WriteNLMM305InLayerProperties { index }

proc NumOfNLMM305InLayerProperties {} {

    global layerPropertiesList NLMM305UsedInLayerProp

    set NLMM305UsedInLayerProp {}

    for {set i 0} {$i < [llength $layerPropertiesList]} {incr i} { 
        if { [lindex [lindex $layerPropertiesList $i] 1] == "NLMM305" && ![ExistsNLMM305InLayerProperties [lindex [lindex $layerPropertiesList $i] 2]] } {
            lappend NLMM305UsedInLayerProp [lindex [lindex $layerPropertiesList $i] 2]
        }
    }

    return [llength $NLMM305UsedInLayerProp]    
}

proc ExistsNLMM305InLayerProperties { matName } {

    global NLMM305UsedInLayerProp

    if { [ExistsListElement $NLMM305UsedInLayerProp [ReplaceSpaceWithUnderscore $matName]] } {
        return 1
    }
    return 0
}

proc WriteNLMM305InLayerProperties { index } {

    global NLMM305UsedInLayerProp

    set nlmm305 ""
    set materials [GiD_Info materials]

    #global PROJECTPATH
    #set result [open "$PROJECTPATH\\aaa_result.dat" "w"]
    ##puts $result $materials
    #set mat [GiD_Info materials [lindex $materials 5]]
    ##puts $result $mat
    #for {set i 1} {$i < [llength $mat]} {incr i} {
    # 	puts $result [lindex $mat $i]
    #}
    #close $result
    #return

    for {set i 0} {$i < [llength $materials]} {incr i} {

    	set material [GiD_Info materials [lindex $materials $i]]
        if { [lindex $material 2] == "NLMM305" && [ExistsNLMM305InLayerProperties [ReplaceSpaceWithUnderscore [lindex $materials $i]]] } {

        	# append nlmm305 "#     A     B     C     D     E     F\n"

        	append nlmm305 "[format %7d $index]  "
        	append nlmm305 [ReplaceSpaceWithUnderscore [lindex $materials $i]]
        	for {set j 4} {$j <= 16} {incr j 2} {
                # append nlmm305 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material $j]]]
                append nlmm305 [format %18.8e [lindex $material $j]]
            }
            append nlmm305 " ;\n"

        	incr index 1
        }
    }
    return $nlmm305
}
