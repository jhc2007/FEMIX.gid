# NLMM151.tcl
#
# NumOfNLMM151InLayerProperties {}
# ExistsNLMM151InLayerProperties { matName }
# WriteNLMM151InLayerProperties { index }

proc NumOfNLMM151InLayerProperties {} {

    global layerPropertiesList NLMM151UsedInLayerProp

    set NLMM151UsedInLayerProp {}

    for {set i 0} {$i < [llength $layerPropertiesList]} {incr i} { 
        if { [lindex [lindex $layerPropertiesList $i] 1] == "NLMM151" && ![ExistsListElement $NLMM151UsedInLayerProp [lindex [lindex $layerPropertiesList $i] 2]] } {
            lappend NLMM151UsedInLayerProp [lindex [lindex $layerPropertiesList $i] 2]
        }
    }

    return [llength $NLMM151UsedInLayerProp]    
}

proc ExistsNLMM151InLayerProperties { matName } {

    global NLMM151UsedInLayerProp

    if { [ExistsListElement $NLMM151UsedInLayerProp [ReplaceSpaceWithUnderscore $matName]] } {
        return 1
    }
    return 0
}

proc WriteNLMM151InLayerProperties { index } {

    global NLMM151UsedInLayerProp

    set nlmm151 ""
    set materials [GiD_Info materials]


    #global PROJECTPATH
    #set result [open "$PROJECTPATH\\aaa_result.dat" "w"]
    ##puts $result $materials
    #set mat [GiD_Info materials [lindex $materials 5]]
    ##puts $result $mat
    #for {set i 1} {$i < [llength $mat]} {incr i} {
    #	puts $result [lindex $mat $i]
    #}
    #close $result
    #return

    for {set i 0} {$i < [llength $materials]} {incr i} {

    	set material [GiD_Info materials [lindex $materials $i]]
        if { [lindex $material 2] == "NLMM151" && [ExistsListElement $NLMM151UsedInLayerProp [ReplaceSpaceWithUnderscore [lindex $materials $i]]] } {

        	append nlmm151 "#     A     B     C     D     E     F\n"
        	
        	append nlmm151 "[format %7d $index]  "
        	append nlmm151 [ReplaceSpaceWithUnderscore [lindex $materials $i]]
        	for {set j 4} {$j <= 10} {incr j 2} {
                # append nlmm151 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material $j]]]
                append nlmm151 [format %18.8e [lindex $material $j]]
            }

            append nlmm151 "\n#     G     H     I     J     K\n"
            
           	for {set j 12} {$j <= 18} {incr j 2} {
                append nlmm151 [format %18.8e [lindex $material $j]]
            }
            append nlmm151 "  \_[lindex $material 20]"

            append nlmm151 "\n#     L     M       N      O       P\n"
            append nlmm151 "\n    0.00    54.55   0.5    0.25    6.5\n"

            append nlmm151 "\n#     Q     R     S     T     U     V     W     X     Y     Z\n"
            
            append nlmm151 [format %21.8e [lindex $material 22]]
            append nlmm151 "  \_[lindex $material 24]"
            for {set j 26} {$j <= 36} {incr j 2} {
                append nlmm151 [format %18.8e [lindex $material $j]]
            }
            switch [lindex $material 24] {
            	"TRILINEAR" {
            		# append nlmm151 [format %21.8e [RemoveUnitsFromParameterValue [lindex $material 38]]]
                    append nlmm151 [format %21.8e [lindex $material 38]]
            	}
            	"QUADRILINEAR" {
            		# append nlmm151 [format %21.8e [RemoveUnitsFromParameterValue [lindex $material 40]]]
                    append nlmm151 [format %21.8e [lindex $material 40]]
            	}
            	default {
            		# append nlmm151 [format %21.8e [RemoveUnitsFromParameterValue [lindex $material 42]]]
                    append nlmm151 [format %21.8e [lindex $material 42]]
            	}
            }
            append nlmm151 [format %18.8e [lindex $material 44]]

            append nlmm151 "\n#     AA     AB     AC     AD     AE     AF     AG     AH     AI\n"

            append nlmm151 "  \_[lindex $material 46]"
            for {set j 48} {$j <= 54} {incr j 2} {
                append nlmm151 [format %18.8e [lindex $material $j]]
            }            

            if { [lindex $material 56] == "VALUE" } {
            	append nlmm151 [format %21.8e [lindex $material 58]]
            } else {
            	append nlmm151 "      \_[lindex $material 56]"
            }
            append nlmm151 "  \_[lindex $material 58]"
            # append nlmm151 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material 60]]]
            append nlmm151 [format %18.8e [lindex $material 60]]
            switch [lindex $material 46] {
            	"NONE" {
            		# append nlmm151 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material 62]]]
                    append nlmm151 [format %18.8e [lindex $material 62]]
            	}
            	"LINEAR" {
            		# append nlmm151 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material 64]]]
                    append nlmm151 [format %18.8e [lindex $material 64]]

            	}
            	"TRILINEAR" {
            		# append nlmm151 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material 66]]]
                    append nlmm151 [format %18.8e [lindex $material 66]]
            	}
            	default {
            		# append nlmm151 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material 68]]]
                    append nlmm151 [format %18.8e [lindex $material 68]]
            	}
            }

            append nlmm151 "\n#     AJ     AK     AL     AM\n"

            append nlmm151 "      \_[lindex $material 70]"
            if { [lindex $material 72] == "VALUE" } {
            	# append nlmm151 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material 74]]]
                append nlmm151 [format %18.8e [lindex $material 74]]
            } else {
            	append nlmm151 "  \_[lindex $material 72]"
            }
            append nlmm151 "  [lindex $material 76]  "  
            # append nlmm151 [RemoveUnitsFromParameterValue [lindex $material 78]]
            append nlmm151 [lindex $material 78]

            append nlmm151 " ;\n"

        	set index [expr $index + 1]
        }
    }
    return $nlmm151
}