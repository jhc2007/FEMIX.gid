# EdgeLoadsFromFemixFile.tcl
#
# CreateEdgeLoadsFromFemixFile

proc CreateEdgeLoadsFromFemixFile { loadCaseData loadCaseNumber } {

	global EdgeLoadsFromFemixFile
	
	set edgeLoadsData [GetBlockData $loadCaseData "EDGE_LOADS"]

	set index 5

	while { $index < [llength $edgeLoadsData] } {
		
		set rangeList [GetFirstLastValuesFromRange [lindex $edgeLoadsData $index]]
		set firstElem [lindex $rangeList 0]
		set lastElem [lindex $rangeList 1]

		incr index
		set face [lindex $edgeLoadsData $index]

		incr index
		set edge [lindex $edgeLoadsData $index]

		set edgeLoad [list $loadCaseNumber $firstElem $lastElem $face $edge]

		incr index
		set key [lindex $edgeLoadsData $index]

		if { [string index $key 0] != "_" } {
			set vector [GetAuxiliaryVectorFromFemixFile $key]
			foreach var $vector {
				lappend edgeLoad $var
			}
		} else {
			lappend edgeLoad $key
		}

		incr index
		lappend edgeLoad [lindex $edgeLoadsData $index]

		incr index
		set numNodes [lindex $edgeLoadsData $index]
		lappend edgeLoad $numNodes

		for {set i 0} {$i < $numNodes} {incr i} {
			incr index
			lappend edgeLoad [lindex $edgeLoadsData $index]
		}

		lappend EdgeLoadsFromFemixFile $edgeLoad

		incr index 3
	}
}

proc AddAuxiliaryVectorFromEdgeLoadsFromFemixFile {} {

	global EdgeLoadsFromFemixFile

	foreach var $EdgeLoadsFromFemixFile {
		if { [string is double [lindex $var 5]] } {
			AddAuxiliaryVector [lindex $var 5] [lindex $var 6] [lindex $var 7]
		}
	}
	return 0
}

proc CountEdgeLoadsFromFemixFile { loadCaseNumber } {

	global EdgeLoadsFromFemixFile

	set index 0
	set count 0

	while { $index < [llength $EdgeLoadsFromFemixFile] && [lindex [lindex $EdgeLoadsFromFemixFile $index] 0] != $loadCaseNumber } {
		incr index
	}

	while { $index < [llength $EdgeLoadsFromFemixFile] && [lindex [lindex $EdgeLoadsFromFemixFile $index] 0] == $loadCaseNumber } {
		set count [expr $count + [lindex [lindex $EdgeLoadsFromFemixFile $index] 2] - [lindex [lindex $EdgeLoadsFromFemixFile $index] 1] + 1]
		incr index
	}

	return $count
}

proc WriteEdgeLoadsFromFemixFile { loadCaseNumber index } {

	global EdgeLoadsFromFemixFile 

	incr index
	set returnString ""
	set i 0

	while { $i < [llength $EdgeLoadsFromFemixFile] && [lindex [lindex $EdgeLoadsFromFemixFile $i] 0] != $loadCaseNumber } {
		incr i
	}

	while { $i < [llength $EdgeLoadsFromFemixFile] && [lindex [lindex $EdgeLoadsFromFemixFile $i] 0] == $loadCaseNumber } {

		set edgeLoad [lindex $EdgeLoadsFromFemixFile $i]

        if { [lindex $edgeLoad 1] == [lindex $edgeLoad 2] } {
            append returnString [format %7d $index]
            append returnString "  [lindex $edgeLoad 1]"
            incr index
        } else {
            append returnString "  \[$index\-[expr $index + [lindex $edgeLoad 2] - [lindex $edgeLoad 1]]\]"
            append returnString "  \[[lindex $edgeLoad 1]\-[lindex $edgeLoad 2]\]"
            set index [expr $index + [lindex $edgeLoad 2] - [lindex $edgeLoad 1] + 1]
        }

        append returnString "  [lindex $edgeLoad 3]"
        append returnString "  [lindex $edgeLoad 4]"

        if { [string is double [lindex $edgeLoad 5]] } {
        	append returnString "  VECTOR_[GetAuxiliaryVectorIndexFromComponents [lindex $edgeLoad 5] [lindex $edgeLoad 6] [lindex $edgeLoad 7]]"
	        for {set j 8} {$j < [llength $edgeLoad]} {incr j} {
	        	append returnString "  [lindex $edgeLoad $j]"
	        }
        } else {
	        for {set j 5} {$j < [llength $edgeLoad]} {incr j} {
	        	append returnString "  [lindex $edgeLoad $j]"
	        }
        }

        append returnString " ;\n"
		incr i
	}

    return $returnString
}

proc CreateFileEdgeLoadsFromFemixFile {} {

	global PROJECTPATH \
		   EdgeLoadsFromFemixFile

	if { [llength $EdgeLoadsFromFemixFile] == 0 } {
		return
	}

	set edgeLoadsFile [open "$PROJECTPATH\\femix_edge_loads.dat" "w"]
    for {set i 0} {$i < [llength $EdgeLoadsFromFemixFile]} {incr i} {
    	foreach var [lindex $EdgeLoadsFromFemixFile $i] {
    		puts $edgeLoadsFile $var
    	}
    }
    close $edgeLoadsFile
}

proc CreateFromFileEdgeLoadsFromFemixFile {} {

	global PROJECTPATH \
		   EdgeLoadsFromFemixFile

	if { [file exists "$PROJECTPATH\\femix_edge_loads.dat"] } {
		
		set edgeLoadsFile [open "$PROJECTPATH\\femix_edge_loads.dat" "r"]
  		set edgeLoadsData [read $edgeLoadsFile]
  		close $edgeLoadsFile

  		set index 0

  		while { $index < [llength $edgeLoadsData] } {

  			set edgeLoad {}

  			for {set i $index} {$i < [expr $index + 5]} {incr i} {
  				lappend edgeLoad [lindex $edgeLoadsData $i]
  			}

  			incr index 5
  			if { [string is double [lindex $edgeLoadsData $index]] } {
	  			for {set i $index} {$i < [expr $index + 4]} {incr i} {
	  				lappend edgeLoad [lindex $edgeLoadsData $i]
	  			}
	  			incr index 4
			} else {
				for {set i $index} {$i < [expr $index + 2]} {incr i} {
	  				lappend edgeLoad [lindex $edgeLoadsData $i]
	  			}
	  			incr index 2
			}

  			set numNodes [lindex $edgeLoadsData $index]
  			lappend edgeLoad $numNodes

  			for {set i 0} {$i < $numNodes} {incr i} {
  				incr index
  				lappend edgeLoad [lindex $edgeLoadsData $index]
  			}

  			incr index
  			
  			lappend EdgeLoadsFromFemixFile $edgeLoad
  		}
	}
}



