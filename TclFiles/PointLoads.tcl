# PointLoads.tcl

proc AddPointLoads { loadCaseNumber } {

	global PointLoadsList

	set conditions [GiD_Info Conditions -interval $loadCaseNumber _Point_Loads Mesh]
	set pointLoads [list $loadCaseNumber]

	foreach var $conditions {
		lappend pointLoads [lindex $var 1]
	}

	lappend PointLoadsList $pointLoads

    # if { $loadCaseNumber > 1 } {
    # 	RemoveInternalPointLoadsInsideLinearElement 8 13
    # }

	return 0
}

proc RemovePointLoads { node } {

	global PointLoadsList

	for {set i 0} {$i < [llength $PointLoadsList]} {incr i} {
		set pointLoads [lindex $PointLoadsList $i]
		for {set j 1} {$j < [llength $pointLoads]} {incr j} {
			if { [lindex $pointLoads $j] == $node } {
				set pointLoads [lreplace $pointLoads $j $j]
			}					
		}
		set PointLoadsList [lreplace $PointLoadsList $i $i $pointLoads]		
	}	

	return 0
}

proc InternalPointLoadsInsideQualElement { element node1 node2 node3 node4 } {

	global PointLoadsList

	for {set i 0} {$i < [llength $PointLoadsList]} {incr i} {
		set pointLoads [lindex $PointLoadsList $i]
		for {set j 1} {$j < [llength $pointLoads]} {incr j} {
			set node [lindex $pointLoads $j]
			if { [PointInsideQuadElement $node $node1 $node2 $node3 $node4] } {
				CreateInternalPointLoads $node $element
				RemovePointLoads $node
			}					
		}
	}	

	# global PROBLEMTYPEPATH
    # set result [open "$PROBLEMTYPEPATH\\result.dat" "w"]
    # puts $result "NODE: $node"
    # puts $result $PointLoadsList
    # close $result

	return 0
}

proc PointInsideQuadElement { node node1 node2 node3 node4 } {

	set coordinates  [GiD_Info Coordinates $node Mesh]
    set coordinates1 [GiD_Info Coordinates $node1 Mesh]
    set coordinates2 [GiD_Info Coordinates $node2 Mesh]
    set coordinates3 [GiD_Info Coordinates $node3 Mesh]
    set coordinates4 [GiD_Info Coordinates $node4 Mesh]

    set x [lindex [lindex $coordinates 0] 0]
    set x1 [lindex [lindex $coordinates1 0] 0]
    set x2 [lindex [lindex $coordinates2 0] 0]
    set x3 [lindex [lindex $coordinates3 0] 0]
    set x4 [lindex [lindex $coordinates4 0] 0]

    set y [lindex [lindex $coordinates 0] 1]
    set y1 [lindex [lindex $coordinates1 0] 1]
    set y2 [lindex [lindex $coordinates2 0] 1]
    set y3 [lindex [lindex $coordinates3 0] 1]
    set y4 [lindex [lindex $coordinates4 0] 1]

    set z [lindex [lindex $coordinates 0] 2]
    set z1 [lindex [lindex $coordinates1 0] 2]
    set z2 [lindex [lindex $coordinates2 0] 2]
    set z3 [lindex [lindex $coordinates3 0] 2]
    set z4 [lindex [lindex $coordinates4 0] 2]

    # set insideSurface [expr double(($x-$x1)*(($y2-$y1)*($z3-$z1)-($z2-$z1)*($y3-$y1)) + \
	   #                              ($y-$y1)*(($z2-$z1)*($x3-$x1)-($x2-$x1)*($z3-$z1)) + \
	   #                              ($z-$z1)*(($x2-$x1)*($y3-$y1)-($y2-$y1)*($x3-$x1)))]

    # if { double($insideSurface != 0) } {
    # 	return false
    # }

    if { ![PointInsideSurface $x $y $z $x1 $y1 $z1 $x2 $y2 $z2 $x3 $y3 $z3] } {
    	return false
    }

    if { ![PointInsideLine $x $y $z $x1 $y1 $z1 $x2 $y2 $z2] && ![PointInsideLine $x $y $z $x4 $y4 $z4 $x3 $y3 $z3] } {
    	if { ![PointBetweenLines $x $y $z $x1 $y1 $z1 $x2 $y2 $z2 $x4 $y4 $z4 $x3 $y3 $z3] } {
    		return false
    	}
    }

    if { ![PointInsideLine $x $y $z $x1 $y1 $z1 $x4 $y4 $z4] && ![PointInsideLine $x $y $z $x2 $y2 $z2 $x3 $y3 $z3] } {
    	if { ![PointBetweenLines $x $y $z $x1 $y1 $z1 $x4 $y4 $z4 $x2 $y2 $z2 $x3 $y3 $z3] } {
    		return false
    	}
    }

    return true
}

proc PointInsideHexaElement { node node1 node2 node3 node4 node5 node6 node7 } {

	set coordinates  [GiD_Info Coordinates $node Mesh]
    set coordinates1 [GiD_Info Coordinates $node1 Mesh]
    set coordinates2 [GiD_Info Coordinates $node2 Mesh]
    set coordinates3 [GiD_Info Coordinates $node3 Mesh]
    set coordinates4 [GiD_Info Coordinates $node4 Mesh]
    set coordinates5 [GiD_Info Coordinates $node5 Mesh]
    set coordinates6 [GiD_Info Coordinates $node6 Mesh]
    set coordinates7 [GiD_Info Coordinates $node7 Mesh]

    set x [lindex [lindex $coordinates 0] 0]
    set x1 [lindex [lindex $coordinates1 0] 0]
    set x2 [lindex [lindex $coordinates2 0] 0]
    set x3 [lindex [lindex $coordinates3 0] 0]
    set x4 [lindex [lindex $coordinates4 0] 0]
    set x5 [lindex [lindex $coordinates5 0] 0]
    set x6 [lindex [lindex $coordinates6 0] 0]
    set x7 [lindex [lindex $coordinates7 0] 0]

    set y [lindex [lindex $coordinates 0] 1]
    set y1 [lindex [lindex $coordinates1 0] 1]
    set y2 [lindex [lindex $coordinates2 0] 1]
    set y3 [lindex [lindex $coordinates3 0] 1]
    set y4 [lindex [lindex $coordinates4 0] 1]
    set y5 [lindex [lindex $coordinates5 0] 1]
    set y6 [lindex [lindex $coordinates6 0] 1]
    set y7 [lindex [lindex $coordinates7 0] 1]

    set z [lindex [lindex $coordinates 0] 2]
    set z1 [lindex [lindex $coordinates1 0] 2]
    set z2 [lindex [lindex $coordinates2 0] 2]
    set z3 [lindex [lindex $coordinates3 0] 2]
    set z4 [lindex [lindex $coordinates4 0] 2]
    set z5 [lindex [lindex $coordinates5 0] 2]
    set z6 [lindex [lindex $coordinates6 0] 2]
    set z7 [lindex [lindex $coordinates7 0] 2]

    if { ![PointInsideSurface $x $y $z $x1 $y1 $z1 $x2 $y2 $z2 $x3 $y3 $z3] && ![PointInsideSurface $x $y $z $x5 $y5 $z5 $x6 $y6 $z6 $x7 $y7 $z7] } {
    	if { ![PointBetweenSurfaces $x $y $z $x1 $y1 $z1 $x2 $y2 $z2 $x3 $y3 $z3 $x5 $y5 $z5 $x6 $y6 $z6 $x7 $y7 $z7] } {
    		return false
    	}
    }

    if { ![PointInsideSurface $x $y $z $x1 $y1 $z1 $x4 $y4 $z4 $x5 $y5 $z5] && ![PointInsideSurface $x $y $z $x2 $y2 $z2 $x3 $y3 $z3 $x6 $y6 $z6] } {
    	if { ![PointBetweenSurfaces $x $y $z $x1 $y1 $z1 $x4 $y4 $z4 $x5 $y5 $z5 $x2 $y2 $z2 $x3 $y3 $z3 $x6 $y6 $z6] } {
    		return false
    	}
    }

    if { ![PointInsideSurface $x $y $z $x1 $y1 $z1 $x2 $y2 $z2 $x5 $y5 $z5] && ![PointInsideSurface $x $y $z $x3 $y3 $z3 $x4 $y4 $z4 $x7 $y7 $z7] } {
    	if { ![PointBetweenSurfaces $x $y $z $x1 $y1 $z1 $x2 $y2 $z2 $x5 $y5 $z5 $x3 $y3 $z3 $x4 $y4 $z4 $x7 $y7 $z7] } {
    		return false
    	}
    }

    return true
}

proc PointInsideSurface { x y z x1 y1 z1 x2 y2 z2 x3 y3 z3 } {

    set insideSurface [expr double(($x-$x1)*(($y2-$y1)*($z3-$z1)-($z2-$z1)*($y3-$y1)) + \
	                               ($y-$y1)*(($z2-$z1)*($x3-$x1)-($x2-$x1)*($z3-$z1)) + \
	                               ($z-$z1)*(($x2-$x1)*($y3-$y1)-($y2-$y1)*($x3-$x1)))]

    if { double($insideSurface == 0) } {
    	return true
    }

    return false
}

proc PointBetweenSurfaces { x y z x1 y1 z1 x2 y2 z2 x3 y3 z3 x5 y5 z5 x6 y6 z6 x7 y7 z7 } {

	set normalVector1 [GetSurfaceNormalVector $x $y $z $x1 $y1 $z1 $x2 $y2 $z2 $x3 $y3 $z3]
	set normalVector2 [GetSurfaceNormalVector $x $y $z $x5 $y5 $z5 $x6 $y6 $z6 $x7 $y7 $z7]

	set betweenSurfaces [expr double([lindex $normalVector1 0] * [lindex $normalVector2 0] + \
	                                 [lindex $normalVector1 1] * [lindex $normalVector2 1] + \
	                                 [lindex $normalVector1 2] * [lindex $normalVector2 2])]

	if { double($betweenSurfaces < 0) } {
		return true
	}

	return false
}

proc PointBetweenLines { x y z x1 y1 z1 x2 y2 z2 x4 y4 z4 x3 y3 z3 } {

	set normalVector1 [GetLineNormalVector $x $y $z $x1 $y1 $z1 $x2 $y2 $z2]
	set normalVector2 [GetLineNormalVector $x $y $z $x4 $y4 $z4 $x3 $y3 $z3]

  #   	global PROBLEMTYPEPATH
	 #    set result [open "$PROBLEMTYPEPATH\\result.dat" "w"]
		# puts $result "NormalVectors:"
		# puts $result "y: $y"
		# puts $result "z: $z"
		# puts $result "x1: $x1"
		# puts $result "y1: $y1"
		# puts $result "z1: $z1"
		# puts $result "x2: $x2"
		# puts $result "y2: $y2"
		# puts $result "z2: $z2"
		# puts $result "x4: $x4"
		# puts $result "y4: $y4"
		# puts $result "z4: $z4"
		# puts $result "x3: $x3"
		# puts $result "y3: $y3"
		# puts $result "z3: $z3"
		# puts $result $normalVector1
		# puts $result $normalVector2
	 #    close $result


	set betweenLines [expr double([lindex $normalVector1 0] * [lindex $normalVector2 0] + \
	                              [lindex $normalVector1 1] * [lindex $normalVector2 1] + \
	                              [lindex $normalVector1 2] * [lindex $normalVector2 2])]

	if { double($betweenLines < 0) } {
		return true
	}

	return false
}

proc GetSurfaceNormalVector { x y z x1 y1 z1 x2 y2 z2 x3 y3 z3 } {

	set a11 [expr double(($y2 - $y1) * ($z3 - $z1) - ($z2 - $z1) * ($y3 - $y1))]
	set a12 [expr double(($z2 - $z1) * ($x3 - $x1) - ($x2 - $x1) * ($z3 - $z1))]
	set a13 [expr double(($x2 - $x1) * ($y3 - $y1) - ($y2 - $y1) * ($x3 - $x1))]
	set b1  [expr double($a11 * $x1 + $a12 * $y1 + $a13 * $z1)]	

	set a21 [expr double(($z2 - $z1) * ($x3 - $x1) - ($z3 - $z1) * ($x2 - $x1))]
	set a22 [expr double(($z2 - $z1) * ($y3 - $y1) - ($y2 - $y1) * ($z3 - $z1))]
	set a23 0
	set b2  [expr double($a21 * $x + $a22 * $y)]	

	set a31 [expr double(($x2 - $x1) * ($y3 - $y1) - ($y2 - $y1) * ($x3 - $x1))]
	set a32 0
	set a33 [expr double(($z2 - $z1) * ($y3 - $y1) - ($y2 - $y1) * ($z3 - $z1))]
	set b3  [expr double($a31 * $x + $a33 * $z)]

	if { double($a21 == 0) && double($a31 == 0) } {
		set px [expr double(($b1 - $a12 * $y - $a13 * $z) / $a11)]
		set py $y
		set pz $z
	} elseif { double($a22 == 0) && double($a31 == 0) } {
		set px $x
		set py [expr double(($b1 - $a11 * $x - $a13 * $z) / $a12)]
		set pz $z
	} elseif { double($a21 == 0) && double($a22 == 0) } {
		set px $x
		set py $y
		set pz [expr double(($b1 - $a11 * $x - $a12 * $y) / $a13)]
	} else {
		set detA [DetMatrix $a11 $a12 $a13 $a21 $a22 $a23 $a31 $a32 $a33]
		if { double($detA != 0) } {
			set px [expr double([DetMatrix $b1 $a12 $a13 $b2 $a22 $a23 $b3 $a32 $a33] / $detA)]
			set py [expr double([DetMatrix $a11 $b1 $a13 $a21 $b2 $a23 $a31 $b3 $a33] / $detA)]
			set pz [expr double([DetMatrix $a11 $a12 $b1 $a21 $a22 $b2 $a31 $a32 $b3] / $detA)]
		}
	}

	return [list [expr double($x - $px)] \
	             [expr double($y - $py)] \
	             [expr double($z - $pz)]]

}

proc GetLineNormalVector { x y z x1 y1 z1 x2 y2 z2 } {

	# set normalVector [list 0 0 0]

	set a11 [expr double($x2 - $x1)]
	set a12 [expr double($y2 - $y1)]
	set a13 [expr double($z2 - $z1)]
	set b1  [expr double($x * $a11 + $y * $a12 + $z * $a13)]

	set a21 [expr double($y2 - $y1)]
	set a22 [expr double($x1 - $x2)]
	set a23 0
	set b2  [expr double($x1 * $a21 + $y1 * $a22)]	

	set a31 [expr double($z2 - $z1)]
	set a32 0
	set a33 [expr double($x1 - $x2)]
	set b3  [expr double($x1 * $a31 + $z1 * $a33)]	

	if { double($a12 == 0) && double($a13 == 0) } {
		set px $x
		set py $y1
		set pz $z1
	} elseif { double($a11 == 0) && double($a13 == 0) } {
		set px $x1
		set py $y
		set pz $z1
	} elseif { double($a11 == 0) && double($a12 == 0) } {
		set px $x1
		set py $y1
		set pz $z
	} else {
		set detA [DetMatrix $a11 $a12 $a13 $a21 $a22 $a23 $a31 $a32 $a33]
		if { double($detA != 0) } {
			set px [expr double([DetMatrix $b1 $a12 $a13 $b2 $a22 $a23 $b3 $a32 $a33] / $detA)]
			set py [expr double([DetMatrix $a11 $b1 $a13 $a21 $b2 $a23 $a31 $b3 $a33] / $detA)]
			set pz [expr double([DetMatrix $a11 $a12 $b1 $a21 $a22 $b2 $a31 $a32 $b3] / $detA)]
		}
	}

	return [list [expr double($x - $px)] \
	             [expr double($y - $py)] \
	             [expr double($z - $pz)]]

	# return $normalVector
}

proc DetMatrix { a11 a12 a13 a21 a22 a23 a31 a32 a33 } {

	return [expr double($a11 * ($a22 * $a33 - $a23 * $a32) + \
		                $a12 * ($a23 * $a31 - $a21 * $a33) + \
		                $a13 * ($a21 * $a32 - $a22 * $a31))]
}

proc PointInsideLine { x y z x1 y1 z1 x2 y2 z2 } {

	set k {}
	set insideLine true

	if { double($x2 == $x1) } {
		if { double($x != $x1) } {
			set insideLine false
		} 
	} else {
		lappend k [expr double(($x - $x1) / ($x2 - $x1))]
	}

	if { double($y2 == $y1) } {
		if { double($y != $y1) } {
			set insideLine false
		} 
	} else {
		lappend k [expr double(($y - $y1) / ($y2 - $y1))]
	}

	if { double($z2 == $z1) } {
		if { double($z != $z1) } {
			set insideLine false
		} 
	} else {
		lappend k [expr double(($z - $z1) / ($z2 - $z1))]
	}

	if { $insideLine } {
		for {set i 1} {$i < [llength $k]} {incr i} {
			if { double([lindex $k $i] != [lindex $k 0]) } {
				return false
			}
		}
	}

	return $insideLine
}

proc CreateInternalPointLoads { node element } {

	global PointLoadsList

    set numLoadCases [lindex [GiD_Info intvdata num] 1]
    set currLoadCase [lindex [GiD_Info intvdata num] 0]
    
    for {set i 1} {$i <= $numLoadCases} {incr i} {
    	set conditions [GiD_Info Conditions -interval $i _Point_Loads Mesh]
		foreach var $conditions {
	    	if { [lindex $var 1] == $node } {
	    		GiD_Process Mescape Data Intervals ChangeInterval $i escape
   				GiD_Process Mescape Data \
        		Conditions AssignCond _Point_Loads Change \
	        		[lindex $var 3] \
	        		[lindex $var 4] \
	        		[lindex $var 5] \
	        		[lindex $var 6] \
	        		[lindex $var 7] \
	        		[lindex $var 8] \
	        		1 $element \
	        		$node \
	        	Mescape
	    	}
		}
    }
	GiD_Process Mescape Data Intervals ChangeInterval $currLoadCase escape
}

proc InternalPointLoadsInsideHexaElement { element node1 node2 node3 node4 node5 node6 node7 } {

	global PointLoadsList

	for {set i 0} {$i < [llength $PointLoadsList]} {incr i} {
		set pointLoads [lindex $PointLoadsList $i]
		for {set j 1} {$j < [llength $pointLoads]} {incr j} {
			set node [lindex $pointLoads $j]
			if { [PointInsideHexaElement $node $node1 $node2 $node3 $node4 $node5 $node6 $node7] } {
				CreateInternalPointLoads $node $element
				RemovePointLoads $node
			}					
		}
	}	

	return 0
}

# proc RemoveInternalPointLoadsInsideLinearElement { node1 node2 } {

# 	global PointLoadsList

# 	if { ![PointLoadsListIsEmpty] } {

# 		# GiD_Process Mescape Geometry Create NurbsLine \
# 		# 	0 0 0 new \
# 		# 	10 10 0 new \
# 		# escape escape escape escape


# 		# GiD_Process Mescape Geometry Create NurbsLine \
#   #       	[lindex [GiD_Info Coordinates $node1 Mesh] 0] [lindex [GiD_Info Coordinates $node1 Mesh] 1] [lindex [GiD_Info Coordinates $node1 Mesh] 2] new \
#   #       	[lindex [GiD_Info Coordinates $node2 Mesh] 0] [lindex [GiD_Info Coordinates $node2 Mesh] 1] [lindex [GiD_Info Coordinates $node2 Mesh] 2] new \
#   #       Mescape
# 		set lineNum [expr [GiD_Info Geometry MaxNumLines] + 1]



# 		# GiD_Geometry create line $lineNum nurbsline "Group1" [GiD_Info Coordinates $node1 Mesh] [GiD_Info Coordinates $node2 Mesh]
# 	}

# 	return 0	
# }

proc ExistsInternalPointLoads {} {

	global PointLoadsList

	foreach var $PointLoadsList {
		if { [llength $var] > 1 } {
			return 1
		}
	}
	return 0
}