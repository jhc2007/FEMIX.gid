    
proc NumOfLinIntLine3DInLayerProperties {} {

    global layerPropertiesList LinIntLine3DUsedInLayerProp

    set LinIntLine3DUsedInLayerProp {}

    for {set i 0} {$i < [llength $layerPropertiesList]} {incr i} { 
        if { [lindex [lindex $layerPropertiesList $i] 1] == "LIN_INT_LINE_3D" && ![ExistsLinIntLine3DInLayerProperties [lindex [lindex $layerPropertiesList $i] 2]] } {
            lappend LinIntLine3DUsedInLayerProp [lindex [lindex $layerPropertiesList $i] 2]
        }
    }

    return [llength $LinIntLine3DUsedInLayerProp]    
}

proc ExistsLinIntLine3DInLayerProperties { matName } {

    global LinIntLine3DUsedInLayerProp

    if { [ExistsListElement $LinIntLine3DUsedInLayerProp [ReplaceSpaceWithUnderscore $matName]] } {
        return 1
    }
    return 0
}

proc WriteLinIntLine3DInLayerProperties { index } {

    global LinIntLine3DUsedInLayerProp

    set linIntLine3D ""
    set materials [GiD_Info materials]

    #global PROJECTPATH
    #set result [open "$PROJECTPATH\\aaa_result.dat" "w"]
    ##puts $result $materials
    #set mat [GiD_Info materials [lindex $materials 5]]
    ##puts $result $mat
    #for {set i 1} {$i < [llength $mat]} {incr i} {
    # 	puts $result [lindex $mat $i]
    #}
    #close $result
    #return

    for {set i 0} {$i < [llength $materials]} {incr i} {

    	set material [GiD_Info materials [lindex $materials $i]]
        if { [lindex $material 2] == "LIN_INT_LINE_3D" && [ExistsLinIntLine3DInLayerProperties [ReplaceSpaceWithUnderscore [lindex $materials $i]]] } {

        	append linIntLine3D "[format %7d $index]  "
        	append linIntLine3D [ReplaceSpaceWithUnderscore [lindex $materials $i]]
        	for {set j 4} {$j <= 8} {incr j 2} {
                # append linIntLine3D [format %18.8e [RemoveUnitsFromParameterValue [lindex $material $j]]]
                append linIntLine3D [format %18.8e [lindex $material $j]]
            }
            append linIntLine3D " ;\n"

        	set index [expr $index + 1]
        }
    }
    return $linIntLine3D
}
