
proc CreateSurfaceSpringsList {} {

  	global surfaceSpringsConditions

  	set surfaceSpringsConditions [lsort -dictionary [GiD_Info Conditions _Surface_Springs Mesh]]

  	if { [llength $surfaceSpringsConditions] == 0 } {
  	    return
  	}

  	global SurfaceSpringsList 
  	       # SpringMaterialsList \
  	       # springVectorsList \
  	       # SpringNumericalIntegrationList 
   
   	# set surfaceSpringsList {}

    AddSurfaceSprings [lindex $surfaceSpringsConditions 0]

  	for {set i 1} {$i < [llength $surfaceSpringsConditions]} {incr i} {
  	  	if { ![UpdateSurfaceSprings [lindex $surfaceSpringsConditions [expr $i - 1]] [lindex $surfaceSpringsConditions $i]] } {
  	        AddSurfaceSprings [lindex $surfaceSpringsConditions $i]
  		}
    } 
}

proc EqualSurfaceSpringConditions { cond1 cond2 } {

    for {set i 3} {$i < 5} {incr i} {
        if { [lindex $cond1 $i] != [lindex $cond2 $i] } {
            return false
        }
    }

    switch [lindex $cond1 4] {
        "Global" {
            if { [lindex $cond1 5] != [lindex $cond2 5] } {
                return false
            }
        }
        "Solid/Shell" {
            if { [lindex $cond1 6] != [lindex $cond2 6] } {
                return false
            }
        }
        "Specified" {
            for {set i 7} {$i < 10} {incr i} {
                if { [lindex $cond1 $i] != [lindex $cond2 $i] } {
                    return false
                }
            }
        }
    }

    for {set i 10} {$i < 16} {incr i} {
        if { [lindex $cond1 $i] != [lindex $cond2 $i] } {
            return false
        }
    }

    set numNodes [lindex $cond1 15]

    if { $numNodes == "All" } {
        if { [lindex $cond1 16] != [lindex $cond2 16] } {
            return false
        }
    } else {
        return false
    }
    
    return true
}

proc UpdateSurfaceSprings { cond1 cond2 } {

    global SurfaceSpringsList

    if { [lindex $cond1 0] == "E" && [lindex $cond1 0] == "E" } {
        if { [lindex $cond2 1] != [expr [lindex $cond1 1] + 1] } {
            return false
        }
    } elseif { [lindex $cond1 0] != "E" && [lindex $cond1 0] != "E" } {
        if { [lindex $cond2 0] != [expr [lindex $cond1 0] + 1] || [lindex $cond2 1] != [lindex $cond1 1] } {
            return false
        }
    } else {
        return false
    }

    if { ![EqualSurfaceSpringConditions $cond1 $cond2] } {
        return false
    }

    set lastSurfaceSpring [lindex $SurfaceSpringsList end]

    if { [lindex $cond2 0] == "E" } {
        set lastSurfaceSpring [lreplace $lastSurfaceSpring 1 1 [lindex $cond2 1]]
    } else {
        set lastSurfaceSpring [lreplace $lastSurfaceSpring 1 1 [lindex $cond2 0]]
    }

    set SurfaceSpringsList [lreplace $SurfaceSpringsList end end $lastSurfaceSpring]
    
    return true
}

proc AddSurfaceSprings { conditions } {

  	global SurfaceSpringsList \
  	       AuxiliaryVectorsList \
  	       SpringNumericalIntegrationList \
           SpringMaterialsList 
           # NUMERICALINTEGRATIONLIST

  	set surfaceSpring {}

  	if { [lindex $conditions 0] == "E" } {
        set elemNum [lindex $conditions 1]
        set faceNodes [lrange [GiD_Info Mesh Elements Quadrilateral $elemNum] 1 end-1]
        if { [llength $faceNodes] == 8 || [llength $faceNodes] == 9 } {
            set faceNodes [GetQuadElemNodesInFemixOrder $faceNodes]
        }
    		lappend surfaceSpring $elemNum
    		lappend surfaceSpring $elemNum
    		lappend surfaceSpring 1
  	} else {
        set elemNum [lindex $conditions 0]
        set elemNodes [lrange [GiD_Info Mesh Elements Hexahedra $elemNum] 1 end-1]
        if { [llength $elemNodes] == 20 } {
            set elemNodes [GetHexaElemNodesInFemixOrder $elemNodes]
        }
        set face [GetFemixFace [lindex $conditions 1]]
        set faceNodes [GetFaceNodes $elemNodes $face]
    		lappend surfaceSpring $elemNum
    		lappend surfaceSpring $elemNum
    		lappend surfaceSpring $face
  	}

    set groupName [lindex $conditions 3]

    if { ![ExistsListElement [GiD_Info layers] $groupName] && ![ExistsListElement [GiD_Groups list] $groupName] } {
        GiD_Process 'Groups Create $groupName Mescape
    }

  	lappend surfaceSpring $groupName
  	lappend surfaceSpring [lindex $conditions 4]

  	switch [lindex $conditions 4] {
  		"Global" {
              lappend surfaceSpring [lindex $conditions 5]
          }
          "Solid/Shell" {
              lappend surfaceSpring [lindex $conditions 6]
          }
          "Specified" {
  	        for {set i 7} {$i < 10} {incr i} {
  	            lappend surfaceSpring [lindex $conditions $i]
  	        }
              AddAuxiliaryVector [lindex $conditions 7] [lindex $conditions 8] [lindex $conditions 9]
          }
  	}

	  lappend surfaceSpring [lindex $conditions 10]

    switch [lindex $conditions 11] {
        "GAUSS-LEGENDRE_" {
            lappend surfaceSpring "_GLEG"
        }
        "GAUSS-LOBATTO" {
            lappend surfaceSpring "_GLOB"
        }
        "NEWTON-COTES" {
            lappend surfaceSpring "_NCOTES"
        }
    }

    lappend surfaceSpring "SPRING_NUMERICAL_INTEGRATION_[GetSpringNumericalIntegrationIndex [list [lindex $conditions 12] [lindex $conditions 13]]]"

    set materialType [lindex $conditions 14]
    lappend surfaceSpring $materialType

    set numNodes [lindex $conditions 15]
    
    if { $numNodes == "All" } {
        lappend surfaceSpring "1"
        set materialName [lindex $conditions 16]
        lappend surfaceSpring $materialName
        if { ![ExistsListElement $SpringMaterialsList [list $materialType $materialName]] } {
            lappend SpringMaterialsList [list $materialType $materialName]
        }
    } else {
        lappend surfaceSpring $numNodes
        set geometryNodes {}
        set materialNames {}
        for {set i 0} {$i < $numNodes} {incr i} {
            lappend materialNames [lindex $conditions [expr 17 + 2 * $i]]
            lappend geometryNodes [lindex $conditions [expr 18 + 2 * $i]]
            if { ![ExistsListElement $SpringMaterialsList [list $materialType [lindex $materialNames $i]]] } {
                lappend SpringMaterialsList [list $materialType [lindex $materialNames $i]]
            }
        }



        while { [llength $faceNodes] > 0 &&  [llength $geometryNodes] > 0 } {
            set index 0
            set elemNodeCoord [lindex [GiD_Info Coordinates [lindex $faceNodes $index] Mesh] 0]
            set geomNodeCoord [lindex [GiD_Info Coordinates [lindex $geometryNodes $index] Geometry] 0]
            while { $index < [llength $geometryNodes] && (double([lindex $elemNodeCoord 0] != [lindex $geomNodeCoord 0]) || double([lindex $elemNodeCoord 1] != [lindex $geomNodeCoord 1]) || double([lindex $elemNodeCoord 2] != [lindex $geomNodeCoord 2])) } {
                incr index
                set geomNodeCoord [lindex [GiD_Info Coordinates [lindex $geometryNodes $index] Geometry] 0]
            }
            if { $index < [llength $geometryNodes] } {
                lappend surfaceSpring [lindex $materialNames $index]
                set geometryNodes [lreplace $geometryNodes $index $index]
                set materialNames [lreplace $materialNames $index $index]
            }
            set faceNodes [lreplace $faceNodes 0 0]
        }
        # lappend surfaceSpring [lindex $materialNames 0]
        
    }

    lappend SurfaceSpringsList $surfaceSpring
}

proc WriteSurfaceSpringsList {} {

    global SurfaceSpringsList \
           numDimension 

    set springsList ""
    set index 1

    for {set i 0} {$i < [llength $SurfaceSpringsList]} {incr i} {

        set surfaceSpring [lindex $SurfaceSpringsList $i]

        if { [lindex $surfaceSpring 0] == [lindex $surfaceSpring 1] } {
            append springsList [format %7d $index]
            append springsList "  [lindex $surfaceSpring 3]  1  "
            append springsList "[lindex $surfaceSpring 0]  "
            append springsList "[lindex $surfaceSpring 2]  "
            incr index
        } else {
            append springsList "  \[$index\-[expr $index + [lindex $surfaceSpring 1] - [lindex $surfaceSpring 0]]\]  "
            append springsList "[lindex $surfaceSpring 3]  1  "
            append springsList "\[[lindex $surfaceSpring 0]\-[lindex $surfaceSpring 1]\]  "
            append springsList "[lindex $surfaceSpring 2]  "
            set index [expr $index + [lindex $surfaceSpring 1] - [lindex $surfaceSpring 0] + 1]
        }

        if { [lindex $surfaceSpring 4] == "Specified" } {

            # set vector [list [lindex $surfaceSpring 5] [lindex $surfaceSpring 6] [lindex $surfaceSpring 7]]

            append springsList "VECTOR_[GetAuxiliaryVectorIndexFromComponents [lindex $surfaceSpring 5] [lindex $surfaceSpring 6] [lindex $surfaceSpring 7]]  "
            if { [lindex $surfaceSpring 8] == "Structure_to_Ground" } {
                append springsList "_SG  "
            } else {
                append springsList "_GS  "
            }

            append springsList "[lindex $surfaceSpring 9]  "

            append springsList "[lindex $surfaceSpring 10]  "
            append springsList "_[lindex $surfaceSpring 11]  "

            for {set j 12} {$j < [llength $surfaceSpring]} {incr j} {
                append springsList "[lindex $surfaceSpring $j]  "
            }

            append springsList ";\n"

        } else {

            if { [lindex $surfaceSpring 4] == "Global" } {

                if { $numDimension == 3 } {
                    append springsList "_[lindex $surfaceSpring 5]  "
                } else {
                    append springsList "_XG[expr [string index [lindex $surfaceSpring 5] 2] % 3 + 1]  "
                }
            
            } else {

            	if { [lindex $surfaceSpring 5] == "N" } {
            		append springsList "_N  "
            	} else {
	            	if { $numDimension == 3 } {
	            		append springsList "_[lindex $surfaceSpring 5]  "
	        		} else {
	        			append springsList "_L[expr [string index [lindex $surfaceSpring 5] 1] + 1]  "
	        		}
            	}
            }

            if { [lindex $surfaceSpring 6] == "Structure_to_Ground" } {
                append springsList "_SG  "
            } else {
                append springsList "_GS  "
            }

            append springsList "[lindex $surfaceSpring 7]  "

            append springsList "[lindex $surfaceSpring 8]  "
            append springsList "_[lindex $surfaceSpring 9]  "

            for {set j 10} {$j < [llength $surfaceSpring]} {incr j} {
                append springsList "[lindex $surfaceSpring $j]  "
            }

            append springsList ";\n"
        }
    }
    return $springsList
}

proc CountSurfaceSprings {} {

	global SurfaceSpringsList

    set count 0
    for {set i 0} {$i < [llength $SurfaceSpringsList]} {incr i} {
        set count [expr $count + [lindex [lindex $SurfaceSpringsList $i] 1] - [lindex [lindex $SurfaceSpringsList $i] 0] + 1]
    }

    return $count
}

# proc GetQuadElemNodesInFemixOrder { gidNodes } {

#     set femixNodes {}

#     for {set i 0} {$i < 8} {incr i} {
#         if { [expr $i % 2] == 0 } {
#             lappend femixNodes [lindex $gidNodes [expr $i / 2]]
#         } else {
#             lappend femixNodes [lindex $gidNodes [expr (7 + $i) / 2]]
#         }
#     }

#     return $femixNodes
# }

proc GetQuadElemNodesInFemixOrder { gidNodes } {

    # 8 and 9 Nodes Quad Elements
    for {set i 0} {$i < 4} {incr i} {
        lappend femixNodes [lindex $gidNodes $i]
        lappend femixNodes [lindex $gidNodes [expr 4 + $i]]
    }

    # 9 Nodes Quad Element
    if { [llength $gidNodes] == 9 } {
        lappend femixNodes [lindex $gidNodes end]
    }

    return $femixNodes
}

proc GetFaceNodes { elemNodes face } {

    # Get face nodes for Hexahedra elements in femix order

    if { [llength $elemNodes] == 20 } {
        switch $face {
            1 {
                return [list [lindex $elemNodes 0] \
                             [lindex $elemNodes 1] \
                             [lindex $elemNodes 2] \
                             [lindex $elemNodes 3] \
                             [lindex $elemNodes 4] \
                             [lindex $elemNodes 5] \
                             [lindex $elemNodes 6] \
                             [lindex $elemNodes 7]]   
            }
            2 {
                return [list [lindex $elemNodes 0] \
                             [lindex $elemNodes 1] \
                             [lindex $elemNodes 2] \
                             [lindex $elemNodes 9] \
                             [lindex $elemNodes 14] \
                             [lindex $elemNodes 13] \
                             [lindex $elemNodes 12] \
                             [lindex $elemNodes 8]]  
            }
            3 {
                return [list [lindex $elemNodes 2] \
                             [lindex $elemNodes 3] \
                             [lindex $elemNodes 4] \
                             [lindex $elemNodes 10] \
                             [lindex $elemNodes 16] \
                             [lindex $elemNodes 15] \
                             [lindex $elemNodes 14] \
                             [lindex $elemNodes 9]] 
            }
            4 {
                return [list [lindex $elemNodes 4] \
                             [lindex $elemNodes 5] \
                             [lindex $elemNodes 6] \
                             [lindex $elemNodes 11] \
                             [lindex $elemNodes 18] \
                             [lindex $elemNodes 17] \
                             [lindex $elemNodes 16] \
                             [lindex $elemNodes 10]] 
            }
            5 {
                return [list [lindex $elemNodes 6] \
                             [lindex $elemNodes 7] \
                             [lindex $elemNodes 0] \
                             [lindex $elemNodes 8] \
                             [lindex $elemNodes 12] \
                             [lindex $elemNodes 19] \
                             [lindex $elemNodes 18] \
                             [lindex $elemNodes 11]] 
            }
            6 {
                return [list [lindex $elemNodes 12] \
                             [lindex $elemNodes 13] \
                             [lindex $elemNodes 14] \
                             [lindex $elemNodes 15] \
                             [lindex $elemNodes 16] \
                             [lindex $elemNodes 17] \
                             [lindex $elemNodes 18] \
                             [lindex $elemNodes 19]] 
            }
        }
    } else {
        switch $face {
            1 {
                return [list [lindex $elemNodes 0] \
                             [lindex $elemNodes 1] \
                             [lindex $elemNodes 2] \
                             [lindex $elemNodes 3]]   
            }
            2 {
                return [list [lindex $elemNodes 0] \
                             [lindex $elemNodes 1] \
                             [lindex $elemNodes 5] \
                             [lindex $elemNodes 4]]  
            }
            3 {
                return [list [lindex $elemNodes 1] \
                             [lindex $elemNodes 2] \
                             [lindex $elemNodes 6] \
                             [lindex $elemNodes 5]] 
            }
            4 {
                return [list [lindex $elemNodes 2] \
                             [lindex $elemNodes 3] \
                             [lindex $elemNodes 7] \
                             [lindex $elemNodes 6]] 
            }
            5 {
                return [list [lindex $elemNodes 3] \
                             [lindex $elemNodes 0] \
                             [lindex $elemNodes 4] \
                             [lindex $elemNodes 7]] 
            }
            6 {
                return [list [lindex $elemNodes 4] \
                             [lindex $elemNodes 5] \
                             [lindex $elemNodes 6] \
                             [lindex $elemNodes 7]] 
            }
        }
    }
}