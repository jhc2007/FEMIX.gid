# NLMM101FromFemixFile.tcl
#
# CreateNLMM101 { femixData }

proc CreateNLMM101 { femixData } {

    set nlmm101Data [GetBlockData $femixData "NLMM101"]

    if { [llength $nlmm101Data] > 0 } {

        global doubleFormatMatParameters
        set allMaterialsNameList [GiD_Info materials]
        set nlmm101NameList {}

        for {set i 0} {$i < [llength $allMaterialsNameList]} {incr i} {
            if { [lindex [GiD_Info materials [lindex $allMaterialsNameList $i]] 2] == "NLMM101" } {
                lappend nlmm101NameList [string toupper [lindex $allMaterialsNameList $i]]
            }
        }

        set numMaterials [lindex $nlmm101Data 2]

        set index 4

        for {set i 0} {$i < $numMaterials} {incr i} {

            set materialName [lindex $nlmm101Data [expr $index + 1]]

            set materialData { NLMM101 }

            # lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm101Data [expr $index + 2]]] kg*mm^-3"
            # lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm101Data [expr $index + 3]]] Cel^-1"
            lappend materialData [format $doubleFormatMatParameters [lindex $nlmm101Data [expr $index + 2]]]
            lappend materialData [format $doubleFormatMatParameters [lindex $nlmm101Data [expr $index + 3]]]
            lappend materialData [format $doubleFormatMatParameters [lindex $nlmm101Data [expr $index + 4]]]

            for {set j 5} {$j < 8} {incr j} {
                # lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm101Data [expr $index + $j]]] N*mm^-2"
                lappend materialData [format $doubleFormatMatParameters [lindex $nlmm101Data [expr $index + $j]]]
            }

            set value [RemoveFirstUnderScore [lindex $nlmm101Data [expr $index + 8]]]
            lappend materialData $value

            for {set j 9} {$j < 13} {incr j} {
                lappend materialData [format $doubleFormatMatParameters [lindex $nlmm101Data [expr $index + $j]]]
            }

            switch $value {
                
                "TRILINEAR" {
                    # lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm101Data [expr $index + 13]]] N*mm^-1"
                    # lappend materialData "0 N*mm^-1"
                    lappend materialData [format $doubleFormatMatParameters [lindex $nlmm101Data [expr $index + 13]]]
                    lappend materialData 0
                }
                "CORNELISSEN" {
                    # lappend materialData "0 N*mm^-1"
                    # lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm101Data [expr $index + 13]]] N*mm^-1"
                    lappend materialData 0
                    lappend materialData [format $doubleFormatMatParameters [lindex $nlmm101Data [expr $index + 13]]]
                }
            }

            lappend materialData [format $doubleFormatMatParameters [lindex $nlmm101Data [expr $index + 14]]]

            for {set j 15} {$j < 17} {incr j} {
                lappend materialData [RemoveFirstUnderScore [lindex $nlmm101Data [expr $index + $j]]]
            }

            if { [string is double [lindex $nlmm101Data [expr $index + 17]]] } {
                lappend materialData "VALUE"
                # lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm101Data [expr $index + 17]]] mm"
                lappend materialData [format $doubleFormatMatParameters [lindex $nlmm101Data [expr $index + 17]]]
            } else {
                lappend materialData [RemoveFirstUnderScore [lindex $nlmm101Data [expr $index + 17]]]
                # lappend materialData "0 mm"
                lappend materialData 0
            }

            lappend materialData [lindex $nlmm101Data [expr $index + 18]]
            # lappend materialData "[lindex $nlmm101Data [expr $index + 19]] degree"
            lappend materialData [lindex $nlmm101Data [expr $index + 19]]

            if { [ExistsListElement $nlmm101NameList [string toupper $materialName]] } {
                GiD_ModifyData materials $materialName $materialData
            } else {
                GiD_CreateData create material NLMM101_Concrete $materialName $materialData
            }

            set index [expr $index + 21]
        }
    }
}