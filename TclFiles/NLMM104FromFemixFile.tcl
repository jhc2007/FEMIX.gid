# NLMM104FromFemixFile.tcl

proc CreateNLMM104 { femixData } {

    set nlmm104Data [GetBlockData $femixData "NLMM104"]

    if { [llength $nlmm104Data] > 0 } {

        global doubleFormatMatParameters
        set allMaterialsNameList [GiD_Info materials]
        set nlmm104NameList {}

  # global PROBLEMTYPEPATH
  # set result [open "$PROBLEMTYPEPATH\\result_new.dat" "w"]
  # puts $result "AQUI"
  # puts $result [GiD_Info materials NONLIN_LAY_7D]
  # close $result

        for {set i 0} {$i < [llength $allMaterialsNameList]} {incr i} {
            if { [lindex [GiD_Info materials [lindex $allMaterialsNameList $i]] 2] == "NLMM104" } {
                lappend nlmm104NameList [string toupper [lindex $allMaterialsNameList $i]]
            }
        }

    	set numMaterials [lindex $nlmm104Data 2]

    	set index 4

    	for {set i 0} {$i < $numMaterials} {incr i} {

    		set materialName [lindex $nlmm104Data [expr $index + 1]]

    		set materialData { NLMM104 }

	    	# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 2]]] kg*mm^-3"
	    	# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 3]]] Cel^-1"
            lappend materialData [format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 2]]]
            lappend materialData [format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 3]]]
	    	lappend materialData [format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 4]]]

	    	for {set j 5} {$j < 8} {incr j} {
    			# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + $j]]] N*mm^-2"
                lappend materialData [format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + $j]]]
    		}

    		set value [RemoveFirstUnderScore [lindex $nlmm104Data [expr $index + 8]]]
    		lappend materialData $value

    		for {set j 9} {$j < 15} {incr j} {
    			lappend materialData [format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + $j]]]
    		}

    		switch $value {

    			"TRILINEAR" {
    				# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 15]]] N*mm^-1"
    				# lappend materialData "0 N*mm^-1"
    				# lappend materialData "0 N*mm^-1"
                    lappend materialData [format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 15]]]
                    lappend materialData 0
                    lappend materialData 0
    			}
    			"QUADRILINEAR" {
    				# lappend materialData "0 N*mm^-1"
    				# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 15]]] N*mm^-1"
    				# lappend materialData "0 N*mm^-1"
                    lappend materialData 0
                    lappend materialData [format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 15]]]
                    lappend materialData 0
    			}
    			"CORNELISSEN" {
    				# lappend materialData "0 N*mm^-1"
    				# lappend materialData "0 N*mm^-1"
    				# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 15]]] N*mm^-1"
                    lappend materialData 0
                    lappend materialData 0
                    lappend materialData [format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 15]]]
    			}
    		}
    		
    		lappend materialData [RemoveFirstUnderScore [lindex $nlmm104Data [expr $index + 16]]]

    		if { [string is double [lindex $nlmm104Data [expr $index + 17]]] } {
    			lappend materialData "VALUE"
    			lappend materialData [format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 17]]]
    		} else {
    			lappend materialData [RemoveFirstUnderScore [lindex $nlmm104Data [expr $index + 17]]]
    			lappend materialData 0
    		}

    		lappend materialData [format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 18]]]
    		lappend materialData [RemoveFirstUnderScore [lindex $nlmm104Data [expr $index + 19]]]

    		if { [string is double [lindex $nlmm104Data [expr $index + 20]]] } {
    			lappend materialData "VALUE"
    			lappend materialData [format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 20]]]
    		} else {
    			lappend materialData [RemoveFirstUnderScore [lindex $nlmm104Data [expr $index + 20]]]
    			lappend materialData 0
    		}

    		lappend materialData [RemoveFirstUnderScore [lindex $nlmm104Data [expr $index + 21]]]
    		# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 22]]] N*mm^-2"
    		# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 23]]] N*mm^-1"
            lappend materialData [format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 22]]]
            lappend materialData [format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 23]]]
    		lappend materialData [RemoveFirstUnderScore [lindex $nlmm104Data [expr $index + 24]]]

    		if { [string is double [lindex $nlmm104Data [expr $index + 25]]] } {
    			lappend materialData "VALUE"
    			# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 25]]] mm"
                lappend materialData [format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 25]]]
    		} else {
    			lappend materialData [RemoveFirstUnderScore [lindex $nlmm104Data [expr $index + 25]]]
    			# lappend materialData "0 mm"
                lappend materialData 0
    		}

    		lappend materialData [lindex $nlmm104Data [expr $index + 26]]
    		# lappend materialData "[lindex $nlmm104Data [expr $index + 27]] degree"
            lappend materialData [lindex $nlmm104Data [expr $index + 27]]

    		set value [RemoveFirstUnderScore [lindex $nlmm104Data [expr $index + 28]]]
    		lappend materialData $value
    		# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 29]]] N*mm^-2"
            lappend materialData [format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 29]]]

    		switch $value {

    			"NONE" {
    				# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 30]]] N*mm^-1"
    				# lappend materialData "0 N*mm^-1"
    				# lappend materialData "0 N*mm^-1"
    				# lappend materialData "0 N*mm^-1"
                    lappend materialData [format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 30]]]
                    lappend materialData 0
                    lappend materialData 0
                    lappend materialData 0
    			}
    			"LINEAR" {
    				# lappend materialData "0 N*mm^-1"
    				# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 30]]] N*mm^-1"
    				# lappend materialData "0 N*mm^-1"
    				# lappend materialData "0 N*mm^-1"
                    lappend materialData 0
                    lappend materialData [format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 30]]]
                    lappend materialData 0
                    lappend materialData 0
    			}
    			"TRILINEAR" {
    				# lappend materialData "0 N*mm^-1"
    				# lappend materialData "0 N*mm^-1"
    				# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 30]]] N*mm^-1"
    				# lappend materialData "0 N*mm^-1"
                    lappend materialData 0
                    lappend materialData 0
                    lappend materialData [format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 30]]]
                    lappend materialData 0
    			}
    			"CORNELISSEN" {
    				# lappend materialData "0 N*mm^-1"
    				# lappend materialData "0 N*mm^-1"
    				# lappend materialData "0 N*mm^-1"
    				# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 30]]] N*mm^-1"
                    lappend materialData 0
                    lappend materialData 0
                    lappend materialData 0
                    lappend materialData [format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + 30]]]
    			}
    		}

    		for {set j 31} {$j < 35} {incr j} {
    			lappend materialData [format $doubleFormatMatParameters [lindex $nlmm104Data [expr $index + $j]]]
    		}

    		if { [ExistsListElement $nlmm104NameList [string toupper $materialName]] } {
	    		GiD_ModifyData materials $materialName $materialData
	    	} else {
	    		GiD_CreateData create material NLMM104_Concrete $materialName $materialData
                # lappend nlmm104NameList $materialName
	    	}

        	set index [expr $index + 36]        	
    	}
    }
}