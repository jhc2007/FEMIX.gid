proc CreateFaceLoadsList {} {

	global FaceLoadsList
	set FaceLoadsList {}
	return 0
}

proc AddFaceLoads { elemType elemNum face direction loadType numNodes args } {

	global FaceLoadsList \
	       numDimension
	       # AuxiliaryVectorsList \


	# $elemType:
	# 1=Linear, 2=Triangle, 3=Quadrilateral, 4=Tetrahedra, 5=Hexahedra

	# if { $numDimension == 2 } {
		# if { $vectorDirection != "N" && $vectorDirection != 0 } {
		# 	set vectorDirection [expr $vectorDirection % 3 + 1]
		# }
	# }

	set lastFaceLoad [lindex $FaceLoadsList end]

	if { $direction == 0 } { 
		set direction "VECTOR_[GetAuxiliaryVectorIndexFromComponents [lindex $args end-2] [lindex $args end-1] [lindex $args end]]"
	} else {
        if { $numDimension == 2 && [string is integer [string index $direction end]] } {
            if { [string index $direction 0] == "X" } {
                set direction "_XG[expr [string index $direction end] % 3 + 1]"
            } else {
                set direction "_L[expr [string index $direction end] % 3 + 1]"
            }
            
        } else {
	        set direction "_$direction"
        }
	}

	if { $loadType == "Force" } {
		set loadType "_FORC"
	} else {
		set loadType "_MOM"
	}

	set newFaceLoad [list $elemNum $elemNum $face $direction $loadType]

	if { $numNodes == 1 } {
        lappend newFaceLoad $numNodes
		lappend newFaceLoad [lindex $args 0]
	} else {
		if { $elemType == 3 } {
			# Quadrilateral
			set faceNodes [lrange [GiD_Info Mesh Elements Quadrilateral $elemNum] 1 end-1]
            if { [llength $faceNodes] == 8 || [llength $faceNodes] == 9 } {
                set faceNodes [GetQuadElemNodesInFemixOrder $faceNodes]
		    }
		} elseif { $elemType == 5 } {
			# Hexahedra
			set elemNodes [lrange [GiD_Info Mesh Elements Hexahedra $elemNum] 1 end-1]
	        if { [llength $elemNodes] == 20 } {
	            set elemNodes [GetHexaElemNodesInFemixOrder $elemNodes]
	        }
	        set faceNodes [GetFaceNodes $elemNodes $face]
		}

        lappend newFaceLoad [llength $faceNodes]
		
        if { $numNodes == 4 } {
			for {set i 0} {$i < [llength $faceNodes]} {incr i} {
				lappend newFaceLoad [GetNodeValueFrom4NodesSurfaceShapeFunction [lindex $faceNodes $i] [lrange $args 0 [expr $numNodes * 2 - 1]]]
			}
		} else {
			for {set i 0} {$i < [llength $faceNodes]} {incr i} {
				lappend newFaceLoad [GetNodeValueFrom8NodesSurfaceShapeFunction [lindex $faceNodes $i] [lrange $args 0 [expr $numNodes * 2 - 1]]]
			}
		}
	}

	if { [EqualFaceLoads $lastFaceLoad $newFaceLoad] } {
		set lastFaceLoad [lreplace $lastFaceLoad 1 1 $elemNum]
		set FaceLoadsList [lreplace $FaceLoadsList end end $lastFaceLoad]
	} else {
		lappend FaceLoadsList $newFaceLoad
	}

	return 0
}

proc GetNodeValueFrom4NodesSurfaceShapeFunction { nodeNum pointValueList } {

  # global PROBLEMTYPEPATH
  # set result [open "$PROBLEMTYPEPATH\\result_new.dat" "w"]

  # puts $result [GiD_Info list_entities Surfaces 1]
  # puts $result [GiD_Info list_entities Lines 1]
  # puts $result [GiD_Info list_entities Lines 2]
  # puts $result [GiD_Info list_entities Lines 3]
  # puts $result [GiD_Info list_entities Lines 4]

  # puts $result "Node: $nodeNum"

	set coordinates [GiD_Info Coordinates $nodeNum Mesh]

	# puts $result $coordinates

	set x [lindex [lindex $coordinates 0] 0]
    set y [lindex [lindex $coordinates 0] 1]
    set z [lindex [lindex $coordinates 0] 2]

    set coordinates [GiD_Info Coordinates [lindex $pointValueList 0] Geometry]

    # puts $result [lindex $pointValueList 0]
    # puts $result $coordinates

    set x1 [lindex [lindex $coordinates 0] 0]
    set y1 [lindex [lindex $coordinates 0] 1]
    set z1 [lindex [lindex $coordinates 0] 2]

    set coordinates [GiD_Info Coordinates [lindex $pointValueList 2] Geometry]

    # puts $result [lindex $pointValueList 2]
    # puts $result $coordinates

    set x2 [lindex [lindex $coordinates 0] 0]
    set y2 [lindex [lindex $coordinates 0] 1]
    set z2 [lindex [lindex $coordinates 0] 2]

    set coordinates [GiD_Info Coordinates [lindex $pointValueList 4] Geometry]

    # puts $result [lindex $pointValueList 4]
    # puts $result $coordinates

    set x3 [lindex [lindex $coordinates 0] 0]
    set y3 [lindex [lindex $coordinates 0] 1]
    set z3 [lindex [lindex $coordinates 0] 2]

    set coordinates [GiD_Info Coordinates [lindex $pointValueList 6] Geometry]

    # puts $result [lindex $pointValueList 6]
    # puts $result $coordinates

    set x4 [lindex [lindex $coordinates 0] 0]
    set y4 [lindex [lindex $coordinates 0] 1]
    set z4 [lindex [lindex $coordinates 0] 2]

    if { double($x1 == $x2) && double($x1 == $x3) } {
    	set x $z
    	set x1 $z1
    	set x2 $z2
    	set x3 $z3
    	set x4 $z4
    } elseif { double($y1 == $y2) && double($y1 == $y3) } {
    	set y $z
    	set y1 $z1
    	set y2 $z2
    	set y3 $z3
    	set y4 $z4
    }

    set s1 -1
    set s2 -1

    set ds1 1
    set ds2 1

    set iteration 0

  # puts $result $pointValueList
  # puts $result $x
  # puts $result $y
  # puts $result $z
  # puts $result $x1
  # puts $result $y1
  # puts $result $z1
  # puts $result $x2
  # puts $result $y2
  # puts $result $z2
  # puts $result $x3
  # puts $result $y3
  # puts $result $z3
  # puts $result $x4
  # puts $result $y4
  # puts $result $z4
  # close $result

    while { ([expr abs($ds1)] >= 1e-8 || [expr abs($ds2)] >= 1e-8) && $iteration < 10 } {

    	set s1 [expr $s1 + $ds1]
    	set s2 [expr $s2 + $ds2]

    	incr iteration

    	# puts $result "s1=$s1"
    	# puts $result "s2=$s2"

    	set n1 [expr double((1 - $s1) * (1 - $s2) * 0.25)]
    	set n2 [expr double((1 + $s1) * (1 - $s2) * 0.25)]
    	set n3 [expr double((1 + $s1) * (1 + $s2) * 0.25)]
    	set n4 [expr double((1 - $s1) * (1 + $s2) * 0.25)]

    	# puts $result "N1=$n1"
    	# puts $result "N2=$n2"
    	# puts $result "N3=$n3"
    	# puts $result "N4=$n4"


    	set dn1ds1 [expr double(($s2 - 1) * 0.25)]
    	set dn1ds2 [expr double(($s1 - 1) * 0.25)]

    	set dn2ds1 [expr double((1 - $s2) * 0.25)]
    	set dn2ds2 [expr double((- $s1 - 1) * 0.25)]

    	set dn3ds1 [expr double((1 + $s2) * 0.25)]
    	set dn3ds2 [expr double((1 + $s1) * 0.25)]

    	set dn4ds1 [expr double((- $s2 - 1) * 0.25)]
    	set dn4ds2 [expr double((1 - $s1) * 0.25)]

    	# puts $result "dn1ds1=$dn1ds1"
    	# puts $result "dn1ds2=$dn1ds2"
    	# puts $result "dn2ds1=$dn2ds1"
    	# puts $result "dn2ds2=$dn2ds2"
    	# puts $result "dn3ds1=$dn3ds1"
    	# puts $result "dn3ds2=$dn3ds2"
    	# puts $result "dn4ds1=$dn4ds1"
    	# puts $result "dn4ds2=$dn4ds2"

    	set f1 [expr double($x - $n1 * $x1 - $n2 * $x2 - $n3 * $x3 - $n4 * $x4)]
    	set f2 [expr double($y - $n1 * $y1 - $n2 * $y2 - $n3 * $y3 - $n4 * $y4)]

    	# puts $result "f1=$f1"
    	# puts $result "f2=$f2"

    	set df1ds1 [expr double($dn1ds1 * $x1 + $dn2ds1 * $x2 + $dn3ds1 * $x3 + $dn4ds1 * $x4)]
    	set df1ds2 [expr double($dn1ds2 * $x1 + $dn2ds2 * $x2 + $dn3ds2 * $x3 + $dn4ds2 * $x4)]

    	set df2ds1 [expr double($dn1ds1 * $y1 + $dn2ds1 * $y2 + $dn3ds1 * $y3 + $dn4ds1 * $y4)]
    	set df2ds2 [expr double($dn1ds2 * $y1 + $dn2ds2 * $y2 + $dn3ds2 * $y3 + $dn4ds2 * $y4)]

    	# puts $result "df1ds1=$df1ds1"
    	# puts $result "df1ds2=$df1ds2"
    	# puts $result "df2ds1=$df2ds1"
    	# puts $result "df2ds2=$df2ds2"

    	set det [expr double($df1ds1 * $df2ds2 - $df1ds2 * $df2ds1)]

    	# puts $result "det=$det"

    	if { double($det != 0) } {
    		set ds1 [expr double(($df2ds2 * $f1 - $df1ds2 * $f2) / $det)]
    		set ds2 [expr double(($df1ds1 * $f2 - $df2ds1 * $f1) / $det)]
    	} else {
    		set ds1 0
    		set ds2 0
    	}

    	# puts $result "ds1=$ds1"
    	# puts $result "ds2=$ds2"
    }

    # close $result
    # return 0
    return [format %16.8e [expr double($n1 * [lindex $pointValueList 1] + $n2 * [lindex $pointValueList 3] + $n3 * [lindex $pointValueList 5] + $n4 * [lindex $pointValueList 7])]]
}

proc GetNodeValueFrom8NodesSurfaceShapeFunction { nodeNum pointValueList } {

  # global PROBLEMTYPEPATH
  # set result [open "$PROBLEMTYPEPATH\\result_new.dat" "w"]

  # puts $result [GiD_Info list_entities Surfaces 1]
  # puts $result [GiD_Info list_entities Lines 1]
  # puts $result [GiD_Info list_entities Lines 2]
  # puts $result [GiD_Info list_entities Lines 3]
  # puts $result [GiD_Info list_entities Lines 4]

  # puts $result "Node: $nodeNum"

	set coordinates [GiD_Info Coordinates $nodeNum Mesh]

	# puts $result $coordinates

	set x [lindex [lindex $coordinates 0] 0]
    set y [lindex [lindex $coordinates 0] 1]
    set z [lindex [lindex $coordinates 0] 2]

    set coordinates [GiD_Info Coordinates [lindex $pointValueList 0] Geometry]

    # puts $result [lindex $pointValueList 0]
    # puts $result $coordinates

    set x1 [lindex [lindex $coordinates 0] 0]
    set y1 [lindex [lindex $coordinates 0] 1]
    set z1 [lindex [lindex $coordinates 0] 2]

    set coordinates [GiD_Info Coordinates [lindex $pointValueList 2] Geometry]

    # puts $result [lindex $pointValueList 2]
    # puts $result $coordinates

    set x2 [lindex [lindex $coordinates 0] 0]
    set y2 [lindex [lindex $coordinates 0] 1]
    set z2 [lindex [lindex $coordinates 0] 2]

    set coordinates [GiD_Info Coordinates [lindex $pointValueList 4] Geometry]

    # puts $result [lindex $pointValueList 4]
    # puts $result $coordinates

    set x3 [lindex [lindex $coordinates 0] 0]
    set y3 [lindex [lindex $coordinates 0] 1]
    set z3 [lindex [lindex $coordinates 0] 2]

    set coordinates [GiD_Info Coordinates [lindex $pointValueList 6] Geometry]

    # puts $result [lindex $pointValueList 6]
    # puts $result $coordinates

    set x4 [lindex [lindex $coordinates 0] 0]
    set y4 [lindex [lindex $coordinates 0] 1]
    set z4 [lindex [lindex $coordinates 0] 2]

    set coordinates [GiD_Info Coordinates [lindex $pointValueList 8] Geometry]

    set x5 [lindex [lindex $coordinates 0] 0]
    set y5 [lindex [lindex $coordinates 0] 1]
    set z5 [lindex [lindex $coordinates 0] 2]

    set coordinates [GiD_Info Coordinates [lindex $pointValueList 10] Geometry]

    set x6 [lindex [lindex $coordinates 0] 0]
    set y6 [lindex [lindex $coordinates 0] 1]
    set z6 [lindex [lindex $coordinates 0] 2]

    set coordinates [GiD_Info Coordinates [lindex $pointValueList 12] Geometry]

    set x7 [lindex [lindex $coordinates 0] 0]
    set y7 [lindex [lindex $coordinates 0] 1]
    set z7 [lindex [lindex $coordinates 0] 2]

    set coordinates [GiD_Info Coordinates [lindex $pointValueList 14] Geometry]

    set x8 [lindex [lindex $coordinates 0] 0]
    set y8 [lindex [lindex $coordinates 0] 1]
    set z8 [lindex [lindex $coordinates 0] 2]

    if { double($x1 == $x3) && double($x1 == $x5) } {
    	set x $z
    	set x1 $z1
    	set x2 $z2
    	set x3 $z3
    	set x4 $z4
        set x5 $z5
        set x6 $z6
        set x7 $z7
        set x8 $z8
    } elseif { double($y1 == $y3) && double($y1 == $y5) } {
    	set y $z
    	set y1 $z1
    	set y2 $z2
    	set y3 $z3
    	set y4 $z4
        set y5 $z5
        set y6 $z6
        set y7 $z7
        set y8 $z8
    }

    set s1 -1
    set s2 -1

    set ds1 1
    set ds2 1

    set iteration 0

  # puts $result $pointValueList
  # puts $result $x
  # puts $result $y
  # puts $result $z
  # puts $result $x1
  # puts $result $y1
  # puts $result $z1
  # puts $result $x2
  # puts $result $y2
  # puts $result $z2
  # puts $result $x3
  # puts $result $y3
  # puts $result $z3
  # puts $result $x4
  # puts $result $y4
  # puts $result $z4
  # close $result

    while { ([expr abs($ds1)] >= 1e-8 || [expr abs($ds2)] >= 1e-8) && $iteration < 10 } {

    	set s1 [expr $s1 + $ds1]
    	set s2 [expr $s2 + $ds2]

    	incr iteration

    	# puts $result "s1=$s1"
    	# puts $result "s2=$s2"

    	set n1 [expr double((1 - $s1) * (1 - $s2) * (- $s1 - $s2 - 1) * 0.25)]
    	set n2 [expr double((1 - pow($s1,2)) * (1 - $s2) * 0.5)]
    	set n3 [expr double((1 + $s1) * (1 - $s2) * ($s1 - $s2 - 1) * 0.25)]
    	set n4 [expr double((1 + $s1) * (1 - pow($s2,2)) * 0.5)]
        set n5 [expr double((1 + $s1) * (1 + $s2) * ($s1 + $s2 - 1) * 0.25)]
        set n6 [expr double((1 - pow($s1,2)) * (1 + $s2) * 0.5)]
        set n7 [expr double((1 - $s1) * (1 + $s2) * (- $s1 + $s2 - 1) * 0.25)]
        set n8 [expr double((1 - $s1) * (1 - pow($s2,2)) * 0.5)]

    	# puts $result "N1=$n1"
    	# puts $result "N2=$n2"
    	# puts $result "N3=$n3"
    	# puts $result "N4=$n4"


    	set dn1ds1 [expr double((1 - $s2) * (2 * $s1 + $s2) * 0.25)]
    	set dn1ds2 [expr double((1 - $s1) * (2 * $s2 + $s1) * 0.25)]

    	set dn2ds1 [expr double(- $s1 * (1 - $s2))]
    	set dn2ds2 [expr double(- 0.5 * (1 - pow($s1,2)))]

    	set dn3ds1 [expr double((1 - $s2) * (2 * $s1 - $s2) * 0.25)]
    	set dn3ds2 [expr double((1 + $s1) * (2 * $s2 - $s1) * 0.25)]

    	set dn4ds1 [expr double(0.5 * (1 - pow($s2,2)))]
    	set dn4ds2 [expr double(- $s2 * (1 + $s1))]

        set dn5ds1 [expr double((1 + $s2) * (2 * $s1 + $s2) * 0.25)]
        set dn5ds2 [expr double((1 + $s1) * (2 * $s2 + $s1) * 0.25)]

        set dn6ds1 [expr double(- $s1 * (1 + $s2))]
        set dn6ds2 [expr double(0.5 * (1 - pow($s1,2)))]

        set dn7ds1 [expr double((1 + $s2) * (2 * $s1 - $s2) * 0.25)]
        set dn7ds2 [expr double((1 - $s1) * (2 * $s2 - $s1) * 0.25)]

        set dn8ds1 [expr double(- 0.5 * (1 - pow($s2,2)))]
        set dn8ds2 [expr double(- $s2 * (1 - $s1))]

    	# puts $result "dn1ds1=$dn1ds1"
    	# puts $result "dn1ds2=$dn1ds2"
    	# puts $result "dn2ds1=$dn2ds1"
    	# puts $result "dn2ds2=$dn2ds2"
    	# puts $result "dn3ds1=$dn3ds1"
    	# puts $result "dn3ds2=$dn3ds2"
    	# puts $result "dn4ds1=$dn4ds1"
    	# puts $result "dn4ds2=$dn4ds2"

    	set f1 [expr double($x - $n1 * $x1 - $n2 * $x2 - $n3 * $x3 - $n4 * $x4 - $n5 * $x5 - $n6 * $x6 - $n7 * $x7 - $n8 * $x8)]
    	set f2 [expr double($y - $n1 * $y1 - $n2 * $y2 - $n3 * $y3 - $n4 * $y4 - $n5 * $y5 - $n6 * $y6 - $n7 * $y7 - $n8 * $y8)]

    	# puts $result "f1=$f1"
    	# puts $result "f2=$f2"

    	set df1ds1 [expr double($dn1ds1 * $x1 + $dn2ds1 * $x2 + $dn3ds1 * $x3 + $dn4ds1 * $x4 + $dn5ds1 * $x5 + $dn6ds1 * $x6 + $dn7ds1 * $x7 + $dn8ds1 * $x8)]
    	set df1ds2 [expr double($dn1ds2 * $x1 + $dn2ds2 * $x2 + $dn3ds2 * $x3 + $dn4ds2 * $x4 + $dn5ds2 * $x5 + $dn6ds2 * $x6 + $dn7ds2 * $x7 + $dn8ds2 * $x8)]

    	set df2ds1 [expr double($dn1ds1 * $y1 + $dn2ds1 * $y2 + $dn3ds1 * $y3 + $dn4ds1 * $y4 + $dn5ds1 * $y5 + $dn6ds1 * $y6 + $dn7ds1 * $y7 + $dn8ds1 * $y8)]
    	set df2ds2 [expr double($dn1ds2 * $y1 + $dn2ds2 * $y2 + $dn3ds2 * $y3 + $dn4ds2 * $y4 + $dn5ds2 * $y5 + $dn6ds2 * $y6 + $dn7ds2 * $y7 + $dn8ds2 * $y8)]

    	# puts $result "df1ds1=$df1ds1"
    	# puts $result "df1ds2=$df1ds2"
    	# puts $result "df2ds1=$df2ds1"
    	# puts $result "df2ds2=$df2ds2"

    	set det [expr double($df1ds1 * $df2ds2 - $df1ds2 * $df2ds1)]

    	# puts $result "det=$det"

    	if { double($det != 0) } {
    		set ds1 [expr double(($df2ds2 * $f1 - $df1ds2 * $f2) / $det)]
    		set ds2 [expr double(($df1ds1 * $f2 - $df2ds1 * $f1) / $det)]
    	} else {
    		set ds1 0
    		set ds2 0
    	}

    	# puts $result "ds1=$ds1"
    	# puts $result "ds2=$ds2"
    }

    # close $result
    # return 0
    return [format %16.8e [expr double($n1 * [lindex $pointValueList 1] + $n2 * [lindex $pointValueList 3] + $n3 * [lindex $pointValueList 5] + $n4 * [lindex $pointValueList 7] + $n5 * [lindex $pointValueList 9] + $n6 * [lindex $pointValueList 11] + $n7 * [lindex $pointValueList 13] + $n8 * [lindex $pointValueList 15])]]
}

proc EqualFaceLoads { lastFaceLoad newFaceLoad } {

	for {set i 2} {$i < [llength $newFaceLoad]} {incr i} {
		if { [lindex $lastFaceLoad $i] != [lindex $newFaceLoad $i] } {
			return false
		}
	}

	if { [expr [lindex $lastFaceLoad 1] + 1] == [lindex $newFaceLoad 0] } {
		return true
	}
	
	return false
}

proc GetFemixFace { gidFace } {

	switch $gidFace {
		2 {
			return 5	
		}
		3 {
			return 2
		}
		4 {
			return 3
		}
		5 {
			return 4
		}
		default {
			return $gidFace
		}
	}
}

proc WriteFaceLoadsList {} {

	global FaceLoadsList 
		   # numDimension

	set returnString ""
	set index 1

	for {set i 0} {$i < [llength $FaceLoadsList]} {incr i} {

		set faceLoad [lindex $FaceLoadsList $i]

        if { [lindex $faceLoad 0] == [lindex $faceLoad 1] } {
            append returnString [format %7d $index]
            append returnString "  [lindex $faceLoad 0]"
            incr index
        } else {
            append returnString "  \[$index\-[expr $index + [lindex $faceLoad 1] - [lindex $faceLoad 0]]\]"
            append returnString "  \[[lindex $faceLoad 0]\-[lindex $faceLoad 1]\]"
            set index [expr $index + [lindex $faceLoad 1] - [lindex $faceLoad 0] + 1]
        }
        
        for {set j 2} {$j < [llength $faceLoad]} {incr j} {
        	append returnString "  [lindex $faceLoad $j]"
        }

        append returnString " ;\n"
	}

	return $returnString
}

proc CountFaceLoads {} {

	global FaceLoadsList

	set count 0
	for {set i 0} {$i < [llength $FaceLoadsList]} {incr i} {
		set count [expr $count + [lindex [lindex $FaceLoadsList $i] 1] - [lindex [lindex $FaceLoadsList $i] 0] + 1]	
	}
	return $count
}