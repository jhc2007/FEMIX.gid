
proc CreateNLSMM101 { femixData } {

    set nlsmm101Data [GetBlockData $femixData "NLSMM101"]

    if { [llength $nlsmm101Data] > 0 } {

        # global doubleFormatMatParameters
        
        set allMaterialsNameList [GiD_Info Materials]
        set nlsmm101NameList {}

        for {set i 0} {$i < [llength $allMaterialsNameList]} {incr i} {
            if { [lindex [GiD_Info Materials [lindex $allMaterialsNameList $i]] 2] == "NLSMM101" } {
                lappend nlsmm101NameList [string toupper [lindex $allMaterialsNameList $i]]
                # lappend nlsmm101NameList [lindex $allMaterialsNameList $i]
            }
        }

    	set numMaterials [lindex $nlsmm101Data 2]

    	set index 5

    	for {set i 0} {$i < $numMaterials} {incr i} {

    		set materialName [lindex $nlsmm101Data $index]

    		set materialData { NLSMM101 }

            for {set j 0} {$j < 3} {incr j} {
                incr index 
                lappend materialData [lindex $nlsmm101Data $index]
            }

            incr index
    		lappend materialData [string range [lindex $nlsmm101Data $index] 1 end]

	    	if { [ExistsListElement $nlsmm101NameList [string toupper $materialName]] } {
	    		GiD_ModifyData materials $materialName $materialData
	    	} else {
	    		GiD_CreateData create material NLSMM101_Spring $materialName $materialData
	    	}

        	incr index 3
	    }
	}
}
