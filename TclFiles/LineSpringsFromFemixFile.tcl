# PointStringsFromFemixFile.tcl

proc CreateLineSprings { femixData } {

    global LineSpringsFromFemixFile \
           NUMERICALINTEGRATIONLIST \
           SpringsNumericalIntegrationsFromFemixFile \
           SpringMaterialsList

	set lineSpringsData [GetBlockData $femixData "LINE_SPRINGS"]

    set firstNumInt yes
	set index 5

	while { $index < [llength $lineSpringsData] } {

		set groupName [lindex $lineSpringsData $index]

		if { ![ExistsListElement [GiD_Info layers] $groupName] && ![ExistsListElement [GiD_Groups list] $groupName] } {
			GiD_Process 'Groups Create $groupName Mescape
		}

		incr index 2
		set rangeList [GetFirstLastValuesFromRange [lindex $lineSpringsData $index]]
		set firstElem [lindex $rangeList 0]
        set lastElem [lindex $rangeList 1]

        incr index
        set face [lindex $lineSpringsData $index]

       	incr index
        set edge [lindex $lineSpringsData $index]

        set lineSpring [list $groupName $firstElem $lastElem $face $edge]

       	incr index
        set key [lindex $lineSpringsData $index]

        if { [string index $key 0] != "_" } {
            set vector [GetAuxiliaryVectorFromFemixFile $key]
            foreach var $vector {
                lappend lineSpring $var
            }
        } else {
            lappend lineSpring $key
        }
        
        for {set i 0} {$i < 4} {incr i} {
            incr index
            lappend lineSpring [lindex $lineSpringsData $index]
        }

        set integrationName [lindex $lineSpringsData $index]

        if { $integrationName != "_DEFAULT" && ![ExistsListElement $NUMERICALINTEGRATIONLIST $integrationName] && ![ExistsSpringsNumericalIntegrationFromFemixFile $integrationName] } {
            if { $firstNumInt } {
                set integrationData [GetBlockData $femixData "NUMERICAL_INTEGRATION"]
                set firstNumInt no
            }
            set intIndex 5
            while { $intIndex < [llength $integrationData] && [lindex $integrationData $intIndex] != $integrationName } {
                incr intIndex
                set numKey [lindex $integrationData $intIndex]
                incr intIndex [expr $numKey + 3]
            }
            if { $intIndex < [llength $integrationData] } {
                incr intIndex
                set numKey [lindex $integrationData $intIndex]
                set numInt [list $integrationName $numKey]
                for {set i 0} {$i < $numKey} {incr i} {
                    incr intIndex
                    lappend numInt [lindex $integrationData $intIndex]
                }
                lappend SpringsNumericalIntegrationsFromFemixFile $numInt
            }
        }

       	incr index
        set matType [lindex $lineSpringsData $index]
        lappend lineSpring $matType
        # set matType [string range $matType 1 [string length $matType]]

       	incr index
        set numNodes [lindex $lineSpringsData $index]
        lappend lineSpring $numNodes

        for {set i 0} {$i < $numNodes} {incr i} {
            incr index
            set matName [lindex $lineSpringsData $index]
            lappend lineSpring $matName
            if { ![ExistsListElement $SpringMaterialsList [list [string range $matType 1 end] $matName]] } {
                lappend SpringMaterialsList [list [string range $matType 1 end] $matName]
            }
        }
        
        lappend LineSpringsFromFemixFile $lineSpring

        incr index 3
    }
}

proc GetEdgeNodes { elem edge } {

	set elemsType { "Quadrilateral" "Hexahedra" }

	for {set i 0} {$i < [llength $elemsType]} {incr i} {
	    set elemsList [GiD_Info Mesh elements [lindex $elemsType $i] -sublist]
	    for {set j 0} {$j < [llength $elemsList]} {incr j} { 
	    	if { [lindex [lindex $elemsList $j] 0] == $elem } {
	    		if { [lindex $elemsType $i] == "Quadrilateral" } {
	    			# Quadrilateral Elements
	    			if { [llength [lindex $elemsList $j]] == 10 } {
	    				# Quadrilateral Elements with 8 Nodes
	    				set nodes [GetQuadElemNodesInFemixOrder [lrange [lindex $elemsList $j] 1 end-1]]
	    				switch $edge {
	    					1 {
	    						return [lrange $nodes 0 2]	
	    					}
	    					2 {
	    						return [lrange $nodes 2 4]	
	    					}
	    					3 {
	    						return [lrange $nodes 4 6]	
	    					}
	    					4 {
	    						return [list [lindex $nodes 6] [lindex $nodes 7] [lindex $nodes 0]]	
	    					}
	    				}
	    			} else {
	    				# Quadrilateral Elements with 4 Nodes
	    				set nodes [lrange [lindex $elemsList $j] 1 end-1]
	    				if { $edge == 4 } {
	    					return [list [lindex $nodes 3] [lindex $nodes 0]]	
	    				} else {
	    					return [lrange $nodes [expr $edge - 1] $edge]
	    				}
	    			}
    			} else {
    				# Hexahedra Elements
    				if { [llength [lindex $elemsList $j]] == 22 } { 
    					# Hexahedra Elements with 20 Nodes
    					set nodes [GetHexaElemNodesInFemixOrder [lrange [lindex $elemsList $j] 1 end-1]]
 						return [GetHexaElemEdgeNodes $nodes $edge]
					} else {
						# Hexahedra Elements with 8 Nodes
					}
    			}
	    	}
	    }
	}
}

proc GetLinearElementNodes { elem } {

    set elemsList [GiD_Info Mesh elements "Linear" -sublist]

    for {set i 0} {$i < [llength $elemsList]} {incr i} {
    	if { [lindex [lindex $elemsList $i] 0] == $elem } {
    		return [lrange [lindex $elemsList $i] 1 end-1]
    	}
    }
}

proc CreateFileLineSpringsFromFemixFile {} {

    global PROJECTPATH \
           LineSpringsFromFemixFile

    if { [llength $LineSpringsFromFemixFile] == 0 } {
        return
    }

    set lineSpringsFile [open "$PROJECTPATH\\femix_line_springs.dat" "w"]
    for {set i 0} {$i < [llength $LineSpringsFromFemixFile]} {incr i} {
        foreach var [lindex $LineSpringsFromFemixFile $i] {
            puts $lineSpringsFile $var
        }
    }
    close $lineSpringsFile
}

proc CreateFromFileLineSpringsFromFemixFile {} {

    global PROJECTPATH \
           LineSpringsFromFemixFile \
           SpringMaterialsList

    if { [file exists "$PROJECTPATH\\femix_line_springs.dat"] } {
        
        set lineSpringsFile [open "$PROJECTPATH\\femix_line_springs.dat" "r"]
        set lineSpringsData [read $lineSpringsFile]
        close $lineSpringsFile

        set index 0

        while { $index < [llength $lineSpringsData] } {

            set lineSpring {}

            for {set i $index} {$i < [expr $index + 5]} {incr i} {
                lappend lineSpring [lindex $lineSpringsData $i]
            }

            incr index 5
            if { [string is double [lindex $lineSpringsData $index]] } {
                for {set i $index} {$i < [expr $index + 7]} {incr i} {
                    lappend lineSpring [lindex $lineSpringsData $i]
                }
                incr index 7
            } else {
                for {set i $index} {$i < [expr $index + 5]} {incr i} {
                    lappend lineSpring [lindex $lineSpringsData $i]
                }
                incr index 5
            }

            set matType [lindex $lineSpringsData $index]
            lappend lineSpring $matType
            set matType [string range $matType 1 end]

            incr index
            set numNodes [lindex $lineSpringsData $index]
            lappend lineSpring $numNodes

            for {set i 0} {$i < $numNodes} {incr i} {
                incr index
                set matName [lindex $lineSpringsData $index]
                lappend lineSpring $matName
                if { ![ExistsListElement $SpringMaterialsList [list $matType $matName]] } {
                    lappend SpringMaterialsList [list $matType $matName]
                }
            }
            
            lappend lineSpringsFromFemixFile $lineSpring
            
            incr index
        }
    }
}

proc CountLineSpringsFromFemixFile {} {

    global LineSpringsFromFemixFile

    set count 0

    for {set i 0} {$i < [llength $LineSpringsFromFemixFile]} {incr i} {
        set count [expr $count + [lindex [lindex $LineSpringsFromFemixFile $i] 2] - [lindex [lindex $LineSpringsFromFemixFile $i] 1] + 1]
    }

    return $count
}

proc WriteLineSpringsFromFemixFile { index } {

    global LineSpringsFromFemixFile

    set returnString ""
    incr index 

  # global PROBLEMTYPEPATH
  # set result [open "$PROBLEMTYPEPATH\\result_new.dat" "w"]
  # puts $result "SurfSprings:"
  # foreach var $LineSpringsFromFemixFile {
  #     puts $result $var
  # }
  # close $result

    for {set i 0} {$i < [llength $LineSpringsFromFemixFile]} {incr i} {
    
        set lineSpring [lindex $LineSpringsFromFemixFile $i]

        if { [lindex $lineSpring 1] == [lindex $lineSpring 2] } {
            append returnString [format %7d $index]
            append returnString "  [lindex $lineSpring 0]  1  "
            append returnString [lindex $lineSpring 1]
            incr index
        } else {
            append returnString "  \[$index\-[expr $index + [lindex $lineSpring 2] - [lindex $lineSpring 1]]\]  "
            append returnString "[lindex $lineSpring 0]  1  "
            append returnString "\[[lindex $lineSpring 1]\-[lindex $lineSpring 2]\]"
            set index [expr $index + [lindex $lineSpring 2] - [lindex $lineSpring 1] + 1]
        }

        append returnString "  [lindex $lineSpring 3]"
        append returnString "  [lindex $lineSpring 4]"

        if { [string is double [lindex $lineSpring 5]] } {
            append returnString "  VECTOR_[GetAuxiliaryVectorIndexFromComponents [lindex $lineSpring 5] [lindex $lineSpring 6] [lindex $lineSpring 7]]"
            for {set j 8} {$j < [llength $lineSpring]} {incr j} {
                append returnString "  [lindex $lineSpring $j]"
            }
        } else {
            for {set j 5} {$j < [llength $lineSpring]} {incr j} {
                append returnString "  [lindex $lineSpring $j]"
            }
        }

        append returnString " ;\n"
    }
    return $returnString
}

proc AddAuxiliaryVectorFromLineSpringsFromFemixFile {} {

    global LineSpringsFromFemixFile

    foreach var $LineSpringsFromFemixFile {
        if { [string is double [lindex $var 5]] } {
            AddAuxiliaryVector [lindex $var 5] [lindex $var 6] [lindex $var 7]
        }
    }
    # return 0
}