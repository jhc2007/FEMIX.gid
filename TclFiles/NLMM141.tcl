# NLMM141.tcl
# 
# NumOfNLMM141InLayerProperties {}
# ExistsNLMM141InLayerProperties { matName }
# WriteNLMM141InLayerProperties { index }

proc NumOfNLMM141InLayerProperties {} {

    global layerPropertiesList NLMM141UsedInLayerProp

    set NLMM141UsedInLayerProp {}

    for {set i 0} {$i < [llength $layerPropertiesList]} {incr i} { 
        if { [lindex [lindex $layerPropertiesList $i] 1] == "NLMM141" && ![ExistsNLMM141InLayerProperties [lindex [lindex $layerPropertiesList $i] 2]] } {
            lappend NLMM141UsedInLayerProp [lindex [lindex $layerPropertiesList $i] 2]
        }
    }

    return [llength $NLMM141UsedInLayerProp]    
}

proc ExistsNLMM141InLayerProperties { matName } {

    global NLMM141UsedInLayerProp

    if { [ExistsListElement $NLMM141UsedInLayerProp [ReplaceSpaceWithUnderscore $matName]] } {
        return 1
    }
    return 0
}

proc WriteNLMM141InLayerProperties { index } {

    global NLMM141UsedInLayerProp

    set nlmm141 ""
    set materials [GiD_Info materials]

    #global PROJECTPATH
    #set result [open "$PROJECTPATH\\aaa_result.dat" "w"]
    #puts $result $materials
    #set mat [GiD_Info materials [lindex $materials 3]]
    #puts $result $mat
    #for {set i 1} {$i < [llength $mat]} {incr i} {
    # 	puts $result [lindex $mat $i]
    #}
    #close $result

    for {set i 0} {$i < [llength $materials]} {incr i} {

    	set material [GiD_Info materials [lindex $materials $i]]
        if { [lindex $material 2] == "NLMM141" && [ExistsNLMM141InLayerProperties [ReplaceSpaceWithUnderscore [lindex $materials $i]]] } {

        	append nlmm141 "#     A     B     C     D     E     F     G     H\n"

        	append nlmm141 "[format %7d $index]  "
        	append nlmm141 [ReplaceSpaceWithUnderscore [lindex $materials $i]]
        	for {set j 4} {$j <= 14} {incr j 2} {
                # append nlmm141 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material $j]]]
                append nlmm141 [format %18.8e [lindex $material $j]]
            }

            append nlmm141 "\n#     I     J     K     L     M     N     O     P\n"

            append nlmm141 [format %21.8e [lindex $material 16]]
            append nlmm141 [format %18.8e [lindex $material 18]]
            # append nlmm141 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material 20]]]
            append nlmm141 [format %18.8e [lindex $material 20]]
            append nlmm141 "  \_[lindex $material 22]"
            for {set j 24} {$j <= 30} {incr j 2} {
                append nlmm141 [format %18.8e [lindex $material $j]]
            }

            append nlmm141 "\n#     Q     R     S     T     U     V     X\n"

            if { [lindex $material 22] == "TRILINEAR" } {
            	# append nlmm141 [format %21.8e [RemoveUnitsFromParameterValue [lindex $material 32]]]
                append nlmm141 [format %21.8e [lindex $material 32]]
            } else {
            	# append nlmm141 [format %21.8e [RemoveUnitsFromParameterValue [lindex $material 34]]]
                append nlmm141 [format %21.8e [lindex $material 34]]
            }
            append nlmm141 [format %18.8e [lindex $material 36]]
            if { [lindex $material 38] == "VALUE" } {
            	append nlmm141 [format %18.8e [lindex $material 40]]
            } else {
            	append nlmm141 "  \_[lindex $material 38]"
            }
            append nlmm141 "  \_[lindex $material 42]"
            if { [lindex $material 44] == "VALUE" } {
            	append nlmm141 [format %18.8e [lindex $material 46]]
            } else {
            	append nlmm141 "  \_[lindex $material 44]"
            }
            append nlmm141 "  [lindex $material 48]  "  
            # append nlmm141 [RemoveUnitsFromParameterValue [lindex $material 50]]
            append nlmm141 [lindex $material 50]

        	append nlmm141 " ;\n"

        	set index [expr $index + 1]
        }
	}
	return $nlmm141
}