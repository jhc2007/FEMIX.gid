
proc CreateLinearSpringMaterials { femixData } {

    set linSpringData [GetBlockData $femixData "LINEAR_SPRING_MATERIALS"]

    if { [llength $linSpringData] > 0 } {

        # global doubleFormatMatParameters
        
        set allMaterialsNameList [GiD_Info Materials]
        set linSpringNameList {}

        for {set i 0} {$i < [llength $allMaterialsNameList]} {incr i} {
            if { [lindex [GiD_Info Materials [lindex $allMaterialsNameList $i]] 2] == "LIN_SPRING" } {
                lappend linSpringNameList [string toupper [lindex $allMaterialsNameList $i]]
                # lappend linSpringNameList [lindex $allMaterialsNameList $i]
            }
        }

    	set numMaterials [lindex $linSpringData 2]

    	set index 4

    	for {set i 0} {$i < $numMaterials} {incr i} {

    		set materialName [lindex $linSpringData [expr $index + 1]]

    		set materialData { LIN_SPRING }

    		lappend materialData [lindex $linSpringData [expr $index + 2]]

	    	if { [ExistsListElement $linSpringNameList [string toupper $materialName]] } {
	    		GiD_ModifyData materials $materialName $materialData
	    	} else {
	    		GiD_CreateData create material Linear_Spring $materialName $materialData
	    	}

        	set index [expr $index + 4]  
	    }
	}
}
