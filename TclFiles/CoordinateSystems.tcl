# CoordinateSystems.tcl
#
# proc GetLocalAxesNumber { name }
# proc GetLocalAxesName { num }
# proc WriteCoordinateSystems {}

proc GetLocalAxesNumber { name } {
    
    set localaxesnames [GiD_Info localaxes]
    set numlocalaxes [llength $localaxesnames]
    for {set i 0} {$i < $numlocalaxes} {incr i} {
    	if {[lindex $localaxesnames $i] == $name} {
    	    return [expr $i + 1] 
    	}
    }
    return 0
}

proc GetLocalAxesName { num } {

    set localaxesnames [GiD_Info localaxes]
    set numlocalaxes [llength $localaxesnames]
    for {set i 0} {$i < $numlocalaxes} {incr i} {
        if { [expr $i + 1] == $num } {
            return [ReplaceSpaceWithUnderscore [lindex $localaxesnames $i]]
        }
    }
    return 0
}

# proc WriteCoordinateSystems {} {

#     set localaxesnames [GiD_Info localaxes]
#     set numlocalaxes [llength $localaxesnames]
#     set coordinatesystems ""

#     for {set i 0} {$i < $numlocalaxes} {incr i} {

#         set localaxisname [ReplaceSpaceWithUnderscore [lindex $localaxesnames $i]]

#         append coordinatesystems "[format %7d [expr $i + 1]]\t"
#         append coordinatesystems "$localaxisname\t"

#         # set auxiliaryVectors [GetCoordinateSystemVectors $localaxisname]

#         # if { $auxiliaryVectors == false } {        

#         append coordinatesystems "$localaxisname"
#         append coordinatesystems "_VECT_1\t"
#         append coordinatesystems "$localaxisname"
#         append coordinatesystems "_VECT_2\t"
#         append coordinatesystems "$localaxisname"
#         append coordinatesystems "_VECT_3 ;\n"

#         # } else {

#         #     append coordinatesystems "[lindex $auxiliaryVectors 0]\t"
#         #     append coordinatesystems "[lindex $auxiliaryVectors 1]\t"
#         #     append coordinatesystems "[lindex $auxiliaryVectors 2] ;\n"

#         # }
#     } 

#     return $coordinatesystems
# }

# proc GetCoordinateSystemVectors { localaxisname } {

#     global CoordinateSystemsFromFemixFile

#     for {set i 0} {$i < [llength $CoordinateSystemsFromFemixFile]} {incr i} {
#         if { [lindex [lindex $CoordinateSystemsFromFemixFile $i] 0] == $localaxisname } {
#             return [list [lindex [lindex $CoordinateSystemsFromFemixFile $i] 1] \
#                          [lindex [lindex $CoordinateSystemsFromFemixFile $i] 2] \
#                          [lindex [lindex $CoordinateSystemsFromFemixFile $i] 3]]
#         }
#     }

#     return false
# }