# NLMM301FromFemixFile.tcl

proc CreateNLMM301 { femixData } {

    set nlmm301Data [GetBlockData $femixData "NLMM301"]

    if { [llength $nlmm301Data] > 0 } {

        global doubleFormatMatParameters
        set allMaterialsNameList [GiD_Info materials]
        set nlmm301NameList {}

        for {set i 0} {$i < [llength $allMaterialsNameList]} {incr i} {
            if { [lindex [GiD_Info materials [lindex $allMaterialsNameList $i]] 2] == "NLMM301" } {
                lappend nlmm301NameList [string toupper [lindex $allMaterialsNameList $i]]
            }
        }

    	set numMaterials [lindex $nlmm301Data 2]

    	set index 4

    	for {set i 0} {$i < $numMaterials} {incr i} {

    		set materialName [lindex $nlmm301Data [expr $index + 1]]

    		set materialData { NLMM301 } 

    		lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm301Data [expr $index + 2]]]"
	    	lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm301Data [expr $index + 3]]]"
	    	lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm301Data [expr $index + 4]]]"
	    	lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm301Data [expr $index + 5]]]"
	    	lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm301Data [expr $index + 6]]]"
	    	lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm301Data [expr $index + 7]]]"

	    	if { [ExistsListElement $nlmm301NameList [string toupper $materialName]] } {
	    		GiD_ModifyData materials $materialName $materialData
	    	} else {
	    		GiD_CreateData create material NLMM301_Interface $materialName $materialData
	    	}

        	set index [expr $index + 9]  
	    }
	}
}
