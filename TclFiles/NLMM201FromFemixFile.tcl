# NLMM201FromFemixFile.tcl

proc CreateNLMM201 { femixData } {

    set nlmm201Data [GetBlockData $femixData "NLMM201"]

    if { [llength $nlmm201Data] > 0 } {

        global doubleFormatMatParameters
        set allMaterialsNameList [GiD_Info materials]
        set nlmm201NameList {}

        for {set i 0} {$i < [llength $allMaterialsNameList]} {incr i} {
            if { [lindex [GiD_Info materials [lindex $allMaterialsNameList $i]] 2] == "NLMM201" } {
                lappend nlmm201NameList [string toupper [lindex $allMaterialsNameList $i]]
            }
        }

    	set numMaterials [lindex $nlmm201Data 2]

    	set index 4

    	for {set i 0} {$i < $numMaterials} {incr i} {

    		set materialName [lindex $nlmm201Data [expr $index + 1]]

    		set materialData { NLMM201 } 

    		# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm201Data [expr $index + 2]]] kg*mm^-3"
	    	# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm201Data [expr $index + 3]]] Cel^-1"
	    	# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm201Data [expr $index + 4]]]"
	    	# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm201Data [expr $index + 5]]] N*mm^-2"
	    	# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm201Data [expr $index + 6]]]"
	    	# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm201Data [expr $index + 7]]] N*mm^-2"
	    	# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm201Data [expr $index + 8]]]"
	    	# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm201Data [expr $index + 9]]] N*mm^-2"
	    	# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm201Data [expr $index + 10]]]"

            lappend materialData [format $doubleFormatMatParameters [lindex $nlmm201Data [expr $index + 2]]]
            lappend materialData [format $doubleFormatMatParameters [lindex $nlmm201Data [expr $index + 3]]]
            lappend materialData [format $doubleFormatMatParameters [lindex $nlmm201Data [expr $index + 4]]]
            lappend materialData [format $doubleFormatMatParameters [lindex $nlmm201Data [expr $index + 5]]]
            lappend materialData [format $doubleFormatMatParameters [lindex $nlmm201Data [expr $index + 6]]]
            lappend materialData [format $doubleFormatMatParameters [lindex $nlmm201Data [expr $index + 7]]]
            lappend materialData [format $doubleFormatMatParameters [lindex $nlmm201Data [expr $index + 8]]]
            lappend materialData [format $doubleFormatMatParameters [lindex $nlmm201Data [expr $index + 9]]]
            lappend materialData [format $doubleFormatMatParameters [lindex $nlmm201Data [expr $index + 10]]]

	    	if { [ExistsListElement $nlmm201NameList [string toupper $materialName]] } {
	    		GiD_ModifyData materials $materialName $materialData
	    	} else {
	    		GiD_CreateData create material NLMM201_Steel $materialName $materialData
	    	}

        	set index [expr $index + 12]  
	    }
	}
}
