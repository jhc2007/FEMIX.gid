# InterfaceElements.tcl

# proc AddInterfaceElementsFromFemixFile { firstElement lastElement } {

# 	global InterfaceElementsList
	
# 	lappend InterfaceElementsList [list $firstElement $lastElement]
# }

proc CreateLayerOfInterfaceElementsList {} {

	global LayerOfInterfaceElementsList
	set LayerOfInterfaceElementsList {}
	return 0
}

proc AddLayerOfInterfaceElements { layerNum } {

	global LayerOfInterfaceElementsList
	lappend LayerOfInterfaceElementsList $layerNum
	return 0
}

proc IsLayerOfInterfaceElements { layerNum } {

	global LayerOfInterfaceElementsList

	if { [ExistsListElement $LayerOfInterfaceElementsList $layerNum] }	{
		return 1
	}
	return 0
}
 
proc IsInterfaceElement { element } {

	global InterfaceElementsList

	for {set i 0} {$i < [llength $InterfaceElementsList]} {incr i} {
		if { $element >= [lindex [lindex $InterfaceElementsList $i] 0] && $element <= [lindex [lindex $InterfaceElementsList $i] 1] } {
			return 1
		}
	}
	return 0
}

proc Write2NodesLinearInterfaceElemsConec { node1 node2 node3 node4 } {

	set changeTangentOrder no 
	# set changeTangentOrder2 no
	set value "        4"

	set vector12x [expr [lindex [lindex [GiD_Info Coordinates $node2 mesh] 0] 0] - [lindex [lindex [GiD_Info Coordinates $node1 mesh] 0] 0]]

	if { $vector12x > -1.0e-10 && $vector12x < 1.0e-10 } {
		set vector12y [expr [lindex [lindex [GiD_Info Coordinates $node2 mesh] 0] 1] - [lindex [lindex [GiD_Info Coordinates $node1 mesh] 0] 1]]
		if { $vector12y < 0 } {
			set changeTangentOrder yes
		}
	} else {
		if { $vector12x < 0 } {
			set changeTangentOrder yes
		} 
	}

	# set vector34x [expr [lindex [lindex [GiD_Info Coordinates $node4 mesh] 0] 0] - [lindex [lindex [GiD_Info Coordinates $node3 mesh] 0] 0]]

	# if { $vector34x > -1.0e-10 && $vector34x < 1.0e-10 } {
	# 	set vector34y [expr [lindex [lindex [GiD_Info Coordinates $node4 mesh] 0] 1] - [lindex [lindex [GiD_Info Coordinates $node3 mesh] 0] 1]]
	# 	if { $vector34y < 0 } {
	# 		set changeTangentOrder2 yes
	# 	}
	# } else {
	# 	if { $vector34x < 0 } {
	# 		set changeTangentOrder2 yes
	# 	} 
	# }

	if { $changeTangentOrder } {
		append value "[format %9s $node2][format %9s $node1][format %9s $node4][format %9s $node3]"
	} else {
		append value "[format %9s $node1][format %9s $node2][format %9s $node3][format %9s $node4]"
	}

	# if { $changeTangentOrder2 } {
	# 	append value "[format %9s $node4][format %9s $node3]"
	# } else {
	# 	append value "[format %9s $node3][format %9s $node4]"
	# }

	return $value
}

proc Write3NodesLinearInterfaceElemsConec { node1 node2 node3 node4 node5 node6 } {

	set changeTangentOrder no 
	set value "        6"

	set vector12x [expr [lindex [lindex [GiD_Info Coordinates $node2 mesh] 0] 0] - [lindex [lindex [GiD_Info Coordinates $node1 mesh] 0] 0]]

	if { $vector12x > -1.0e-10 && $vector12x < 1.0e-10 } {
		set vector12y [expr [lindex [lindex [GiD_Info Coordinates $node2 mesh] 0] 1] - [lindex [lindex [GiD_Info Coordinates $node1 mesh] 0] 1]]
		if { $vector12y < 0 } {
			set changeTangentOrder yes
		}
	} else {
		if { $vector12x < 0 } {
			set changeTangentOrder yes
		} 
	}

	if { $changeTangentOrder } {
		append value "[format %9s $node3][format %9s $node2][format %9s $node1][format %9s $node6][format %9s $node5][format %9s $node4]"
	} else {
		append value "[format %9s $node1][format %9s $node2][format %9s $node3][format %9s $node4][format %9s $node5][format %9s $node6]"
	}

	return $value
}

proc Write4NodesQuadInterfaceElemsConec { conec1 conec2 conec3 conec4 } {

	set changeTangentOrder no
	set changeNormalOrder no

	set vector12x [expr [lindex [lindex [GiD_Info Coordinates $conec2 mesh] 0] 0] - [lindex [lindex [GiD_Info Coordinates $conec1 mesh] 0] 0]]

	if { $vector12x > -1.0e-10 && $vector12x < 1.0e-10 } {
		set vector12y [expr [lindex [lindex [GiD_Info Coordinates $conec2 mesh] 0] 1] - [lindex [lindex [GiD_Info Coordinates $conec1 mesh] 0] 1]]
		if { $vector12y < 0 } {
			set changeTangentOrder yes
		}
	} else {
		if { $vector12x < 0 } {
			set changeTangentOrder yes
		} 
	}

	set vector13y [expr [lindex [lindex [GiD_Info Coordinates $conec3 mesh] 0] 1] - [lindex [lindex [GiD_Info Coordinates $conec1 mesh] 0] 1]]

	if { $vector13y > -1.0e-10 && $vector13y < 1.0e-10 } {
		set vector13x [expr [lindex [lindex [GiD_Info Coordinates $conec3 mesh] 0] 0] - [lindex [lindex [GiD_Info Coordinates $conec1 mesh] 0] 0]]
		if { $vector13x < 0 } {
			set changeNormalOrder yes
		}
	} else {
		if { $vector13y < 0 } {
			set changeNormalOrder yes
		}
	}

	if { $changeTangentOrder } {
		if { $changeNormalOrder } {
			return "[format %9s $conec4][format %9s $conec3][format %9s $conec2][format %9s $conec1]"
		} else {
			return "[format %9s $conec2][format %9s $conec1][format %9s $conec4][format %9s $conec3]"
		}
	} else {
		if { $changeNormalOrder } {
			return "[format %9s $conec3][format %9s $conec4][format %9s $conec1][format %9s $conec2]"
		} else {
			return "[format %9s $conec1][format %9s $conec2][format %9s $conec3][format %9s $conec4]"
		}
	}
}

proc Write6NodesQuadInterfaceElemsConec { conec1 conec2 conec3 conec4 conec5 conec6 } {

	set changeTangentOrder no
	set changeNormalOrder no

	set vector12x [expr [lindex [lindex [GiD_Info Coordinates $conec2 mesh] 0] 0] - [lindex [lindex [GiD_Info Coordinates $conec1 mesh] 0] 0]]

	if { $vector12x > -1.0e-10 && $vector12x < 1.0e-10 } {
		set vector12y [expr [lindex [lindex [GiD_Info Coordinates $conec2 mesh] 0] 1] - [lindex [lindex [GiD_Info Coordinates $conec1 mesh] 0] 1]]
		if { $vector12y < 0 } {
			set changeTangentOrder yes
		}
	} else {
		if { $vector12x < 0 } {
			set changeTangentOrder yes
		} 
	}

	set vector14y [expr [lindex [lindex [GiD_Info Coordinates $conec4 mesh] 0] 1] - [lindex [lindex [GiD_Info Coordinates $conec1 mesh] 0] 1]]

	if { $vector14y > -1.0e-10 && $vector14y < 1.0e-10 } {
		set vector14x [expr [lindex [lindex [GiD_Info Coordinates $conec4 mesh] 0] 0] - [lindex [lindex [GiD_Info Coordinates $conec1 mesh] 0] 0]]
		if { $vector14x < 0 } {
			set changeNormalOrder yes
		}
	} else {
		if { $vector14y < 0 } {
			set changeNormalOrder yes
		}
	}

	if { $changeTangentOrder } {
		if { $changeNormalOrder } {
			return "[format %9s $conec6][format %9s $conec5][format %9s $conec4][format %9s $conec3][format %9s $conec2][format %9s $conec1]"
		} else {
			return "[format %9s $conec3][format %9s $conec2][format %9s $conec1][format %9s $conec6][format %9s $conec5][format %9s $conec4]"
		}
	} else {
		if { $changeNormalOrder } {
			return "[format %9s $conec4][format %9s $conec5][format %9s $conec6][format %9s $conec1][format %9s $conec2][format %9s $conec3]"
		} else {
			return "[format %9s $conec1][format %9s $conec2][format %9s $conec3][format %9s $conec4][format %9s $conec5][format %9s $conec6]"
		}
	}
}

# proc PrintLayer { layer } {

#   puts $result [GiD_Info layers -entities elements $layer]
#   close $result

#   	return 0

# }

proc CompareNodeCoordinatesWithConec12 { node conec1 conec2 } {

	# if { $node == $conec1 || $node == $conec2 } {
	# 	return 0
	# }

	# if { ( ([lindex [lindex [GiD_Info Coordinates $node mesh] 0] 0] == [lindex [lindex [GiD_Info Coordinates $conec1 mesh] 0] 0]) && ([lindex [lindex [GiD_Info Coordinates $node mesh] 0] 1] == [lindex [lindex [GiD_Info Coordinates $conec1 mesh] 0] 1]) && ([lindex [lindex [GiD_Info Coordinates $node mesh] 0] 2] == [lindex [lindex [GiD_Info Coordinates $conec1 mesh] 0] 2]) ) || ( ([lindex [lindex [GiD_Info Coordinates $node mesh] 0] 0] == [lindex [lindex [GiD_Info Coordinates $conec2 mesh] 0] 0]) && ([lindex [lindex [GiD_Info Coordinates $node mesh] 0] 1] == [lindex [lindex [GiD_Info Coordinates $conec2 mesh] 0] 1]) && ([lindex [lindex [GiD_Info Coordinates $node mesh] 0] 2] == [lindex [lindex [GiD_Info Coordinates $conec2 mesh] 0] 2]) ) } {
	# 	 	return 1
	# }
	# global PROBLEMTYPEPATH
	# set result [open "$PROBLEMTYPEPATH\\result_new.dat" "w"]
	# puts $result "node: $node"
	# puts $result [lindex [lindex [GiD_Info Coordinates $node mesh] 0] 0]
	# puts $result [lindex [lindex [GiD_Info Coordinates $node mesh] 0] 1]
	# puts $result [lindex [lindex [GiD_Info Coordinates $node mesh] 0] 2]
	# puts $result "conec1: $conec1"
	# puts $result [lindex [lindex [GiD_Info Coordinates $conec1 mesh] 0] 0]
	# puts $result [lindex [lindex [GiD_Info Coordinates $conec1 mesh] 0] 1]
	# puts $result [lindex [lindex [GiD_Info Coordinates $conec1 mesh] 0] 2]
	# puts $result "conec2: $conec2"
	# puts $result [lindex [lindex [GiD_Info Coordinates $conec2 mesh] 0] 0]
	# puts $result [lindex [lindex [GiD_Info Coordinates $conec2 mesh] 0] 1]
	# puts $result [lindex [lindex [GiD_Info Coordinates $conec2 mesh] 0] 2]
	# close $result

	if { ([lindex [lindex [GiD_Info Coordinates $node mesh] 0] 0] == [lindex [lindex [GiD_Info Coordinates $conec1 mesh] 0] 0]) && ([lindex [lindex [GiD_Info Coordinates $node mesh] 0] 1] == [lindex [lindex [GiD_Info Coordinates $conec1 mesh] 0] 1]) && ([lindex [lindex [GiD_Info Coordinates $node mesh] 0] 2] == [lindex [lindex [GiD_Info Coordinates $conec1 mesh] 0] 2]) } {
		return 1
	}

	if { ([lindex [lindex [GiD_Info Coordinates $node mesh] 0] 0] == [lindex [lindex [GiD_Info Coordinates $conec2 mesh] 0] 0]) && ([lindex [lindex [GiD_Info Coordinates $node mesh] 0] 1] == [lindex [lindex [GiD_Info Coordinates $conec2 mesh] 0] 1]) && ([lindex [lindex [GiD_Info Coordinates $node mesh] 0] 2] == [lindex [lindex [GiD_Info Coordinates $conec2 mesh] 0] 2]) } {
		return 2
	}

	return 0

}

proc CompareNodeCoordinatesWithConec123 { node conec1 conec2 conec3 } {

	if { ([lindex [lindex [GiD_Info Coordinates $node mesh] 0] 0] == [lindex [lindex [GiD_Info Coordinates $conec1 mesh] 0] 0]) && ([lindex [lindex [GiD_Info Coordinates $node mesh] 0] 1] == [lindex [lindex [GiD_Info Coordinates $conec1 mesh] 0] 1]) && ([lindex [lindex [GiD_Info Coordinates $node mesh] 0] 2] == [lindex [lindex [GiD_Info Coordinates $conec1 mesh] 0] 2]) } {
		return 1
	}

	if { ([lindex [lindex [GiD_Info Coordinates $node mesh] 0] 0] == [lindex [lindex [GiD_Info Coordinates $conec2 mesh] 0] 0]) && ([lindex [lindex [GiD_Info Coordinates $node mesh] 0] 1] == [lindex [lindex [GiD_Info Coordinates $conec2 mesh] 0] 1]) && ([lindex [lindex [GiD_Info Coordinates $node mesh] 0] 2] == [lindex [lindex [GiD_Info Coordinates $conec2 mesh] 0] 2]) } {
		return 2
	}

	if { ([lindex [lindex [GiD_Info Coordinates $node mesh] 0] 0] == [lindex [lindex [GiD_Info Coordinates $conec3 mesh] 0] 0]) && ([lindex [lindex [GiD_Info Coordinates $node mesh] 0] 1] == [lindex [lindex [GiD_Info Coordinates $conec3 mesh] 0] 1]) && ([lindex [lindex [GiD_Info Coordinates $node mesh] 0] 2] == [lindex [lindex [GiD_Info Coordinates $conec3 mesh] 0] 2]) } {
		return 3
	}

	return 0

}

proc ConvertNodeNumFromFemixToGidOrder { nodeNum } {

	switch $nodeNum {
		2 {
			return 5	
		}
		3 {
			return 2
		}
		4 {
			return 6
		}
		5 {
			return 3
		}
		6 {
			return 7	
		}
		7 {
			return 4	
		}
		default {
			return $nodeNum
		}
	}
	# return $nodeNum
}