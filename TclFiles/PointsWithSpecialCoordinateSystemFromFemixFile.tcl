# PointsWithSpecialCoordinateSystemFromFemixFile.tcl

proc AssignPointsWithSpecialCoordinateSystem { femixData } {

	global numDimension

	set pointsData [GetBlockData $femixData "POINTS_WITH_SPECIAL_COORDINATE_SYSTEM"]

	if { [llength $pointsData] > 0 } {

		set index 7

		while { $index < [llength $pointsData] } {

			set rangeList [GetFirstLastValuesFromRange [lindex $pointsData $index]]

			set firstPoint [lindex $rangeList 0]
	        set lastPoint [lindex $rangeList 1]

	        set index [expr $index + 1]
	        set coordinateSystem [lindex $pointsData $index]

	        GiD_Process Mescape Data Conditions AssignCond Points_With_Special_Local_Axes Change \
	        $coordinateSystem $firstPoint:$lastPoint Mescape

	        set index [expr $index + 5]

	    }
	}
}