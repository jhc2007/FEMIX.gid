proc CreateSurfaceHeatFluxList {} {

	global SurfaceHeatFluxList
	set SurfaceHeatFluxList {}
	return 0
}

proc AddSurfaceHeatFlux { elemType elemNum face numNodes args } {

	global SurfaceHeatFluxList 

	set lastSurfaceHeatFlux [lindex $SurfaceHeatFluxList end]

	set newSurfaceHeatFlux [list $elemNum $elemNum $face]

	if { $numNodes == 1 } {
		lappend newSurfaceHeatFlux $numNodes
		lappend newSurfaceHeatFlux [lindex $args 0]
	} else {

		if { $elemType == 3 } {
			# Quadrilateral
			set faceNodes [lrange [GiD_Info Mesh Elements Quadrilateral $elemNum] 1 end-1]
		    if { [llength $faceNodes] == 8 || [llength $faceNodes] == 9 } {
		        set faceNodes [GetQuadElemNodesInFemixOrder $faceNodes]
		    }

		} elseif { $elemType == 5 } {
			# Hexahedra
			set elemNodes [lrange [GiD_Info Mesh Elements Hexahedra $elemNum] 1 end-1]
	        if { [llength $elemNodes] == 20 } {
	            set elemNodes [GetHexaElemNodesInFemixOrder $elemNodes]
	        }
	        set faceNodes [GetFaceNodes $elemNodes $face]
		}

		lappend newSurfaceHeatFlux [llength $faceNodes]

		if { $numNodes == 4 } {
			for {set i 0} {$i < [llength $faceNodes]} {incr i} {
				lappend newSurfaceHeatFlux [GetNodeValueFrom4NodesSurfaceShapeFunction [lindex $faceNodes $i] [lrange $args 0 [expr $numNodes * 2 - 1]]]
			}
		} else {
			for {set i 0} {$i < [llength $faceNodes]} {incr i} {
				lappend newSurfaceHeatFlux [GetNodeValueFrom8NodesSurfaceShapeFunction [lindex $faceNodes $i] [lrange $args 0 [expr $numNodes * 2 - 1]]]
			}
		}
	}

	# aqui
	
	if { [EqualSurfaceHeatFlux $lastSurfaceHeatFlux $newSurfaceHeatFlux] } {
		set lastSurfaceHeatFlux [lreplace $lastSurfaceHeatFlux 1 1 $elemNum]
		set SurfaceHeatFluxList [lreplace $SurfaceHeatFluxList end end $lastSurfaceHeatFlux]
	} else {
		lappend SurfaceHeatFluxList $newSurfaceHeatFlux
	}

	return 0
}

proc EqualSurfaceHeatFlux { lastSurfaceHeatFlux newSurfaceHeatFlux } {

	for {set i 2} {$i < [llength $newSurfaceHeatFlux]} {incr i} {
		if { [lindex $lastSurfaceHeatFlux $i] != [lindex $newSurfaceHeatFlux $i] } {
			return false
		}
	}

	if { [expr [lindex $lastSurfaceHeatFlux 1] + 1] == [lindex $newSurfaceHeatFlux 0] } {
		return true
	}

	return false
}

proc CountSurfaceHeatFlux {} {

	global SurfaceHeatFluxList

	set count 0
	for {set i 0} {$i < [llength $SurfaceHeatFluxList]} {incr i} {
		set count [expr $count + [lindex [lindex $SurfaceHeatFluxList $i] 1] - [lindex [lindex $SurfaceHeatFluxList $i] 0] + 1]	
	}
	return $count
}

proc WriteSurfaceHeatFluxList {} {

	global SurfaceHeatFluxList 

	set returnString ""
	set index 1

	for {set i 0} {$i < [llength $SurfaceHeatFluxList]} {incr i} {

		set surfaceHeatFlux [lindex $SurfaceHeatFluxList $i]

        if { [lindex $surfaceHeatFlux 0] == [lindex $surfaceHeatFlux 1] } {
            append returnString [format %7d $index]
            append returnString "  [lindex $surfaceHeatFlux 0]"
            incr index
        } else {
            append returnString "  \[$index\-[expr $index + [lindex $surfaceHeatFlux 1] - [lindex $surfaceHeatFlux 0]]\]"
            append returnString "  \[[lindex $surfaceHeatFlux 0]\-[lindex $surfaceHeatFlux 1]\]"
            set index [expr $index + [lindex $surfaceHeatFlux 1] - [lindex $surfaceHeatFlux 0] + 1]
        }
        
        for {set j 2} {$j < [llength $surfaceHeatFlux]} {incr j} {
        	append returnString "  [lindex $surfaceHeatFlux $j]"
        }

        append returnString " ;\n"
	}

	return $returnString
}
