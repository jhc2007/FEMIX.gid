# AuxiliaryVectorsFromFemixFile.tcl
#

# proc CreateFromFileAuxiliaryVectorsFromFemixFile {} {

#     global PROJECTPATH \
#            AuxiliaryVectorsList

#     set AuxiliaryVectorsList {}

#     if { [file exists "$PROJECTPATH\\femix_auxiliary_vectors.dat"] } {
        
#         set auxVectorsFile [open "$PROJECTPATH\\femix_auxiliary_vectors.dat" "r"]
#         set auxVectorsData [read $auxVectorsFile]
#         close $auxVectorsFile

#         set index 0

#         while { $index < [llength $auxVectorsData] } {

#             set auxVector {}

#             for {set i $index} {$i < [expr $index + 3]} {incr i} {
#                 lappend auxVector [lindex $auxVectorsData $i]
#             }

#             incr index 3
            
#             lappend AuxiliaryVectorsList $auxVector
#         }
#     }
# }

# proc CreateFileAuxiliaryVectorsFromFemixFile {} {

#     global PROJECTPATH \
#            AuxiliaryVectorsList

#     if { [llength $AuxiliaryVectorsList] == 0 } {
#         return
#     }

#     set auxVectorsFile [open "$PROJECTPATH\\femix_auxiliary_vectors.dat" "w"]
#     for {set i 0} {$i < [llength $AuxiliaryVectorsList]} {incr i} {
#         foreach var [lindex $AuxiliaryVectorsList $i] {
#             puts $auxVectorsFile $var
#         }
#     }
#     close $auxVectorsFile

#     # destroy $AuxiliaryVectorsFromFemixFile
# }

proc CreateAuxiliaryVectors { femixData } {

    global AuxiliaryVectorsFromFemixFile \
           numDimension

    # set AuxiliaryVectorsFromFemixFile {}
    set index 5

    set auxiliaryVectorsData [GetBlockData $femixData "AUXILIARY_VECTORS"]

    if { $numDimension == 3 } {
        while { $index < [llength $auxiliaryVectorsData] } {
            lappend AuxiliaryVectorsFromFemixFile [list [lindex $auxiliaryVectorsData $index] \
    	                                                [format %.5e [lindex $auxiliaryVectorsData [expr $index + 1]]] \
    	                                                [format %.5e [lindex $auxiliaryVectorsData [expr $index + 2]]] \
    	                                                [format %.5e [lindex $auxiliaryVectorsData [expr $index + 3]]]]
            incr index 6
        }
    } else {
        while { $index < [llength $auxiliaryVectorsData] } {
            lappend AuxiliaryVectorsFromFemixFile [list [lindex $auxiliaryVectorsData $index] \
                                                        [format %.5e [lindex $auxiliaryVectorsData [expr $index + 2]]] \
                                                        [format %.5e [lindex $auxiliaryVectorsData [expr $index + 3]]] \
                                                        [format %.5e [lindex $auxiliaryVectorsData [expr $index + 1]]]]
            incr index 6
        }
    }
}

# proc CountAuxiliaryVectorFromFemixFile {} {

# 	global AuxiliaryVectorsFromFemixFile
# 	return [llength $AuxiliaryVectorsFromFemixFile]	
# }
     
# proc WriteAuxiliaryVectorsFromFemixFile { index } {

#     global AuxiliaryVectorsFromFemixFile \
#            numDimension

#     set auxiliaryVectors ""

#     if { $numDimension == 3 } {
#         for {set i 0} {$i < [llength $AuxiliaryVectorsFromFemixFile]} {incr i} {
#             append auxiliaryVectors [format %7s $index]
#             append auxiliaryVectors "  [lindex [lindex $AuxiliaryVectorsFromFemixFile $i] 0]"
#             append auxiliaryVectors [format %.5e [lindex [lindex $AuxiliaryVectorsFromFemixFile $i] 1]]
#             append auxiliaryVectors [format %.5e [lindex [lindex $AuxiliaryVectorsFromFemixFile $i] 2]]
#             append auxiliaryVectors [format %.5e [lindex [lindex $AuxiliaryVectorsFromFemixFile $i] 3]]
#             append auxiliaryVectors " ;\n"
#             set index [expr $index + 1]
#         }
#     } else {
#         for {set i 0} {$i < [llength $AuxiliaryVectorsFromFemixFile]} {incr i} {
#             append auxiliaryVectors [format %7s $index]
#             append auxiliaryVectors "  [lindex [lindex $AuxiliaryVectorsFromFemixFile $i] 0]"
#             append auxiliaryVectors [format %.5e [lindex [lindex $AuxiliaryVectorsFromFemixFile $i] 3]]
#             append auxiliaryVectors [format %.5e [lindex [lindex $AuxiliaryVectorsFromFemixFile $i] 1]]
#             append auxiliaryVectors [format %.5e [lindex [lindex $AuxiliaryVectorsFromFemixFile $i] 2]]
#             append auxiliaryVectors " ;\n"
#             set index [expr $index + 1]
#         }
#     }

#     return $auxiliaryVectors
# }

proc GetAuxiliaryVectorNameFromFemixFile { vector } {

    global AuxiliaryVectorsFromFemixFile

    for {set i 0} {$i < [llength $AuxiliaryVectorsFromFemixFile]} {incr i} {
        set auxVector [lrange [lindex $AuxiliaryVectorsFromFemixFile $i] 1 end]
        if { double([lindex $auxVector 0] == [lindex $vector 0]) && double([lindex $auxVector 1] == [lindex $vector 1]) && double([lindex $auxVector 2] == [lindex $vector 2]) } {
            return [lindex [lindex $AuxiliaryVectorsFromFemixFile $i] 0]
        }
    }
    return ""    
}

proc GetAuxiliaryVectorFromFemixFile { vectorName } {

    global AuxiliaryVectorsFromFemixFile
   
    for {set i 0} {$i < [llength $AuxiliaryVectorsFromFemixFile]} {incr i} {
        if { [lindex [lindex $AuxiliaryVectorsFromFemixFile $i] 0] == $vectorName } {
            return [lrange [lindex $AuxiliaryVectorsFromFemixFile $i] 1 end]
        }
    }
}

# proc GetAuxiliaryVector { vectorName } {

#     global AuxiliaryVectorsFromFemixFile 
#            # numDimension

#     for {set i 0} {$i < [llength $AuxiliaryVectorsFromFemixFile]} {incr i} {
#         if { [lindex [lindex $AuxiliaryVectorsFromFemixFile $i] 0] == $vectorName } {
#             # if { $numDimension == 3 } {
#             return [lrange [lindex $AuxiliaryVectorsFromFemixFile $i] 1 end]
#             # return [list [lindex [lindex $AuxiliaryVectorsFromFemixFile $i] 1] \
#             #              [lindex [lindex $AuxiliaryVectorsFromFemixFile $i] 3] ]
#             #              [lindex [lindex $AuxiliaryVectorsFromFemixFile $i] 2] \
#             # } else {
#             #   return [list [lindex [lindex $AuxiliaryVectorsFromFemixFile $i] 2] \
#             #                  [lindex [lindex $AuxiliaryVectorsFromFemixFile $i] 3] \
#             #                  [lindex [lindex $AuxiliaryVectorsFromFemixFile $i] 1] ]
#             # }
#         }
#     }

#     return [list 0 0 0]
# }
