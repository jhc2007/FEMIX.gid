
proc CreateLinIntLine3D { femixData } {

    set linIntLine3DData [GetBlockData $femixData "LINEAR_INTERFACE_LINE_3D_PROPERTIES"]

    if { [llength $linIntLine3DData] > 0 } {

        global doubleFormatMatParameters
        
        set allMaterialsNameList [GiD_Info materials]
        set linIntLine3DNameList {}

        for {set i 0} {$i < [llength $allMaterialsNameList]} {incr i} {
            if { [lindex [GiD_Info materials [lindex $allMaterialsNameList $i]] 2] == "LIN_INT_LINE_3D" } {
                lappend linIntLine3DNameList [string toupper [lindex $allMaterialsNameList $i]]
            }
        }

    	set numMaterials [lindex $linIntLine3DData 2]

    	set index 4

    	for {set i 0} {$i < $numMaterials} {incr i} {

    		set materialName [lindex $linIntLine3DData [expr $index + 1]]

    		set materialData { LIN_INT_LINE_3D }

    		lappend materialData "[format $doubleFormatMatParameters [lindex $linIntLine3DData [expr $index + 2]]]"
	    	lappend materialData "[format $doubleFormatMatParameters [lindex $linIntLine3DData [expr $index + 3]]]"
            lappend materialData "[format $doubleFormatMatParameters [lindex $linIntLine3DData [expr $index + 4]]]"

	    	if { [ExistsListElement $linIntLine3DNameList [string toupper $materialName]] } {
	    		GiD_ModifyData materials $materialName $materialData
	    	} else {
	    		GiD_CreateData create material Linear_Interface_Line_3D $materialName $materialData
	    	}

        	set index [expr $index + 6]  
	    }
	}
}
