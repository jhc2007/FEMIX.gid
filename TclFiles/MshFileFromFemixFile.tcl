# PostMshFileFromFemixFile.tcl
#
# CreateMshFileFromFemixFile { femixFile femixData }
# PutsElemTypeHeader { postMshFile numNodesElem }
# GetPointCoordinatesFromFemixFile { fileData }
# CreateSolidHexaRanges { fileData }
# IsSolidHexa { element }
# GetNumCombinationsFromFemixFile { fileData }

proc CreateMshFileFromFemixFile { femixFile femixData } {

    global solidHexaRanges \
           numCombinations \
           doubleFormatCoordinates \
           InterfaceElementsList

    set mshFilePath "[string range $femixFile 0 [expr [string length $femixFile] - 8]].msh"

    set mshFile [open $mshFilePath "w"]

    # set femixFile [open "$femixFile" "r"]
    # set femixData [read $femixFile]
    # close $femixFile 

    CreateSolidHexaRanges $femixData
    set coordinates [GetPointCoordinatesFromFemixFile $femixData]
    set numCombinations [GetNumCombinationsFromFemixFile $femixData]

    set index 0

    while { $index < [llength $femixData] && [lindex $femixData $index] != "<ELEMENT_NODES>" } {
        incr index
    }

    while { $index < [llength $femixData] && ![string is integer [lindex $femixData $index]] } {
        incr index
    }

    if { $index < [llength $femixData] } {

        set numElems [lindex $femixData $index]
        incr index
        
        while { $index < [llength $femixData] && ![string is integer [lindex $femixData $index]] } {
            incr index
        }

        set numNodesElem [lindex $femixData [expr $index + 1]]

        if { $numNodesElem == 8 && [IsSolidHexa [lindex $femixData $index]] } {
            set numNodesElem 0
        }

        if { $numNodesElem == 4 && [IsInterfaceElement [lindex $femixData $index]] && [PointsWithSameCoordinates $coordinates [lindex $femixData [expr $index + 2]] [lindex $femixData [expr $index + 4]]] } {
            set numNodesElem 1
        }

        if { $numNodesElem == 6 && [PointsWithSameCoordinates $coordinates [lindex $femixData [expr $index + 2]] [lindex $femixData [expr $index + 5]]] } {
            set numNodesElem 5
        }        
                       
        PutsElemTypeHeader $mshFile $numNodesElem

        puts $mshFile "Coordinates\n"

        for {set i 0} {$i < [llength $coordinates]} {incr i} {
            puts $mshFile [format %8s$doubleFormatCoordinates$doubleFormatCoordinates$doubleFormatCoordinates [lindex [lindex $coordinates $i] 0] [lindex [lindex $coordinates $i] 1] [lindex [lindex $coordinates $i] 2] [lindex [lindex $coordinates $i] 3]]
        }

        set numPoints [expr [llength $coordinates] + 1]

        set auxIndex $index
        for {set i 0} {$i < [llength $InterfaceElementsList]} {incr i} {
            while { [lindex $femixData $auxIndex] != [lindex [lindex $InterfaceElementsList $i] 0] } {
                set auxIndex [expr $auxIndex + 3 + [lindex $femixData [expr $auxIndex + 1]]]
            }
            if { [lindex $femixData [expr $auxIndex + 1]] == 6 && ![PointsWithSameCoordinates $coordinates [lindex $femixData [expr $auxIndex + 2]] [lindex $femixData [expr $auxIndex + 5]]] } {
                for {set j [lindex [lindex $InterfaceElementsList $i] 0]} {$j <= [lindex [lindex $InterfaceElementsList $i] 1]} {incr j} {
                    set pointCoord [GetMiddlePointCoordinates $coordinates [lindex $femixData [expr $auxIndex + 2]] [lindex $femixData [expr $auxIndex + 5]]]
                    puts $mshFile [format %8s$doubleFormatCoordinates$doubleFormatCoordinates$doubleFormatCoordinates $numPoints [lindex $pointCoord 0] [lindex $pointCoord 1] [lindex $pointCoord 2]]
                    set pointCoord [GetMiddlePointCoordinates $coordinates [lindex $femixData [expr $auxIndex + 4]] [lindex $femixData [expr $auxIndex + 7]]]
                    puts $mshFile [format %8s$doubleFormatCoordinates$doubleFormatCoordinates$doubleFormatCoordinates [expr $numPoints + 1] [lindex $pointCoord 0] [lindex $pointCoord 1] [lindex $pointCoord 2]]
                    set numPoints [expr $numPoints + 2]
                    set auxIndex [expr $auxIndex + 3 + [lindex $femixData [expr $auxIndex + 1]]]
                }                
            }
        }
        set numPoints [expr [llength $coordinates] + 1]

        puts $mshFile "\nEnd Coordinates\n"

        puts $mshFile "Elements\n"

        for {set i 0} {$i < $numElems} {incr i} {

            set tmpNumNodesElem [lindex $femixData [expr $index + 1]]

            if { $tmpNumNodesElem == 8 && [IsSolidHexa [lindex $femixData $index]] } {
                set tmpNumNodesElem 0
            }

            if { $tmpNumNodesElem == 4 && [IsInterfaceElement [lindex $femixData $index]] && [PointsWithSameCoordinates $coordinates [lindex $femixData [expr $index + 2]] [lindex $femixData [expr $index + 4]]] } {
                set tmpNumNodesElem 1
            }

            if { $tmpNumNodesElem == 6 && [PointsWithSameCoordinates $coordinates [lindex $femixData [expr $index + 2]] [lindex $femixData [expr $index + 5]]] } {
                set tmpNumNodesElem 5
            }

            if { $tmpNumNodesElem != $numNodesElem } {
                set numNodesElem $tmpNumNodesElem
                puts $mshFile "\nEnd Elements\n"
                PutsElemTypeHeader $mshFile $numNodesElem
                puts $mshFile "Coordinates\n"
                puts $mshFile "End Coordinates\n"
                puts $mshFile "Elements\n"
            }

            # puts $result [lindex $femixData $index]
            # puts $result $numNodesElem
            # puts $result [IsInterfaceElement [lindex $femixData $index]]
            
            switch $numNodesElem {

                0 {
                    # Hexahedra 8 nodes
                    puts $mshFile [format %8s%8s%8s%8s%8s%8s%8s%8s%8s \
                                  [lindex $femixData $index] \
                                  [lindex $femixData [expr $index + 2]] \
                                  [lindex $femixData [expr $index + 3]] \
                                  [lindex $femixData [expr $index + 4]] \
                                  [lindex $femixData [expr $index + 5]] \
                                  [lindex $femixData [expr $index + 6]] \
                                  [lindex $femixData [expr $index + 7]] \
                                  [lindex $femixData [expr $index + 8]] \
                                  [lindex $femixData [expr $index + 9]]]
                    incr index 11               
                }
                1 {
                    # Linear Interface Elements 2 nodes
                    puts $mshFile [format %8s%8s%8s \
                                  [lindex $femixData $index] \
                                  [lindex $femixData [expr $index + 2]] \
                                  [lindex $femixData [expr $index + 3]]]
                    incr index 7
                }
                2 { 
                    # Linear 2 nodes
                    puts $mshFile [format %8s%8s%8s \
                                  [lindex $femixData $index] \
                                  [lindex $femixData [expr $index + 2]] \
                                  [lindex $femixData [expr $index + 3]]]
                    incr index 5
                }
                3 {
                    # Linear 3 nodes
                    puts $mshFile [format %8s%8s%8s%8s \
                                  [lindex $femixData $index] \
                                  [lindex $femixData [expr $index + 2]] \
                                  [lindex $femixData [expr $index + 4]] \
                                  [lindex $femixData [expr $index + 3]]]
                    incr index 6
                }
                4 {
                    # Quadrilateral 4 nodes
                    if { [IsInterfaceElement [lindex $femixData $index]] } {
                        puts $mshFile [format %8s%8s%8s%8s%8s \
                                      [lindex $femixData $index] \
                                      [lindex $femixData [expr $index + 2]] \
                                      [lindex $femixData [expr $index + 3]] \
                                      [lindex $femixData [expr $index + 5]] \
                                      [lindex $femixData [expr $index + 4]]]
                    } else {
                        puts $mshFile [format %8s%8s%8s%8s%8s \
                                      [lindex $femixData $index] \
                                      [lindex $femixData [expr $index + 2]] \
                                      [lindex $femixData [expr $index + 3]] \
                                      [lindex $femixData [expr $index + 4]] \
                                      [lindex $femixData [expr $index + 5]]]
                    }
                    incr index 7
                }
                5 {
                    # Linear Interface Elements 3 nodes
                    puts $mshFile [format %8s%8s%8s%8s \
                                  [lindex $femixData $index] \
                                  [lindex $femixData [expr $index + 2]] \
                                  [lindex $femixData [expr $index + 4]] \
                                  [lindex $femixData [expr $index + 3]]]
                    incr index 9
                }
                6 {
                    # Quadrilateral Interface Elements 8 nodes
                    puts $mshFile [format %8s%8s%8s%8s%8s%8s%8s%8s%8s \
                                  [lindex $femixData $index] \
                                  [lindex $femixData [expr $index + 2]] \
                                  [lindex $femixData [expr $index + 4]] \
                                  [lindex $femixData [expr $index + 7]] \
                                  [lindex $femixData [expr $index + 5]] \
                                  [lindex $femixData [expr $index + 3]] \
                                  [expr $numPoints + 1] \
                                  [lindex $femixData [expr $index + 6]] \
                                  $numPoints]

                    set numPoints [expr $numPoints + 2]
                    incr index 9
                }
                8 {
                    # Quadrilateral 8 nodes
                    puts $mshFile [format %8s%8s%8s%8s%8s%8s%8s%8s%8s \
                                  [lindex $femixData $index] \
                                  [lindex $femixData [expr $index + 2]] \
                                  [lindex $femixData [expr $index + 4]] \
                                  [lindex $femixData [expr $index + 6]] \
                                  [lindex $femixData [expr $index + 8]] \
                                  [lindex $femixData [expr $index + 3]] \
                                  [lindex $femixData [expr $index + 5]] \
                                  [lindex $femixData [expr $index + 7]] \
                                  [lindex $femixData [expr $index + 9]]]
                    incr index 11
                }
                9 {
                    # Quadrilateral 9 nodes
                    puts $mshFile [format %8s%8s%8s%8s%8s%8s%8s%8s%8s%8s \
                                  [lindex $femixData $index] \
                                  [lindex $femixData [expr $index + 2]] \
                                  [lindex $femixData [expr $index + 4]] \
                                  [lindex $femixData [expr $index + 6]] \
                                  [lindex $femixData [expr $index + 8]] \
                                  [lindex $femixData [expr $index + 3]] \
                                  [lindex $femixData [expr $index + 5]] \
                                  [lindex $femixData [expr $index + 7]] \
                                  [lindex $femixData [expr $index + 9]] \
                                  [lindex $femixData [expr $index + 10]]]
                    incr index 12
                }
                20 {
                    # Hexahedra 20 nodes
                    puts $mshFile [format %8s%8s%8s%8s%8s%8s%8s%8s%8s%8s%8s%8s%8s%8s%8s%8s%8s%8s%8s%8s%8s \
                                  [lindex $femixData $index] \
                                  [lindex $femixData [expr $index + 2]] \
                                  [lindex $femixData [expr $index + 4]] \
                                  [lindex $femixData [expr $index + 6]] \
                                  [lindex $femixData [expr $index + 8]] \
                                  [lindex $femixData [expr $index + 14]] \
                                  [lindex $femixData [expr $index + 16]] \
                                  [lindex $femixData [expr $index + 18]] \
                                  [lindex $femixData [expr $index + 20]] \
                                  [lindex $femixData [expr $index + 3]] \
                                  [lindex $femixData [expr $index + 5]] \
                                  [lindex $femixData [expr $index + 7]] \
                                  [lindex $femixData [expr $index + 9]] \
                                  [lindex $femixData [expr $index + 10]] \
                                  [lindex $femixData [expr $index + 11]] \
                                  [lindex $femixData [expr $index + 12]] \
                                  [lindex $femixData [expr $index + 13]] \
                                  [lindex $femixData [expr $index + 15]] \
                                  [lindex $femixData [expr $index + 17]] \
                                  [lindex $femixData [expr $index + 19]] \
                                  [lindex $femixData [expr $index + 21]]]
                    incr index 23
                }
            }
        }
        puts $mshFile "\nEnd Elements\n"
    }

    #puts $mshFile [GiD_Info Geometry NumVolumes]
    #puts $mshFile [GiD_Info Geometry NumSurfaces]
    #puts $mshFile [GiD_Info Geometry NumLines]
    #puts $mshFile [GiD_Info Geometry NumPoints]

    #set s [GiD_Info Geometry NumSurfaces]

    close $mshFile

    #GiD_Geometry delete surface {1 $s}
    
# global PROBLEMTYPEPATH 
# set result [open "$PROBLEMTYPEPATH\\result_new.dat" "w"]
# puts $result $numPoints
# puts $result [expr [llength $coordinates] + 1]

    # if { $numPoints > [expr [llength $coordinates] + 1] } {
    #     set firstPoint [expr [llength $coordinates] + 1]
    #     set lastPoint [expr $numPoints - 1]
    #     GiD_Process Mescape \
    #     Utilities Collapse nodes $firstPoint:$lastPoint Mescape
    # }

# close $result

    #GiD_Process Mescape Files New
    #GiD_Process Meshing CancelMesh
    #GiD_Process Files MeshRead
    LoadFileInGid "$mshFilePath"

    # aqui
    file delete $mshFilePath

    #ExecuteFileCommand "MeshRead" "PRE" "saving" $mshFilePath
    #GiD_Process Mescape Files "Read" $mshFilePath

  # close $result
    
    # destroy $femixFile $femixData $mshFile
}

proc GetMiddlePointCoordinates { coordinates point1 point2 } {

  # global numDimension

  set numPoints 0
  set pointsCoordinates {} 
  set index 0

  while { $index < [llength $coordinates] && $numPoints != 2 } {
    if { [lindex [lindex $coordinates $index] 0] == $point1 || [lindex [lindex $coordinates $index] 0] == $point2 } {
      lappend pointsCoordinates [lindex $coordinates $index]
      set numPoints [expr $numPoints + 1]
    }
    incr index
  }
  set index {}
  if { $numPoints == 2 } {
    for {set i 1} {$i < 4} {incr i} {
      lappend index [expr double([lindex [lindex $pointsCoordinates 0] $i] + [lindex [lindex $pointsCoordinates 1] $i]) / 2]
    }
  }
  return $index
}

proc PointsWithSameCoordinates { coordinates point1 point2 } {

  set numPoints 0
  set pointsCoordinates {} 
  set index 0

  while { $index < [llength $coordinates] && $numPoints != 2 } {
    if { [lindex [lindex $coordinates $index] 0] == $point1 || [lindex [lindex $coordinates $index] 0] == $point2 } {
      lappend pointsCoordinates [lindex $coordinates $index]
      set numPoints [expr $numPoints + 1]
    }
    incr index
  }
  if { $numPoints == 2 } {
    for {set i 1} {$i < 4} {incr i} {
      if { [lindex [lindex $pointsCoordinates 0] $i] != [lindex [lindex $pointsCoordinates 1] $i] } {
        return false
      }
    }
    return true
  }
  return false
}

proc PutsElemTypeHeader { mshFile numNodesElem } {

    switch $numNodesElem {

        0 {
            puts $mshFile "MESH dimension 3 ElemType Hexahedra Nnode 8\n" 
        }
        1 {
            puts $mshFile "MESH dimension 3 ElemType Linear Nnode 2\n"
        }
        2 {
            puts $mshFile "MESH dimension 3 ElemType Linear Nnode 2\n"
        }
        3 {
            puts $mshFile "MESH dimension 3 ElemType Linear Nnode 3\n"
        }
        4 {
            puts $mshFile "MESH dimension 3 ElemType Quadrilateral Nnode 4\n"
        }
        5 {
            puts $mshFile "MESH dimension 3 ElemType Linear Nnode 3\n"
        }
        6 {
            puts $mshFile "MESH dimension 3 ElemType Quadrilateral Nnode 8\n"
        }
        8 {
            puts $mshFile "MESH dimension 3 ElemType Quadrilateral Nnode 8\n"
        }
        9 {
            puts $mshFile "MESH dimension 3 ElemType Quadrilateral Nnode 9\n"
        }
        20 {
            puts $mshFile "MESH dimension 3 ElemType Hexahedra Nnode 20\n"
        }
    }
}

proc GetPointCoordinatesFromFemixFile { fileData } {

    global numDimension 
    #solidHexaRanges



    set index 0
    set coordinates {}

    while { $index < [llength $fileData] && [lindex $fileData $index] != "<POINT_COORDINATES>" } {
        incr index
    }

    while { $index < [llength $fileData] && ![string is integer [lindex $fileData $index]] } {
        incr index
    }

    if { $index < [llength $fileData] } {

        set numPoints [lindex $fileData $index]
        incr index

        while { $index < [llength $fileData] && ![string is integer [lindex $fileData $index]] } {
            incr index
        }

        if { $index < [llength $fileData] } { 
            # && $numDimension > 0

            if { $numDimension == 3 } {
                # Num of Dimensions == 3
                for {set i 0} {$i < $numPoints } {incr i} {
                    set pointCoord {}
                    lappend pointCoord [lindex $fileData $index]
                    lappend pointCoord [lindex $fileData [expr $index + 1]]
                    lappend pointCoord [lindex $fileData [expr $index + 2]]
                    lappend pointCoord [lindex $fileData [expr $index + 3]]
                    lappend coordinates $pointCoord
                    set index [expr $index + 5]
                }

            } else {
                # Num of Dimensions == 2
                for {set i 0} {$i < $numPoints } {incr i} {
                    set pointCoord {}
                    lappend pointCoord [lindex $fileData $index]
                    lappend pointCoord [lindex $fileData [expr $index + 2]]
                    lappend pointCoord [lindex $fileData [expr $index + 3]]
                    lappend pointCoord [lindex $fileData [expr $index + 1]]
                    lappend coordinates $pointCoord
                    set index [expr $index + 5]
                }                
            }
        }
    }
    return $coordinates
}

proc CreateSolidHexaRanges { fileData } {

    global solidHexaRanges
    # numDimension

    set solidHexaRanges {}

    set index 0

    while { $index < [llength $fileData] && [lindex $fileData $index] != "<ELEMENT_PROPERTIES>" } {
        incr index
    }

    while { $index < [llength $fileData] && ![string is integer [lindex $fileData $index]] } {
        incr index
    }

    set numElemProp [lindex $fileData $index]
    incr index

    while { $index < [llength $fileData] && ![string is integer [lindex $fileData $index]] } {
        incr index
    }

    if { $index < [llength $fileData] } {
        
        if { [lindex $fileData [expr $index + 4]] == "_SOLID_HEXA" || [lindex $fileData [expr $index + 4]] == "_SOLID_HEXA_THERMAL" } {
            set rangeList [GetFirstLastValuesFromRange [lindex $fileData [expr $index + 3]]]
            lappend solidHexaRanges $rangeList
            # set numDimension 3
        } 
        # else {
        #     set numDimension [GetNumDimension [lindex $fileData [expr $index + 4]]]
        # }

        incr index 12

        for {set i 1} {$i < $numElemProp} {incr i} {
            if { [lindex $fileData [expr $index + 4]] == "_SOLID_HEXA" || [lindex $fileData [expr $index + 4]] == "_SOLID_HEXA_THERMAL" } {
                # set newRange {}
                set rangeList [GetFirstLastValuesFromRange [lindex $fileData [expr $index + 3]]]
                # if { [string is integer $strRange] } {
                #   lappend newRange $strRange
                #   lappend newRange $strRange
                # } else {
                #   lappend newRange [string range $strRange 1 [expr [string first "-" $strRange] - 1]]
                #   lappend newRange [string range $strRange [expr [string first "-" $strRange] + 1] [expr [string length $strRange] - 2]]
                # }        
                lappend solidHexaRanges $rangeList
            }
            incr index 12
        }
    }
}

proc IsSolidHexa { element } {

    global solidHexaRanges

    for {set i 0} {$i < [llength $solidHexaRanges]} {incr i} {
        if { $element >= [lindex [lindex $solidHexaRanges $i] 0] && $element <= [lindex [lindex $solidHexaRanges $i] 1] } {
            return true
        }
    }
    return false
}

proc GetNumCombinationsFromFemixFile { fileData } {

    set index 0

    while { $index < [llength $fileData] && [lindex $fileData $index] != "<NUMBER_OF_COMBINATIONS>" } {
        incr index
    }

    return [lindex $fileData [expr $index + 2]]
}