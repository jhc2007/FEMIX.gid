# CoordinateSystemsFromFemixFile.tcl

proc AssignCoordinateSystems { femixData } {

    # global CoordinateSystemsFromFemixFile

    # set CoordinateSystemsFromFemixFile {}
    set index 5

    set coordinateSystemsData [GetBlockData $femixData "COORDINATE_SYSTEMS"]

    while { $index < [llength $coordinateSystemsData] } {
        # lappend CoordinateSystemsFromFemixFile [list [lindex $coordinateSystemsData $index] \
	       #                                           [lindex $coordinateSystemsData [expr $index + 1]] \
	       #                                           [lindex $coordinateSystemsData [expr $index + 2]] \
	       #                                           [lindex $coordinateSystemsData [expr $index + 3]]]

	   	set localAxisName [lindex $coordinateSystemsData $index]
		set xAxis [GetAuxiliaryVectorFromFemixFile [lindex $coordinateSystemsData [expr $index + 1]]]
		# set yAxis [GetAuxiliaryVectorFromFemixFile [lindex $coordinateSystemsData [expr $index + 2]]]
		set zAxis [GetAuxiliaryVectorFromFemixFile [lindex $coordinateSystemsData [expr $index + 3]]]

		# AddAuxiliaryVector [lindex $xAxis 0] [lindex $xAxis 1] [lindex $xAxis 2]
		# AddAuxiliaryVector [lindex $yAxis 0] [lindex $yAxis 1] [lindex $yAxis 2]
		# AddAuxiliaryVector [lindex $zAxis 0] [lindex $zAxis 1] [lindex $zAxis 2]

        if { [ExistsCenterNode] } {
    		GiD_Process Mescape Data \
            LocalAxes DefineLocAxes $localAxisName 3PointsXZ 0 0 0 old \
            		  [lindex $xAxis 0] [lindex $xAxis 1] [lindex $xAxis 2] \
            		  [lindex $zAxis 0] [lindex $zAxis 1] [lindex $zAxis 2] \
            Mescape
        } else {
            GiD_Process Mescape Data \
            LocalAxes DefineLocAxes $localAxisName 3PointsXZ 0 0 0 \
                      [lindex $xAxis 0] [lindex $xAxis 1] [lindex $xAxis 2] \
                      [lindex $zAxis 0] [lindex $zAxis 1] [lindex $zAxis 2] \
            Mescape
        }
        
        incr index 6
    }

    # AssignLocalAxes
}

proc ExistsCenterNode {} {

    set nodesCoordinates [GiD_Info Mesh Nodes]
    set index 0
    while { $index < [llength $nodesCoordinates] } {
        if { double([lindex $nodesCoordinates [expr $index + 1]] == 0) && double([lindex $nodesCoordinates [expr $index + 2]] == 0) && double([lindex $nodesCoordinates [expr $index + 3]] == 0) } {
            return true
        }
        incr index 4
    }
    return false
}

# proc AssignLocalAxes {} {

# 	global CoordinateSystemsFromFemixFile

# 	for {set i 0} {$i < [llength $CoordinateSystemsFromFemixFile]} {incr i} {
		
# 		set localAxisName [lindex [lindex $CoordinateSystemsFromFemixFile $i] 0]
# 		set xAxis [GetAuxiliaryVectorCoordinates [lindex [lindex $CoordinateSystemsFromFemixFile $i] 1]]
# 		set zAxis [GetAuxiliaryVectorCoordinates [lindex [lindex $CoordinateSystemsFromFemixFile $i] 3]]

# 		GiD_Process Mescape Data \
#         LocalAxes DefineLocAxes $localAxisName 3PointsXZ 0 0 0 \
#         												 [lindex $xAxis 0] [lindex $xAxis 1] [lindex $xAxis 2] \
#         												 [lindex $zAxis 0] [lindex $zAxis 1] [lindex $zAxis 2] \
#         Mescape
# 	}
# }

# proc GetAuxiliaryVector { vectorName } {

# 	global AuxiliaryVectorsFromFemixFile 
# 	       # numDimension

# 	for {set i 0} {$i < [llength $AuxiliaryVectorsFromFemixFile]} {incr i} {
# 		if { [lindex [lindex $AuxiliaryVectorsFromFemixFile $i] 0] == $vectorName } {
# 			# if { $numDimension == 3 } {
# 			return [lrange [lindex $AuxiliaryVectorsFromFemixFile $i] 1 end]
# 			# return [list [lindex [lindex $AuxiliaryVectorsFromFemixFile $i] 1] \
# 		    #              [lindex [lindex $AuxiliaryVectorsFromFemixFile $i] 3] ]
# 		    #              [lindex [lindex $AuxiliaryVectorsFromFemixFile $i] 2] \
# 			# } else {
# 			# 	return [list [lindex [lindex $AuxiliaryVectorsFromFemixFile $i] 2] \
# 			#                  [lindex [lindex $AuxiliaryVectorsFromFemixFile $i] 3] \
# 			#                  [lindex [lindex $AuxiliaryVectorsFromFemixFile $i] 1] ]
# 			# }
# 		}
# 	}

# 	return [list 0 0 0]
# }

# proc CountLocalAxesFromFemixFile {} {

# 	global CoordinateSystemsFromFemixFile

# 	return [llength $CoordinateSystemsFromFemixFile]
# }

# proc LocalAxisFromFemixFile { num } {

# 	global CoordinateSystemsFromFemixFile

# 	set localAxisName [GetLocalAxesName $num]

# 	for {set i 0} {$i < [llength $CoordinateSystemsFromFemixFile]} {incr i} {
# 		if { [lindex [lindex $CoordinateSystemsFromFemixFile $i] 0] == $localAxisName } {
# 			return 1
# 		}		
# 	}	
# 	return 0
# }