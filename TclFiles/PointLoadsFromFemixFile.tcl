# PointLoadsFromFemixFile.tcl
#
# AssignInternalPointLoads { loadCaseData }
# AssignPointLoads { loadCaseData }
# GetPointLoadValues { pointLoadsData index }

proc AssignInternalPointLoads { loadCaseData } {

	global numDimension 
	# strengthUnit momentUnit

    set internalPointLoadsData [GetBlockData $loadCaseData "INTERNAL_POINT_LOADS"]

	if { [llength $internalPointLoadsData] > 0 } {

		set index 5

		if { $numDimension == 3 } {

			while { $index < [llength $internalPointLoadsData] } {

				set elementNum [lindex $internalPointLoadsData $index]
				incr index
				set rangeList [GetFirstLastValuesFromRange [lindex $internalPointLoadsData $index]]

				set firstPoint [lindex $rangeList 0]
				set lastPoint [lindex $rangeList 1]

				set pointLoadValues [GetPointLoadValues $internalPointLoadsData $index]

	   #       GiD_Process Mescape Data \
       #       Conditions AssignCond _Point_Loads Change \
	   #      		[lindex $pointLoadValues 0]$strengthUnit \
	   #      		[lindex $pointLoadValues 1]$strengthUnit \
	   #      		[lindex $pointLoadValues 2]$strengthUnit \
	   #      		[lindex $pointLoadValues 3]$momentUnit \
	   #      		[lindex $pointLoadValues 4]$momentUnit \
	   #      		[lindex $pointLoadValues 5]$momentUnit \
	   #      		1 $elementNum \
	   #      		$firstPoint:$lastPoint \
	   #      	Mescape

	   			GiD_Process Mescape Data \
        		Conditions AssignCond _Point_Loads Change \
	        		[lindex $pointLoadValues 0] \
	        		[lindex $pointLoadValues 1] \
	        		[lindex $pointLoadValues 2] \
	        		[lindex $pointLoadValues 3] \
	        		[lindex $pointLoadValues 4] \
	        		[lindex $pointLoadValues 5] \
	        		1 $elementNum \
	        		$firstPoint:$lastPoint \
	        	Mescape

				incr index 9
			}

		} else {
			# $numDimension == 2
			while { $index < [llength $internalPointLoadsData] } {

				set elementNum [lindex $internalPointLoadsData $index]
				incr index
				set rangeList [GetFirstLastValuesFromRange [lindex $internalPointLoadsData $index]]

				set firstPoint [lindex $rangeList 0]
				set lastPoint [lindex $rangeList 1]

				set pointLoadValues [GetPointLoadValues $internalPointLoadsData $index]

	   #        GiD_Process Mescape Data \
       #     	Conditions AssignCond _Point_Loads Change \
	   #      		[lindex $pointLoadValues 1]$strengthUnit \
	   #      		[lindex $pointLoadValues 2]$strengthUnit \
	   #      		[lindex $pointLoadValues 0]$strengthUnit \
	   #      		[lindex $pointLoadValues 4]$momentUnit \
	   #      		[lindex $pointLoadValues 5]$momentUnit \
	   #      		[lindex $pointLoadValues 3]$momentUnit \
	   #      		1 $elementNum \
	   #      		$firstPoint:$lastPoint \
	   #      	Mescape

   				GiD_Process Mescape Data \
        		Conditions AssignCond _Point_Loads Change \
	        		[lindex $pointLoadValues 1] \
	        		[lindex $pointLoadValues 2] \
	        		[lindex $pointLoadValues 0] \
	        		[lindex $pointLoadValues 4] \
	        		[lindex $pointLoadValues 5] \
	        		[lindex $pointLoadValues 3] \
	        		1 $elementNum \
	        		$firstPoint:$lastPoint \
	        	Mescape

				incr index 9
			}
		}
	}  
}

proc AssignPointLoads { loadCaseData } {

	global numDimension 
	# strengthUnit momentUnit

    set pointLoadsData [GetBlockData $loadCaseData "POINT_LOADS"]

	if { [llength $pointLoadsData] > 0 } {

		set index 5

		if { $numDimension == 3 } {

			while { $index < [llength $pointLoadsData] } {
				        
				set rangeList [GetFirstLastValuesFromRange [lindex $pointLoadsData $index]]

				set firstPoint [lindex $rangeList 0]
				set lastPoint [lindex $rangeList 1]

				set pointLoadValues [GetPointLoadValues $pointLoadsData $index]

	   #        GiD_Process Mescape Data \
       #     	Conditions AssignCond _Point_Loads Change \
	   #      		[lindex $pointLoadValues 0]$strengthUnit \
	   #      		[lindex $pointLoadValues 1]$strengthUnit \
	   #      		[lindex $pointLoadValues 2]$strengthUnit \
	   #      		[lindex $pointLoadValues 3]$momentUnit \
	   #      		[lindex $pointLoadValues 4]$momentUnit \
	   #      		[lindex $pointLoadValues 5]$momentUnit \
	   #      		0 0 \
	   #      		$firstPoint:$lastPoint \
	   #      	Mescape

   				GiD_Process Mescape Data \
        		Conditions AssignCond _Point_Loads Change \
	        		[lindex $pointLoadValues 0] \
	        		[lindex $pointLoadValues 1] \
	        		[lindex $pointLoadValues 2] \
	        		[lindex $pointLoadValues 3] \
	        		[lindex $pointLoadValues 4] \
	        		[lindex $pointLoadValues 5] \
	        		0 0 \
	        		$firstPoint:$lastPoint \
	        	Mescape

				incr index 9
			}

		} else {
			# $numDimension == 2
			while { $index < [llength $pointLoadsData] } {
				        
				set rangeList [GetFirstLastValuesFromRange [lindex $pointLoadsData $index]]

				set firstPoint [lindex $rangeList 0]
				set lastPoint [lindex $rangeList 1]

				set pointLoadValues [GetPointLoadValues $pointLoadsData $index]

	   #        GiD_Process Mescape Data \
       #     	Conditions AssignCond _Point_Loads Change \
	   #      		[lindex $pointLoadValues 1]$strengthUnit \
	   #      		[lindex $pointLoadValues 2]$strengthUnit \
	   #      		[lindex $pointLoadValues 0]$strengthUnit \
	   #      		[lindex $pointLoadValues 4]$momentUnit \
	   #      		[lindex $pointLoadValues 5]$momentUnit \
	   #      		[lindex $pointLoadValues 3]$momentUnit \
	   #      		0 0 \
	   #      		$firstPoint:$lastPoint \
	   #      	Mescape

   				GiD_Process Mescape Data \
        		Conditions AssignCond _Point_Loads Change \
	        		[lindex $pointLoadValues 1] \
	        		[lindex $pointLoadValues 2] \
	        		[lindex $pointLoadValues 0] \
	        		[lindex $pointLoadValues 4] \
	        		[lindex $pointLoadValues 5] \
	        		[lindex $pointLoadValues 3] \
	        		0 0 \
	        		$firstPoint:$lastPoint \
	        	Mescape

				incr index 9
			}
		}
	}  
	# close $result
}

proc GetPointLoadValues { pointLoadsData index } {

	set values {}

	for {set i 1} {$i < 7} {incr i} {
		lappend values [lindex $pointLoadsData [expr $index + $i]]
	}

	return $values
}