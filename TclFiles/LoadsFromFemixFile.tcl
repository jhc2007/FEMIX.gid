# LoadsFromFemixFile.tcl
#
# AssignLoads { femixData }
# SetNewNumericalIntegrationParameters { integrationData integrationName }
# GetLoadCaseParametersValue { loadCaseData parameter }

proc AssignLoads { femixData } {

	global NUMERICALINTEGRATIONLIST \
	       numDimension 
	       # accelerationUnit
		   # AuxiliaryVectorsList \
		   # AuxiliaryVectorsFromFemixFile \

	set loadData [GetBlockData $femixData "LOAD"]

	if { [llength $loadData] > 0 } {
		
		set index 0
		set numLoadCase 0
		set firstNumericalIntegration yes
		set existsConditionTypeList no
		# set existsAuxiliaryVectorsList no

		while { $index < [llength $loadData] } {

			set loadCaseData {}
		    
		    while { $index < [llength $loadData] && [lindex $loadData $index] != "<LOAD_CASE>" } {
		        incr index
		    }

		    if { $index < [llength $loadData] } {

		        incr index
		        incr numLoadCase

		        while { $index < [llength $loadData] && [lindex $loadData $index] != "</LOAD_CASE>" } {
		            lappend loadCaseData [lindex $loadData $index]
		            incr index
		        }

		        # set loadCaseParametersData [GetBlockData $loadCaseData "LOAD_CASE_PARAMETERS"]

		        if { $numLoadCase > 1 } {
		        	GiD_Process Mescape Data Intervals NewInterval Yes No escape 
		        }

		        set loadTitle [GetLoadCaseParametersValue $loadCaseData "LOAD_CASE_TITLE"]
		        GiD_Process Data IntervalData -SingleField- Load_Title $loadTitle Mescape

					  # global PROBLEMTYPEPATH
					  # set result [open "$PROBLEMTYPEPATH\\result_new.dat" "w"]
					  # puts $result $loadTitle
					  # close $result


		        set loadCaseGroup [GetLoadCaseParametersValue $loadCaseData "LOAD_CASE_GROUP"]
		        
		        if { ![ExistsListElement [GiD_Info layers] $loadCaseGroup] && ![ExistsListElement [GiD_Groups list] $loadCaseGroup] } {
					GiD_Process 'Groups Create $loadCaseGroup Mescape
				}
				GiD_Process Data IntervalData -SingleField- Group_Name $loadCaseGroup Mescape


		        switch [GetLoadCaseParametersValue $loadCaseData "INTEGRATION_TYPE"] {

		        	"_NONE" {
		        		set integrationType "NONE"
		        	}
			       	"_GLEG" {
	                    set integrationType "GAUSS-LEGENDRE"
	                }
	                "_GLOB" {
	                    set integrationType "GAUSS-LOBATTO"
	                }
	                "_NCOTES" {
	                    set integrationType "NEWTON-COTES"
	                }
	                default {
	                	set integrationType "NONE"
	                }
		        }

		        set integrationName [GetLoadCaseParametersValue $loadCaseData "INTEGRATION_NAME"]

		        if { $integrationName == "" || $integrationName == "_DEFAULT" } {
		        	set integrationName "DEFAULT"
		        }


		        if { $integrationName == "DEFAULT" || [ExistsListElement $NUMERICALINTEGRATIONLIST $integrationName] } {

			       	GiD_Process Data IntervalData -SingleField- New_Numerical_Integration#CB#(0,1) 0 Mescape
			        GiD_Process Data IntervalData -SingleField- Integration_Type $integrationType Mescape
			        GiD_Process Data IntervalData -SingleField- Integration_Name $integrationName Mescape
		        


		        } else {

			       	# GiD_Process Data IntervalData -SingleField- Load_Title [GetLoadCaseParametersValue $loadCaseData "LOAD_CASE_TITLE"] Mescape
			       	# GiD_Process Data IntervalData -SingleField- Layer_Name [GetLoadCaseParametersValue $loadCaseData "LOAD_CASE_GROUP"] Mescape
			       	GiD_Process Data IntervalData -SingleField- New_Numerical_Integration#CB#(0,1) 1 Mescape
			       	GiD_Process Data IntervalData -SingleField- Integration_Name_ $integrationName Mescape
			       	GiD_Process Data IntervalData -SingleField- Integration_Type#CB#(NONE,GAUSS-LEGENDRE,GAUSS-LOBATTO,NEWTON-COTES) $integrationType Mescape

		        	if { $firstNumericalIntegration } {
                		set integrationData [GetBlockData $femixData "NUMERICAL_INTEGRATION"]
                		set firstNumericalIntegration no
            		}

            		SetNewNumericalIntegrationParameters $integrationData $integrationName
		        }

		        set gravityData [GetBlockData $loadCaseData "GRAVITY_LOAD"]
	        	set i 0
	        	set gravity 0

	        	while { $i < [expr [llength $gravityData] - 1] } {
	        		if { [lindex $gravityData $i] != 0 } {
	        			set gravity 1
	        			set i 3
	        		} else {
	        			incr i
	        		}
	        	}
		        
		        GiD_Process Data IntervalData -SingleField- Gravity_Load#CB#(0,1) $gravity Mescape

		        if { $gravity == 1 } {

		        	# if { $numDimension == 2 } {
			        # 	GiD_Process Data IntervalData -SingleField- X-Acceleration#UNITS# [lindex $gravityData 1]$accelerationUnit Mescape
			        # 	GiD_Process Data IntervalData -SingleField- Y-Acceleration#UNITS# [lindex $gravityData 2]$accelerationUnit Mescape
		        	# 	GiD_Process Data IntervalData -SingleField- Z-Acceleration#UNITS# [lindex $gravityData 0]$accelerationUnit Mescape
		        	# } else {
		        	# 	GiD_Process Data IntervalData -SingleField- X-Acceleration#UNITS# [lindex $gravityData 0]$accelerationUnit Mescape
			        # 	GiD_Process Data IntervalData -SingleField- Y-Acceleration#UNITS# [lindex $gravityData 1]$accelerationUnit Mescape
			        # 	GiD_Process Data IntervalData -SingleField- Z-Acceleration#UNITS# [lindex $gravityData 2]$accelerationUnit Mescape
		        	# }
		        	if { $numDimension == 2 } {
			        	GiD_Process Data IntervalData -SingleField- X-Acceleration [lindex $gravityData 1] Mescape
			        	GiD_Process Data IntervalData -SingleField- Y-Acceleration [lindex $gravityData 2] Mescape
		        		GiD_Process Data IntervalData -SingleField- Z-Acceleration [lindex $gravityData 0] Mescape
		        	} else {
		        		GiD_Process Data IntervalData -SingleField- X-Acceleration [lindex $gravityData 0] Mescape
			        	GiD_Process Data IntervalData -SingleField- Y-Acceleration [lindex $gravityData 1] Mescape
			        	GiD_Process Data IntervalData -SingleField- Z-Acceleration [lindex $gravityData 2] Mescape
		        	}
		        
		        }

   				AssignPointLoads $loadCaseData
   				AssignInternalPointLoads $loadCaseData
   				CreateEdgeLoadsFromFemixFile $loadCaseData $numLoadCase
   				CreateFaceLoadsFromFemixFile $loadCaseData $numLoadCase
   				AssignPrescribedDisplacements $loadCaseData
   				AssignPrescribedPointTemperatures $loadCaseData
   				CreateSurfaceHeatFluxFromFemixFile $loadCaseData $numLoadCase
   				AssignVolumetricInternalHeatGeneration $loadCaseData

   	# 			set temperatureVariationData [GetBlockData $loadCaseData "TEMPERATURE_VARIATION"]

   	# 			if { [llength $temperatureVariationData] > 0 } {
   					
   	# 				if { !$existsConditionTypeList } {
   	# 					set conditionTypeList [CreateConditionTypeList [GetBlockData $femixData "ELEMENT_PROPERTIES"]]
				# 		set existsConditionTypeList yes
   	# 				}

				# 	AssignTemperatureVariation $temperatureVariationData $conditionTypeList
				# }
		    }
		}
        if { $numLoadCase > 1 } {
        	GiD_Process Mescape Data Intervals ChangeInterval 1	escape
        }
	}

	# set AuxiliaryVectorsFromFemixFile $AuxiliaryVectorsList
}

proc SetNewNumericalIntegrationParameters { integrationData integrationName } {

    # set values {}

    set index 0

    while { $index < [llength $integrationData] && [lindex $integrationData $index] != $integrationName } {
        incr index 1
    }

    if { $index < [llength $integrationData] } {

        set numKey [lindex $integrationData [expr $index + 1]]

		GiD_Process Data IntervalData -SingleField- Number_of_Keywords#CB#(1,2,3,4,5,6,7,8,9) $numKey Mescape

        for {set i 1} {$i <= $numKey} {incr i} {

            set key [lindex $integrationData [expr $index + 1 + $i]]

            switch [string index $key 1] {

                "G" {
                	GiD_Process Data IntervalData -SingleField- \
                	Type_of_Terms_\[$i\]#CB#(General,Membrane,Shear,Torsional,Bending) "General" \
                	Mescape
                }
                "M" {
                    GiD_Process Data IntervalData -SingleField- \
                	Type_of_Terms_\[$i\]#CB#(General,Membrane,Shear,Torsional,Bending) "Membrane" \
                	Mescape
                }
                "S" {
                    GiD_Process Data IntervalData -SingleField- \
                	Type_of_Terms_\[$i\]#CB#(General,Membrane,Shear,Torsional,Bending) "Shear" \
                	Mescape
                }
                "T" {
                    GiD_Process Data IntervalData -SingleField- \
                	Type_of_Terms_\[$i\]#CB#(General,Membrane,Shear,Torsional,Bending) "Torsional" \
                	Mescape
                }
                "B" {
                    GiD_Process Data IntervalData -SingleField- \
                	Type_of_Terms_\[$i\]#CB#(General,Membrane,Shear,Torsional,Bending) "Bending" \
                	Mescape
                }
            }

            GiD_Process Data IntervalData -SingleField- \
            Direction_\[$i\]#CB#(s1,s2,s3) "s[string index $key 4]" \
            Mescape

            if { [string length $key] > 7 } {
            	GiD_Process Data IntervalData -SingleField- \
                Number_of_Points_\[$i\]#CB#(1,2,3,4,5,6,7,8,9,10) 10 \
                Mescape
            } else {
            	GiD_Process Data IntervalData -SingleField- \
                Number_of_Points_\[$i\]#CB#(1,2,3,4,5,6,7,8,9,10) [string index $key 6] \
                Mescape
            }

        }
    }
    # close $result
    # return $values
}

proc GetLoadCaseParametersValue { loadCaseData parameter } {

	set value ""
    set index 0

    while { $index < [llength $loadCaseData] && [lindex $loadCaseData $index] != $parameter } {
        incr index 1
    }

    set index [expr $index + 2]
   	
   	while { $index < [llength $loadCaseData] && [lindex $loadCaseData $index] != ";" } {
    	append value "[lindex $loadCaseData $index]_"
    	incr index 1
    }

    if { $index < [llength $loadCaseData] } {
    	set value [string range $value 0 [expr [string length $value] - 2]]
    }

    return $value
}

proc CreateConditionTypeList { elementPropertiesData } {

	set conditionTypeList {}
	set index 7

	while { $index < [llength $elementPropertiesData] } {
		set rangeList [GetFirstLastValuesFromRange [lindex $elementPropertiesData $index]]
		set firstElem [lindex $rangeList 0]
		set lastElem [lindex $rangeList 1]
		set conditionType [CreateConditionType [lindex $elementPropertiesData [expr $index + 1]]]
		lappend conditionTypeList [list $firstElem $lastElem $conditionType]
		set index [expr $index + 12]
	}

	return $conditionTypeList
}

proc CreateConditionType { elemType } {

	set linearElem { "_CABLE_2D" \
					 "_CABLE_3D" \
					 "_EMB_CABLE_2D" \
					 "_EMB_CABLE_3D" \
					 "_TRUSS_2D" \
					 "_TRUSS_3D" \
					 "_TIMOSHENKO_BEAM_2D" \
					 "_TIMOSHENKO_BEAM_3D" \
					 "_FRAME_2D" \
					 "_FRAME_3D" } 

	set quadrilateralElem { "_PLANE_STRESS_QUAD" \
							"_MINDLIN_SLAB_QUAD" \
							"_MINDLIN_SHELL_QUAD" }

	set hexahedraElem { "_SOLID_HEXA" }

	for {set i 0} {$i < [llength $linearElem]} {incr i} {
		if { [lindex $linearElem $i] == $elemType } {
			return "Lines"
		}
	}

	for {set i 0} {$i < [llength $quadrilateralElem]} {incr i} {
		if { [lindex $quadrilateralElem $i] == $elemType } {
			return "Surfaces"
		}
	}

	for {set i 0} {$i < [llength $hexahedraElem]} {incr i} {
		if { [lindex $hexahedraElem $i] == $elemType } {
			return "Volumes"
		}
	}

}