# Combinations.tcl
#
# proc CreateCombinations {}
# proc OpenCombinationsWindow {}
# proc ShowCombinationParameters {}
# proc CreateFemixCombinationsFile {}
# proc GetLoadCaseIndex { loadname }
# proc RemoveCombination {}
# proc UpdateCombination {}
# proc AddCombination {}
# proc NumOfCombinations {}
# proc WriteCombinations {}

proc CreateCombinations {} {

    global PROJECTPATH
    global combinations

    if { [file exists "$PROJECTPATH\\femix_combinations.dat"] } {
        set combFile [open "$PROJECTPATH\\femix_combinations.dat" "r"]
        set combData [read $combFile]
        close $combFile
        set numComb [lindex $combData 0]
        set index 1
        for {set i 0} {$i < $numComb} {incr i} {
            set combTitle    [lindex $combData $index]
            set layerName    [lindex $combData [expr $index + 1]]
            set firstComb    [lindex $combData [expr $index + 2]]
            set lastComb     [lindex $combData [expr $index + 3]]
            set stepDuration [lindex $combData [expr $index + 4]]
            set numLoad      [lindex $combData [expr $index + 5]]
            set comb [list $combTitle $layerName $firstComb $lastComb $stepDuration $numLoad]
            incr index 6
            for {set j 0} {$j < $numLoad} {incr j} {
                lappend comb [lindex $combData $index]
                lappend comb [lindex $combData [expr $index + 1]]
                incr index 2
            }
            lappend combinations $comb
        }
        destroy $combFile $combData
    }
}

proc OpenCombinationsWindow {} {

    global PROBLEMTYPEPATH PROJECTPATH 
    global combWin numLoad loadNamesList combinations buttonAddUpdateText


    # set result [open "$PROBLEMTYPEPATH\\result_new.dat" "w"]
    # puts $result [lindex [GiD_Info GenData] 2]
    # close $result

    # if { [lindex [GiD_Info GenData] 2] == "THERMAL_TRANSIENT" } {
    #     set thermalTransient yes
    # } else {
    #     set thermalTransient no
    # }

    if { [GidUtils::AreWindowsDisabled] } {
       return
    }  
    if { $PROBLEMTYPEPATH == $PROJECTPATH } {
        OpenWarningWindow "Warning" "Save the project before edit combinations" 1
        return
    }

    if { $combinations == "" && [file exists "$PROJECTPATH\\femix_combinations.dat"] } {
        CreateCombinations
    }
    
    set buttonAddUpdateText "Add"    
    
    set combWin .gid.combinationswindow
    InitWindow $combWin [= "Combinations"] "" "" "" 1
    if { ![winfo exists $combWin] } {
       return
    }
    #toplevel $combwin
    set numLoadList { Select }
    set loadNamesList { }
    for {set i 1} {$i <= [lindex [GiD_Info intvdata num] 1]} {incr i} {
        lappend numLoadList $i
        lappend loadNamesList [lindex [GiD_Info intvdata -interval $i] 2]
    }
    set numComb "1"
    set numLoad 0
    
    #set firstLayerName [lindex [split [GiD_Info layers]] 0]
    
    label $combWin.labelCombTitle -text "Combination Title"
    label $combWin.labelLayerName -text "Group Name"
    label $combWin.labelNumComb   -text "Number of Increments in the Combination"
    label $combWin.labelNumLoad   -text "Number of Load Cases"
    label $combWin.labelStepDuration -text "Step Duration"

    entry $combWin.textFieldCombTitle -width 30
    
    if { [llength [GiD_Groups list]] == 0 } {
        CreateDefaultGroup
    }

    ttk::combobox $combWin.comboBoxLayerName -values [GiD_Groups list] -width 27
    $combWin.comboBoxLayerName set [lindex [GiD_Groups list] 0]

    entry $combWin.textFieldNumComb -width 10
    $combWin.textFieldNumComb insert 0 "1"

    entry $combWin.textFieldStepDuration -width 10

    if { [lindex [GiD_Info GenData] 2] != "THERMAL_TRANSIENT" && [lindex [GiD_Info GenData] 2] != "DYNAMIC" } {
        $combWin.labelStepDuration configure -state disabled
        $combWin.textFieldStepDuration insert 0 "0"
        $combWin.textFieldStepDuration configure -state disabled
    }
    
    ttk::combobox $combWin.comboBoxNumLoad -values $numLoadList -width 7
    $combWin.comboBoxNumLoad set "Select"

    button $combWin.buttonParametes -text "Parameters" -command "ShowCombinationParameters" -width 14
    button $combWin.buttonRemove -text "Remove" -command "RemoveCombination" -width 14

    button $combWin.buttonAccept -text "Accept" -command "CreateFemixCombinationsFile" -width 14
    button $combWin.buttonClose  -text "Close"  -command "destroy $combWin" -width 14

    #pack $combwin.labeltitle -side left
    #pack $combwin.buttonaccept -side bottom
    #pack $combwin.textfieldtitle
    
    listbox $combWin.listBoxComb -width 40
    set numDiferentComb [llength $combinations]
    for {set i 0} {$i < $numDiferentComb} {incr i} {
        $combWin.listBoxComb insert end [lindex [lindex $combinations $i] 0]
    }

    grid $combWin.labelCombTitle    -row 0 -column 0 -padx 10 -sticky w
    grid $combWin.labelLayerName    -row 1 -column 0 -padx 10 -sticky w
    grid $combWin.labelNumComb      -row 2 -column 0 -padx 10 -sticky w
    grid $combWin.labelStepDuration -row 3 -column 0 -padx 10 -sticky w
    grid $combWin.labelNumLoad      -row 4 -column 0 -padx 10 -sticky w
     
    grid $combWin.textFieldCombTitle    -row 0 -column 1 -padx 10 -sticky w
    grid $combWin.comboBoxLayerName     -row 1 -column 1 -padx 10 -sticky w
    grid $combWin.textFieldNumComb      -row 2 -column 1 -padx 10 -sticky w
    grid $combWin.textFieldStepDuration -row 3 -column 1 -padx 10 -sticky w
    grid $combWin.comboBoxNumLoad       -row 4 -column 1 -padx 10 -sticky w


    
    grid $combWin.buttonParametes -row 5 -column 2 -pady 10
    grid $combWin.buttonRemove -row 5 -column 3 -pady 10

    grid $combWin.buttonAccept -row 6 -column 2 -pady 10
    # -padx 10 -sticky e
    grid $combWin.buttonClose  -row 6 -column 3 -pady 10
    # -pady 10 -sticky e
    #-sticky w
    grid $combWin.listBoxComb -row 0 -column 2 -columnspan 2 -rowspan 5 -padx 10 -pady 8

    #set combTitle [$combWin.textFieldCombTitle get10

    #bind $combWin.comboBoxLayerName <<ComboboxSelected>> {
    #    global combWin layerName
    #    set layerName [$combWin.comboBoxLayerName get]
    #}
    
    bind $combWin.comboBoxNumLoad <<ComboboxSelected>> {

        global combWin numLoad loadNamesList buttonAddUpdateText selectedCombIndex
        # layerName
        destroy $combWin.buttonAccept
        destroy $combWin.buttonClose
        button $combWin.buttonAccept -text "Accept" -command "CreateFemixCombinationsFile" -width 14
        button $combWin.buttonClose  -text "Close"  -command "destroy $combWin" -width 14
        if {$numLoad != "Select" && $numLoad != 0} { 
            destroy $combWin.buttonAdd
            destroy $combWin.labelLoadFactor
            for {set i 1} {$i <= $numLoad} {incr i} {
                destroy $combWin.comboBoxLoad$i
                destroy $combWin.textFieldLoadFactor$i
                #grid $combWin.comboBoxLoad$i -row [expr 3 + $i] -column 0 -padx 10 -pady 4 -sticky w
            }
        }
        set numLoad [$combWin.comboBoxNumLoad get]
        if {$numLoad != "Select"} { 
            label $combWin.labelLoadFactor -text "Load Factor"
            grid $combWin.labelLoadFactor -row 5 -column 1 -padx 8 -pady 4 -sticky w
            for {set i 1} {$i <= $numLoad} {incr i} {
                ttk::combobox $combWin.comboBoxLoad$i -values $loadNamesList -width 27
                entry $combWin.textFieldLoadFactor$i -width 10
                if {$buttonAddUpdateText == "Add" || $i > [lindex [lindex $combinations $selectedCombIndex] 4]} {
                    $combWin.comboBoxLoad$i set [lindex $loadNamesList [expr $i - 1]]
                } else {
                    $combWin.comboBoxLoad$i set [lindex $loadNamesList \
                                                [expr \
                                                [lindex \
                                                [lindex $combinations $selectedCombIndex] [expr 4 + 2 * $i]] - 1]]
                    $combWin.textFieldLoadFactor$i delete 0 [string length [$combWin.textFieldLoadFactor$i get]]
                    $combWin.textFieldLoadFactor$i insert 0 [lindex [lindex $combinations $selectedCombIndex] [expr 4 + 2 * $i]]
                }
                grid $combWin.comboBoxLoad$i -row [expr 5 + $i] -column 0 -padx 10 -pady 5 -sticky w
                grid $combWin.textFieldLoadFactor$i -row [expr 5 + $i] -column 1 -padx 10 -pady 5 -sticky w
            }
            grid $combWin.buttonAccept -row [expr 5 + $i] -column 2 -pady 10
            #  -padx 10 -sticky e
            grid $combWin.buttonClose  -row [expr 5 + $i] -column 3 -pady 10
            # -sticky w
            if {$buttonAddUpdateText == "Add"} {
                button $combWin.buttonAdd -text $buttonAddUpdateText -command "AddCombination" -width 25
            } else {
                button $combWin.buttonAdd -text $buttonAddUpdateText -command "UpdateCombination" -width 25
            } 
            grid $combWin.buttonAdd -padx 10 -pady 10 -row [expr 5 + $i] -column 0 -sticky w -columnspan 2
        } else {
            $combWin.textFieldCombTitle delete 0 [string length [$combWin.textFieldCombTitle get]]
            $combWin.comboBoxLayerName set [lindex [split [GiD_Groups list]] 0]
            $combWin.textFieldNumComb delete 0 [string length [$combWin.textFieldNumComb get]]
            $combWin.textFieldNumComb insert 0 "1"
            set buttonAddUpdateText "Add"
            #$combWin.textFieldCombTitle insert 0 [lindex [lindex $combinations $combIndex] 0]
            grid $combWin.buttonAccept -row 6 -column 2 -pady 10
            #  -padx 10 -sticky e
            grid $combWin.buttonClose  -row 6 -column 3 -pady 10
            # -sticky w
        }
        #$combWin.labelNumLoad configure -text $selectedItem
    }
    #label $combwin.lbl -textvariable combtitle -anchor sw -justify left 
    #pack $combwin.lbl -expand 1 -fill x
}

proc ShowCombinationParameters {} {

    #global PROJECTPATH
    global combWin combinations loadNamesList numLoad buttonAddUpdateText selectedCombIndex
    
    set selectedCombIndex [$combWin.listBoxComb curselection] 

    if { $selectedCombIndex == "" } { return }

    set buttonAddUpdateText "Update"
    $combWin.textFieldCombTitle delete 0 [string length [$combWin.textFieldCombTitle get]]
    $combWin.textFieldCombTitle insert 0 [lindex [lindex $combinations $selectedCombIndex] 0]
    $combWin.comboBoxLayerName set [lindex [lindex $combinations $selectedCombIndex] 1]
    $combWin.textFieldNumComb delete 0 [string length [$combWin.textFieldNumComb get]]
    $combWin.textFieldNumComb insert 0 [expr [lindex [lindex $combinations $selectedCombIndex] 3] - \
                                             [lindex [lindex $combinations $selectedCombIndex] 2] + 1]
    #set combNumLoadCases [lindex [lindex $combinations $combIndex] 4]
    $combWin.textFieldStepDuration delete 0 end
    $combWin.textFieldStepDuration insert 0 [lindex [lindex $combinations $selectedCombIndex] 4]

    # global PROJECTPATH
    # set result [open "$PROJECTPATH\\result.dat" "w"]
    # puts $result [lindex [lindex $combinations $selectedCombIndex] 4]
    # close $result

    set numLoad [lindex [lindex $combinations $selectedCombIndex] 5]
    set prevNumLoadCases [$combWin.comboBoxNumLoad get]
    $combWin.comboBoxNumLoad set $numLoad

    if {$prevNumLoadCases == "Select"} {
        destroy $combWin.buttonAccept
        destroy $combWin.buttonClose
        button $combWin.buttonAccept -text "Accept" -command "CreateFemixCombinationsFile" -width 14
        button $combWin.buttonClose  -text "Close"  -command "destroy $combWin" -width 14
        label $combWin.labelLoadFactor -text "Load Factor"
        grid $combWin.labelLoadFactor -row 5 -column 1 -padx 8 -pady 4 -sticky w
        for {set i 1} {$i <= $numLoad} {incr i} {
            ttk::combobox $combWin.comboBoxLoad$i -values $loadNamesList -width 27
            #$combWin.comboBoxLoad$i set [lindex $loadNamesList 0]
            entry $combWin.textFieldLoadFactor$i -width 10
            grid $combWin.comboBoxLoad$i -row [expr 5 + $i] -column 0 -padx 10 -pady 5 -sticky w
            grid $combWin.textFieldLoadFactor$i -row [expr 5 + $i] -column 1 -padx 10 -pady 5 -sticky w
        }
        grid $combWin.buttonAccept -row [expr 5 + $i] -column 2 -pady 10
        grid $combWin.buttonClose  -row [expr 5 + $i] -column 3 -pady 10
        button $combWin.buttonAdd -text $buttonAddUpdateText -command "UpdateCombination" -width 25
        grid $combWin.buttonAdd -padx 10 -pady 10 -row [expr 5 + $i] -column 0 -sticky w -columnspan 2
    } elseif {$prevNumLoadCases != $numLoad} { 
        destroy $combWin.buttonAccept
        destroy $combWin.buttonClose
        button $combWin.buttonAccept -text "Accept" -command "CreateFemixCombinationsFile" -width 14
        button $combWin.buttonClose  -text "Close"  -command "destroy $combWin" -width 14
        destroy $combWin.buttonAdd
        #destroy $combWin.labelLoadFactor
        for {set i 1} {$i <= $prevNumLoadCases} {incr i} {
            destroy $combWin.comboBoxLoad$i
            destroy $combWin.textFieldLoadFactor$i
            #grid $combWin.comboBoxLoad$i -row [expr 3 + $i] -column 0 -padx 10 -pady 4 -sticky w
        }
        for {set i 1} {$i <= $numLoad} {incr i} {
            ttk::combobox $combWin.comboBoxLoad$i -values $loadNamesList -width 27
            #$combWin.comboBoxLoad$i set [lindex $loadNamesList 0]
            entry $combWin.textFieldLoadFactor$i -width 10
            grid $combWin.comboBoxLoad$i -row [expr 5 + $i] -column 0 -padx 10 -pady 5 -sticky w
            grid $combWin.textFieldLoadFactor$i -row [expr 5 + $i] -column 1 -padx 10 -pady 5 -sticky w
        }
        grid $combWin.buttonAccept -row [expr 5 + $i] -column 2 -pady 10
        grid $combWin.buttonClose  -row [expr 5 + $i] -column 3 -pady 10
        #destroy $combWin.buttonAdd
        button $combWin.buttonAdd -text $buttonAddUpdateText -command "UpdateCombination" -width 25
        grid $combWin.buttonAdd -padx 10 -pady 10 -row [expr 5 + $i] -column 0 -sticky w -columnspan 2
    } else {
        $combWin.buttonAdd configure -text "Update" -command "UpdateCombination"
    }
    for {set i 1} {$i <= $numLoad} {incr i} {
        #$combWin.comboBoxLoad$i set [lindex $loadNamesList 0]
        $combWin.comboBoxLoad$i set [lindex $loadNamesList \
                                    [expr \
                                    [lindex \
                                    [lindex $combinations $selectedCombIndex] [expr 4 + 2 * $i]] - 1]]
        #$combWin.comboBoxLoad$i -values $loadNamesList
        #$combWin.comboBoxLoad$i set [lindex $loadNamesList 0]
        #$combWin.textFieldLoadFactor$i 
        $combWin.textFieldLoadFactor$i delete 0 [string length [$combWin.textFieldLoadFactor$i get]]
        $combWin.textFieldLoadFactor$i insert 0 [lindex [lindex $combinations $selectedCombIndex] [expr 5 + 2 * $i]]
    
        #grid $combWin.comboBoxLoad$i -row [expr 4 + $i] -column 0 -padx 10 -pady 5 -sticky w
        #grid $combWin.textFieldLoadFactor$i -row [expr 4 + $i] -column 1 -padx 10 -pady 5 -sticky w
    }
    #label $combWin.labelLoadFactor -text "Load Factor"
    #grid $combWin.labelLoadFactor -row 4 -column 1 -padx 8 -pady 4 -sticky w
    $combWin.buttonAccept configure -state disabled
    # -sticky w
}

proc CreateFemixCombinationsFile {} {

    global PROJECTPATH 
    global combinations combWin
    #if { [file exists "$PROJECTPATH\\femix_combinations.dat"] != 1 } {
    set combinationsFile [open "$PROJECTPATH\\femix_combinations.dat" "w"]
    set numComb [llength $combinations]
    puts $combinationsFile $numComb
    for {set i 0} {$i < $numComb} {incr i} { 
        set numLoad [llength [lindex $combinations $i]]
        for {set j 0} {$j < $numLoad} {incr j} {
            puts $combinationsFile [lindex [lindex $combinations $i] $j]
        }
        #puts $combinationsFile [lindex $combinationsFile $i]
    }
    close $combinationsFile
    destroy $combWin
    #   }
}

proc GetLoadCaseIndex { loadname } {


    set numLoadCases [lindex [GiD_Info intvdata num] 1]
    for {set i 1} {$i <= $numLoadCases} {incr i} {
        if {[lindex [GiD_Info intvdata -interval $i] 2] == $loadname} {
            return $i
        }
        #puts $result [lindex [GiD_Info intvdata -interval $i] 2]
    }
    #close $result
}

proc RemoveCombination {} {

    global combWin combinations buttonAddUpdateText

    set selectedCombIndex [$combWin.listBoxComb curselection] 
    if {$selectedCombIndex == ""} { return }

    set numLoad [$combWin.comboBoxNumLoad get]
    if {$numLoad != "Select"} {
        for {set i 1} {$i <= $numLoad} {incr i} {
            destroy $combWin.comboBoxLoad$i
            destroy $combWin.textFieldLoadFactor$i
        }
        $combWin.comboBoxNumLoad set "Select"   
        destroy $combWin.buttonAdd
        destroy $combWin.labelLoadFactor
    }
    set numComb [expr [lindex [lindex $combinations $selectedCombIndex] 3] - [lindex [lindex $combinations $selectedCombIndex] 2] + 1]
    for {set i [expr $selectedCombIndex + 1]} {$i < [llength $combinations]} {incr i} {
        set tmpcomb [lreplace [lindex $combinations $i] 2 2 [expr [lindex [lindex $combinations $i] 2] - $numComb]]
        set tmpcomb [lreplace $tmpcomb 3 3 [expr [lindex $tmpcomb 3] - $numComb]]        
        set combinations [lreplace $combinations $i $i $tmpcomb]
        #set comb [lreplace [lindex $combinations $i] 3 3 [expr [lindex [lindex $combinations $i] 3] + $numComb - 1]]
        #set combinations [lreplace $combinations $i [lreplace [lindex $combinations $i] 3 3 [expr [lindex [lindex $combinations $i] 3] + $numComb - 1]]]
    }
    set combinations [lreplace $combinations $selectedCombIndex $selectedCombIndex]

    $combWin.listBoxComb delete $selectedCombIndex

    set combTitle [$combWin.textFieldCombTitle get]
    set numComb [$combWin.textFieldNumComb get]

    $combWin.textFieldCombTitle delete 0 [string length $combTitle]
    $combWin.comboBoxLayerName set [lindex [split [GiD_Groups list]] 0]
    $combWin.textFieldNumComb delete 0 [string length $numComb]
    $combWin.textFieldNumComb insert 0 "1"

    destroy $combWin.buttonAccept
    destroy $combWin.buttonClose
    button $combWin.buttonAccept -text "Accept" -command "CreateFemixCombinationsFile" -width 14
    button $combWin.buttonClose  -text "Close"  -command "destroy $combWin" -width 14

    grid $combWin.buttonAccept -row 6 -column 2 -pady 10
    grid $combWin.buttonClose  -row 6 -column 3 -pady 10
}

proc UpdateCombination {} {

    global combWin buttonAddUpdateText selectedCombIndex combinations

    set buttonAddUpdateText "Add"
   
    set combTitle [ReplaceSpaceWithUnderscore [$combWin.textFieldCombTitle get]]
    set layerName [ReplaceSpaceWithUnderscore [$combWin.comboBoxLayerName get]]
    set numComb [$combWin.textFieldNumComb get]
    set stepDuration [$combWin.textFieldStepDuration get]
    set numLoad [$combWin.comboBoxNumLoad get]
    if { $selectedCombIndex == 0 } {
        set firstComb 1 
    } else {
        set firstComb [expr [lindex [lindex $combinations [expr $selectedCombIndex - 1]] 3] + 1]
    }
    set lastComb [expr $firstComb + $numComb - 1]
    set comb [list $combTitle $layerName $firstComb $lastComb $stepDuration $numLoad]
    for {set i 1} {$i <= $numLoad} {incr i} {
        lappend comb [GetLoadCaseIndex [$combWin.comboBoxLoad$i get]]
        lappend comb [$combWin.textFieldLoadFactor$i get]
        destroy $combWin.comboBoxLoad$i
        destroy $combWin.textFieldLoadFactor$i
    }

    if { $lastComb != [lindex [lindex $combinations $selectedCombIndex] 3] } {
        for {set i [expr $selectedCombIndex + 1]} {$i < [llength $combinations]} {incr i} {
            set tmpcomb [lreplace [lindex $combinations $i] 2 2 [expr [lindex [lindex $combinations $i] 2] + $numComb - 1]]
            set tmpcomb [lreplace $tmpcomb 3 3 [expr $tmpcomb + $numComb - 1]]        
            set combinations [lreplace $combinations $i $i $tmpcomb]
            #set comb [lreplace [lindex $combinations $i] 3 3 [expr [lindex [lindex $combinations $i] 3] + $numComb - 1]]
            #set combinations [lreplace $combinations $i [lreplace [lindex $combinations $i] 3 3 [expr [lindex [lindex $combinations $i] 3] + $numComb - 1]]]
        }
    }
    set combinations [lreplace $combinations $selectedCombIndex $selectedCombIndex $comb]
    # global PROJECTPATH
    # set result [open "$PROJECTPATH\\result.dat" "w"]
    # puts $result $comb
    # puts $result [lindex $combinations $selectedCombIndex]
    # close $result

    $combWin.listBoxComb delete $selectedCombIndex
    $combWin.listBoxComb insert $selectedCombIndex $combTitle
    $combWin.textFieldCombTitle delete 0 [string length $combTitle]
    $combWin.comboBoxLayerName set [lindex [split [GiD_Groups list]] 0]
    $combWin.textFieldNumComb delete 0 [string length $numComb]
    $combWin.textFieldNumComb insert 0 "1"
    $combWin.comboBoxNumLoad set "Select"

    destroy $combWin.buttonAccept
    destroy $combWin.buttonClose
    button $combWin.buttonAccept -text "Accept" -command "CreateFemixCombinationsFile" -width 14
    button $combWin.buttonClose  -text "Close"  -command "destroy $combWin" -width 14

    destroy $combWin.buttonAdd
    destroy $combWin.labelLoadFactor

    grid $combWin.buttonAccept -row 6 -column 2 -pady 10
    grid $combWin.buttonClose  -row 6 -column 3 -pady 10
}

proc AddCombination {} {

    #global PROJECTPATH
    global combWin numLoad combinations 
    #set result [open "$PROJECTPATH\\result.dat" "w"]
    #set elem ""
    set combTitle [ReplaceSpaceWithUnderscore [$combWin.textFieldCombTitle get]]
    #append elem $combTitle
    set layerName [ReplaceSpaceWithUnderscore [$combWin.comboBoxLayerName get]]
    #append elem $layerName
    set numComb [$combWin.textFieldNumComb get]
    #append elem $numComb
    set stepDuration [$combWin.textFieldStepDuration get]

    if { [llength $combinations] > 0 } {
        set firstComb [expr [lindex [lindex $combinations end] 3] + 1]
    } else {
        set firstComb 1
    }
    set lastComb [expr $firstComb + $numComb - 1]
    set comb [list $combTitle $layerName $firstComb $lastComb $stepDuration $numLoad]
    for {set i 1} {$i <= $numLoad} {incr i} {
        #set loadCase($i) 
        lappend comb [GetLoadCaseIndex [$combWin.comboBoxLoad$i get]]
        #GetLoadCaseIndex "SIM"
        #append elem $loadCase($i)
        #set loadFactor($i) [$combWin.textFieldLoadFactor$i get]
        lappend comb [$combWin.textFieldLoadFactor$i get]
        #append elem $loadFactor($i)
    # for {set i 0} {$i < $n} {incr i} {
    #     puts $result [lindex $combinations $i]
    # }
        destroy $combWin.comboBoxLoad$i
        destroy $combWin.textFieldLoadFactor$i
    }
    # puts $result $comb
    lappend combinations $comb
    # puts $result [llength $combinations]
    # set n [llength $combinations]
    $combWin.listBoxComb insert end $combTitle
    $combWin.textFieldCombTitle delete 0 [string length $combTitle]
    $combWin.comboBoxLayerName set [lindex [split [GiD_Info layers]] 0]
    $combWin.textFieldNumComb delete 0 [string length $numComb]
    $combWin.textFieldNumComb insert 0 "1"
    $combWin.comboBoxNumLoad set "Select"

    destroy $combWin.buttonAccept
    destroy $combWin.buttonClose
    button $combWin.buttonAccept -text "Accept" -command "CreateFemixCombinationsFile" -width 14
    button $combWin.buttonClose  -text "Close"  -command "destroy $combWin" -width 14

    destroy $combWin.buttonAdd
    destroy $combWin.labelLoadFactor

    grid $combWin.buttonAccept -row 6 -column 2 -pady 10
    #  -padx 10 -sticky e
    grid $combWin.buttonClose  -row 6 -column 3 -pady 10
    #close $result
}

proc NumOfCombinations {} {

    global PROJECTPATH \
           PROBLEMTYPEPATH \
           numCombinations
    
    if { $PROBLEMTYPEPATH == $PROJECTPATH } {
        #OpenWarningWindow "Warning" "Save the project before calculate" 1
        return 0
    }
    if { ![file exists "$PROJECTPATH\\femix_combinations.dat"] } { 
        return 0 
    }
    set combinationsFile [open "$PROJECTPATH\\femix_combinations.dat" "r"]
    set filedata [read $combinationsFile]
    close $combinationsFile     
    #if {[lindex $filedata 0] == 0} { return 0 }
    #set numCombinations 0
    set index 4
    for {set i 0} {$i < [lindex $filedata 0]} {incr i} {
        set numCombinations [lindex $filedata $index]
        set index [expr $index + [lindex $filedata [expr $index + 2]] * 2 + 6]
    }
    destroy $combinationsFile $filedata
    return $numCombinations
}

proc WriteCombinations {} {

    global PROJECTPATH \
           PROBLEMTYPEPATH
    

    #if {$PROJECTPATH == "" || $PROJECTPATH == $PROBLEMTYPEPATH} {
    #    OpenWarningWindow "Warning" "Save the project before calculate" 1
    #    return $combinations
    #}
    
    if { ![file exists "$PROJECTPATH\\femix_combinations.dat"] } {
        return ""
    }


    set combinationsFile [open "$PROJECTPATH\\femix_combinations.dat" "r"]
    set filedata [split [read $combinationsFile] "\n"]
    close $combinationsFile 
    
    set numCombinations [lindex $filedata 0]
    #set result [open "$PROBLEMTYPEPATH\\JarFiles\\result.dat" "w"]

    if { $numCombinations == 0 } {
        return ""   
    }

    set combinations ""
    set index 3

    append combinations "\n<LOAD_CASE_COMBINATIONS>\n"

    for {set i 0} {$i < $numCombinations} {incr i} {
        
        append combinations "\n<COMBINATION>\n\n<COMBINATION_PARAMETERS>\n\n"
        if { [lindex $filedata $index] == [lindex $filedata [expr $index + 1]] } {
            append combinations "\tCOMBINATION_NUMBER = [lindex $filedata $index] ;\n"
        } else {
            append combinations "\tCOMBINATION_RANGE = \[[lindex $filedata $index]-[lindex $filedata [expr $index + 1]]\] ;\n"   
        }
        append combinations "\tCOMBINATION_TITLE = [lindex $filedata [expr $index - 2]] ;\n"
        append combinations "\tCOMBINATION_GROUP = [lindex $filedata [expr $index - 1]] ;\n"

        if { [lindex [GiD_Info GenData] 2] == "THERMAL_TRANSIENT" || [lindex [GiD_Info GenData] 2] == "DYNAMIC" } {
            append combinations "\tSTEP_DURATION = [lindex $filedata [expr $index + 2]] ;\n"
        }

        append combinations "\n</COMBINATION_PARAMETERS>\n\n"

        incr index 3
        set numLoadCases [lindex $filedata $index]

        append combinations "<LOAD_CASE_FACTORS>\n\n"
        append combinations "\t## Load case factors defining a combination\n"
        append combinations "\tCOUNT = $numLoadCases ; # N. of load cases in the combination\n\n"
        append combinations "\t## Content of each column:\n"
        append combinations "\t#  A -> Counter (or counter range)\n"
        append combinations "\t#  B -> Load case number (or load case range)\n"
        append combinations "\t#  C -> Load case factor\n"
        append combinations "\t#\tA\t\tB\t\tC\n\n"
        
        for {set j 1} {$j <= $numLoadCases} {incr j} {
            incr index 1
            append combinations "\t\t$j\t\t[lindex $filedata $index]\t\t[lindex $filedata [expr $index + 1]] ;\n"
            incr index 1
        }
        append combinations "\n</LOAD_CASE_FACTORS>\n\n</COMBINATION>\n"
        incr index 3
    }
    # if {[lindex $filedata 0] > 0} {
    append combinations "\n</LOAD_CASE_COMBINATIONS>\n\n"
    # }
    #close $result
    return $combinations
}