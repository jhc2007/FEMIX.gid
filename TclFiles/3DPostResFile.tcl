# 3DPostResFile.tcl
#
# proc Create3DPostResFile { basename }
# proc PutsStressesStrains3DPostResFile {}
# proc WriteAhmadShellQuadStressesStrains { loadType combFlag combNum }
# proc WriteMindlinShellQuadStressesStrains { loadType combFlag combNum }
# proc WriteCable3DStressesStrains { loadType combFlag combNum cableType }
# proc WriteSolidHexaStressesStrains { loadType combFlag combNum }
# proc PutsReactions3DPostResFile {}

proc Create3DPostResFile { basename } {

    global PROJECTPATH

    set rslptFileName $basename
    append rslptFileName "_rs.lpt"

    # set result [open "$PROJECTPATH\\result.dat" "w"]
    # puts $result "ASSIM sim"
    # puts $result $rslptFileName

    if { [file exists "$PROJECTPATH\\$rslptFileName"] } {

        global numDimension          
        # lengthUnit
        global postResFile rslptData rslptDataLength
        global combNum combFlag index divLine

        global firstMindlinShellQuadGaussPoints \
               firstSolidHexaGaussPoints \
               firstAhmadShellQuadGaussPoints \
               firstEmbCable3DGaussPoints \
               firstCable3DGaussPoints

        set firstMindlinShellQuadGaussPoints true
        set firstSolidHexaGaussPoints true
        set firstAhmadShellQuadGaussPoints true
        set firstEmbCable3DGaussPoints true
        set firstCable3DGaussPoints true

        set rslptFile [open "$PROJECTPATH\\$rslptFileName" "r"]
        set rslptData [read $rslptFile]
        close $rslptFile 

        #set numDimension 3
        #set numNodes [GiD_Info Mesh NumNodes]
        #set numNodes 161

        set postResFile $basename
        append postResFile ".post.res"
        set postResFile [open "$PROJECTPATH\\$postResFile" "w"]
        puts $postResFile "GiD Post Results File 1.0\n"

        set divLine "------------------------------------------------------------------------------"
        set index 0
        set rslptDataLength [llength $rslptData]
        
        while { $index <  $rslptDataLength } {

            while { $index < $rslptDataLength && [lindex $rslptData $index] != $divLine } {
                set index [expr $index + 1]
            }
            while { $index < $rslptDataLength && [lindex $rslptData $index] != "Combination" &&  [lindex $rslptData $index] != "Load" } {
                set index [expr $index + 1]
            }
            if { $index < $rslptDataLength } {
                if { [lindex $rslptData $index] == "Combination" } {
                    set combNum [lindex $rslptData [expr $index + 1]]
                    set combFlag "Combinations"    
                } else {
                    set combNum [lindex $rslptData [expr $index + 2]]
                    set combFlag "Load Cases"
                }
                set combNum [string range $combNum 0 [expr [string length $combNum] - 2]]
 
                #if { $numDimension == 2 } {
                puts $postResFile "Result \"Displacements\" \"$combFlag\" $combNum Vector OnNodes\n" 
                puts $postResFile "ComponentNames \"X-Displacement\" \"Y-Displacement\" \"Z-Displacement\"\n"
                # puts $postResFile "ComponentNames \"X-Displacement ($lengthUnit)\" \"Y-Displacement ($lengthUnit)\" \"Z-Displacement ($lengthUnit)\"\n"
                # \"Z-Displacement\"\n"
                puts $postResFile "Values"
                puts $postResFile "#NodeNum      X-Displacement      Y-Displacement      Z-Displacement\n"
                #} else {
                #}
                # Z-Displacement\n"
            }
            while { $index < $rslptDataLength && [lindex $rslptData $index] != "Displacements:"} {
                set index [expr $index + 1]
            }
            while { $index < $rslptDataLength && ![string is integer [lindex $rslptData $index]] } {
                set index [expr $index + 1]
            }
            if { $index < $rslptDataLength } {
                if { [lindex $rslptData [expr $index - 1]] == "R3" } {
                    set rotationData "Result \"Rotations\" \"$combFlag\" $combNum Vector OnNodes\n \
                                      \nComponentNames \"X-Rotation\" \"Y-Rotation\" \"Z-Rotation\"\n \
                                      \nValues \
                                      \n#NodeNum          X-Rotation          Y-Rotation          Z-Rotation\n\n" 
                                     
                    while { [string is integer [lindex $rslptData $index]] } {
                        puts $postResFile [format %8s%20s%20s%20s \
                                          [lindex $rslptData $index] \
                                          [GetStringValue [lindex $rslptData [expr $index + 2]]] \
                                          [GetStringValue [lindex $rslptData [expr $index + 3]]] \
                                          [GetStringValue [lindex $rslptData [expr $index + 4]]]]
                        if { [string is double [lindex $rslptData [expr $index + 5]]] } {
                            append rotationData [format %8s%20s%20s%20s%s \
                                                [lindex $rslptData $index] \
                                                [GetStringValue [lindex $rslptData [expr $index + 5]]] \
                                                [GetStringValue [lindex $rslptData [expr $index + 6]]] \
                                                [GetStringValue [lindex $rslptData [expr $index + 7]]] "\n"]
                        }
                        set index [expr $index + 8]
                    }
                    puts $postResFile "\nEnd Values\n"
                    #puts $tmpFile "\nEnd Values"   
                    append rotationData "\nEnd Values\n"
                    #close $tmpFile 
                    puts $postResFile $rotationData
                    destroy $rotationData
                } else {
                    while { [string is integer [lindex $rslptData $index]] } {
                        puts $postResFile [format %8s%20s%20s%20s \
                                          [lindex $rslptData $index] \
                                          [GetStringValue [lindex $rslptData [expr $index + 2]]] \
                                          [GetStringValue [lindex $rslptData [expr $index + 3]]] \
                                          [GetStringValue [lindex $rslptData [expr $index + 4]]]]
                        set index [expr $index + 5]
                    }
                    puts $postResFile "\nEnd Values\n"
                }

                while { $index < $rslptDataLength && [lindex $rslptData $index] != $divLine && [lindex $rslptData $index] != "Reactions:" && [lindex $rslptData $index] != "Stresses" && [lindex $rslptData $index] != "Strains" } {
                    set index [expr $index + 1]
                }

                if { [lindex $rslptData $index] == "Reactions:" } {
                    PutsReactions3DPostResFile
                }

                while { $index < $rslptDataLength && [lindex $rslptData $index] != $divLine && [lindex $rslptData $index] != "Stresses" && [lindex $rslptData $index] != "Strains" } {
                    set index [expr $index + 1]
                }
                
                if { [lindex $rslptData $index] == "Stresses" || [lindex $rslptData $index] == "Strains" } {
                    PutsStressesStrains3DPostResFile
                }
            }
        }
        close $postResFile

    } 
    # else {
        # OpenWarningWindow "Warning" "To view results execute the command \"rslpt\" at the FEMIX Post Processor" 0
    # }
    #close $result
}

proc PutsStressesStrains3DPostResFile {} {

    # global stressUnit 
    global postResFile rslptData rslptDataLength
    global combNum combFlag index divLine

    global firstMindlinShellQuadGaussPoints \
           firstSolidHexaGaussPoints \
           firstAhmadShellQuadGaussPoints \
           firstEmbCable3DGaussPoints \
           firstCable3DGaussPoints

    while { [lindex $rslptData $index] == "Stresses" || [lindex $rslptData $index] == "Strains" } { 
        
        set loadType [lindex $rslptData $index]
        set elemType [lindex $rslptData [expr $index - 1]]
        
        set index [expr $index + 1]

        while { $index < $rslptDataLength && [lindex $rslptData $index] != $divLine && [lindex $rslptData $index] != "n." && [lindex $rslptData $index] != "Stresses" && [lindex $rslptData $index] != "Strains" } {
            set index [expr $index + 1]
        }

        if { [lindex $rslptData $index] == "n." } {

            switch $elemType {

                "_MINDLIN_SHELL_QUAD" {
                    if { $firstMindlinShellQuadGaussPoints } {
                        puts $postResFile "GaussPoints \"Mindlin Shell Quad Elements (bottom)\" ElemType Quadrilateral\n"
                        puts $postResFile "Number of Gauss Points: 4"
                        #puts $postResFile "Natural Coordinates: Given\n"
                        puts $postResFile "Natural Coordinates: Internal\n"
                        #puts $postResFile [format %28s%20s "0.57735027E+00" "-0.57735027E+00"]
                        #puts $postResFile [format %28s%20s "-0.57735027E+00" "-0.57735027E+00"]
                        #puts $postResFile [format %28s%20s " 0.57735027E+00" "0.57735027E+00"]
                        #puts $postResFile [format %28s%20s%s "-0.57735027E+00" "0.57735027E+00" "\n"]
                        puts $postResFile "End GaussPoints\n"
                        puts $postResFile "GaussPoints \"Mindlin Shell Quad Elements (top)\" ElemType Quadrilateral\n"
                        puts $postResFile "Number of Gauss Points: 4"
                        puts $postResFile "Natural Coordinates: Internal\n"
                        puts $postResFile "End GaussPoints\n"
                        puts $postResFile "GaussPoints \"Mindlin Shell Quad Elements (middle)\" ElemType Quadrilateral\n"
                        puts $postResFile "Number of Gauss Points: 4"
                        puts $postResFile "Natural Coordinates: Internal\n"
                        puts $postResFile "End GaussPoints\n"
                        set firstMindlinShellQuadGaussPoints false
                    }
                    WriteMindlinShellQuadStressesStrains $loadType $combFlag $combNum
                }
                "_AHMAD_SHELL_QUAD" {
                    if { $firstAhmadShellQuadGaussPoints } {
                        puts $postResFile "GaussPoints \"Ahmad Shell Quad Elements\" ElemType Quadrilateral\n"
                        puts $postResFile "Number of Gauss Points: 4"
                        #puts $postResFile "Natural Coordinates: Given\n"
                        puts $postResFile "Natural Coordinates: Internal\n"
                        #puts $postResFile [format %28s%20s "0.57735027E+00" "-0.57735027E+00"]
                        #puts $postResFile [format %28s%20s "-0.57735027E+00" "-0.57735027E+00"]
                        #puts $postResFile [format %28s%20s " 0.57735027E+00" "0.57735027E+00"]
                        #puts $postResFile [format %28s%20s%s "-0.57735027E+00" "0.57735027E+00" "\n"]
                        puts $postResFile "End GaussPoints\n"
                        set firstAhmadShellQuadGaussPoints false
                    }
                    WriteAhmadShellQuadStressesStrains $loadType $combFlag $combNum
                }
                "_SOLID_HEXA" {
                    if { $firstSolidHexaGaussPoints } {
                        puts $postResFile "GaussPoints \"Solid Hexa Elements\" ElemType Hexahedra\n"
                        puts $postResFile "Number of Gauss Points: 8"
                        puts $postResFile "Natural Coordinates: Internal\n"
                        #puts $postResFile [format %28s%20s "0.57735027E+00" "-0.57735027E+00"]
                        #puts $postResFile [format %28s%20s "-0.57735027E+00" "-0.57735027E+00"]
                        #puts $postResFile [format %28s%20s " 0.57735027E+00" "0.57735027E+00"]
                        #puts $postResFile [format %28s%20s%s "-0.57735027E+00" "0.57735027E+00" "\n"]
                        puts $postResFile "End GaussPoints\n"
                        set firstSolidHexaGaussPoints false
                    }
                    WriteSolidHexaStressesStrains $loadType $combFlag $combNum
                }
                "_EMB_CABLE_3D" {
                    if { $firstEmbCable3DGaussPoints } {
                        puts $postResFile "GaussPoints \"Embedded Cable 3D Elements\" ElemType Linear\n"
                        puts $postResFile "Number of Gauss Points: 2"
                        puts $postResFile "Natural Coordinates: Internal\n"
                        puts $postResFile "End GaussPoints\n"
                        set firstEmbCable3DGaussPoints false
                    }
                    WriteCable3DStressesStrains $loadType $combFlag $combNum "Embedded Cable 3D"
                }
                "_CABLE_3D" {
                    if { $firstCable3DGaussPoints } {
                        puts $postResFile "GaussPoints \"Cable 3D Elements\" ElemType Linear\n"
                        puts $postResFile "Number of Gauss Points: 2"
                        puts $postResFile "Natural Coordinates: Internal\n"
                        puts $postResFile "End GaussPoints\n"
                        set firstCable3DGaussPoints false
                    }
                    WriteCable3DStressesStrains $loadType $combFlag $combNum "Cable 3D"
                }
            }
            set index [expr $index - 1]
            while { $index < $rslptDataLength && [lindex $rslptData $index] != $divLine && [lindex $rslptData $index] != "Stresses" && [lindex $rslptData $index] != "Strains" } {
                set index [expr $index + 1]
            }
        }
    }
}

proc WriteAhmadShellQuadStressesStrains { loadType combFlag combNum } {

    global postResFile rslptData index
    # stressUnit 

    puts $postResFile "Result \"$loadType on Gauss Points\" \"$combFlag\" $combNum Vector OnGaussPoints \"Ahmad Shell Quad Elements\"\n"
    set shearData     "Result \"$loadType on Gauss Points\" \"$combFlag\" $combNum Vector OnGaussPoints \"Ahmad Shell Quad Elements\"\n\n"

    if { $loadType == "Stresses" } {
        puts $postResFile "ComponentNames \"Stress-X\" \"Stress-Y\"\n\nValues"
        # puts $postResFile "ComponentNames \"Stress-X ($stressUnit)\" \"Stress-Y ($stressUnit)\"\n\nValues"
        append shearData    "ComponentNames \"ShearStress-XY\" \"ShearStress-YZ\" \"ShearStress-ZX\"\n\nValues\n"
        # append shearData    "ComponentNames \"ShearStress-XY ($stressUnit)\" \"ShearStress-YZ ($stressUnit)\" \"ShearStress-ZX ($stressUnit)\"\n\nValues\n"
        puts $postResFile [format %8s%20s%20s%s "#ElemNum" "Sigma1" "Sigma2" "\n"]
        append shearData    [format %8s%20s%20s%20s%s%s "#ElemNum" "Tau12" "Tau23" "Tau31" "\n" "\n"]
        while { [lindex $rslptData $index] == "n." } {           
            set index [expr $index + 1]
            set elemNum [string range [lindex $rslptData $index] 0 [expr [string length [lindex $rslptData $index]] - 2]] 
            set index [expr $index + 13]
            puts $postResFile [format %8s%20.10e%20.10e $elemNum \
                              [expr ([lindex $rslptData $index] + [lindex $rslptData [expr $index + 36]]) / 2] \
                              [expr ([lindex $rslptData [expr $index + 1]] + [lindex $rslptData [expr $index + 37]]) / 2]]
            append shearData    [format %8s%20.10e%20.10e%20.10e%s $elemNum \
                              [expr ([lindex $rslptData [expr $index + 2]] + [lindex $rslptData [expr $index + 38]] ) / 2] \
                              [expr ([lindex $rslptData [expr $index + 3]] + [lindex $rslptData [expr $index + 39]] ) / 2] \
                              [expr ([lindex $rslptData [expr $index + 4]] + [lindex $rslptData [expr $index + 40]] ) / 2] "\n"]
            for { set i 0 } { $i < 3 } { incr i } {
                set index [expr $index + 9]
                puts $postResFile [format %28.10e%20.10e \
                                  [expr ([lindex $rslptData $index] + [lindex $rslptData [expr $index + 36]]) / 2] \
                                  [expr ([lindex $rslptData [expr $index + 1]] + [lindex $rslptData [expr $index + 37]]) / 2]]
                append shearData    [format %28.10e%20.10e%20.10e%s \
                                  [expr ([lindex $rslptData [expr $index + 2]] + [lindex $rslptData [expr $index + 38]] ) / 2] \
                                  [expr ([lindex $rslptData [expr $index + 3]] + [lindex $rslptData [expr $index + 39]] ) / 2] \
                                  [expr ([lindex $rslptData [expr $index + 4]] + [lindex $rslptData [expr $index + 40]] ) / 2] "\n"]
            }
            set index [expr $index + 42]
        }
    } else {
        # $loadType == "Strains"
        puts $postResFile "ComponentNames \"Strain-X\" \"Strain-Y\"\n\nValues" 
        append shearData    "ComponentNames \"ShearStrain-XY/2\" \"ShearStrain-YZ/2\" \"ShearStrain-ZX/2\"\n\nValues\n"
        puts $postResFile [format %8s%20s%20s%s "#ElemNum" "Ept1" "Ept2" "\n"]
        append shearData    [format %8s%20s%20s%20s%s%s "#ElemNum" "Gamma12/2" "Gamma23/2" "Gamma31/2" "\n" "\n"]
        while { [lindex $rslptData $index] == "n." } {           
            set index [expr $index + 1]
            set elemNum [string range [lindex $rslptData $index] 0 [expr [string length [lindex $rslptData $index]] - 2]] 
            set index [expr $index + 13]
            puts $postResFile [format %8s%20.10e%20.10e $elemNum \
                              [expr ([lindex $rslptData $index] + [lindex $rslptData [expr $index + 36]]) / 2] \
                              [expr ([lindex $rslptData [expr $index + 1]] + [lindex $rslptData [expr $index + 37]]) / 2]]
            append shearData    [format %8s%20.10e%20.10e%20.10e%s $elemNum \
                              [expr ([lindex $rslptData [expr $index + 2]] + [lindex $rslptData [expr $index + 38]] ) / 4] \
                              [expr ([lindex $rslptData [expr $index + 3]] + [lindex $rslptData [expr $index + 39]] ) / 4] \
                              [expr ([lindex $rslptData [expr $index + 4]] + [lindex $rslptData [expr $index + 40]] ) / 4] "\n"]
            for { set i 0 } { $i < 3 } { incr i } {
                set index [expr $index + 9]
                puts $postResFile [format %28.10e%20.10e \
                                  [expr ([lindex $rslptData $index] + [lindex $rslptData [expr $index + 36]]) / 2] \
                                  [expr ([lindex $rslptData [expr $index + 1]] + [lindex $rslptData [expr $index + 37]]) / 2]]
                append shearData    [format %28.10e%20.10e%20.10e%s \
                                  [expr ([lindex $rslptData [expr $index + 2]] + [lindex $rslptData [expr $index + 38]] ) / 4] \
                                  [expr ([lindex $rslptData [expr $index + 3]] + [lindex $rslptData [expr $index + 39]] ) / 4] \
                                  [expr ([lindex $rslptData [expr $index + 4]] + [lindex $rslptData [expr $index + 40]] ) / 4] "\n"]
            }
            set index [expr $index + 42]
        }                                                                  
    }
    puts $postResFile "\nEnd Values\n"
    append shearData "\nEnd Values\n"
    puts $postResFile $shearData
}

proc WriteMindlinShellQuadStressesStrains { loadType combFlag combNum } {

    global postResFile rslptData index
    # stressUnit

    puts $postResFile "Result \"$loadType on Gauss Points\" \"$combFlag\" $combNum Matrix OnGaussPoints \"Mindlin Shell Quad Elements (bottom)\"\n"
    set topLoad "Result \"$loadType on Gauss Points\" \"$combFlag\" $combNum Matrix OnGaussPoints \"Mindlin Shell Quad Elements (top)\"\n"
    set middleLoad "Result \"$loadType on Gauss Points\" \"$combFlag\" $combNum Vector OnGaussPoints \"Mindlin Shell Quad Elements (middle)\"\n"

    if { $loadType == "Stresses"} {
        puts $postResFile "ComponentNames \"Stress-X\" \"Stress-Y\" \"ShearStress-XY\"\n\nValues"
        # puts $postResFile "ComponentNames \"Stress-X ($stressUnit)\" \"Stress-Y ($stressUnit)\" \"ShearStress-XY ($stressUnit)\"\n\nValues"
        append topLoad "ComponentNames \"Stress-X\" \"Stress-Y\" \"ShearStress-XY\"\n\nValues\n"
        # append topLoad "ComponentNames \"Stress-X ($stressUnit)\" \"Stress-Y ($stressUnit)\" \"ShearStress-XY ($stressUnit)\"\n\nValues\n"
        append middleLoad "ComponentNames \"ShearStress-YZ\" \"ShearStressZX\"\n\nValues\n"
        # append middleLoad "ComponentNames \"ShearStress-YZ ($stressUnit)\" \"ShearStressZX ($stressUnit)\"\n\nValues\n"
        #puts $postResFile "Values\n"
        puts $postResFile [format %8s%20s%20s%20s%s "#ElemNum" "Sigma1-Bottom" "Sigma2-Bottom" "Tau12-Bottom" "\n"]
        append topLoad    [format %8s%20s%20s%20s%s%s "#ElemNum" "Sigma1-Top" "Sigma2-Top" "Tau12-Top" "\n" "\n"]
        append middleLoad  [format %8s%20s%20s%s%s "#ElemNum" "Tau23-Middle" "Tau31-Middle" "\n" "\n"]
        
        while { [lindex $rslptData $index] == "n." } {
            set index [expr $index + 1]
            set elemNum [string range [lindex $rslptData $index] 0 [expr [string length [lindex $rslptData $index]] - 2]] 
            set index [expr $index + 11]
            puts $postResFile [format %8s%20.10e%20.10e%20.10e $elemNum \
                              [lindex $rslptData $index] \
                              [lindex $rslptData [expr $index + 1]] \
                              [lindex $rslptData [expr $index + 2]]]
            append topLoad    [format %8s%20.10e%20.10e%20.10e%s $elemNum \
                              [lindex $rslptData [expr $index + 34]] \
                              [lindex $rslptData [expr $index + 35]] \
                              [lindex $rslptData [expr $index + 36]] "\n"]
                              
            append middleLoad  [format %8s%20.10e%20.10e%s $elemNum \
                              [lindex $rslptData [expr $index + 67]] \
                              [lindex $rslptData [expr $index + 68]] "\n"]
            for { set i 0 } { $i < 3 } { incr i } {
                set index [expr $index + 7]
                puts $postResFile [format %28.10e%20.10e%20.10e \
                                  [lindex $rslptData $index] \
                                  [lindex $rslptData [expr $index + 1]] \
                                  [lindex $rslptData [expr $index + 2]]]
                append topLoad    [format %28.10e%20.10e%20.10e%s \
                                  [lindex $rslptData [expr $index + 34]] \
                                  [lindex $rslptData [expr $index + 35]] \
                                  [lindex $rslptData [expr $index + 36]] "\n"]
                append middleLoad  [format %28.10e%20.10e%s \
                                  [lindex $rslptData [expr $index + 66 - $i]] \
                                  [lindex $rslptData [expr $index + 67 - $i]] "\n"]
            }
            set index [expr $index + 67]
        }

    } else {
        # $loadType == "Strains"
        puts $postResFile "ComponentNames \"Strain-X\" \"Strain-Y\" \"ShearStrain-XY/2\"\n\nValues" 
        append topLoad    "ComponentNames \"Strain-X\" \"Strain-Y\" \"ShearStrain-XY/2\"\n\nValues\n"
        append middleLoad  "ComponentNames \"ShearStrain-YZ/2\" \"ShearStrain-ZX/2\"\n\nValues\n"
        #puts $postResFile "Values\n"
        puts $postResFile [format %8s%20s%20s%20s%s "#ElemNum" "Eps1-Bottom" "Eps2-Bottom" "Gamma12-Bottom/2" "\n"]
        append topLoad    [format %8s%20s%20s%20s%s%s "#ElemNum" "Eps1-Top" "Eps2-Top" "Gamma12-Top/2" "\n" "\n"]
        append middleLoad  [format %8s%20s%20s%s%s "#ElemNum" "Gamma23-Middle/2" "Gamma31-Middle/2" "\n" "\n"]
    
        while { [lindex $rslptData $index] == "n." } {
        
            set index [expr $index + 1]
            set elemNum [string range [lindex $rslptData $index] 0 [expr [string length [lindex $rslptData $index]] - 2]] 
            set index [expr $index + 11]
    
            puts $postResFile [format %8s%20.10e%20.10e%20.10e $elemNum \
                              [lindex $rslptData $index] \
                              [lindex $rslptData [expr $index + 1]] \
                              [expr [lindex $rslptData [expr $index + 2]] / 2]]
    
            append topLoad    [format %8s%20.10e%20.10e%20.10e%s $elemNum \
                              [lindex $rslptData [expr $index + 34]] \
                              [lindex $rslptData [expr $index + 35]] \
                              [expr [lindex $rslptData [expr $index + 36]] / 2] "\n"]
            
            append middleLoad  [format %8s%20.10e%20.10e%s $elemNum \
                              [expr [lindex $rslptData [expr $index + 67]] / 2] \
                              [expr [lindex $rslptData [expr $index + 68]] / 2] "\n"]
                                                                           
            for { set i 0 } { $i < 3 } { incr i } {
                set index [expr $index + 7]
                puts $postResFile [format %28.10e%20.10e%20.10e \
                                  [lindex $rslptData $index] \
                                  [lindex $rslptData [expr $index + 1]] \
                                  [expr [lindex $rslptData [expr $index + 2]] / 2]]
                append topLoad    [format %28.10e%20.10e%20.10e%s \
                                  [lindex $rslptData [expr $index + 34]] \
                                  [lindex $rslptData [expr $index + 35]] \
                                  [expr [lindex $rslptData [expr $index + 36]] / 2] "\n"]
                append middleLoad  [format %28.10e%20.10e%s \
                                  [expr [lindex $rslptData [expr $index + 66 - $i]] / 2] \
                                  [expr [lindex $rslptData [expr $index + 67 - $i]] / 2] "\n"]
            }
            set index [expr $index + 67]
        }
    }
    puts $postResFile "\nEnd Values\n"
    append topLoad "\nEnd Values\n"
    append middleLoad "\nEnd Values\n"
    puts $postResFile $topLoad
    puts $postResFile $middleLoad
}

proc WriteCable3DStressesStrains { loadType combFlag combNum cableType } {

    global postResFile rslptData index  
    # stressUnit 
    
    puts $postResFile "Result \"$loadType on Gauss Points\" \"$combFlag\" $combNum Scalar OnGaussPoints \"$cableType Elements\"\n"

    if { $loadType == "Stresses"} {
        puts $postResFile "ComponentNames \"Sigma-X ($cableType Elements)\"\n"
        puts $postResFile "Values"
        puts $postResFile [format %8s%20s%s "#ElemNum" "Sigma-X" "\n"]
    } else {
        puts $postResFile "ComponentNames \"Eps-X ($cableType Elements)\"\n"
        puts $postResFile "Values"
        puts $postResFile [format %8s%20s%s "#ElemNum" "Eps-X" "\n"]
    }
    
    while { [lindex $rslptData $index] == "n." } {
    
        set index [expr $index + 1]
        set elemNum [string range [lindex $rslptData $index] 0 [expr [string length [lindex $rslptData $index]] - 2]] 
        set index [expr $index + 9]
        puts $postResFile [format %8s%20.10e $elemNum [lindex $rslptData $index]]
        set index [expr $index + 5]
        puts $postResFile [format %28.10e [lindex $rslptData $index]]
        set index [expr $index + 2]

    }
    puts $postResFile "\nEnd Values\n"
}

proc WriteSolidHexaStressesStrains { loadType combFlag combNum } {

    global postResFile rslptData index
    # stressUnit

    puts $postResFile "Result \"$loadType on Gauss Points\" \"$combFlag\" $combNum Matrix OnGaussPoints \"Solid Hexa Elements\"\n"
    #set topLoad "Result \"$loadType on Gauss Points (top)\" \"$combFlag\" $combNum Matrix OnGaussPoints \"Mindlin Shell Quad Elements\"\n"

    if { $loadType == "Stresses" } {
        puts $postResFile "ComponentNames \"Stress-X\" \"Stress-Y\" \"Stress-Z\" \"ShearStress-YZ\" \"ShearStress-ZX\" \"ShearStress-XY\"\n\nValues"
        # puts $postResFile "ComponentNames \"Stress-X ($stressUnit)\" \"Stress-Y ($stressUnit)\" \"Stress-Z ($stressUnit)\" \"ShearStress-YZ ($stressUnit)\" \"ShearStress-ZX ($stressUnit)\" \"ShearStress-XY ($stressUnit)\"\n\nValues"
        #append topLoad "ComponentNames \"Stress-X ($stressUnit)\" \"Stress-Y ($stressUnit)\" \"ShearStress-XY ($stressUnit)\"\n\nValues\n"
        puts $postResFile [format %8s%20s%20s%20s%20s%20s%20s%s "#ElemNum" "Sigma1" "Sigma2" "Sigma3" "Tau23" "Tau31" "Tau12" "\n"]
        #append topLoad    [format %8s%20s%20s%20s%s%s "#ElemNum" "Sigma1-Top" "Sigma2-Top" "Tau12-Top" "\n" "\n"]
        while { [lindex $rslptData $index] == "n." } {           
            set index [expr $index + 1]
            set elemNum [string range [lindex $rslptData $index] 0 [expr [string length [lindex $rslptData $index]] - 2]] 
            set index [expr $index + 14]
            puts $postResFile [format %8s%20.10e%20.10e%20.10e%20.10e%20.10e%20.10e $elemNum \
                              [lindex $rslptData $index] \
                              [lindex $rslptData [expr $index + 1]] \
                              [lindex $rslptData [expr $index + 2]] \
                              [lindex $rslptData [expr $index + 3]] \
                              [lindex $rslptData [expr $index + 4]] \
                              [lindex $rslptData [expr $index + 5]]]
            for { set i 0 } { $i < 7 } { incr i } {
                set index [expr $index + 10]
                puts $postResFile [format %28.10e%20.10e%20.10e%20.10e%20.10e%20.10e \
                                  [lindex $rslptData $index] \
                                  [lindex $rslptData [expr $index + 1]] \
                                  [lindex $rslptData [expr $index + 2]] \
                                  [lindex $rslptData [expr $index + 3]] \
                                  [lindex $rslptData [expr $index + 4]] \
                                  [lindex $rslptData [expr $index + 5]]]
            }
            set index [expr $index + 7]
        }
    } else {
        # $loadType == "Strains"
        puts $postResFile "ComponentNames \"Strain-X\" \"Strain-Y\" \"Strain-Z\" \"ShearStrain-YZ/2\" \"ShearStrain-ZX/2\" \"ShearStrain-XY/2\"\n\nValues" 
        #append topLoad    "ComponentNames \"Strain-X\" \"Strain-Y\" \"ShearStrain-XY/2\"\n\nValues\n"
        puts $postResFile [format %8s%20s%20s%20s%20s%20s%20s%s "#ElemNum" "Eps1" "Eps2" "Eps3" "Gamma23/2" "Gamma31/2" "Gamma12/2" "\n"]
        #append topLoad    [format %8s%20s%20s%20s%s%s "#ElemNum" "Eps1-Top" "Eps2-Top" "Gamma12-Top/2" "\n" "\n"]
        while { [lindex $rslptData $index] == "n." } {           
            set index [expr $index + 1]
            set elemNum [string range [lindex $rslptData $index] 0 [expr [string length [lindex $rslptData $index]] - 2]] 
            set index [expr $index + 14]
            puts $postResFile [format %8s%20.10e%20.10e%20.10e%20.10e%20.10e%20.10e $elemNum \
                              [lindex $rslptData $index] \
                              [lindex $rslptData [expr $index + 1]] \
                              [lindex $rslptData [expr $index + 2]] \
                              [expr [lindex $rslptData [expr $index + 3]] / 2] \
                              [expr [lindex $rslptData [expr $index + 4]] / 2] \
                              [expr [lindex $rslptData [expr $index + 5]] / 2]]
            for { set i 0 } { $i < 7 } { incr i } {
                set index [expr $index + 10]
                puts $postResFile [format %28.10e%20.10e%20.10e%20.10e%20.10e%20.10e \
                                  [lindex $rslptData $index] \
                                  [lindex $rslptData [expr $index + 1]] \
                                  [lindex $rslptData [expr $index + 2]] \
                                  [expr [lindex $rslptData [expr $index + 3]] / 2] \
                                  [expr [lindex $rslptData [expr $index + 4]] / 2] \
                                  [expr [lindex $rslptData [expr $index + 5]] / 2]]
            }
            set index [expr $index + 7]
        }                                                                  
    }
    puts $postResFile "\nEnd Values\n"
}

proc PutsReactions3DPostResFile {} {

    # global strengthUnit lengthUnit      
    global postResFile rslptData rslptDataLength
    global combNum combFlag index

    puts $postResFile "Result \"Reactions (Force)\" \"$combFlag\" $combNum Vector OnNodes\n" 
    puts $postResFile "ComponentNames \"X-Reaction\" \"Y-Reaction\" \"Z-Reaction\"\n"
    # puts $postResFile "ComponentNames \"X-Reaction ($strengthUnit)\" \"Y-Reaction ($strengthUnit)\" \"Z-Reaction ($strengthUnit)\"\n"
    puts $postResFile "Values"
    puts $postResFile "#NodeNum          X-Reaction          Y-Reaction          Z-Reaction\n"

    while { $index < $rslptDataLength && ![string is integer [lindex $rslptData $index]] } {
        set index [expr $index + 1]
    }

    if { [lindex $rslptData [expr $index - 1]] == "R3" } {
        
        set rotationData "Result \"Reactions (Moment)\" \"$combFlag\" $combNum Vector OnNodes\n\n" 
        append rotationData "ComponentNames \"X-Moment\" \"Y-Moment\" \"Z-Moment\"\n\n"
        # append rotationData "ComponentNames \"X-Moment ($strengthUnit*$lengthUnit)\" \"Y-Moment ($strengthUnit*$lengthUnit)\" \"Z-Moment ($strengthUnit*$lengthUnit)\"\n\n"
        append rotationData "Values\n"
        append rotationData "#NodeNum            X-Moment            Y-Moment            Z-Moment\n\n"

        while { $index < $rslptDataLength && [string is integer [lindex $rslptData $index]] } {
            puts $postResFile [format %8s%20s%20s%20s \
                              [lindex $rslptData $index] \
                              [GetStringValue [lindex $rslptData [expr $index + 2]]] \
                              [GetStringValue [lindex $rslptData [expr $index + 3]]] \
                              [GetStringValue [lindex $rslptData [expr $index + 4]]]]
    
                if { [string is double [lindex $rslptData [expr $index + 5]]] } {
                    append rotationData [format %8s%20s%20s%20s%s \
                                        [lindex $rslptData $index] \
                                        [lindex $rslptData [expr $index + 5]] \
                                        [GetStringValue [lindex $rslptData [expr $index + 6]]] \
                                        [GetStringValue [lindex $rslptData [expr $index + 7]]] "\n"]
                }
                set index [expr $index + 8]
        }
        puts $postResFile "\nEnd Values\n"
        append rotationData "\nEnd Values\n"
        puts $postResFile $rotationData
        destroy $rotationData
    } else {
        while { $index < $rslptDataLength && [string is integer [lindex $rslptData $index]] } {
            puts $postResFile   [format %8s%20s%20s%20s \
                                [lindex $rslptData $index] \
                                [GetStringValue [lindex $rslptData [expr $index + 2]]] \
                                [GetStringValue [lindex $rslptData [expr $index + 3]]] \
                                [GetStringValue [lindex $rslptData [expr $index + 4]]]]
    
            set index [expr $index + 5]
        }
        puts $postResFile "\nEnd Values\n"

    }
}