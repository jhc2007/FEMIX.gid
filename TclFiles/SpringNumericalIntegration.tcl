proc GetSpringNumericalIntegrationIndex { numberOfPointsList } {

    global SpringNumericalIntegrationList

    for {set i 0} {$i < [llength $SpringNumericalIntegrationList]} {incr i} {
        if { [lindex $SpringNumericalIntegrationList $i] == $numberOfPointsList } {
            return [expr $i + 1]
        }
    }
    lappend SpringNumericalIntegrationList $numberOfPointsList
    return [llength $SpringNumericalIntegrationList]
}

# proc ExistsSpringNumericalIntegration { intName } {

#     global SpringNumericalIntegrationList

#     for {set i 0} {$i < [llength $SpringNumericalIntegrationList]} {incr i} {
#         if { [lindex [lindex $SpringNumericalIntegrationList $i] 0] == $intName } {
#             return true
#         }
#     }
#     return false
# }

proc CountSpringNumericalIntegration {} {

    global SpringNumericalIntegrationList
    return [llength $SpringNumericalIntegrationList]
}

proc WriteSpringsNumericalIntegration { index } {

    global SpringNumericalIntegrationList

    set returnString ""
    incr index

    for {set i 0} {$i < [llength $SpringNumericalIntegrationList]} {incr i} {
        append returnString [format %7d $index]
        append returnString "  SPRING_NUMERICAL_INTEGRATION_[expr $i + 1]"
        set numKeywords [llength [lindex $SpringNumericalIntegrationList $i]]
        append returnString "  $numKeywords"
        for {set j 0} {$j < $numKeywords} {incr j} {
            append returnString "  _G_S[expr $j + 1]_[lindex [lindex $SpringNumericalIntegrationList $i] $j]"
        }
        append returnString " ;\n"
        incr index
    }

    return $returnString
}