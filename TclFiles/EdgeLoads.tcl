# EdgeLoads.tcl

proc CreateEdgeLoadsList { interval } {

    # global PROBLEMTYPEPATH 
    # set result [open "$PROBLEMTYPEPATH\\result_new.dat" "w"]
    # puts $result [GiD_Info Geometry]
    # close $result

  	global edgeLoadsConditionsOverNodes \
           edgeLoadsConditionsOverLines
  	  
    set edgeLoadsConditionsOverLines [GiD_Info Conditions -interval $interval Edge_Loads Geometry]  
  	set edgeLoadsConditionsOverNodes [GiD_Info Conditions -interval $interval Edge_Loads Mesh]

  	if { [llength $edgeLoadsConditionsOverLines] == 0 } {
  	    return 0
  	}

  	global EdgeLoadsList \
  		     edgeLoadsNodes

  	set EdgeLoadsList {}
  	set elementConditions {}
  	  
  	set edgeLoadsNodes {}
  	for {set i 0} {$i < [llength $edgeLoadsConditionsOverNodes]} {incr i} {
  	    lappend edgeLoadsNodes [lindex [lindex $edgeLoadsConditionsOverNodes $i] 1]
  	}

	  set elemsType { "Linear" "Quadrilateral" "Hexahedra" }

  	for {set i 0} {$i < [llength $elemsType]} {incr i} {
        set elemsList [GiD_Info Mesh elements [lindex $elemsType $i] -sublist]
  	    for {set j 0} {$j < [llength $elemsList]} {incr j} {
           	set elemNodes {}
        	  for {set k 1} {$k < [expr [llength [lindex $elemsList $j]] - 1]} {incr k} {
            	  if { [ExistsListElement $edgeLoadsNodes [lindex [lindex $elemsList $j] $k]] } {
                    lappend elemNodes [lindex [lindex $elemsList $j] $k]
            	  }
        	  }
          	if { [llength $elemNodes] > 1 } {
           		  switch [lindex $elemsType $i] {
                    "Linear" {
                      	if { [llength [lindex $elemsList $j]] == 5 } {
                            # Linear Element With 3 Nodes
                            set edgeNode1 [lindex [lindex $elemsList $j] 1]
                            set edgeNode2 [lindex [lindex $elemsList $j] 2]
                          	set middleEdgeNode [lindex [lindex $elemsList $j] 3]
                          	while { [ExistsListElement $edgeLoadsNodes $middleEdgeNode] } {
                              	# set nodeConditions [GetFirstNodeEdgeLoadsConditions $middleEdgeNode]
                                set elementCond [list [lindex [lindex $elemsList $j] 0] 0 1 [GetFirstNodeEdgeLoadsConditions $middleEdgeNode] $edgeNode1 $middleEdgeNode $edgeNode2]
                              	RemoveMiddleNodeEdgeLoadsConditions $middleEdgeNode
                              	# lappend elementCond $nodeConditions
                              	lappend elementConditions $elementCond                              
                          	}
                      	} else {
                      		  # Linear Element With 2 Nodes
                            set edgeNode1 [lindex [lindex $elemsList $j] 1]
                            set edgeNode2 [lindex [lindex $elemsList $j] 2]
                          	set nodeConditions1 [GetAllNodeEdgeLoadsConditions $edgeNode1]
                          	set nodeConditions2 [GetAllNodeEdgeLoadsConditions $edgeNode2]
                          	set x 0
                          	while { $x < [llength $nodeConditions1] } {
                          		  set y 0
                               	while { $y < [llength $nodeConditions2] } {
                                   	if { [EqualEdgeLoadsConditions [lindex $nodeConditions1 $x] [lindex $nodeConditions2 $y]] } {
                                        set lines [GetAssignedEdgeLoadsLines [lindex $nodeConditions1 $x]]
                                        if { [NodesInsideSameLine $edgeNode1 $edgeNode2 $lines] } {
                                       		  set elementCond [list [lindex [lindex $elemsList $j] 0] 0 1 [lindex $nodeConditions1 $x] $edgeNode1 0 $edgeNode2]
                              				      lappend elementConditions $elementCond 
                                           	# lappend elementCond 
                                           	# Remove2NodeEdgeLoadsConditions [lindex $nodeConditions1 $x] [lindex [lindex $elemsList $j] 1] [lindex [lindex $elemsList $j] 2]
                                           	set nodeConditions2 [lreplace $nodeConditions2 $y $y]
                                           	# set x [llength $nodeConditions1]
                                        }
                                       	set y [llength $nodeConditions2]
                                   	}
                                   	incr y
                               	}
                               	incr x
                           	}
                      	}
                    }
                    "Quadrilateral" {
                        set edges [GetQuadElementEdges [lrange [lindex $elemsList $j] 1 end-1] $elemNodes]
                        if { [llength [lindex $elemsList $j]] == 10 || [llength [lindex $elemsList $j]] == 11 } {
                            # Quad Element With 8 Nodes
                            for {set n 0} {$n < [llength $edges]} {incr n} {
                                set edgeNode1 [lindex [lindex $elemsList $j] [lindex $edges $n]]
                                set edgeNode2 [lindex [lindex $elemsList $j] [expr [lindex $edges $n] % 4 + 1]]
                                set middleEdgeNode [lindex [lindex $elemsList $j] [expr [lindex $edges $n] + 4]]
                                while { [ExistsListElement $edgeLoadsNodes $middleEdgeNode] } {
                                    set elementCond [list [lindex [lindex $elemsList $j] 0] 1 [lindex $edges $n] [GetFirstNodeEdgeLoadsConditions $middleEdgeNode] $edgeNode1 $middleEdgeNode $edgeNode2]
                                    RemoveMiddleNodeEdgeLoadsConditions $middleEdgeNode
                                    lappend elementConditions $elementCond                              
                                }
                            }
                        } else {
                          	# Quad Element With 4 Nodes
                          	for {set n 0} {$n < [llength $edges]} {incr n} {
                                set edgeNode1 [lindex [lindex $elemsList $j] [lindex $edges $n]]
                                set edgeNode2 [lindex [lindex $elemsList $j] [expr [lindex $edges $n] % 4 + 1]]
                              	set nodeConditions1 [GetAllNodeEdgeLoadsConditions $edgeNode1]
                              	set nodeConditions2 [GetAllNodeEdgeLoadsConditions $edgeNode2]
                              	set x 0
                              	while { $x < [llength $nodeConditions1] } {
                                  	set y 0
                                  	while { $y < [llength $nodeConditions2] } {
                                      	if { [EqualEdgeLoadsConditions [lindex $nodeConditions1 $x] [lindex $nodeConditions2 $y]] } {
                                            set lines [GetAssignedEdgeLoadsLines [lindex $nodeConditions1 $x]]
                                            if { [NodesInsideSameLine $edgeNode1 $edgeNode2 $lines] } {
                                  				      set elementCond [list [lindex [lindex $elemsList $j] 0] 1 [lindex $edges $n] [lindex $nodeConditions1 $x] $edgeNode1 0 $edgeNode2]
                                  				      lappend elementConditions $elementCond                              
                                              	# Remove2NodeEdgeLoadsConditions [lindex $nodeConditions1 $x] [lindex [lindex $elemsList $j] [lindex $edges $n]] [lindex [lindex $elemsList $j] [expr [lindex $edges $n] % 4 + 1]]
                                              	set nodeConditions2 [lreplace $nodeConditions2 $y $y]
                                              	# set x [llength $nodeConditions1]
                                            }
                                            set y [llength $nodeConditions2]
                                      	}
                                      	incr y
                                  	}
                                  	incr x
                              	}
                          	}
                      	}
                  	}
                  	"Hexahedra" {
                      	set facesEdges [GetHexaElementFacesEdges [lrange [lindex $elemsList $j] 1 end-1] $elemNodes]
                      	if { [llength [lindex $elemsList $j]] == 22 } {
                          	# Hexa Element With 20 Nodes
                          	for {set n 0} {$n < [llength $facesEdges]} {incr n} {
                              	set face [lindex [lindex $facesEdges $n] 0]
                              	set edge [lindex [lindex $facesEdges $n] 1]
                              	set nodes [GetHexaElemNodesInFemixOrder [lrange [lindex $elemsList $j] 1 end-1]]
                              	set edgeNodes [GetHexaElemEdgeNodes $nodes $edge]
                              	while { [ExistsListElement $edgeLoadsNodes [lindex $edgeNodes 1]] } {
                                  	set elementCond [list [lindex [lindex $elemsList $j] 0] $face $edge [GetFirstNodeEdgeLoadsConditions [lindex $edgeNodes 1]] [lindex $edgeNodes 0] [lindex $edgeNodes 1] [lindex $edgeNodes 2]]
                                  	RemoveMiddleNodeEdgeLoadsConditions [lindex $edgeNodes 1]
                                  	lappend elementConditions $elementCond                              
                              	}
                          	}
                      	} else {
                          	# Hexa Element With 8 Nodes
                          	for {set n 0} {$n < [llength $facesEdges]} {incr n} {
                              	set face [lindex [lindex $facesEdges $n] 0]
                              	set edge [lindex [lindex $facesEdges $n] 1]
                              	set nodes [lrange [lindex $elemsList $j] 1 end-1]
                              	set edgeNodes [GetHexaElemEdgeNodes $nodes $edge]
                              	set nodeConditions1 [GetAllNodeEdgeLoadsConditions [lindex $edgeNodes 0]]
                              	set nodeConditions2 [GetAllNodeEdgeLoadsConditions [lindex $edgeNodes 1]]
                                set x 0
                                while { $x < [llength $nodeConditions1] } {
                                    set y 0
                                    while { $y < [llength $nodeConditions2] } {
                                        if { [EqualEdgeLoadsConditions [lindex $nodeConditions1 $x] [lindex $nodeConditions2 $y]] } {
                                            set lines [GetAssignedEdgeLoadsLines [lindex $nodeConditions1 $x]]
                                            if { [NodesInsideSameLine [lindex $edgeNodes 0] [lindex $edgeNodes 1] $lines] } {
                                  				      set elementCond [list [lindex [lindex $elemsList $j] 0] $face $edge [lindex $nodeConditions1 $x] [lindex $edgeNodes 0] 0 [lindex $edgeNodes 1]]
                                              	set nodeConditions2 [lreplace $nodeConditions2 $y $y]
                                  				      lappend elementConditions $elementCond                              
                                            }
                                            set y [llength $nodeConditions2]
                                      	}
                                      	incr y
                                  	}
                                  	incr x
                              	}
                          	}
                      	}
                  	}
                }
            }
        }
    }

    # close $result
    # return 0

    # AddEdgeLoads { element face edge conditions edgeNode1 middleEdgeNode edgeNode2 }
    AddEdgeLoads [lindex [lindex $elementConditions 0] 0] \
                 [lindex [lindex $elementConditions 0] 1] \
                 [lindex [lindex $elementConditions 0] 2] \
                 [lindex [lindex $elementConditions 0] 3] \
                 [lindex [lindex $elementConditions 0] 4] \
                 [lindex [lindex $elementConditions 0] 5] \
                 [lindex [lindex $elementConditions 0] 6]


    for {set i 1} {$i < [llength $elementConditions]} {incr i} {
      	if { ![UpdateEdgeLoads [lindex $elementConditions [expr $i - 1]] [lindex $elementConditions $i]] } {
            AddEdgeLoads [lindex [lindex $elementConditions $i] 0] \
                         [lindex [lindex $elementConditions $i] 1] \
                         [lindex [lindex $elementConditions $i] 2] \
                         [lindex [lindex $elementConditions $i] 3] \
                         [lindex [lindex $elementConditions $i] 4] \
                         [lindex [lindex $elementConditions $i] 5] \
                         [lindex [lindex $elementConditions $i] 6]
      	}
  	} 
 
  	destroy $edgeLoadsConditionsOverNodes \
            $edgeLoadsNodes \
            $elementConditions

  	return [CountEdgeLoads]
}

proc NodesInsideSameLine { node1 node2 lines } {

    for {set i 0} {$i < [llength $lines]} {incr i} {
        set node1InsideLine [GiD_Info IsPointInside Line [lindex $lines $i] [lindex [GiD_Info Coordinates $node1 Mesh] 0]]
        set node2InsideLine [GiD_Info IsPointInside Line [lindex $lines $i] [lindex [GiD_Info Coordinates $node2 Mesh] 0]]
        if { $node1InsideLine == 1 && $node2InsideLine == 1 } {
            return true
        }
    }
    return false
}

proc GetAssignedEdgeLoadsLines { conditions } {

    global edgeLoadsConditionsOverLines

    set lines {}
    for {set i 0} {$i < [llength $edgeLoadsConditionsOverLines]} {incr i} {
        if { [EqualEdgeLoadsConditions [lrange [lindex $edgeLoadsConditionsOverLines $i] 3 end] $conditions] } {
            lappend lines [lindex [lindex $edgeLoadsConditionsOverLines $i] 1]
        }
    }

    return $lines
}

# proc Remove2NodeEdgeLoadsConditions { conditions node1 node2 } {

#     global edgeLoadsConditionsOverNodes \
#            edgeLoadsNodes
    
#     set index 0
#     set index1 -1
#     set index2 -1

#     while { $index1 < 0 || $index2 < 0 } {

#         if { [lindex $edgeLoadsNodes $index] == $node1 } {
#             set conditions1 [GetFirstNodeEdgeLoadsConditions $node1]
#             if { [EqualEdgeLoadsConditions $conditions $conditions1] } {
#                 set index1 $index
#             }
#         } elseif { [lindex $edgeLoadsNodes $index] == $node2 } {
#             set conditions2 [GetFirstNodeEdgeLoadsConditions $node2]
#             if { [EqualEdgeLoadsConditions $conditions $onditions2] } {
#                 set index2 $index
#             }
#         }
#         incr index
#     }

#     set edgeLoadsNodes [lreplace $edgeLoadsNodes $index1 $index1]
#     set edgeLoadsConditionsOverNodes [lreplace $edgeLoadsConditionsOverNodes $index1 $index1]

#     if { $index2 > $index1 } {
#         set index2 [expr $index2 - 1]
#     }

#     set edgeLoadsNodes [lreplace $edgeLoadsNodes $index2 $index2]
#     set edgeLoadsConditionsOverNodes [lreplace $edgeLoadsConditionsOverNodes $index2 $index2]
# }

proc AddEdgeLoads { element face edge conditions edgeNode1 middleEdgeNode edgeNode2 } {

    global EdgeLoadsList \
           numDimension
           # AuxiliaryVectorsList \

    set edgeLoad {}

    lappend edgeLoad $element
    lappend edgeLoad $element

    if { $face == 0 } {
        set face "_NONE"
    }

    lappend edgeLoad $face
    lappend edgeLoad $edge

    switch [lindex $conditions 0] {

        "Global" {
            set direction [string index [lindex $conditions 1] end]
            if { $numDimension == 2 } {
              set direction [expr $direction % 3 + 1]
            } 
            lappend edgeLoad "_XG$direction"
        }
        "Frame" {
            set direction [string index [lindex $conditions 2] end]
            if { $numDimension == 2 } {
              set direction [expr $direction % 3 + 1]
            } 
            lappend edgeLoad "_L$direction"
        }
        "Solid/Shell" {
            lappend edgeLoad "_[lindex $conditions 3]"
        }
        "Plane_Stress/Strain/Axisymmetry" {
            lappend edgeLoad "_[lindex $conditions 4]"
        }
        "Specified" {
            set vectorName "VECTOR_[GetAuxiliaryVectorIndexFromComponents [lindex $conditions 5] [lindex $conditions 6] [lindex $conditions 7]]"
            lappend edgeLoad $vectorName
        }
    }

    if { [lindex $conditions 8] == "Force" } {
      lappend edgeLoad "_FORC"
    } else {
      lappend edgeLoad "_MOM"
    }

    set numNodes [lindex $conditions 9]

    if { $numNodes == "All" } {
        lappend edgeLoad "1"
        lappend edgeLoad [lindex $conditions 10]
    } else {
        if { $middleEdgeNode == 0 } {
            lappend edgeLoad "2"
        } else {
            lappend edgeLoad "3"
        }
        if { $numNodes == 2 } {
            lappend edgeLoad [GetLoadValueFrom2NodesLineShapeFunction $edgeNode1 \
                                                      [lindex $conditions 11] \
                                                      [lindex $conditions 12] \
                                                      [lindex $conditions 13] \
                                                      [lindex $conditions 14]]
            if { $middleEdgeNode != 0 } {
                lappend edgeLoad [GetLoadValueFrom2NodesLineShapeFunction $middleEdgeNode \
                                                          [lindex $conditions 11] \
                                                          [lindex $conditions 12] \
                                                          [lindex $conditions 13] \
                                                          [lindex $conditions 14]] 
            }
            lappend edgeLoad [GetLoadValueFrom2NodesLineShapeFunction $edgeNode2 \
                                                      [lindex $conditions 11] \
                                                      [lindex $conditions 12] \
                                                      [lindex $conditions 13] \
                                                      [lindex $conditions 14]]             
        } else {
            lappend edgeLoad [GetLoadValueFrom3NodesLineShapeFunction $edgeNode1 \
                                                      [lindex $conditions 11] \
                                                      [lindex $conditions 12] \
                                                      [lindex $conditions 13] \
                                                      [lindex $conditions 14] \
                                                      [lindex $conditions 15] \
                                                      [lindex $conditions 16]]
            if { $middleEdgeNode != 0 } {
                lappend edgeLoad [GetLoadValueFrom3NodesLineShapeFunction $middleEdgeNode \
                                                          [lindex $conditions 11] \
                                                          [lindex $conditions 12] \
                                                          [lindex $conditions 13] \
                                                          [lindex $conditions 14] \
                                                          [lindex $conditions 15] \
                                                          [lindex $conditions 16]]  

            }
            lappend edgeLoad [GetLoadValueFrom3NodesLineShapeFunction $edgeNode2 \
                                                      [lindex $conditions 11] \
                                                      [lindex $conditions 12] \
                                                      [lindex $conditions 13] \
                                                      [lindex $conditions 14] \
                                                      [lindex $conditions 15] \
                                                      [lindex $conditions 16]]  
        }
        # lappend edgeLoad $numNodes
        # for {set i 11} {$i < [expr 11 + $numNodes]} {incr i} {
        #     lappend edgeLoad [lindex $conditions $i]
        # }
    }

    lappend EdgeLoadsList $edgeLoad
}

proc GetLoadValueFrom3NodesLineShapeFunction { node node1 load1 node2 load2 node3 load3 } {

    set coordinates  [GiD_Info Coordinates $node Mesh]
    set coordinates1 [GiD_Info Coordinates $node1 Geometry]
    set coordinates2 [GiD_Info Coordinates $node2 Geometry]
    set coordinates3 [GiD_Info Coordinates $node3 Geometry]

    if { [lindex [lindex $coordinates1 0] 0] != [lindex [lindex $coordinates2 0] 0] } {
        set x [lindex [lindex $coordinates 0] 0]
        set x1 [lindex [lindex $coordinates1 0] 0]
        set x2 [lindex [lindex $coordinates2 0] 0]
        set x3 [lindex [lindex $coordinates3 0] 0]
        set n1 [expr double(($x - $x2) * ($x - $x3) / (($x1 - $x2) * ($x1 - $x3)))]
        set n2 [expr double(($x - $x1) * ($x - $x3) / (($x2 - $x1) * ($x2 - $x3)))]
        set n3 [expr double(($x - $x1) * ($x - $x2) / (($x3 - $x1) * ($x3 - $x2)))]
    } elseif { [lindex [lindex $coordinates1 0] 1] != [lindex [lindex $coordinates2 0] 1] } {
        set y [lindex [lindex $coordinates 0] 1]
        set y1 [lindex [lindex $coordinates1 0] 1]
        set y2 [lindex [lindex $coordinates2 0] 1]
        set y3 [lindex [lindex $coordinates3 0] 1]
        set n1 [expr double(($y - $y2) * ($y - $y3) / (($y1 - $y2) * ($y1 - $y3)))]
        set n2 [expr double(($y - $y1) * ($y - $y3) / (($y2 - $y1) * ($y2 - $y3)))]
        set n3 [expr double(($y - $y1) * ($y - $y2) / (($y3 - $y1) * ($y3 - $y2)))]
    } else {
        set z [lindex [lindex $coordinates 0] 2]
        set z1 [lindex [lindex $coordinates1 0] 2]
        set z2 [lindex [lindex $coordinates2 0] 2]
        set z3 [lindex [lindex $coordinates3 0] 2]
        set n1 [expr double(($z - $z2) * ($z - $z3) / (($z1 - $z2) * ($z1 - $z3)))]
        set n2 [expr double(($z - $z1) * ($z - $z3) / (($z2 - $z1) * ($z2 - $z3)))]
        set n3 [expr double(($z - $z1) * ($z - $z2) / (($z3 - $z1) * ($z3 - $z2)))]
    }
    return [expr double($n1 * $load1 + $n2 * $load2 + $n3 * $load3)]
}

proc GetLoadValueFrom2NodesLineShapeFunction { node node1 load1 node2 load2 } {

    set coordinates  [GiD_Info Coordinates $node Mesh]
    set coordinates1 [GiD_Info Coordinates $node1 Geometry]
    set coordinates2 [GiD_Info Coordinates $node2 Geometry]

    if { [lindex [lindex $coordinates1 0] 0] != [lindex [lindex $coordinates2 0] 0] } {
        set x [lindex [lindex $coordinates 0] 0]
        set x1 [lindex [lindex $coordinates1 0] 0]
        set x2 [lindex [lindex $coordinates2 0] 0]
        set n1 [expr double(($x - $x2) / ($x1 - $x2))]
        set n2 [expr double(($x - $x1) / ($x2 - $x1))]
    } elseif { [lindex [lindex $coordinates1 0] 1] != [lindex [lindex $coordinates2 0] 1] } {
        set y [lindex [lindex $coordinates 0] 1]
        set y1 [lindex [lindex $coordinates1 0] 1]
        set y2 [lindex [lindex $coordinates2 0] 1]
        set n1 [expr double(($y - $y2) / ($y1 - $y2))]
        set n2 [expr double(($y - $y1) / ($y2 - $y1))]
    } else {
        set z [lindex [lindex $coordinates 0] 2]
        set z1 [lindex [lindex $coordinates1 0] 2]
        set z2 [lindex [lindex $coordinates2 0] 2]
        set n1 [expr double(($z - $z2) / ($z1 - $z2))]
        set n2 [expr double(($z - $z1) / ($z2 - $z1))]
    }
    return [expr double($n1 * $load1 + $n2 * $load2)]
}

proc UpdateEdgeLoads { elemCond1 elemCond2 } {

    if { [lindex [lindex $elemCond1 3] 9] != "All" || [lindex [lindex $elemCond2 3] 9] != "All" } {
        return false
    }

    global EdgeLoadsList

    if { [lindex $elemCond2 0] != [expr [lindex $elemCond1 0] + 1] || [lindex $elemCond2 1] != [lindex $elemCond1 1] || [lindex $elemCond2 2] != [lindex $elemCond1 2] } {
        return false
    }
    if { ![EqualEdgeLoadsConditions [lindex $elemCond1 3] [lindex $elemCond2 3]] } {
        return false
    }

    set lastEdgeLoad [lindex $EdgeLoadsList end]
    set lastEdgeLoad [lreplace $lastEdgeLoad 1 1 [lindex $elemCond2 0]]
    set EdgeLoadsList [lreplace $EdgeLoadsList end end $lastEdgeLoad]
    
    return true
}

proc EqualEdgeLoadsConditions { cond1 cond2 } {


    if { [lindex $cond1 0] != [lindex $cond2 0] } {
        return false
    }

    switch [lindex $cond1 0] {
        "Global" {
            if { [lindex $cond1 1] != [lindex $cond2 1] } {
                return false
            }
        }
        "Frame" {
            if { [lindex $cond1 2] != [lindex $cond2 2] } {
                return false
            }
        }
        "Solid/Shell" {
            if { [lindex $cond1 3] != [lindex $cond2 3] } {
                return false
            }
        }
        "Plane_Stress/Strain/Axisymmetry" {
            if { [lindex $cond1 4] != [lindex $cond2 4] } {
                return false
            }
        }
        "Specified" {
            for {set i 5} {$i < 8} {incr i} {
                if { [lindex $cond1 $i] != [lindex $cond2 $i] } {
                    return false
                }
            }
        }
    }

    for {set i 8} {$i < 10} {incr i} {
        if { [lindex $cond1 $i] != [lindex $cond2 $i] } {
            return false
        }
    }
    set numNodes [lindex $cond1 9]
    if { $numNodes == "All" } {
        if { [lindex $cond1 10] != [lindex $cond2 10] } {
            return false
        }
    } else {
        for {set i 11} {$i < [expr 11 + 2 * $numNodes]} {incr i} {
        	  if { [lindex $cond1 $i] != [lindex $cond2 $i] } {
                return false
            }
        }
    }
    
    return true
}

proc GetAllNodeEdgeLoadsConditions { node } {

    global edgeLoadsConditionsOverNodes

    set nodeConditions {}
    for {set i 0} {$i < [llength $edgeLoadsConditionsOverNodes]} {incr i} {
        if { [lindex [lindex $edgeLoadsConditionsOverNodes $i] 1] == $node } {
            lappend nodeConditions [lrange [lindex $edgeLoadsConditionsOverNodes $i] 3 end]
        }
    }
    return $nodeConditions
}

proc GetFirstNodeEdgeLoadsConditions { node } {

    global edgeLoadsConditionsOverNodes

    for {set i 0} {$i < [llength $edgeLoadsConditionsOverNodes]} {incr i} {
        if { [lindex [lindex $edgeLoadsConditionsOverNodes $i] 1] == $node } {
            return [lrange [lindex $edgeLoadsConditionsOverNodes $i] 3 end]
        }
    }
}

proc RemoveMiddleNodeEdgeLoadsConditions { middleNode } {

    global edgeLoadsConditionsOverNodes \
    	     edgeLoadsNodes

    set index 0

    while { [lindex $edgeLoadsNodes $index] != $middleNode } {
        incr index
    }

    set edgeLoadsNodes [lreplace $edgeLoadsNodes $index $index]
    set edgeLoadsConditionsOverNodes [lreplace $edgeLoadsConditionsOverNodes $index $index]
}

proc CountEdgeLoads {} {

    global EdgeLoadsList

    set count 0
    for {set i 0} {$i < [llength $EdgeLoadsList]} {incr i} {
        set count [expr $count + [lindex [lindex $EdgeLoadsList $i] 1] - [lindex [lindex $EdgeLoadsList $i] 0] + 1] 
    }
    return $count
}

proc WriteEdgeLoadsList {} {

    global EdgeLoadsList 
           # numDimension

    set returnString ""
    set index 1

    for {set i 0} {$i < [llength $EdgeLoadsList]} {incr i} {

        set edgeLoad [lindex $EdgeLoadsList $i]

        if { [lindex $edgeLoad 0] == [lindex $edgeLoad 1] } {
            append returnString [format %7d $index]
            append returnString "  [lindex $edgeLoad 0]"
            incr index
        } else {
            append returnString "  \[$index\-[expr $index + [lindex $edgeLoad 1] - [lindex $edgeLoad 0]]\]"
            append returnString "  \[[lindex $edgeLoad 0]\-[lindex $edgeLoad 1]\]"
            set index [expr $index + [lindex $edgeLoad 1] - [lindex $edgeLoad 0] + 1]
        }
        
        for {set j 2} {$j < [llength $edgeLoad]} {incr j} {
            append returnString "  [lindex $edgeLoad $j]"
        }

        append returnString " ;\n"
    }

    return $returnString
}


# proc Remove3NodeEdgeLoadsConditions { middleNode firstNode thirdNode } {

#     global edgeLoadsConditionsOverNodes \
#     	   edgeLoadsNodes

#     set index 0



#     while { [lindex $edgeLoadsNodes $index] != $middleNode } {
#         incr index
#     }

#     set middleConditions [GetFirstNodeEdgeLoadsConditions $middleNode]
#     set edgeLoadsNodes [lreplace $edgeLoadsNodes $index $index]
#     set edgeLoadsConditionsOverNodes [lreplace $edgeLoadsConditionsOverNodes $index $index]

#     set index 0
#     set firstIndex -1
#     set thirdIndex -1

#     # puts $result $edgeLoadsNodes
#     # puts $result [llength $edgeLoadsNodes]

#     while { $index < [llength $edgeLoadsNodes] && ($firstIndex < 0 || $thirdIndex < 0) } {
#         # puts $result "um:"
#         # puts $result [lindex $edgeLoadsNodes $index]
#         if { [lindex $edgeLoadsNodes $index] == $firstNode } {
#             set firstConditions [lrange [lindex $edgeLoadsConditionsOverNodes $index] 3 end]
#             if { [EqualLineSpringConditions $middleConditions $firstConditions] } {
#                 set firstIndex $index
#                 # puts $result "FIRST"
#             }
#         } elseif { [lindex $edgeLoadsNodes $index] == $thirdNode } {
#             set thirdConditions [lrange [lindex $edgeLoadsConditionsOverNodes $index] 3 end]
#             if { [EqualLineSpringConditions $middleConditions $thirdConditions] } {
#                 # set edgeLoadsNodes [lreplace $edgeLoadsNodes $index $index]
#                 # set edgeLoadsConditionsOverNodes [lreplace $edgeLoadsConditionsOverNodes $index $index]
#                 set thirdIndex $index
#                 # puts $result "THIRD"
#             }
#         }
#         incr index
#     }

#     set edgeLoadsNodes [lreplace $edgeLoadsNodes $firstIndex $firstIndex]
#     set edgeLoadsConditionsOverNodes [lreplace $edgeLoadsConditionsOverNodes $firstIndex $firstIndex]

#     if { $thirdIndex > $firstIndex } {
#         set thirdIndex [expr $thirdIndex - 1]
#     }

#     set edgeLoadsNodes [lreplace $edgeLoadsNodes $thirdIndex $thirdIndex]
#     set edgeLoadsConditionsOverNodes [lreplace $edgeLoadsConditionsOverNodes $thirdIndex $thirdIndex]
# }

