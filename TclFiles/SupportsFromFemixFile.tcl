# SupportsFromFemixFile.tcl
#

proc AssignSupports { femixData } {

	global numDimension

	set supportsData [GetBlockData $femixData "SUPPORTS"]

	if { [llength $supportsData] > 0 } {

		set index 7

		if { $numDimension == 3 } {

			while { $index < [llength $supportsData] } {

				set rangeList [GetFirstLastValuesFromRange [lindex $supportsData $index]]

				set firstPoint [lindex $rangeList 0]
		        set lastPoint [lindex $rangeList 1]

				# if { [string is integer $strRange] } {
		  #           set firstPoint $strRange
		  #           set lastPoint $strRange
		  #       } else {
		  #           set firstPoint [string range $strRange 1 [expr [string first "-" $strRange] - 1]]
		  #           set lastPoint  [string range $strRange [expr [string first "-" $strRange] + 1] [expr [string length $strRange] - 2]]
		  #       }

		        set index [expr $index + 1]
		        set numKey [lindex $supportsData $index]

		        set constraints { 0 0 0 0 0 0 }

		        for {set i 0} {$i < $numKey} {incr i} {

		        	set index [expr $index + 1]
		        	set key [lindex $supportsData $index]

		        	if { [string index $key 1] == "D" } {
		        		set constIndex [expr [string index $key 2] - 1]
		        	} else {
		        		set constIndex [expr [string index $key 2] + 2]
		        	}

		        	set constraints [lreplace $constraints $constIndex $constIndex 1]
		        }

		        GiD_Process Mescape Data Conditions AssignCond Constraints_for_Points Change \
		        			[lindex $constraints 0] \
		        			[lindex $constraints 1] \
		        			[lindex $constraints 2] \
		        			[lindex $constraints 3] \
		        			[lindex $constraints 4] \
		        			[lindex $constraints 5] \
		        			$firstPoint:$lastPoint Mescape

		        set index [expr $index + 5]
	    	}
    	} else { 
    		# numDimension == 2
    		while { $index < [llength $supportsData] } {

				set rangeList [GetFirstLastValuesFromRange [lindex $supportsData $index]]

				set firstPoint [lindex $rangeList 0]
	            set lastPoint [lindex $rangeList 1]

				# if { [string is integer $strRange] } {
		  #           set firstPoint $strRange
		  #           set lastPoint $strRange
		  #       } else {
		  #           set firstPoint [string range $strRange 1 [expr [string first "-" $strRange] - 1]]
		  #           set lastPoint  [string range $strRange [expr [string first "-" $strRange] + 1] [expr [string length $strRange] - 2]]
		  #       }

		        set index [expr $index + 1]
		        set numKey [lindex $supportsData $index]

		        set constraints { 0 0 0 0 0 0 }

		        for {set i 0} {$i < $numKey} {incr i} {

		        	set index [expr $index + 1]
		        	set key [lindex $supportsData $index]

		        	set constIndex [expr ( [string index $key 2] + 1 ) % 3]

		        	if { [string index $key 1] == "R" } {
		        		set constIndex [expr $constIndex + 3]
		        	}

		        	set constraints [lreplace $constraints $constIndex $constIndex 1]
		        }

		        GiD_Process Mescape Data Conditions AssignCond Constraints_for_Points Change \
		        			[lindex $constraints 0] \
		        			[lindex $constraints 1] \
		        			[lindex $constraints 2] \
		        			[lindex $constraints 3] \
		        			[lindex $constraints 4] \
		        			[lindex $constraints 5] \
		        			$firstPoint:$lastPoint \
		        Mescape

		        set index [expr $index + 5]
	    	}
    	}
	}
}