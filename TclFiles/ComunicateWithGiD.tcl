namespace eval DisconnectConditions {

    variable LoadTitleIdentifier
    variable GroupNameIdentifier
    variable IntegrationTypeIdentifier
    variable IntegrationNameIdentifier
    # variable AnalysisTypeIdentifier
    variable NonLinearProblemIdentifier
    # variable AssignedElementPropertiesIdentifier
    variable LayerPatternNameIdentifier
    variable SpringGroupNameIdentifier
    variable SpringMaterialTypeIdentifier
    variable SpringMaterialNameIdentifier
    variable SpringMaterialNameIdentifier_1
    variable SpringMaterialNameIdentifier_2
    variable SpringMaterialNameIdentifier_3
    variable SpringMaterialNameIdentifier_4
    variable SpringMaterialNameIdentifier_5
    variable SpringMaterialNameIdentifier_6
    variable SpringMaterialNameIdentifier_7
    variable SpringMaterialNameIdentifier_8
    # variable SpringNumNodesIdentifier
    # variable LoadValueNumNodesIdentifier
    variable NumNodesIdentifier
    variable NodeNumberIdentifier_1
    variable NodeNumberIdentifier_2
    variable NodeNumberIdentifier_3
    variable NodeNumberIdentifier_4
    variable NodeNumberIdentifier_5
    variable NodeNumberIdentifier_6
    variable NodeNumberIdentifier_7
    variable NodeNumberIdentifier_8
    variable LoadValueIdentifier
    variable LoadValueIdentifier_1
    variable LoadValueIdentifier_2
    variable LoadValueIdentifier_3
    variable LoadValueIdentifier_4
    variable LoadValueIdentifier_5
    variable LoadValueIdentifier_6
    variable LoadValueIdentifier_7
    variable LoadValueIdentifier_8
    variable FunctionSourceIdentifier
    variable FunctionNameIdentifier
    variable NumberSetsValuesIdentifier
}

proc DisconnectConditions::ComunicateWithGiD { event args } {

    variable LoadTitleIdentifier
    variable GroupNameIdentifier
    variable IntegrationTypeIdentifier
    variable IntegrationNameIdentifier
    # variable AnalysisTypeIdentifier
    variable NonLinearProblemIdentifier
    # variable AssignedElementPropertiesIdentifier
    variable LayerPatternNameIdentifier
    variable SpringGroupNameIdentifier
    variable SpringMaterialTypeIdentifier
    variable SpringMaterialNameIdentifier
    variable SpringMaterialNameIdentifier_1
    variable SpringMaterialNameIdentifier_2
    variable SpringMaterialNameIdentifier_3
    variable SpringMaterialNameIdentifier_4
    variable SpringMaterialNameIdentifier_5
    variable SpringMaterialNameIdentifier_6
    variable SpringMaterialNameIdentifier_7
    variable SpringMaterialNameIdentifier_8
    # variable SpringNumNodesIdentifier
    # variable LoadValueNumNodesIdentifier
    variable NumNodesIdentifier
    variable NodeNumberIdentifier_1
    variable NodeNumberIdentifier_2
    variable NodeNumberIdentifier_3
    variable NodeNumberIdentifier_4
    variable NodeNumberIdentifier_5
    variable NodeNumberIdentifier_6
    variable NodeNumberIdentifier_7
    variable NodeNumberIdentifier_8
    variable LoadValueIdentifier
    variable LoadValueIdentifier_1
    variable LoadValueIdentifier_2
    variable LoadValueIdentifier_3
    variable LoadValueIdentifier_4
    variable LoadValueIdentifier_5
    variable LoadValueIdentifier_6
    variable LoadValueIdentifier_7
    variable LoadValueIdentifier_8
    variable FunctionSourceIdentifier
    variable FunctionNameIdentifier
    variable NumberSetsValuesIdentifier

    # set "DisconnectConditions::IntegrationTypeIdentifier" "GAUSS-LEGENDRE"
    # set "DisconnectConditions::IntegrationNameIdentifier" "DEFAULT"
    # set "DisconnectConditions::LayerPatternNameIdentifier" "NONE"
    # set "DisconnectConditions::SpringMaterialTypeIdentifier" "LIN_SPRING"
    # set "DisconnectConditions::SpringMaterialNameIdentifier_1" "Linear_Spring"
    # set "DisconnectConditions::SpringNumNodesIdentifier" 1

    # variable springMaterialNameRow

    global PARENT \
           PROJECTPATH \
           PROBLEMTYPEPATH \
           NUMERICALINTEGRATIONLIST \
           layerPatternsList \
           allMaterialsNameList \
           comboBoxSpringMaterialType \
           comboBoxSpringMaterialName \
           rowSpringMaterialName \
           rowSpringMaterialName_1 \
           rowSpringMaterialName_2 \
           rowSpringMaterialName_3 \
           rowSpringMaterialName_4 \
           rowSpringMaterialName_5 \
           rowSpringMaterialName_6 \
           rowSpringMaterialName_7 \
           rowSpringMaterialName_8 \
           rowNodeNumber_1 \
           rowNodeNumber_2 \
           rowNodeNumber_3 \
           rowNodeNumber_4 \
           rowNodeNumber_5 \
           rowNodeNumber_6 \
           rowNodeNumber_7 \
           rowNodeNumber_8 \
           rowLoadValue \
           rowLoadValue_1 \
           rowLoadValue_2 \
           rowLoadValue_3 \
           rowLoadValue_4 \
           rowLoadValue_5 \
           rowLoadValue_6 \
           rowLoadValue_7 \
           rowLoadValue_8 \
           comboBoxSpringNumNodes \
           springMaterialsNameList \
           comboBoxLoadNumNodes \
           numNodes \
           rowFunctionName \
           rowNumberSets \
           comboBoxFunctionSource \
           functionSource \
           geometryType \
           loadType

    switch $event {

        "INIT" {

            set PARENT   [lindex $args 0]
            upvar        [lindex $args 1] ROW 
            set GDN      [lindex $args 2]
            set STRUCT   [lindex $args 3]
            set QUESTION [lindex $args 4]
            
            global $GDN
            upvar \#0 $GDN GidData

            set currentIntv [lindex [GiD_Info intvdata num] 0]
                       
            set IntegrationTypeIdentifier "GAUSS-LEGENDRE"
            set IntegrationNameIdentifier "DEFAULT"
            set LayerPatternNameIdentifier "NONE"
            set SpringMaterialTypeIdentifier "LIN_SPRING"
            set SpringMaterialNameIdentifier "Linear_Spring"
            # set SpringMaterialNameIdentifier_1 "Linear_Spring"
            # set SpringNumNodesIdentifier "All"
            set NumNodesIdentifier "All"
            # set "DisconnectConditions::IntegrationTypeIdentifier" "GAUSS-LEGENDRE"
            # set "DisconnectConditions::IntegrationNameIdentifier" "DEFAULT"

            # set NumberSetsValuesIdentifier "21"
            # set FunctionNameIdentifier "NLMM401_Material"

            set allMaterialsNameList [GiD_Info Materials]
            set numNodes "All"

            # set rowFunctionName 0

            if { $QUESTION == "Non_Linear_Problem" } {

                set NonLinearProblemIdentifier [lindex [GiD_Info gendata] 4]
                DWLocalSetValue $GDN $STRUCT Non_Linear_Problem $NonLinearProblemIdentifier
                                     
            } elseif { $QUESTION == "Assigned_Element_Properties" } {

                button $PARENT.buttonAssigned -text "View" -command "OpenElementPropertiesWindow" -width 20
                
                # set AssignedElementPropertiesIdentifier 0
                # DWLocalSetValue $GDN $STRUCT Assigned_Element_Properties $AssignedElementPropertiesIdentifier
                grid $PARENT.buttonAssigned -row $ROW -column 0 -pady 10

            } else {

                switch $QUESTION {
                    
                    "Layer_Pattern_Name" {
                        if { [file exists "$PROJECTPATH\\femix_layer_patterns.dat"] } {
                            CreateLayerPatternsList
                        }
                        set list {} 
                        for {set i 0} {$i < [llength $layerPatternsList]} {incr i} {
                            lappend list [lindex [lindex $layerPatternsList $i] 0]
                        }
                        set lab [label $PARENT.lab$ROW -text [= "Layer Pattern Name"]]
                        #set "DisconnectConditions::LayerPatternNameIdentifier" [lindex $list 0]
                        if { [llength $list] > 0 } {
                            ttk::combobox $PARENT.comb$ROW -textvariable "DisconnectConditions::LayerPatternNameIdentifier" -values $list
                            $PARENT.comb$ROW set [lindex $list 0]
                            set "DisconnectConditions::LayerPatternNameIdentifier" [lindex $list 0]
                        } else {
                            set "DisconnectConditions::LayerPatternNameIdentifier" "NONE"
                        }
                        #if { [llength $list] == 0 } {
                        #    set "DisconnectConditions::LayerPatternNameIdentifier" ""
                        #} else {
                        #    set "DisconnectConditions::LayerPatternNameIdentifier" [lindex $list 0]
                        #}
                    }
                    "Load_Title" {
                        set lab [label $PARENT.lab$ROW -text [= "Load Title"]]
                        #set LoadTitleIdentifier ""
                        if { [lindex [GiD_Info intvdata -interval $currentIntv] 2] != "" } {
                            set "DisconnectConditions::LoadTitleIdentifier" [lindex [GiD_Info intvdata -interval $currentIntv] 2]
                        } else {
                            set "DisconnectConditions::LoadTitleIdentifier" ""
                        }
                        #
                        
                        entry $PARENT.comb$ROW -textvariable "DisconnectConditions::LoadTitleIdentifier" -width 23
                        #entry $PARENT.comb$ROW -textvariable $LoadTitleIdentifier -width 23
                        #DisconnectConditions::LoadTitleIdentifier
                        #set LoadTitleIdentifier [lindex [GiD_Info intvdata -interval $currentIntv] 2]
                        #$PARENT.comb$ROW insert 0 [lindex [GiD_Info intvdata -interval $currentIntv] 2]
                    } 
                    "Spring_Material_Type" {
                        # set list { LIN_SPRING LIN_ISO }
                        set list { LIN_SPRING NLSMM101 NLSMM102 }
                        set lab [label $PARENT.lab$ROW -text [= "Material Type"]]
                        set "DisconnectConditions::SpringMaterialTypeIdentifier" [lindex $list 0]
                        set comboBoxSpringMaterialType $PARENT.comb$ROW
                        ttk::combobox $PARENT.comb$ROW -textvariable "DisconnectConditions::SpringMaterialTypeIdentifier" -values $list
                    }
                    "Point_Spring_Material_Name" {
                        # set springMaterialNameRow $ROW
                        # set allMaterialsNameList [GiD_Info Materials]
                        set springMaterialsNameList {}
                        for {set i 0} {$i < [llength $allMaterialsNameList]} {incr i} {
                           if { [lindex [GiD_Info Materials [lindex $allMaterialsNameList $i]] 2] == "LIN_SPRING" } {
                               lappend springMaterialsNameList [ReplaceSpaceWithUnderscore [lindex $allMaterialsNameList $i]]
                           }
                        }
                        set lab [label $PARENT.lab$ROW -text [= "Material Name"]]
                        set "DisconnectConditions::SpringMaterialNameIdentifier" [lindex $springMaterialsNameList 0]
                        set comboBoxSpringMaterialName $PARENT.comb$ROW
                        ttk::combobox $PARENT.comb$ROW -textvariable "DisconnectConditions::SpringMaterialNameIdentifier" -values $springMaterialsNameList

                        bind $comboBoxSpringMaterialType <<ComboboxSelected>> {
                            global allMaterialsNameList springMaterialsNameList
                            set springMaterialsNameList {}
                            # set allMaterialsNameList [GiD_Info Materials]
                            for {set i 0} {$i < [llength $allMaterialsNameList]} {incr i} {
                               if { [lindex [GiD_Info Materials [lindex $allMaterialsNameList $i]] 2] == [$comboBoxSpringMaterialType get] } {
                                   lappend springMaterialsNameList [ReplaceSpaceWithUnderscore [lindex $allMaterialsNameList $i]]
                               }
                            }
                            # set lab [label $PARENT.labSpringMaterialName -text [= "Material Name"]]
                            set "DisconnectConditions::SpringMaterialNameIdentifier" [lindex $springMaterialsNameList 0]
                            # set newComboBoxSpringMaterialName $comboBoxSpringMaterialName
                            # destroy $comboBoxSpringMaterialName
                            $comboBoxSpringMaterialName configure -values $springMaterialsNameList
                        }
                    }
                    "Group_Name" {
                        if { [llength [GiD_Groups list]] == 0 } {
                            CreateDefaultGroup
                        }
                        set list [GiD_Groups list]
                        ##[split [GiD_Info layers]]
                        #set list {}
                        #for { set i 0 } { $i < [llength [GiD_Info layers]] } { incr i } {
                        #    if { [llength [lindex [GiD_Info layers] $i]] > 1 } {
                        #            set tmpLayerName [lindex [lindex [GiD_Info layers] $i] 0]
                        #            for { set j 1 } { $j < [llength [lindex [GiD_Info layers] $i]] } { incr j } {
                        #                append tmpLayerName "_"
                        #                append tmpLayerName [lindex [lindex [GiD_Info layers] $i] $j]
                        #            }
                        #            lappend list $tmpLayerName
                        #        } else {
                        #            lappend list [lindex [GiD_Info layers] $i]
                        #        }
                        #}
                        set lab [label $PARENT.lab$ROW -text [= "Group Name"]]
                        if { [lindex [GiD_Info intvdata -interval $currentIntv] 4] == "" } {
                            set "DisconnectConditions::GroupNameIdentifier" [lindex $list 0]
                        } else {
                            set "DisconnectConditions::GroupNameIdentifier" [lindex [GiD_Info intvdata -interval $currentIntv] 4]
                        }
                        #set result 
                        #set comb [ComboBox $PARENT.comb$ROW \
                        #-textvariable DisconnectConditions::LayerNameIdentifier \
                        #-values $list]
                        ttk::combobox $PARENT.comb$ROW -textvariable "DisconnectConditions::GroupNameIdentifier" -values $list                  
                        #if { [lindex [GiD_Info intvdata] 4] != ""} {
                        #    $PARENT.comb$ROW set [lindex [GiD_Info intvdata] 4]
                        #}
                        #$PARENT.comb$ROW set [lindex $list 0]
                        # set "DisconnectConditions::SpringMaterialTypeIdentifier" "LIN_SPRING"
                        # set "DisconnectConditions::SpringMaterialNameIdentifier_1" "Linear_Spring"
                        # set "DisconnectConditions::SpringNumNodesIdentifier" 1
                    }
                    "Integration_Type" {
                        set list {NONE GAUSS-LEGENDRE GAUSS-LOBATTO NEWTON-COTES}
                        set lab [label $PARENT.lab$ROW -text [= "Integration Type"]]
                        #set "DisconnectConditions::IntegrationTypeIdentifier" "GAUSS-LEGENDRE"
                        #set comb [ComboBox $PARENT.comb$ROW \
                        #-textvariable DisconnectConditions::IntegrationTypeIdentifier \
                        #-values $list]
                        ttk::combobox $PARENT.comb$ROW -textvariable "DisconnectConditions::IntegrationTypeIdentifier" -values $list
                        #if { [lindex [GiD_Info intvdata] 6] != ""} {
                        #    $PARENT.comb$ROW set [lindex [GiD_Info intvdata] 6]
                        #}
                        #$PARENT.comb$ROW set [lindex $list 1]
                    }
                    "Integration_Name" {
                        set list { DEFAULT }
                        for {set i 1} {$i < [llength $NUMERICALINTEGRATIONLIST]} {incr i 2} {
                            lappend list [lindex $NUMERICALINTEGRATIONLIST $i]
                        }
                        set lab [label $PARENT.lab$ROW -text [= "Integration Name"]]
                        ttk::combobox $PARENT.comb$ROW -textvariable "DisconnectConditions::IntegrationNameIdentifier" -values $list
                    }
                    "Edge_Loads_Number_of_Nodes" {
                        set geometryType "LINE"
                        set loadType "LOAD"
                        set list {All 2 3}
                        set lab [label $PARENT.lab$ROW -text [= "Number of Nodes"]]
                        set NumNodesIdentifier [lindex $list 0]
                        set comboBoxLoadNumNodes $PARENT.comb$ROW
                        ttk::combobox $PARENT.comb$ROW -textvariable "DisconnectConditions::NumNodesIdentifier" -values $list -width 7
                        $PARENT.comb$ROW set [lindex $list 0]
                    }
                    "Line_Spring_Number_of_Nodes" {
                        set geometryType "LINE"
                        set list {All 2 3}
                        set lab [label $PARENT.lab$ROW -text [= "Number of Nodes"]]
                        set NumNodesIdentifier [lindex $list 0]
                        set comboBoxSpringNumNodes $PARENT.comb$ROW
                        ttk::combobox $PARENT.comb$ROW -textvariable "DisconnectConditions::NumNodesIdentifier" -values $list
                        $PARENT.comb$ROW set [lindex $list 0]
                        # $PARENT.comb$ROW -state disabled
                    }
                    "Face_Loads_Number_of_Nodes" {
                        set geometryType "SURFACE"
                        set loadType "LOAD"
                        set list {All 4 8}
                        set lab [label $PARENT.lab$ROW -text [= "Number of Nodes"]]
                        set NumNodesIdentifier [lindex $list 0]
                        set comboBoxLoadNumNodes $PARENT.comb$ROW
                        ttk::combobox $PARENT.comb$ROW -textvariable "DisconnectConditions::NumNodesIdentifier" -values $list -width 7
                        $PARENT.comb$ROW set [lindex $list 0]
                    }
                    "Surface_Spring_Number_of_Nodes" {
                        set geometryType "SURFACE"
                        set list {All 4 8}
                        set lab [label $PARENT.lab$ROW -text [= "Number of Nodes"]]
                        set NumNodesIdentifier [lindex $list 0]
                        set comboBoxSpringNumNodes $PARENT.comb$ROW
                        ttk::combobox $PARENT.comb$ROW -textvariable "DisconnectConditions::NumNodesIdentifier" -values $list
                        $PARENT.comb$ROW set [lindex $list 0]
                    }
                    "Line_Spring_Material_Name" {
                        set springMaterialsNameList {}
                        for {set i 0} {$i < [llength $allMaterialsNameList]} {incr i} {
                           if { [lindex [GiD_Info Materials [lindex $allMaterialsNameList $i]] 2] == "LIN_SPRING" } {
                               lappend springMaterialsNameList [ReplaceSpaceWithUnderscore [lindex $allMaterialsNameList $i]]
                           }
                        }
                        set lab [label $PARENT.lab$ROW -text [= "Material Name"]]
                        set "DisconnectConditions::SpringMaterialNameIdentifier" [lindex $springMaterialsNameList 0]
                        set rowSpringMaterialName $ROW
                        ttk::combobox $PARENT.comb$ROW -textvariable "DisconnectConditions::SpringMaterialNameIdentifier" -values $springMaterialsNameList
                    }
                    "Surface_Spring_Material_Name" {
                        set springMaterialsNameList {}
                        for {set i 0} {$i < [llength $allMaterialsNameList]} {incr i} {
                           if { [lindex [GiD_Info Materials [lindex $allMaterialsNameList $i]] 2] == "LIN_SPRING" } {
                               lappend springMaterialsNameList [ReplaceSpaceWithUnderscore [lindex $allMaterialsNameList $i]]
                           }
                        }
                        set lab [label $PARENT.lab$ROW -text [= "Material Name"]]
                        set "DisconnectConditions::SpringMaterialNameIdentifier" [lindex $springMaterialsNameList 0]
                        set rowSpringMaterialName $ROW
                        ttk::combobox $PARENT.comb$ROW -textvariable "DisconnectConditions::SpringMaterialNameIdentifier" -values $springMaterialsNameList
                    }
                    "Surface_Heat_Flux_Number_of_Nodes" {
                        set geometryType "SURFACE"
                        set loadType "HEATFLUX"
                        set list {All 4 8}
                        set lab [label $PARENT.lab$ROW -text [= "Number of Nodes"]]
                        set NumNodesIdentifier [lindex $list 0]
                        set comboBoxLoadNumNodes $PARENT.comb$ROW
                        ttk::combobox $PARENT.comb$ROW -textvariable "DisconnectConditions::NumNodesIdentifier" -values $list -width 11
                        $PARENT.comb$ROW set [lindex $list 0]
                    }
                    "Load_Value" {
                        if { $loadType == "LOAD" } {
                            set lab [label $PARENT.lab$ROW -text [= "Load Value"]]    
                        } else {
                            set lab [label $PARENT.lab$ROW -text [= "Heat Flux"]]
                        }
                        set rowLoadValue $ROW
                        set "DisconnectConditions::LoadValueIdentifier" ""
                        entry $PARENT.comb$ROW -textvariable "DisconnectConditions::LoadValueIdentifier" -width 10
                    }
                    "Node_Number_1" {
                        set rowNodeNumber_1 $ROW
                    }
                    "Spring_Material_Name_1" {
                        set rowSpringMaterialName_1 $ROW
                    }
                    "Load_Value_1" {
                        set rowLoadValue_1 $ROW
                    }
                    "Node_Number_2" {
                        set rowNodeNumber_2 $ROW
                    }
                    "Spring_Material_Name_2" {
                        # set springMaterialsNameList {}
                        # for {set i 0} {$i < [llength $allMaterialsNameList]} {incr i} {
                        #    if { [lindex [GiD_Info Materials [lindex $allMaterialsNameList $i]] 2] == "LIN_SPRING" } {
                        #        lappend springMaterialsNameList [ReplaceSpaceWithUnderscore [lindex $allMaterialsNameList $i]]
                        #    }
                        # }
                        # set lab [label $PARENT.lab$ROW -text [= "Material Name \[2\]"]]
                        # set "DisconnectConditions::SpringMaterialNameIdentifier_2" [lindex $springMaterialsNameList 0]
                        set rowSpringMaterialName_2 $ROW
                        # ttk::combobox $PARENT.comb$ROW -textvariable "DisconnectConditions::SpringMaterialNameIdentifier_2" -values $springMaterialsNameList
                    }
                    "Load_Value_2" {
                        set rowLoadValue_2 $ROW
                    }
                    "Node_Number_3" {
                        set rowNodeNumber_3 $ROW
                    }
                    "Spring_Material_Name_3" {
                        # set springMaterialsNameList {}
                        # for {set i 0} {$i < [llength $allMaterialsNameList]} {incr i} {
                        #    if { [lindex [GiD_Info Materials [lindex $allMaterialsNameList $i]] 2] == "LIN_SPRING" } {
                        #        lappend springMaterialsNameList [ReplaceSpaceWithUnderscore [lindex $allMaterialsNameList $i]]
                        #    }
                        # }
                        # set lab [label $PARENT.lab$ROW -text [= "Material Name \[3\]"]]
                        # set "DisconnectConditions::SpringMaterialNameIdentifier_3" [lindex $springMaterialsNameList 0]
                        set rowSpringMaterialName_3 $ROW
                        # ttk::combobox $PARENT.comb$ROW -textvariable "DisconnectConditions::SpringMaterialNameIdentifier_3" -values $springMaterialsNameList

                        if { $geometryType == "LINE" } {

                            bind $comboBoxSpringMaterialType <<ComboboxSelected>> {

                                global allMaterialsNameList springMaterialsNameList 
                                # springNumNodes
                                set springMaterialsNameList {}
                                for {set i 0} {$i < [llength $allMaterialsNameList]} {incr i} {
                                   if { [lindex [GiD_Info Materials [lindex $allMaterialsNameList $i]] 2] == [$comboBoxSpringMaterialType get] } {
                                       lappend springMaterialsNameList [ReplaceSpaceWithUnderscore [lindex $allMaterialsNameList $i]]
                                   }
                                }
                                if { [$comboBoxSpringNumNodes get] == "All" } {
                                    set "DisconnectConditions::SpringMaterialNameIdentifier" [lindex $springMaterialsNameList 0]
                                    $PARENT.comb$rowSpringMaterialName configure -values $springMaterialsNameList
                                    $PARENT.comb$rowSpringMaterialName set [lindex $springMaterialsNameList 0]
                                } else {
                                    if { [$comboBoxSpringNumNodes get] > 1 } {
                                        set "DisconnectConditions::SpringMaterialNameIdentifier_1" [lindex $springMaterialsNameList 0]
                                        $PARENT.comb$rowSpringMaterialName_1 configure -values $springMaterialsNameList
                                        set "DisconnectConditions::SpringMaterialNameIdentifier_2" [lindex $springMaterialsNameList 0]
                                        $PARENT.comb$rowSpringMaterialName_2 configure -values $springMaterialsNameList
                                    }
                                    if { [$comboBoxSpringNumNodes get] > 2 } {
                                        set "DisconnectConditions::SpringMaterialNameIdentifier_3" [lindex $springMaterialsNameList 0]
                                        $PARENT.comb$rowSpringMaterialName_3 configure -values $springMaterialsNameList
                                    }
                                }
                            }

                            bind $comboBoxSpringNumNodes <<ComboboxSelected>> {
                                
                                global numNodes
                                set numNodes [$comboBoxSpringNumNodes get]

                                destroy $PARENT.lab$rowSpringMaterialName
                                destroy $PARENT.comb$rowSpringMaterialName
                                destroy $PARENT.lab$rowNodeNumber_1
                                destroy $PARENT.comb$rowNodeNumber_1
                                destroy $PARENT.lab$rowSpringMaterialName_1
                                destroy $PARENT.comb$rowSpringMaterialName_1
                                destroy $PARENT.lab$rowNodeNumber_2
                                destroy $PARENT.comb$rowNodeNumber_2
                                destroy $PARENT.lab$rowSpringMaterialName_2
                                destroy $PARENT.comb$rowSpringMaterialName_2
                                destroy $PARENT.lab$rowNodeNumber_3
                                destroy $PARENT.comb$rowNodeNumber_3
                                destroy $PARENT.lab$rowSpringMaterialName_3
                                destroy $PARENT.comb$rowSpringMaterialName_3

                                switch $numNodes {

                                    "All" {
                                        set lab [label $PARENT.lab$rowSpringMaterialName -text [= "Material Name"]]
                                        set "DisconnectConditions::SpringMaterialNameIdentifier" [lindex $springMaterialsNameList 0]
                                        ttk::combobox $PARENT.comb$rowSpringMaterialName -textvariable "DisconnectConditions::SpringMaterialNameIdentifier" -values $springMaterialsNameList
                                        grid $lab -row $rowSpringMaterialName -column 0 -sticky e
                                        grid $PARENT.comb$rowSpringMaterialName -row $rowSpringMaterialName -column 1 -sticky w -pady 3
                                    }
                                    "2" {
                                        set lab [label $PARENT.lab$rowNodeNumber_1 -text [= "Node Number \[1\]"]]
                                        set "DisconnectConditions::NodeNumberIdentifier_1" 0
                                        entry $PARENT.comb$rowNodeNumber_1 -textvariable "DisconnectConditions::NodeNumberIdentifier_1" -width 8
                                        grid $lab -row $rowNodeNumber_1 -column 2 -sticky e
                                        grid $PARENT.comb$rowNodeNumber_1 -row $rowNodeNumber_1 -column 3 -sticky w -pady 3

                                        set lab [label $PARENT.lab$rowSpringMaterialName_1 -text [= "Material Name \[1\]"]]
                                        set "DisconnectConditions::SpringMaterialNameIdentifier_1" [lindex $springMaterialsNameList 0]
                                        ttk::combobox $PARENT.comb$rowSpringMaterialName_1 -textvariable "DisconnectConditions::SpringMaterialNameIdentifier_1" -values $springMaterialsNameList
                                        # grid $lab -row $rowSpringMaterialName_1 -column 0 -sticky e
                                        # grid $PARENT.comb$rowSpringMaterialName_1 -row $rowSpringMaterialName_1 -column 1 -sticky w -pady 3
                                        grid $lab -row $rowNodeNumber_1 -column 0 -sticky e
                                        grid $PARENT.comb$rowSpringMaterialName_1 -row $rowNodeNumber_1 -column 1 -sticky w -pady 3

                                        set lab [label $PARENT.lab$rowNodeNumber_2 -text [= "Node Number \[2\]"]]
                                        set "DisconnectConditions::NodeNumberIdentifier_2" 0
                                        entry $PARENT.comb$rowNodeNumber_2 -textvariable "DisconnectConditions::NodeNumberIdentifier_2" -width 8
                                        grid $lab -row $rowNodeNumber_2 -column 2 -sticky e
                                        grid $PARENT.comb$rowNodeNumber_2 -row $rowNodeNumber_2 -column 3 -sticky w -pady 3

                                        set lab [label $PARENT.lab$rowSpringMaterialName_2 -text [= "Material Name \[2\]"]]
                                        set "DisconnectConditions::SpringMaterialNameIdentifier_2" [lindex $springMaterialsNameList 0]
                                        ttk::combobox $PARENT.comb$rowSpringMaterialName_2 -textvariable "DisconnectConditions::SpringMaterialNameIdentifier_2" -values $springMaterialsNameList
                                        # grid $lab -row $rowSpringMaterialName_2 -column 0 -sticky e
                                        # grid $PARENT.comb$rowSpringMaterialName_2 -row $rowSpringMaterialName_2 -column 1 -sticky w -pady 3
                                        grid $lab -row $rowNodeNumber_2 -column 0 -sticky e
                                        grid $PARENT.comb$rowSpringMaterialName_2 -row $rowNodeNumber_2 -column 1 -sticky w -pady 3
                                    }
                                    "3" {
                                        set lab [label $PARENT.lab$rowNodeNumber_1 -text [= "Node Number \[1\]"]]
                                        set "DisconnectConditions::NodeNumberIdentifier_1" 0
                                        entry $PARENT.comb$rowNodeNumber_1 -textvariable "DisconnectConditions::NodeNumberIdentifier_1" -width 8
                                        grid $lab -row $rowNodeNumber_1 -column 2 -sticky e
                                        grid $PARENT.comb$rowNodeNumber_1 -row $rowNodeNumber_1 -column 3 -sticky w -pady 3

                                        set lab [label $PARENT.lab$rowSpringMaterialName_1 -text [= "Material Name \[1\]"]]
                                        set "DisconnectConditions::SpringMaterialNameIdentifier_1" [lindex $springMaterialsNameList 0]
                                        ttk::combobox $PARENT.comb$rowSpringMaterialName_1 -textvariable "DisconnectConditions::SpringMaterialNameIdentifier_1" -values $springMaterialsNameList
                                        # grid $lab -row $rowSpringMaterialName_1 -column 0 -sticky e
                                        # grid $PARENT.comb$rowSpringMaterialName_1 -row $rowSpringMaterialName_1 -column 1 -sticky w -pady 3
                                        grid $lab -row $rowNodeNumber_1 -column 0 -sticky e
                                        grid $PARENT.comb$rowSpringMaterialName_1 -row $rowNodeNumber_1 -column 1 -sticky w -pady 3

                                        set lab [label $PARENT.lab$rowNodeNumber_2 -text [= "Node Number \[2\]"]]
                                        set "DisconnectConditions::NodeNumberIdentifier_2" 0
                                        entry $PARENT.comb$rowNodeNumber_2 -textvariable "DisconnectConditions::NodeNumberIdentifier_2" -width 8
                                        grid $lab -row $rowNodeNumber_2 -column 2 -sticky e
                                        grid $PARENT.comb$rowNodeNumber_2 -row $rowNodeNumber_2 -column 3 -sticky w -pady 3

                                        set lab [label $PARENT.lab$rowSpringMaterialName_2 -text [= "Material Name \[2\]"]]
                                        set "DisconnectConditions::SpringMaterialNameIdentifier_2" [lindex $springMaterialsNameList 0]
                                        ttk::combobox $PARENT.comb$rowSpringMaterialName_2 -textvariable "DisconnectConditions::SpringMaterialNameIdentifier_2" -values $springMaterialsNameList
                                        # grid $lab -row $rowSpringMaterialName_2 -column 0 -sticky e
                                        # grid $PARENT.comb$rowSpringMaterialName_2 -row $rowSpringMaterialName_2 -column 1 -sticky w -pady 3
                                        grid $lab -row $rowNodeNumber_2 -column 0 -sticky e
                                        grid $PARENT.comb$rowSpringMaterialName_2 -row $rowNodeNumber_2 -column 1 -sticky w -pady 3

                                        set lab [label $PARENT.lab$rowNodeNumber_3 -text [= "Node Number \[3\]"]]
                                        set "DisconnectConditions::NodeNumberIdentifier_3" 0
                                        entry $PARENT.comb$rowNodeNumber_3 -textvariable "DisconnectConditions::NodeNumberIdentifier_3" -width 8
                                        grid $lab -row $rowNodeNumber_3 -column 2 -sticky e
                                        grid $PARENT.comb$rowNodeNumber_3 -row $rowNodeNumber_3 -column 3 -sticky w -pady 3

                                        set lab [label $PARENT.lab$rowSpringMaterialName_3 -text [= "Material Name \[3\]"]]
                                        set "DisconnectConditions::SpringMaterialNameIdentifier_3" [lindex $springMaterialsNameList 0]
                                        ttk::combobox $PARENT.comb$rowSpringMaterialName_3 -textvariable "DisconnectConditions::SpringMaterialNameIdentifier_3" -values $springMaterialsNameList
                                        # grid $lab -row $rowSpringMaterialName_3 -column 0 -sticky e
                                        # grid $PARENT.comb$rowSpringMaterialName_3 -row $rowSpringMaterialName_3 -column 1 -sticky w -pady 3
                                        grid $lab -row $rowNodeNumber_3 -column 0 -sticky e
                                        grid $PARENT.comb$rowSpringMaterialName_3 -row $rowNodeNumber_3 -column 1 -sticky w -pady 3
                                    }
                                }
                            }
                        }
                    }
                    "Load_Value_3" {

                        set rowLoadValue_3 $ROW

                        if { $geometryType == "LINE" && $loadType == "LOAD" } {

                            bind $comboBoxLoadNumNodes <<ComboboxSelected>> {

                                global numNodes
                                set numNodes [$comboBoxLoadNumNodes get]

                                destroy $PARENT.lab$rowNodeNumber_1
                                destroy $PARENT.lab$rowNodeNumber_2
                                destroy $PARENT.lab$rowNodeNumber_3
                                # destroy $PARENT.lab$rowNodeNumber_4
                                # destroy $PARENT.lab$rowNodeNumber_5
                                # destroy $PARENT.lab$rowNodeNumber_6
                                # destroy $PARENT.lab$rowNodeNumber_7
                                # destroy $PARENT.lab$rowNodeNumber_8

                                destroy $PARENT.comb$rowNodeNumber_1
                                destroy $PARENT.comb$rowNodeNumber_2
                                destroy $PARENT.comb$rowNodeNumber_3
                                # destroy $PARENT.comb$rowNodeNumber_4
                                # destroy $PARENT.comb$rowNodeNumber_5
                                # destroy $PARENT.comb$rowNodeNumber_6
                                # destroy $PARENT.comb$rowNodeNumber_7
                                # destroy $PARENT.comb$rowNodeNumber_8

                                destroy $PARENT.lab$rowLoadValue
                                destroy $PARENT.lab$rowLoadValue_1
                                destroy $PARENT.lab$rowLoadValue_2
                                destroy $PARENT.lab$rowLoadValue_3
                                # destroy $PARENT.lab$rowLoadValue_4
                                # destroy $PARENT.lab$rowLoadValue_5
                                # destroy $PARENT.lab$rowLoadValue_6
                                # destroy $PARENT.lab$rowLoadValue_7
                                # destroy $PARENT.lab$rowLoadValue_8

                                destroy $PARENT.comb$rowLoadValue
                                destroy $PARENT.comb$rowLoadValue_1
                                destroy $PARENT.comb$rowLoadValue_2
                                destroy $PARENT.comb$rowLoadValue_3
                                # destroy $PARENT.comb$rowLoadValue_4
                                # destroy $PARENT.comb$rowLoadValue_5
                                # destroy $PARENT.comb$rowLoadValue_6
                                # destroy $PARENT.comb$rowLoadValue_7
                                # destroy $PARENT.comb$rowLoadValue_8

                                if { $numNodes == "All" } {

                                    set lab [label $PARENT.lab$rowLoadValue -text [= "Load Value"]]
                                    set "DisconnectConditions::LoadValueIdentifier" ""
                                    entry $PARENT.comb$rowLoadValue -textvariable "DisconnectConditions::LoadValueIdentifier" -width 10
                                    grid $lab -row $rowLoadValue -column 0 -sticky e
                                    grid $PARENT.comb$rowLoadValue -row $rowLoadValue -column 1 -sticky w -pady 3

                                } else {

                                    set lab [label $PARENT.lab$rowNodeNumber_1 -text [= "Node Number \[1\]"]]
                                    set "DisconnectConditions::NodeNumberIdentifier_1" ""
                                    entry $PARENT.comb$rowNodeNumber_1 -textvariable "DisconnectConditions::NodeNumberIdentifier_1" -width 10
                                    grid $lab -row $rowNodeNumber_1 -column 0 -sticky e
                                    grid $PARENT.comb$rowNodeNumber_1 -row $rowNodeNumber_1 -column 1 -sticky w -pady 3

                                    set lab [label $PARENT.lab$rowLoadValue_1 -text [= "Load Value \[1\]"]]
                                    set "DisconnectConditions::LoadValueIdentifier_1" ""
                                    entry $PARENT.comb$rowLoadValue_1 -textvariable "DisconnectConditions::LoadValueIdentifier_1" -width 10
                                    grid $lab -row $rowNodeNumber_1 -column 1
                                     # -sticky e
                                    grid $PARENT.comb$rowLoadValue_1 -row $rowNodeNumber_1 -column 1 -sticky e -padx 8

                                    set lab [label $PARENT.lab$rowNodeNumber_2 -text [= "Node Number \[2\]"]]
                                    set "DisconnectConditions::NodeNumberIdentifier_2" ""
                                    entry $PARENT.comb$rowNodeNumber_2 -textvariable "DisconnectConditions::NodeNumberIdentifier_2" -width 10
                                    grid $lab -row $rowNodeNumber_2 -column 0 -sticky e
                                    grid $PARENT.comb$rowNodeNumber_2 -row $rowNodeNumber_2 -column 1 -sticky w -pady 3

                                    set lab [label $PARENT.lab$rowLoadValue_2 -text [= "Load Value \[2\]"]]
                                    set "DisconnectConditions::LoadValueIdentifier_2" ""
                                    entry $PARENT.comb$rowLoadValue_2 -textvariable "DisconnectConditions::LoadValueIdentifier_2" -width 10
                                    grid $lab -row $rowNodeNumber_2 -column 1
                                     # -sticky e
                                    grid $PARENT.comb$rowLoadValue_2 -row $rowNodeNumber_2 -column 1 -sticky e -padx 8

                                    if { $numNodes == 3 } {

                                        set lab [label $PARENT.lab$rowNodeNumber_3 -text [= "Node Number \[3\]"]]
                                        set "DisconnectConditions::NodeNumberIdentifier_3" ""
                                        entry $PARENT.comb$rowNodeNumber_3 -textvariable "DisconnectConditions::NodeNumberIdentifier_3" -width 10
                                        grid $lab -row $rowNodeNumber_3 -column 0 -sticky e
                                        grid $PARENT.comb$rowNodeNumber_3 -row $rowNodeNumber_3 -column 1 -sticky w -pady 3

                                        set lab [label $PARENT.lab$rowLoadValue_3 -text [= "Load Value \[3\]"]]
                                        set "DisconnectConditions::LoadValueIdentifier_3" ""
                                        entry $PARENT.comb$rowLoadValue_3 -textvariable "DisconnectConditions::LoadValueIdentifier_3" -width 10
                                        grid $lab -row $rowNodeNumber_3 -column 1
                                         # -sticky e
                                        grid $PARENT.comb$rowLoadValue_3 -row $rowNodeNumber_3 -column 1 -sticky e -padx 8

                                    }
                                }
                            }
                        }
                    }
                    "Node_Number_4" {
                        set rowNodeNumber_4 $ROW
                    }
                    "Spring_Material_Name_4" {
                        set rowSpringMaterialName_4 $ROW
                    }
                    "Load_Value_4" {
                        set rowLoadValue_4 $ROW
                    }
                    "Node_Number_5" {
                        set rowNodeNumber_5 $ROW
                    }
                    "Spring_Material_Name_5" {
                        set rowSpringMaterialName_5 $ROW
                    }
                    "Load_Value_5" {
                        set rowLoadValue_5 $ROW
                    }
                    "Node_Number_6" {
                        set rowNodeNumber_6 $ROW
                    }
                    "Spring_Material_Name_6" {
                        set rowSpringMaterialName_6 $ROW
                    }
                    "Load_Value_6" {
                        set rowLoadValue_6 $ROW
                    }
                    "Node_Number_7" {
                        set rowNodeNumber_7 $ROW
                    }
                    "Spring_Material_Name_7" {
                        set rowSpringMaterialName_7 $ROW
                    }
                    "Load_Value_7" {
                        set rowLoadValue_7 $ROW
                    }
                    "Node_Number_8" {
                        set rowNodeNumber_8 $ROW
                    }
                    "Spring_Material_Name_8" {
                        set rowSpringMaterialName_8 $ROW

                        bind $comboBoxSpringMaterialType <<ComboboxSelected>> {
                            global allMaterialsNameList springMaterialsNameList 
                            # springNumNodes
                            set springMaterialsNameList {}
                            for {set i 0} {$i < [llength $allMaterialsNameList]} {incr i} {
                                if { [lindex [GiD_Info Materials [lindex $allMaterialsNameList $i]] 2] == [$comboBoxSpringMaterialType get] } {
                                    lappend springMaterialsNameList [ReplaceSpaceWithUnderscore [lindex $allMaterialsNameList $i]]
                                }
                            }
                            if { [$comboBoxSpringNumNodes get] == "All" } {
                                set "DisconnectConditions::SpringMaterialNameIdentifier" [lindex $springMaterialsNameList 0]
                                $PARENT.comb$rowSpringMaterialName configure -values $springMaterialsNameList
                                $PARENT.comb$rowSpringMaterialName set [lindex $springMaterialsNameList 0]
                            } else {
                                if { [$comboBoxSpringNumNodes get] > 3 } {
                                    set "DisconnectConditions::SpringMaterialNameIdentifier_1" [lindex $springMaterialsNameList 0]
                                    $PARENT.comb$rowSpringMaterialName_1 configure -values $springMaterialsNameList
                                    set "DisconnectConditions::SpringMaterialNameIdentifier_2" [lindex $springMaterialsNameList 0]
                                    $PARENT.comb$rowSpringMaterialName_2 configure -values $springMaterialsNameList
                                    set "DisconnectConditions::SpringMaterialNameIdentifier_3" [lindex $springMaterialsNameList 0]
                                    $PARENT.comb$rowSpringMaterialName_3 configure -values $springMaterialsNameList
                                    set "DisconnectConditions::SpringMaterialNameIdentifier_4" [lindex $springMaterialsNameList 0]
                                    $PARENT.comb$rowSpringMaterialName_4 configure -values $springMaterialsNameList
                                }
                                if { [$comboBoxSpringNumNodes get] > 7 } {
                                    set "DisconnectConditions::SpringMaterialNameIdentifier_5" [lindex $springMaterialsNameList 0]
                                    $PARENT.comb$rowSpringMaterialName_5 configure -values $springMaterialsNameList
                                    set "DisconnectConditions::SpringMaterialNameIdentifier_6" [lindex $springMaterialsNameList 0]
                                    $PARENT.comb$rowSpringMaterialName_6 configure -values $springMaterialsNameList
                                    set "DisconnectConditions::SpringMaterialNameIdentifier_7" [lindex $springMaterialsNameList 0]
                                    $PARENT.comb$rowSpringMaterialName_7 configure -values $springMaterialsNameList
                                    set "DisconnectConditions::SpringMaterialNameIdentifier_8" [lindex $springMaterialsNameList 0]
                                    $PARENT.comb$rowSpringMaterialName_8 configure -values $springMaterialsNameList
                                }
                            }
                        }

                        bind $comboBoxSpringNumNodes <<ComboboxSelected>> {

                            global numNodes
                            set numNodes [$comboBoxSpringNumNodes get]

                            destroy $PARENT.lab$rowSpringMaterialName
                            destroy $PARENT.comb$rowSpringMaterialName
                            destroy $PARENT.lab$rowNodeNumber_1
                            destroy $PARENT.comb$rowNodeNumber_1
                            destroy $PARENT.lab$rowSpringMaterialName_1
                            destroy $PARENT.comb$rowSpringMaterialName_1
                            destroy $PARENT.lab$rowNodeNumber_2
                            destroy $PARENT.comb$rowNodeNumber_2
                            destroy $PARENT.lab$rowSpringMaterialName_2
                            destroy $PARENT.comb$rowSpringMaterialName_2
                            destroy $PARENT.lab$rowNodeNumber_3
                            destroy $PARENT.comb$rowNodeNumber_3
                            destroy $PARENT.lab$rowSpringMaterialName_3
                            destroy $PARENT.comb$rowSpringMaterialName_3
                            destroy $PARENT.lab$rowNodeNumber_4
                            destroy $PARENT.comb$rowNodeNumber_4
                            destroy $PARENT.lab$rowSpringMaterialName_4
                            destroy $PARENT.comb$rowSpringMaterialName_4
                            destroy $PARENT.lab$rowNodeNumber_5
                            destroy $PARENT.comb$rowNodeNumber_5
                            destroy $PARENT.lab$rowSpringMaterialName_5
                            destroy $PARENT.comb$rowSpringMaterialName_5
                            destroy $PARENT.lab$rowNodeNumber_6
                            destroy $PARENT.comb$rowNodeNumber_6
                            destroy $PARENT.lab$rowSpringMaterialName_6
                            destroy $PARENT.comb$rowSpringMaterialName_6
                            destroy $PARENT.lab$rowNodeNumber_7
                            destroy $PARENT.comb$rowNodeNumber_7
                            destroy $PARENT.lab$rowSpringMaterialName_7
                            destroy $PARENT.comb$rowSpringMaterialName_7
                            destroy $PARENT.lab$rowNodeNumber_8
                            destroy $PARENT.comb$rowNodeNumber_8
                            destroy $PARENT.lab$rowSpringMaterialName_8
                            destroy $PARENT.comb$rowSpringMaterialName_8

                            if { $numNodes == "All" } {
                                set lab [label $PARENT.lab$rowSpringMaterialName -text [= "Material Name"]]
                                set "DisconnectConditions::SpringMaterialNameIdentifier" [lindex $springMaterialsNameList 0]
                                ttk::combobox $PARENT.comb$rowSpringMaterialName -textvariable "DisconnectConditions::SpringMaterialNameIdentifier" -values $springMaterialsNameList
                                grid $lab -row $rowSpringMaterialName -column 0 -sticky e
                                grid $PARENT.comb$rowSpringMaterialName -row $rowSpringMaterialName -column 1 -sticky w -pady 3
                            } else {
                                set lab [label $PARENT.lab$rowNodeNumber_1 -text [= "Node Number \[1\]"]]
                                set "DisconnectConditions::NodeNumberIdentifier_1" 0
                                entry $PARENT.comb$rowNodeNumber_1 -textvariable "DisconnectConditions::NodeNumberIdentifier_1" -width 8
                                grid $lab -row $rowNodeNumber_1 -column 2 -sticky e
                                grid $PARENT.comb$rowNodeNumber_1 -row $rowNodeNumber_1 -column 3 -sticky w -pady 3

                                set lab [label $PARENT.lab$rowSpringMaterialName_1 -text [= "Material Name \[1\]"]]
                                set "DisconnectConditions::SpringMaterialNameIdentifier_1" [lindex $springMaterialsNameList 0]
                                ttk::combobox $PARENT.comb$rowSpringMaterialName_1 -textvariable "DisconnectConditions::SpringMaterialNameIdentifier_1" -values $springMaterialsNameList
                                grid $lab -row $rowNodeNumber_1 -column 0 -sticky e
                                grid $PARENT.comb$rowSpringMaterialName_1 -row $rowNodeNumber_1 -column 1 -sticky w -pady 3

                                set lab [label $PARENT.lab$rowNodeNumber_2 -text [= "Node Number \[2\]"]]
                                set "DisconnectConditions::NodeNumberIdentifier_2" 0
                                entry $PARENT.comb$rowNodeNumber_2 -textvariable "DisconnectConditions::NodeNumberIdentifier_2" -width 8
                                grid $lab -row $rowNodeNumber_2 -column 2 -sticky e
                                grid $PARENT.comb$rowNodeNumber_2 -row $rowNodeNumber_2 -column 3 -sticky w -pady 3

                                set lab [label $PARENT.lab$rowSpringMaterialName_2 -text [= "Material Name \[2\]"]]
                                set "DisconnectConditions::SpringMaterialNameIdentifier_2" [lindex $springMaterialsNameList 0]
                                ttk::combobox $PARENT.comb$rowSpringMaterialName_2 -textvariable "DisconnectConditions::SpringMaterialNameIdentifier_2" -values $springMaterialsNameList
                                grid $lab -row $rowNodeNumber_2 -column 0 -sticky e
                                grid $PARENT.comb$rowSpringMaterialName_2 -row $rowNodeNumber_2 -column 1 -sticky w -pady 3

                                set lab [label $PARENT.lab$rowNodeNumber_3 -text [= "Node Number \[3\]"]]
                                set "DisconnectConditions::NodeNumberIdentifier_3" 0
                                entry $PARENT.comb$rowNodeNumber_3 -textvariable "DisconnectConditions::NodeNumberIdentifier_3" -width 8
                                grid $lab -row $rowNodeNumber_3 -column 2 -sticky e
                                grid $PARENT.comb$rowNodeNumber_3 -row $rowNodeNumber_3 -column 3 -sticky w -pady 3

                                set lab [label $PARENT.lab$rowSpringMaterialName_3 -text [= "Material Name \[3\]"]]
                                set "DisconnectConditions::SpringMaterialNameIdentifier_3" [lindex $springMaterialsNameList 0]
                                ttk::combobox $PARENT.comb$rowSpringMaterialName_3 -textvariable "DisconnectConditions::SpringMaterialNameIdentifier_3" -values $springMaterialsNameList
                                grid $lab -row $rowNodeNumber_3 -column 0 -sticky e
                                grid $PARENT.comb$rowSpringMaterialName_3 -row $rowNodeNumber_3 -column 1 -sticky w -pady 3

                                set lab [label $PARENT.lab$rowNodeNumber_4 -text [= "Node Number \[4\]"]]
                                set "DisconnectConditions::NodeNumberIdentifier_4" 0
                                entry $PARENT.comb$rowNodeNumber_4 -textvariable "DisconnectConditions::NodeNumberIdentifier_4" -width 8
                                grid $lab -row $rowNodeNumber_4 -column 2 -sticky e
                                grid $PARENT.comb$rowNodeNumber_4 -row $rowNodeNumber_4 -column 3 -sticky w -pady 3

                                set lab [label $PARENT.lab$rowSpringMaterialName_4 -text [= "Material Name \[4\]"]]
                                set "DisconnectConditions::SpringMaterialNameIdentifier_4" [lindex $springMaterialsNameList 0]
                                ttk::combobox $PARENT.comb$rowSpringMaterialName_4 -textvariable "DisconnectConditions::SpringMaterialNameIdentifier_4" -values $springMaterialsNameList
                                grid $lab -row $rowNodeNumber_4 -column 0 -sticky e
                                grid $PARENT.comb$rowSpringMaterialName_4 -row $rowNodeNumber_4 -column 1 -sticky w -pady 3

                                if { $numNodes == 8 } {

                                    set lab [label $PARENT.lab$rowNodeNumber_5 -text [= "Node Number \[5\]"]]
                                    set "DisconnectConditions::NodeNumberIdentifier_5" 0
                                    entry $PARENT.comb$rowNodeNumber_5 -textvariable "DisconnectConditions::NodeNumberIdentifier_5" -width 8
                                    grid $lab -row $rowNodeNumber_5 -column 2 -sticky e
                                    grid $PARENT.comb$rowNodeNumber_5 -row $rowNodeNumber_5 -column 3 -sticky w -pady 3

                                    set lab [label $PARENT.lab$rowSpringMaterialName_5 -text [= "Material Name \[5\]"]]
                                    set "DisconnectConditions::SpringMaterialNameIdentifier_5" [lindex $springMaterialsNameList 0]
                                    ttk::combobox $PARENT.comb$rowSpringMaterialName_5 -textvariable "DisconnectConditions::SpringMaterialNameIdentifier_5" -values $springMaterialsNameList
                                    grid $lab -row $rowNodeNumber_5 -column 0 -sticky e
                                    grid $PARENT.comb$rowSpringMaterialName_5 -row $rowNodeNumber_5 -column 1 -sticky w -pady 3

                                    set lab [label $PARENT.lab$rowNodeNumber_6 -text [= "Node Number \[6\]"]]
                                    set "DisconnectConditions::NodeNumberIdentifier_6" 0
                                    entry $PARENT.comb$rowNodeNumber_6 -textvariable "DisconnectConditions::NodeNumberIdentifier_6" -width 8
                                    grid $lab -row $rowNodeNumber_6 -column 2 -sticky e
                                    grid $PARENT.comb$rowNodeNumber_6 -row $rowNodeNumber_6 -column 3 -sticky w -pady 3

                                    set lab [label $PARENT.lab$rowSpringMaterialName_6 -text [= "Material Name \[6\]"]]
                                    set "DisconnectConditions::SpringMaterialNameIdentifier_6" [lindex $springMaterialsNameList 0]
                                    ttk::combobox $PARENT.comb$rowSpringMaterialName_6 -textvariable "DisconnectConditions::SpringMaterialNameIdentifier_6" -values $springMaterialsNameList
                                    grid $lab -row $rowNodeNumber_6 -column 0 -sticky e
                                    grid $PARENT.comb$rowSpringMaterialName_6 -row $rowNodeNumber_6 -column 1 -sticky w -pady 3

                                    set lab [label $PARENT.lab$rowNodeNumber_7 -text [= "Node Number \[7\]"]]
                                    set "DisconnectConditions::NodeNumberIdentifier_7" 0
                                    entry $PARENT.comb$rowNodeNumber_7 -textvariable "DisconnectConditions::NodeNumberIdentifier_7" -width 8
                                    grid $lab -row $rowNodeNumber_7 -column 2 -sticky e
                                    grid $PARENT.comb$rowNodeNumber_7 -row $rowNodeNumber_7 -column 3 -sticky w -pady 3

                                    set lab [label $PARENT.lab$rowSpringMaterialName_7 -text [= "Material Name \[7\]"]]
                                    set "DisconnectConditions::SpringMaterialNameIdentifier_7" [lindex $springMaterialsNameList 0]
                                    ttk::combobox $PARENT.comb$rowSpringMaterialName_7 -textvariable "DisconnectConditions::SpringMaterialNameIdentifier_7" -values $springMaterialsNameList
                                    grid $lab -row $rowNodeNumber_7 -column 0 -sticky e
                                    grid $PARENT.comb$rowSpringMaterialName_7 -row $rowNodeNumber_7 -column 1 -sticky w -pady 3

                                    set lab [label $PARENT.lab$rowNodeNumber_8 -text [= "Node Number \[8\]"]]
                                    set "DisconnectConditions::NodeNumberIdentifier_8" 0
                                    entry $PARENT.comb$rowNodeNumber_8 -textvariable "DisconnectConditions::NodeNumberIdentifier_8" -width 8
                                    grid $lab -row $rowNodeNumber_8 -column 2 -sticky e
                                    grid $PARENT.comb$rowNodeNumber_8 -row $rowNodeNumber_8 -column 3 -sticky w -pady 3

                                    set lab [label $PARENT.lab$rowSpringMaterialName_8 -text [= "Material Name \[8\]"]]
                                    set "DisconnectConditions::SpringMaterialNameIdentifier_8" [lindex $springMaterialsNameList 0]
                                    ttk::combobox $PARENT.comb$rowSpringMaterialName_8 -textvariable "DisconnectConditions::SpringMaterialNameIdentifier_8" -values $springMaterialsNameList
                                    grid $lab -row $rowNodeNumber_8 -column 0 -sticky e
                                    grid $PARENT.comb$rowSpringMaterialName_8 -row $rowNodeNumber_8 -column 1 -sticky w -pady 3
                                }
                            }
                        }
                    }
                    "Load_Value_8" {

                        set rowLoadValue_8 $ROW

                        # global PROBLEMTYPEPATH
                        # set result [open "$PROBLEMTYPEPATH\\result.dat" "w"]
                        # puts $result "ASSSSSSSSSIM"
                        # puts $result $loadType
                        # close $result

                        if { $loadType == "LOAD" } {

                            bind $comboBoxLoadNumNodes <<ComboboxSelected>> {

                                global numNodes
                                set numNodes [$comboBoxLoadNumNodes get]

                                destroy $PARENT.lab$rowNodeNumber_1
                                destroy $PARENT.lab$rowNodeNumber_2
                                destroy $PARENT.lab$rowNodeNumber_3
                                destroy $PARENT.lab$rowNodeNumber_4
                                destroy $PARENT.lab$rowNodeNumber_5
                                destroy $PARENT.lab$rowNodeNumber_6
                                destroy $PARENT.lab$rowNodeNumber_7
                                destroy $PARENT.lab$rowNodeNumber_8

                                destroy $PARENT.comb$rowNodeNumber_1
                                destroy $PARENT.comb$rowNodeNumber_2
                                destroy $PARENT.comb$rowNodeNumber_3
                                destroy $PARENT.comb$rowNodeNumber_4
                                destroy $PARENT.comb$rowNodeNumber_5
                                destroy $PARENT.comb$rowNodeNumber_6
                                destroy $PARENT.comb$rowNodeNumber_7
                                destroy $PARENT.comb$rowNodeNumber_8

                                destroy $PARENT.lab$rowLoadValue
                                destroy $PARENT.lab$rowLoadValue_1
                                destroy $PARENT.lab$rowLoadValue_2
                                destroy $PARENT.lab$rowLoadValue_3
                                destroy $PARENT.lab$rowLoadValue_4
                                destroy $PARENT.lab$rowLoadValue_5
                                destroy $PARENT.lab$rowLoadValue_6
                                destroy $PARENT.lab$rowLoadValue_7
                                destroy $PARENT.lab$rowLoadValue_8

                                destroy $PARENT.comb$rowLoadValue
                                destroy $PARENT.comb$rowLoadValue_1
                                destroy $PARENT.comb$rowLoadValue_2
                                destroy $PARENT.comb$rowLoadValue_3
                                destroy $PARENT.comb$rowLoadValue_4
                                destroy $PARENT.comb$rowLoadValue_5
                                destroy $PARENT.comb$rowLoadValue_6
                                destroy $PARENT.comb$rowLoadValue_7
                                destroy $PARENT.comb$rowLoadValue_8

                                if { $numNodes == "All" } {

                                    set lab [label $PARENT.lab$rowLoadValue -text [= "Load Value"]]
                                    set "DisconnectConditions::LoadValueIdentifier" ""
                                    entry $PARENT.comb$rowLoadValue -textvariable "DisconnectConditions::LoadValueIdentifier" -width 10
                                    grid $lab -row $rowLoadValue -column 0 -sticky e
                                    grid $PARENT.comb$rowLoadValue -row $rowLoadValue -column 1 -sticky w -pady 3

                                } else {

                                    set lab [label $PARENT.lab$rowNodeNumber_1 -text [= "Node Number \[1\]"]]
                                    set "DisconnectConditions::NodeNumberIdentifier_1" ""
                                    entry $PARENT.comb$rowNodeNumber_1 -textvariable "DisconnectConditions::NodeNumberIdentifier_1" -width 10
                                    grid $lab -row $rowNodeNumber_1 -column 0 -sticky e
                                    grid $PARENT.comb$rowNodeNumber_1 -row $rowNodeNumber_1 -column 1 -sticky w -pady 3

                                    set lab [label $PARENT.lab$rowLoadValue_1 -text [= "Load Value \[1\]"]]
                                    set "DisconnectConditions::LoadValueIdentifier_1" ""
                                    entry $PARENT.comb$rowLoadValue_1 -textvariable "DisconnectConditions::LoadValueIdentifier_1" -width 10
                                    grid $lab -row $rowNodeNumber_1 -column 1
                                    grid $PARENT.comb$rowLoadValue_1 -row $rowNodeNumber_1 -column 1 -sticky e -padx 8

                                    set lab [label $PARENT.lab$rowNodeNumber_2 -text [= "Node Number \[2\]"]]
                                    set "DisconnectConditions::NodeNumberIdentifier_2" ""
                                    entry $PARENT.comb$rowNodeNumber_2 -textvariable "DisconnectConditions::NodeNumberIdentifier_2" -width 10
                                    grid $lab -row $rowNodeNumber_2 -column 0 -sticky e
                                    grid $PARENT.comb$rowNodeNumber_2 -row $rowNodeNumber_2 -column 1 -sticky w -pady 3

                                    set lab [label $PARENT.lab$rowLoadValue_2 -text [= "Load Value \[2\]"]]
                                    set "DisconnectConditions::LoadValueIdentifier_2" ""
                                    entry $PARENT.comb$rowLoadValue_2 -textvariable "DisconnectConditions::LoadValueIdentifier_2" -width 10
                                    grid $lab -row $rowNodeNumber_2 -column 1
                                    grid $PARENT.comb$rowLoadValue_2 -row $rowNodeNumber_2 -column 1 -sticky e -padx 8

                                    set lab [label $PARENT.lab$rowNodeNumber_3 -text [= "Node Number \[3\]"]]
                                    set "DisconnectConditions::NodeNumberIdentifier_3" ""
                                    entry $PARENT.comb$rowNodeNumber_3 -textvariable "DisconnectConditions::NodeNumberIdentifier_3" -width 10
                                    grid $lab -row $rowNodeNumber_3 -column 0 -sticky e
                                    grid $PARENT.comb$rowNodeNumber_3 -row $rowNodeNumber_3 -column 1 -sticky w -pady 3

                                    set lab [label $PARENT.lab$rowLoadValue_3 -text [= "Load Value \[3\]"]]
                                    set "DisconnectConditions::LoadValueIdentifier_3" ""
                                    entry $PARENT.comb$rowLoadValue_3 -textvariable "DisconnectConditions::LoadValueIdentifier_3" -width 10
                                    grid $lab -row $rowNodeNumber_3 -column 1
                                    grid $PARENT.comb$rowLoadValue_3 -row $rowNodeNumber_3 -column 1 -sticky e -padx 8

                                    set lab [label $PARENT.lab$rowNodeNumber_4 -text [= "Node Number \[4\]"]]
                                    set "DisconnectConditions::NodeNumberIdentifier_4" ""
                                    entry $PARENT.comb$rowNodeNumber_4 -textvariable "DisconnectConditions::NodeNumberIdentifier_4" -width 10
                                    grid $lab -row $rowNodeNumber_4 -column 0 -sticky e
                                    grid $PARENT.comb$rowNodeNumber_4 -row $rowNodeNumber_4 -column 1 -sticky w -pady 3

                                    set lab [label $PARENT.lab$rowLoadValue_4 -text [= "Load Value \[4\]"]]
                                    set "DisconnectConditions::LoadValueIdentifier_4" ""
                                    entry $PARENT.comb$rowLoadValue_4 -textvariable "DisconnectConditions::LoadValueIdentifier_4" -width 10
                                    grid $lab -row $rowNodeNumber_4 -column 1
                                    grid $PARENT.comb$rowLoadValue_4 -row $rowNodeNumber_4 -column 1 -sticky e -padx 8

                                    if { $numNodes == 8 } {

                                        set lab [label $PARENT.lab$rowNodeNumber_5 -text [= "Node Number \[5\]"]]
                                        set "DisconnectConditions::NodeNumberIdentifier_5" ""
                                        entry $PARENT.comb$rowNodeNumber_5 -textvariable "DisconnectConditions::NodeNumberIdentifier_5" -width 10
                                        grid $lab -row $rowNodeNumber_5 -column 0 -sticky e
                                        grid $PARENT.comb$rowNodeNumber_5 -row $rowNodeNumber_5 -column 1 -sticky w -pady 3

                                        set lab [label $PARENT.lab$rowLoadValue_5 -text [= "Load Value \[5\]"]]
                                        set "DisconnectConditions::LoadValueIdentifier_5" ""
                                        entry $PARENT.comb$rowLoadValue_5 -textvariable "DisconnectConditions::LoadValueIdentifier_5" -width 10
                                        grid $lab -row $rowNodeNumber_5 -column 1
                                        grid $PARENT.comb$rowLoadValue_5 -row $rowNodeNumber_5 -column 1 -sticky e -padx 8

                                        set lab [label $PARENT.lab$rowNodeNumber_6 -text [= "Node Number \[6\]"]]
                                        set "DisconnectConditions::NodeNumberIdentifier_6" ""
                                        entry $PARENT.comb$rowNodeNumber_6 -textvariable "DisconnectConditions::NodeNumberIdentifier_6" -width 10
                                        grid $lab -row $rowNodeNumber_6 -column 0 -sticky e
                                        grid $PARENT.comb$rowNodeNumber_6 -row $rowNodeNumber_6 -column 1 -sticky w -pady 3

                                        set lab [label $PARENT.lab$rowLoadValue_6 -text [= "Load Value \[6\]"]]
                                        set "DisconnectConditions::LoadValueIdentifier_6" ""
                                        entry $PARENT.comb$rowLoadValue_6 -textvariable "DisconnectConditions::LoadValueIdentifier_6" -width 10
                                        grid $lab -row $rowNodeNumber_6 -column 1
                                        grid $PARENT.comb$rowLoadValue_6 -row $rowNodeNumber_6 -column 1 -sticky e -padx 8

                                        set lab [label $PARENT.lab$rowNodeNumber_7 -text [= "Node Number \[7\]"]]
                                        set "DisconnectConditions::NodeNumberIdentifier_7" ""
                                        entry $PARENT.comb$rowNodeNumber_7 -textvariable "DisconnectConditions::NodeNumberIdentifier_7" -width 10
                                        grid $lab -row $rowNodeNumber_7 -column 0 -sticky e
                                        grid $PARENT.comb$rowNodeNumber_7 -row $rowNodeNumber_7 -column 1 -sticky w -pady 3

                                        set lab [label $PARENT.lab$rowLoadValue_7 -text [= "Load Value \[7\]"]]
                                        set "DisconnectConditions::LoadValueIdentifier_7" ""
                                        entry $PARENT.comb$rowLoadValue_7 -textvariable "DisconnectConditions::LoadValueIdentifier_7" -width 10
                                        grid $lab -row $rowNodeNumber_7 -column 1
                                        grid $PARENT.comb$rowLoadValue_7 -row $rowNodeNumber_7 -column 1 -sticky e -padx 8

                                        set lab [label $PARENT.lab$rowNodeNumber_8 -text [= "Node Number \[8\]"]]
                                        set "DisconnectConditions::NodeNumberIdentifier_8" ""
                                        entry $PARENT.comb$rowNodeNumber_8 -textvariable "DisconnectConditions::NodeNumberIdentifier_8" -width 10
                                        grid $lab -row $rowNodeNumber_8 -column 0 -sticky e
                                        grid $PARENT.comb$rowNodeNumber_8 -row $rowNodeNumber_8 -column 1 -sticky w -pady 3

                                        set lab [label $PARENT.lab$rowLoadValue_8 -text [= "Load Value \[8\]"]]
                                        set "DisconnectConditions::LoadValueIdentifier_8" ""
                                        entry $PARENT.comb$rowLoadValue_8 -textvariable "DisconnectConditions::LoadValueIdentifier_8" -width 10
                                        grid $lab -row $rowNodeNumber_8 -column 1
                                        grid $PARENT.comb$rowLoadValue_8 -row $rowNodeNumber_8 -column 1 -sticky e -padx 8

                                    }
                                }
                            }
                        } else {
                            
                            bind $comboBoxLoadNumNodes <<ComboboxSelected>> {

                                global numNodes
                                set numNodes [$comboBoxLoadNumNodes get]

                                destroy $PARENT.lab$rowNodeNumber_1
                                destroy $PARENT.lab$rowNodeNumber_2
                                destroy $PARENT.lab$rowNodeNumber_3
                                destroy $PARENT.lab$rowNodeNumber_4
                                destroy $PARENT.lab$rowNodeNumber_5
                                destroy $PARENT.lab$rowNodeNumber_6
                                destroy $PARENT.lab$rowNodeNumber_7
                                destroy $PARENT.lab$rowNodeNumber_8

                                destroy $PARENT.comb$rowNodeNumber_1
                                destroy $PARENT.comb$rowNodeNumber_2
                                destroy $PARENT.comb$rowNodeNumber_3
                                destroy $PARENT.comb$rowNodeNumber_4
                                destroy $PARENT.comb$rowNodeNumber_5
                                destroy $PARENT.comb$rowNodeNumber_6
                                destroy $PARENT.comb$rowNodeNumber_7
                                destroy $PARENT.comb$rowNodeNumber_8

                                destroy $PARENT.lab$rowLoadValue
                                destroy $PARENT.lab$rowLoadValue_1
                                destroy $PARENT.lab$rowLoadValue_2
                                destroy $PARENT.lab$rowLoadValue_3
                                destroy $PARENT.lab$rowLoadValue_4
                                destroy $PARENT.lab$rowLoadValue_5
                                destroy $PARENT.lab$rowLoadValue_6
                                destroy $PARENT.lab$rowLoadValue_7
                                destroy $PARENT.lab$rowLoadValue_8

                                destroy $PARENT.comb$rowLoadValue
                                destroy $PARENT.comb$rowLoadValue_1
                                destroy $PARENT.comb$rowLoadValue_2
                                destroy $PARENT.comb$rowLoadValue_3
                                destroy $PARENT.comb$rowLoadValue_4
                                destroy $PARENT.comb$rowLoadValue_5
                                destroy $PARENT.comb$rowLoadValue_6
                                destroy $PARENT.comb$rowLoadValue_7
                                destroy $PARENT.comb$rowLoadValue_8

                                if { $numNodes == "All" } {

                                    set lab [label $PARENT.lab$rowLoadValue -text [= "Heat Flux"]]
                                    set "DisconnectConditions::LoadValueIdentifier" ""
                                    entry $PARENT.comb$rowLoadValue -textvariable "DisconnectConditions::LoadValueIdentifier" -width 14
                                    grid $lab -row $rowLoadValue -column 0 -sticky e
                                    grid $PARENT.comb$rowLoadValue -row $rowLoadValue -column 1 -sticky w -pady 3

                                } else {

                                    set lab [label $PARENT.lab$rowNodeNumber_1 -text [= "Node Number \[1\]"]]
                                    set "DisconnectConditions::NodeNumberIdentifier_1" ""
                                    entry $PARENT.comb$rowNodeNumber_1 -textvariable "DisconnectConditions::NodeNumberIdentifier_1" -width 14
                                    grid $lab -row $rowNodeNumber_1 -column 0 -sticky e
                                    grid $PARENT.comb$rowNodeNumber_1 -row $rowNodeNumber_1 -column 1 -sticky w -pady 3

                                    set lab [label $PARENT.lab$rowLoadValue_1 -text [= "Heat Flux \[1\]"]]
                                    set "DisconnectConditions::LoadValueIdentifier_1" ""
                                    entry $PARENT.comb$rowLoadValue_1 -textvariable "DisconnectConditions::LoadValueIdentifier_1" -width 14
                                    grid $lab -row $rowNodeNumber_1 -column 2 -sticky e
                                    grid $PARENT.comb$rowLoadValue_1 -row $rowNodeNumber_1 -column 3 -sticky w -pady 3

                                    set lab [label $PARENT.lab$rowNodeNumber_2 -text [= "Node Number \[2\]"]]
                                    set "DisconnectConditions::NodeNumberIdentifier_2" ""
                                    entry $PARENT.comb$rowNodeNumber_2 -textvariable "DisconnectConditions::NodeNumberIdentifier_2" -width 14
                                    grid $lab -row $rowNodeNumber_2 -column 0 -sticky e
                                    grid $PARENT.comb$rowNodeNumber_2 -row $rowNodeNumber_2 -column 1 -sticky w -pady 3

                                    set lab [label $PARENT.lab$rowLoadValue_2 -text [= "Heat Flux \[2\]"]]
                                    set "DisconnectConditions::LoadValueIdentifier_2" ""
                                    entry $PARENT.comb$rowLoadValue_2 -textvariable "DisconnectConditions::LoadValueIdentifier_2" -width 14
                                    grid $lab -row $rowNodeNumber_2 -column 2 -sticky e
                                    grid $PARENT.comb$rowLoadValue_2 -row $rowNodeNumber_2 -column 3 -sticky w -pady 3

                                    set lab [label $PARENT.lab$rowNodeNumber_3 -text [= "Node Number \[3\]"]]
                                    set "DisconnectConditions::NodeNumberIdentifier_3" ""
                                    entry $PARENT.comb$rowNodeNumber_3 -textvariable "DisconnectConditions::NodeNumberIdentifier_3" -width 14
                                    grid $lab -row $rowNodeNumber_3 -column 0 -sticky e
                                    grid $PARENT.comb$rowNodeNumber_3 -row $rowNodeNumber_3 -column 1 -sticky w -pady 3

                                    set lab [label $PARENT.lab$rowLoadValue_3 -text [= "Heat Flux \[3\]"]]
                                    set "DisconnectConditions::LoadValueIdentifier_3" ""
                                    entry $PARENT.comb$rowLoadValue_3 -textvariable "DisconnectConditions::LoadValueIdentifier_3" -width 14
                                    grid $lab -row $rowNodeNumber_3 -column 2 -sticky e
                                    grid $PARENT.comb$rowLoadValue_3 -row $rowNodeNumber_3 -column 3 -sticky w -pady 3

                                    set lab [label $PARENT.lab$rowNodeNumber_4 -text [= "Node Number \[4\]"]]
                                    set "DisconnectConditions::NodeNumberIdentifier_4" ""
                                    entry $PARENT.comb$rowNodeNumber_4 -textvariable "DisconnectConditions::NodeNumberIdentifier_4" -width 14
                                    grid $lab -row $rowNodeNumber_4 -column 0 -sticky e
                                    grid $PARENT.comb$rowNodeNumber_4 -row $rowNodeNumber_4 -column 1 -sticky w -pady 3

                                    set lab [label $PARENT.lab$rowLoadValue_4 -text [= "Heat Flux \[4\]"]]
                                    set "DisconnectConditions::LoadValueIdentifier_4" ""
                                    entry $PARENT.comb$rowLoadValue_4 -textvariable "DisconnectConditions::LoadValueIdentifier_4" -width 14
                                    grid $lab -row $rowNodeNumber_4 -column 2 -sticky e
                                    grid $PARENT.comb$rowLoadValue_4 -row $rowNodeNumber_4 -column 3 -sticky w -pady 3

                                    if { $numNodes == 8 } {

                                        set lab [label $PARENT.lab$rowNodeNumber_5 -text [= "Node Number \[5\]"]]
                                        set "DisconnectConditions::NodeNumberIdentifier_5" ""
                                        entry $PARENT.comb$rowNodeNumber_5 -textvariable "DisconnectConditions::NodeNumberIdentifier_5" -width 14
                                        grid $lab -row $rowNodeNumber_5 -column 0 -sticky e
                                        grid $PARENT.comb$rowNodeNumber_5 -row $rowNodeNumber_5 -column 1 -sticky w -pady 3

                                        set lab [label $PARENT.lab$rowLoadValue_5 -text [= "Heat Flux \[5\]"]]
                                        set "DisconnectConditions::LoadValueIdentifier_5" ""
                                        entry $PARENT.comb$rowLoadValue_5 -textvariable "DisconnectConditions::LoadValueIdentifier_5" -width 14
                                        grid $lab -row $rowNodeNumber_5 -column 2 -sticky e
                                        grid $PARENT.comb$rowLoadValue_5 -row $rowNodeNumber_5 -column 3 -sticky w -pady 3

                                        set lab [label $PARENT.lab$rowNodeNumber_6 -text [= "Node Number \[6\]"]]
                                        set "DisconnectConditions::NodeNumberIdentifier_6" ""
                                        entry $PARENT.comb$rowNodeNumber_6 -textvariable "DisconnectConditions::NodeNumberIdentifier_6" -width 14
                                        grid $lab -row $rowNodeNumber_6 -column 0 -sticky e
                                        grid $PARENT.comb$rowNodeNumber_6 -row $rowNodeNumber_6 -column 1 -sticky w -pady 3

                                        set lab [label $PARENT.lab$rowLoadValue_6 -text [= "Heat Flux \[6\]"]]
                                        set "DisconnectConditions::LoadValueIdentifier_6" ""
                                        entry $PARENT.comb$rowLoadValue_6 -textvariable "DisconnectConditions::LoadValueIdentifier_6" -width 14
                                        grid $lab -row $rowNodeNumber_6 -column 2 -sticky e
                                        grid $PARENT.comb$rowLoadValue_6 -row $rowNodeNumber_6 -column 3 -sticky w -pady 3

                                        set lab [label $PARENT.lab$rowNodeNumber_7 -text [= "Node Number \[7\]"]]
                                        set "DisconnectConditions::NodeNumberIdentifier_7" ""
                                        entry $PARENT.comb$rowNodeNumber_7 -textvariable "DisconnectConditions::NodeNumberIdentifier_7" -width 14
                                        grid $lab -row $rowNodeNumber_7 -column 0 -sticky e
                                        grid $PARENT.comb$rowNodeNumber_7 -row $rowNodeNumber_7 -column 1 -sticky w -pady 3

                                        set lab [label $PARENT.lab$rowLoadValue_7 -text [= "Heat Flux \[7\]"]]
                                        set "DisconnectConditions::LoadValueIdentifier_7" ""
                                        entry $PARENT.comb$rowLoadValue_7 -textvariable "DisconnectConditions::LoadValueIdentifier_7" -width 14
                                        grid $lab -row $rowNodeNumber_7 -column 2 -sticky e
                                        grid $PARENT.comb$rowLoadValue_7 -row $rowNodeNumber_7 -column 3 -sticky w -pady 3

                                        set lab [label $PARENT.lab$rowNodeNumber_8 -text [= "Node Number \[8\]"]]
                                        set "DisconnectConditions::NodeNumberIdentifier_8" ""
                                        entry $PARENT.comb$rowNodeNumber_8 -textvariable "DisconnectConditions::NodeNumberIdentifier_8" -width 14
                                        grid $lab -row $rowNodeNumber_8 -column 0 -sticky e
                                        grid $PARENT.comb$rowNodeNumber_8 -row $rowNodeNumber_8 -column 1 -sticky w -pady 3

                                        set lab [label $PARENT.lab$rowLoadValue_8 -text [= "Heat Flux \[8\]"]]
                                        set "DisconnectConditions::LoadValueIdentifier_8" ""
                                        entry $PARENT.comb$rowLoadValue_8 -textvariable "DisconnectConditions::LoadValueIdentifier_8" -width 14
                                        grid $lab -row $rowNodeNumber_8 -column 2 -sticky e
                                        grid $PARENT.comb$rowLoadValue_8 -row $rowNodeNumber_8 -column 3 -sticky w -pady 3

                                    }
                                }
                            }
                        }
                    }
                    "Function_Source" {
                        set list { "User_Specified" "Import_from_File"}
                        set lab [label $PARENT.lab$ROW -text [= "Function Source"]]
                        # ttk::combobox $PARENT.comb$ROW -values $list -width 21
                        # set "DisconnectConditions::FunctionSourceIdentifier" "Import"
                        ttk::combobox $PARENT.comb$ROW -textvariable "DisconnectConditions::FunctionSourceIdentifier" -values $list -width 21
                        set comboBoxFunctionSource $PARENT.comb$ROW
                        set functionSource "Import"
                        $comboBoxFunctionSource set [lindex $list 1]
                    }
                    "Function_Name" {
                        set rowFunctionName $ROW
                        
                        if { $functionSource == "Import" } {
                            # set rowFunctionName $ROW
                            set lab [label $PARENT.lab$rowFunctionName -text [= "Function Name"]]
                            set "DisconnectConditions::FunctionNameIdentifier" "NLMM401_Material_1"
                            entry $PARENT.comb$rowFunctionName -textvariable "DisconnectConditions::FunctionNameIdentifier" -width 24
                            $PARENT.comb$rowFunctionName configure -state disabled
                        }
                    }
                    "Number_of_Sets_of_Values" {
                        # global NLMM401Name
                        # if { $NLMM401Name == "" || [lindex [GiD_Info Materials $NLMM401Name] 2] != "NLMM401" } {
                        #     OpenWarningWindow "Warning" "Update changes before edit the values of \nnormalized heat generation rate function" 0
                        # } else {

                        # }
                        set rowNumberSets $ROW
                        # set row [expr $ROW + 2]
  #                       set functionName [$PARENT.comb1 get]
  #                       set numSetsValues [$PARENT.comb3 get]


                        set lab [label $PARENT.lab$rowNumberSets -text [= "Number of Sets of Values"]]
                        set "DisconnectConditions::NumberSetsValuesIdentifier" ""
                        entry $PARENT.comb$rowNumberSets -textvariable "DisconnectConditions::NumberSetsValuesIdentifier" -width 7

                        if { $functionSource == "Specified" } {
                            button $PARENT.buttonValues -text "Insert" -command "OpenValuesOfNormalizedHeatGenerationRateFunctionWindow $ROW" -width 7
                            grid $PARENT.buttonValues -row $ROW -column 1 -sticky e -padx 32
                        } else {
                            set dbFile "$PROBLEMTYPEPATH\\MaterialDataBases\\NLMM401\\$FunctionNameIdentifier\_db.dat"
                            set dbFile [open $dbFile "r"]
                            set dbData [read $dbFile]
                            close $dbFile
                            $PARENT.comb$rowNumberSets insert 0 [expr [llength $dbData] / 2]
                            $PARENT.comb$rowNumberSets configure -state disabled
                            OpenValuesOfNormalizedHeatGenerationRateFunctionWindow 5 $dbData
                        }

                        # if { $functionSource == "Import" } {
                        #     grid $PARENT.buttonValues -row $ROW -column 1 -sticky e -padx 35
                        # } else {
                        #     grid $PARENT.buttonValues -row $ROW -column 2 -sticky e -padx 9
                        # }

                        bind $comboBoxFunctionSource <<ComboboxSelected>> {

                            global PROBLEMTYPEPATH \
                                   functionSource \
                                   comboBoxFunctionSource \
                                   rowFunctionName \
                                   rowNumberSets

                            set numSetsValues [$PARENT.comb$rowNumberSets get]
                            # $PARENT.comb$rowNumberSets delete 0 end

                            for {set i 0} {$i < $numSetsValues} {incr i} {
                                destroy $PARENT.x$i $PARENT.y$i $PARENT.labValue$i
                            }

                            destroy $PARENT.lab$rowFunctionName \
                                    $PARENT.comb$rowFunctionName \
                                    $PARENT.buttonValues \
                                    $PARENT.lab$rowNumberSets \
                                    $PARENT.comb$rowNumberSets

                            # $PARENT.comb$rowNumberSets configure -state normal

                            if { [$comboBoxFunctionSource get] == "User_Specified" } {
                                set functionSource "Specified"
                                # set FunctionSourceIdentifier "Specified"

                                label $PARENT.lab$rowNumberSets -text [= "Number of Sets of Values"]
                                set "DisconnectConditions::NumberSetsValuesIdentifier" ""
                                entry $PARENT.comb$rowNumberSets -textvariable "DisconnectConditions::NumberSetsValuesIdentifier" -width 7
                                
                                grid $PARENT.lab$rowNumberSets -row 3 -column 0 -sticky e
                                grid $PARENT.comb$rowNumberSets -row 3 -column 1 -sticky w -pady 3

                                # $PARENT.comb$rowNumberSets configure -state normal

                                button $PARENT.buttonValues -text "Insert" -command "OpenValuesOfNormalizedHeatGenerationRateFunctionWindow 3" -width 7
                                grid $PARENT.buttonValues -row $rowFunctionName -column 1 -sticky e -padx 32

                            } else {
                                set functionSource "Import"
                                # set FunctionSourceIdentifier "Import"

                                # set filename [Browser-ramR file read .gid [= "Read Ansys file"] {"$PROBLEMTYPEPATH\\MaterialDataBases\\NLMM401\\NLMM401_Material_1_db.dat"} {{{Ansys} {.prp }} {{All files} {.*}}} {} 0 {} ]

                                # set dataFile [Browser-ramR file read .gid [= "Import Material DB File"] {} {{{Material DB} {*_db.dat }}} {} 0 {}]
                                set dbFile [tk_getOpenFile -title "Import Material DB File" -filetypes {{"Material DB" {*_db.dat}}} -initialdir "$PROBLEMTYPEPATH\\MaterialDataBases\\NLMM401\\"]

                                if { $dbFile == "" } {

                                    $comboBoxFunctionSource set "User_Specified"
                                    set functionSource "Specified"

                                    label $PARENT.lab$rowNumberSets -text [= "Number of Sets of Values"]
                                    set "DisconnectConditions::NumberSetsValuesIdentifier" ""
                                    entry $PARENT.comb$rowNumberSets -textvariable "DisconnectConditions::NumberSetsValuesIdentifier" -width 7
                                
                                    
                                    $PARENT.comb$rowNumberSets delete 0 end
                                    grid $PARENT.lab$rowNumberSets -row 3 -column 0 -sticky e
                                    grid $PARENT.comb$rowNumberSets -row 3 -column 1 -sticky w -pady 3

                                    # $PARENT.comb$rowNumberSets configure -state normal

                                    button $PARENT.buttonValues -text "Insert" -command "OpenValuesOfNormalizedHeatGenerationRateFunctionWindow 3" -width 7
                                    grid $PARENT.buttonValues -row $rowFunctionName -column 1 -sticky e -padx 16

                                } else {

                                    set dbName [string range $dbFile [expr [string last "\/" $dbFile] + 1] [expr [string last "\_" $dbFile] - 1]]

                                    set dbFile [open $dbFile "r"]
                                    set dbData [read $dbFile]
                                    close $dbFile

                                    set lab [label $PARENT.lab$rowFunctionName -text [= "Function Name"]]
                                    entry $PARENT.comb$rowFunctionName -textvariable "DisconnectConditions::FunctionNameIdentifier" -width 24
                                    
                                    $PARENT.comb$rowFunctionName delete 0 end
                                    $PARENT.comb$rowFunctionName insert 0 $dbName
                                    $PARENT.comb$rowFunctionName configure -state disabled

                                    label $PARENT.lab$rowNumberSets -text [= "Number of Sets of Values"]
                                    set "DisconnectConditions::NumberSetsValuesIdentifier" ""
                                    entry $PARENT.comb$rowNumberSets -textvariable "DisconnectConditions::NumberSetsValuesIdentifier" -width 7
                                    
                                    $PARENT.comb$rowNumberSets insert 0 [expr [llength $dbData] / 2]
                                    $PARENT.comb$rowNumberSets configure -state disabled

                                    grid $PARENT.lab$rowFunctionName -row 3 -column 0 -sticky e
                                    grid $PARENT.comb$rowFunctionName -row 3 -column 1 -sticky w -pady 3

                                    grid $PARENT.lab$rowNumberSets -row 5 -column 0 -sticky e
                                    grid $PARENT.comb$rowNumberSets -row 5 -column 1 -sticky w -pady 3

                                    OpenValuesOfNormalizedHeatGenerationRateFunctionWindow 5 $dbData
                                    
                                } 


                                # set filenames [Browser-ramR file read .gid [_ "Read Calculix results file"] \
                                # {} {{{Calculix} {.frd }} {{All files} {.*}}} {} 1 \
                                # [list [_ "Import options"] Calculix::MoreImportOptions]]


                                # set rowFunctionName $ROW
                                # set lab [label $PARENT.lab$ROW -text [= "Function Name"]]
                                # set "DisconnectConditions::FunctionNameIdentifier" "NLMM401_Material"
                                # entry $PARENT.comb$ROW -textvariable "DisconnectConditions::FunctionNameIdentifier" -width 23
                            }
                        }
                        
                    }
                }
                #-values [concat Select [split [GiD_Info layers] " "]]
                grid $lab -row $ROW -column 0 -sticky e
                #grid $comb -row $ROW -column 1 -sticky w -pady 3
                grid $PARENT.comb$ROW -row $ROW -column 1 -sticky w -pady 3
            }
            return ""
        }
        "SYNC" {

            set GDN      [lindex $args 0]
            set STRUCT   [lindex $args 1]
            set QUESTION [lindex $args 2]
            
            #set currentIntv [lindex [GiD_Info intvdata num] 0]

            switch $QUESTION {
            
                "Layer_Pattern_Name" {
                    set LayerPatternNameIdentifier [string trim $LayerPatternNameIdentifier]
                    DWLocalSetValue $GDN $STRUCT Layer_Pattern_Name $LayerPatternNameIdentifier
                }
                "Load_Title" {
                    #set currentIntv [lindex [GiD_Info intvdata num] 0]
                    #set LoadTitleIdentifier [lindex [GiD_Info intvdata -interval $currentIntv] 2]
                    set LoadTitleIdentifier [string trim $LoadTitleIdentifier]    
                    DWLocalSetValue $GDN $STRUCT Load_Title $LoadTitleIdentifier
                    #set LoadTitleIdentifier [lindex [GiD_Info intvdata -interval $currentIntv] 2]
                } 
                "Group_Name" {
                    set GroupNameIdentifier [string trim $GroupNameIdentifier]    
                    DWLocalSetValue $GDN $STRUCT Group_Name $GroupNameIdentifier
                } 
                "Integration_Type" {
                    if { $IntegrationTypeIdentifier != "" } {
                        set IntegrationTypeIdentifier [string trim $IntegrationTypeIdentifier]
                    } else  {
                        set IntegrationTypeIdentifier "GAUSS-LEGENDRE"
                    }
                    DWLocalSetValue $GDN $STRUCT Integration_Type $IntegrationTypeIdentifier 
                } 
                "Integration_Name" {
                    set IntegrationNameIdentifier [string trim $IntegrationNameIdentifier]
                    DWLocalSetValue $GDN $STRUCT Integration_Name $IntegrationNameIdentifier       
                }
                "Spring_Group_Name" {
                    set SpringGroupNameIdentifier [string trim $SpringGroupNameIdentifier]
                    DWLocalSetValue $GDN $STRUCT Spring_Group_Name $SpringGroupNameIdentifier
                }
                "Spring_Material_Type" {

                    set SpringMaterialTypeIdentifier [string trim $SpringMaterialTypeIdentifier]
                    DWLocalSetValue $GDN $STRUCT Spring_Material_Type $SpringMaterialTypeIdentifier

                    # set list {}
                    # set allMaterialsNameList [GiD_Info Materials]
                    # for {set i 0} {$i < [llength $allMaterialsNameList]} {incr i} {
                    #    if { [lindex [GiD_Info Materials [lindex $allMaterialsNameList $i]] 2] == $SpringMaterialTypeIdentifier } {
                    #        lappend list [ReplaceSpaceWithUnderscore [lindex $allMaterialsNameList $i]]
                    #    }
                    # }
                    # # set lab [label $PARENT.labSpringMaterialName -text [= "Material Name"]]
                    # set "DisconnectConditions::SpringMaterialNameIdentifier" [lindex $list 0]
                    # # set newComboBoxSpringMaterialName $comboBoxSpringMaterialName
                    # # destroy $comboBoxSpringMaterialName
                    # $comboBoxSpringMaterialName configure -values $list
                }
                "Spring_Material_Name" {
                    set SpringMaterialNameIdentifier [string trim $SpringMaterialNameIdentifier]
                    DWLocalSetValue $GDN $STRUCT Spring_Material_Name $SpringMaterialNameIdentifier
                }
                "Line_Spring_Number_of_Nodes" {
                    set NumNodesIdentifier [string trim $NumNodesIdentifier]
                    DWLocalSetValue $GDN $STRUCT Line_Spring_Number_of_Nodes $NumNodesIdentifier
                }
                "Line_Spring_Material_Name" {
                    global numNodes
                    if { $numNodes == "All" } {
                        set SpringMaterialNameIdentifier [string trim $SpringMaterialNameIdentifier]
                        DWLocalSetValue $GDN $STRUCT Line_Spring_Material_Name $SpringMaterialNameIdentifier
                    }
                }
                "Surface_Spring_Material_Name" {
                    global numNodes
                    if { $numNodes == "All" } {
                        set SpringMaterialNameIdentifier [string trim $SpringMaterialNameIdentifier]
                        DWLocalSetValue $GDN $STRUCT Surface_Spring_Material_Name $SpringMaterialNameIdentifier
                    }
                }
                "Edge_Loads_Number_of_Nodes" {
                    set NumNodesIdentifier [string trim $NumNodesIdentifier]
                    DWLocalSetValue $GDN $STRUCT Edge_Loads_Number_of_Nodes $NumNodesIdentifier
                }
                "Face_Loads_Number_of_Nodes" {
                    set NumNodesIdentifier [string trim $NumNodesIdentifier]
                    DWLocalSetValue $GDN $STRUCT Face_Loads_Number_of_Nodes $NumNodesIdentifier
                }
                "Surface_Spring_Number_of_Nodes" {
                    set NumNodesIdentifier [string trim $NumNodesIdentifier]
                    DWLocalSetValue $GDN $STRUCT Surface_Spring_Number_of_Nodes $NumNodesIdentifier
                }
                "Surface_Heat_Flux_Number_of_Nodes" {
                    set NumNodesIdentifier [string trim $NumNodesIdentifier]
                    DWLocalSetValue $GDN $STRUCT Surface_Heat_Flux_Number_of_Nodes $NumNodesIdentifier
                }
                "Load_Value" {
                    global numNodes
                    if { $numNodes == "All" } {
                        set LoadValueIdentifier [string trim $LoadValueIdentifier]
                        DWLocalSetValue $GDN $STRUCT Load_Value $LoadValueIdentifier
                    }
                }
                "Node_Number_1" {
                    global numNodes
                    if { $numNodes != "All" } {
                        set NodeNumberIdentifier_1 [string trim $NodeNumberIdentifier_1]
                        DWLocalSetValue $GDN $STRUCT Node_Number_1 $NodeNumberIdentifier_1 
                    }
                }
                "Spring_Material_Name_1" {
                    global numNodes
                    if { $numNodes != "All" } {
                        set SpringMaterialNameIdentifier_1 [string trim $SpringMaterialNameIdentifier_1]
                        DWLocalSetValue $GDN $STRUCT Spring_Material_Name_1 $SpringMaterialNameIdentifier_1
                    }
                }
                "Load_Value_1" {
                    global numNodes
                    if { $numNodes != "All" } {
                        set LoadValueIdentifier_1 [string trim $LoadValueIdentifier_1]
                        DWLocalSetValue $GDN $STRUCT Load_Value_1 $LoadValueIdentifier_1
                    }
                }
                "Node_Number_2" {
                    global numNodes
                    if { $numNodes != "All" } {
                        set NodeNumberIdentifier_2 [string trim $NodeNumberIdentifier_2]
                        DWLocalSetValue $GDN $STRUCT Node_Number_2 $NodeNumberIdentifier_2
                    }
                }
                "Spring_Material_Name_2" {
                    global numNodes
                    if { $numNodes != "All" } {
                        set SpringMaterialNameIdentifier_2 [string trim $SpringMaterialNameIdentifier_2]
                        DWLocalSetValue $GDN $STRUCT Spring_Material_Name_2 $SpringMaterialNameIdentifier_2
                    }
                }
                "Load_Value_2" {
                    global numNodes
                    if { $numNodes != "All" } {
                        set LoadValueIdentifier_2 [string trim $LoadValueIdentifier_2]
                        DWLocalSetValue $GDN $STRUCT Load_Value_2 $LoadValueIdentifier_2
                    }
                }
                "Node_Number_3" {
                    global numNodes
                    if { $numNodes != "All" && $numNodes > 2 } {
                        set NodeNumberIdentifier_3 [string trim $NodeNumberIdentifier_3]
                        DWLocalSetValue $GDN $STRUCT Node_Number_3 $NodeNumberIdentifier_3
                    }
                }
                "Spring_Material_Name_3" {
                    global numNodes
                    if { $numNodes != "All" && $numNodes > 2 } {
                        set SpringMaterialNameIdentifier_3 [string trim $SpringMaterialNameIdentifier_3]
                        DWLocalSetValue $GDN $STRUCT Spring_Material_Name_3 $SpringMaterialNameIdentifier_3
                    }
                }
                "Load_Value_3" {
                    global numNodes
                    if { $numNodes != "All" && $numNodes > 2 } {
                        set LoadValueIdentifier_3 [string trim $LoadValueIdentifier_3]
                        DWLocalSetValue $GDN $STRUCT Load_Value_3 $LoadValueIdentifier_3
                    }
                }
                "Node_Number_4" {
                    global numNodes
                    if { $numNodes != "All" && $numNodes > 3 } {
                        set NodeNumberIdentifier_4 [string trim $NodeNumberIdentifier_4]
                        DWLocalSetValue $GDN $STRUCT Node_Number_4 $NodeNumberIdentifier_4
                    }
                }
                "Spring_Material_Name_4" {
                    global numNodes
                    if { $numNodes != "All" && $numNodes > 3 } {
                        set SpringMaterialNameIdentifier_4 [string trim $SpringMaterialNameIdentifier_4]
                        DWLocalSetValue $GDN $STRUCT Spring_Material_Name_4 $SpringMaterialNameIdentifier_4
                    }
                }
                "Load_Value_4" {
                    global numNodes
                    if { $numNodes != "All" && $numNodes > 3 } {
                        set LoadValueIdentifier_4 [string trim $LoadValueIdentifier_4]
                        DWLocalSetValue $GDN $STRUCT Load_Value_4 $LoadValueIdentifier_4
                    }
                }
                "Node_Number_5" {
                    global numNodes
                    if { $numNodes != "All" && $numNodes > 4 } {
                        set NodeNumberIdentifier_5 [string trim $NodeNumberIdentifier_5]
                        DWLocalSetValue $GDN $STRUCT Node_Number_5 $NodeNumberIdentifier_5
                    }
                }
                "Spring_Material_Name_5" {
                    global numNodes
                    if { $numNodes != "All" && $numNodes > 4 } {
                        set SpringMaterialNameIdentifier_5 [string trim $SpringMaterialNameIdentifier_5]
                        DWLocalSetValue $GDN $STRUCT Spring_Material_Name_5 $SpringMaterialNameIdentifier_5
                    }
                }
                "Load_Value_5" {
                    global numNodes
                    if { $numNodes != "All" && $numNodes > 4 } {
                        set LoadValueIdentifier_5 [string trim $LoadValueIdentifier_5]
                        DWLocalSetValue $GDN $STRUCT Load_Value_5 $LoadValueIdentifier_5
                    }
                }
                "Node_Number_6" {
                    global numNodes
                    if { $numNodes != "All" && $numNodes > 4 } {
                        set NodeNumberIdentifier_6 [string trim $NodeNumberIdentifier_6]
                        DWLocalSetValue $GDN $STRUCT Node_Number_6 $NodeNumberIdentifier_6
                    }
                }
                "Spring_Material_Name_6" {
                    global numNodes
                    if { $numNodes != "All" && $numNodes > 4 } {
                        set SpringMaterialNameIdentifier_6 [string trim $SpringMaterialNameIdentifier_6]
                        DWLocalSetValue $GDN $STRUCT Spring_Material_Name_6 $SpringMaterialNameIdentifier_6
                    }
                }
                "Load_Value_6" {
                    global numNodes
                    if { $numNodes != "All" && $numNodes > 4 } {
                        set LoadValueIdentifier_6 [string trim $LoadValueIdentifier_6]
                        DWLocalSetValue $GDN $STRUCT Load_Value_6 $LoadValueIdentifier_6
                    }
                }
                "Node_Number_7" {
                    global numNodes
                    if { $numNodes != "All" && $numNodes > 4 } {
                        set NodeNumberIdentifier_7 [string trim $NodeNumberIdentifier_7]
                        DWLocalSetValue $GDN $STRUCT Node_Number_7 $NodeNumberIdentifier_7
                    }
                }
                "Spring_Material_Name_7" {
                    global numNodes
                    if { $numNodes != "All" && $numNodes > 4 } {
                        set SpringMaterialNameIdentifier_7 [string trim $SpringMaterialNameIdentifier_7]
                        DWLocalSetValue $GDN $STRUCT Spring_Material_Name_7 $SpringMaterialNameIdentifier_7
                    }
                }
                "Load_Value_7" {
                    global numNodes
                    if { $numNodes != "All" && $numNodes > 4 } {
                        set LoadValueIdentifier_7 [string trim $LoadValueIdentifier_7]
                        DWLocalSetValue $GDN $STRUCT Load_Value_7 $LoadValueIdentifier_7
                    }
                }
                "Node_Number_8" {
                    global numNodes
                    if { $numNodes != "All" && $numNodes > 4 } {
                        set NodeNumberIdentifier_8 [string trim $NodeNumberIdentifier_8]
                        DWLocalSetValue $GDN $STRUCT Node_Number_8 $NodeNumberIdentifier_8
                    }
                }
                "Spring_Material_Name_8" {
                    global numNodes
                    if { $numNodes != "All" && $numNodes > 4 } {
                        set SpringMaterialNameIdentifier_8 [string trim $SpringMaterialNameIdentifier_8]
                        DWLocalSetValue $GDN $STRUCT Spring_Material_Name_8 $SpringMaterialNameIdentifier_8
                    }
                }
                "Load_Value_8" {
                    global numNodes
                    if { $numNodes != "All" && $numNodes > 4 } {
                        set LoadValueIdentifier_8 [string trim $LoadValueIdentifier_8]
                        DWLocalSetValue $GDN $STRUCT Load_Value_8 $LoadValueIdentifier_8
                    }
                }
                "Function_Source" {
                    set FunctionSourceIdentifier [string trim $FunctionSourceIdentifier]
                    DWLocalSetValue $GDN $STRUCT Function_Source $FunctionSourceIdentifier
                }
                "Function_Name" {
                    set FunctionNameIdentifier [string trim $FunctionNameIdentifier]
                    DWLocalSetValue $GDN $STRUCT Function_Name $FunctionNameIdentifier
                }
                "Number_of_Sets_of_Values" {
                    set NumberSetsValuesIdentifier [string trim $NumberSetsValuesIdentifier]
                    DWLocalSetValue $GDN $STRUCT Number_of_Sets_of_Values $NumberSetsValuesIdentifier
                }
            }

            return ""
        }
        default {

        }
    }
}

proc OpenValuesOfNormalizedHeatGenerationRateFunctionWindow { row args } {

    global PROBLEMTYPEPATH \
           PARENT \
           rowNumberSets 
           # functionSource 
           # rowFunctionName \
           # numSetsValues

    # if { $functionSource == "Import" } {
    #     set functionName [$PARENT.comb$rowFunctionName get]
    # }

    # global PROBLEMTYPEPATH
    # set result [open "$PROBLEMTYPEPATH\\result.dat" "w"]
    # puts $result [llength $args]
    # for {set i 0} {$i < [llength [lindex $args 0]]} {incr i} {
    #     puts $result [lindex [lindex $args 0] $i]
    # }
    # close $result

    set numSetsValues [$PARENT.comb$rowNumberSets get]
   
    # append functionName "_Values.dat"   

    if { $args == "" } {
        if { [$PARENT.buttonValues cget -text] == "Insert" } {

            # set numSetsValuesValues 0
            # set index 0
            
            incr row
            
            # if { [file exists "$PROBLEMTYPEPATH\\NLMM401_Values\\$functionName"] } {
            #     set valuesFile [open "$PROBLEMTYPEPATH\\NLMM401_Values\\$functionName" "r"]
            #     set valuesData [read $valuesFile]
            #     close $valuesFile

            #     set numSetsValuesValues [expr [llength $valuesData] / 2]
            #     set val 0

            #     while { $index < $numSetsValuesValues && $index < $numSetsValues } {
            #         label $PARENT.labValue$index -text [expr $index + 1]
            #         entry $PARENT.x$index -width 18
            #         $PARENT.x$index insert 0 [lindex $valuesData $val]
            #         incr val
            #         entry $PARENT.y$index -width 18
            #         $PARENT.y$index insert 0 [lindex $valuesData $val]
            #         grid $PARENT.labValue$index -row $row -column 0 -sticky w -padx 2 -pady 2
            #         grid $PARENT.x$index -row $row -column 0 -sticky e -padx 2 -pady 2
            #         grid $PARENT.y$index -row $row -column 1 -sticky e -padx 2 -pady 2
            #         incr row
            #         incr index
            #         incr val
            #     }
            # }
            # while { $index < $numSetsValues } {
            #     label $PARENT.labValue$index -text [expr $index + 1]
            #     entry $PARENT.x$index -width 18
            #     entry $PARENT.y$index -width 18
            #     grid $PARENT.labValue$index -row $row -column 0 -sticky w -padx 2 -pady 2
            #     grid $PARENT.x$index -row $row -column 0 -sticky e -padx 2 -pady 2
            #     grid $PARENT.y$index -row $row -column 1 -sticky e -padx 2 -pady 2
            #     incr row
            #     incr index
            # }

            for {set i 0} {$i < $numSetsValues} {incr i} {
                label $PARENT.labValue$i -text [expr $i + 1]
                entry $PARENT.x$i -width 18
                entry $PARENT.y$i -width 18
                grid $PARENT.labValue$i -row $row -column 0 -sticky w -padx 2 -pady 2
                grid $PARENT.x$i -row $row -column 0 -sticky e -padx 2 -pady 2
                grid $PARENT.y$i -row $row -columnspan 2 -column 1 -sticky w -padx 2 -pady 2
                incr row
            }
            $PARENT.comb$rowNumberSets configure -state disabled
            $PARENT.buttonValues configure -text "Delete"
        } else {
            # set valuesFile [open "$PROBLEMTYPEPATH\\NLMM401_Values\\$functionName" "w"]
            for {set i 0} {$i < $numSetsValues} {incr i} {
                # puts $valuesFile "[$PARENT.x$i get]\t[$PARENT.y$i get]"
                destroy $PARENT.x$i $PARENT.y$i $PARENT.labValue$i
            }
            # close $valuesFile
            $PARENT.buttonValues configure -text "Insert"
            $PARENT.comb$rowNumberSets configure -state normal
        }
    } else {
        incr row
        set val 0

        for {set i 0} {$i < $numSetsValues} {incr i} {
            label $PARENT.labValue$i -text [expr $i + 1]
            entry $PARENT.x$i -width 18
            $PARENT.x$i insert 0 [lindex [lindex $args 0] $val]
            $PARENT.x$i configure -state disabled
            incr val
            entry $PARENT.y$i -width 18
            $PARENT.y$i insert 0 [lindex [lindex $args 0] $val]
            $PARENT.y$i configure -state disabled
            grid $PARENT.labValue$i -row $row -column 0 -sticky w -padx 2 -pady 2
            grid $PARENT.x$i -row $row -column 0 -sticky e -padx 2 -pady 2
            grid $PARENT.y$i -row $row -column 1 -sticky w -padx 2 -pady 2
            incr row
            incr val
        }
    }
}