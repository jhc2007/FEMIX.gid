# ArcLengthParametersFromFemixFile.tcl

proc AssignArcLengthParameters { femixData } {

	global numDimension

	set arcLengthParametersData [GetBlockData $femixData "ARC_LENGTH_PARAMETERS"]

	if { [llength $arcLengthParametersData] > 0 } {
		
		set displacementControl [GetDisplacementControl $arcLengthParametersData]

		if { $displacementControl == "" } {
			return
		}

		GiD_Process Data ProblemData -SingleField- Displacement_Control $displacementControl escape
		
		if { $displacementControl == "DEFAULT" } {
		
			set value [GetArcLengthParameterValue $arcLengthParametersData "POINT_NUMBER"]
			if { $value != "" } {
				GiD_Process Data ProblemData -SingleField- Node_Number $value escape
			}

			set value [GetArcLengthParameterValue $arcLengthParametersData "DISPLACEMENT_INCREMENT"]
			if { $value != "" } {
				GiD_Process Data ProblemData -SingleField- Displacement_Increment $value escape
			}

		} else {

			# $displacementControl == "RELATIVE"
			
			set value [GetArcLengthParameterValue $arcLengthParametersData "POINT_A_NUMBER"]
			if { $value != "" } {
				GiD_Process Data ProblemData -SingleField- Node_Number_[A] $value escape
			}

			set value [GetArcLengthParameterValue $arcLengthParametersData "POINT_B_NUMBER"]
			if { $value != "" } {
				GiD_Process Data ProblemData -SingleField- Node_Number_[B] $value escape
			}

			set value [GetArcLengthParameterValue $arcLengthParametersData "RELATIVE_DISPLACEMENT_INCREMENT"]
			if { $value != "" } {
				GiD_Process Data ProblemData -SingleField- Relative_Displacement_Increment $value escape
			}
		}

		set value [GetArcLengthParameterValue $arcLengthParametersData "DEGREE_OF_FREEDOM"]
		if { $value != "" } {
			if { $numDimension == 3 } {
				switch $value {
					"_D1" {
						GiD_Process Data ProblemData -SingleField- Degree_of_Freedom "X" escape
					}
					"_D2" {
						GiD_Process Data ProblemData -SingleField- Degree_of_Freedom "Y" escape
					}
					"_D3" {
						GiD_Process Data ProblemData -SingleField- Degree_of_Freedom "Z" escape
					}
				}
			} else {
				# $numDimension == 2
				switch $value {
					"_D1" {
						GiD_Process Data ProblemData -SingleField- Degree_of_Freedom "Z" escape
					}
					"_D2" {
						GiD_Process Data ProblemData -SingleField- Degree_of_Freedom "X" escape
					}
					"_D3" {
						GiD_Process Data ProblemData -SingleField- Degree_of_Freedom "Y" escape
					}
				}
			}
		}

		set value [GetArcLengthParametersType $arcLengthParametersData]
		GiD_Process Data ProblemData -SingleField- Arc_Length_Parameters $value escape

		set value [GetArcLengthParameterValue $arcLengthParametersData "CONSTANT_RADIUS"]
		if { $value != "" } {
			
			if { $value == "_Y" } {
				GiD_Process Data ProblemData -SingleField- Constant_Radius 1 escape
				set value [GetArcLengthParameterValue $arcLengthParametersData "RADIUS_FACTOR"]
				if { $value != "" } {
					GiD_Process Data ProblemData -SingleField- Radius_Factor $value escape
				}
			} else {
				GiD_Process Data ProblemData -SingleField- Constant_Radius 0 escape
				set value [GetArcLengthParameterValue $arcLengthParametersData "LOAD_FACTOR"]
				if { $value != "" } {
					GiD_Process Data ProblemData -SingleField- Load_Factor $value escape
				}
			}
		}

		set value [GetArcLengthParameterValue $arcLengthParametersData "FORCE_DISPLACEMENT_SCALING_FACTOR"]
		if { $value != "" } {
			GiD_Process Data ProblemData -SingleField- Force_Displacement_Scaling_Factor $value escape
		}

		set value [GetArcLengthParameterValue $arcLengthParametersData "MOMENT_ROTATION_SCALING_FACTOR"]
		if { $value != "" } {
			GiD_Process Data ProblemData -SingleField- Moment_Rotation_Scaling_Factor $value escape
		}
	}
}

proc GetDisplacementControl { arcLengthParametersData } {

	set index 0

	while { $index < [llength $arcLengthParametersData] && [lindex $arcLengthParametersData $index] != "DISPLACEMENT_CONTROL" && [lindex $arcLengthParametersData $index] != "RELATIVE_DISPLACEMENT_CONTROL" } {
		set index [expr $index + 1]
	}

	if { $index == [llength $arcLengthParametersData] } {
		return ""
	}

	if { [lindex $arcLengthParametersData $index] == "DISPLACEMENT_CONTROL" } {
		return "DEFAULT"
	}

	return "RELATIVE"
}

proc GetArcLengthParameterValue { arcLengthParametersData parameter } {

	set index 0

	while { $index < [llength $arcLengthParametersData] && [lindex $arcLengthParametersData $index] != $parameter } {
		set index [expr $index + 1]
	}	

	if { $index == [llength $arcLengthParametersData] } {
		return ""
	}

	return [lindex $arcLengthParametersData [expr $index + 2]]
}

proc GetArcLengthParametersType { arcLengthParametersData } {

	set parameters { "CONSTANT_RADIUS" \
					 "RADIUS_FACTOR" \
					 "LOAD_FACTOR" \
					 "FORCE_DISPLACEMENT_SCALING_FACTOR" \
					 "MOMENT_ROTATION_SCALING_FACTOR"
				   }

	for {set i 0} {$i < [llength $arcLengthParametersData]} {incr i} {
		if { [ExistsListElement $parameters [lindex $arcLengthParametersData $i]] } {
			return "CUSTOM"
		}
	}

	return "DEFAULT"
}