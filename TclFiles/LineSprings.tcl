# LineSprings.tcl

proc CreateLineSpringsList {} {

  global lineSpringsConditionsOverNodes \
         lineSpringsConditionsOverLines

  set lineSpringsConditionsOverLines [GiD_Info Conditions _Line_Springs Geometry]
  set lineSpringsConditionsOverNodes [GiD_Info Conditions _Line_Springs Mesh]

  if { [llength $lineSpringsConditionsOverLines] == 0 } {
      return
  }

  global lineSpringsNodes

  # set lineSpringsList {}
  # set SpringMaterialsList {}
  # set springVectorsList {}
  # set SpringNumericalIntegrationList {}

  set elementConditions {}
  
  set lineSpringsNodes {}
  for {set i 0} {$i < [llength $lineSpringsConditionsOverNodes]} {incr i} {
      lappend lineSpringsNodes [lindex [lindex $lineSpringsConditionsOverNodes $i] 1]
  }

  set elemsType { "Linear" "Quadrilateral" "Hexahedra" }
  # set elemsType { "Quadrilateral" }
  # set elemsNodes {}

  for {set i 0} {$i < [llength $elemsType]} {incr i} {
      set elemsList [GiD_Info Mesh elements [lindex $elemsType $i] -sublist]
      for {set j 0} {$j < [llength $elemsList]} {incr j} {
          # set elemsNodes [list [lindex [lindex $elemsList $j] 0] 0]
          set elemNodes {}
          for {set k 1} {$k < [expr [llength [lindex $elemsList $j]] - 1]} {incr k} {
              if { [ExistsListElement $lineSpringsNodes [lindex [lindex $elemsList $j] $k]] } {
                  # set elemsNodes [lreplace $elemsNodes 1 1 [expr [lindex $elemsNodes 1] + 1]]
                  lappend elemNodes [lindex [lindex $elemsList $j] $k]
              }
          }
          if { [llength $elemNodes] > 1 } {
              switch [lindex $elemsType $i] {
                  "Linear" {
                      # set elementCond [list [lindex [lindex $elemsList $j] 0] 0 1]
                      if { [llength [lindex $elemsList $j]] == 5 } {
                          # Linear Element With 3 Nodes
                          set edgeNode1 [lindex [lindex $elemsList $j] 1]
                          set edgeNode2 [lindex [lindex $elemsList $j] 2]
                          set middleEdgeNode [lindex [lindex $elemsList $j] 3]
                          while { [ExistsListElement $edgeLoadsNodes $middleEdgeNode] } {
                              set elementCond [list [lindex [lindex $elemsList $j] 0] 0 1 [GetFirstNodeLineSpringsConditions $middleEdgeNode] [list $edgeNode1 $middleEdgeNode $edgeNode2]]
                              RemoveMiddleNodeLineSpringsConditions $middleEdgeNode
                              lappend elementConditions $elementCond                              
                          }                            
                      } else {
                          # Linear Element With 2 Nodes
                          set edgeNode1 [lindex [lindex $elemsList $j] 1]
                          set edgeNode2 [lindex [lindex $elemsList $j] 2]
                          set nodeConditions1 [GetAllNodeLineSpringsConditions $edgeNode1]
                          set nodeConditions2 [GetAllNodeLineSpringsConditions $edgeNode2]
                          set x 0
                          while { $x < [llength $nodeConditions1] } {
                              set y 0
                              while { $y < [llength $nodeConditions2] } {
                                  if { [EqualLineSpringConditions [lindex $nodeConditions1 $x] [lindex $nodeConditions2 $y]] } {
                                      set lines [GetAssignedLineSpringsLines [lindex $nodeConditions1 $x]]
                                      if { [NodesInsideSameLine $edgeNode1 $edgeNode2 $lines] } {
                                          set elementCond [list [lindex [lindex $elemsList $j] 0] 0 1 [lindex $nodeConditions1 $x] [list $edgeNode1 $edgeNode2]]
                                          lappend elementConditions $elementCond 
                                          set nodeConditions2 [lreplace $nodeConditions2 $y $y]
                                      }
                                      set y [llength $nodeConditions2]
                                  }
                                  incr y
                              }
                              incr x
                          }
                      }
                  }
                  "Quadrilateral" {
                      set edges [GetQuadElementEdges [lrange [lindex $elemsList $j] 1 end-1] $elemNodes]
                      if { [llength [lindex $elemsList $j]] == 10 } {
                          # Quad Element With 8 Nodes
                          for {set n 0} {$n < [llength $edges]} {incr n} {
                              set edgeNode1 [lindex [lindex $elemsList $j] [lindex $edges $n]]
                              set edgeNode2 [lindex [lindex $elemsList $j] [expr [lindex $edges $n] % 4 + 1]]
                              set middleEdgeNode [lindex [lindex $elemsList $j] [expr [lindex $edges $n] + 4]]
                              while { [ExistsListElement $lineSpringsNodes $middleEdgeNode] } {
                                  set elementCond [list [lindex [lindex $elemsList $j] 0] 1 [lindex $edges $n] [GetFirstNodeLineSpringsConditions $middleEdgeNode] [list $edgeNode1 $middleEdgeNode $edgeNode2]]
                                  RemoveMiddleNodeLineSpringsConditions $middleEdgeNode
                                  lappend elementConditions $elementCond                              
                              }
                          }
                      } else {
                          # Quad Element With 4 Nodes
                          for {set n 0} {$n < [llength $edges]} {incr n} {
                              set edgeNode1 [lindex [lindex $elemsList $j] [lindex $edges $n]]
                              set edgeNode2 [lindex [lindex $elemsList $j] [expr [lindex $edges $n] % 4 + 1]]
                              set nodeConditions1 [GetAllNodeLineSpringsConditions $edgeNode1]
                              set nodeConditions2 [GetAllNodeLineSpringsConditions $edgeNode2]
                              set x 0
                              while { $x < [llength $nodeConditions1] } {
                                  set y 0
                                  while { $y < [llength $nodeConditions2] } {
                                      if { [EqualLineSpringConditions [lindex $nodeConditions1 $x] [lindex $nodeConditions2 $y]] } {
                                          set lines [GetAssignedLineSpringsLines [lindex $nodeConditions1 $x]]
                                          if { [NodesInsideSameLine $edgeNode1 $edgeNode2 $lines] } {
                                              set elementCond [list [lindex [lindex $elemsList $j] 0] 1 [lindex $edges $n] [lindex $nodeConditions1 $x] [list $edgeNode1 $edgeNode2]]
                                              lappend elementConditions $elementCond                              
                                              set nodeConditions2 [lreplace $nodeConditions2 $y $y]
                                          }
                                          set y [llength $nodeConditions2]
                                      }
                                      incr y
                                  }
                                  incr x
                              }
                          }
                      }
                  }
                  "Hexahedra" {
                      set facesEdges [GetHexaElementFacesEdges [lrange [lindex $elemsList $j] 1 end-1] $elemNodes]
                      if { [llength [lindex $elemsList $j]] == 22 } {
                          # Hexa Element With 20 Nodes
                          for {set n 0} {$n < [llength $facesEdges]} {incr n} {
                              set face [lindex [lindex $facesEdges $n] 0]
                              set edge [lindex [lindex $facesEdges $n] 1]
                              set nodes [GetHexaElemNodesInFemixOrder [lrange [lindex $elemsList $j] 1 end-1]]
                              set edgeNodes [GetHexaElemEdgeNodes $nodes $edge]
                              while { [ExistsListElement $lineSpringsNodes [lindex $edgeNodes 1]] } {
                                  set elementCond [list [lindex [lindex $elemsList $j] 0] $face $edge [GetFirstNodeLineSpringsConditions [lindex $edgeNodes 1]] [list [lindex $edgeNodes 0] [lindex $edgeNodes 1] [lindex $edgeNodes 2]]]
                                  RemoveMiddleNodeLineSpringsConditions [lindex $edgeNodes 1]
                                  lappend elementConditions $elementCond                              
                              }
                          }
                      } else {
                          # Hexa Element With 8 Nodes
                          for {set n 0} {$n < [llength $facesEdges]} {incr n} {
                              set face [lindex [lindex $facesEdges $n] 0]
                              set edge [lindex [lindex $facesEdges $n] 1]
                              set nodes [lrange [lindex $elemsList $j] 1 end-1]
                              set edgeNodes [GetHexaElemEdgeNodes $nodes $edge]
                              set nodeConditions1 [GetAllNodeLineSpringsConditions [lindex $edgeNodes 0]]
                              set nodeConditions2 [GetAllNodeLineSpringsConditions [lindex $edgeNodes 1]]
                              set x 0
                              while { $x < [llength $nodeConditions1] } {
                                  set y 0
                                  while { $y < [llength $nodeConditions2] } {
                                      if { [EqualLineSpringConditions [lindex $nodeConditions1 $x] [lindex $nodeConditions2 $y]] } {
                                          set lines [GetAssignedLineSpringsLines [lindex $nodeConditions1 $x]]
                                          if { [NodesInsideSameLine [lindex $edgeNodes 0] [lindex $edgeNodes 1] $lines] } {
                                              set elementCond [list [lindex [lindex $elemsList $j] 0] $face $edge [lindex $nodeConditions1 $x] [list [lindex $edgeNodes 0] [lindex $edgeNodes 1]]]
                                              lappend elementConditions $elementCond                              
                                              set nodeConditions2 [lreplace $nodeConditions2 $y $y]
                                          }
                                          set y [llength $nodeConditions2]
                                      }
                                      incr y
                                  }
                                  incr x
                              }
                          }
                      }
                  }
              }
          }
      }
  }

  # AddLineSprings { element face edge conditions edgeNodesList }
  AddLineSprings [lindex [lindex $elementConditions 0] 0] \
                 [lindex [lindex $elementConditions 0] 1] \
                 [lindex [lindex $elementConditions 0] 2] \
                 [lindex [lindex $elementConditions 0] 3] \
                 [lindex [lindex $elementConditions 0] 4] 

  for {set i 1} {$i < [llength $elementConditions]} {incr i} {
      if { ![UpdateLineSprings [lindex $elementConditions [expr $i - 1]] [lindex $elementConditions $i]] } {
            AddLineSprings [lindex [lindex $elementConditions $i] 0] [lindex [lindex $elementConditions $i] 1] [lindex [lindex $elementConditions $i] 2] [lindex [lindex $elementConditions $i] 3] [lindex [lindex $elementConditions $i] 4]
      }
  } 
 
  destroy $lineSpringsConditionsOverNodes \
          $lineSpringsConditionsOverLines \
          $lineSpringsNodes \
          $elementConditions
}

proc GetAssignedLineSpringsLines { conditions } {

    global lineSpringsConditionsOverLines

    set lines {}
    for {set i 0} {$i < [llength $lineSpringsConditionsOverLines]} {incr i} {
        if { [EqualLineSpringConditions [lrange [lindex $lineSpringsConditionsOverLines $i] 3 end] $conditions] } {
            lappend lines [lindex [lindex $lineSpringsConditionsOverLines $i] 1]
        }
    }

    return $lines
}

proc GetHexaElemEdgeNodes { nodes edge } {

  if { [llength $nodes] == 20 } {
    # Hexahedra Elements with 20 Nodes
    if { $edge < 5 } {
      set index [expr 2 * ($edge - 1)]
      return [list [lindex $nodes $index] [lindex $nodes [expr $index + 1]] [lindex $nodes [expr ($index + 2) % 8]]]
    } elseif { $edge > 8 } {
      set index [expr ($edge - 9) * 2 + 12]
      return [list [lindex $nodes $index] [lindex $nodes [expr $index + 1]] [lindex $nodes [expr 12 + ($index - 10) % 8]]]
      # if { $edge == 12 } {
      #   return [list [lindex $nodes $index] [lindex $nodes [expr $index + 1]] [lindex $nodes 12]]
      # }
      # return [lrange $nodex $index [expr $index + 2]]
    } else {
      set index [expr $edge - 5]
      return [list [lindex $nodes [expr 2 * $index]] [lindex $nodes [expr 8 + $index]] [lindex $nodes [expr 12 + 2 * $index]]]
    }
  } else {
    # Hexahedra Elements with 8 Nodes
    if { $edge < 5 } {
      set index [expr $edge - 1]
      return [list [lindex $nodes $index] [lindex $nodes [expr ($index + 1) % 4]]]
    } elseif { $edge > 8 } {
      set index [expr $edge - 5]
      return [list [lindex $nodes $index] [lindex $nodes [expr 4 + ($index - 3) % 4]]]
      # if { $edge == 12 } {
      #   return [list [lindex $nodes $index] [lindex $nodes 4]]
      # }
      # return [lrange $nodex $index [expr $index + 1]]
    } else {
      set index [expr $edge - 5]
      return [list [lindex $nodes $index] [lindex $nodes [expr 4 + $index]]]
    }
  }
}

proc RemoveMiddleNodeLineSpringsConditions { middleNode } {

    global lineSpringsConditionsOverNodes \
           lineSpringsNodes

    set index 0

    while { [lindex $lineSpringsNodes $index] != $middleNode } {
        incr index
    }

    set lineSpringsNodes [lreplace $lineSpringsNodes $index $index]
    set lineSpringsConditionsOverNodes [lreplace $lineSpringsConditionsOverNodes $index $index]
}

proc UpdateLineSprings { elemCond1 elemCond2 } {

    if { [lindex [lindex $elemCond1 3] 13] != "All" || [lindex [lindex $elemCond2 3] 13] != "All" } {
        return false
    }

    global LineSpringsList

    if { [lindex $elemCond2 0] != [expr [lindex $elemCond1 0] + 1] || [lindex $elemCond2 1] != [lindex $elemCond1 1] || [lindex $elemCond2 2] != [lindex $elemCond1 2] } {
        return false
    }
    if { ![EqualLineSpringConditions [lindex $elemCond1 3] [lindex $elemCond2 3]] } {
        return false
    }

    set lastLineSpring [lindex $LineSpringsList end]
    set lastLineSpring [lreplace $lastLineSpring 1 1 [lindex $elemCond2 0]]
    set LineSpringsList [lreplace $LineSpringsList end end $lastLineSpring]
    
    return true
}

proc AddLineSprings { element face edge conditions edgeNodesList } {

    global LineSpringsList \
           AuxiliaryVectorsList \
           SpringMaterialsList 
           # SpringNumericalIntegrationList \
           # NUMERICALINTEGRATIONLIST

    set lineSpring {}

    lappend lineSpring $element
    lappend lineSpring $element
    lappend lineSpring $face
    lappend lineSpring $edge
    lappend lineSpring [lindex $conditions 0]
    lappend lineSpring [lindex $conditions 1]

    switch [lindex $conditions 1] {
        "Global" {
            lappend lineSpring [lindex $conditions 2]
        }
        "Frame" {
            lappend lineSpring [lindex $conditions 3]
        }
        "Solid/Shell" {
            lappend lineSpring [lindex $conditions 4]
        }
        "Plane_Stress/Strain/Axisymmetry" {
            lappend lineSpring [lindex $conditions 5]
        }
        "Specified" {
            # set vector {}
            for {set i 6} {$i < 9} {incr i} {
                lappend lineSpring [lindex $conditions $i]
                # lappend vector [format %.5e [lindex $conditions $i]]
            }
            AddAuxiliaryVector [lindex $conditions 6] [lindex $conditions 7] [lindex $conditions 8]
            # if { ![ExistsListElement $AuxiliaryVectorsList $vector] } {
            #     lappend AuxiliaryVectorsList $vector
            # }
        }
    }
    for {set i 9} {$i < 11} {incr i} {
        lappend lineSpring [lindex $conditions $i]
    }

    switch [lindex $conditions 11] {
        "GAUSS-LEGENDRE_" {
            lappend lineSpring "_GLEG"
        }
        "GAUSS-LOBATTO" {
            lappend lineSpring "_GLOB"
        }
        "NEWTON-COTES" {
            lappend lineSpring "_NCOTES"
        }
    }

    lappend lineSpring "SPRING_NUMERICAL_INTEGRATION_[GetSpringNumericalIntegrationIndex [list [lindex $conditions 12]]]"

    set materialType [lindex $conditions 14]
    lappend lineSpring $materialType

    set numNodes [lindex $conditions 13]
    
    if { $numNodes == "All" } {
        lappend lineSpring "1"
        set materialName [lindex $conditions 15]
        lappend lineSpring $materialName
        if { ![ExistsListElement $SpringMaterialsList [list $materialType $materialName]] } {
            lappend SpringMaterialsList [list $materialType $materialName]
        }
    } else {
        lappend lineSpring $numNodes
        set geometryNodes {}
        set materialNames {}
        for {set i 0} {$i < $numNodes} {incr i} {
            lappend geometryNodes [lindex $conditions [expr 16 + 2 * $i]]
            lappend materialNames [lindex $conditions [expr 17 + 2 * $i]]
            if { ![ExistsListElement $SpringMaterialsList [list $materialType [lindex $materialNames $i]]] } {
                lappend SpringMaterialsList [list $materialType [lindex $materialNames $i]]
            }
        }
        while { [llength $edgeNodesList] > 1 } {
            set index 0
            set edgeNodeCoord [lindex [GiD_Info Coordinates [lindex $edgeNodesList $index] Mesh] 0]
            set geomNodeCoord [lindex [GiD_Info Coordinates [lindex $geometryNodes $index] Geometry] 0]
            while { $index < [llength $geometryNodes] && (double([lindex $edgeNodeCoord 0] != [lindex $geomNodeCoord 0]) || double([lindex $edgeNodeCoord 1] != [lindex $geomNodeCoord 1]) || double([lindex $edgeNodeCoord 2] != [lindex $geomNodeCoord 2])) } {
                incr index
                set geomNodeCoord [lindex [GiD_Info Coordinates [lindex $geometryNodes $index] Geometry] 0]
            }
            if { $index < [llength $geometryNodes] } {
                lappend lineSpring [lindex $materialNames $index]
                set geometryNodes [lreplace $geometryNodes $index $index]
                set materialNames [lreplace $materialNames $index $index]
            }
            set edgeNodesList [lreplace $edgeNodesList 0 0]
        }
        lappend lineSpring [lindex $materialNames 0]
    }

  # global PROBLEMTYPEPATH
  # set result [open "$PROBLEMTYPEPATH\\result_new.dat" "w"]
  # puts $result $SpringMaterialsList
  # close $result
 
    lappend LineSpringsList $lineSpring
}

proc GetFirstNodeLineSpringsConditions { node } {

    global lineSpringsConditionsOverNodes

    for {set i 0} {$i < [llength $lineSpringsConditionsOverNodes]} {incr i} {
        if { [lindex [lindex $lineSpringsConditionsOverNodes $i] 1] == $node } {
            return [lrange [lindex $lineSpringsConditionsOverNodes $i] 3 end]
        }
    }
}

proc GetAllNodeLineSpringsConditions { node } {

    global lineSpringsConditionsOverNodes

    set nodeConditions {}
    for {set i 0} {$i < [llength $lineSpringsConditionsOverNodes]} {incr i} {
        if { [lindex [lindex $lineSpringsConditionsOverNodes $i] 1] == $node } {
            lappend nodeConditions [lrange [lindex $lineSpringsConditionsOverNodes $i] 3 end]
        }
    }
    return $nodeConditions
}

proc GetHexaElementFacesEdges { allElemNodes assignedNodes } {

    set facesEdges {}

    if { [llength $allElemNodes] == 20 } {
        # Hexahedra Elements with 20 Nodes
        set allElemNodes [GetHexaElemNodesInFemixOrder $allElemNodes]
        for {set i 0} {$i < 8} {incr i 2} {
            set edgeNodes [list [lindex $allElemNodes $i] [lindex $allElemNodes [expr $i + 1]] [lindex $allElemNodes [expr ($i + 2) % 8]]]
            if { [EdgeNodesAreAssigned $edgeNodes $assignedNodes] } {
                lappend facesEdges [list 1 [expr $i / 2 + 1]]
            }
        }
        for {set i 0} {$i < 4} {incr i} {
            set edgeNodes [list [lindex $allElemNodes [expr 2 * $i]] [lindex $allElemNodes [expr 8 + $i]] [lindex $allElemNodes [expr 12 + 2 * $i]]]
            if { [EdgeNodesAreAssigned $edgeNodes $assignedNodes] } {
                lappend facesEdges [list [expr 2 + $i] [expr 5 + $i]]
            }
        }
        for {set i 0} {$i < 8} {incr i 2} {
            set edgeNodes [list [lindex $allElemNodes [expr 12 + $i]] [lindex $allElemNodes [expr 13 + $i]] [lindex $allElemNodes [expr 12 + ($i + 2) % 8]]]
            if { [EdgeNodesAreAssigned $edgeNodes $assignedNodes] } {
                lappend facesEdges [list 6 [expr 9 + $i / 2]]
            }
        }
    } else {
        # Hexahedra Elements with 8 Nodes
        for {set i 0} {$i < 4} {incr i} {
            set edgeNodes [list [lindex $allElemNodes $i] [lindex $allElemNodes [expr ($i + 1) % 4]]]          
            if { [EdgeNodesAreAssigned $edgeNodes $assignedNodes] } {
                lappend facesEdges [list 1 [expr $i + 1]]
            }
        }
        for {set i 0} {$i < 4} {incr i} {
            set edgeNodes [list [lindex $allElemNodes $i] [lindex $allElemNodes [expr 4 + $i]]]          
            if { [EdgeNodesAreAssigned $edgeNodes $assignedNodes] } {
                lappend facesEdges [list [expr 2 + $i] [expr 5 + $i]]
            }
        }
        for {set i 0} {$i < 4} {incr i} {
            set edgeNodes [list [lindex $allElemNodes [expr 4 + $i]] [lindex $allElemNodes [expr 4 + ($i + 1) % 4]]]          
            if { [EdgeNodesAreAssigned $edgeNodes $assignedNodes] } {
                lappend facesEdges [list 6 [expr $i + 9]]
            }
        }

    }

    return $facesEdges
}

proc GetQuadElementEdges { allElemNodes assignedNodes } {
    
    set edges {}

    if { [llength $allElemNodes] == 8 } {
        # Quad Element with 8 Nodes
        set allElemNodes [GetQuadElemNodesInFemixOrder $allElemNodes]
        for {set i 0} {$i < 8} {incr i 2} {
            set edgeNodes [list [lindex $allElemNodes $i] [lindex $allElemNodes [expr $i + 1]] [lindex $allElemNodes [expr ($i + 2) % 8]]]
            if { [EdgeNodesAreAssigned $edgeNodes $assignedNodes] } {
                lappend edges [expr $i / 2 + 1]
            }
        }
    } else {
        # Quad Element with 4 Nodes
        for {set i 0} {$i < 4} {incr i} {
            set edgeNodes [list [lindex $allElemNodes $i] [lindex $allElemNodes [expr ($i + 1) % 4]]]
            if { [EdgeNodesAreAssigned $edgeNodes $assignedNodes] } {
                lappend edges [expr $i + 1]
            }
        }
    }

    return $edges
}

proc EdgeNodesAreAssigned { edgeNodes assignedNodes } {

    set countNodes 0

    for {set i 0} {$i < [llength $edgeNodes]} {incr i} {
       if { [ExistsListElement $assignedNodes [lindex $edgeNodes $i]] } {
          set countNodes [expr $countNodes + 1]
       }
    }
    if { $countNodes == [llength $edgeNodes] } {
        return true
    }
    return false
}

proc GetHexaElemNodesInFemixOrder { gidNodes } {

    for {set i 0} {$i < 4} {incr i} {
        lappend femixNodes [lindex $gidNodes $i]
        lappend femixNodes [lindex $gidNodes [expr 8 + $i]]
    }
    for {set i 12} {$i < 16} {incr i} {
        lappend femixNodes [lindex $gidNodes $i]
    }
    for {set i 0} {$i < 4} {incr i} {
        lappend femixNodes [lindex $gidNodes [expr 4 + $i]]
        lappend femixNodes [lindex $gidNodes [expr 16 + $i]]
    }

    return $femixNodes
}

proc EqualLineSpringConditions { cond1 cond2 } {

    for {set i 0} {$i < 2} {incr i} {
        if { [lindex $cond1 $i] != [lindex $cond2 $i] } {
            return false
        }
    }
    
    switch [lindex $cond1 1] {

        "Global" {
            if { [lindex $cond1 2] != [lindex $cond2 2] } {
                return false
            }
        }
        "Frame" {
            if { [lindex $cond1 3] != [lindex $cond2 3] } {
                return false
            }
        }
        "Solid/Shell" {
            if { [lindex $cond1 4] != [lindex $cond2 4] } {
                return false
            }
        }
        "Plane_Stress/Strain/Axisymmetry" {
            if { [lindex $cond1 5] != [lindex $cond2 5] } {
                return false
            }
        }
        "Specified" {
            for {set i 6} {$i < 9} {incr i} {
                if { [lindex $cond1 $i] != [lindex $cond2 $i] } {
                    return false
                }
            }
        }
    }

    for {set i 9} {$i < 15} {incr i} {
        if { [lindex $cond1 $i] != [lindex $cond2 $i] } {
            return false
        }
    }
    
    set numNodes [lindex $cond1 13]
    if { $numNodes == "All" } {
        if { [lindex $cond1 15] != [lindex $cond2 15] } {
            return false
        }
    } else {
        for {set i 16} {$i < [expr 16 + 2 * $numNodes]} {incr i} {
            if { [lindex $cond1 $i] != [lindex $cond2 $i] } {
                return false
            }
        }
    }
    
    return true
}

proc CountLineSprings {} {

    global LineSpringsList

    set count 0
    for {set i 0} {$i < [llength $LineSpringsList]} {incr i} {
        set count [expr $count + [lindex [lindex $LineSpringsList $i] 1] - [lindex [lindex $LineSpringsList $i] 0] + 1]
    }

    return $count
}

proc WriteLineSpringsList {} {

    global LineSpringsList \
           numDimension


    #{1 6 1 1 FEMIXDefaultGroup Global XG1 Structure_to_Ground Translational _GLEG SPRING_INTEGRATION_1 LIN_SPRING 1 Linear_Spring}

    set springsList ""
    set index 1

    for {set i 0} {$i < [llength $LineSpringsList]} {incr i} {

        set lineSpring [lindex $LineSpringsList $i]

        if { [lindex $lineSpring 0] == [lindex $lineSpring 1] } {
            append springsList [format %7d $index]
            append springsList "  [lindex $lineSpring 4]  1  "
            append springsList "[lindex $lineSpring 0]  "
            if { [lindex $lineSpring 2] > 0 } {
                append springsList "    [lindex $lineSpring 2]  "
            } else {
                append springsList "_NONE  "
            }
            append springsList "[lindex $lineSpring 3]  "
            set index [expr $index + 1]
        } else {
            append springsList "  \[$index\-[expr $index + [lindex $lineSpring 1] - [lindex $lineSpring 0]]\]  "
            append springsList "[lindex $lineSpring 4]  1  "
            append springsList "\[[lindex $lineSpring 0]\-[lindex $lineSpring 1]\]  "
            if { [lindex $lineSpring 2] > 0 } {
                append springsList "    [lindex $lineSpring 2]  "
            } else {
                append springsList "_NONE  "
            }
            append springsList "[lindex $lineSpring 3]  "
            set index [expr $index + [lindex $lineSpring 1] - [lindex $lineSpring 0] + 1]
        }

        if { [lindex $lineSpring 5] == "Specified" } {

            # set vector [list [lindex $lineSpring 6] [lindex $lineSpring 7] [lindex $lineSpring 8]]
            # set auxVectorName [GetAuxiliaryVectorNameFromFemixFile $vector]
            # if { $auxVectorName == "" } {
            append springsList "VECTOR_[GetAuxiliaryVectorIndexFromComponents [lindex $lineSpring 6] [lindex $lineSpring 7] [lindex $lineSpring 8]]  "
            # } else {
                # append springsList "$auxVectorName  "
            # }
            if { [lindex $lineSpring 9] == "Structure_to_Ground" } {
                append springsList "_SG  "
            } else {
                append springsList "_GS  "
            }
            if { [lindex $lineSpring 10] == "Translational" } {
                append springsList "_TRANS  "
            } else {
                append springsList "_ROT  "
            }
            append springsList "[lindex $lineSpring 11]  "
            append springsList "[lindex $lineSpring 12]  "
            append springsList "_[lindex $lineSpring 13]  "
            append springsList "[lindex $lineSpring 14]  "

            for {set j 0} {$j < [lindex $lineSpring 14]} {incr j} {
                append springsList "[lindex $lineSpring [expr 15 + $j]]  "
            }
            append springsList ";\n"

        } else {

            if { [lindex $lineSpring 5] == "Global" } {

                if { $numDimension == 3 } {
                    append springsList "_[lindex $lineSpring 6]  "
                } else {
                    append springsList "_XG[expr [string index [lindex $lineSpring 6] end] % 3 + 1]  "
                }
            
            } elseif { [lindex $lineSpring 5] == "Frame" } {

                if { $numDimension == 3 } {
                    append springsList "_[lindex $lineSpring 6]  "
                } else {
                    append springsList "_L[expr [string index [lindex $lineSpring 6] end] % 3 + 1]  "
                }

            } else {
                append springsList "_[lindex $lineSpring 6]  "
            }

            if { [lindex $lineSpring 7] == "Structure_to_Ground" } {
                append springsList "_SG  "
            } else {
                append springsList "_GS  "
            }
            if { [lindex $lineSpring 8] == "Translational" } {
                append springsList "_TRANS  "
            } else {
                append springsList "_ROT  "
            }

            append springsList "[lindex $lineSpring 9]  "
            append springsList "[lindex $lineSpring 10]  "
            append springsList "_[lindex $lineSpring 11]  "
            append springsList "[lindex $lineSpring 12]  "

            for {set j 0} {$j < [lindex $lineSpring 12]} {incr j} {
                append springsList "[lindex $lineSpring [expr 13 + $j]]  "
            }
            append springsList ";\n"
        }
    }
    return $springsList
}

# proc Remove2NodeLineSpringsConditions { conditions node1 node2 } {

#     global lineSpringsConditionsOverNodes \
#            lineSpringsNodes
    
#     set index 0
#     set index1 -1
#     set index2 -1

#     while { $index1 < 0 || $index2 < 0 } {

#         if { [lindex $lineSpringsNodes $index] == $node1 } {
#             set conditions1 [GetFirstNodeLineSpringsConditions $node1]
#             if { [EqualLineSpringConditions $conditions $conditions1] } {
#                 set index1 $index
#             }
#         } elseif { [lindex $lineSpringsNodes $index] == $node2 } {
#             set conditions2 [GetFirstNodeLineSpringsConditions $node2]
#             if { [EqualLineSpringConditions $conditions $onditions2] } {
#                 set index2 $index
#             }
#         }
#         incr index
#     }

#     set lineSpringsNodes [lreplace $lineSpringsNodes $index1 $index1]
#     set lineSpringsConditionsOverNodes [lreplace $lineSpringsConditionsOverNodes $index1 $index1]

#     if { $index2 > $index1 } {
#         set index2 [expr $index2 - 1]
#     }

#     set lineSpringsNodes [lreplace $lineSpringsNodes $index2 $index2]
#     set lineSpringsConditionsOverNodes [lreplace $lineSpringsConditionsOverNodes $index2 $index2]
# }