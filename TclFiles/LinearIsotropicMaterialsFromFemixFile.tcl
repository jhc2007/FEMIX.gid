# LinearIsotropicMaterialsFromFemixFile.tcl
#
# CreateLinearIsotropicMaterials { femixData }

proc CreateLinearIsotropicMaterials { femixData } {

    set linIsoData [GetBlockData $femixData "LINEAR_ISOTROPIC_MATERIALS"]

    if { [llength $linIsoData] > 0 } {

        global doubleFormatMatParameters
    	set allMaterialsNameList [GiD_Info materials]
    	set linIsoMaterialsNameList {}

        for {set i 0} {$i < [llength $allMaterialsNameList]} {incr i} {
            if { [lindex [GiD_Info materials [lindex $allMaterialsNameList $i]] 2] == "LIN_ISO" } {
                lappend linIsoMaterialsNameList [string toupper [lindex $allMaterialsNameList $i]]
            }
        }

        set numMaterials [lindex $linIsoData 2]

        set index 4

        for {set i 0} {$i < $numMaterials} {incr i} {

            set materialName [lindex $linIsoData [expr $index + 1]]

            set materialData { LIN_ISO }

            lappend materialData "[format $doubleFormatMatParameters [lindex $linIsoData [expr $index + 2]]]"
            # kg*mm^-3"
            lappend materialData "[format $doubleFormatMatParameters [lindex $linIsoData [expr $index + 3]]]" 
            # Cel^-1"
            lappend materialData "[format $doubleFormatMatParameters [lindex $linIsoData [expr $index + 4]]]"
             # N*mm^-2"
            lappend materialData "[format $doubleFormatMatParameters [lindex $linIsoData [expr $index + 5]]]"

            if { [ExistsListElement $linIsoMaterialsNameList [string toupper $materialName]] } {
                GiD_ModifyData materials $materialName $materialData
            } else {
                GiD_CreateData create material LIN_ISO_Concrete $materialName $materialData
            }

            set index [expr $index + 7]
        }
    }
}