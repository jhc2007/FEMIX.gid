# ElementPropertiesFromFemixFile.tcl
#
# AssignElementProperties { femixFile }
# CreateLayerPatternsFile { femixData }
# CreateLayerPropertiesFile { femixData lamThickData geomPattData }
# GetGeometryValues { geometryData geometryName numValues }
# GetGeometryPatternsValues { geomPattData geometryName }
# AssignNumericalIntegrationValues { integrationData integrationName }

proc AssignElementProperties { femixData } {

    # set femixFile [open "$femixFile" "r"]
    # set femixData [read $femixFile]
    # close $femixFile

    global NUMERICALINTEGRATIONLIST

    set firstLamThick yes
    set firstCsArea yes
    set firstCs2d yes
    set firstCs3d yes
    set firstGeomPatt yes
    set firstNumericalIntegration yes
    set existsLayerPatterns no

    if { [lindex [GiD_Info gendata] 4] == 1 } {
        set nonLinearProblem yes
    } else {
        set nonLinearProblem no 
    }

    set index 0

    while { $index < [llength $femixData] && [lindex $femixData $index] != "<ELEMENT_PROPERTIES>" } {
        incr index
    }

    while { $index < [llength $femixData] && ![string is integer [lindex $femixData $index]] } {
        incr index
    }

    set numLayers [lindex $femixData $index]

    incr index

    while { $index < [llength $femixData] && ![string is integer [lindex $femixData $index]] } {
        incr index
    }

    for {set i 0} {$i < $numLayers} {incr i} {

        incr index
        set layerName [lindex $femixData $index]

        # if { [ExistsListElement [GiD_Info Layers] $layerName ] } {
        #     append layerName "_[expr $i + 1]"
        # } 
        
        if { $i == 0 } {
            if { $layerName != "Layer0" } {
                GiD_Process Layers ChangeName Layer0 $layerName escape
            }
            set NUMERICALINTEGRATIONLIST {}
            # CreateNumericalIntegrationList
        } elseif { ![ExistsListElement [GiD_Info Layers] $layerName] } {
            GiD_Process Layers New $layerName escape  
        }

        incr index 2
        set rangeList [GetFirstLastValuesFromRange [lindex $femixData $index]]

        set firstElem [lindex $rangeList 0]
        set lastElem [lindex $rangeList 1]

        GiD_Process Layers Entities $layerName LowerEntities Elements $firstElem:$lastElem Mescape

        # puts $result "old: $i"
        # puts $result [lindex [GiD_Info conditions Element_Properties mesh] $i]

        # set elemPropValues {}
        # lappend elemPropValues [lindex [GiD_Info conditions Element_Properties mesh] $i]
        # set elemPropValues [lindex [GiD_Info conditions Element_Properties mesh] $i]
        # # set elemPropValues [lreplace $elemPropValues 3 3 [lindex [GiD_Info gendata] 4]]

        # GiD_AssignData condition Element_Properties layers $elemPropValues $layerName

        # puts $result "new: $i"
        # puts $result [lindex [GiD_Info conditions Element_Properties mesh] $i]

        # set allElem {}
        #for {set j $firstElem} {$j <= $lastElem} {incr j} {
            # lappend allElem $j
            #GiD_Process Layers Entities $layerName LowerEntities Elements $j escape Mescape
        #}

        # GiD_Process Layers Entities $layerName LowerEntities Elements [split $allElem] escape Mescape

        incr index

        # set elementType [lindex $femixData $index]
        # set elementType [string range $elementType 1 [expr [string length $elementType] - 1]]
        set elementType [RemoveFirstUnderScore [lindex $femixData $index]]

        if { $nonLinearProblem } {
            GiD_AccessValue set conditions Element_Properties _Element_Type $elementType
        } else {
            GiD_AccessValue set conditions Element_Properties Element_Type $elementType
        }

        incr index 2                        

        set materialName [lindex $femixData $index]

        if { [lindex $femixData [expr $index - 1]] == "_LAY_PATT" } {
            set layeredConfiguration yes
        } else {
            set layeredConfiguration no
        }

        if { $layeredConfiguration } {
            set existsLayerPatterns yes
            GiD_AccessValue set conditions Element_Properties Layered_Configuration 1
            GiD_AccessValue set conditions Element_Properties Layer_Pattern_Name $materialName
        } else {
            GiD_AccessValue set conditions Element_Properties Layered_Configuration 0
            GiD_Process Data Materials AssignMaterial $materialName $firstElem:$lastElem Mescape
        }

        incr index                        
        set geometryType [lindex $femixData $index]

        incr index                        
        set geometryName [lindex $femixData $index]

        if { $geometryType == "_NONE" } {
            
            GiD_AccessValue set conditions Element_Properties Geometry_Type "CROSS_SECTION_AREAS"
            GiD_AccessValue set conditions Element_Properties Area 0

        } else {

            switch $geometryType {

                "_CS_AREA" {

                    set parameters { "Area" }

                    GiD_AccessValue set conditions Element_Properties Geometry_Type "CROSS_SECTION_AREAS"
                    
                    if { $firstCsArea } {
                        set csAreaData [GetBlockData $femixData "CROSS_SECTION_AREAS"]
                        set firstCsArea no
                    }
                    
                    set values [GetGeometryValues $csAreaData $geometryName [llength $parameters]]
                    
                    # GiD_AccessValue set conditions Element_Properties Area [lindex $values 0]
                }
                "_CS_2D" {

                    set parameters { "Area" \
                                     "Shear_Factor" \
                                     "Moment_of_Inertia" \
                                     "Cross_Section_Height" }

                    GiD_AccessValue set conditions Element_Properties Geometry_Type "FRAME_2D_CROSS_SECTIONS"

                    if { $firstCs2d } {
                        set cs2dData [GetBlockData $femixData "FRAME_2D_CROSS_SECTIONS"]
                        set firstCs2d no
                    }

                    set values [GetGeometryValues $cs2dData $geometryName [llength $parameters]]
                    # GiD_AccessValue set conditions Element_Properties Area [lindex $values 0]
                    # GiD_AccessValue set conditions Element_Properties Shear_Factor [lindex $values 1]
                    # GiD_AccessValue set conditions Element_Properties Moment_of_Inertia [lindex $values 2]
                    # GiD_AccessValue set conditions Element_Properties Cross_Section_Height [lindex $values 3]
                }
                "_CS_3D" {

                    set parameters { "Area" \
                                     "Shear_Factor_l2" \
                                     "Shear_Factor_l3" \
                                     "Torsional_Constant" \
                                     "Moment_of_Inertia_l2" \
                                     "Moment_of_Inertia_l3" \
                                     "Local_Coord._of_the_Shear_Center_l2" \
                                     "Local_Coord._of_the_Shear_Center_l3" \
                                     "Cross_Section_Height_l2" \
                                     "Cross_Section_Height_l3" \
                                     "Angle" }

                    GiD_AccessValue set conditions Element_Properties Geometry_Type "FRAME_3D_CROSS_SECTIONS"
                    
                    if { $firstCs3d } {
                        set cs3dData [GetBlockData $femixData "FRAME_3D_CROSS_SECTIONS"]
                        set firstCs3d no
                    }
                    
                    set values [GetGeometryValues $cs3dData $geometryName [llength $parameters]]

                    # GiD_AccessValue set conditions Element_Properties Area [lindex $values 0]
                    # GiD_AccessValue set conditions Element_Properties Shear_Factor_l2 [lindex $values 1]
                    # GiD_AccessValue set conditions Element_Properties Shear_Factor_l3 [lindex $values 2]
                    # GiD_AccessValue set conditions Element_Properties Torsional_Constant [lindex $values 3]
                    # GiD_AccessValue set conditions Element_Properties Moment_of_Inertia_l2 [lindex $values 4]
                    # GiD_AccessValue set conditions Element_Properties Moment_of_Inertia_l3 [lindex $values 5]
                    # GiD_AccessValue set conditions Element_Properties Local_Coord._of_the_Shear_Center_l2 [lindex $values 6]
                    # GiD_AccessValue set conditions Element_Properties Local_Coord._of_the_Shear_Center_l3 [lindex $values 7]
                    # GiD_AccessValue set conditions Element_Properties Cross_Section_Height_l2 [lindex $values 8]
                    # GiD_AccessValue set conditions Element_Properties Cross_Section_Height_l3 [lindex $values 9]
                    # GiD_AccessValue set conditions Element_Properties Angle [lindex $values 10]
                }
                "_LAM_THICK" {

                    set parameters { "Thickness" }

                    GiD_AccessValue set conditions Element_Properties Geometry_Type "LAMINATE_THICKNESSES"
                    
                    if { $firstLamThick } {
                        set lamThickData [GetBlockData $femixData "LAMINATE_THICKNESSES"]
                        set firstLamThick no
                    }
                    
                    set values [GetGeometryValues $lamThickData $geometryName [llength $parameters]]
                    
                    #GiD_AccessValue set conditions Element_Properties Thickness [lindex $values 0]
                }
                "_GEOM_PATT" {

                    GiD_AccessValue set conditions Element_Properties Geometry_Type "GEOMETRY_PATTERNS"

                    if { $firstGeomPatt } {
                        set geomPattData [GetBlockData $femixData "GEOMETRY_PATTERNS"]
                        set firstGeomPatt no
                    }

                    set geomPattValues [GetGeometryPatternsValues $geomPattData $geometryName]

                    switch [lindex $geomPattValues 0] {

                        "_CS_AREA" {
                            set parameters { "Area_[1]" "Area_[2]" }
                            set values {}
                            GiD_AccessValue set conditions Element_Properties Geometry_Patterns_Type "CROSS_SECTION_AREAS"
                            if { $firstCsArea } {
                                set csAreaData [GetBlockData $femixData "CROSS_SECTION_AREAS"]
                                set firstCsArea no
                            }
                            for {set j 0} {$j < [lindex $geomPattValues 1]} {incr j} {
                                lappend values [lindex [GetGeometryValues $csAreaData [lindex $geomPattValues [expr 2 + $j]] 1] 0]
                            }                     
                        }
                        "_CS_2D" {
                            set parameters { "Area_[1]" \
                                             "Shear_Factor_[1]" \
                                             "Moment_of_Inertia_[1]" \
                                             "Cross_Section_Height_[1]" \
                                             "Area_[2]" \
                                             "Shear_Factor_[2]" \
                                             "Moment_of_Inertia_[2]" \
                                             "Cross_Section_Height_[2]" }
                            set values {}
                            GiD_AccessValue set conditions Element_Properties Geometry_Patterns_Type "FRAME_2D_CROSS_SECTIONS"
                            if { $firstCs2d } {
                                set cs2dData [GetBlockData $femixData "FRAME_2D_CROSS_SECTIONS"]
                                set firstCs2d no
                            }
                            for {set j 0} {$j < [lindex $geomPattValues 1]} {incr j} {
                                set val [GetGeometryValues $cs2dData [lindex $geomPattValues [expr 2 + $j]] 4]
                                for {set k 0} {$k < [llength $val]} {incr k} {
                                    lappend values [lindex $val $k]                                    
                                }
                            }   
                        }
                        "_CS_3D" {
                            set parameters { "Area_[1]" \
                                             "Shear_Factor_l2_[1]" \
                                             "Shear_Factor_l3_[1]" \
                                             "Torsional_Constant_[1]" \
                                             "Moment_of_Inertia_l2_[1]" \
                                             "Moment_of_Inertia_l3_[1]" \
                                             "Local_Coord._of_the_Shear_Center_l2_[1]" \
                                             "Local_Coord._of_the_Shear_Center_l3_[1]" \
                                             "Cross_Section_Height_l2_[1]" \
                                             "Cross_Section_Height_l3_[1]" \
                                             "Angle_[1]" \
                                             "Area_[2]" \
                                             "Shear_Factor_l2_[2]" \
                                             "Shear_Factor_l3_[2]" \
                                             "Torsional_Constant_[2]" \
                                             "Moment_of_Inertia_l2_[2]" \
                                             "Moment_of_Inertia_l3_[2]" \
                                             "Local_Coord._of_the_Shear_Center_l2_[2]" \
                                             "Local_Coord._of_the_Shear_Center_l3_[2]" \
                                             "Cross_Section_Height_l2_[2]" \
                                             "Cross_Section_Height_l3_[2]" \
                                             "Angle_[2]" }
                            set values {}
                            GiD_AccessValue set conditions Element_Properties Geometry_Patterns_Type "FRAME_3D_CROSS_SECTIONS"
                            if { $firstCs3d } {
                                set cs3dData [GetBlockData $femixData "FRAME_3D_CROSS_SECTIONS"]
                                set firstCs3d no
                            }
                            for {set j 0} {$j < [lindex $geomPattValues 1]} {incr j} {
                                set val [GetGeometryValues $cs3dData [lindex $geomPattValues [expr 2 + $j]] 11]
                                for {set k 0} {$k < [llength $val]} {incr k} {
                                    lappend values [lindex $val $k]                                    
                                }
                            } 
                        }
                        "_LAM_THICK" {
                            set parameters { "Thickness_[1]" \
                                             "Thickness_[2]" \
                                             "Thickness_[3]" \
                                             "Thickness_[4]" }
                            if { [lindex $geomPattValues 1] == 8 } {
                                set parameters [concat $parameters { "Thickness_[5]" \
                                                                     "Thickness_[6]" \
                                                                     "Thickness_[7]" \
                                                                     "Thickness_[8]" } ]
                            }
                            set values {}
                            GiD_AccessValue set conditions Element_Properties Geometry_Patterns_Type "LAMINATE_THICKNESSES"
                            if { $firstLamThick } {
                                set lamThickData [GetBlockData $femixData "LAMINATE_THICKNESSES"]
                                set firstLamThick no
                            }
                            for {set j 0} {$j < [lindex $geomPattValues 1]} {incr j} {
                                lappend values [lindex [GetGeometryValues $lamThickData [lindex $geomPattValues [expr 2 + $j]] 1] 0]
                            }  
                        }
                    }
                }
            }
            for {set j 0} {$j < [llength $parameters]} {incr j} {
                GiD_AccessValue set conditions Element_Properties [lindex $parameters $j] [lindex $values $j]
            }

        }

        incr index                        
        set integrationType [lindex $femixData $index]

        incr index                        
        set integrationName [lindex $femixData $index]

        if { $integrationType == "_NONE" } {

            GiD_AccessValue set conditions Element_Properties Number_of_Keywords 0

        } else {

            if { $layeredConfiguration } {
                switch $integrationType {

                    "_GLEG" {
                        GiD_AccessValue set conditions Element_Properties _Integration_Type "GAUSS-LEGENDRE"
                    }
                    "_GLOB" {
                        GiD_AccessValue set conditions Element_Properties _Integration_Type "GAUSS-LOBATTO"
                    }
                    "_NCOTES" {
                        GiD_AccessValue set conditions Element_Properties _Integration_Type "NEWTON-COTES"
                    }
                }
                
                GiD_AccessValue set conditions Element_Properties _Integration_Name $integrationName
            } else {
                switch $integrationType {

                    "_GLEG" {
                        GiD_AccessValue set conditions Element_Properties Integration_Type "GAUSS-LEGENDRE"
                    }
                    "_GLOB" {
                        GiD_AccessValue set conditions Element_Properties Integration_Type "GAUSS-LOBATTO"
                    }
                    "_NCOTES" {
                        GiD_AccessValue set conditions Element_Properties Integration_Type "NEWTON-COTES"
                    }
                }
                
                GiD_AccessValue set conditions Element_Properties Integration_Name $integrationName
            }


            if { $firstNumericalIntegration } {
                set integrationData [GetBlockData $femixData "NUMERICAL_INTEGRATION"]
                set firstNumericalIntegration no
            }

            AssignNumericalIntegrationValues $integrationData $integrationName

            AddNumericalIntegration [expr $i + 1] $integrationName
        }

        GiD_AssignData condition Element_Properties layers "" $layerName
        
        incr index 2

        # puts $result $firstElem 
        # puts $result $lastElem
        # puts $result $allElem
        # puts $result $layerName

        # puts $result "Element_Properties: $i"
        # puts $result [lindex [GiD_Info conditions Element_Properties mesh] $i]
        

    }

    # GiD_Process Layers Delete Layer0 escape

    if { $nonLinearProblem } {
        GiD_Process Data Conditions AssignCond Element_Properties ReplaceField Non_Linear_Problem#CB#(0,1) 0 1
    }
    # puts $result $mainParametersData
    # puts $result [GiD_Info conditions Element_Properties mesh]
    #puts $result "AQUI:"
    # puts $result "NONLINEAR:"
    # puts $result [lindex [GiD_Info gendata] 4]
    # # puts $result [GiD_Info conditions Element_Properties mesh]
    # close $result

    # puts $result [GiD_Info Layers]
    # close $result
    if { $existsLayerPatterns } {
        if { $firstLamThick } {
            set lamThickData [GetBlockData $femixData "LAMINATE_THICKNESSES"]
        }
        if { $firstGeomPatt } {
            set geomPattData [GetBlockData $femixData "GEOMETRY_PATTERNS"]
        }
        CreateLayerPropertiesFile $femixData $lamThickData $geomPattData
        CreateLayerPatternsFile $femixData
    }
    # destroy $femixFile $femixData
}

proc CreateLayerPatternsFile { femixData } {

    global PROBLEMTYPEPATH

    set index 2

    set layerPattData [GetBlockData $femixData "LAYER_PATTERNS"]

    set layerPattFile [open "$PROBLEMTYPEPATH\\TmpFiles\\femix_layer_patterns.dat" "w"]

    set numLayerPatt [lindex $layerPattData $index]

    incr index 2

    for {set i 0} {$i < $numLayerPatt} {incr i} {
        puts $layerPattFile [lindex $layerPattData [expr $index + 1]]
        set index [expr $index + 3]
        set strRange [lindex $layerPattData $index]
        while { $strRange != ";" } {
            set rangeList [GetFirstLastValuesFromRange $strRange]
            set firstLayer [lindex $rangeList 0]
            set lastLayer [lindex $rangeList 1]
            
            # if { [string is integer $strRange] } {
            #     set firstLayer $strRange
            #     set lastLayer  $strRange
            # } else {
            #     set firstLayer [string range $strRange 1 [expr [string first "-" $strRange] - 1]]
            #     set lastLayer  [string range $strRange [expr [string first "-" $strRange] + 1] [expr [string length $strRange] - 2]]
            # }
            puts $layerPattFile $firstLayer
            puts $layerPattFile $lastLayer
            puts $layerPattFile [lindex $layerPattData [expr $index + 1]]
            incr index 2
            set strRange [lindex $layerPattData $index]
        }
        incr index
    }

    close $layerPattFile
}

proc CreateLayerPropertiesFile { femixData lamThickData geomPattData } {

    global PROBLEMTYPEPATH

    set index 2

    set layerPropData [GetBlockData $femixData "LAYER_PROPERTIES"]

    set layerPropFile [open "$PROBLEMTYPEPATH\\TmpFiles\\femix_layer_properties.dat" "w"]

    set numLayerProp [lindex $layerPropData $index]

    incr index 2

    for {set i 0} {$i < $numLayerProp} {incr i} {
        set layerPropName [lindex $layerPropData [expr $index + 1]]
        set materialType  [lindex $layerPropData [expr $index + 2]]
        set materialType  [string range $materialType 1 [expr [string length $materialType] - 1]]
        set materialName  [lindex $layerPropData [expr $index + 3]]
        set geometryType  [lindex $layerPropData [expr $index + 4]]
        switch $geometryType {
            "_NONE" {
                puts $layerPropFile "$layerPropName $materialType $materialName NONE"
                set index [expr $index + 6]
            }
            "_LAM_THICK" {
                # set geometryType "LAMINATE_THICKNESSES"
                set geometryName [lindex $layerPropData [expr $index + 5]]
                set thickness [lindex [GetGeometryValues $lamThickData $geometryName 1] 0]
                puts $layerPropFile "$layerPropName $materialType $materialName LAMINATE_THICKNESSES $thickness"
                set index [expr $index + 7]
            }
            "_GEOM_PATT" {
                set geometryName [lindex $layerPropData [expr $index + 5]]
                set geomPattValues [GetGeometryPatternsValues $geomPattData $geometryName]
                set line "$layerPropName $materialType $materialName GEOMETRY_PATTERNS [lindex $geomPattValues 1]"
                for {set j 0} {$j < [lindex $geomPattValues 1]} {incr j} {
                    append line " " [lindex [GetGeometryValues $lamThickData [lindex $geomPattValues [expr 2 + $j]] 1] 0]
                } 
                puts $layerPropFile $line
                set index [expr $index + 7]
            }
        }    
    }

    close $layerPropFile
}

proc GetGeometryValues { geometryData geometryName numValues } {

    set values {}
    set index 0

    while { $index < [llength $geometryData] && [lindex $geometryData $index] != $geometryName } {
        incr index
    }

    if { $index < [llength $geometryData] } {
        for {set i 1} {$i <= $numValues} {incr i} {
            lappend values [lindex $geometryData [expr $index + $i]]
        }
    } 
    return $values
}

proc GetGeometryPatternsValues { geomPattData geometryName } {

    set values {}
    set index 0

    while { $index < [llength $geomPattData] && [lindex $geomPattData $index] != $geometryName } {
        incr index
    }

    if { $index < [llength $geomPattData] } {
        lappend values [lindex $geomPattData [expr $index + 1]]
        set numNodes [lindex $geomPattData [expr $index + 2]]
        lappend values $numNodes
        for {set i 0} {$i < $numNodes} {incr i} {
            lappend values [lindex $geomPattData [expr $index + 3 + $i]]
        }
    } 
    return $values
}

proc AssignNumericalIntegrationValues { integrationData integrationName } {

    # set values {}

    # global PROBLEMTYPEPATH
    # set result [open "$PROBLEMTYPEPATH\\result.dat" "w"]

    set index 0

    while { $index < [llength $integrationData] && [lindex $integrationData $index] != $integrationName } {
        incr index
    }

    if { $index < [llength $integrationData] } {
        set numKey [lindex $integrationData [expr $index + 1]]
        GiD_AccessValue set conditions Element_Properties Number_of_Keywords $numKey
        for {set i 1} {$i <= $numKey} {incr i} {
            set key [lindex $integrationData [expr $index + 1 + $i]]

            # puts $result $key
            # puts $result [string index $key 1]
            switch [string index $key 1] {
                "G" {
                    GiD_AccessValue set conditions Element_Properties Type_of_Terms_\[$i\] "General"
                }
                "M" {
                    GiD_AccessValue set conditions Element_Properties Type_of_Terms_\[$i\] "Membrane"
                }
                "S" {
                    GiD_AccessValue set conditions Element_Properties Type_of_Terms_\[$i\] "Shear"
                }
                "T" {
                    GiD_AccessValue set conditions Element_Properties Type_of_Terms_\[$i\] "Torsional"
                }
                "B" {
                    GiD_AccessValue set conditions Element_Properties Type_of_Terms_\[$i\] "Bending"
                }
            }

            GiD_AccessValue set conditions Element_Properties Direction_\[$i\] "s[string index $key 4]"

            if { [string length $key] > 7 } {
                GiD_AccessValue set conditions Element_Properties Number_of_Points_\[$i\] 10
            } else {
                GiD_AccessValue set conditions Element_Properties Number_of_Points_\[$i\] [string index $key 6]
            }

        }
    }
    # close $result
    # return $values
}

# proc GetAreaOrThickness { geometryData  geometryName } {

#     set index 0

#     while { $index < [llength $geometryData] && [lindex $geometryData $index] != $geometryName } {
#         incr index
#     }

#     if { $index < [llength $geometryData] } {
#         return [lindex $geometryData [expr $index + 1]]
#     } 

#     return 0   
# }

# proc GetCs2dValues { cs2dData geometryName } {

#     set value {}
#     set index 0

#     while { $index < [llength $cs2dData] && [lindex $cs2dData $index] != $geometryName } {
#         incr index
#     }

#     if { $index < [llength $cs2dData] } {
#         for {set i 1} {$i < 5} {incr i} {
#             lappend value [lindex $cs2dData [expr $index + $i]]
#         }
#     } 
#     return $value
# }