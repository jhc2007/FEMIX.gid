# NLMM111FromFemixFile.tcl

proc CreateNLMM111 { femixData } {

    set nlmm111Data [GetBlockData $femixData "NLMM111"]

    if { [llength $nlmm111Data] > 0 } {

        global doubleFormatMatParameters
        set allMaterialsNameList [GiD_Info materials]
        set nlmm111NameList {}

        for {set i 0} {$i < [llength $allMaterialsNameList]} {incr i} {
            if { [lindex [GiD_Info materials [lindex $allMaterialsNameList $i]] 2] == "NLMM111" } {
                lappend nlmm111NameList [string toupper [lindex $allMaterialsNameList $i]]
            }
        }

    	set numMaterials [lindex $nlmm111Data 2]

    	set index 4

    	for {set i 0} {$i < $numMaterials} {incr i} {

    		set materialName [lindex $nlmm111Data [expr $index + 1]]

    		set materialData { NLMM111 }

    		# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm111Data [expr $index + 2]]] kg*mm^-3"
	    	# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm111Data [expr $index + 3]]] Cel^-1"
            lappend materialData [format $doubleFormatMatParameters [lindex $nlmm111Data [expr $index + 2]]]
            lappend materialData [format $doubleFormatMatParameters [lindex $nlmm111Data [expr $index + 3]]]
	    	lappend materialData [format $doubleFormatMatParameters [lindex $nlmm111Data [expr $index + 4]]]

	        for {set j 5} {$j < 8} {incr j} {
    			# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm111Data [expr $index + $j]]] N*mm^-2"
                lappend materialData [format $doubleFormatMatParameters [lindex $nlmm111Data [expr $index + $j]]]
    		}

    		set value [RemoveFirstUnderScore [lindex $nlmm111Data [expr $index + 8]]]
    		lappend materialData $value

    		for {set j 9} {$j < 15} {incr j} {
    			lappend materialData [format $doubleFormatMatParameters [lindex $nlmm111Data [expr $index + $j]]]
    		}

    		switch $value {

    			"TRILINEAR" {
    				# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm111Data [expr $index + 15]]] N*mm^-1"
    				# lappend materialData "0 N*mm^-1"
    				# lappend materialData "0 N*mm^-1"
                    lappend materialData [format $doubleFormatMatParameters [lindex $nlmm111Data [expr $index + 15]]]
                    lappend materialData 0
                    lappend materialData 0
    			}
    			"QUADRILINEAR" {
    				# lappend materialData "0 N*mm^-1"
    				# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm111Data [expr $index + 15]]] N*mm^-1"
    				# lappend materialData "0 N*mm^-1"
                    lappend materialData 0
                    lappend materialData [format $doubleFormatMatParameters [lindex $nlmm111Data [expr $index + 15]]]
                    lappend materialData 0
    			}
    			"CORNELISSEN" {
    				# lappend materialData "0 N*mm^-1"
    				# lappend materialData "0 N*mm^-1"
    				# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm111Data [expr $index + 15]]] N*mm^-1"
                    lappend materialData 0
                    lappend materialData 0
                    lappend materialData [format $doubleFormatMatParameters [lindex $nlmm111Data [expr $index + 15]]]
    			}
    		}

    		lappend materialData [format $doubleFormatMatParameters [lindex $nlmm111Data [expr $index + 16]]]

    		set value [RemoveFirstUnderScore [lindex $nlmm111Data [expr $index + 17]]]
    		lappend materialData $value

    		for {set j 18} {$j < 22} {incr j} {
    			lappend materialData [format $doubleFormatMatParameters [lindex $nlmm111Data [expr $index + $j]]]
    		}

    		if { [string is double [lindex $nlmm111Data [expr $index + 22]]] } {
    			lappend materialData "VALUE"
    			lappend materialData [format $doubleFormatMatParameters [lindex $nlmm111Data [expr $index + 22]]]
    		} else {
    			lappend materialData [RemoveFirstUnderScore [lindex $nlmm111Data [expr $index + 22]]]
    			lappend materialData 0
    		}

    		lappend materialData [RemoveFirstUnderScore [lindex $nlmm111Data [expr $index + 23]]]
    		# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm111Data [expr $index + 24]]] N*mm^-2"
            lappend materialData [format $doubleFormatMatParameters [lindex $nlmm111Data [expr $index + 24]]]

    		switch $value {

    			"NONE" {
    				# lappend materialData "0 N*mm^-1"
    				# lappend materialData "0 N*mm^-1"
    				# lappend materialData "0 N*mm^-1"
                    lappend materialData 0
                    lappend materialData 0
                    lappend materialData 0
    			}
    			"LINEAR" {
    				# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm111Data [expr $index + 25]]] N*mm^-1"
    				# lappend materialData "0 N*mm^-1"
    				# lappend materialData "0 N*mm^-1"
                    lappend materialData [format $doubleFormatMatParameters [lindex $nlmm111Data [expr $index + 25]]]
                    lappend materialData 0
                    lappend materialData 0
    			}
    			"TRILINEAR" {
    				# lappend materialData "0 N*mm^-1"
    				# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm111Data [expr $index + 25]]] N*mm^-1"
    				# lappend materialData "0 N*mm^-1"
                    lappend materialData 0
                    lappend materialData [format $doubleFormatMatParameters [lindex $nlmm111Data [expr $index + 25]]]
                    lappend materialData 0
    			}
    			"CORNELISSEN" {
    				# lappend materialData "0 N*mm^-1"
    				# lappend materialData "0 N*mm^-1"
    				# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm111Data [expr $index + 25]]] N*mm^-1"
                    lappend materialData 0
                    lappend materialData 0
                    lappend materialData [format $doubleFormatMatParameters [lindex $nlmm111Data [expr $index + 25]]]
    			}
    		}

    		lappend materialData [RemoveFirstUnderScore [lindex $nlmm111Data [expr $index + 26]]]

    		if { [string is double [lindex $nlmm111Data [expr $index + 27]]] } {
    			lappend materialData "VALUE"
    			# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm111Data [expr $index + 27]]] mm"
                lappend materialData [format $doubleFormatMatParameters [lindex $nlmm111Data [expr $index + 27]]]
    		} else {
    			lappend materialData [RemoveFirstUnderScore [lindex $nlmm111Data [expr $index + 27]]]
    			# lappend materialData "0 mm"
                lappend materialData 0
    		}

    		lappend materialData [lindex $nlmm111Data [expr $index + 28]]
    		# lappend materialData "[lindex $nlmm111Data [expr $index + 29]] degree"
            lappend materialData [lindex $nlmm111Data [expr $index + 29]]

    		if { [ExistsListElement $nlmm111NameList [string toupper $materialName]] } {
	    		GiD_ModifyData materials $materialName $materialData
	    	} else {
	    		GiD_CreateData create material NLMM111_Concrete $materialName $materialData
	    	}

        	set index [expr $index + 31]    
    	}

    }
}