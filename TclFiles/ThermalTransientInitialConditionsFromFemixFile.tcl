# ThermalTransientInitialConditionsFromFemixFile.tcl

proc AssignThermalTransientInitialConditions { femixData } {

	set thermalTransientInitialConditionsData [GetBlockData $femixData "THERMAL_TRANSIENT_INITIAL_CONDITIONS"]

	if { [llength $thermalTransientInitialConditionsData] > 0 } {

		set index 5

		while { $index < [llength $thermalTransientInitialConditionsData] } {

			set rangeList [GetFirstLastValuesFromRange [lindex $thermalTransientInitialConditionsData $index]]
			set firstPoint [lindex $rangeList 0]
	        set lastPoint [lindex $rangeList 1]

	        incr index

	        set temperature [lindex $thermalTransientInitialConditionsData $index]

    		GiD_Process Mescape Data \
		        Conditions AssignCond _Thermal_Transient_Initial_Conditions Change $temperature \
		        $firstPoint:$lastPoint Mescape 

	        incr index 3
	    }
	}
}