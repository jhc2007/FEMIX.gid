# NumericalIntegration.tcl
#
# CreateNumericalIntegrationList {}
# AddNumericalIntegration { layer name }
# GetNumericalIntegrationName { num }
# GetNumericalIntegrationLayerNumber { num }

# proc CreateNumericalIntegrationList {} {
    
#     global NUMERICALINTEGRATIONLIST
#     set NUMERICALINTEGRATIONLIST {}
#     return 0
# }

# proc PrintNumericalIntegrationList {} {

#     global NUMERICALINTEGRATIONLIST

#     set nimInt ""

#     for {set i 0} {$i < [llength $NUMERICALINTEGRATIONLIST]} {incr i} {
#         append nimInt "[lindex $NUMERICALINTEGRATIONLIST $i] "
#     }
#     return $nimInt
# }

proc AddNumericalIntegration { layerNum name } {

    # layerNum > 0 for NumericalIntegration defined from Element_Properties
    # layerNum < 0 for NumericalIntegration defined from Interval_Data (Load Cases)
    # layerNum = 0 for NumericalIntegration defined from Line_Springs ans Surface_Springs

    global NUMERICALINTEGRATIONLIST

    set name [ReplaceSpaceWithUnderscore $name]
    set numNumericalIntegration [expr [llength $NUMERICALINTEGRATIONLIST] / 2]
    for {set i 0} {$i < $numNumericalIntegration} {incr i} {
    	if { [lindex $NUMERICALINTEGRATIONLIST [expr $i * 2 + 1]] == $name } {
    	    return $numNumericalIntegration
    	}
    }
    lappend NUMERICALINTEGRATIONLIST $layerNum
    lappend NUMERICALINTEGRATIONLIST $name
    
    return [expr [llength $NUMERICALINTEGRATIONLIST] / 2]
}

proc GetNumericalIntegrationName { num } {

    global NUMERICALINTEGRATIONLIST
    set numNumericalIntegration [expr [llength $NUMERICALINTEGRATIONLIST] / 2]
    for {set i 0} {$i < $numNumericalIntegration} {incr i} {
    	if {$i == $num} {
    	    return [lindex $NUMERICALINTEGRATIONLIST [expr 2 * $i + 1]]
    	}
    }
}

proc GetNumericalIntegrationLayerNumber { num } {

    global NUMERICALINTEGRATIONLIST
    set numNumericalIntegration [expr [llength $NUMERICALINTEGRATIONLIST] / 2]
    for {set i 0} {$i < $numNumericalIntegration} {incr i} {
    	if {$i == $num} {
    	    return [lindex $NUMERICALINTEGRATIONLIST [expr 2 * $i]]
    	}
    }
}