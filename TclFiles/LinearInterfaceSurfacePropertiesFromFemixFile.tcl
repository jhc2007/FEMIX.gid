
proc CreateLinIntSurface { femixData } {

    set linIntSurfaceData [GetBlockData $femixData "LINEAR_INTERFACE_SURFACE_PROPERTIES"]

    if { [llength $linIntSurfaceData] > 0 } {

        global doubleFormatMatParameters
        
        set allMaterialsNameList [GiD_Info materials]
        set linIntSurfaceNameList {}

        for {set i 0} {$i < [llength $allMaterialsNameList]} {incr i} {
            if { [lindex [GiD_Info materials [lindex $allMaterialsNameList $i]] 2] == "LIN_INT_SURF" } {
                lappend linIntSurfaceNameList [string toupper [lindex $allMaterialsNameList $i]]
            }
        }

    	set numMaterials [lindex $linIntSurfaceData 2]

    	set index 4

    	for {set i 0} {$i < $numMaterials} {incr i} {

    		set materialName [lindex $linIntSurfaceData [expr $index + 1]]

    		set materialData { LIN_INT_SURF }

    		lappend materialData "[format $doubleFormatMatParameters [lindex $linIntSurfaceData [expr $index + 2]]]"
	    	lappend materialData "[format $doubleFormatMatParameters [lindex $linIntSurfaceData [expr $index + 3]]]"
            lappend materialData "[format $doubleFormatMatParameters [lindex $linIntSurfaceData [expr $index + 4]]]"

	    	if { [ExistsListElement $linIntSurfaceNameList [string toupper $materialName]] } {
	    		GiD_ModifyData materials $materialName $materialData
	    	} else {
	    		GiD_CreateData create material Linear_Interface_Surface $materialName $materialData
	    	}

        	set index [expr $index + 6]  
	    }
	}
}
