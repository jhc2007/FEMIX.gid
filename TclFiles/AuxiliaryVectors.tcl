proc AddAuxiliaryVector { x y z } {

    global AuxiliaryVectorsList

    if { [expr abs($x)] < 1e-10 } {
        set x 0
    }
        if { [expr abs($y)] < 1e-10 } {
        set y 0
    }
        if { [expr abs($z)] < 1e-10 } {
        set z 0
    }

    set vector [list [format %.5e $x] \
                     [format %.5e $y] \
                     [format %.5e $z]]

    if { ![ExistsListElement $AuxiliaryVectorsList $vector] } {
        lappend AuxiliaryVectorsList $vector
    }

    return 0
}

proc GetAuxiliaryVectorIndexFromComponents { x y z } {

    global AuxiliaryVectorsList

    if { [expr abs($x)] < 1e-10 } {
        set x 0
    }
        if { [expr abs($y)] < 1e-10 } {
        set y 0
    }
        if { [expr abs($z)] < 1e-10 } {
        set z 0
    }

    set vector [list [format %.5e $x] \
                     [format %.5e $y] \
                     [format %.5e $z]]

    # return [GetAuxiliaryVectorIndex $vector]

    for {set i 0} {$i < [llength $AuxiliaryVectorsList]} {incr i} {
        if { [lindex $AuxiliaryVectorsList $i] == $vector } {
            return [expr $i + 1]
        }     
    }    
    
    return 0
}

# proc GetAuxiliaryVectorIndex { vector } {

#     global AuxiliaryVectorsList

#     for {set i 0} {$i < [llength $AuxiliaryVectorsList]} {incr i} {
#         if { [lindex $AuxiliaryVectorsList $i] == $vector } {
#             return [expr $i + 1]
#         }     
#     }    
#     return 0
# }

proc CountAuxiliaryVectors {} {

	global AuxiliaryVectorsList
    return [llength $AuxiliaryVectorsList]
}

proc WriteAuxiliaryVectorsList { } {

	global AuxiliaryVectorsList \
           numDimension

    set index 1
    set vectorList ""

    if { $numDimension == 3 } {
        for {set i 0} {$i < [llength $AuxiliaryVectorsList]} {incr i} {
            append vectorList [format %7s $index]
            append vectorList "  VECTOR_[expr $i + 1]"
            append vectorList "  [lindex [lindex $AuxiliaryVectorsList $i] 0]"
            append vectorList "  [lindex [lindex $AuxiliaryVectorsList $i] 1]"
            append vectorList "  [lindex [lindex $AuxiliaryVectorsList $i] 2]"
            append vectorList " ;\n"
            incr index
        }
    } else {
        for {set i 0} {$i < [llength $AuxiliaryVectorsList]} {incr i} {
            append vectorList [format %7s $index]
            append vectorList "  VECTOR_[expr $i + 1]"
            append vectorList "  [lindex [lindex $AuxiliaryVectorsList $i] 2]"
            append vectorList "  [lindex [lindex $AuxiliaryVectorsList $i] 0]"
            append vectorList "  [lindex [lindex $AuxiliaryVectorsList $i] 1]"
            append vectorList " ;\n"
            incr index
        }
    }

    return $vectorList
}