# LinearIsotropicMaterials.tcl
#
# ExistsLinIsoMaterialInLayerProperties { matName }
# NumOfLinIsoMaterialsInLayerProperties {} 
# WriteLinIsoMaterialsInLayerProperties { index }


proc ExistsLinIsoMaterialInLayerProperties { matName } {

    global linIsoUsedInLayerProp

    if { [ExistsListElement $linIsoUsedInLayerProp [ReplaceSpaceWithUnderscore $matName]] } {
        return 1
    }
    return 0
}

proc NumOfLinIsoMaterialsInLayerProperties {} {

    global layerPropertiesList linIsoUsedInLayerProp

    set linIsoUsedInLayerProp {}

    for {set i 0} {$i < [llength $layerPropertiesList]} {incr i} { 
        if { [lindex [lindex $layerPropertiesList $i] 1] == "LIN_ISO" && ![ExistsListElement $linIsoUsedInLayerProp [lindex [lindex $layerPropertiesList $i] 2]] } {
            lappend linIsoUsedInLayerProp [lindex [lindex $layerPropertiesList $i] 2]
        }
    }

    return [llength $linIsoUsedInLayerProp]    
}

proc WriteLinIsoMaterialsInLayerProperties { index } {

    global linIsoUsedInLayerProp

    set linIso ""
    set materials [GiD_Info materials]

    for {set i 0} {$i < [llength $materials]} {incr i} {
        set material [GiD_Info materials [lindex $materials $i]]
        if { [lindex $material 2] == "LIN_ISO" && [ExistsListElement $linIsoUsedInLayerProp [ReplaceSpaceWithUnderscore [lindex $materials $i]]] } {
            append linIso "[format %7d $index]  "
            append linIso "[ReplaceSpaceWithUnderscore [lindex $materials $i]]"
            for {set j 4} {$j <= 10} {incr j 2} {
                # append linIso [format %18.8e [RemoveUnitsFromParameterValue [lindex $material $j]]]
                append linIso [format %18.8e [lindex $material $j]]
            }
            append linIso " ;\n"
            set index [expr $index + 1]
        }
    }
    return $linIso
}
