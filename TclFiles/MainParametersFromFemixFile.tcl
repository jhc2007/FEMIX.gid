# MainParametersFromFemixFile.tcl
#
# AssignMainParameters { femixData }
# AssignPreconditionedResidualDecay { mainParametersData }
# AssignSystemOfLinearEqAlgorithm { mainParametersData }
# AssignStiffnessMatrixStorageTechnique { mainParametersData }
# AssignGeometricallyNonlinearProblem { mainParametersData }
# AssignMateriallyNonlinearProblem { mainParametersData }
# AssignAnalysisType { mainParametersData }
# CreateMainParametersData { femixData }

proc AssignMainParameters { femixData } {

	set mainParametersData [GetBlockData $femixData "MAIN_PARAMETERS"]

 	AssignAnalysisType $mainParametersData
 	
 	if { [AssignMateriallyNonlinearProblem $mainParametersData] } {
 		AssignArcLengthParameters $femixData
 	}

 	AssignGeometricallyNonlinearProblem $mainParametersData
 	AssignStiffnessMatrixStorageTechnique $mainParametersData
 	AssignSystemOfLinearEqAlgorithm $mainParametersData
 
}

proc AssignPreconditionedResidualDecay { mainParametersData } {

	set index 0

    while { $index < [llength $mainParametersData] && [lindex $mainParametersData $index] != "PRECONDITIONED_RESIDUAL_DECAY" } {
        set index [expr $index + 1]
    }

    if { $index < [llength $mainParametersData] } {

	    set value [lindex $mainParametersData [expr $index + 2]]

	    GiD_Process Data ProblemData -SingleField- Preconditioned_Residual_Decay $value escape 
	}
}

proc AssignSystemOfLinearEqAlgorithm { mainParametersData } {

	set index 0

    while { $index < [llength $mainParametersData] && [lindex $mainParametersData $index] != "SYSTEM_LIN_EQ_ALGORITHM" } {
        set index [expr $index + 1]
    }

    if { $index < [llength $mainParametersData] } {

	    set value [lindex $mainParametersData [expr $index + 2]]
	    set value [string range $value 1 [expr [string length $value] - 1]]

	    if { $value == "GAUSS_ELIM" } {
	    	GiD_Process Data ProblemData -SingleField- System_of_Linear_Eq._Algorithm $value escape
	    } else {
	    	GiD_Process Data ProblemData -SingleField- _System_of_Linear_Eq._Algorithm $value escape
	    }
	}
}

proc AssignStiffnessMatrixStorageTechnique { mainParametersData } {

	set index 0

    while { $index < [llength $mainParametersData] && [lindex $mainParametersData $index] != "STIFFNESS_MATRIX_STORAGE_TECHNIQUE" } {
        set index [expr $index + 1]
    }

    if { $index < [llength $mainParametersData] } {

	    set value [lindex $mainParametersData [expr $index + 2]]
	    set value [string range $value 1 [expr [string length $value] - 1]]

	    GiD_Process Data ProblemData -SingleField- Stiffness_Matrix_Storage_Technique $value escape

	    if { $value == "SYMMETRIC_SPARSE" } {
	    	AssignPreconditionedResidualDecay $mainParametersData
	    }
	}
}

proc AssignGeometricallyNonlinearProblem { mainParametersData } {

	set index 0

    while { $index < [llength $mainParametersData] && [lindex $mainParametersData $index] != "GEOMETRICALLY_NONLINEAR_PROBLEM" } {
        set index [expr $index + 1]
    }

	if { $index < [llength $mainParametersData] } {

	    set value [lindex $mainParametersData [expr $index + 2]]

	    if { $value == "_Y" } {
		    GiD_Process Data ProblemData -SingleField- Geometrically_Nonlinear_Problem 1 escape 
	    }
	}
}

proc AssignMateriallyNonlinearProblem { mainParametersData } {

	set index 0

    while { $index < [llength $mainParametersData] && [lindex $mainParametersData $index] != "MATERIALLY_NONLINEAR_PROBLEM" } {
        set index [expr $index + 1]
    }

    if { $index < [llength $mainParametersData] } {

	    set value [lindex $mainParametersData [expr $index + 2]]

	    if { $value == "_Y" } {

		    GiD_Process Data ProblemData -SingleField- Materially_Nonlinear_Problem 1 escape

				return [AssignNonlinearAnalysisParameters $mainParametersData]
	    }
	}
	return no
}

proc AssignNonlinearAnalysisParameters { mainParametersData } {

	set parameters { "Iterative_Algorithm" \
					 "Convergence_Criterion" \
					 "Path_Behavior" \
					 "Tolerance_in_Each_Combination" \
					 "Maximum_Number_of_Iterations_in_Each_Combination" \
					 "Number_of_Combinations_Before_Restart" \
					 "Add_the_Unbalanced_Forces_in_Each_Combination" \
					 "Line_Search" \
					 "Arc_Length" \
 					 "Maximum_Number_of_Arc_Length_Combinations" }

 	set arcLength no

	for { set i 0 } { $i < [llength $parameters] } { incr i } {

		set index 0

	    while { $index < [llength $mainParametersData] && [lindex $mainParametersData $index] != [string toupper [lindex $parameters $i]] } {
	        set index [expr $index + 1]
	    }

	    if { $index < [llength $mainParametersData] } {

		    set value [lindex $mainParametersData [expr $index + 2]]

		    if { $i < 3 } {
			    set value [string range $value 1 [expr [string length $value] - 1]]
		    } elseif { $i == 6 || $i == 7 } {
		    	if { $value == "_Y" } {
		    		set value 1
		    	} else {
		    		set value 0
		    	}
		    } elseif { $i == 8 } {
		    	if { $value == "_Y" } {
		    		set value 1
		    		set arcLength yes
		    	} else {
		    		set value 0
		    		set arcLength no
		    	}
		    }

		    GiD_Process Data ProblemData -SingleField- [lindex $parameters $i] $value escape 
		}
	}
	return $arcLength
}

proc AssignAnalysisType { mainParametersData } {

	set index 0

    while { $index < [llength $mainParametersData] && [lindex $mainParametersData $index] != "ANALYSIS_TYPE" } {
        set index [expr $index + 1]
    }

    if { $index < [llength $mainParametersData] } {

	    set value [lindex $mainParametersData [expr $index + 2]]
	    set value [string range $value 1 end]

	    GiD_Process Data ProblemData -SingleField- Analysis_Type $value escape 

	    if { $value == "THERMAL_TRANSIENT" } {
	    	AssignThermalTransientTImeSteppingMethod $mainParametersData
	    }
	}
}

proc AssignThermalTransientTImeSteppingMethod { mainParametersData } {

	set index 0

    while { $index < [llength $mainParametersData] && [lindex $mainParametersData $index] != "THERMAL_TRANSIENT_TIME_STEPPING_METHOD" } {
        set index [expr $index + 1]
    }

    if { $index < [llength $mainParametersData] } {

	    set value [lindex $mainParametersData [expr $index + 2]]
	    set value [string range $value 1 end]

	    GiD_Process Data ProblemData -SingleField- Thermal_Transient_Time_Stepping_Method $value escape 

	}
}

# proc CreateMainParametersData { femixData } {

# 	set index 0
# 	set mainParametersData {}

#     while { $index < [llength $femixData] && [lindex $femixData $index] != "<MAIN_PARAMETERS>" } {
#         set index [expr $index + 1]
#     }

#     if { $index < [llength $femixData] } {

# 	    set index [expr $index + 1]

# 	    while { $index < [llength $femixData] && [lindex $femixData $index] != "</MAIN_PARAMETERS>" } {
# 	    	lappend mainParametersData [lindex $femixData $index]
# 	        set index [expr $index + 1]
# 	    }
# 	}

#     return $mainParametersData
# }