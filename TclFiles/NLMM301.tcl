# nlmm301.tcl
#
# NumOfNLMM301InLayerProperties {}
# ExistsNLMM301InLayerProperties { matName }
# WriteNLMM301InLayerProperties { index }

proc NumOfNLMM301InLayerProperties {} {

    global layerPropertiesList NLMM301UsedInLayerProp

    set NLMM301UsedInLayerProp {}

    for {set i 0} {$i < [llength $layerPropertiesList]} {incr i} { 
        if { [lindex [lindex $layerPropertiesList $i] 1] == "NLMM301" && ![ExistsNLMM301InLayerProperties [lindex [lindex $layerPropertiesList $i] 2]] } {
            lappend NLMM301UsedInLayerProp [lindex [lindex $layerPropertiesList $i] 2]
        }
    }

    return [llength $NLMM301UsedInLayerProp]    
}

proc ExistsNLMM301InLayerProperties { matName } {

    global NLMM301UsedInLayerProp

    if { [ExistsListElement $NLMM301UsedInLayerProp [ReplaceSpaceWithUnderscore $matName]] } {
        return 1
    }
    return 0
}

proc WriteNLMM301InLayerProperties { index } {

    global NLMM301UsedInLayerProp

    set nlmm301 ""
    set materials [GiD_Info materials]

    #global PROJECTPATH
    #set result [open "$PROJECTPATH\\aaa_result.dat" "w"]
    ##puts $result $materials
    #set mat [GiD_Info materials [lindex $materials 5]]
    ##puts $result $mat
    #for {set i 1} {$i < [llength $mat]} {incr i} {
    # 	puts $result [lindex $mat $i]
    #}
    #close $result
    #return

    for {set i 0} {$i < [llength $materials]} {incr i} {

    	set material [GiD_Info materials [lindex $materials $i]]
        if { [lindex $material 2] == "NLMM301" && [ExistsNLMM301InLayerProperties [ReplaceSpaceWithUnderscore [lindex $materials $i]]] } {

        	# append nlmm301 "#     A     B     C     D     E     F\n"

        	append nlmm301 "[format %7d $index]  "
        	append nlmm301 [ReplaceSpaceWithUnderscore [lindex $materials $i]]
        	for {set j 4} {$j <= 14} {incr j 2} {
                # append nlmm301 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material $j]]]
                append nlmm301 [format %18.8e [lindex $material $j]]
            }
            append nlmm301 " ;\n"

        	incr index 1
        }
    }
    return $nlmm301
}
