# ReadFemixFile.tcl
#
# OpenFemixFileSelectionWindow {}
# DeleteTmpFiles {}
# CreateFemixDataWithoutComments { femixFile }
# GetBlockData { femixData tag }

proc OpenFemixFileSelectionWindow { editable } {


    # global PROBLEMTYPEPATH 
    # set result [open "$PROBLEMTYPEPATH\\result.dat" "w"]
    # for {set i 0} {$i < [llength [GiD_Info Materials "NLMM104_Concrete"]]} {incr i} {
    #     puts $result "$i -> [lindex [GiD_Info Materials "NLMM104_Concrete"] $i]"
    # }
    # close $result

  #   if { $editable } {

  #       GiD_Process Mescape Data \
  #       Conditions AssignCond _Surface_Springs Change FEMIXDefaultGroup Global 1 1 0 0 \
  #       0 Structure_to_Ground 0 GAUSS-LEGENDRE DEFAULT  GAUSS-LEGENDRE 1 General s1 2 \
  #       General s2 2 General s3 2 General s1 2 General s2 2 General s3 2 General s1 2 \
  #       General s2 2 General s3 2 LIN_SPRING 1 Linear_Spring 0 0 0 12 escape Mescape \
  #       escape escape escape escape



  #       return 
  #   }

    global originalFemixFile
    # useFemixFile
    # set editable 0

    GiD_Process Mescape Files New 
    #No escape

    #set types {{{FEMIX Files} {.dat}}}

    #set femixFile [tk_getOpenFile -filetypes $types]
    #ttk::getOpenFile gid.openFemixFileWin

    #set openWin .gid.openFemixFileWin
    #InitWindow $openWin [= "Combinations"] "" "" "" 1

    #set femixFile [$openFemixFileWin open]
    #[::tk::dialog::file:: open {*}$args]
    
    set originalFemixFile [Browser-ramR file read .gid [= "Read FEMIX file"] {} {{{FEMIX} {*_gl.dat }}} {} 0 {}]
    # set femixFile [tk_getOpenFile -title "Import FEMIX File" -filetypes {{"FEMIX files" {*_gl.dat}}}]
    
    # set femixFile [tk::MotifFDialog open -filetypes {{ "FEMIX files" {*_gl.dat} }}]
    # set originalFemixFile [::tk::dialog::file:: open -filetypes {{"FEMIX files" {*_gl.dat}}}]

    if { $originalFemixFile != "" } {

        OpenWaitWindow

        DeleteTmpFiles

        global FaceLoadsFromFemixFile \
               EdgeLoadsFromFemixFile \
               numDimension \
               InterfaceElementsList \
               femixModelEditable \
               AuxiliaryVectorsList \
               AuxiliaryVectorsFromFemixFile
               # GroupNames

        set InterfaceElementsList {}
        set FaceLoadsFromFemixFile {}
        set EdgeLoadsFromFemixFile {}
        set AuxiliaryVectorsList {}
        set AuxiliaryVectorsFromFemixFile {}
        # set GroupNames {}
        set index 8

        set femixDataWithoutComments [CreateFemixDataWithoutComments $originalFemixFile]
        
        # CreateMshFileFromFemixFile $originalFemixFile $femixDataWithoutComments

        set elementPropertiesData [GetBlockData $femixDataWithoutComments "ELEMENT_PROPERTIES"]

        if { [llength $elementPropertiesData] > 0 } {
            set numDimension [GetNumDimensionFromFemixFile $elementPropertiesData]
            while { $index < [llength $elementPropertiesData] } {
                if { [lindex $elementPropertiesData $index] == "_INTERFACE_LINE_2D" } {
                    set rangeList [GetFirstLastValuesFromRange [lindex $elementPropertiesData [expr $index - 1]]]
                    set firstElem [lindex $rangeList 0]
                    set lastElem [lindex $rangeList 1]
                    lappend InterfaceElementsList [list $firstElem $lastElem]
                    # AddInterfaceElementsFromFemixFile $firstElem $lastElem
                }
                set index [expr $index + 12]
            }
        } else {
            set numDimension 3   
        }

        CreateMshFileFromFemixFile $originalFemixFile $femixDataWithoutComments

        if { $editable } {

            set femixModelEditable yes

            AssignMainParameters $femixDataWithoutComments
            CreateLinearIsotropicMaterials $femixDataWithoutComments
            CreateNLMM101 $femixDataWithoutComments
            CreateNLMM104 $femixDataWithoutComments
            CreateNLMM111 $femixDataWithoutComments
            CreateNLMM141 $femixDataWithoutComments
            CreateNLMM151 $femixDataWithoutComments
            CreateNLMM201 $femixDataWithoutComments
            CreateNLMM301 $femixDataWithoutComments
            CreateNLMM305 $femixDataWithoutComments
            CreateLinIntLine2D $femixDataWithoutComments
            CreateLinIntLine3D $femixDataWithoutComments
            CreateLinIntSurface $femixDataWithoutComments
            CreateLinearSpringMaterials $femixDataWithoutComments
            CreateNLSMM101 $femixDataWithoutComments
            CreateNLSMM102 $femixDataWithoutComments
            CreateThermalIsotropicMaterials $femixDataWithoutComments
            CreateNLMM401 $femixDataWithoutComments
            AssignElementProperties $femixDataWithoutComments
            AssignSupports $femixDataWithoutComments
            CreateAuxiliaryVectors $femixDataWithoutComments
            AssignCoordinateSystems $femixDataWithoutComments
            AssignPointsWithSpecialCoordinateSystem $femixDataWithoutComments
            AssignPointSprings $femixDataWithoutComments
            CreateLineSprings $femixDataWithoutComments
            CreateSurfaceSprings $femixDataWithoutComments
            AssignThermalTransientInitialConditions $femixDataWithoutComments
            AssignLoads $femixDataWithoutComments
            AssignCombinations $femixDataWithoutComments

        } else {
            set femixModelEditable no   
        }
        # set useFemixFile yes              
        DestroyWaitWindow
    }
}

# proc PrintAuxiliaryVectors {} {

#       global PROBLEMTYPEPATH AuxiliaryVectorsList AuxiliaryVectorsFromFemixFile PointSpringsList
#       set result [open "$PROBLEMTYPEPATH\\result_new.dat" "w"]
#       puts $result "AuxiliaryVectorsFromFemixFile:"
#       foreach var $AuxiliaryVectorsFromFemixFile {
#           puts $result $var
#       }
#       puts $result "AuxiliaryVectorsList:"
#       foreach var $AuxiliaryVectorsList {
#           puts $result $var
#       }
#       puts $result "PointSpringsList:"
#       foreach var $PointSpringsList {
#           puts $result $var
#       }
#       close $result
#       return 0
# }

proc DeleteTmpFiles {} {

    global PROBLEMTYPEPATH

    set tmpFiles [glob -nocomplain -directory $PROBLEMTYPEPATH\\TmpFiles *]

    foreach file $tmpFiles {
        file delete $file
    }
}

proc CreateFemixDataWithoutComments { femixFile } {

    set femixFile [open "$femixFile" "r"]
    set femixData [read $femixFile]
    close $femixFile

    set femixData [split $femixData "\n"]

    set femixDataWithoutComments {}

    foreach line $femixData {

        if { [string index [lindex $line 0] 0] != "#" } {
            set i 0
            while {$i < [llength $line]} {
                if { [string index [lindex $line $i] 0] == "#" } {
                    set i [llength $line]
                } elseif { [lindex $line $i] != "\n"} {
                    lappend femixDataWithoutComments [lindex $line $i]
                }
                set i [expr $i + 1]
            }
        }
    }
    return $femixDataWithoutComments
}

proc GetBlockData { femixData tag } {

    set index 0
    set data {}

    while { $index < [llength $femixData] && [lindex $femixData $index] != "\<$tag\>" } {
        set index [expr $index + 1]
    }

    if { $index < [llength $femixData] } {

        set index [expr $index + 1]

        while { $index < [llength $femixData] && [lindex $femixData $index] != "\<\/$tag\>" } {
            lappend data [lindex $femixData $index]
            set index [expr $index + 1]
        }
    }

    return $data
}

proc GetFirstLastValuesFromRange { strRange } {

    set values {}

    if { [string is integer $strRange] } {
        set firstValue $strRange
        set lastValue $strRange
    } else {
        set firstValue [string range $strRange 1 [expr [string first "-" $strRange] - 1]]
        set lastValue  [string range $strRange [expr [string first "-" $strRange] + 1] [expr [string length $strRange] - 2]]
    }

    lappend values $firstValue
    lappend values $lastValue
    
    return $values
}

proc GetNumDimensionFromFemixFile { elemPropData } {

    set index 8
    set elemType2D [list  "_CABLE_2D" \
                          "_EMB_CABLE_2D" \
                          "_FRAME_2D" \
                          "_PLANE_STRESS_QUAD" \
                          "_PLANE_STRESS_QUAD_THERMAL" \
                          "_TIMOSHENKO_BEAM_2D" \
                          "_TRUSS_2D" \
                          "_INTERFACE_LINE_2D" ]

    while {$index < [llength $elemPropData] } {
        if { [ExistsListElement $elemType2D [lindex $elemPropData $index]] } {
            return 2
        }
        set index [expr $index + 12]
    }
    return 3
}

