# NLMM305FromFemixFile.tcl

proc CreateNLMM305 { femixData } {

    set nlmm305Data [GetBlockData $femixData "NLMM305"]

    if { [llength $nlmm305Data] > 0 } {

        global doubleFormatMatParameters
        set allMaterialsNameList [GiD_Info materials]
        set nlmm305NameList {}

        for {set i 0} {$i < [llength $allMaterialsNameList]} {incr i} {
            if { [lindex [GiD_Info materials [lindex $allMaterialsNameList $i]] 2] == "NLMM305" } {
                lappend nlmm305NameList [string toupper [lindex $allMaterialsNameList $i]]
            }
        }

    	set numMaterials [lindex $nlmm305Data 2]

    	set index 4

    	for {set i 0} {$i < $numMaterials} {incr i} {

    		set materialName [lindex $nlmm305Data [expr $index + 1]]

    		set materialData { NLMM305 } 

    		lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm305Data [expr $index + 2]]]"
	    	lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm305Data [expr $index + 3]]]"
	    	lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm305Data [expr $index + 4]]]"
	    	lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm305Data [expr $index + 5]]]"
	    	lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm305Data [expr $index + 6]]]"
	    	lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm305Data [expr $index + 7]]]"
            lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm305Data [expr $index + 8]]]"

	    	if { [ExistsListElement $nlmm305NameList [string toupper $materialName]] } {
	    		GiD_ModifyData materials $materialName $materialData
	    	} else {
	    		GiD_CreateData create material NLMM305_Interface $materialName $materialData
	    	}

        	set index [expr $index + 9]  
	    }
	}
}
