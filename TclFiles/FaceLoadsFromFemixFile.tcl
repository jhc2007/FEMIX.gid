# FaceLoadsFromFemixFile.tcl
#

proc CreateFaceLoadsFromFemixFile { loadCaseData loadCaseNumber } {

	global FaceLoadsFromFemixFile
	
	set faceLoadsData [GetBlockData $loadCaseData "FACE_LOADS"]

	set index 5

	while { $index < [llength $faceLoadsData] } {
		
		set rangeList [GetFirstLastValuesFromRange [lindex $faceLoadsData $index]]
		set firstElem [lindex $rangeList 0]
		set lastElem [lindex $rangeList 1]

		incr index
		set face [lindex $faceLoadsData $index]

		set faceLoad [list $loadCaseNumber $firstElem $lastElem $face]

		incr index
		set key [lindex $faceLoadsData $index]

		if { [string index $key 0] != "_" } {

			set vector [GetAuxiliaryVectorFromFemixFile $key]
			# set vector [list [format %.5e [lindex $vector 0]] \
   			#                  [format %.5e [lindex $vector 1]] \
   			#                  [format %.5e [lindex $vector 2]]]
			# set key [AddAuxiliaryVector [lindex $vector 0] [lindex $vector 1] [lindex $vector 2]]
			# set key "VECTOR_[GetAuxiliaryVectorIndex $vector]"
			foreach var $vector {
				lappend faceLoad $var
			}
		} else {
			lappend faceLoad $key
		}

		incr index
		# set loadType [lindex $faceLoadsData $index]
		lappend faceLoad [lindex $faceLoadsData $index]

		incr index
		set numNodes [lindex $faceLoadsData $index]
		lappend faceLoad $numNodes

		# set faceLoad [list $loadCaseNumber $firstElem $lastElem $face $key $loadType $numNodes]

		for {set i 0} {$i < $numNodes} {incr i} {
			incr index
			lappend faceLoad [lindex $faceLoadsData $index]
		}

		lappend FaceLoadsFromFemixFile $faceLoad

		incr index 3
	}
}

proc AddAuxiliaryVectorFromFaceLoadsFromFemixFile {} {

	global FaceLoadsFromFemixFile

	foreach var $FaceLoadsFromFemixFile {
		if { [string is double [lindex $var 4]] } {
			AddAuxiliaryVector [lindex $var 4] [lindex $var 5] [lindex $var 6]
		}
	}
	return 0
}

# proc GetAuxiliaryVectorIndex { auxiliaryVectorsList name } {

# 	for {set i 0} {$i < [llength $auxiliaryVectorsList]} {incr i} {
# 		if { [lindex [lindex $auxiliaryVectorsList $i] 0] == $name } {
# 			return $i
# 		}
# 	}
# 	return -1
# }

proc CountFaceLoadsFromFemixFile { loadCaseNumber } {

	global FaceLoadsFromFemixFile

	set index 0
	set count 0

	while { $index < [llength $FaceLoadsFromFemixFile] && [lindex [lindex $FaceLoadsFromFemixFile $index] 0] != $loadCaseNumber } {
		incr index
	}

	while { $index < [llength $FaceLoadsFromFemixFile] && [lindex [lindex $FaceLoadsFromFemixFile $index] 0] == $loadCaseNumber } {
		set count [expr $count + [lindex [lindex $FaceLoadsFromFemixFile $index] 2] - [lindex [lindex $FaceLoadsFromFemixFile $index] 1] + 1]
		incr index
	}

	return $count
}

proc WriteFaceLoadsFromFemixFile { loadCaseNumber index } {

	global FaceLoadsFromFemixFile 

	incr index
	set returnString ""
	set i 0

	while { $i < [llength $FaceLoadsFromFemixFile] && [lindex [lindex $FaceLoadsFromFemixFile $i] 0] != $loadCaseNumber } {
		incr i
	}

	while { $i < [llength $FaceLoadsFromFemixFile] && [lindex [lindex $FaceLoadsFromFemixFile $i] 0] == $loadCaseNumber } {

		set faceLoad [lindex $FaceLoadsFromFemixFile $i]

        if { [lindex $faceLoad 1] == [lindex $faceLoad 2] } {
            append returnString [format %7d $index]
            append returnString "  [lindex $faceLoad 1]"
            incr index
        } else {
            append returnString "  \[$index\-[expr $index + [lindex $faceLoad 2] - [lindex $faceLoad 1]]\]"
            append returnString "  \[[lindex $faceLoad 1]\-[lindex $faceLoad 2]\]"
            set index [expr $index + [lindex $faceLoad 2] - [lindex $faceLoad 1] + 1]
        }

        append returnString "  [lindex $faceLoad 3]"

        if { [string is double [lindex $faceLoad 4]] } {
        	append returnString "  VECTOR_[GetAuxiliaryVectorIndexFromComponents [lindex $faceLoad 4] [lindex $faceLoad 5] [lindex $faceLoad 6]]"
	        for {set j 7} {$j < [llength $faceLoad]} {incr j} {
	        	append returnString "  [lindex $faceLoad $j]"
	        }
        } else {
	        for {set j 4} {$j < [llength $faceLoad]} {incr j} {
	        	append returnString "  [lindex $faceLoad $j]"
	        }
        }

        append returnString " ;\n"

		incr i
	}

    return $returnString
}

proc CreateFileFaceLoadsFromFemixFile {} {

	global PROJECTPATH \
		   FaceLoadsFromFemixFile

	if { [llength $FaceLoadsFromFemixFile] == 0 } {
		return
	}

	set faceLoadsFile [open "$PROJECTPATH\\femix_face_loads.dat" "w"]
    for {set i 0} {$i < [llength $FaceLoadsFromFemixFile]} {incr i} {
    	foreach var [lindex $FaceLoadsFromFemixFile $i] {
    		puts $faceLoadsFile $var
    	}
    }
    close $faceLoadsFile
}

proc CreateFromFileFaceLoadsFromFemixFile {} {

	global PROJECTPATH \
		   FaceLoadsFromFemixFile

	if { [file exists "$PROJECTPATH\\femix_face_loads.dat"] } {
		
		set faceLoadsFile [open "$PROJECTPATH\\femix_face_loads.dat" "r"]
  		set faceLoadsData [read $faceLoadsFile]
  		close $faceLoadsFile

  		set index 0

  		while { $index < [llength $faceLoadsData] } {

  			set faceLoad {}

  			for {set i $index} {$i < [expr $index + 4]} {incr i} {
  				lappend faceLoad [lindex $faceLoadsData $i]
  			}

  			incr index 4
  			if { [string is double [lindex $faceLoadsData $index]] } {
	  			for {set i $index} {$i < [expr $index + 4]} {incr i} {
	  				lappend faceLoad [lindex $faceLoadsData $i]
	  			}
	  			incr index 4
			} else {
				for {set i $index} {$i < [expr $index + 2]} {incr i} {
	  				lappend faceLoad [lindex $faceLoadsData $i]
	  			}
	  			incr index 2
			}

  			set numNodes [lindex $faceLoadsData $index]
  			lappend faceLoad $numNodes

  			for {set i 0} {$i < $numNodes} {incr i} {
  				incr index
  				lappend faceLoad [lindex $faceLoadsData $index]
  			}

  			incr index
  			
  			lappend FaceLoadsFromFemixFile $faceLoad
  		}
	}
}