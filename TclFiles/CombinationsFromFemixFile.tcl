# CombinationsFromFemixFile.tcl

# AssignCombinations { $femixData }

proc AssignCombinations { femixData } {

	global PROBLEMTYPEPATH 
	
	# set result [open "$PROBLEMTYPEPATH\\result_new.dat" "w"]

	set loadCaseCombinationsData [GetBlockData $femixData "LOAD_CASE_COMBINATIONS"]

	if { [llength $loadCaseCombinationsData] > 0 } {

		set index 0
		set numCombinations 0
		set combinationsList { 0 }	

		while { $index < [llength $loadCaseCombinationsData] } {

			set combinationData {}

			while { $index < [llength $loadCaseCombinationsData] && [lindex $loadCaseCombinationsData $index] != "<COMBINATION>" } {
				incr index 1
			}

			if { $index < [llength $loadCaseCombinationsData] } {

		        incr index 1
				set numCombinations [expr $numCombinations + 1]

				while { $index < [llength $loadCaseCombinationsData] && [lindex $loadCaseCombinationsData $index] != "</COMBINATION>" } {
					lappend combinationData [lindex $loadCaseCombinationsData $index]
		        	incr index				
				}

				lappend combinationsList [GetCombinationTitle $combinationData]
				lappend combinationsList [GetCombinationGroup $combinationData]

				set combRange [GetFirstLastCombination $combinationData]

				# set firstComb [lindex $combRange 0]
				# set lastComb [lindex $combRange 1]

				lappend combinationsList [lindex $combRange 0]
				lappend combinationsList [lindex $combRange 1]

				lappend combinationsList [GetStepDuration $combinationData]

				set combinationsList [GetLoadCaseFactorsValues $combinationData $combinationsList]				

				# puts $result [GetCombinationGroup $combinationData]
				# puts $result [GetCombinationTitle $combinationData]
				# puts $result [lindex $combRange 0]
				# puts $result [lindex $combRange 1]
			}

		}

		if { $numCombinations > 0 } {
			set combinationsList [lreplace $combinationsList 0 0 $numCombinations]
		}

		set femixCombinationsFile [open "$PROBLEMTYPEPATH\\TmpFiles\\femix_combinations.dat" "w"]
		for {set i 0} {$i < [llength $combinationsList]} {incr i} {
			puts $femixCombinationsFile [lindex $combinationsList $i]
		}
		close $femixCombinationsFile
	}
}

proc GetLoadCaseFactorsValues { combinationData combinationsList } {

	set loadCaseFactorsData [GetBlockData $combinationData "LOAD_CASE_FACTORS"]

	if { [llength $loadCaseFactorsData] > 0 } {

		lappend combinationsList [lindex $loadCaseFactorsData 2]

		set index 5

		while { $index < [llength $loadCaseFactorsData] } {

			set loadCaseRange [GetFirstLastValuesFromRange [lindex $loadCaseFactorsData $index]]

			set firstLoadCase [lindex $loadCaseRange 0]
			set lastLoadCase [lindex $loadCaseRange 1]

			for {set i $firstLoadCase} {$i <= $lastLoadCase} {incr i} {
				lappend combinationsList $i
				lappend combinationsList [lindex $loadCaseFactorsData [expr $index + 1]]
			}

			incr index 4
		}
	}

	return $combinationsList
}

proc GetCombinationGroup { combinationData } {

	set index 0

	while { $index < [llength $combinationData] && [lindex $combinationData $index] != "COMBINATION_GROUP" } {
		incr index
	}

	incr index 2

	if { $index < [llength $combinationData] } {

		set groupName [lindex $combinationData $index]

		if { ![ExistsListElement [GiD_Info layers] $groupName] && ![ExistsListElement [GiD_Groups list] $groupName] } {
			GiD_Process 'Groups Create $groupName Mescape
		}
		
		return $groupName
	}

	return ""
}

proc GetStepDuration { combinationData } {

	set index 0

	while { $index < [llength $combinationData] && [lindex $combinationData $index] != "STEP_DURATION" } {
		incr index
	}

	incr index 2

	if { $index < [llength $combinationData] } {
		return [lindex $combinationData $index]
	}

	return 0
}

proc GetCombinationTitle { combinationData } {

	set index 0

	while { $index < [llength $combinationData] && [lindex $combinationData $index] != "COMBINATION_TITLE" } {
		incr index 1
	}

	set index [expr $index + 2]

	if { $index < [llength $combinationData] } {
		set title [lindex $combinationData $index]
	} else {
		return ""
	}

	incr index 1

	while { $index < [llength $combinationData] && [lindex $combinationData $index] != ";" } {
		append title "_[lindex $combinationData $index]"
		incr index 1
	}

	return $title		
}

proc GetFirstLastCombination { combinationData } {

	set index 0

	while { $index < [llength $combinationData] && [lindex $combinationData $index] != "COMBINATION_NUMBER" && [lindex $combinationData $index] != "COMBINATION_RANGE" } {
		incr index 1
	}

	set index [expr $index + 2]

	if { $index < [llength $combinationData] } {
		return [GetFirstLastValuesFromRange [lindex $combinationData $index]]
	}
	
	return { 0 0 }
}