
proc CreateNLSMM102 { femixData } {

    set nlsmm102Data [GetBlockData $femixData "NLSMM102"]

    if { [llength $nlsmm102Data] > 0 } {

        # global doubleFormatMatParameters
        
        set allMaterialsNameList [GiD_Info Materials]
        set nlsmm102NameList {}

        for {set i 0} {$i < [llength $allMaterialsNameList]} {incr i} {
            if { [lindex [GiD_Info Materials [lindex $allMaterialsNameList $i]] 2] == "NLSMM102" } {
                lappend nlsmm102NameList [string toupper [lindex $allMaterialsNameList $i]]
                # lappend nlsmm102NameList [lindex $allMaterialsNameList $i]
            }
        }

    	set numMaterials [lindex $nlsmm102Data 2]

    	set index 5

    	for {set i 0} {$i < $numMaterials} {incr i} {

    		set materialName [lindex $nlsmm102Data $index]

    		set materialData { NLSMM102 }

            incr index 
            lappend materialData [string range [lindex $nlsmm102Data $index] 1 end]

            for {set j 0} {$j < 7} {incr j} {
                incr index 
                lappend materialData [lindex $nlsmm102Data $index]
            }

	    	if { [ExistsListElement $nlsmm102NameList [string toupper $materialName]] } {
	    		GiD_ModifyData materials $materialName $materialData
	    	} else {
	    		GiD_CreateData create material NLSMM102_Spring $materialName $materialData
	    	}

        	incr index 3
	    }
	}
}
