# LayerPatterns.tcl
#
# proc OpenLayerPatternsWindow {}
# proc RemoveLayerPatterns {}
# proc EditLayerPatterns {}
# proc UpdateLayerPatternsName { layerPattIndex }
# proc UpdateLayerPropInLayerPatterns { layerPattIndex layerPropIndex }
# proc GetNumLayers { layerPattIndex  layerPropIndex }
# proc CreateLayerPatternsList {}
# proc CreateFemixLayerPatternsFile {}
# proc AddLayerPatterns {}
# proc FillListBoxLayerPatterns {}
# proc GetLayerPatternsIndex { name }
# proc CloseLayerPatternsWin {}
# proc NumOfLayerPatterns {}
# proc WriteLayerPatterns {}


proc OpenLayerPatternsWindow {} {

    global PROBLEMTYPEPATH PROJECTPATH
    global layerPatternsList layerPropertiesList layerPatternsWin layerPropNameList

    if { [GidUtils::AreWindowsDisabled] } {
       return
    }      
    if { $PROBLEMTYPEPATH == $PROJECTPATH } {
        OpenWarningWindow "Warning" "Save the project before edit layer patterns" 1
        return
    }

    if { [file exists "$PROJECTPATH\\femix_layer_patterns.dat"] } {
        CreateLayerPatternsList
    }

    if { [file exists "$PROJECTPATH\\femix_layer_properties.dat"] } {
        CreateLayerPropertiesList
    }

    set layerPropNameList {}
    for {set i 0} {$i < [llength $layerPropertiesList]} {incr i} {
        lappend layerPropNameList [lindex [lindex $layerPropertiesList $i] 0]
    }

    set layerPatternsWin .gid.layerpatternswindow
    InitWindow $layerPatternsWin [= "Layer Patterns"] "" "" "" 1
    if { ![winfo exists $layerPatternsWin] } {
       return
    }

    label $layerPatternsWin.labelLayerPatternsName -text "Layer Pattern Name"
    
    entry $layerPatternsWin.textFieldLayerPatternsName -textvariable "" -width 30
    $layerPatternsWin.textFieldLayerPatternsName insert 0 "Layer_Pattern_[expr [llength $layerPatternsList] + 1]"

    #frame $layerPatternsWin.frameLayerProp -relief ridge -borderwidth 2

    #label $layerPatternsWin.frameLayerProp.labelLayerPropName -text "Layer Properties Name"
    #label $layerPatternsWin.frameLayerProp.labelNumLayers -text "Number of Layers"
    #label $layerPatternsWin.frameLayerProp.labelNull -text ""

    label $layerPatternsWin.labelLayerPropName -text "Layer Properties Name"
    label $layerPatternsWin.labelNumLayers -text "Number of Layers"
    label $layerPatternsWin.labelNull -text ""

    #ttk::combobox $layerPatternsWin.frameLayerProp.comboBoxLayerPropName -values $layerPropNameList -width 26
    #$layerPatternsWin.frameLayerProp.comboBoxLayerPropName set [lindex $layerPropNameList 0]

    ttk::combobox $layerPatternsWin.comboBoxLayerPropName -values $layerPropNameList -width 27
    if { [llength $layerPropNameList] > 0 } {
        $layerPatternsWin.comboBoxLayerPropName current 0
    }
    #$layerPatternsWin.comboBoxLayerPropName set [lindex $layerPropNameList 0]

    #entry $layerPatternsWin.frameLayerProp.textFieldNumLayers -textvariable "" -width 10
    entry $layerPatternsWin.textFieldNumLayers -textvariable "" -width 10

    #button $layerPatternsWin.frameLayerProp.buttonAddLayerProp -text "Add Layer Properties" -command "AddLayerPropToLayerPattern" -width 16

    listbox $layerPatternsWin.listBoxLayerPatterns -width 40 -height 12
    FillListBoxLayerPatterns    

    #button $layerPatternsWin.buttonAddLayerPatt -text "Add Layer Pattern" -command "AddLayerPatterns" -width 16
    button $layerPatternsWin.buttonAdd -text "Add" -command "AddLayerPatterns" -width 12
    button $layerPatternsWin.buttonEdit -text "Edit" -command "EditLayerPatterns" -width 14
    button $layerPatternsWin.buttonRemove -text "Remove" -command "RemoveLayerPatterns" -width 14

    button $layerPatternsWin.buttonAccept -text "Accept" -command "CreateFemixLayerPatternsFile" -width 12
    button $layerPatternsWin.buttonClose -text "Close" -command "CloseLayerPatternsWin" -width 12

    grid $layerPatternsWin.labelLayerPatternsName -row 0 -column 2 -padx 10 -sticky w
    grid $layerPatternsWin.textFieldLayerPatternsName -row 0 -column 3 -columnspan 2 -padx 10 -sticky w

    #grid $layerPatternsWin.frameLayerProp -row 1 -column 0 -columnspan 2 -sticky nw -padx 5
    # -rowspan 4

    #grid $layerPatternsWin.frameLayerProp.labelLayerPropName -row 0 -column 0 -padx 5 -pady 4 -sticky w    
    #grid $layerPatternsWin.frameLayerProp.labelNumLayers -row 1 -column 1 -padx 5 -sticky w    
    grid $layerPatternsWin.labelLayerPropName -row 1 -column 2 -padx 10 -sticky w    
    grid $layerPatternsWin.labelNumLayers -row 2 -column 2 -padx 10 -sticky w    

    #grid $layerPatternsWin.frameLayerProp.comboBoxLayerPropName -row 0 -column 1 -columnspan 2 -padx 5 -pady 5 -sticky w
    #grid $layerPatternsWin.frameLayerProp.textFieldNumLayers -row 1 -column 2 -padx 5 -pady 5 -sticky w
    #grid $layerPatternsWin.frameLayerProp.labelNull -row 2 -column 0 -pady 5 -sticky w
    #grid $layerPatternsWin.frameLayerProp.buttonAddLayerProp -row 3 -column 1 -columnspan 2 -padx 5 -pady 5 -sticky e
    grid $layerPatternsWin.comboBoxLayerPropName -row 1 -column 3 -columnspan 2 -padx 10 -sticky w
    grid $layerPatternsWin.textFieldNumLayers -row 2 -column 3 -padx 10 -sticky w

    grid $layerPatternsWin.listBoxLayerPatterns -row 0 -column 0 -rowspan 4 -columnspan 2 -padx 10 -pady 10

    grid $layerPatternsWin.buttonAdd -row 2 -column 4 -padx 10 -sticky e

    grid $layerPatternsWin.labelNull -row 3 -column 2 -padx 10 -sticky w

    grid rowconfigure $layerPatternsWin 4 -weight 1

    grid $layerPatternsWin.buttonEdit -row 4 -column 0 -sticky w -padx 10
    grid $layerPatternsWin.buttonRemove -row 4 -column 1 -sticky e -padx 10  

    grid $layerPatternsWin.buttonAccept -row 4 -column 3 -sticky e
    grid $layerPatternsWin.buttonClose -row 4 -column 4 -padx 10 -sticky e

}

proc RemoveLayerPatterns {} {

    global layerPatternsWin layerPatternsList

    set selectedIndex [$layerPatternsWin.listBoxLayerPatterns curselection]

    if { $selectedIndex == "" } {
        return
    }

    set index $selectedIndex

    while { [string range [$layerPatternsWin.listBoxLayerPatterns get $index] 0 2] == ">  " } {
        set index [expr $index - 1]
    }

    set layerPattIndex [GetLayerPatternsIndex [$layerPatternsWin.listBoxLayerPatterns get $index]]

    $layerPatternsWin.textFieldLayerPatternsName delete 0 [string length [$layerPatternsWin.textFieldLayerPatternsName get]]
    
    $layerPatternsWin.comboBoxLayerPropName current 0
    $layerPatternsWin.textFieldNumLayers delete 0 [string length [$layerPatternsWin.textFieldNumLayers get]]

    if { $index == $selectedIndex } {
        set layerPatternsList [lreplace $layerPatternsList $layerPattIndex $layerPattIndex]
    } else {
        set layerPatt [lindex $layerPatternsList $layerPattIndex]
        set layerPropInLayerPattIndex [expr 3 * ($selectedIndex - $index)]
        set numLayers [expr [lindex $layerPatt [expr $layerPropInLayerPattIndex - 1]] - [lindex $layerPatt [expr $layerPropInLayerPattIndex - 2]] + 1]
        set layerPatt [lreplace $layerPatt [expr $layerPropInLayerPattIndex - 2] $layerPropInLayerPattIndex]
        if { [llength $layerPatt] == 1 } {
            set layerPatternsList [lreplace $layerPatternsList $layerPattIndex $layerPattIndex]
        } else {
            while { $layerPropInLayerPattIndex < [llength $layerPatt] } {
                set layerPatt [lreplace $layerPatt [expr $layerPropInLayerPattIndex - 1] [expr $layerPropInLayerPattIndex - 1] [expr [lindex $layerPatt [expr $layerPropInLayerPattIndex - 1]] - $numLayers]]
                set layerPatt [lreplace $layerPatt [expr $layerPropInLayerPattIndex - 2] [expr $layerPropInLayerPattIndex - 2] [expr [lindex $layerPatt [expr $layerPropInLayerPattIndex - 2]] - $numLayers]]
                set layerPropInLayerPattIndex [expr $layerPropInLayerPattIndex + 3]
            }
            set layerPatternsList [lreplace $layerPatternsList $layerPattIndex $layerPattIndex $layerPatt]
        }
    }

    FillListBoxLayerPatterns
    $layerPatternsWin.textFieldLayerPatternsName insert 0 "Layer_Pattern_[expr [llength $layerPatternsList] + 1]"
}

proc EditLayerPatterns {} {

    global layerPatternsWin layerPatternsList

    set selectedIndex [$layerPatternsWin.listBoxLayerPatterns curselection]

    if { $selectedIndex == "" } {
        return
    }

    $layerPatternsWin.textFieldLayerPatternsName configure -state normal
    $layerPatternsWin.comboBoxLayerPropName configure -state normal 
    $layerPatternsWin.textFieldNumLayers configure -state normal

    set index $selectedIndex
    #set selectedLine 

    while { [string range [$layerPatternsWin.listBoxLayerPatterns get $index] 0 2] == ">  " } {
        set index [expr $index - 1]
    }

    set layerPattIndex [GetLayerPatternsIndex [$layerPatternsWin.listBoxLayerPatterns get $index]]

    $layerPatternsWin.textFieldLayerPatternsName delete 0 [string length [$layerPatternsWin.textFieldLayerPatternsName get]]
    $layerPatternsWin.textFieldNumLayers delete 0 [string length [$layerPatternsWin.textFieldNumLayers get]]

    $layerPatternsWin.textFieldLayerPatternsName insert 0 [lindex [lindex $layerPatternsList $layerPattIndex] 0]

    if { $index == $selectedIndex } {
        $layerPatternsWin.comboBoxLayerPropName current 0
        $layerPatternsWin.buttonAdd configure -text "Update" -command "UpdateLayerPatternsName $layerPattIndex"
        $layerPatternsWin.comboBoxLayerPropName configure -state disabled
        $layerPatternsWin.textFieldNumLayers configure -state disabled
    } else {
        $layerPatternsWin.textFieldLayerPatternsName configure -state disabled
        #set layerPropName [lindex [split [$layerPatternsWin.listBoxLayerPatterns get $selectedIndex]] 3]
        $layerPatternsWin.comboBoxLayerPropName set [lindex [split [$layerPatternsWin.listBoxLayerPatterns get $selectedIndex]] 3]
        set layerPropInLayerPattIndex [expr 3 * ($selectedIndex - $index)]
        $layerPatternsWin.textFieldNumLayers insert 0 [GetNumLayers $layerPattIndex $layerPropInLayerPattIndex]
        $layerPatternsWin.buttonAdd configure -text "Update" -command "UpdateLayerPropInLayerPatterns $layerPattIndex $layerPropInLayerPattIndex"
    }

    
    #global PROJECTPATH    
    #set result [open "$PROJECTPATH\\result.dat" "w"]
    #puts $result [llength $selectedLine]
    #puts $result $selectedLine
    #close $result
}

proc UpdateLayerPatternsName { layerPattIndex } {

    global layerPatternsWin layerPatternsList

    set layerPattName [ReplaceSpaceWithUnderscore [$layerPatternsWin.textFieldLayerPatternsName get]]
    if { $layerPattName == "" } {
        OpenWarningWindow "Layer Patterns" "Insert a layer pattern name" 0
        return
    }

    set layerPatt [lindex $layerPatternsList $layerPattIndex]
    set layerPatt [lreplace $layerPatt 0 0 $layerPattName]

    set layerPatternsList [lreplace $layerPatternsList $layerPattIndex $layerPattIndex $layerPatt]

    $layerPatternsWin.comboBoxLayerPropName configure -state normal
    $layerPatternsWin.textFieldNumLayers configure -state normal

    FillListBoxLayerPatterns

    $layerPatternsWin.buttonAdd configure -text "Add" -command "AddLayerPatterns"
    $layerPatternsWin.textFieldLayerPatternsName delete 0 [string length [$layerPatternsWin.textFieldLayerPatternsName get]]
    $layerPatternsWin.textFieldLayerPatternsName insert 0 "Layer_Pattern_[expr [llength $layerPatternsList] + 1]"
    #$layerPatternsWin.comboBoxLayerPropName set [lindex $layerPropNameList 0]
    #$layerPatternsWin.textFieldNumLayers delete [string length [$layerPatternsWin.textFieldNumLayers get]]
}

proc UpdateLayerPropInLayerPatterns { layerPattIndex layerPropIndex } {

    global layerPatternsWin layerPatternsList

    set newNumLayers [$layerPatternsWin.textFieldNumLayers get]
    if { $newNumLayers == "" } {
        OpenWarningWindow "Layer Patterns" "Insert number of layers" 0
        return
    }

    set layerPatt [lindex $layerPatternsList $layerPattIndex]

    set layerProp [$layerPatternsWin.comboBoxLayerPropName get]  

    set layerPatt [lreplace $layerPatt $layerPropIndex $layerPropIndex $layerProp]

    set oldNumLayers [expr [lindex $layerPatt [expr $layerPropIndex - 1]] - [lindex $layerPatt [expr $layerPropIndex - 2]] + 1]

    if { $newNumLayers != $oldNumLayers } {
        
        set firstLayer [lindex $layerPatt [expr $layerPropIndex - 2]]
        set lastLayer [expr $firstLayer + $newNumLayers - 1]
        set layerPatt [lreplace $layerPatt [expr $layerPropIndex - 1] [expr $layerPropIndex - 1] $lastLayer]
        set layerPropIndex [expr $layerPropIndex + 3]

        while { $layerPropIndex < [llength $layerPatt] } {
            set firstLayer [expr $lastLayer + 1]
            set numLayers [expr [lindex $layerPatt [expr $layerPropIndex - 1]] - [lindex $layerPatt [expr $layerPropIndex - 2]] + 1]
            set lastLayer [expr $firstLayer + $numLayers - 1]
            set layerPatt [lreplace $layerPatt [expr $layerPropIndex - 2] [expr $layerPropIndex - 2] $firstLayer]
            set layerPatt [lreplace $layerPatt [expr $layerPropIndex - 1] [expr $layerPropIndex - 1] $lastLayer]
            set layerPropIndex [expr $layerPropIndex + 3]
        }
    }
    
    set layerPatternsList [lreplace $layerPatternsList $layerPattIndex $layerPattIndex $layerPatt]

    $layerPatternsWin.textFieldLayerPatternsName configure -state normal

    FillListBoxLayerPatterns

    $layerPatternsWin.buttonAdd configure -text "Add" -command "AddLayerPatterns"    
    $layerPatternsWin.textFieldLayerPatternsName insert 0 "Layer_Pattern_[expr [llength $layerPatternsList] + 1]"
    $layerPatternsWin.comboBoxLayerPropName current 0
    #set [lindex $layerPropNameList 0]
    $layerPatternsWin.textFieldNumLayers delete [string length [$layerPatternsWin.textFieldNumLayers get]]
}

proc GetNumLayers { layerPattIndex  layerPropIndex } {

    global layerPatternsList

    set layerPatt [lindex $layerPatternsList $layerPattIndex]

    return [expr [lindex $layerPatt [expr $layerPropIndex - 1]] - [lindex $layerPatt [expr $layerPropIndex - 2]] + 1]
}

proc CreateLayerPatternsList {} {

    global PROJECTPATH layerPatternsList

    set layerPattFile [open "$PROJECTPATH\\femix_layer_patterns.dat" "r"]
    set layerPattData [read $layerPattFile]
    close $layerPattFile 

    set layerPatternsList {}
    set index 0

    while { $index < [llength $layerPattData] } {
        set layerPatt {}
        lappend layerPatt [lindex $layerPattData $index]
        set index [expr $index + 1]
        while { $index < [llength $layerPattData] && [string is integer [lindex $layerPattData $index]] } {
            lappend layerPatt [lindex $layerPattData $index]
            lappend layerPatt [lindex $layerPattData [expr $index + 1]]
            lappend layerPatt [lindex $layerPattData [expr $index + 2]]
            set index [expr $index + 3]
        }
        lappend layerPatternsList $layerPatt
    }
    #global PROJECTPATH    
    #set result [open "$PROJECTPATH\\result.dat" "w"]
    #puts $result $layerPatternsList
    #puts $result [lindex [lindex $layerPatternsList 0] 0]
    #close $result
    
    destroy $layerPattData $layerPattFile
}

proc CreateFemixLayerPatternsFile {} {

    global PROJECTPATH layerPatternsList layerPatternsWin
 
    set layerPattFile [open "$PROJECTPATH\\femix_layer_patterns.dat" "w"]

    for {set i 0} {$i < [llength $layerPatternsList]} {incr i} {
        puts $layerPattFile [lindex $layerPatternsList $i]
    }

    close $layerPattFile
    destroy $layerPatternsWin
}

proc AddLayerPatterns {} {

    global layerPatternsList layerPatternsWin 
    #layerPropNameList

    set layerPattName [ReplaceSpaceWithUnderscore [$layerPatternsWin.textFieldLayerPatternsName get]]
    if { $layerPattName == "" } {
        OpenWarningWindow "Layer Patterns" "Insert a layer pattern name" 0
        return
    }

    set numLayers [$layerPatternsWin.textFieldNumLayers get]
    if { $numLayers == "" } {
        OpenWarningWindow "Layer Patterns" "Insert number of layers" 0
        return
    }

    set layerProp [$layerPatternsWin.comboBoxLayerPropName get]
    set layerPattIndex [GetLayerPatternsIndex $layerPattName]

    if { $layerPattIndex < 0 } {
        set layerPatt {}
        lappend layerPatt $layerPattName 
        lappend layerPatt "1"
        lappend layerPatt $numLayers
        lappend layerPatt $layerProp
        lappend layerPatternsList $layerPatt
        $layerPatternsWin.listBoxLayerPatterns insert end $layerPattName
        if { $numLayers == "1"} {
            $layerPatternsWin.listBoxLayerPatterns insert end ">  $numLayers $layerProp"    
        } else {
            $layerPatternsWin.listBoxLayerPatterns insert end ">  \[1\-$numLayers\] $layerProp"
        }
    } else {
        set layerPatt [lindex $layerPatternsList $layerPattIndex]
        lappend layerPatt [expr [lindex $layerPatt [expr [llength $layerPatt] - 2]] + 1]
        lappend layerPatt [expr [lindex $layerPatt [expr [llength $layerPatt] - 3]] + $numLayers] 
        lappend layerPatt $layerProp
        set layerPatternsList [lreplace $layerPatternsList $layerPattIndex $layerPattIndex $layerPatt]
        #$layerPatternsWin.listBoxLayerPatterns delete 0 [$layerPatternsWin.listBoxLayerPatterns size]
        FillListBoxLayerPatterns
    }
    $layerPatternsWin.comboBoxLayerPropName current 0
    #set [lindex $layerPropNameList 0]
    $layerPatternsWin.textFieldNumLayers delete 0 [string length [$layerPatternsWin.textFieldNumLayers get]]

}

proc FillListBoxLayerPatterns {} {

    global layerPatternsList layerPatternsWin layerPropName

    $layerPatternsWin.listBoxLayerPatterns delete 0 [$layerPatternsWin.listBoxLayerPatterns size]

    for {set i 0} {$i < [llength $layerPatternsList]} {incr i} {
        $layerPatternsWin.listBoxLayerPatterns insert end [lindex [lindex $layerPatternsList $i] 0]
        for {set j 1} {$j < [llength [lindex $layerPatternsList $i]]} {incr j 3} {
            if { [lindex [lindex $layerPatternsList $i] $j] == [lindex [lindex $layerPatternsList $i] [expr $j + 1]] } {
                $layerPatternsWin.listBoxLayerPatterns insert end ">  [lindex [lindex $layerPatternsList $i] $j] [lindex [lindex $layerPatternsList $i] [expr $j + 2]]"
            } else {
                $layerPatternsWin.listBoxLayerPatterns insert end ">  \[[lindex [lindex $layerPatternsList $i] $j]-[lindex [lindex $layerPatternsList $i] [expr $j + 1]]\] [lindex [lindex $layerPatternsList $i] [expr $j + 2]]"
            }
        }
    }
}

proc GetLayerPatternsIndex { name } {

    global layerPatternsList

    for {set i 0} {$i < [llength $layerPatternsList]} {incr i} {
        if { [lindex [lindex $layerPatternsList $i] 0] == $name } {
            return $i
        }
    }
    return -1
}

proc CloseLayerPatternsWin {} {

    global layerPatternsList layerPatternsWin

    set layerPatternsList {}
    destroy $layerPatternsWin
}

proc NumOfLayerPatterns {} {

    global PROJECTPATH \
           layerPatternsList

    if { ![file exists "$PROJECTPATH\\femix_layer_patterns.dat"] } { 
        return 0 
    }

    CreateLayerPatternsList

    return [llength $layerPatternsList]
}

proc WriteLayerPatterns {} {

    global layerPatternsList

    set layerPatterns ""

    for {set i 0} {$i < [llength $layerPatternsList]} {incr i} {

    	set layerPatt [lindex $layerPatternsList $i]
    	if { [llength $layerPatt] > 3 } {
	    	set index 1
        	append layerPatterns "[format %5d [expr $i + 1]]  "
        	append layerPatterns "[lindex $layerPatt 0]  "
        	append layerPatterns "[format %5d [lindex $layerPatt [expr [llength $layerPatt] - 2]]]  "
        	while { $index < [llength $layerPatt] } {
        		if { [lindex $layerPatt $index] == [lindex $layerPatt [expr $index + 1]] } {
		        	append layerPatterns "[lindex $layerPatt $index]  "
        		} else {
        			append layerPatterns "\[[lindex $layerPatt $index]\-[lindex $layerPatt [expr $index + 1]]\]  "
        		}
        		append layerPatterns "[lindex $layerPatt [expr $index + 2]]  "
        		set index [expr $index + 3]
        	}
        	append layerPatterns ";\n"
    	}
    }
    return $layerPatterns
}
