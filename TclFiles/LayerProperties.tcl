# LayerProperties.tcl
#
# proc OpenLayerPropertiesWindow {}
# proc WriteLaminateThicknessesInLayerProperties { index }
# proc WriteGeometryPatternsInLayerProperties { index }
# proc NumOfGeometryPatternsInLayerProperties {}
# proc NumOfLinearIsotropicInLayerPropertis {}
# proc NumOfLaminateThicknessesInLayerProperties {}
# proc NumOfLayerProperties {}
# proc WriteLayerProperties {}
# proc CloseLayerPropWin {}
# proc RemoveLayerProperties {}
# proc EditLayerPropertiesParameters {}
# proc UpdateLayerProperties { selectedIndex } 
# proc CreateLayerPropertiesList {}
# proc FillListBoxLayerProperties {}
# proc CreateFemixLayerPropertiesFile {}
# proc AddLayerProperties {}
# proc GetLayerPropParameters {}
# proc ResetLayerPropWin {}
# proc SetMaterialNameList { materialType }

proc OpenLayerPropertiesWindow {} {

    global PROBLEMTYPEPATH PROJECTPATH
    global layerPropertiesList layerPropWin materialTypeList materialNameList geometryTypeList
    global numThicknesses

    if { [GidUtils::AreWindowsDisabled] } {
       return
    }      
    if { $PROBLEMTYPEPATH == $PROJECTPATH } {
        OpenWarningWindow "Warning" "Save the project before edit layer properties" 1
        return
    }

    if { [file exists "$PROJECTPATH\\femix_layer_properties.dat"] } {
        CreateLayerPropertiesList
    } 

    set layerPropWin .gid.layerpropertieswindow
    InitWindow $layerPropWin [= "Layer Properties"] "" "" "" 1
    if { ![winfo exists $layerPropWin] } {
       return
    }

    set materialTypeList { "Linear Isotropic" "NLMM101" "NLMM104" "NLMM111" "NLMM141" "NLMM151" "NLMM201" }

    SetMaterialNameList "LIN_ISO"
    
    set geometryTypeList { "NONE" "LAMINATE_THICKNESSES" "GEOMETRY_PATTERNS" }
    set numThicknesses 0

    label $layerPropWin.labelLayerPropName    -text "Layer Properties Name"
    label $layerPropWin.labelMaterialType     -text "Material Type"
    label $layerPropWin.labelMaterialName     -text "Material Name"
    label $layerPropWin.labelGeometryType     -text "Geometry Type"

    entry $layerPropWin.textFieldLayerPropName -textvariable "" -width 30

    ttk::combobox $layerPropWin.comboBoxMaterialType -values $materialTypeList -width 27
    $layerPropWin.comboBoxMaterialType set [lindex $materialTypeList 0]

    ttk::combobox $layerPropWin.comboBoxMaterialName -values $materialNameList -width 27
    $layerPropWin.comboBoxMaterialName set [lindex $materialNameList 0]

    ttk::combobox $layerPropWin.comboBoxGeometryType -values $geometryTypeList -width 27
    $layerPropWin.comboBoxGeometryType set [lindex $geometryTypeList 0]

    listbox $layerPropWin.listBoxLayerProp -width 40

    FillListBoxLayerProperties

    button $layerPropWin.buttonEdit -text "Edit" -command "EditLayerPropertiesParameters" -width 14
    button $layerPropWin.buttonRemove -text "Remove" -command "RemoveLayerProperties" -width 14
    button $layerPropWin.buttonAccept -text "Accept" -command "CreateFemixLayerPropertiesFile" -width 14
    button $layerPropWin.buttonClose -text "Close" -command "CloseLayerPropWin" -width 14
    button $layerPropWin.buttonAdd -text "Add" -command "AddLayerProperties" -width 14

    grid $layerPropWin.labelLayerPropName -row 0 -column 0 -padx 10 -sticky w
    grid $layerPropWin.labelMaterialType  -row 1 -column 0 -padx 10 -sticky w
    grid $layerPropWin.labelMaterialName  -row 2 -column 0 -padx 10 -sticky w
    grid $layerPropWin.labelGeometryType  -row 3 -column 0 -padx 10 -sticky w
    
    grid $layerPropWin.textFieldLayerPropName -row 0 -column 1 -columnspan 3 -padx 10 -sticky e
    grid $layerPropWin.comboBoxMaterialType   -row 1 -column 1 -columnspan 3 -padx 10 -sticky e
    grid $layerPropWin.comboBoxMaterialName   -row 2 -column 1 -columnspan 3 -padx 10 -sticky e
    grid $layerPropWin.comboBoxGeometryType   -row 3 -column 1 -columnspan 3 -padx 10 -sticky e

    grid $layerPropWin.listBoxLayerProp -row 0 -column 4 -columnspan 2 -rowspan 4 -padx 10 -pady 10

    grid $layerPropWin.buttonEdit -row 4 -column 4 -padx 10 -sticky w
    grid $layerPropWin.buttonRemove -row 4 -column 5 -padx 10 -sticky e

    grid rowconfigure $layerPropWin 5 -weight 1
    grid $layerPropWin.buttonAccept -row 5 -column 4 -padx 10 -pady 10 -sticky sw
    grid $layerPropWin.buttonClose -row 5 -column 5 -padx 10 -pady 10 -sticky se
    # -padx 10 -pady 5 -sticky e
    grid $layerPropWin.buttonAdd -row 5 -column 0 -padx 10 -pady 10 -sticky sw

    bind $layerPropWin.comboBoxMaterialType <<ComboboxSelected>> {

        global layerPropWin materialNameList

        if { [$layerPropWin.comboBoxMaterialType get] == "Linear Isotropic" } {
            SetMaterialNameList "LIN_ISO"
        } else {
            SetMaterialNameList [$layerPropWin.comboBoxMaterialType get]
        }

        destroy $layerPropWin.comboBoxMaterialName

        ttk::combobox $layerPropWin.comboBoxMaterialName -values $materialNameList -width 27
        $layerPropWin.comboBoxMaterialName set [lindex $materialNameList 0]

        grid $layerPropWin.comboBoxMaterialName -row 2 -column 1 -columnspan 3 -padx 10 -sticky w

    }

    bind $layerPropWin.comboBoxGeometryType <<ComboboxSelected>> {

        global layerPropWin numThicknesses

        if { $numThicknesses > 0 } {
            destroy $layerPropWin.labelThickness
        }
        for {set i 0} {$i < $numThicknesses} {incr i} {
            destroy $layerPropWin.textFieldThickness($i)
        }
        if { $numThicknesses > 1 } {
            destroy $layerPropWin.labelNumNodes
            destroy $layerPropWin.comboBoxNumNodes
        }
        destroy $layerPropWin.buttonAdd
        destroy $layerPropWin.buttonAccept
        destroy $layerPropWin.buttonClose

        switch [$layerPropWin.comboBoxGeometryType get] {
            
            "NONE" {
                set numThicknesses 0
            }
            "LAMINATE_THICKNESSES" {
                label $layerPropWin.labelThickness -text "Thickness"
                entry $layerPropWin.textFieldThickness(0) -textvariable "" -width 14
                grid $layerPropWin.labelThickness -row 4 -padx 10 -column 1 -sticky w
                grid $layerPropWin.textFieldThickness(0) -row 4 -column 2 -columnspan 2 -padx 10 -sticky e
                set numThicknesses 1
            }
            "GEOMETRY_PATTERNS" {

                label $layerPropWin.labelNumNodes -text "Number of Nodes"
                ttk::combobox $layerPropWin.comboBoxNumNodes -values { 4 8 } -width 3
                set numThicknesses 4
                $layerPropWin.comboBoxNumNodes set $numThicknesses
                grid $layerPropWin.labelNumNodes    -row 4 -column 1 -columnspan 2 -pady 5 -padx 10 -sticky w
                grid $layerPropWin.comboBoxNumNodes -row 4 -column 3 -pady 5 -padx 10 -sticky e

                label $layerPropWin.labelThickness -text "Thicknesses" 
                grid $layerPropWin.labelThickness -row 5 -column 1 -padx 10 -sticky w
                for {set i 0} {$i < $numThicknesses} {incr i} {
                    #label $layerPropWin.labelThickness($i) -text "Thickness ([expr $i + 1])" 
                    entry $layerPropWin.textFieldThickness($i) -textvariable "" -width 14
                    #grid $layerPropWin.labelThickness($i) -row [expr $i + 5] -column 1 -padx 10 -sticky w
                    grid $layerPropWin.textFieldThickness($i) -row [expr $i + 5] -column 2 -columnspan 2 -padx 10 -sticky e           
                }  
                
                bind $layerPropWin.comboBoxNumNodes <<ComboboxSelected>> {

                    global layerPropWin numThicknesses

                    destroy $layerPropWin.labelThickness
                    for {set i 0} {$i < $numThicknesses} {incr i} {
                        #destroy $layerPropWin.labelThickness($i)
                        destroy $layerPropWin.textFieldThickness($i)
                    }
                        
                    set numThicknesses [$layerPropWin.comboBoxNumNodes get]
            
                    label $layerPropWin.labelThickness -text "Thicknesses"
                    grid $layerPropWin.labelThickness -row 5 -column 1 -padx 10 -sticky w
                    for {set i 0} {$i < $numThicknesses} {incr i} {
                        #label $layerPropWin.labelThickness($i) -text "Thickness ([expr $i + 1])"
                        entry $layerPropWin.textFieldThickness($i) -textvariable "" -width 14
                        #grid $layerPropWin.labelThickness($i) -row [expr $i + 5] -column 1 -padx 10 -sticky w
                        grid $layerPropWin.textFieldThickness($i) -row [expr $i + 5] -column 2 -columnspan 2 -padx 10 -sticky e
                    }
                    destroy $layerPropWin.buttonAdd
                    destroy $layerPropWin.buttonAccept
                    destroy $layerPropWin.buttonClose
                    button $layerPropWin.buttonAdd -text "Add" -command "AddLayerProperties" -width 14
                    grid $layerPropWin.buttonAdd -row [expr $numThicknesses + 5] -column 0 -padx 10 -pady 10 -sticky sw        
                    button $layerPropWin.buttonAccept -text "Accept" -command "CreateFemixLayerPropertiesFile" -width 14
                    grid $layerPropWin.buttonAccept -row [expr $numThicknesses + 5] -column 4 -padx 10 -pady 10 -sticky sw 
                    button $layerPropWin.buttonClose -text "Close" -command "CloseLayerPropWin" -width 14
                    grid $layerPropWin.buttonClose -row [expr $numThicknesses + 5] -column 5 -padx 10 -pady 10 -sticky se         
                }
            }
        }
        button $layerPropWin.buttonAdd -text "Add" -command "AddLayerProperties" -width 14
        button $layerPropWin.buttonClose -text "Close" -command "CloseLayerPropWin" -width 14
        button $layerPropWin.buttonAccept -text "Accept" -command "CreateFemixLayerPropertiesFile" -width 14
        if { $numThicknesses > 1 } {
            grid $layerPropWin.buttonAdd -row [expr $numThicknesses + 5] -column 0 -padx 10 -pady 10 -sticky sw
            grid $layerPropWin.buttonAccept -row [expr $numThicknesses + 5] -column 4 -padx 10 -pady 10 -sticky sw        
            grid $layerPropWin.buttonClose -row [expr $numThicknesses + 5] -column 5 -padx 10 -pady 10 -sticky se        
        } else {
            grid $layerPropWin.buttonAdd -row 5 -column 0 -padx 10 -pady 10 -sticky sw
            grid $layerPropWin.buttonAccept -row 5 -column 4 -padx 10 -pady 10 -sticky sw        
            grid $layerPropWin.buttonClose -row 5 -column 5 -padx 10 -pady 10 -sticky se
        }
    }
}

proc WriteLaminateThicknessesInLayerProperties { index } {

    global layerPropertiesList

    set laminateThicknesses ""

    for {set i 0} {$i < [llength $layerPropertiesList]} {incr i} {
        set layerPropName [lindex [lindex $layerPropertiesList $i] 0]
        set geometryType [lindex [lindex $layerPropertiesList $i] 3]
        if { $geometryType == "LAMINATE_THICKNESSES" } {
            append laminateThicknesses [format %7d $index]
            append laminateThicknesses "  $layerPropName"
            append laminateThicknesses "_LAM_THICK  "
            append laminateThicknesses [lindex [lindex $layerPropertiesList $i] 4]
            append laminateThicknesses " ;\n"
            set index [expr $index + 1]
        } elseif { $geometryType == "GEOMETRY_PATTERNS" } {
            set numNodes [lindex [lindex $layerPropertiesList $i] 4]
            for {set j 1} {$j <= $numNodes} {incr j} {
                append laminateThicknesses [format %7d $index]
                append laminateThicknesses "  $layerPropName"
                append laminateThicknesses "_LAM_THICK_$j  "
                append laminateThicknesses [lindex [lindex $layerPropertiesList $i] [expr $j + 4]]
                append laminateThicknesses " ;\n"
                set index [expr $index + 1]
            }
        }
    }
    return $laminateThicknesses  
}

proc WriteGeometryPatternsInLayerProperties { index } {

    global layerPropertiesList

    set geometryPatterns ""

    for {set i 0} {$i < [llength $layerPropertiesList]} {incr i} {
    
        if { [lindex [lindex $layerPropertiesList $i] 3] == "GEOMETRY_PATTERNS" } {
            set layerPropName [lindex [lindex $layerPropertiesList $i] 0]
            #append layerPropName "_GEOM_PATT"
            append laminateThicknesses [format %7d $index]
            append laminateThicknesses "  $layerPropName"
            append laminateThicknesses "_GEOM_PATT  "
            append laminateThicknesses "_LAM_THICK  "
            set numNodes [lindex [lindex $layerPropertiesList $i] 4]
            append laminateThicknesses "$numNodes  "
            for {set j 1} {$j <= $numNodes} {incr j} {
                append laminateThicknesses $layerPropName
                append laminateThicknesses "_LAM_THICK_"
                append laminateThicknesses "$j "
            }
            append laminateThicknesses ";\n"
            set index [expr $index + 1]
        }
    }        
    return $laminateThicknesses
}

proc NumOfGeometryPatternsInLayerProperties {} {

    global layerPropertiesList

    set count 0 

    for {set i 0} {$i < [llength $layerPropertiesList]} {incr i} {
        if { [lindex [lindex $layerPropertiesList $i] 3] == "GEOMETRY_PATTERNS" } {
            set count [expr $count + 1]
        }
    }

    return $count    
}

proc NumOfLaminateThicknessesInLayerProperties {} {
    
    global PROJECTPATH 
    global layerPropertiesList
    
    if { ![file exists "$PROJECTPATH\\femix_layer_properties.dat"] } { 
        return 0 
    }
    CreateLayerPropertiesList

    set count 0

    for {set i 0} {$i < [llength $layerPropertiesList]} {incr i} {
        set geometryType [lindex [lindex $layerPropertiesList $i] 3]
        if { $geometryType == "LAMINATE_THICKNESSES" } {
            set count [expr $count + 1]
        } elseif { $geometryType == "GEOMETRY_PATTERNS" } {
            set count [expr $count + [lindex [lindex $layerPropertiesList $i] 4]]
        }
    }

    return $count
}

proc NumOfLayerProperties {} {

    global PROJECTPATH 
    global layerPropertiesList
    
    #if { $PROBLEMTYPEPATH == $PROJECTPATH } {
    #    #OpenWarningWindow "Warning" "Save the project before calculate" 1
    #    return 0
    #}
    if { ![file exists "$PROJECTPATH\\femix_layer_properties.dat"] } { 
        return 0 
    }
    #CreateLayerPropertiesList
    return [llength $layerPropertiesList]
}

proc WriteLayerProperties {} {

    global layerPropertiesList

    set layerProperties ""

    for {set i 0} {$i < [llength $layerPropertiesList]} {incr i} {

        append layerProperties "[format %7d [expr $i + 1]]  "
        set layerPropName [lindex [lindex $layerPropertiesList $i] 0]
        append layerProperties "$layerPropName  "
        set materialType [lindex [lindex $layerPropertiesList $i] 1]
        if { $materialType == "Linear Isotropic" } {
            append layerProperties "_LIN_ISO  "    
        } else {
            append layerProperties "_$materialType  "
        }
        append layerProperties "[lindex [lindex $layerPropertiesList $i] 2]  "
        set geometryType [lindex [lindex $layerPropertiesList $i] 3]
        if { $geometryType == "LAMINATE_THICKNESSES" } {
            append layerPropName "_LAM_THICK"
            append layerProperties "_LAM_THICK  $layerPropName "
        } elseif { $geometryType == "GEOMETRY_PATTERNS" } {
            append layerPropName "_GEOM_PATT"
            append layerProperties "_GEOM_PATT  $layerPropName "
        } else {
            append layerProperties "_NONE "
        }
        append layerProperties ";\n"
    }
    return $layerProperties
}

proc CloseLayerPropWin {} {

    global layerPropertiesList layerPropWin

    set layerPropertiesList {}
    destroy $layerPropWin
}

proc RemoveLayerProperties {} {

    global layerPropWin layerPropertiesList

    set selectedLayerPropIndex [$layerPropWin.listBoxLayerProp curselection]

    if { $selectedLayerPropIndex == "" } {
        return
    }

    set layerPropertiesList [lreplace $layerPropertiesList $selectedLayerPropIndex $selectedLayerPropIndex]

    $layerPropWin.listBoxLayerProp delete $selectedLayerPropIndex
}

proc EditLayerPropertiesParameters {} {

    global layerPropWin layerPropertiesList numThicknesses

    set selectedLayerPropIndex [$layerPropWin.listBoxLayerProp curselection]

    if { $selectedLayerPropIndex == "" } {
        return
    }

    set layerProp [lindex $layerPropertiesList $selectedLayerPropIndex]

    $layerPropWin.textFieldLayerPropName delete 0 [string length [$layerPropWin.textFieldLayerPropName get]]
    $layerPropWin.textFieldLayerPropName insert 0 [lindex $layerProp 0]
    set materialType [lindex $layerProp 1]
    if { $materialType == "LIN_ISO" } {
        $layerPropWin.comboBoxMaterialType set "Linear Isotropic"    
    } else {
        $layerPropWin.comboBoxMaterialType set $materialType    
    }
    $layerPropWin.comboBoxMaterialName set [lindex $layerProp 2]
    set geometryType [lindex $layerProp 3]
    $layerPropWin.comboBoxGeometryType set $geometryType

    if { $numThicknesses > 0 } {
        destroy $layerPropWin.labelThickness
    }
    for {set i 0} {$i < $numThicknesses} {incr i} {
        destroy $layerPropWin.textFieldThickness($i)
    }
    if { $numThicknesses > 1 } {
        destroy $layerPropWin.labelNumNodes
        destroy $layerPropWin.comboBoxNumNodes
    }
    destroy $layerPropWin.buttonAdd
    destroy $layerPropWin.buttonAccept
    destroy $layerPropWin.buttonClose

    switch $geometryType {

        "NONE" {
            set numThicknesses 0
        }
        "LAMINATE_THICKNESSES" {
            label $layerPropWin.labelThickness -text "Thickness"
            entry $layerPropWin.textFieldThickness(0) -textvariable "" -width 14
            $layerPropWin.textFieldThickness(0) insert 0 [lindex $layerProp 4]
            grid $layerPropWin.labelThickness -row 4 -padx 10 -column 1 -sticky w
            grid $layerPropWin.textFieldThickness(0) -row 4 -column 2 -columnspan 2 -padx 10 -sticky e
            set numThicknesses 1
        }
        "GEOMETRY_PATTERNS" {
            label $layerPropWin.labelNumNodes -text "Number of Nodes"
            ttk::combobox $layerPropWin.comboBoxNumNodes -values { 4 8 } -width 3
            set numThicknesses [lindex $layerProp 4]
            $layerPropWin.comboBoxNumNodes set $numThicknesses
            grid $layerPropWin.labelNumNodes    -row 4 -column 1 -columnspan 2 -pady 5 -padx 10 -sticky w
            grid $layerPropWin.comboBoxNumNodes -row 4 -column 3 -pady 5 -padx 10 -sticky e
            label $layerPropWin.labelThickness -text "Thicknesses" 
            grid $layerPropWin.labelThickness -row 5 -column 1 -padx 10 -sticky w

            for {set i 0} {$i < $numThicknesses} {incr i} {
                entry $layerPropWin.textFieldThickness($i) -textvariable "" -width 14
                $layerPropWin.textFieldThickness($i) insert 0 [lindex $layerProp [expr 5 + $i]]
                grid $layerPropWin.textFieldThickness($i) -row [expr $i + 5] -column 2 -columnspan 2 -padx 10 -sticky e           
            }  
            
            bind $layerPropWin.comboBoxNumNodes <<ComboboxSelected>> {

                global layerPropWin numThicknesses

                destroy $layerPropWin.labelThickness

                for {set i 0} {$i < $numThicknesses} {incr i} {
                    destroy $layerPropWin.textFieldThickness($i)
                }
                    
                set numThicknesses [$layerPropWin.comboBoxNumNodes get]
        
                label $layerPropWin.labelThickness -text "Thicknesses"
                grid $layerPropWin.labelThickness -row 5 -column 1 -padx 10 -sticky w
                for {set i 0} {$i < $numThicknesses} {incr i} {
                    entry $layerPropWin.textFieldThickness($i) -textvariable "" -width 14
                    grid $layerPropWin.textFieldThickness($i) -row [expr $i + 5] -column 2 -columnspan 2 -padx 10 -sticky e
                }
                destroy $layerPropWin.buttonAdd
                destroy $layerPropWin.buttonAccept
                destroy $layerPropWin.buttonClose
                button $layerPropWin.buttonAdd -text "Add" -command "AddLayerProperties" -width 14
                grid $layerPropWin.buttonAdd -row [expr $numThicknesses + 5] -column 0 -padx 10 -pady 10 -sticky sw        
                button $layerPropWin.buttonAccept -text "Accept" -command "CreateFemixLayerPropertiesFile" -width 14
                grid $layerPropWin.buttonAccept -row [expr $numThicknesses + 5] -column 4 -padx 10 -pady 10 -sticky sw 
                button $layerPropWin.buttonClose -text "Close" -command "destroy $layerPropWin" -width 14
                grid $layerPropWin.buttonClose -row [expr $numThicknesses + 5] -column 5 -padx 10 -pady 10 -sticky se         
            }
        }
    }
    button $layerPropWin.buttonAdd -text "Update" -command "UpdateLayerProperties $selectedLayerPropIndex" -width 14
    button $layerPropWin.buttonClose -text "Close" -command "destroy $layerPropWin" -width 14
    button $layerPropWin.buttonAccept -text "Accept" -command "CreateFemixLayerPropertiesFile" -width 14
    if { $numThicknesses > 1 } {
        grid $layerPropWin.buttonAdd -row [expr $numThicknesses + 5] -column 0 -padx 10 -pady 10 -sticky sw
        grid $layerPropWin.buttonAccept -row [expr $numThicknesses + 5] -column 4 -padx 10 -pady 10 -sticky sw        
        grid $layerPropWin.buttonClose -row [expr $numThicknesses + 5] -column 5 -padx 10 -pady 10 -sticky se        
    } else {
        grid $layerPropWin.buttonAdd -row 5 -column 0 -padx 10 -pady 10 -sticky sw
        grid $layerPropWin.buttonAccept -row 5 -column 4 -padx 10 -pady 10 -sticky sw        
        grid $layerPropWin.buttonClose -row 5 -column 5 -padx 10 -pady 10 -sticky se
    }
}

proc UpdateLayerProperties { selectedIndex } {

    global layerPropertiesList layerPropWin

    set layerPropertiesList [lreplace $layerPropertiesList $selectedIndex $selectedIndex [GetLayerPropParameters]]
    $layerPropWin.listBoxLayerProp delete $selectedIndex
    $layerPropWin.listBoxLayerProp insert $selectedIndex [$layerPropWin.textFieldLayerPropName get]
    ResetLayerPropWin 
}

proc CreateLayerPropertiesList {} {

    global PROJECTPATH \
           layerPropertiesList \
           layerPropWin

    set layerPropFile [open "$PROJECTPATH\\femix_layer_properties.dat" "r"]
    set layerPropData [read $layerPropFile]
    close $layerPropFile 

    set layerPropertiesList {}
    set index 0

    while { $index < [llength $layerPropData] } {

        set layerProp {}
        set layerPropName [lindex $layerPropData $index]
        lappend layerProp $layerPropName
        lappend layerProp [lindex $layerPropData [expr $index + 1]]
        lappend layerProp [lindex $layerPropData [expr $index + 2]]
        set geometryType [lindex $layerPropData [expr $index + 3]]
        lappend layerProp $geometryType
        if { $geometryType == "LAMINATE_THICKNESSES" } {
            lappend layerProp [lindex $layerPropData [expr $index + 4]]
            set index [expr $index + 5]
        } elseif { $geometryType == "GEOMETRY_PATTERNS" } {
            set numNodes [lindex $layerPropData [expr $index + 4]]
            lappend layerProp $numNodes
            for {set i 0} {$i < $numNodes} {incr i} {
                lappend layerProp [lindex $layerPropData [expr $index + 5 + $i]]
            }
            set index [expr $index + 5 + $i]
        } else {
            set index [expr $index + 4]
        }
        
        lappend layerPropertiesList $layerProp
    }
    destroy $layerPropFile $layerPropData
}

proc FillListBoxLayerProperties {} {

    global layerPropertiesList \
           layerPropWin

    for {set i 0} {$i < [llength $layerPropertiesList]} {incr i} {
        $layerPropWin.listBoxLayerProp insert end [lindex [lindex $layerPropertiesList $i] 0]
    }
}

proc CreateFemixLayerPropertiesFile {} {

    global PROJECTPATH
    global layerPropWin layerPropertiesList

    set layerPropFile [open "$PROJECTPATH\\femix_layer_properties.dat" "w"]
    #puts $layerPropFile [llength $layerPropertiesList]
    for {set i 0} {$i < [llength $layerPropertiesList]} {incr i} {
        puts $layerPropFile [lindex $layerPropertiesList $i]   
    }
    close $layerPropFile

    destroy $layerPropWin
}

proc AddLayerProperties {} {

    #global materialTypeList materialNameList 
    #geometryTypeList
    global layerPropWin layerPropertiesList numThicknesses 
    #layerProp

    set layerPropName [ReplaceSpaceWithUnderscore [$layerPropWin.textFieldLayerPropName get]]

    if { $layerPropName == "" } {
        OpenWarningWindow "Layer Properties" "Insert a layer properties name" 0
        return
    }

    #GetLayerPropParameters
    
    lappend layerPropertiesList [GetLayerPropParameters]
    
    $layerPropWin.listBoxLayerProp insert end $layerPropName

    ResetLayerPropWin    

    set numThicknesses 0
}

proc GetLayerPropParameters {} {

    global layerPropWin numThicknesses
    # layerProp
    set layerProp {}

    lappend layerProp [ReplaceSpaceWithUnderscore [$layerPropWin.textFieldLayerPropName get]]
    if { [$layerPropWin.comboBoxMaterialType get] == "Linear Isotropic" } {
        lappend layerProp "LIN_ISO"    
    } else {
        lappend layerProp [$layerPropWin.comboBoxMaterialType get]
    }
    lappend layerProp [$layerPropWin.comboBoxMaterialName get]
    lappend layerProp [$layerPropWin.comboBoxGeometryType get]

    #set layerProp { $layerPropName $materialType $materialName $geometryType }

    switch [$layerPropWin.comboBoxGeometryType get] {

        "LAMINATE_THICKNESSES" {
            lappend layerProp [$layerPropWin.textFieldThickness(0) get]
        }
        "GEOMETRY_PATTERNS" {
            lappend layerProp [$layerPropWin.comboBoxNumNodes get]
            for {set i 0} {$i < $numThicknesses} {incr i} {
                lappend layerProp [$layerPropWin.textFieldThickness($i) get]
            }
        }
    }
    return $layerProp
}

proc ResetLayerPropWin {} {

    global layerPropWin materialTypeList materialNameList numThicknesses
    #geometryTypeList

    $layerPropWin.textFieldLayerPropName delete 0 [string length [$layerPropWin.textFieldLayerPropName get]]
    $layerPropWin.comboBoxMaterialType set [lindex $materialTypeList 0]
    
    SetMaterialNameList "LIN_ISO"
    destroy $layerPropWin.comboBoxMaterialName
    ttk::combobox $layerPropWin.comboBoxMaterialName -values $materialNameList -width 27
    $layerPropWin.comboBoxMaterialName set [lindex $materialNameList 0]
    grid $layerPropWin.comboBoxMaterialName -row 2 -column 1 -columnspan 3 -padx 10 -sticky w
    
    $layerPropWin.comboBoxGeometryType set "NONE"    
 
    if { $numThicknesses > 0 } {
        destroy $layerPropWin.labelThickness
    }
    for {set i 0} {$i < $numThicknesses} {incr i} {
        destroy $layerPropWin.textFieldThickness($i)
    }
    if { $numThicknesses > 1 } {
        destroy $layerPropWin.labelNumNodes
        destroy $layerPropWin.comboBoxNumNodes
    }
    destroy $layerPropWin.buttonAdd
    destroy $layerPropWin.buttonAccept
    destroy $layerPropWin.buttonClose

    button $layerPropWin.buttonAdd -text "Add" -command "AddLayerProperties" -width 14
    button $layerPropWin.buttonClose -text "Close" -command "destroy $layerPropWin" -width 14
    button $layerPropWin.buttonAccept -text "Accept" -command "CreateFemixLayerPropertiesFile" -width 14
    grid $layerPropWin.buttonAdd -row 5 -column 0 -padx 10 -pady 10 -sticky sw
    grid $layerPropWin.buttonAccept -row 5 -column 4 -padx 10 -pady 10 -sticky sw        
    grid $layerPropWin.buttonClose -row 5 -column 5 -padx 10 -pady 10 -sticky se
}

proc SetMaterialNameList { materialType } {

    global materialNameList

    set materialNameList {}

    set allMaterialNameList [GiD_Info materials]

    for {set i 0} {$i < [llength $allMaterialNameList]} {incr i} {
        if { $materialType == [lindex [GiD_Info materials [lindex $allMaterialNameList $i]] 2] } {
            lappend materialNameList [lindex $allMaterialNameList $i]
        }
    }
}