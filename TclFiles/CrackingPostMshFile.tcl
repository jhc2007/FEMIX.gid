# CrackingPostMshFile.tcl
# 
# proc CreateCrackingPostMshFile { basename }

proc CreateCrackingPostMshFile { basename } {

    global PROJECTPATH numDimension

    set cps3dFileName $basename
    append cps3dFileName "_cp.s3d"

    if { [file exists "$PROJECTPATH\\$cps3dFileName"] } {

        set cpPostMshName $basename
        append cpPostMshName "_cp.post.msh"

        set cps3dFile [open "$PROJECTPATH\\$cps3dFileName" "r"]
        set cps3dData [read $cps3dFile]
        close $cps3dFile

        set index 0

        while { [lindex $cps3dData $index] != "Mesh" || ![string is integer -strict [lindex $cps3dData [expr $index + 1]]] } {
            set index [expr $index + 1]
        }

        set numElems [lindex $cps3dData [expr $index + 1]]
        set numNodes [lindex $cps3dData [expr $index + 2]]

        set index [expr $index + 4]

        set numNodesElem 0
        set numElemsType 0

        for {set i 0} {$i < $numElems} {incr i} {

            set tmpNumNodesElem [lindex $cps3dData [expr $index + 1]]

            if { $tmpNumNodesElem != 0 && $tmpNumNodesElem != $numNodesElem } {
                set numNodesElem $tmpNumNodesElem
                set numElemsType [expr $numElemsType + 1]
                if {$numNodesElem == 2 || $numNodesElem == 3} {
                    set elemsData($numElemsType) [list "Linear" $numNodesElem]
                } elseif {$numNodesElem == 4 || $numNodesElem == 6} {
                    # Interface 2D elements
                    set elemsData($numElemsType) [list "Quadrilateral" 4]
                } elseif {$numNodesElem == 5 || $numNodesElem == 9} {
                    set elemsData($numElemsType) [list "Quadrilateral" [expr $numNodesElem - 1]]
                } else {
                    # $numNodesElem == 8 || $numNodesElem == 20
                    set elemsData($numElemsType) [list "Hexahedra" $numNodesElem]
                } 
            }
            set data [list [lindex $cps3dData $index]] 

            switch $numNodesElem {
                3 {
                    lappend data [lindex $cps3dData [expr $index + 2]]
                    lappend data [lindex $cps3dData [expr $index + 4]]
                    lappend data [lindex $cps3dData [expr $index + 3]]
                }
                4 {
                    lappend data [lindex $cps3dData [expr $index + 2]]
                    lappend data [lindex $cps3dData [expr $index + 3]]
                    lappend data [lindex $cps3dData [expr $index + 5]]
                    lappend data [lindex $cps3dData [expr $index + 4]]
                }
                5 {
                    for {set j 0} { $j < [expr $numNodesElem - 1] } {incr j} {
                        lappend data [lindex $cps3dData [expr $index + 2 + $j]]
                    }
                } 
                6 {
                    lappend data [lindex $cps3dData [expr $index + 2]]
                    lappend data [lindex $cps3dData [expr $index + 4]]
                    lappend data [lindex $cps3dData [expr $index + 7]]
                    lappend data [lindex $cps3dData [expr $index + 5]]
                }
                9 {
                    for {set j 2} { $j <= 8 } {incr j 2} {
                        lappend data [lindex $cps3dData [expr $index + $j]]
                    }
                    for {set j 3} { $j <= 9 } {incr j 2} {
                        lappend data [lindex $cps3dData [expr $index + $j]]
                    }
                } 
                20 {
                    for {set j 2} { $j <= 8 } {incr j 2} {
                        lappend data [lindex $cps3dData [expr $index + $j]]
                    }
                    for {set j 14} { $j <= 20 } {incr j 2} {
                        lappend data [lindex $cps3dData [expr $index + $j]]
                    }
                    for {set j 3} { $j <= 9 } {incr j 2} {
                        lappend data [lindex $cps3dData [expr $index + $j]]
                    }
                    for {set j 10} { $j <= 13 } {incr j} {
                        lappend data [lindex $cps3dData [expr $index + $j]]
                    }
                    for {set j 15} { $j <= 21 } {incr j 2} {
                        lappend data [lindex $cps3dData [expr $index + $j]]
                    }
                }
                default {
                    # 2 or 8
                    for {set j 0} {$j < $numNodesElem} {incr j} {
                        lappend data [lindex $cps3dData [expr $index + 2 + $j]]
                    }
                }
            }

            lappend elemsData($numElemsType) $data

            set index [expr $index + $numNodesElem + 2]
        }

        set cpPostMshFile [open "$PROJECTPATH\\$cpPostMshName" "w"]

        set nodesData {}
        if { $numDimension == 2 } {
            for {set i 0} {$i < $numNodes} {incr i} {
                lappend nodesData [list [lindex $cps3dData $index] \
                                        [lindex $cps3dData [expr $index + 2]] \
                                        [lindex $cps3dData [expr $index + 3]] \
                                        [lindex $cps3dData [expr $index + 1]]]
                set index [expr $index + 4]                        
            }
        } else {
            for {set i 0} {$i < $numNodes} {incr i} {
                lappend nodesData [list [lindex $cps3dData $index] \
                                        [lindex $cps3dData [expr $index + 1]] \
                                        [lindex $cps3dData [expr $index + 2]] \
                                        [lindex $cps3dData [expr $index + 3]]]
                set index [expr $index + 4]                        
            }
        }
        # if { [lindex $elemsData(1) 0] == "Interface" } {
        #     puts $cpPostMshFile "MESH \"Mesh ([lindex $elemsData(1) 0])\" dimension 3 ElemType Quadrilateral Nnode [lindex $elemsData(1) 1]"    
        # } else {
        puts $cpPostMshFile "MESH \"Layer 1\" dimension 3 ElemType [lindex $elemsData(1) 0] Nnode [lindex $elemsData(1) 1]"
        # }
        puts $cpPostMshFile "\nCoordinates\n"
        for {set i 0} {$i < $numNodes} {incr i} {
            puts $cpPostMshFile [lindex $nodesData $i]
        }
        puts $cpPostMshFile "\nEnd Coordinates\n"
        for {set i 1} {$i <= $numElemsType} {incr i} {
            if {$i > 1} {
                # if { [lindex $elemsData($i) 0] == "Interface" } {
                #     puts $cpPostMshFile "MESH \"Mesh ([lindex $elemsData($i) 0])\" dimension 3 ElemType Quadrilateral Nnode [lindex $elemsData($i) 1]"
                # } else {
                puts $cpPostMshFile "MESH \"Layer $i\" dimension 3 ElemType [lindex $elemsData($i) 0] Nnode [lindex $elemsData($i) 1]"
                # }
                puts $cpPostMshFile "\nCoordinates"
                puts $cpPostMshFile "End Coordinates\n"
            }
            puts $cpPostMshFile "Elements\n"
            set dataSize [llength $elemsData($i)]
            for {set j 2} {$j < $dataSize} {incr j} {
                puts $cpPostMshFile [lindex $elemsData($i) $j]
            }      
            puts $cpPostMshFile "\nEnd Elements\n"
        }
        set index 0
        #set result [open "$PROJECTPATH\\result.dat" "w"]
        for {set i 0} {$i < 6} {incr i} {

            while { [lindex $cps3dData $index] != "Crack" || [lindex $cps3dData [expr $index + 1]] != "status:" } {
                set index [expr $index + 1]
            }
            set crackType [string range [lindex $cps3dData [expr $index + 2]] 1 [string length [lindex $cps3dData [expr $index + 2]]]]  
            set numCrackElems [lindex $cps3dData [expr $index + 3]]
            set numCrackNodes [lindex $cps3dData [expr $index + 4]]
            set index [expr $index + 6]

            if { $numCrackElems > 0 && $numCrackNodes > 0 } {

                if { [lindex $cps3dData [expr $index + 1]] == 5 || [lindex $cps3dData [expr $index + 1]] == 9 } {
                    set numNodesElem [expr [lindex $cps3dData [expr $index + 1]] - 1]
                } else {
                    set numNodesElem [lindex $cps3dData [expr $index + 1]]
                }

                if {$numNodesElem == 2 || $numNodesElem == 3} {
                    set elementsData [list "Linear" $numNodesElem]
                } elseif {$numNodesElem == 4 || $numNodesElem == 8} {
                    set elementsData [list "Quadrilateral" $numNodesElem]
                } else {
                    set elementsData [list "Hexahedra" $numNodesElem]
                }

                for {set j 0} {$j < $numCrackElems} {incr j} {

                    set data [list [expr [lindex $cps3dData $index] + $numElems]] 
                    for {set k 0} {$k < $numNodesElem} {incr k} {
                        lappend data [expr [lindex $cps3dData [expr $index + 2 + $k]] + $numNodes]
                    }
                    lappend elementsData $data
                    if { $numNodesElem == 4 || $numNodesElem == 8 } {
                        set index [expr $index + $numNodesElem + 3]
                    } else {
                        set index [expr $index + $numNodesElem + 2]
                    }
                }

                set nodesData {}
                if { $numDimension == 2 } {
                    for {set j 0} {$j < $numCrackNodes} {incr j} {
                        lappend nodesData [list [expr [lindex $cps3dData $index] + $numNodes] \
                                                [lindex $cps3dData [expr $index + 2]] \
                                                [lindex $cps3dData [expr $index + 3]] \
                                                [lindex $cps3dData [expr $index + 1]]]
                        set index [expr $index + 4]                        
                    }
                } else {
                    for {set j 0} {$j < $numCrackNodes} {incr j} {
                        lappend nodesData [list [expr [lindex $cps3dData $index] + $numNodes] \
                                                [lindex $cps3dData [expr $index + 1]] \
                                                [lindex $cps3dData [expr $index + 2]] \
                                                [lindex $cps3dData [expr $index + 3]]]
                        set index [expr $index + 4]                        
                    }
                }
                puts $cpPostMshFile "MESH \"$crackType\" dimension 3 ElemType [lindex $elementsData 0] Nnode [lindex $elementsData 1]"
                puts $cpPostMshFile "\nCoordinates\n"
                for {set j 0} {$j < $numCrackNodes} {incr j} {
                    puts $cpPostMshFile [lindex $nodesData $j]
                }
                puts $cpPostMshFile "\nEnd Coordinates\n"

                puts $cpPostMshFile "Elements\n"
                set dataSize [llength $elementsData]
                for {set j 2} {$j < $dataSize} {incr j} {
                    puts $cpPostMshFile [lindex $elementsData $j]
                }      
                puts $cpPostMshFile "\nEnd Elements\n"

                set numElems [expr $numElems + $numCrackElems]
                set numNodes [expr $numNodes + $numCrackNodes]
            }
            #puts $result $crackType
            #puts $result $numElems
            #puts $result $numNodes
                
            
        }
        #close $result
        close $cpPostMshFile

    }
}