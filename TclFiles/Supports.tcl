proc SetSupportsList { index } {

	global supportsList
	set index [expr $index - 1]
	set supportsList [lreplace $supportsList $index $index 1]

	return 0
}

proc AssignSupportsFromPrescribedDisplacements { node } {

	global supportsList
	
	GiD_Process Mescape Data Conditions AssignCond Constraints_for_Points Change \
	    [lindex $supportsList 0] \
	    [lindex $supportsList 1] \
	    [lindex $supportsList 2] \
	    [lindex $supportsList 3] \
	    [lindex $supportsList 4] \
	    [lindex $supportsList 5] \
    	$node \
    Mescape
	# switch $index {
	# 	1 {
	# 		GiD_Process Data Conditions AssignCond Constraints_for_Points ReplaceField X-Displacement#CB#(0,1) 0 1 $node Mescape
	# 	}
	# 	2 {
	# 		GiD_Process Data Conditions AssignCond Constraints_for_Points ReplaceField Y-Displacement#CB#(0,1) 0 1 $node Mescape
	# 	}
	# 	3 {
	# 		GiD_Process Data Conditions AssignCond Constraints_for_Points ReplaceField Z-Displacement#CB#(0,1) 0 1 $node Mescape
	# 	}
	# 	4 {
	# 		GiD_Process Data Conditions AssignCond Constraints_for_Points ReplaceField X-Rotation#CB#(0,1) 0 1 $node Mescape
	# 	}
	# 	5 {
	# 		GiD_Process Data Conditions AssignCond Constraints_for_Points ReplaceField Y-Rotation#CB#(0,1) 0 1 $node Mescape
	# 	}
	# 	6 {
	# 		GiD_Process Data Conditions AssignCond Constraints_for_Points ReplaceField Z-Rotation#CB#(0,1) 0 1 $node Mescape
	# 	}
	# }
	return 0
}

# proc AssignSupportsFromPrescribedDisplacements { node } {

# 	global supportsList
#     GiD_Process Mescape Data Conditions AssignCond Constraints_for_Points Change \
#     $supportsList $node \
#     Mescape

# 	return 0
# }

# proc CreateSupportsList {} {
# 	global supportsList
# 	set supportsList { 0 0 0 0 0 0 }
# 	return 0
# }

# proc WriteSupports { } {

# 	return [GiD_Info conditions Constraints_for_Points mesh]
# }

proc GetSupportsList { node } {

	global supportsList

	set supportsList [GiD_Info conditions Constraints_for_Points mesh]

	for {set i 0} {$i < [llength $supportsList]} {incr i} {
		if { [lindex [lindex $supportsList $i] 1] == $node } {
			set supportsList [lreplace [lindex $supportsList $i] 0 2]
			return 0
		}
	}
	set supportsList [list 0 0 0 0 0 0]
	return 0
}