# NLMM111.tcl
#
# NumOfNLMM111InLayerProperties {}
# ExistsNLMM111InLayerProperties { matName }
# WriteNLMM111InLayerProperties { index }

proc NumOfNLMM111InLayerProperties {} {

    global layerPropertiesList NLMM111UsedInLayerProp

    set NLMM111UsedInLayerProp {}

    for {set i 0} {$i < [llength $layerPropertiesList]} {incr i} { 
        if { [lindex [lindex $layerPropertiesList $i] 1] == "NLMM111" && ![ExistsListElement $NLMM111UsedInLayerProp [lindex [lindex $layerPropertiesList $i] 2]] } {
            lappend NLMM111UsedInLayerProp [lindex [lindex $layerPropertiesList $i] 2]
        }
    }

    return [llength $NLMM111UsedInLayerProp]    
}

proc ExistsNLMM111InLayerProperties { matName } {

    global NLMM111UsedInLayerProp

    if { [ExistsListElement $NLMM111UsedInLayerProp [ReplaceSpaceWithUnderscore $matName]] } {
        return 1
    }
    return 0
}

proc WriteNLMM111InLayerProperties { index } {

    global NLMM111UsedInLayerProp

    set nlmm111 ""
    set materials [GiD_Info materials]


    #global PROJECTPATH
    #set result [open "$PROJECTPATH\\aaa_result.dat" "w"]
    ##puts $result $materials
    #set mat [GiD_Info materials [lindex $materials 5]]
    ##puts $result $mat
    #for {set i 1} {$i < [llength $mat]} {incr i} {
    #	puts $result [lindex $mat $i]
    #}
    #close $result
    #return

    for {set i 0} {$i < [llength $materials]} {incr i} {

    	set material [GiD_Info materials [lindex $materials $i]]
        if { [lindex $material 2] == "NLMM111" && [ExistsListElement $NLMM111UsedInLayerProp [ReplaceSpaceWithUnderscore [lindex $materials $i]]] } {

        	append nlmm111 "#     A     B     C     D     E     F     G\n"
        	
        	append nlmm111 "[format %7d $index]  "
        	append nlmm111 [ReplaceSpaceWithUnderscore [lindex $materials $i]]
        	for {set j 4} {$j <= 12} {incr j 2} {
                # append nlmm111 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material $j]]]
                append nlmm111 [format %18.8e [lindex $material $j]]
            }

            append nlmm111 "\n#     H     I     J     K     L     M     N     O\n"
            
            # append nlmm111 [format %21.8e [RemoveUnitsFromParameterValue [lindex $material 14]]]
            append nlmm111 [format %21.8e [lindex $material 14]]
            append nlmm111 "  \_[lindex $material 16]"
            for {set j 18} {$j <= 28} {incr j 2} {
                append nlmm111 [format %18.8e [lindex $material $j]]
            }

            append nlmm111 "\n#     P     Q     R     S     T     U     V\n"

            switch [lindex $material 16] {
            	"TRILINEAR" {
            		# append nlmm111 [format %21.8e [RemoveUnitsFromParameterValue [lindex $material 30]]]
                    append nlmm111 [format %21.8e [lindex $material 30]]
            	}
            	"QUADRILINEAR" {
            		# append nlmm111 [format %21.8e [RemoveUnitsFromParameterValue [lindex $material 32]]]
                    append nlmm111 [format %21.8e [lindex $material 32]]
            	}
            	default {
            		# append nlmm111 [format %21.8e [RemoveUnitsFromParameterValue [lindex $material 34]]]
                    append nlmm111 [format %21.8e [lindex $material 34]]
            	}
            }
            append nlmm111 [format %18.8e [lindex $material 36]]
            append nlmm111 "  \_[lindex $material 38]"
			for {set j 40} {$j <= 46} {incr j 2} {
                append nlmm111 [format %18.8e [lindex $material $j]]
            }            

            append nlmm111 "\n#     W     X     Y     Z\n"

            if { [lindex $material 48] == "VALUE" } {
            	append nlmm111 [format %21.8e [lindex $material 50]]
            } else {
            	append nlmm111 "      \_[lindex $material 48]"
            }
            append nlmm111 "  \_[lindex $material 52]"
            # append nlmm111 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material 54]]]
            append nlmm111 [format %18.8e [lindex $material 54]]
            switch [lindex $material 38] {
            	"NONE" {
            		# append nlmm111 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material 56]]]
                    append nlmm111 [format %18.8e [lindex $material 56]]
            	}
            	"LINEAR" {
            		# append nlmm111 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material 56]]]
                    append nlmm111 [format %18.8e [lindex $material 56]]

            	}
            	"TRILINEAR" {
            		# append nlmm111 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material 58]]]
                    append nlmm111 [format %18.8e [lindex $material 58]]
            	}
            	default {
            		# append nlmm111 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material 60]]]
                    append nlmm111 [format %18.8e [lindex $material 60]]
            	}
            }
 
            append nlmm111 "\n#     AA     AB     AC     AD\n"

            append nlmm111 "      \_[lindex $material 62]"
            if { [lindex $material 64] == "VALUE" } {
            	# append nlmm111 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material 66]]]
                append nlmm111 [format %18.8e [lindex $material 66]]
            } else {
            	append nlmm111 "  \_[lindex $material 64]"
            }
            append nlmm111 "  [lindex $material 68]  "  
            # append nlmm111 [RemoveUnitsFromParameterValue [lindex $material 70]]
            append nlmm111 [lindex $material 70]

            append nlmm111 " ;\n"

        	set index [expr $index + 1]
        }
    }
    return $nlmm111
}