# NLMM141FromFemixFile.tcl

proc CreateNLMM141 { femixData } {

    set nlmm141Data [GetBlockData $femixData "NLMM141"]

    if { [llength $nlmm141Data] > 0 } {

    	global doubleFormatMatParameters
        set allMaterialsNameList [GiD_Info materials]
        set nlmm141NameList {}

        for {set i 0} {$i < [llength $allMaterialsNameList]} {incr i} {
            if { [lindex [GiD_Info materials [lindex $allMaterialsNameList $i]] 2] == "NLMM141" } {
                lappend nlmm141NameList [string toupper [lindex $allMaterialsNameList $i]]
            }
        }

    	set numMaterials [lindex $nlmm141Data 2]

    	set index 4

    	for {set i 0} {$i < $numMaterials} {incr i} {

    		set materialName [lindex $nlmm141Data [expr $index + 1]]

    		set materialData { NLMM141 }

    		# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm141Data [expr $index + 2]]] kg*mm^-3"
	    	# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm141Data [expr $index + 3]]] Cel^-1"
            lappend materialData [format $doubleFormatMatParameters [lindex $nlmm141Data [expr $index + 2]]]
            lappend materialData [format $doubleFormatMatParameters [lindex $nlmm141Data [expr $index + 3]]]
	    	lappend materialData [format $doubleFormatMatParameters [lindex $nlmm141Data [expr $index + 4]]]

	        for {set j 5} {$j < 7} {incr j} {
    			# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm141Data [expr $index + $j]]] N*mm^-2"
                lappend materialData [format $doubleFormatMatParameters [lindex $nlmm141Data [expr $index + $j]]]
    		}

    		for {set j 7} {$j < 10} {incr j} {
    			lappend materialData [format $doubleFormatMatParameters [lindex $nlmm141Data [expr $index + $j]]]
    		}

    		# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm141Data [expr $index + 10]]] N*mm^-2"
            lappend materialData [format $doubleFormatMatParameters [lindex $nlmm141Data [expr $index + 10]]]

    		set value [RemoveFirstUnderScore [lindex $nlmm141Data [expr $index + 11]]]
    		lappend materialData $value

    		for {set j 12} {$j < 16} {incr j} {
    			lappend materialData [format $doubleFormatMatParameters [lindex $nlmm141Data [expr $index + $j]]]
    		}

    		switch $value {

    			"TRILINEAR" {
    				# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm141Data [expr $index + 16]]] N*mm^-1"
    				# lappend materialData "0 N*mm^-1"
                    lappend materialData [format $doubleFormatMatParameters [lindex $nlmm141Data [expr $index + 16]]]
                    lappend materialData 0
    			}
    			"CORNELISSEN" {
    				# lappend materialData "0 N*mm^-1"
    				# lappend materialData "[format $doubleFormatMatParameters [lindex $nlmm141Data [expr $index + 16]]] N*mm^-1"
                    lappend materialData 0
                    lappend materialData [format $doubleFormatMatParameters [lindex $nlmm141Data [expr $index + 16]]]
    			}
    		}

    		lappend materialData [format $doubleFormatMatParameters [lindex $nlmm141Data [expr $index + 17]]]

    		if { [string is double [lindex $nlmm141Data [expr $index + 18]]] } {
    			lappend materialData "VALUE"
    			lappend materialData [format $doubleFormatMatParameters [lindex $nlmm141Data [expr $index + 18]]]
    		} else {
    			lappend materialData [RemoveFirstUnderScore [lindex $nlmm141Data [expr $index + 18]]]
    			lappend materialData 0
    		}

    		lappend materialData [RemoveFirstUnderScore [lindex $nlmm141Data [expr $index + 19]]]

    		if { [string is double [lindex $nlmm141Data [expr $index + 20]]] } {
    			lappend materialData "VALUE"
    			lappend materialData [format $doubleFormatMatParameters [lindex $nlmm141Data [expr $index + 20]]]
    		} else {
    			lappend materialData [RemoveFirstUnderScore [lindex $nlmm141Data [expr $index + 20]]]
    			lappend materialData 0
    		}

    		lappend materialData [lindex $nlmm141Data [expr $index + 21]]
    		# lappend materialData "[lindex $nlmm141Data [expr $index + 22]] degree"
            lappend materialData [lindex $nlmm141Data [expr $index + 22]]

    		if { [ExistsListElement $nlmm141NameList [string toupper $materialName]] } {
	    		GiD_ModifyData materials $materialName $materialData
	    	} else {
	    		GiD_CreateData create material NLMM141_Concrete $materialName $materialData
	    	}

        	set index [expr $index + 24]  
    	}
    }
}