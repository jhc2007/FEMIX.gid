# proc CreateSpringMaterialsList {} {

#     global SpringMaterialsList
#     set SpringMaterialsList {}
#     return 0
# }

proc CountSpringMaterials { matType } {

    global SpringMaterialsList

    set count 0

    for {set i 0} {$i < [llength $SpringMaterialsList]} {incr i} {
        if { [lindex [lindex $SpringMaterialsList $i] 0] == $matType } {
            set count [expr $count + 1]
        }
    }
    return $count
}

# proc UsedSpringMaterial { matType matName } {

#     global SpringMaterialsList

#     if { [ExistsListElement $SpringMaterialsList [list $matType $matName] } {
#         return 1
#     }
#     return 0
# }

proc WriteLinearSpringMaterials { } {

	global SpringMaterialsList
    set springMaterials ""
    set index 1

    for {set i 0} {$i < [llength $SpringMaterialsList]} {incr i} {
        if { [lindex [lindex $SpringMaterialsList $i] 0] == "LIN_SPRING" } {
            set matName [lindex [lindex $SpringMaterialsList $i] 1]
            append springMaterials [format %7d $index]
            append springMaterials "  $matName"
            append springMaterials "  [lindex [GiD_Info Material $matName] 4] ;\n"
            incr index
        }
    }

    return $springMaterials
}

proc WriteNLSMM101 {} {

    global SpringMaterialsList

    set springMaterials ""
    set index 1

    for {set i 0} {$i < [llength $SpringMaterialsList]} {incr i} {
        if { [lindex [lindex $SpringMaterialsList $i] 0] == "NLSMM101" } {
            set matName [lindex [lindex $SpringMaterialsList $i] 1]
            append springMaterials [format %7d $index]
            append springMaterials "  $matName"
            set matParameters [GiD_Info Material $matName]
            for {set j 4} {$j < 10} {incr j 2} {
                append springMaterials "  [lindex $matParameters $j]"
            }
            append springMaterials "  _[lindex $matParameters 10] ;\n"
            incr index
        }
    }

    return $springMaterials
}

proc WriteNLSMM102 {} {

    global SpringMaterialsList

    set springMaterials ""
    set index 1

    for {set i 0} {$i < [llength $SpringMaterialsList]} {incr i} {
        if { [lindex [lindex $SpringMaterialsList $i] 0] == "NLSMM102" } {
            set matName [lindex [lindex $SpringMaterialsList $i] 1]
            append springMaterials [format %7d $index]
            append springMaterials "  $matName"
            set matParameters [GiD_Info Material $matName]
            append springMaterials "  _[lindex $matParameters 4]"
            for {set j 6} {$j < 20} {incr j 2} {
                append springMaterials "  [lindex $matParameters $j]"
            }
            append springMaterials " ;\n"
            incr index
        }
    }

    return $springMaterials
}