# PointSprings.tcl

# proc CreatePointSpringsList {} {

# 	global pointSpringsList  
# 	set pointSpringsList {}
# 	return 0
    
# }

proc AddPointSprings { node groupName dirVector xComp yComp zComp location dof matType matName } {

	global PointSpringsList \
           SpringMaterialsList 
           # numDimension
           # AuxiliaryVectorsList \

	if { $dirVector == "Specified" } {
		lappend PointSpringsList [list $node $node $groupName $dirVector $xComp $yComp $zComp $location $dof $matType $matName]
        # set vector [list [format %.5e $xComp] \
        #                  [format %.5e $yComp] \
        #                  [format %.5e $zComp]]
        AddAuxiliaryVector $xComp $yComp $zComp
	} else {
		lappend PointSpringsList [list $node $node $groupName $dirVector $location $dof $matType $matName]	
	}
    if { ![ExistsListElement $SpringMaterialsList [list $matType $matName]] } {
        lappend SpringMaterialsList [list $matType $matName]
    }
	return 0
}

proc UpdatePointSprings {node groupName dirVector xComp yComp zComp location dof matType matName} {

    global PointSpringsList

    set pointSpring [list $xComp $yComp $zComp $location $dof $matType $matName]
    set lastPointSpring [lindex $PointSpringsList end]

    if { $node != [expr [lindex $lastPointSpring 1] + 1] || $groupName != [lindex $lastPointSpring 2] || $dirVector != [lindex $lastPointSpring 3] } {
        AddPointSprings $node $groupName $dirVector $xComp $yComp $zComp $location $dof $matType $matName
        return 0
    }
    if { $dirVector == "Specified" } {
        for {set i 0} {$i < 7} {incr i} {
            if { [lindex $pointSpring $i] != [lindex $lastPointSpring [expr $i + 4]] } {
                AddPointSprings $node $groupName $dirVector $xComp $yComp $zComp $location $dof $matType $matName
                return 0
            }
        }
    } else {
        for {set i 3} {$i < 7} {incr i} {
            if { [lindex $pointSpring $i] != [lindex $lastPointSpring [expr $i + 1]] } {
                AddPointSprings $node $groupName $dirVector $xComp $yComp $zComp $location $dof $matType $matName
                return 0
            }
        }
    }
    set lastPointSpring [lreplace $lastPointSpring 1 1 $node]
    set PointSpringsList [lreplace $PointSpringsList [expr [llength $PointSpringsList] - 1] [expr [llength $PointSpringsList] - 1] $lastPointSpring]
	return 0
}

proc WritePointSpringsList {} {

    global PointSpringsList \
           numDimension

# global PROBLEMTYPEPATH AuxiliaryVectorsList
# set result [open "$PROBLEMTYPEPATH\\result_new.dat" "w"]
# puts $result $AuxiliaryVectorsList
# puts $result $PointSpringsList
# close $result

    set springsList ""
    set index 1

    for {set i 0} {$i < [llength $PointSpringsList]} {incr i} {

        set pointSpring [lindex $PointSpringsList $i]

        if { [lindex $pointSpring 0] == [lindex $pointSpring 1] } {
            append springsList [format %7d $index]
            append springsList "  [lindex $pointSpring 2]  1  "
            append springsList "[lindex $pointSpring 0]  "
            set index [expr $index + 1]
        } else {
            append springsList "  \[$index\-[expr $index + [lindex $pointSpring 1] - [lindex $pointSpring 0]]\]  "
            append springsList "[lindex $pointSpring 2]  1  "
            append springsList "\[[lindex $pointSpring 0]\-[lindex $pointSpring 1]\]  "
            set index [expr $index + [lindex $pointSpring 1] - [lindex $pointSpring 0] + 1]
        }

        if { [lindex $pointSpring 3] == "Specified" } {
            # if { $numDimension == 3 } {
            append springsList "VECTOR_[GetAuxiliaryVectorIndexFromComponents [lindex $pointSpring 4] [lindex $pointSpring 5] [lindex $pointSpring 6]]  "
                # set vector [list [format %.5e [lindex $pointSpring 4]] \
                #                  [format %.5e [lindex $pointSpring 5]] \
                #                  [format %.5e [lindex $pointSpring 6]]]
            # } else {
            #     append springsList "VECTOR_[GetAuxiliaryVectorIndexFromComponents [lindex $pointSpring 6] [lindex $pointSpring 4] [lindex $pointSpring 5]]  "
            #     # set vector [list [format %.5e [lindex $pointSpring 6]] \
            #     #                  [format %.5e [lindex $pointSpring 4]] \
            #     #                  [format %.5e [lindex $pointSpring 5]]]
            # }
            # set auxVectorName [GetAuxiliaryVectorNameFromFemixFile $vector]
            # if { $auxVectorName == "" } {
            # } else {
                # append springsList "$auxVectorName  "
            # }
            if { [lindex $pointSpring 7] == "Structure_to_Ground" } {
                append springsList "_SG  "
            } else {
                append springsList "_GS  "
            }
            if { [lindex $pointSpring 8] == "Translational" } {
                append springsList "_TRANS  "
            } else {
                append springsList "_ROT  "
            }
            append springsList "_[lindex $pointSpring 9]  "
            append springsList "[lindex $pointSpring 10]  ;\n"
        } else {
            if { $numDimension == 3 } {
                append springsList "_[lindex $pointSpring 3]"
                # switch [lindex $pointSpring 3] {
                #     "Global_1" {
                #         append springsList "_XG1  "
                #     }
                #     "Global_2" {
                #         append springsList "_XG2  "   
                #     }
                #     "Global_3" {
                #         append springsList "_XG3  "
                #     }
                # }
            } else {
                append springsList "_XG[expr [string index [lindex $pointSpring 3] end] % 3 + 1]"
                # switch [lindex $pointSpring 3] {
                #     "XG1" {
                #         append springsList "_XG2  "
                #     }
                #     "XG2" {
                #         append springsList "_XG3  "   
                #     }
                #     "XG3" {
                #         append springsList "_XG1  "
                #     }
                # }
            }
            if { [lindex $pointSpring 4] == "Structure_to_Ground" } {
                append springsList "_SG  "
            } else {
                append springsList "_GS  "
            }
            if { [lindex $pointSpring 5] == "Translational" } {
                append springsList "_TRANS  "
            } else {
                append springsList "_ROT  "
            }
            append springsList "_[lindex $pointSpring 6]  "
            append springsList "[lindex $pointSpring 7]  ;\n"
        }
    }
    return $springsList
}

# proc PrintPointSprings {} {

#     global pointSpringsList AuxiliaryVectorsList

#   global PROBLEMTYPEPATH
#   set result [open "$PROBLEMTYPEPATH\\result_new.dat" "w"]
#   puts $result "AQUI::::::::"
#   puts $result $pointSpringsList
#   puts $result $AuxiliaryVectorsList
#   close $result

#   return 0
# }
