# NLMM104.tcl
#
# NumOfNLMM104InLayerProperties {}
# ExistsNLMM104InLayerProperties { matName }
# WriteNLMM104InLayerProperties { index }

proc NumOfNLMM104InLayerProperties {} {

    global layerPropertiesList NLMM104UsedInLayerProp

    set NLMM104UsedInLayerProp {}

    for {set i 0} {$i < [llength $layerPropertiesList]} {incr i} { 
        if { [lindex [lindex $layerPropertiesList $i] 1] == "NLMM104" && ![ExistsListElement $NLMM104UsedInLayerProp [lindex [lindex $layerPropertiesList $i] 2]] } {
            lappend NLMM104UsedInLayerProp [lindex [lindex $layerPropertiesList $i] 2]
        }
    }

    return [llength $NLMM104UsedInLayerProp]    
}

proc ExistsNLMM104InLayerProperties { matName } {

    global NLMM104UsedInLayerProp

    if { [ExistsListElement $NLMM104UsedInLayerProp [ReplaceSpaceWithUnderscore $matName]] } {
        return 1
    }
    return 0
}

proc WriteNLMM104InLayerProperties { index } {

    global NLMM104UsedInLayerProp

    set nlmm104 ""
    set materials [GiD_Info materials]

    #global PROJECTPATH
    #set result [open "$PROJECTPATH\\aaa_result.dat" "w"]
    #set mat [GiD_Info materials [lindex $materials 2]]
    #puts $result $mat
    #for {set i 1} {$i < [llength $mat]} {incr i} {
    #	puts $result [lindex $mat $i]
    #}
    #close $result
    #return

    for {set i 0} {$i < [llength $materials]} {incr i} {
    	
    	set material [GiD_Info materials [lindex $materials $i]]
        if { [lindex $material 2] == "NLMM104" && [ExistsListElement $NLMM104UsedInLayerProp [ReplaceSpaceWithUnderscore [lindex $materials $i]]] } {
        	
        	append nlmm104 "#     A     B     C     D     E     F     G\n"
        	
        	append nlmm104 "[format %7d $index]  "
        	append nlmm104 "[ReplaceSpaceWithUnderscore [lindex $materials $i]]"
        	for {set j 4} {$j <= 12} {incr j 2} {
                # append nlmm104 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material $j]]]
                append nlmm104 [format %18.8e [lindex $material $j]]
            }
            
            append nlmm104 "\n#     H     I     J     K     L     M     N     O\n"
            
            # append nlmm104 [format %21.8e [RemoveUnitsFromParameterValue [lindex $material 14]]]
            append nlmm104 [format %21.8e [lindex $material 14]]
            append nlmm104 "  \_[lindex $material 16]"
            for {set j 18} {$j <= 28} {incr j 2} {
                append nlmm104 "[format %18.8e [lindex $material $j]]"
            }
            
            append nlmm104 "\n#     P     Q     R     S     T     U\n"

            switch [lindex $material 16] {
            	"TRILINEAR" {
            		# append nlmm104 [format %21.8e [RemoveUnitsFromParameterValue [lindex $material 30]]]
                    append nlmm104 [format %21.8e [lindex $material 30]]
            	}
            	"QUADRILINEAR" {
            		# append nlmm104 [format %21.8e [RemoveUnitsFromParameterValue [lindex $material 32]]]
                    append nlmm104 [format %21.8e [lindex $material 32]]
            	}
            	default {
            		# append nlmm104 [format %21.8e [RemoveUnitsFromParameterValue [lindex $material 34]]]
                    append nlmm104 [format %21.8e [lindex $material 34]]
            	}
            }
            append nlmm104 "  \_[lindex $material 36]"
            if { [lindex $material 38] == "VALUE" } {
            	append nlmm104 "[format %18.8e [lindex $material 40]]"
            } else {
            	append nlmm104 "  \_[lindex $material 38]"
            }
            append nlmm104 "[format %18.8e [lindex $material 42]]"
            append nlmm104 "  \_[lindex $material 44]"
            if { [lindex $material 46] == "VALUE" } {
            	append nlmm104 "[format %18.8e [lindex $material 48]]"
            } else {
            	append nlmm104 "  \_[lindex $material 46]"
            }

            append nlmm104 "\n#     V     W     X\n"

            append nlmm104 "      \_[lindex $material 50]"
            # append nlmm104 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material 52]]]
            # append nlmm104 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material 54]]]
            append nlmm104 [format %18.8e [lindex $material 52]]
            append nlmm104 [format %18.8e [lindex $material 54]]

            append nlmm104 "\n#     Y     Z     AA     AB     AC     AD     AE\n"

            append nlmm104 "      \_[lindex $material 56]"
            if { [lindex $material 58] == "VALUE" } {
            	# append nlmm104 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material 60]]]
                append nlmm104 [format %18.8e [lindex $material 60]]
            } else {
            	append nlmm104 "  \_[lindex $material 58]"
            }
            append nlmm104 "  [lindex $material 62]  "
            # append nlmm104 [RemoveUnitsFromParameterValue [lindex $material 64]]
            append nlmm104 [lindex $material 64]
            append nlmm104 "  \_[lindex $material 66]"
            # append nlmm104 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material 68]]]
            append nlmm104 [format %18.8e [lindex $material 68]]
            switch [lindex $material 66] {
            	"NONE" {
            		# append nlmm104 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material 70]]]
                    append nlmm104 [format %18.8e [lindex $material 70]]
            	}
            	"LINEAR" {
            		# append nlmm104 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material 72]]]
                    append nlmm104 [format %18.8e [lindex $material 72]]
            	}
            	"TRILINEAR" {
            		# append nlmm104 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material 74]]]
                    append nlmm104 [format %18.8e [lindex $material 74]]
            	}
            	default {
            		# append nlmm104 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material 76]]]
                    append nlmm104 [format %18.8e [lindex $material 76]]
            	}
            }

            append nlmm104 "\n#     AF     AG     AH     AI\n   "

            for {set j 78} {$j <= 84} {incr j 2} {
                append nlmm104 "[format %18.8e [lindex $material $j]]"
            }
            append nlmm104 " ;\n"

        	set index [expr $index + 1]
        }
    }
    return $nlmm104
}