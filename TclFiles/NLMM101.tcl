# NLMM101.tcl
#
# NumOfNLMM101InLayerProperties {}
# ExistsNLMM101InLayerProperties { matName }
# WriteNLMM101InLayerProperties { index }

proc NumOfNLMM101InLayerProperties {} {

    global layerPropertiesList NLMM101UsedInLayerProp

    set NLMM101UsedInLayerProp {}

    for {set i 0} {$i < [llength $layerPropertiesList]} {incr i} { 
        if { [lindex [lindex $layerPropertiesList $i] 1] == "NLMM101" && ![ExistsNLMM101InLayerProperties [lindex [lindex $layerPropertiesList $i] 2]] } {
            lappend NLMM101UsedInLayerProp [lindex [lindex $layerPropertiesList $i] 2]
        }
    }

    return [llength $NLMM101UsedInLayerProp]    
}

proc ExistsNLMM101InLayerProperties { matName } {

    global NLMM101UsedInLayerProp

    if { [ExistsListElement $NLMM101UsedInLayerProp [ReplaceSpaceWithUnderscore $matName]] } {
        return 1
    }
    return 0
}

proc WriteNLMM101InLayerProperties { index } {

    global NLMM101UsedInLayerProp

    set nlmm101 ""
    set materials [GiD_Info materials]

    #global PROJECTPATH
    #set result [open "$PROJECTPATH\\aaa_result.dat" "w"]
    #set mat [GiD_Info materials [lindex $materials 2]]
    #puts $result $mat
    #for {set i 1} {$i < [llength $mat]} {incr i} {
    #	puts $result [lindex $mat $i]
    #}
    #close $result
    #return

    for {set i 0} {$i < [llength $materials]} {incr i} {
    	
    	set material [GiD_Info materials [lindex $materials $i]]
        if { [lindex $material 2] == "NLMM101" && [ExistsNLMM101InLayerProperties [ReplaceSpaceWithUnderscore [lindex $materials $i]]] } {
        	
        	append nlmm101 "#     A     B     C     D     E     F     G\n"
        	
        	append nlmm101 "[format %7d $index]  "
        	append nlmm101 [ReplaceSpaceWithUnderscore [lindex $materials $i]]
        	for {set j 4} {$j <= 12} {incr j 2} {
                # append nlmm101 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material $j]]]
                append nlmm101 [format %18.8e [lindex $material $j]]
            }
            
            append nlmm101 "\n#     H     I     J     K     L     M\n"
            
            # append nlmm101 [format %21.8e [RemoveUnitsFromParameterValue [lindex $material 14]]]
            append nlmm101 [format %21.8e [lindex $material 14]]
            append nlmm101 "  \_[lindex $material 16]"
            for {set j 18} {$j <= 24} {incr j 2} {
                append nlmm101 [format %18.8e [lindex $material $j]]
            }
            
            append nlmm101 "\n#     N     O     P     Q     R     S     T\n"

            if { [lindex $material 16] == "TRILINEAR" } {
            	# append nlmm101 [format %21.8e [RemoveUnitsFromParameterValue [lindex $material 26]]]
                append nlmm101 [format %21.8e [lindex $material 26]]
            } else  {
           		# append nlmm101 [format %21.8e [RemoveUnitsFromParameterValue [lindex $material 28]]]
                append nlmm101 [format %21.8e [lindex $material 28]]
            }
            append nlmm101 [format %18.8e [lindex $material 30]]
            append nlmm101 "  \_[lindex $material 32]"
            append nlmm101 "  \_[lindex $material 34]"
            if { [lindex $material 36] == "VALUE" } {
            	append nlmm101 [format %18.8e [lindex $material 38]]
            } else {
            	append nlmm101 "  \_[lindex $material 36]"
            }
            append nlmm101 "  [lindex $material 40]"
            # append nlmm101 "  [RemoveUnitsFromParameterValue [lindex $material 42]]"
            append nlmm101 "  [lindex $material 42]"

            append nlmm101 " ;\n"

        	set index [expr $index + 1]
        }
    }
    return $nlmm101
}