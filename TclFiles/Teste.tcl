proc IsPointInLine {} {
	  global PROBLEMTYPEPATH
	  set result [open "$PROBLEMTYPEPATH\\result_new.dat" "w"]
	  puts $result [GiD_Info IsPointInside Line 5 [lindex [GiD_Info Coordinates 10 Mesh] 0]]
	  close $result
	  return 0
}