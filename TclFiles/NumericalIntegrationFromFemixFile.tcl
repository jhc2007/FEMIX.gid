proc GetNumericalIntegrationFromFemixFile { intData intName } {
	
	set index 4

	while { $index < [llength $intData] } { 
		set index [expr $index + 1]
		set numKeywords [lindex $intData [expr $index + 1]]
		if { [lindex $intData $index] == $intName } {
			set numericalInt {}
			set index [expr $index + 1]
			for {set i 0} {$i < $numKeywords} {incr i} {
				set index [expr $index + 1]
				set keyword [lindex $intData $index]
				switch [string index $keyword 1] {
					"G" {
						lappend numericalInt "General"
					}
					"M" {
						lappend numericalInt "Membrane"
					}
					"S" {
						lappend numericalInt "Shear"
					}
					"T" {
						lappend numericalInt "Torsional"
					}
					"B" {
						lappend numericalInt "Bending"
					}
				}
				lappend numericalInt [string tolower [string range $keyword 3 4]]
				lappend numericalInt [string index $keyword 6]
			}
			return $numericalInt
		}
		set index [expr $index + $numKeywords + 3]
	}
}

proc CountSpringsNumericalIntegrationsFromFemixFile {} {

	global SpringsNumericalIntegrationsFromFemixFile
	return [llength $SpringsNumericalIntegrationsFromFemixFile]
}

proc WriteSpringsNumericalIntegrationsFromFemixFile { index } {

	global SpringsNumericalIntegrationsFromFemixFile

	set returnString ""
	incr index

	for {set i 0} {$i < [llength $SpringsNumericalIntegrationsFromFemixFile]} {incr i} {
		append returnString [format %7d $index]
		for {set j 0} {$j < [llength [lindex $SpringsNumericalIntegrationsFromFemixFile $i]]} {incr j} {
			append returnString "  [lindex [lindex $SpringsNumericalIntegrationsFromFemixFile $i] $j]"
		}
		append returnString " ;\n"
		incr index
	}

	return $returnString
}

proc ExistsSpringsNumericalIntegrationFromFemixFile { intName } {

	global SpringsNumericalIntegrationsFromFemixFile

	for {set i 0} {$i < [llength $SpringsNumericalIntegrationsFromFemixFile]} {incr i} {
		if { [lindex [lindex $SpringsNumericalIntegrationsFromFemixFile $i] 0] == $intName } {
			return true
		}
	}
	return false
}

proc CreateFileSpringsNumericalIntegrationsFromFemixFile {} {

	global PROJECTPATH \
		   SpringsNumericalIntegrationsFromFemixFile


	if { [llength $SpringsNumericalIntegrationsFromFemixFile] == 0 } {
		return
	}

	set numIntegrationFile [open "$PROJECTPATH\\femix_springs_numerical_integrations.dat" "w"]
    for {set i 0} {$i < [llength $SpringsNumericalIntegrationsFromFemixFile]} {incr i} {
    	foreach var [lindex $SpringsNumericalIntegrationsFromFemixFile $i] {
    		puts $numIntegrationFile $var
    	}
    }
    close $numIntegrationFile

	  # global PROBLEMTYPEPATH
	  # set result [open "$PROBLEMTYPEPATH\\result_new.dat" "w"]
	  # foreach var $SurfaceSpringsNumericalIntegrationsFromFemixFile {
	  # 	puts $result $var
	  # }
	  # close $result 

}

proc CreateFromFileSpringsNumericalIntegrationsFromFemixFile {} {

	global PROJECTPATH \
		   SpringsNumericalIntegrationsFromFemixFile

	if { [file exists "$PROJECTPATH\\femix_springs_numerical_integrations.dat"] } {
		
		set numIntegrationFile [open "$PROJECTPATH\\femix_springs_numerical_integrations.dat" "r"]
  		set numIntegrationData [read $numIntegrationFile]
  		close $numIntegrationFile

  		set index 0

  		while { $index < [llength $numIntegrationData] } {

  			set numIntegration {}

  			for {set i $index} {$i < [expr $index + 2]} {incr i} {
  				lappend numIntegration [lindex $numIntegrationData $i]
  			}

  			incr index
  			set numKeys [lindex $numIntegrationData $index]

  			for {set i 0} {$i < $numKeys} {incr i} {
  				incr index
  				lappend numIntegration [lindex $numIntegrationData $index]
  			}

  			incr index
  			
  			lappend SpringsNumericalIntegrationsFromFemixFile $numIntegration
  		}
	}
}