# NLMM201.tcl
#
# NumOfNLMM201InLayerProperties {}
# ExistsNLMM201InLayerProperties { matName }
# WriteNLMM201InLayerProperties { index }

proc NumOfNLMM201InLayerProperties {} {

    global layerPropertiesList NLMM201UsedInLayerProp

    set NLMM201UsedInLayerProp {}

    for {set i 0} {$i < [llength $layerPropertiesList]} {incr i} { 
        if { [lindex [lindex $layerPropertiesList $i] 1] == "NLMM201" && ![ExistsNLMM201InLayerProperties [lindex [lindex $layerPropertiesList $i] 2]] } {
            lappend NLMM201UsedInLayerProp [lindex [lindex $layerPropertiesList $i] 2]
        }
    }

    return [llength $NLMM201UsedInLayerProp]    
}

proc ExistsNLMM201InLayerProperties { matName } {

    global NLMM201UsedInLayerProp

    if { [ExistsListElement $NLMM201UsedInLayerProp [ReplaceSpaceWithUnderscore $matName]] } {
        return 1
    }
    return 0
}

proc WriteNLMM201InLayerProperties { index } {

    global NLMM201UsedInLayerProp

    set nlmm201 ""
    set materials [GiD_Info materials]

    #global PROJECTPATH
    #set result [open "$PROJECTPATH\\aaa_result.dat" "w"]
    ##puts $result $materials
    #set mat [GiD_Info materials [lindex $materials 5]]
    ##puts $result $mat
    #for {set i 1} {$i < [llength $mat]} {incr i} {
    # 	puts $result [lindex $mat $i]
    #}
    #close $result
    #return

    for {set i 0} {$i < [llength $materials]} {incr i} {

    	set material [GiD_Info materials [lindex $materials $i]]
        if { [lindex $material 2] == "NLMM201" && [ExistsNLMM201InLayerProperties [ReplaceSpaceWithUnderscore [lindex $materials $i]]] } {

        	append nlmm201 "#     A     B     C     D     E     F\n"

        	append nlmm201 "[format %7d $index]  "
        	append nlmm201 [ReplaceSpaceWithUnderscore [lindex $materials $i]]
        	for {set j 4} {$j <= 10} {incr j 2} {
                # append nlmm201 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material $j]]]
                append nlmm201 [format %18.8e [lindex $material $j]]
            }

            append nlmm201 "\n#     G     H     I     J     K\n"

            append nlmm201 [format %21.8e [lindex $material 12]]
            for {set j 14} {$j <= 20} {incr j 2} {
                # append nlmm201 [format %18.8e [RemoveUnitsFromParameterValue [lindex $material $j]]]
                append nlmm201 [format %18.8e [lindex $material $j]]
            }

            append nlmm201 " ;\n"

        	set index [expr $index + 1]
        }
    }
    return $nlmm201
}
