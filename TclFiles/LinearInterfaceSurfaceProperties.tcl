    
proc NumOfLinIntSurfaceInLayerProperties {} {

    global layerPropertiesList LinIntSurfaceUsedInLayerProp

    set LinIntSurfaceUsedInLayerProp {}

    for {set i 0} {$i < [llength $layerPropertiesList]} {incr i} { 
        if { [lindex [lindex $layerPropertiesList $i] 1] == "LIN_INT_SURF" && ![ExistsLinIntSurfaceInLayerProperties [lindex [lindex $layerPropertiesList $i] 2]] } {
            lappend LinIntSurfaceUsedInLayerProp [lindex [lindex $layerPropertiesList $i] 2]
        }
    }

    return [llength $LinIntSurfaceUsedInLayerProp]    
}

proc ExistsLinIntSurfaceInLayerProperties { matName } {

    global LinIntSurfaceUsedInLayerProp

    if { [ExistsListElement $LinIntSurfaceUsedInLayerProp [ReplaceSpaceWithUnderscore $matName]] } {
        return 1
    }
    return 0
}

proc WriteLinIntSurfaceInLayerProperties { index } {

    global LinIntSurfaceUsedInLayerProp

    set linIntSurface ""
    set materials [GiD_Info materials]

    #global PROJECTPATH
    #set result [open "$PROJECTPATH\\aaa_result.dat" "w"]
    ##puts $result $materials
    #set mat [GiD_Info materials [lindex $materials 5]]
    ##puts $result $mat
    #for {set i 1} {$i < [llength $mat]} {incr i} {
    # 	puts $result [lindex $mat $i]
    #}
    #close $result
    #return

    for {set i 0} {$i < [llength $materials]} {incr i} {

    	set material [GiD_Info materials [lindex $materials $i]]
        if { [lindex $material 2] == "LIN_INT_SURF" && [ExistsLinIntSurfaceInLayerProperties [ReplaceSpaceWithUnderscore [lindex $materials $i]]] } {

        	append linIntSurface "[format %7d $index]  "
        	append linIntSurface [ReplaceSpaceWithUnderscore [lindex $materials $i]]
        	for {set j 4} {$j <= 8} {incr j 2} {
                # append linIntSurface [format %18.8e [RemoveUnitsFromParameterValue [lindex $material $j]]]
                append linIntSurface [format %18.8e [lindex $material $j]]
            }
            append linIntSurface " ;\n"

        	set index [expr $index + 1]
        }
    }
    return $linIntSurface
}
