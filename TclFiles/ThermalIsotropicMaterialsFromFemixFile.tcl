# LinearIsotropicMaterialsFromFemixFile.tcl
#
# CreateLinearIsotropicMaterials { femixData }

proc CreateThermalIsotropicMaterials { femixData } {

    set thermalIsoData [GetBlockData $femixData "THERMAL_ISOTROPIC_MATERIALS"]

    if { [llength $thermalIsoData] > 0 } {

        global doubleFormatMatParameters
    	set allMaterialsNameList [GiD_Info materials]
    	set thermalIsoMaterialsNameList {}

        for {set i 0} {$i < [llength $allMaterialsNameList]} {incr i} {
            if { [lindex [GiD_Info materials [lindex $allMaterialsNameList $i]] 2] == "THERMAL_ISO" } {
                lappend thermalIsoMaterialsNameList [string toupper [lindex $allMaterialsNameList $i]]
            }
        }

        set numMaterials [lindex $thermalIsoData 2]

        set index 4

        for {set i 0} {$i < $numMaterials} {incr i} {

            set materialName [lindex $thermalIsoData [expr $index + 1]]

            set materialData { THERMAL_ISO }

            lappend materialData "[format $doubleFormatMatParameters [lindex $thermalIsoData [expr $index + 2]]]"
            lappend materialData "[format $doubleFormatMatParameters [lindex $thermalIsoData [expr $index + 3]]]"
            lappend materialData "[format $doubleFormatMatParameters [lindex $thermalIsoData [expr $index + 4]]]"

            if { [ExistsListElement $thermalIsoMaterialsNameList [string toupper $materialName]] } {
                GiD_ModifyData materials $materialName $materialData
            } else {
                GiD_CreateData create material THERMAL_ISO_Material $materialName $materialData
            }

            incr index 6
        }
    }
}