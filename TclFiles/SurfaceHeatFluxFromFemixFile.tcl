proc CreateSurfaceHeatFluxFromFemixFile { loadCaseData loadCaseNumber } {

	global SurfaceHeatFluxFromFemixFile
	
	set surfaceHeatFluxData [GetBlockData $loadCaseData "SURFACE_HEAT_FLUX"]

	set index 5

	while { $index < [llength $surfaceHeatFluxData] } {
		
		set rangeList [GetFirstLastValuesFromRange [lindex $surfaceHeatFluxData $index]]
		set firstElem [lindex $rangeList 0]
		set lastElem [lindex $rangeList 1]

		incr index
		set face [lindex $surfaceHeatFluxData $index]

		incr index
		set numNodes [lindex $surfaceHeatFluxData $index]

		set surfaceHeatFlux [list $loadCaseNumber $firstElem $lastElem $face $numNodes]

		for {set i 0} {$i < $numNodes} {incr i} {
			incr index
			lappend surfaceHeatFlux [lindex $surfaceHeatFluxData $index]
		}

		lappend SurfaceHeatFluxFromFemixFile $surfaceHeatFlux

		incr index 3
	}
}

proc CountSurfaceHeatFluxFromFemixFile { loadCaseNumber } {

	global SurfaceHeatFluxFromFemixFile

	set index 0
	set count 0

	while { $index < [llength $SurfaceHeatFluxFromFemixFile] && [lindex [lindex $SurfaceHeatFluxFromFemixFile $index] 0] != $loadCaseNumber } {
		incr index
	}

	while { $index < [llength $SurfaceHeatFluxFromFemixFile] && [lindex [lindex $SurfaceHeatFluxFromFemixFile $index] 0] == $loadCaseNumber } {
		set count [expr $count + [lindex [lindex $SurfaceHeatFluxFromFemixFile $index] 2] - [lindex [lindex $SurfaceHeatFluxFromFemixFile $index] 1] + 1]
		incr index
	}

	return $count
}

proc WriteSurfaceHeatFluxFromFemixFile { loadCaseNumber index } {

	global SurfaceHeatFluxFromFemixFile 

	incr index
	set returnString ""
	set i 0

	while { $i < [llength $SurfaceHeatFluxFromFemixFile] && [lindex [lindex $SurfaceHeatFluxFromFemixFile $i] 0] != $loadCaseNumber } {
		incr i
	}

	while { $i < [llength $SurfaceHeatFluxFromFemixFile] && [lindex [lindex $SurfaceHeatFluxFromFemixFile $i] 0] == $loadCaseNumber } {

		set surfaceHeatFlux [lindex $SurfaceHeatFluxFromFemixFile $i]

        if { [lindex $surfaceHeatFlux 1] == [lindex $surfaceHeatFlux 2] } {
            append returnString [format %7d $index]
            append returnString "  [lindex $surfaceHeatFlux 1]"
            incr index
        } else {
            append returnString "  \[$index\-[expr $index + [lindex $surfaceHeatFlux 2] - [lindex $surfaceHeatFlux 1]]\]"
            append returnString "  \[[lindex $surfaceHeatFlux 1]\-[lindex $surfaceHeatFlux 2]\]"
            set index [expr $index + [lindex $surfaceHeatFlux 2] - [lindex $surfaceHeatFlux 1] + 1]
        }

        for {set j 3} {$j < [llength $surfaceHeatFlux]} {incr j} {
        	append returnString "  [lindex $surfaceHeatFlux $j]"
        }

        append returnString " ;\n"

		incr i
	}

    return $returnString
}

proc CreateFileSurfaceHeatFluxFromFemixFile {} {

	global PROJECTPATH \
		   SurfaceHeatFluxFromFemixFile

	if { [llength $SurfaceHeatFluxFromFemixFile] == 0 } {
		return
	}

	set surfaceHeatFluxFile [open "$PROJECTPATH\\femix_surface_heat_flux.dat" "w"]

    for {set i 0} {$i < [llength $SurfaceHeatFluxFromFemixFile]} {incr i} {
    	foreach var [lindex $SurfaceHeatFluxFromFemixFile $i] {
    		puts $surfaceHeatFluxFile $var
    	}
    }
    close $surfaceHeatFluxFile
}

proc CreateFromFileSurfaceHeatFluxFromFemixFile {} {

	global PROJECTPATH \
		   SurfaceHeatFluxFromFemixFile

	if { [file exists "$PROJECTPATH\\femix_surface_heat_flux.dat"] } {
		
		set surfaceHeatFluxFile [open "$PROJECTPATH\\femix_surface_heat_flux.dat" "r"]
  		set surfaceHeatFluxData [read $surfaceHeatFluxFile]
  		close $surfaceHeatFluxFile

  		set index 0

  		while { $index < [llength $surfaceHeatFluxData] } {

  			set surfaceHeatFlux {}

  			for {set i $index} {$i < [expr $index + 5]} {incr i} {
  				lappend surfaceHeatFlux [lindex $surfaceHeatFluxData $i]
  			}

  			incr index 4
  			set numNodes [lindex $surfaceHeatFluxData $index]

  			for {set i 0} {$i < $numNodes} {incr i} {
  				incr index
  				lappend surfaceHeatFlux [lindex $surfaceHeatFluxData $index]
  			}

  			incr index
  			
  			lappend SurfaceHeatFluxFromFemixFile $surfaceHeatFlux
  		}
	}
}