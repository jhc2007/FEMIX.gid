# 2DPostResFile.tcl
#
# proc Create2DPostResFile { basename }
# proc PutsReactions2DPostResFile {}
# PutsStressesStrains2DPostResFile {}

proc Create2DPostResFile { basename } {

    global PROJECTPATH

    set rslptFileName $basename
    append rslptFileName "_rs.lpt"

    # set result [open "$PROJECTPATH\\result.dat" "w"]
    # puts $result "ASSIM sim"
    # puts $result $rslptFileName

    if { [file exists "$PROJECTPATH\\$rslptFileName"] } {

        global ExtraNodes
        #numDimension lengthUnit
        global postResFile rslptData rslptDataLength
        global combNum combFlag index divLine

        global firstCable2DGaussPoints \
               firstEmbCable2DGaussPoints \
               firstPlaneStressQuadGaussPoints \
               firstLinearInterfaceLine2DGaussPoints \
               firstQuadInterfaceLine2DGaussPoints

        set firstCable2DGaussPoints true
        set firstEmbCable2DGaussPoints true
        set firstPlaneStressQuadGaussPoints true
        set firstLinearInterfaceLine2DGaussPoints true
        set firstQuadInterfaceLine2DGaussPoints true

        set rslptFile [open "$PROJECTPATH\\$rslptFileName" "r"]
        set rslptData [read $rslptFile]
        close $rslptFile 

        #set numDimension 3
        #set numNodes [GiD_Info Mesh NumNodes]
        #set numNodes 161

        set postResFile $basename
        append postResFile ".post.res"
        set postResFile [open "$PROJECTPATH\\$postResFile" "w"]
        puts $postResFile "GiD Post Results File 1.0\n"

        set divLine "------------------------------------------------------------------------------"
        set index 0
        set rslptDataLength [llength $rslptData]
        
        while { $index <  $rslptDataLength } {

            while { $index < $rslptDataLength && [lindex $rslptData $index] != $divLine } {
                set index [expr $index + 1]
            }
            while { $index < $rslptDataLength && [lindex $rslptData $index] != "Combination" &&  [lindex $rslptData $index] != "Load" } {
                set index [expr $index + 1]
            }
            if { $index < $rslptDataLength } {
                if { [lindex $rslptData $index] == "Combination" } {
                    set combNum [lindex $rslptData [expr $index + 1]]
                    set combFlag "Combinations"    
                } else {
                    set combNum [lindex $rslptData [expr $index + 2]]
                    set combFlag "Load Cases"
                }
                set combNum [string range $combNum 0 [expr [string length $combNum] - 2]]
 
                #if { $numDimension == 2 } {
                puts $postResFile "Result \"Displacements\" \"$combFlag\" $combNum Vector OnNodes\n" 
                puts $postResFile "ComponentNames \"X-Displacement\" \"Y-Displacement\"\n"
                # puts $postResFile "ComponentNames \"X-Displacement ($lengthUnit)\" \"Y-Displacement ($lengthUnit)\"\n"
                ### puts $postResFile "ComponentNames \"X-Displacement ($lengthUnit)\" \"Y-Displacement ($lengthUnit)\" \"Z-Displacement ($lengthUnit)\"\n"
                puts $postResFile "Values"
                puts $postResFile "#NodeNum      X-Displacement      Y-Displacement\n"
                #puts $postResFile "#NodeNum      X-Displacement      Y-Displacement      Z-Displacement\n"
                #} else {
                #}
                # Z-Displacement\n"
            }
            while { $index < $rslptDataLength && [lindex $rslptData $index] != "Displacements:"} {
                set index [expr $index + 1]
            }
            while { $index < $rslptDataLength && ![string is integer [lindex $rslptData $index]] } {
                set index [expr $index + 1]
            }
            if { $index < $rslptDataLength } {
                
                if { [lindex $rslptData [expr $index - 1]] == "R1" } {
                    set rotationData "Result \"Rotations\" \"$combFlag\" $combNum Scalar OnNodes\n \
                                      \nComponentNames \"Z-Rotation\"\n \
                                      \nValues \
                                      \n#NodeNum          Z-Rotation\n\n" 
                    while { [string is integer [lindex $rslptData $index]] } {
                        puts $postResFile [format %8s%20.10e%20.10e \
                                          [lindex $rslptData $index] \
                                          [GetStringValue [lindex $rslptData [expr $index + 2]]] \
                                          [GetStringValue [lindex $rslptData [expr $index + 3]]]]
                        if { [string is double [lindex $rslptData [expr $index + 4]]] } {
                            append rotationData [format %8s%20.10e%s \
                                                [lindex $rslptData $index] \
                                                [lindex $rslptData [expr $index + 4]] "\n"]
                        }
                        set index [expr $index + 5]
                    }
                    puts $postResFile "\nEnd Values\n"
                    append rotationData "\nEnd Values\n"
                    puts $postResFile $rotationData
                    destroy $rotationData
                } else {
                    if { [llength $ExtraNodes] == 0 } {
                        while { [string is integer [lindex $rslptData $index]] } {
                            puts $postResFile [format %8s%20.10e%20.10e \
                                              [lindex $rslptData $index] \
                                              [GetStringValue [lindex $rslptData [expr $index + 2]]] \
                                              [GetStringValue [lindex $rslptData [expr $index + 3]]]]
                            set index [expr $index + 4]
                        }
                    } else {
                        set displacementsData {}
                        while { [string is integer [lindex $rslptData $index]] } {
                            lappend displacementsData [list \
                                                      [lindex $rslptData $index] \
                                                      [GetStringValue [lindex $rslptData [expr $index + 2]]] \
                                                      [GetStringValue [lindex $rslptData [expr $index + 3]]]]
                            set index [expr $index + 4]
                        }
                        for {set i 0} {$i < [llength $displacementsData]} {incr i} {
                            puts $postResFile [format %8s%20.10e%20.10e \
                                              [lindex [lindex $displacementsData $i] 0] \
                                              [lindex [lindex $displacementsData $i] 1] \
                                              [lindex [lindex $displacementsData $i] 2]]
                        }
                        for {set i 0} {$i < [llength $ExtraNodes]} {incr i} {
                            set extraNodeDisplacements [GetExtraNodeDisplacements $displacementsData [lindex [lindex $ExtraNodes $i] 1] [lindex [lindex $ExtraNodes $i] 2]]
                            puts $postResFile [format %8s%20.10e%20.10e \
                                              [lindex [lindex $ExtraNodes $i] 0] \
                                              [lindex $extraNodeDisplacements 0] \
                                              [lindex $extraNodeDisplacements 1]]
                        }
                        destroy $displacementsData
                    }
                    puts $postResFile "\nEnd Values\n"
                }
                while { $index < $rslptDataLength && [lindex $rslptData $index] != $divLine && [lindex $rslptData $index] != "Reactions:" && [lindex $rslptData $index] != "Stresses" && [lindex $rslptData $index] != "Strains" && [lindex $rslptData $index] != "Relative" } {
                	set index [expr $index + 1]
                }
                if { [lindex $rslptData $index] == "Reactions:" } {
                	PutsReactions2DPostResFile
                }
                while { $index < $rslptDataLength && [lindex $rslptData $index] != $divLine && [lindex $rslptData $index] != "Stresses" && [lindex $rslptData $index] != "Strains" && [lindex $rslptData $index] != "Relative" } {
                    set index [expr $index + 1]
                }
                if { [lindex $rslptData $index] == "Stresses" || [lindex $rslptData $index] == "Strains" || [lindex $rslptData $index] == "Relative" } {
                    PutsStressesStrainsRelativeDisplacement2DPostResFile
                }
            }
        }
        close $postResFile
    } 

    # else {
        # OpenWarningWindow "Warning" "To view results execute the command \"rslpt\" at the FEMIX Post Processor" 0
    # }
    #close $result
}

proc GetExtraNodeDisplacements { displacementsData node1 node2 } {

    set index 0
    set numNodesFound 0
    set node1node2Displacements {}
    set extraNodeDisplacements {}

    while { $index < [llength $displacementsData] && $numNodesFound < 2 } {
        if { [lindex [lindex $displacementsData $index] 0] == $node1 || [lindex [lindex $displacementsData $index] 0] == $node2 } {
            set numNodesFound [expr $numNodesFound + 1]
            lappend node1node2Displacements [lindex $displacementsData $index]
        }
        set index [expr $index + 1]
    }
    if { $numNodesFound == 2 } {
        set extraNodeDisplacements [list \
                                   [expr ([lindex [lindex $node1node2Displacements 0] 1] + [lindex [lindex $node1node2Displacements 1] 1]) / 2] \
                                   [expr ([lindex [lindex $node1node2Displacements 0] 2] + [lindex [lindex $node1node2Displacements 1] 2]) / 2]]
    } 
    return $extraNodeDisplacements
}

proc PutsStressesStrainsRelativeDisplacement2DPostResFile {} {

    # global stressUnit         
    global postResFile rslptData rslptDataLength
    global combNum combFlag index divLine

    global firstCable2DGaussPoints \
           firstEmbCable2DGaussPoints \
           firstPlaneStressQuadGaussPoints 
           # firstLinearInterfaceLine2DGaussPoints \
           # firstQuadInterfaceLine2DGaussPoints

    while { [lindex $rslptData $index] == "Stresses" || [lindex $rslptData $index] == "Strains" || [lindex $rslptData $index] == "Relative" } { 

        set loadType [lindex $rslptData $index]
        set elemType [lindex $rslptData [expr $index - 1]]
        
        set index [expr $index + 1]

        while { $index < $rslptDataLength && [lindex $rslptData $index] != $divLine && [lindex $rslptData $index] != "n." && [lindex $rslptData $index] != "Stresses" && [lindex $rslptData $index] != "Strains" && [lindex $rslptData $index] != "Relative" } {
            set index [expr $index + 1]
        }

        if { [lindex $rslptData $index] == "n." } {

            switch $elemType {
                "_INTERFACE_LINE_2D" {

                    if { $loadType == "Relative" } {
                        append loadType " Displacement"
                    }
                    WriteInterfaceLine2DStressesStrainsRelativeDisplacement $loadType $combFlag $combNum
                }
                "_CABLE_2D" {
                    if { $firstCable2DGaussPoints } {
                        puts $postResFile "GaussPoints \"Cable 2D Elements\" ElemType Linear\n"
                        puts $postResFile "Number of Gauss Points: 2"
                        puts $postResFile "Natural Coordinates: Internal\n"
                        puts $postResFile "End GaussPoints\n"
                        set firstCable2DGaussPoints false
                    }
                    WriteCable2DStressesStrains $loadType $combFlag $combNum "Cable 2D"
                }
                "_EMB_CABLE_2D" {
                    if { $firstEmbCable2DGaussPoints } {
                        puts $postResFile "GaussPoints \"Emb Cable 2D Elements\" ElemType Linear\n"
                        puts $postResFile "Number of Gauss Points: 2"
                        puts $postResFile "Natural Coordinates: Internal\n"
                        puts $postResFile "End GaussPoints\n"
                        set firstEmbCable2DGaussPoints false
                    }
                    WriteCable2DStressesStrains $loadType $combFlag $combNum "Emb Cable 2D"
                }
                "_PLANE_STRESS_QUAD" {
                    if { $firstPlaneStressQuadGaussPoints } {
                        puts $postResFile "GaussPoints \"Plane Stress Quad Elements\" ElemType Quadrilateral\n"
                        puts $postResFile "Number of Gauss Points: 4"
                        puts $postResFile "Natural Coordinates: Internal\n"
                        puts $postResFile "End GaussPoints\n"
                        set firstPlaneStressQuadGaussPoints false
                    }
                    WritePlaneStressQuadStressesStrains $loadType $combFlag $combNum
                }
            }
            set index [expr $index - 1]
            while { $index < $rslptDataLength && [lindex $rslptData $index] != $divLine && [lindex $rslptData $index] != "Stresses" && [lindex $rslptData $index] != "Strains" && [lindex $rslptData $index] != "Relative" } {
                set index [expr $index + 1]
            }
# if { [lindex $rslptData $index] == "Relative" } {
#     global PROBLEMTYPEPATH
#     set result [open "$PROBLEMTYPEPATH\\result_new.dat" "w"]
#     puts $result "Relative::"
#     puts $result [lindex $rslptData $index]
#     #puts $result $format
#     #puts $result $error
#     close $result
# }
        }
    }
}

proc WriteInterfaceLine2DStressesStrainsRelativeDisplacement { loadType combFlag combNum } {

    global postResFile rslptData index firstLinearInterfaceLine2DGaussPoints firstQuadInterfaceLine2DGaussPoints
    # lengthUnit stressUnit

    set elemNum [string range [lindex $rslptData [expr $index + 1]] 0 [expr [string length [lindex $rslptData [expr $index + 1]]] - 2]]

    if { [lindex [GiD_Mesh get element $elemNum] 1] == "Linear" } {
        if { $firstLinearInterfaceLine2DGaussPoints } {
            puts $postResFile "GaussPoints \"Linear Interface Line 2D Elements\" ElemType Linear\n"
            puts $postResFile "Number of Gauss Points: 2"
            puts $postResFile "Natural Coordinates: Internal\n"
            puts $postResFile "End GaussPoints\n"
            set firstLinearInterfaceLine2DGaussPoints false
        }
        puts $postResFile "Result \"$loadType on Gauss Points\" \"$combFlag\" $combNum Vector OnGaussPoints \"Linear Interface Line 2D Elements\"\n"
        set interfaceType "Linear"
    } else {
        if { $firstQuadInterfaceLine2DGaussPoints } {
            puts $postResFile "GaussPoints \"Quad Interface Line 2D Elements\" ElemType Quadrilateral\n"
            puts $postResFile "Number of Gauss Points: 4"
            puts $postResFile "Natural Coordinates: Internal\n"
            puts $postResFile "End GaussPoints\n"
            set firstQuadInterfaceLine2DGaussPoints false
        }
        puts $postResFile "Result \"$loadType on Gauss Points\" \"$combFlag\" $combNum Vector OnGaussPoints \"Quad Interface Line 2D Elements\"\n"
        set interfaceType "Quadrilateral"
    }
    if { $loadType == "Stresses" } {
        puts $postResFile "ComponentNames \"ShearStress\" \"Stress\"\n"
        # puts $postResFile "ComponentNames \"ShearStress ($stressUnit)\" \"Stress ($stressUnit)\"\n"
        puts $postResFile "Values"
        puts $postResFile [format %8s%20s%20s%s "#ElemNum" "Tau" "Sigma" "\n"]
    } else {
        # Relative Displacement
        # puts $postResFile "ComponentNames \"Tangencial Relative Displacement ($lengthUnit)\" \"Normal Relative Displacement ($lengthUnit)\"\n"
        puts $postResFile "ComponentNames \"Tangencial Relative Displacement\" \"Normal Relative Displacement\"\n"
        puts $postResFile "Values"
        puts $postResFile [format %8s%20s%20s%s "#ElemNum" "Delta-t" "Delta-n" "\n"]

    }
    
    while { [lindex $rslptData $index] == "n." } {

        set index [expr $index + 1]
        set elemNum [string range [lindex $rslptData $index] 0 [expr [string length [lindex $rslptData $index]] - 2]] 

        if { [lindex [GiD_Mesh get element $elemNum] 1] == "Linear" && $interfaceType == "Quadrilateral" } {
            puts $postResFile "\nEnd Values\n"
            if { $firstLinearInterfaceLine2DGaussPoints } {
                puts $postResFile "GaussPoints \"Linear Interface Line 2D Elements\" ElemType Linear\n"
                puts $postResFile "Number of Gauss Points: 2"
                puts $postResFile "Natural Coordinates: Internal\n"
                puts $postResFile "End GaussPoints\n"
                set firstLinearInterfaceLine2DGaussPoints false
            }
            puts $postResFile "Result \"$loadType on Gauss Points\" \"$combFlag\" $combNum Vector OnGaussPoints \"Linear Interface Line 2D Elements\"\n"
            if { $loadType == "Stresses" } {
                puts $postResFile "ComponentNames \"ShearStress\" \"Stress\"\n"
                # puts $postResFile "ComponentNames \"ShearStress ($stressUnit)\" \"Stress ($stressUnit)\"\n"
                puts $postResFile "Values"
                puts $postResFile [format %8s%20s%20s%s "#ElemNum" "Tau" "Sigma" "\n"]
            } else {
                # Relative Displacement
                # puts $postResFile "ComponentNames \"Tangencial Relative Displacement ($lengthUnit)\" \"Normal Relative Displacement ($lengthUnit)\"\n"
                puts $postResFile "ComponentNames \"Tangencial Relative Displacement\" \"Normal Relative Displacement\"\n"
                puts $postResFile "Values"
                puts $postResFile [format %8s%20s%20s%s "#ElemNum" "Delta-t" "Delta-n" "\n"]

            }
            set interfaceType "Linear"
        } elseif { [lindex [GiD_Mesh get element $elemNum] 1] == "Quadrilateral" && $interfaceType == "Linear" } {
            puts $postResFile "\nEnd Values\n"
            if { firstQuadInterfaceLine2DGaussPoints } {
                puts $postResFile "GaussPoints \"Quad Interface Line 2D Elements\" ElemType Quadrilateral\n"
                puts $postResFile "Number of Gauss Points: 4"
                puts $postResFile "Natural Coordinates: Internal\n"
                puts $postResFile "End GaussPoints\n"
                set firstQuadInterfaceLine2DGaussPoints false
            }
            puts $postResFile "Result \"$loadType on Gauss Points\" \"$combFlag\" $combNum Vector OnGaussPoints \"Quad Interface Line 2D Elements\"\n"
            if { $loadType == "Stresses" } {
                puts $postResFile "ComponentNames \"ShearStress\" \"Stress\"\n"
                # puts $postResFile "ComponentNames \"ShearStress ($stressUnit)\" \"Stress ($stressUnit)\"\n"
                puts $postResFile "Values"
                puts $postResFile [format %8s%20s%20s%s "#ElemNum" "Tau" "Sigma" "\n"]
            } else {
                # Relative Displacement
                # puts $postResFile "ComponentNames \"Tangencial Relative Displacement ($lengthUnit)\" \"Normal Relative Displacement ($lengthUnit)\"\n"
                puts $postResFile "ComponentNames \"Tangencial Relative Displacement\" \"Normal Relative Displacement\"\n"
                puts $postResFile "Values"
                puts $postResFile [format %8s%20s%20s%s "#ElemNum" "Delta-t" "Delta-n" "\n"]

            }
            set interfaceType "Quadrilateral"
        }
        set index [expr $index + 8]
        if { $interfaceType == "Linear" } {
            puts $postResFile [format %8s%20.10e%20.10e $elemNum [lindex $rslptData $index] [lindex $rslptData [expr $index + 1]]]
            set index [expr $index + 5]
            puts $postResFile [format %28.10e%20.10e [lindex $rslptData $index] [lindex $rslptData [expr $index + 1]]]
            set index [expr $index + 3] 
        } else {
            set firstPointValue1 [lindex $rslptData $index]
            set firstPointValue2 [lindex $rslptData [expr $index + 1]]
            puts $postResFile [format %8s%20.10e%20.10e $elemNum $firstPointValue1 $firstPointValue2]
            set index [expr $index + 5]
            puts $postResFile [format %28.10e%20.10e [lindex $rslptData $index] [lindex $rslptData [expr $index + 1]]]
            puts $postResFile [format %28.10e%20.10e [lindex $rslptData $index] [lindex $rslptData [expr $index + 1]]]
            puts $postResFile [format %28.10e%20.10e $firstPointValue1 $firstPointValue2]
            set index [expr $index + 3]  
        }

    }

    puts $postResFile "\nEnd Values\n"
}

proc WritePlaneStressQuadStressesStrains { loadType combFlag combNum } {

    global postResFile rslptData index  
    # stressUnit 
    
    puts $postResFile "Result \"$loadType on Gauss Points\" \"$combFlag\" $combNum Vector OnGaussPoints \"Plane Stress Quad Elements\"\n"

    if { $loadType == "Stresses"} {
        puts $postResFile "ComponentNames \"Stress-X\" \"Stress-Y\" \"ShearStress-XY\"\n"
        # puts $postResFile "ComponentNames \"Stress-X ($stressUnit)\" \"Stress-Y ($stressUnit)\" \"ShearStress-XY ($stressUnit)\"\n"
        puts $postResFile "Values"
        puts $postResFile [format %8s%20s%20s%20s%s "#ElemNum" "Sigma1" "Sigma2" "Tau12" "\n"]
        while { [lindex $rslptData $index] == "n." } {
            set index [expr $index + 1]
            set elemNum [string range [lindex $rslptData $index] 0 [expr [string length [lindex $rslptData $index]] - 2]] 
            set index [expr $index + 9]
            puts $postResFile [format %8s%20.10e%20.10e%20.10e $elemNum [lindex $rslptData $index] [lindex $rslptData [expr $index + 1]] [lindex $rslptData [expr $index + 2]]]
            for {set i 0} {$i < 3} {incr i} {
                set index [expr $index + 6]
                puts $postResFile [format %28.10e%20.10e%20.10e [lindex $rslptData $index] [lindex $rslptData [expr $index + 1]] [lindex $rslptData [expr $index + 2]]]   
            }
            set index [expr $index + 4]
        }
    } else {
        puts $postResFile "ComponentNames \"Strain-X\" \"Strain-Y\" \"ShearStrain-XY/2\"\n"
        puts $postResFile "Values"
        puts $postResFile [format %8s%20s%20s%20s%s "#ElemNum" "Eps1" "Eps2" "Gamma12/2" "\n"]
        while { [lindex $rslptData $index] == "n." } {
            set index [expr $index + 1]
            set elemNum [string range [lindex $rslptData $index] 0 [expr [string length [lindex $rslptData $index]] - 2]] 
            set index [expr $index + 9]
            puts $postResFile [format %8s%20.10e%20.10e%20.10e $elemNum [lindex $rslptData $index] [lindex $rslptData [expr $index + 1]] [expr [lindex $rslptData [expr $index + 2]] / 2]]
            for {set i 0} {$i < 3} {incr i} {
                set index [expr $index + 6]
                puts $postResFile [format %28.10e%20.10e%20.10e [lindex $rslptData $index] [lindex $rslptData [expr $index + 1]] [expr [lindex $rslptData [expr $index + 2]] / 2]]   
            }
            set index [expr $index + 4]
        }
    }
    
    puts $postResFile "\nEnd Values\n"
}

proc WriteCable2DStressesStrains { loadType combFlag combNum cableType } {

    global postResFile rslptData index  
    # stressUnit
    
    puts $postResFile "Result \"$loadType on Gauss Points\" \"$combFlag\" $combNum Scalar OnGaussPoints \"$cableType Elements\"\n"

    if { $loadType == "Stresses"} {
        puts $postResFile "ComponentNames \"Stress-Z ($cableType Elements)\"\n"
        puts $postResFile "Values"
        puts $postResFile [format %8s%20s%s "#ElemNum" "Sigma3" "\n"]
    } else {
        puts $postResFile "ComponentNames \"Strain-Z ($cableType Elements)\"\n"
        puts $postResFile "Values"
        puts $postResFile [format %8s%20s%s "#ElemNum" "Eps3" "\n"]
    }
    
    while { [lindex $rslptData $index] == "n." } {
    
        set index [expr $index + 1]
        set elemNum [string range [lindex $rslptData $index] 0 [expr [string length [lindex $rslptData $index]] - 2]] 
        set index [expr $index + 7]
        puts $postResFile [format %8s%20.10e $elemNum [lindex $rslptData $index]]
        set index [expr $index + 4]
        puts $postResFile [format %28.10e [lindex $rslptData $index]]
        set index [expr $index + 2]

    }
    puts $postResFile "\nEnd Values\n"
}

proc PutsReactions2DPostResFile {} {

    # global strengthUnit lengthUnit
    global postResFile rslptData rslptDataLength
    global combNum combFlag index

    puts $postResFile "Result \"Reactions (Force)\" \"$combFlag\" $combNum Vector OnNodes\n" 
    puts $postResFile "ComponentNames \"X-Reaction\" \"Y-Reaction\"\n"
    # puts $postResFile "ComponentNames \"X-Reaction ($strengthUnit)\" \"Y-Reaction ($strengthUnit)\"\n"
    #puts $postResFile "ComponentNames \"X-Reaction ($strengthUnit)\" \"Y-Reaction ($strengthUnit)\" \"Z-Reaction ($strengthUnit)\"\n"
    puts $postResFile "Values"
    puts $postResFile "#NodeNum          X-Reaction          Y-Reaction\n"
    #puts $postResFile "#NodeNum          X-Reaction          Y-Reaction          Z-Reaction\n"

    while { $index < $rslptDataLength && ![string is integer [lindex $rslptData $index]] } {
        set index [expr $index + 1]
    }

    if { [lindex $rslptData [expr $index - 1]] == "R1" } {
        
        set rotationData "Result \"Reactions (Moment)\" \"$combFlag\" $combNum Scalar OnNodes\n\n" 
        # append rotationData "ComponentNames \"Z-Moment ($strengthUnit*$lengthUnit)\"\n\n"
        append rotationData "ComponentNames \"Z-Moment\"\n\n"
        append rotationData "Values\n"
        append rotationData "#NodeNum            Z-Moment\n\n"

        while { $index < $rslptDataLength && [string is integer [lindex $rslptData $index]] } {
            puts $postResFile [format %8s%20s%20s [lindex $rslptData $index] [lindex $rslptData [expr $index + 2]] [lindex $rslptData [expr $index + 3]]]
            if { [string is double [lindex $rslptData [expr $index + 4]]] } {
                append rotationData [format %8s%20s%s \
                                    [lindex $rslptData $index] \
                                    [lindex $rslptData [expr $index + 4]] "\n"]
            }
            set index [expr $index + 5]
        }
        puts $postResFile "\nEnd Values\n"
        append rotationData "\nEnd Values\n"
        puts $postResFile $rotationData
        destroy $rotationData
    } else {
        while { $index < $rslptDataLength && [string is integer [lindex $rslptData $index]] } {
            puts $postResFile [format %8s%20s%20s [lindex $rslptData $index] \
                                                  [GetStringValue [lindex $rslptData [expr $index + 2]]] \
                                                  [GetStringValue [lindex $rslptData [expr $index + 3]]]]
            set index [expr $index + 4]
        }
        puts $postResFile "\nEnd Values\n"
    }
}
