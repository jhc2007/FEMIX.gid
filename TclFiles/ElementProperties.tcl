# ElementProperties.tcl
#
# proc OpenElementPropertiesWindow {}
# proc FillListBoxElementProperties { layerName }
# proc ElementTypeWithGeometry { elementType }
# proc ElementTypeWithIntegration { elementType }
# proc GetLayerNameIndex { layerName }

proc OpenElementPropertiesWindow {} {

    global elemPropWin elemPropConditions

  # global PROBLEMTYPEPATH
  # set result [open "$PROBLEMTYPEPATH\\result_new.dat" "w"]
  # for {set i 0} {$i < [llength [lindex $elemPropConditions 0]]} {incr i} {
  #   puts $result "$i -> [lindex [lindex $elemPropConditions 0] $i]"
  # }
  # close $result

    if { [GidUtils::AreWindowsDisabled] } {
       return
    } 

    set elemPropConditions [GiD_Info Conditions Element_Properties mesh]

    set elemPropWin .gid.elementPropertiesWindow
    InitWindow $elemPropWin [= "Element Properties"] "" "" "" 1
    if { ![winfo exists $elemPropWin] } {
       return
    }

    label $elemPropWin.labelLayerName -text "Layer Name"

    set layerNameList [GiD_Info layers]

    ttk::combobox $elemPropWin.comboBoxLayerName -values $layerNameList
    $elemPropWin.comboBoxLayerName set [lindex $layerNameList 0]

    listbox $elemPropWin.listBoxProperties -width 48
    # listbox $elemPropWin.listBoxProperties
    scrollbar $elemPropWin.scrollBarListBoxProperties -orient vertical -command [list $elemPropWin.listBoxProperties yview] 
    $elemPropWin.listBoxProperties configure -yscrollcommand [list $elemPropWin.scrollBarListBoxProperties set]

    #pack $elemPropWin.listBoxProperties -side left
    #pack $elemPropWin.scrollBarListBoxProperties -side right -expand yes -fill y

    FillListBoxElementProperties [lindex $layerNameList 0]

    button $elemPropWin.buttonClose -text "Close" -command "destroy $elemPropWin $elemPropConditions" -width 10

    for {set i 1} {$i < 12} {incr i} {
        grid rowconfigure $elemPropWin $i -weight 1  
    }
    # grid columnconfigure $elemPropWin 0 -weight 1
    grid columnconfigure $elemPropWin 1 -weight 1

    grid $elemPropWin.labelLayerName    -row 0 -column 0 -padx 10 -pady 20 -sticky ws
    grid $elemPropWin.comboBoxLayerName -row 0 -column 1 -padx 10 -pady 20 -sticky ews

    grid $elemPropWin.listBoxProperties -row 1 -column 0 -columnspan 2 -rowspan 10 -padx 10 -sticky news
    #$elemPropWin.scrollBarListBoxProperties
    grid $elemPropWin.scrollBarListBoxProperties -row 1 -padx 10 -column 1 -rowspan 10 -sticky nse 


    
    grid $elemPropWin.buttonClose -row 12 -column 1 -padx 10 -pady 10 -sticky ne
    
    bind $elemPropWin.comboBoxLayerName <<ComboboxSelected>> {
    
        global elemPropWin

        #set layerNameIndex [GetLayerNameIndex [$elemPropWin.comboBoxLayerName get]]
        #[$elemPropWin.listBoxProperties index end]
        #for { set i 0 } { $i < 2 } { incr i } {
        $elemPropWin.listBoxProperties delete 0 [$elemPropWin.listBoxProperties index end]          
        #}
        FillListBoxElementProperties [$elemPropWin.comboBoxLayerName get]

        # $elemPropWin.listBoxProperties insert end [lindex [lindex [GiD_Info Conditions Element_Properties mesh] $layerNameIndex] 3]
        
        # set geometryType [lindex [lindex [GiD_Info Conditions Element_Properties mesh] $layerNameIndex] 4]
        # $elemPropWin.listBoxProperties insert end $geometryType

        # switch $geometryType {
        #     "CROSS_SECTION_AREAS" {
        #         set geomPropList { "Area" 5 }
        #     }
        #     "FRAME_2D_CROSS_SECTIONS" {
        #         set geomPropList { "Area" 5 "Shear Factor" 6 "Moment of Inertia" 10 "Cross Section Height" 15 }               
        #     }
        #     #default {}
        # }
        # for {set i 0} {$i < [llength $geomPropList]} {incr i 2} {
        #     $elemPropWin.listBoxProperties insert end "[lindex $geomPropList $i]: [lindex [lindex [GiD_Info Conditions Element_Properties mesh] $layerNameIndex] [lindex $geomPropList [expr $i + 1]]]"                           
        # } 



    }
}

proc FillListBoxElementProperties { layerName } {

    global elemPropWin elemPropConditions

    set index [GetLayerNameIndex $layerName]

    if { $index < 0 } {

        $elemPropWin.listBoxProperties insert end ""
        $elemPropWin.listBoxProperties insert end "     Element Properties not defined for this Layer "

    } else {

        # set elemPropConditions [GiD_Info Conditions Element_Properties mesh]

        if { [lindex [lindex $elemPropConditions $index] 3] == 0 } {
            set elementType [lindex [lindex $elemPropConditions $index] 4]
        } else {
            set elementType [lindex [lindex $elemPropConditions $index] 5]
        }

        if { $elementType == "Select" } {
            $elemPropWin.listBoxProperties insert end ""
            $elemPropWin.listBoxProperties insert end "     Element Properties not defined for this Layer "
            return
        }

        $elemPropWin.listBoxProperties insert end $elementType
        
        if { [lindex [lindex $elemPropConditions $index] 6] == 1 } {
            # Layer Patterns
            $elemPropWin.listBoxProperties insert end [lindex [lindex $elemPropConditions $index] 7]
            $elemPropWin.listBoxProperties insert end [lindex [lindex $elemPropConditions $index] 93]
            $elemPropWin.listBoxProperties insert end [lindex [lindex $elemPropConditions $index] 92]
            $elemPropWin.listBoxProperties insert end "General s1 2"
            $elemPropWin.listBoxProperties insert end "General s2 2"
            
        } elseif { [ElementTypeWithGeometry $elementType] } {

            set geometryType [lindex [lindex $elemPropConditions $index] 8]
            $elemPropWin.listBoxProperties insert end $geometryType

            #global PROJECTPATH
            #set result [open "$PROJECTPATH\\result.dat" "w"] 
            #puts $result $elemPropConditions
            #puts $result "AQUI"
            #puts $result $geometryType
            #puts $result "AQUI"
            #close $result
            #return

            switch $geometryType {
            
                "CROSS_SECTION_AREAS" {
                    set geomPropList { "Area" 9 }
                }
                "FRAME_2D_CROSS_SECTIONS" {
                    set geomPropList { "Area" 9 \
                                       "Shear Factor" 10 \
                                       "Moment of Inertia" 14 \
                                       "Cross Section Height" 19 }               
                }
                "FRAME_3D_CROSS_SECTIONS" {
                    set geomPropList { "Area" 9 \
                                       "Shear Factor l2" 11 \
                                       "Shear Factor l3" 12 \
                                       "Torsional Constant" 13 \
                                       "Moment of Inertia l2" 15 \
                                       "Moment of Inertia l3" 16 \
                                       "Local Coord. of the Shear Center l2" 17 \
                                       "Local Coord. of the Shear Center l3" 18 \
                                       "Cross Section Height l2" 20 \
                                       "Cross Section Height l3" 21 \
                                       "Angle" 22 }               
                }
                "LAMINATE_THICKNESSES" {
                    set geomPropList { "Thickness" 23 }   
                }
                "GEOMETRY_PATTERNS" {
                    set geometryPatternsType [lindex [lindex $elemPropConditions $index] 24]
                    $elemPropWin.listBoxProperties insert end $geometryPatternsType
                    
                    switch $geometryPatternsType {
                        "CROSS_SECTION_AREAS" {
                            set geomPropList { "Area_[1]" 26 \
                                               "Area_[2]" 40 }
                        }
                        "FRAME_2D_CROSS_SECTIONS" {
                            set geomPropList { "Area_[1]" 26 \
                                               "Shear_Factor_[1]" 27 \
                                               "Moment_of_Inertia_[1]" 28 \
                                               "Cross_Section_Height_[1]" 29 \
                                               "Area_[2]" 40 \
                                               "Shear_Factor_[2]" 41 \
                                               "Moment_of_Inertia_[2]" 42 \
                                               "Cross_Section_Height_[2]" 43 }
                        }
                        "FRAME_3D_CROSS_SECTIONS" {
                            set geomPropList { "Area_[1]" 26 \
                                               "Shear_Factor_l2_[1]" 30 \
                                               "Shear_Factor_l3_[1]" 31 \
                                               "Torsional_Constant_[1]" 32 \
                                               "Moment_of_Inertia_l2_[1]" 33 \
                                               "Moment_of_Inertia_l3_[1]" 34 \
                                               "Local_Coord._of_Shear_Center_l2_[1]" 35 \
                                               "Local_Coord._of_Shear_Center_l3_[1]" 36 \
                                               "Cross_Section_Height_l2_[1]" 37 \
                                               "Cross_Section_Height_l3_[1]" 38 \
                                               "Angle_[1]" 39 \
                                               "Area_[2]" 40 \
                                               "Shear_Factor_l2_[2]" 44 \
                                               "Shear_Factor_l3_[2]" 45 \
                                               "Torsional_Constant_[2]" 46 \
                                               "Moment_of_Inertia_l2_[2]" 47 \
                                               "Moment_of_Inertia_l3_[2]" 48 \
                                               "Local_Coord._of_Shear_Center_l2_[2]" 49 \
                                               "Local_Coord._of_Shear_Center_l3_[2]" 50 \
                                               "Cross_Section_Height_l2_[2]" 51 \
                                               "Cross_Section_Height_l3_[2]" 52 \
                                               "Angle_[2]" 53 \ }
                        }
                        "LAMINATE_THICKNESSES" {
                            set geomPropList { "Thickness_[1]" 54 \
                                               "Thickness_[2]" 55 \
                                               "Thickness_[3]" 56 \
                                               "Thickness_[4]" 57 \ }
                            if { [lindex [lindex $elemPropConditions $index] 25] == 8 } {
                                set geomPropList [concat $geomPropList { "Thickness_[5]" 58 \
                                                                         "Thickness_[6]" 59 \
                                                                         "Thickness_[7]" 60 \
                                                                         "Thickness_[8]" 61 }]
                            }
                        }
                    }   
                }
            }
            for { set i 0 } { $i < [llength $geomPropList] } { incr i 2 } {
                $elemPropWin.listBoxProperties insert end "[lindex $geomPropList $i]: [lindex [lindex $elemPropConditions $index] [lindex $geomPropList [expr $i + 1]]]"                           
            }

            set numKeywords [lindex [lindex $elemPropConditions $index] 64]
            #$elemPropWin.listBoxProperties insert end $geometryType
            if { $numKeywords > 0 } {
                $elemPropWin.listBoxProperties insert end [lindex [lindex $elemPropConditions $index] 63]
                $elemPropWin.listBoxProperties insert end [lindex [lindex $elemPropConditions $index] 62]
                for {set i 0} {$i < $numKeywords} {incr i} {
                    set typeTerms [lindex [lindex $elemPropConditions $index] [expr 65 + $i * 3]]
                    set direction [lindex [lindex $elemPropConditions $index] [expr 66 + $i * 3]]                
                    set numPoints [lindex [lindex $elemPropConditions $index] [expr 67 + $i * 3]]                
                    $elemPropWin.listBoxProperties insert end [concat $typeTerms " " $direction " " $numPoints ]
                }
            }
        }
    }     
}

proc ElementTypeWithGeometry { elementType } {

    set elementTypeWithoutGeometry { AXIS_LINE \
                                     AXIS_QUAD \
                                     INTERFACE_POINT \
                                     INTERFACE_QUAD \
                                     PLANE_STRAIN_QUAD \
                                     SOLID_HEXA \
                                     SOLID_HEXA_THERMAL \
                                     SOLID_TETRA \
                                    }

    for {set i 0} {$i < [llength $elementTypeWithoutGeometry]} {incr i} {
        if { [lindex $elementTypeWithoutGeometry $i] == $elementType } {
            return false
        }
    }
    return true
}

proc ElementTypeWithIntegration { elementType } {

    set elementTypeWithoutIntegration { FRAME_2D \
                                        FRAME_3D \
                                        INTERFACE_POINT \
                                        PLANE_STRESS_QUAD_THERMAL \
                                        TRUSS_2D \
                                        TRUSS_3D \
                                      }

    for {set i 0} {$i < [llength $elementTypeWithoutIntegration]} {incr i} {
        if { [lindex $elementTypeWithoutIntegration $i] == $elementType } {
            return false
        }
    }
    return true
}

proc GetLayerNameIndex { layerName } {

    global elemPropConditions

    for {set i 0} {$i < [llength $elemPropConditions]} {incr i} {
        if { [lindex [lindex $elemPropConditions $i] 1] == $layerName } {
            return $i
        }
    }
    return -1
}

proc GetNumDimension { elemPropList index } {

    set elemType2D [list  "CABLE_2D" \
                          "EMB_CABLE_2D" \
                          "FRAME_2D" \
                          "PLANE_STRESS_QUAD" \
                          "PLANE_STRESS_QUAD_THERMAL" \
                          "TIMOSHENKO_BEAM_2D" \
                          "TRUSS_2D" \
                          "INTERFACE_LINE_2D" ]

    for {set i 0} {$i < [llength $elemPropList] } {incr i} {
        set elemType [lindex [lindex $elemPropList $i] $index]
        if { [ExistsListElement $elemType2D $elemType] } {
            return 2
        }
    }
    return 3
}

proc CreateElementPropertiesList {} {

  global ElementPropertiesList
  set ElementPropertiesList {}
  return 1
}

proc CreateElementProperties { layerName } {

  global ElementProperties
  set ElementProperties [list [ReplaceSpaceWithUnderscore $layerName]]
  return 0
}

proc AddElementPropertiesElements { firstElem lastElem } {

  global ElementProperties
  lappend ElementProperties $firstElem
  lappend ElementProperties $lastElem
  return 0
}

proc AddElementPropertiesValue { value underScore } {

  global ElementProperties
  if { $underScore == 1 } {
    lappend ElementProperties "_$value"
  } else {
    lappend ElementProperties [ReplaceSpaceWithUnderscore $value]
  }
  return 0
}

proc AddElementPropertiesList {} {

  global ElementPropertiesList ElementProperties
  lappend ElementPropertiesList $ElementProperties
  return 0
}

proc CountElementProperties {} {

  global ElementPropertiesList
  return [llength $ElementPropertiesList]
}

proc WriteElementPropertiesList {} {

  global ElementPropertiesList
  set value ""

  for {set i 0} {$i < [llength $ElementPropertiesList]} {incr i} {
    append value "[format %7d [expr $i + 1]]  "
    append value "[lindex [lindex $ElementPropertiesList $i] 0]  1  "
    if { [lindex [lindex $ElementPropertiesList $i] 1] == [lindex [lindex $ElementPropertiesList $i] 2] } {
      append value "[lindex [lindex $ElementPropertiesList $i] 1]  "
    } else {
      append value "\[[lindex [lindex $ElementPropertiesList $i] 1]\-[lindex [lindex $ElementPropertiesList $i] 2]\]  "  
    }
    for {set j 3} {$j < 7} {incr j} {
      append value "[lindex [lindex $ElementPropertiesList $i] $j]  "      
    }
    if { [lindex [lindex $ElementPropertiesList $i] 6] == "_NONE" } {
      append value "_NONE  "  
    } else {
      append value "[string range [lindex [lindex $ElementPropertiesList $i] 6] 1 [expr [string length [lindex [lindex $ElementPropertiesList $i] 6]] - 1]]\_[lindex [lindex $ElementPropertiesList $i] 0]  "      
    }
    for {set j 7} {$j < 9} {incr j} {
      append value "[lindex [lindex $ElementPropertiesList $i] $j]  "      
    }
    append value ";\n"
  }

  return $value
}

proc CreateExtraNodesList {} {

  global ExtraNodes 
  set ExtraNodes {}
  return 0
}

proc AddExtraNodes { node1 node4 conec1 conec2 conec3 conec4 conec5 conec6 conec7 conec8 } {

  global ExtraNodes

  set value [expr $node1 * $node4]

  if { $value == 2 || $value == 12 } {
    if { ![ExistsExtraNode $conec5] } {
      lappend ExtraNodes [list $conec5 $conec1 $conec2]
    }
    if { ![ExistsExtraNode $conec7] } {
      lappend ExtraNodes [list $conec7 $conec3 $conec4]
    }
  } elseif { $value == 4 || $value == 6 } {
    if { ![ExistsExtraNode $conec6] } {
      lappend ExtraNodes [list $conec6 $conec2 $conec3]
    }
    if { ![ExistsExtraNode $conec8] } {
      lappend ExtraNodes [list $conec8 $conec1 $conec4]
    }
  } elseif { $value == 30 || $value == 56 } {
    if { ![ExistsExtraNode $conec2] } {
      lappend ExtraNodes [list $conec2 $conec5 $conec6]
    }
    if { ![ExistsExtraNode $conec4] } {
      lappend ExtraNodes [list $conec4 $conec7 $conec8]
    }
  } else {
    if { ![ExistsExtraNode $conec1] } {
      lappend ExtraNodes [list $conec1 $conec5 $conec8]
    }
    if { ![ExistsExtraNode $conec3] } {
      lappend ExtraNodes [list $conec3 $conec6 $conec7]
    }
  }
  
  return 0
}

proc ExistsExtraNode { node } {

  global ExtraNodes

  for {set i 0} {$i < [llength $ExtraNodes]} {incr i} {
     if { [lindex [lindex $ExtraNodes $i] 0] == $node } {
        return true
     }   
  }  
  return false
}

# proc WriteExtraNodes {} {

#   global PROBLEMTYPEPATH ExtraNodes
#   set result [open "$PROBLEMTYPEPATH\\result_new.dat" "w"]
#   puts $result "WWWWWWWWWWWWWWW"
#   puts $result $ExtraNodes
#   close $result
#   return 0

# }
