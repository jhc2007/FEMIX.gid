# PointStringsFromFemixFile.tcl

proc AssignPointSprings { femixData } {

	global numDimension

	set pointSpringsData [GetBlockData $femixData "POINT_SPRINGS"]

	if { [llength $pointSpringsData] > 0 } {

		set index 5

		while { $index < [llength $pointSpringsData] } {

			set groupName [lindex $pointSpringsData $index]

			if { ![ExistsListElement [GiD_Info layers] $groupName] && ![ExistsListElement [GiD_Groups list] $groupName] } {
				GiD_Process 'Groups Create $groupName Mescape
			}

			incr index 2
			set rangeList [GetFirstLastValuesFromRange [lindex $pointSpringsData $index]]
			set firstPoint [lindex $rangeList 0]
	        set lastPoint [lindex $rangeList 1]

	        incr index
	        set vectorType "Global"
	        set direction [lindex $pointSpringsData $index]
	        set xComp 0
	        set yComp 0
	        set zComp 0
	        
	        switch $direction {
	        	_XG1 {
	        		if { $numDimension == 3 } {
	        			set direction "XG1"
	        		} else {
						set direction "XG3"
	        		}
	        	}
	        	_XG2 {
	        		if { $numDimension == 3 } {
	        			set direction "XG2"
	        		} else {
	        			set direction "XG1"
	        		}
	        	}
	        	_XG3 {
	        		if { $numDimension == 3 } {
	        			set direction "XG3" 
	        		} else {
	        			set direction "XG2"
	        		}
	        	}
	        	default {
	        		set vectorType "Specified"
	        		set direction "XG1"
	        		set vector [GetAuxiliaryVectorFromFemixFile $direction]
	        		# set direction "Specified"
        			# if { $numDimension == 3 } {
        			set xComp [lindex $vector 0]
    				set yComp [lindex $vector 1]
    				set zComp [lindex $vector 2]
        			# } else {
        			# 	set xComp [lindex $vector 1]
        			# 	set yComp [lindex $vector 2]
        			# 	set zComp [lindex $vector 0]
        			# }
        			# AddAuxiliaryVector $xComp $yComp $zComp
	        	}
	        }

	        incr index
	        set location [lindex $pointSpringsData $index]
	        if { $location == "_SG" } {
	        	set location "Structure_to_Ground"
	        } else {
	        	set location "Ground_to_Structure"
	        }

	       	incr index
	        set dof [lindex $pointSpringsData $index]
	        if { $dof == "_TRANS" } {
	        	set dof "Translational"
	        } else {
	        	set dof "Rotational"
	        }

	       	incr index
	        set matType [lindex $pointSpringsData $index]
	        set matType [string range $matType 1 [string length $matType]]

	       	incr index
	        set matName [lindex $pointSpringsData $index]

    		GiD_Process Mescape Data \
		        Conditions AssignCond _Point_Springs Change $groupName $vectorType $direction $xComp $yComp $zComp \
		        $location $dof $matType $matName "location" \
		        $firstPoint:$lastPoint Mescape 

	        set index [expr $index + 3]
	    }
	}
}