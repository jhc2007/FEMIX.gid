# package require Thread

# set x 1

##########################################################

# set NLMM401Name ""

set doubleFormatCoordinates "%18.8e"
set doubleFormatMatParameters "%14.5e"

set lengthUnit "mm"
set strengthUnit "N"
set stressUnit "MPa"
# set accelerationUnit "m*s^-2"
# set momentUnit "N*mm"
# set angleUnit "degree"

set PROBLEMTYPEPATH ""
set PROJECTPATH ""

set RUNFEMIX yes
set femixModelEditable yes
#set useFemixFile no
#set originalFemixFile ""

set NUMERICALINTEGRATIONLIST {}
# set EDGELOADSVECTORLIST {}
# set FACELOADSVECTORLIST {}

# set AuxiliaryVectorsFromFemixFile {}
# set CoordinateSystemsFromFemixFile {}
    
# # # set loadCaseNumber 1
set EdgeLoadsFromFemixFile {}
set FaceLoadsFromFemixFile {}

# set numDimension 0
# set numCombinations 0
# set combinations ""

set layerPropertiesList {}
set layerPatternsList {}

# # set GroupNames {}
# set ExtraNodes {}

set LineSpringsFromFemixFile {}
set SurfaceSpringsFromFemixFile {}
set SpringsNumericalIntegrationsFromFemixFile {}
set SpringMaterialsList {}

set SurfaceHeatFluxFromFemixFile {}

set waitWindowTitle "Processing Data"
set waitWindowMessage "GiD-FEMIX is working, please wait..."


# {{extra_node top_node bottom_node}}

# set InterfaceElementsList {}

#proc SelectGIDBatFile { dir basename } { 
#
#    global useFemixFile
#    if { $useFemixFile } {
#        return "$dir\\FEMIX_UseFemixFile.bat"
#    }
#    return "$dir\\FEMIX.bat"
#} 

# proc AfterCreateMaterial { name } { 
#     global useFemixFile
#     set useFemixFile no
# }

# proc AfterRenameMaterial { oldname newname } { 
#     global useFemixFile
#     set useFemixFile no
# }

# proc AfterAssignMaterial { name leveltype } { 
#     global useFemixFile
#     set useFemixFile no
# }

# proc AfterCreatePoint { num } { 
#     global useFemixFile
#     set useFemixFile no
# } 

# proc BeforeDeletePoint { num } { 
#     global useFemixFile
#     set useFemixFile no
# }

# proc AfterCreateLine { num } { 
#     global useFemixFile
#     set useFemixFile no
# } 

# proc BeforeDeleteLine { num } { 
#     global useFemixFile
#     set useFemixFile no
# }

# proc AfterCreateSurface { num } { 
#     global useFemixFile
#     set useFemixFile no
# } 

# proc BeforeDeleteSurface { num } { 
#     global useFemixFile
#     set useFemixFile no
# }

# proc AfterCreateVolume { num } { 
#     global useFemixFile
#     set useFemixFile no
# }

# proc BeforeDeleteVolume { num } { 
#     global useFemixFile
#     set useFemixFile no
# }

# proc AfterCreateLayer { name } { 
#     global useFemixFile
#     set useFemixFile no
# } 

# proc AfterRenameLayer { oldname newname } { 
#     global useFemixFile
#     set useFemixFile no
# } 

# proc AfterChangeLayer { name property } { 
#     global useFemixFile
#     set useFemixFile no
# }

# proc BeforeDeleteLayer { name } {
#     global useFemixFile
#     set useFemixFile no
#     return 0 
# } 

#proc LoadResultsGIDPostProcess { file } {
#
#    global PROBLEMTYPEPATH
#    set result [open "$PROBLEMTYPEPATH\\result_new.dat" "w"]
#    puts $result "AQUIlllll"
#    puts $result $file
#    #puts $result $format
#    #puts $result $error
#    close $result
#
#}

#proc AfterOpenFile { filename format error } {
#
#
#}

# proc AfterCreateMaterial { name } { 
#    global PROBLEMTYPEPATH
#    set result [open "$PROBLEMTYPEPATH\\result_new_create.dat" "w"]
#    puts $result "After Create Material:"
#    puts $result $name
#    close $result
# }

# changedfields
proc AfterChangeMaterial { name args } {
    
    global PROBLEMTYPEPATH \
           PROJECTPATH \
           PARENT

    set name [ReplaceSpaceWithUnderscore $name]
    
    # set result [open "$PROBLEMTYPEPATH\\result.dat" "w"]
    # puts $result $PROJECTPATH
    # puts $result $PROBLEMTYPEPATH
    # puts $result "Material Name: $name"
    # for {set i 0} {$i < [llength [GiD_Info Materials $name]]} {incr i} {
    #     puts $result "$i -> [lindex [GiD_Info Materials $name] $i]"
    # }
    # close $result

    if { [lindex [GiD_Info Materials $name] 2] == "NLMM401" && [lindex [GiD_Info Materials $name] 22] == "User_Specified" } {
        set numSets [lindex [GiD_Info Materials $name] 26]
        if { $PROBLEMTYPEPATH == $PROJECTPATH } {
            set dbFile [open "$PROBLEMTYPEPATH\\TmpFiles\\$name.dat" "w"]
        } else {
            set dbFile [open "$PROJECTPATH\\$name.dat" "w"]
        }
        for {set i 0} {$i < $numSets} {incr i} {
            puts $dbFile "[$PARENT.x$i get]\t[$PARENT.y$i get]"
        }
        close $dbFile
    }
}

proc BeforeInitGIDPostProcess {} {

    global numCombinations numDimension

    if { [lindex [GiD_Info gendata] 4] == 0 } {
        set numDimension [GetNumDimension [GiD_Info conditions Element_Properties mesh] 4]
    } else {
        set numDimension [GetNumDimension [GiD_Info conditions Element_Properties mesh] 5]
    }

    set numCombinations [NumOfCombinations]

}

proc EndGIDProject {} {
    global ProblemTypePriv
    RemoveFemixPreToolBar
}

proc LoadGIDProject { filespd } {

    global PROBLEMTYPEPATH \
           PROJECTPATH \
           RUNFEMIX \
           femixModelEditable \
           combinations \
           projectName

    set femixModelEditable yes
    set combinations ""
    set RUNFEMIX yes

    set projectName [string range $filespd [expr [string last "\\" $filespd] + 1] [expr [string length $filespd] - 5]]
    set PROJECTPATH [string range $filespd 0 [expr [string last "\\" $filespd] - 1]]

    if { $PROJECTPATH != $PROBLEMTYPEPATH } {
        CreateCombinations
        # CreateFromFileAuxiliaryVectorsFromFemixFile
        CreateFromFileEdgeLoadsFromFemixFile
        CreateFromFileFaceLoadsFromFemixFile
        CreateFromFileLineSpringsFromFemixFile
        CreateFromFileSurfaceSpringsFromFemixFile
        CreateFromFileSpringsNumericalIntegrationsFromFemixFile
        CreateFromFileSurfaceHeatFluxFromFemixFile
    }

    DeleteTmpFiles

}

proc SaveGIDProject { filespd } {

    global RUNFEMIX \
           PROJECTPATH \
           PROBLEMTYPEPATH \
           projectName 

    # global useFemixFile
    #global PROJECTLOADORSAVE
    #set PROJECTLOADORSAVE yes
    set RUNFEMIX yes
    set OLDPROJECTPATH $PROJECTPATH
    #set PROJECTPATH [file dirname $filespd]
    set PROJECTPATH [string range $filespd 0 [expr [string last "\\" $filespd] - 1]]
    set projectName [string range $filespd [expr [string last "\\" $filespd] + 1] [expr [string length $filespd] - 5]]

    # CreateFileAuxiliaryVectorsFromFemixFile
    CreateFileEdgeLoadsFromFemixFile
    CreateFileFaceLoadsFromFemixFile
    CreateFileLineSpringsFromFemixFile
    CreateFileSurfaceSpringsFromFemixFile
    CreateFileSpringsNumericalIntegrationsFromFemixFile
    CreateFileSurfaceHeatFluxFromFemixFile
    
    set tmpFiles [glob -nocomplain -directory $PROBLEMTYPEPATH\\TmpFiles *]

    foreach file $tmpFiles {
        file copy $file $PROJECTPATH
    }

    DeleteTmpFiles

    if { $OLDPROJECTPATH != $PROBLEMTYPEPATH && $PROJECTPATH != $OLDPROJECTPATH } {
    	if { [file exists "$OLDPROJECTPATH\\femix_layer_properties.dat"] } {
    	    exec cmd /c copy "$OLDPROJECTPATH\\femix_layer_properties.dat" "$PROJECTPATH\\femix_layer_properties.dat"
    	}
        if { [file exists "$OLDPROJECTPATH\\femix_layer_patterns.dat"] } {
            exec cmd /c copy "$OLDPROJECTPATH\\femix_layer_patterns.dat" "$PROJECTPATH\\femix_layer_patterns.dat"
        }
    	if { [file exists "$OLDPROJECTPATH\\femix_combinations.dat"] } {
    	    exec cmd /c copy "$OLDPROJECTPATH\\femix_combinations.dat" "$PROJECTPATH\\femix_combinations.dat"
    	}
    }

    AddNumericalIntegrationFromElementProperties
}

proc AddNumericalIntegrationFromElementProperties {} {

    global NUMERICALINTEGRATIONLIST

    set elemPropConditions [GiD_Info Conditions Element_Properties mesh]

    for {set i 0} {$i < [llength $elemPropConditions]} {incr i} {
        set integrationName [ReplaceSpaceWithUnderscore [lindex [lindex $elemPropConditions $i] 62]]
        if { ![ExistsListElement $NUMERICALINTEGRATIONLIST $integrationName] } {
            lappend NUMERICALINTEGRATIONLIST [expr $i + 1]
            lappend NUMERICALINTEGRATIONLIST $integrationName
        }
    }

}

proc InitGIDProject { dir } {
    
    global PROBLEMTYPEPATH \
           ProgramName \
           GidPriv \
           combinations \
           femixModelEditable

    set femixModelEditable yes
    set ProgramName [_ "GiD+FEMIX"]
    set GidPriv(ProgName) $ProgramName

    set PROBLEMTYPEPATH $dir
    set combinations ""
   
    set tclFiles [glob -nocomplain -directory $PROBLEMTYPEPATH\\TclFiles *]

    foreach file $tclFiles {
        source $file
    }

    UpdateFemixMenus
    CreateFemixPreToolBar $PROBLEMTYPEPATH
    # ViewFemixLogo $dir 1500
}

proc ViewFemixLogo { dir { TimeOut 0 } } {
    
    #set TimeOut 10
    # global GIDDEFAULT
    if { [.gid.central.s disable windows] } { return }
    if { [ winfo exist .splash] } {
        destroy .splash
        update
    }
    toplevel .splash
    set im [image create photo -file [ file join $dir Images FEMIX_logo_old.png]]
    set x [expr [winfo screenwidth .splash]/2-[image width $im]/2]
    set y [expr [winfo screenheight .splash]/2-[image height $im]/2]
    wm geom .splash +$x+$y
    wm transient .splash .gid
    wm overrideredirect .splash 1
    pack [label .splash.l -image $im -relief ridge -bd 2]
    #bind .splash <1> "destroy .splash"
    raise .splash .gid
    grab .splash
    focus .splash
    update
    #update

    if { $TimeOut > 0 } {
        after $TimeOut "destroy .splash"
        focus .gid
        update
    }
    #after 1 "destroy .splash"
}

proc UpdateFemixMenus {} {

    GidChangeDataLabel "Interval data" "Load Cases"
    # GidChangeDataLabel "Interval" ""
    GidChangeDataLabel  "Problem data" ""
    GidAddUserDataOptions [= "Main Parameters"] "GidOpenProblemData Main_Parameters" 2
    
    # GidAddUserDataOptions [= "Current Load Case"] { -np- GidOpenInterval current} 3
    GidAddUserDataOptions [= "Combinations"] "OpenCombinationsWindow" 6
    GidAddUserDataOptions "---" "" 7
    GidAddUserDataOptions [= "Layer Properties"] "OpenLayerPropertiesWindow" 8
    GidAddUserDataOptions [= "Layer Patterns"] "OpenLayerPatternsWindow" 9
    GidAddUserDataOptions "---" "" 10

    # TO DELETE
    # GidAddUserDataOptions [= "CreateStressPostMshFile"] "ConvertFemixPostMeshToGidMesh" 7
    # TO DELETE
    #GidAddUserDataOptions [= "CreateStressPostResFile"] "CreateStressPostResFile" 8
    #GiDMenu::Create "FEMIX" PRE -1 =
    #GiDMenu::InsertOption "FEMIX" [list "pos FEMIX"] 0 PRE "Command_1" "" "" insert =
    #GiDMenu::UpdateMenus
    CreateMenu "FEMIX" POST
    #InsertMenuOption "FEMIX" "Open FEMIX File" 0 OpenFemixFileSelectionWindow POST
    InsertMenuOption "FEMIX" "PosFemix" 0 OpenPosFemix POST
    InsertMenuOption "FEMIX" "DrawMesh" 1 OpenDrawMesh POST 
    #InsertMenuOption "FEMIX" "View Stress/Strain" 2 [list OpenWarningWindow "Stress/Strain" "How to view stresses and strains" 0] POST

    #CreateMenu "HELP" "FEMIX" PRE 
    #InsertMenuOption "View" "Element Properties" 0 OpenElementPropertiesWindow PRE
    #InsertMenuOption "View" "---" 1 "" PRE

    GiDMenu::InsertOption "Files" [list "Import" "FEMIX model (editable)"] 10 PRE [list OpenFemixFileSelectionWindow 1] "" "" insert _
    GiDMenu::InsertOption "Files" [list "Import" "FEMIX model (non-editable)"] 11 PRE [list OpenFemixFileSelectionWindow 0] "" "" insert _

    GiDMenu::InsertOption "View" [list "Element Properties"] 0 PRE [list OpenElementPropertiesWindow] "" "" insert _
    GiDMenu::InsertOption "View" [list "---"] 1 PRE "" "" "" insert _

    GiDMenu::InsertOption "Help" [list "FEMIX"] 0 PRE "" "" "" insert _
    GiDMenu::InsertOption "Help" [list "---"]   1 PRE "" "" "" insert _

    GiDMenu::InsertOption "Help" [list "FEMIX" "Units"] 0 PRE [list OpenUnitsTableWindow] "" "" replace _

    GiDMenu::InsertOption "Help" [list "FEMIX"] 0 POST "" "" "" insert _ 
    GiDMenu::InsertOption "Help" [list "---"]   1 POST "" "" "" insert _
    
    GiDMenu::InsertOption "Help" [list "FEMIX" "Stress/Strain"] 0 POST [list OpenWarningWindow "Stress/Strain" "How to view stresses and strains" 0] "" "" replace _
    GiDMenu::InsertOption "Help" [list "FEMIX" "Cracking"] 1 POST [list OpenWarningWindow "Cracking" "How to view cracking" 0] "" "" replace _
    #InsertMenuOption "Help" "FEMIX" 0 "" PRE
    #InsertMenuOption "Help" [list "FEMIX" [list "VIEW"]] 0 "" PRE
    #InsertMenuOption [list "Help" "FEMIX"] "View Stress/Strain" 0 [list OpenWarningWindow "Stress/Strain" "How to view stresses and strains" 0] POST
    UpdateMenus
}

proc OpenDrawMesh {} {

    global PROBLEMTYPEPATH 
    # PROJECTPATH
    # global projectName

    # set mes3dFile $projectName
    #set meb3dFile $projectName

    # append mes3dFile "_me.s3d"
    #append meb3dFile "_me.b3d"

    # if { [file exists "$PROJECTPATH\\$mes3dFile"] } {
        set drawmesh "$PROBLEMTYPEPATH\\ExeFiles\\drawmesh.exe"
        #cd $PROJECTPATH
        #exec cmd /c start cmd /k $drawmesh $projectName
        #exec $drawmesh $projectName
        exec $drawmesh
    # } else {
        # OpenWarningWindow "Warning" "File not found: $mes3dFile\n\nExecute the command \"mes3d\" at the FEMIX Post Processor to create it" 0
    # }    
}

proc OpenPosFemix {} {

    global PROBLEMTYPEPATH PROJECTPATH
    global projectName numDimension

    set ctrldatFile $projectName
    set glbinFile $projectName

    append ctrldatFile "_ctrl.dat"
    append glbinFile "_gl.bin"

    if { [file exists "$PROJECTPATH\\$ctrldatFile"] && [file exists "$PROJECTPATH\\$glbinFile"] } {
        set posfemix "$PROBLEMTYPEPATH\\ExeFiles\\Posfemix.exe"
        cd $PROJECTPATH

        exec cmd /c start cmd /k $posfemix $projectName


            # # thread::create {

        # global waitWindowTitle waitWindowMessage

        # # set waitWindowTitle "SSS"
        # # set waitWindowMessage "AAA"

        # set waitWindow .gid.waitwindow
        # InitWindow $waitWindow $waitWindowTitle "" "" "" 1
        # label $waitWindow.labelMessage -text $waitWindowMessage
        # button $waitWindow.buttonOk -text "OK" -command "destroy $waitWindow" -width 14
        # grid $waitWindow.labelMessage -row 0 -column 0 -padx 20 -pady 30 -sticky n
        # # grid $waitWindow.buttonOk -row 1 -column 0 
        # grid rowconfigure $waitWindow 1 -weight 1


            # # }
        

        # OpenWaitWindow

        # global GIDDEFAULT
        # if { [ winfo exist .splash] } {
        #     destroy .splash
        #     update
        # }
        # toplevel .splash
        # set im [image create photo -file [file join $PROBLEMTYPEPATH Images FEMIX_logo_old.png]]
        # set x [expr [winfo screenwidth .splash]/2-[image width $im]/2]
        # set y [expr [winfo screenheight .splash]/2-[image height $im]/2]
        # wm geom .splash +$x+$y
        # wm transient .splash .gid
        # wm overrideredirect .splash 1
        # pack [label .splash.l -image $im -relief ridge -bd 2]
        # #bind .splash <1> "destroy .splash"
        # raise .splash .gid
        # grab .splash
        # focus .splash
        # update
        # if {0 > 2} {
        OpenWaitWindow

        CreateStressStrainPostMshFile $projectName
        if { $numDimension == 2 } {
            Create2DPostResFile $projectName
            Create2DStressStrainPostResFile $projectName
        } else {
            Create3DPostResFile $projectName
            Create3DStressStrainPostResFile $projectName
        }
        CreateCrackingPostMshFile $projectName
        # }

        DestroyWaitWindow
        # DestroyWaitWindow
        # if { [ winfo exist .splash] } {
        #     after 10000 "destroy .splash"
        #     focus .gid
        #     update
        # }


    } else {
        OpenWarningWindow "Warning" "Files not found: $ctrldatFile & $glbinFile" 0
    }
}

proc CreateFemixPreToolBar { dir { type "DEFAULT INSIDELEFT"} } {

    global MyPreBitmapsNames MyPreBitmapsCommands MyPreBitmapsHelp ProgramName ProblemTypePriv

    set MyPreBitmapsNames(0) { "ToolBarImages/import.png" \
                               --- \
                               "ToolBarImages/main_parameters.png" \
                               --- \
                               "ToolBarImages/liniso.png" \
                               "ToolBarImages/concrete.png" \
                               "ToolBarImages/steel.png" \
                               "ToolBarImages/interface.png" \
                               --- \
                               "ToolBarImages/supports.png" \
                               "ToolBarImages/springs.png" \
                               "ToolBarImages/loads.png" \
                               --- \
                               "ToolBarImages/combinations.png" \
                               --- \
                               "ToolBarImages/calculator.png" \
                               --- \
                               "ToolBarImages/femix_logo.png" }

    set MyPreBitmapsCommands(0) { {-np- OpenFemixFileSelectionWindow 1} \
                                  "" \
                                  {-np- GidOpenProblemData Main_Parameters} \
                                  "" \
                                  {-np- GidOpenMaterials Continuous_Materials LIN_ISO_Concrete} \
                                  "" \
                                  "" \
                                  "" \
                                  "" \
                                  {-np- GidOpenConditions Supports} \
                                  {-np- GidOpenConditions Springs} \
                                  {-np- GidOpenConditions Loads} \
                                  "" \
                                  {-np- OpenCombinationsWindow} \
                                  "" \
                                  {Utilities Calculate} \
                                  "" \
                                  {-np- OpenWarningWindow "GiD-FEMIX" \
                                                          "          FEMIX Problem Type          \n\n\
                                                          Version 2.0.8d\
                                                          \n\nby SC-ISISE" 0} }

    set MyPreBitmapsHelp(0) { "Import FEMIX model (editable)" \
                              "" \
                              "Main Parameters" \
                              "" \
                              "Linear Isotropic" \
                              "Concrete" \
                              "Steel Reinforcement" \
                              "Interface" \
                              "" \
                              "Supports" \
                              "Springs"
                              "Loads" \
                              "" \
                              "Combinations" \
                              "" \
                              "Calculate (FEMIX)" \
                              "" \
                              "" }

    set MyPreBitmapsNames(0,5) { "ToolBarImages/nlmm101.png" \
                                 "ToolBarImages/nlmm104.png" \
                                 "ToolBarImages/nlmm111.png" \
                                 "ToolBarImages/nlmm141.png" \
                                 "ToolBarImages/nlmm151.png" }

    set MyPreBitmapsCommands(0,5) { { -np- GidOpenMaterials Continuous_Materials NLMM101_Concrete } \
                                    { -np- GidOpenMaterials Continuous_Materials NLMM104_Concrete } \
                                    { -np- GidOpenMaterials Continuous_Materials NLMM111_Concrete } \
                                    { -np- GidOpenMaterials Continuous_Materials NLMM141_Concrete } \
                                    { -np- GidOpenMaterials Continuous_Materials NLMM151_Concrete } }

    set MyPreBitmapsNames(0,6) { "ToolBarImages/nlmm201.png" }

    set MyPreBitmapsCommands(0,6) { { -np- GidOpenMaterials Continuous_Materials NLMM201_Steel } }

    set MyPreBitmapsNames(0,7) { "ToolBarImages/int_line_2d.png" \
                                 "ToolBarImages/int_line_3d.png" \
                                 "ToolBarImages/int_surface.png" \
                                 "ToolBarImages/nlmm301.png" \
                                 "ToolBarImages/nlmm305.png" }

    set MyPreBitmapsCommands(0,7) { { -np- GidOpenMaterials Interface_Materials Linear_Interface_Line_2D } \
                                    { -np- GidOpenMaterials Interface_Materials Linear_Interface_Line_3D } \
                                    { -np- GidOpenMaterials Interface_Materials Linear_Interface_Surface } \
                                    { -np- GidOpenMaterials Interface_Materials NLMM301_Interface } \
                                    { -np- GidOpenMaterials Interface_Materials NLMM305_Interface } }


    # set MyPreBitmapsNames(0,8) { "ToolBarImages/springs.png" }

    # set MyPreBitmapsCommands(0,8) { { -np- GidOpenMaterials Spring_Materials Linear_Spring } }

    set prefix Pre
    set ProblemTypePriv(pretoolbarwin) [CreateOtherBitmaps FemixPreToolBar "Femix Pre Tool Bar" \
            MyPreBitmapsNames MyPreBitmapsCommands \
            MyPreBitmapsHelp $dir "CreateFemixPreToolBar [list $dir]" $type $prefix]
    AddNewToolbar "$ProgramName femixprebar" ${prefix}FemixPreToolBarWindowGeom "CreateFemixPreToolBar [list $dir]"
    #AddNewToolbar "$ProgramName prebar" ${prefix}MyBarWindowGeom "MyPreBitmaps [list $dir]"
}

proc RemoveFemixPreToolBar {} {
    
    global ProblemTypePriv ProgramName

    
    ReleaseToolbar "$ProgramName femixprebar"
    rename CreateFemixPreToolBar ""
    
    destroy $ProblemTypePriv(pretoolbarwin)
    #catch { destroy $ProblemTypePriv(pretoolbarwin) }
    #catch { destroy $ProblemTypePriv(loadcasewin) }
}

proc ReplaceSpaceWithUnderscore { str } {

    set newstring ""
    for {set i 0} {$i < [string length $str]} {incr i} {
        if {[string compare [string index $str $i] " "] == 0} {
            append newstring "_"
        } else {
            append newstring [string index $str $i]
        }
    }
    return $newstring
}

proc ListReplaceSpaceWithUnderscore { lst } {

    for {set i 0} {$i < [llength $lst]} {incr i} {
       set lst [lreplace $lst $i $i [ReplaceSpaceWithUnderscore [lindex $lst $i]]]
    }
    return $lst
}

# proc SetUseFemixFile {} {
    
#     global useFemixFile

#     foreach var { ovpnt ovline ovsurf ovvol ovlayer } {
#         for {set i 0} {$i < [llength [GiD_Info conditions $var]]} {incr i} {
#             if { [llength [GiD_Info conditions [lindex [GiD_Info conditions $var] $i] mesh]] > 0 } {
#                 return no
#             }
#         } 
#     }

#     return $useFemixFile
# }

proc IsFemixModelEditable {} {

    global femixModelEditable

    if { $femixModelEditable } {
        return 1
    }
    return 0
}

proc SetLengthUnit { unit } {
    
    global lengthUnit
    set lengthUnit $unit
    return 0
}

proc SetStrengthUnit { unit } {
    
    global strengthUnit
    set strengthUnit $unit
    return 0
}

proc SetStressUnit { unit } {
    
    global stressUnit
    set stressUnit $unit
    return 0
}

proc SetNumDimension { dimension } {

    global numDimension
    set numDimension $dimension
    return 0
}

# proc RemoveUnitsFromParameterValue { value } {

#     if { [llength $value] == 2 } {
#         set value [lindex $value 0]  
#     } else {
#         set index [string length $value]
#         while { ![string is double [string range $value 0 $index]] } {
#             set index [expr $index - 1]
#         }
#         set value [string range $value 0 $index]
#     }
#     return $value
# }

proc SetAllGlobalVariables {} {

    global numDimension \
           numCombinations \
           combinations \
           PointSpringsList \
           LineSpringsList \
           SurfaceSpringsList \
           SpringNumericalIntegrationList \
           NUMERICALINTEGRATIONLIST \
           layerPropertiesList \
           layerPatternsList \
           ExtraNodes \
           EdgeLoadsList \
           FaceLoadsList \
           AuxiliaryVectorsList \
           AuxiliaryVectorsFromFemixFile \
           BoundaryPointsWithFixedTemperaturesList \
           SurfaceHeatFluxList \
           PointLoadsList
           # EDGELOADSVECTORLIST \
           # FACELOADSVECTORLIST \

    set numDimension 0
    set numCombinations 0
    set combinations ""

    set PointSpringsList {}
    set LineSpringsList {}
    set SurfaceSpringsList {}

    set AuxiliaryVectorsList {}
    set SpringNumericalIntegrationList {}

    set NUMERICALINTEGRATIONLIST {}
    # set EDGELOADSVECTORLIST {}
    # set FACELOADSVECTORLIST {}
    
    set AuxiliaryVectorsFromFemixFile {}
    # set CoordinateSystemsFromFemixFile {}
        
    # set EdgeLoadsFromFemixFile {}
    # set FaceLoadsFromFemixFile {}
    
    set layerPropertiesList {}
    set layerPatternsList {}
    
    set ExtraNodes {}

    set EdgeLoadsList {}
    set FaceLoadsList {}
    # set SurfaceSpringsFromFemixFile {}

    set BoundaryPointsWithFixedTemperaturesList {}
    set SurfaceHeatFluxList {}

    set PointLoadsList {}
}

proc DestroyAllGlobalVariables {} {

    global SpringMaterialsList \
           SpringNumericalIntegrationList \
           FaceLoadsList \
           PointLoadsList
           # AuxiliaryVectorsList \
           # NUMERICALINTEGRATIONLIST

    # set NUMERICALINTEGRATIONLIST {}

    destroy $SpringMaterialsList \
            $SpringNumericalIntegrationList \
            $FaceLoadsList \
            $PointLoadsList
            # $AuxiliaryVectorsList \
}

proc BeforeWriteCalcFileGIDProject { projectpath } {  

    # global LineSpringsConditions

    SetAllGlobalVariables

    CreateLineSpringsList
    CreateSurfaceSpringsList

    AddAuxiliaryVectorFromSurfaceSpringsFromFemixFile
    AddAuxiliaryVectorFromLineSpringsFromFemixFile

  #     global SpringMaterialsList
  # global PROBLEMTYPEPATH
  # set result [open "$PROBLEMTYPEPATH\\result_new.dat" "w"]
  # puts $result "AQUI::::::::"
  # puts $result $SpringMaterialsList
  # close $result

    # set LineSpringsConditions [lsort -integer -index 0 [GiD_Info Conditions _Line_Springs Mesh]]
}

proc BeforeRunCalculation { batfilename basename dir problemtypedir gidexe args } {

    global PROJECTPATH PROBLEMTYPEPATH 
    # InterfaceElementsList
    # useFemixFile

    GiD_Process Mescape Files Save 
    
    if {$PROJECTPATH == "" || $PROBLEMTYPEPATH == $PROJECTPATH} {
        OpenWarningWindow "Warning" "Save the project before calculate" 1
        return -cancel-
    }

    DestroyAllGlobalVariables

    # set InterfaceElementsList {}

    #set filesToDelete { }
    set filesToDelete { "_rs.lpt" "_um.s3d" "_cp.s3d" "_me.s3d" ".vv" ".post.vv" }
    #set filesToDelete { "_um.s3d" "_cp.s3d" "_me.s3d" ".vv" ".post.vv" }

    for { set i 0 } { $i < [llength $filesToDelete] } { incr i } {
        set originalBasename $basename
        append originalBasename [lindex $filesToDelete $i]
        file delete $originalBasename    
    }

    set pvaFiles [lsort -dictionary [glob -nocomplain *.pva]]
    #set pvaFiles { }
    for {set i 0} {$i < [llength $pvaFiles]} {incr i} {
        file delete [lindex $pvaFiles $i]        
    }
}

proc GetMainTitle {} {

    global projectName
    return $projectName
    
}

proc AfterRunCalculation { basename dir problemtypedir where error errorfilename } {

    global PROBLEMTYPEPATH \
           PROJECTPATH \
           RUNFEMIX \
           femixModelEditable \
           numCombinations \
           projectName \
           numDimension \
           originalFemixFile
       
    set projectName $basename

    if { !$femixModelEditable } {
        file delete "$PROJECTPATH\\$basename\_gl.dat"
        file copy "$originalFemixFile" "$PROJECTPATH\\$basename\_gl.dat"
    }

    #CreateCrackingPostMshFile $basename
    #CreatePostResFile $basename
    #CreateStressStrainPostMshFile $CreateStressStrainPostResFile
    #basename $basename
        
        #if { $numDimension == 2 } {
            #Create2DPostResFile $basename
            #Create2DStressStrainPostResFile $basename
        #} else {
            #Create3DPostResFile $basename
            #Create3DStressStrainPostResFile $basename
        #}
        #Create3DStressStrainPostResFile $basename
        #Create3DPostResFile $basename
        #return 

    #set result [open "$PROBLEMTYPEPATH\\result.dat" "w"]
    #puts $result [GiD_Info list_entities Dimensions]
    #puts $result [lindex $dir 0]\\$basename
    #puts $result $problemtypedir
    #puts $result $where
    #puts $result $error
    #puts $result $errorfilename
    #puts $result "exec cmd /c start cmd /k [lindex $problemtypedir 0]\\ExeFiles\\Prefemix.exe [lindex $dir 0]\\$basename"
    #close $result
    #return
    #exec cmd /c start cmd /k "[lindex $problemtypedir 0]/ExeFiles/Prefemix.exe" "[lindex $dir 0]\\$basename"
    if { $RUNFEMIX } {
    	
    	set prefemix "$PROBLEMTYPEPATH\\ExeFiles\\Prefemix.exe"
        set femix "$PROBLEMTYPEPATH\\ExeFiles\\Femix.exe"
        set posfemix "$PROBLEMTYPEPATH\\ExeFiles\\Posfemix.exe"
    	#set glfile "[lindex $dir 0]\\$basename"
    	#append prefemix $PROBLEMTYPEPATH
    	#append prefemix "\\ExeFiles\\Prefemix.exe"


    	# set result [open "$PROBLEMTYPEPATH\\result.dat" "w"]
    	# puts $result [lindex [GiD_Info intvdata num] 1]
     #   	close $result


       	
    	#exec $prefemix $basename
    
        cd [lindex $dir 0]	

        if { ![file exists "$basename\_pos.dat"] } {
            file copy "$PROBLEMTYPEPATH\\DatFiles\\femix_pos.dat" "$basename\_pos.dat"
        }
        #exec cmd /c start cmd /k $prefemix $basename
        #exec cmd /c start cmd /k $femix $basename
        #exec cmd /c start cmd /k $posfemix $basename
        #return
        #set output [exec $prefemix $basename]
        if { [catch {exec cmd /c $prefemix $basename} errormessage] } {
            set output [exec $prefemix $basename]
        }
        #set output [exec $femix $basename]
        # global PROBLEMTYPEPATH
        # set result [open "$PROBLEMTYPEPATH\\result.dat" "w"]
        # puts $result "aqui"
        # close $result
        #set output [exec cmd c/ $femix $basename]

        #if { [catch { exec cmd /c $femix $basename } errormessage] } {}

        set clean [open "$PROJECTPATH\\clean.bat" "w"]
        puts $clean "\@echo off"
        puts $clean "set jname=$basename"
        set numLoadCases [lindex [GiD_Info intvdata num] 1]
        set gllpt "(echo gllpt & echo n & "
        for {set i 0} {$i < $numLoadCases} {incr i} {
            append gllpt "echo n & "
        }
        append gllpt "echo end) | \"$posfemix\" %jname% 1\> NUL"
        puts $clean $gllpt
        puts $clean "del \%jname\%_gl.dat"
        puts $clean "ren \%jname\%_gl.lpt \%jname\%_gl.dat"
        close $clean

        set clean "$PROJECTPATH\\clean.bat"
        exec $clean
        file delete $clean

        exec cmd /c start cmd /k $femix $basename
        #if { [catch { exec cmd /c $femix $basename } errormessage] } {

        #}        

        #set output [exec cmd /c start cmd /k $femix $basename]
        #puts $result $output
        #close $result
        exec cmd /c start cmd /k $posfemix $basename
         # /k "gllpt"
        
        OpenWaitWindow

        # DESCOMENTAR
        #CreatePostResFile $basename

        #set ums3dFileName $basename
        #append ums3dFileName "_um.s3d"
        #if { [file exists $ums3dFileName] } { 
        #CreateStressPostMshFile $basename
        #CreateStressPostResFile $basename

        #CreateStrainPostResFile $basename        

        # OpenWaitWindow

        # global GIDDEFAULT
        # if { [ winfo exist .splash] } {
        #     destroy .splash
        #     update
        # }
        # toplevel .splash
        # set im [image create photo -file [file join $PROBLEMTYPEPATH Images FEMIX_logo_old.png]]
        # set x [expr [winfo screenwidth .splash]/2-[image width $im]/2]
        # set y [expr [winfo screenheight .splash]/2-[image height $im]/2]
        # wm geom .splash +$x+$y
        # wm transient .splash .gid
        # wm overrideredirect .splash 1
        # pack [label .splash.l -image $im -relief ridge -bd 2]
        # #bind .splash <1> "destroy .splash"
        # raise .splash .gid
        # grab .splash
        # focus .splash
        # update

        CreateStressStrainPostMshFile $basename

        if { $numDimension == 2 } {
            Create2DPostResFile $basename
            Create2DStressStrainPostResFile $basename
        } else {
            Create3DPostResFile $basename
            Create3DStressStrainPostResFile $basename
        }
        CreateCrackingPostMshFile $basename

        DestroyWaitWindow

        # DestroyWaitWindow

        #}
        #cd [lindex $dir 0]
        #
        #set femixout [exec $prefemix $basename]
        #set femixout [split $femixout "\n"]

        #lappend prefemix ">"
        #lappend prefemix "tmp_femix_out.txt"
    	
        #cd C:\\Program\ Files
        #exec cmd /c start cmd /k dir
        
        # > tmp_femix_out.txt
        #exec cmd /c $prefemix $basename > tmp_femix_out.txt

        #set tmpFile [open "tmp_femix_out.txt" "r"]
        #set femixOutData [split [read $tmpFile] "\n"]
        #close $tmpFile 


    	#exec cmd /c start cmd /k "$PROBLEMTYPEPATH\\ExeFiles\\Femix.exe" $basename
        #exec cmd /c start cmd /k "$PROBLEMTYPEPATH\\ExeFiles\\Prefemix.exe" "[lindex $dir 0]\\$basename"
    }
}

proc ExistsListElement { lis ele } {

    for {set i 0} {$i < [llength $lis]} {incr i} {
        if { [lindex $lis $i] == $ele } {
            return true
        }
    }
    return false
}

proc RemoveFirstUnderScore { str } {

    return [string range $str 1 end]
}

proc OpenWarningWindow { title message runfemixflag } {

    if { $runfemixflag } {
        global RUNFEMIX
        set RUNFEMIX no
    }
    if { [GidUtils::AreWindowsDisabled] } {
       return
    }  
    set ww .gid.warningwindow
    InitWindow $ww $title "" "" "" 1
    # [= "Warning"]
    # WINCOMB "" "" 1
    if { ![winfo exists $ww] } {
       return
    }
        
    #ttk::frame $ww.information;# -relief ridge
    label $ww.labelMessage -text $message

    #ttk::frame $ww.bottom
    button $ww.buttonOk -text "OK" -command "destroy $ww" -width 14

    #grid $ww.top.title_text -sticky ew
    #grid $ww.top -sticky new
    #grid $w.information.path -sticky w -padx 6 -pady 6
    grid $ww.labelMessage -row 0 -column 0 -padx 20 -pady 30 -sticky n
    #grid $w.information.conditions -sticky w -padx 6 -pady 6
    #grid $ww.information -sticky n
    
    grid $ww.buttonOk -row 1 -column 0 
    #grid $ww.bottom -sticky s -padx 10 -pady 10
    #if { $::tcl_version >= 8.5 } { grid anchor $ww.bottom center }
    grid rowconfigure $ww 1 -weight 1
    #grid columnconfigure $ww 0 -weight 1    
    focus $ww
}

proc OpenUnitsTableWindow { } {

    global PROBLEMTYPEPATH

    if { [GidUtils::AreWindowsDisabled] } {
       return
    }  
    set unitsWindow .gid.unitswindow
    InitWindow $unitsWindow "FEMIX Units" "" "" "" 1
    
    if { ![winfo exists $unitsWindow] } {
       return
    }

    $unitsWindow configure -bg white

    # label $unitsWindow.title -text "Units usage: each column of the table represents a consistent set of units"

    image create photo table -file "$PROBLEMTYPEPATH\\Images\\units_table.png"
    # -width 400 -height 400
        
    label $unitsWindow.table

    $unitsWindow.table configure -image table

    button $unitsWindow.buttonOk -text "OK" -command "destroy $unitsWindow" -width 14

    grid $unitsWindow.table -row 0 -column 0
    grid $unitsWindow.buttonOk -row 1 -column 0
    # -sticky e -padx 20
    grid rowconfigure $unitsWindow 1 -weight 1

    focus $unitsWindow
}

proc DestroyWaitWindow {} {

    # global GIDDEFAULT

    if { [ winfo exist .waitWindow] } {
        destroy .waitWindow
        focus .gid
        update
    }

    # if { [GidUtils::AreWindowsDisabled] } {
    #     return
    # }

    # global waitWindow

    # destroy $waitWindow
    # focus .gid
}

proc OpenWaitWindow {} {

    # package require Thread

    global PROBLEMTYPEPATH

    destroy .waitWindow

    toplevel .waitWindow
    set im [image create photo -file [ file join $PROBLEMTYPEPATH Images wait_message.png]]
    set x [expr [winfo screenwidth .waitWindow]/2-[image width $im]/2]
    set y [expr [winfo screenheight .waitWindow]/2-[image height $im]/2]
    wm geom .waitWindow +$x+$y
    # wm transient .waitWindow .gid
    wm overrideredirect .waitWindow 1
    pack [label .waitWindow.l -image $im -relief ridge -bd 2]
    #bind .splash <1> "destroy .splash"
    # raise .waitWindow .gid
    grab .waitWindow
    focus .waitWindow
    # tkwait visibility .waitWindow
    update

    ######################################################################################

    # global waitWindow 
    # # PROBLEMTYPEPATH

    # set title "Processing Data"
    # set message "GiD-FEMIX is working, please wait..."

    # # set im [image create photo -file [file join $PROBLEMTYPEPATH Images wait.gif]]

    # set waitWindow .gid.waitwindow
    # # toplevel $waitWindow
    # InitWindow $waitWindow $title "" -nodestroy
    # # label $waitWindow.labelMessage -image $im
    # label $waitWindow.labelMessage -text $message
    # # button $waitWindow.buttonOk -text "OK" -command "destroy $waitWindow" -width 14
    # # grid $waitWindow.labelMessage -row 0 -column 0 -padx 20 -pady 30 -sticky n
    # grid $waitWindow.labelMessage -padx 20 -pady 30
    # # grid $waitWindow.buttonOk -row 1 -column 0 
    # # grid rowconfigure $waitWindow 1 -weight 1
    # focus $waitWindow
    # # tkwait visibility $waitWindow
    # # tkwait window $waitWindow

    ########################################################################################
    # global PROBLEMTYPEPATH 
    # GIDDEFAULT
    # tsv::set application PROBLEMTYPEPATH $PROBLEMTYPEPATH
    # tsv::set application GIDDEFAULT $GIDDEFAULT
    # tsv::set application SPLASH .splash
    # tsv::set application GID .gid

    # set threadID [thread::create {

        # set PROBLEMTYPEPATH [tsv::get application PROBLEMTYPEPATH]
        # # set GIDDEFAULT [tsv::get application GIDDEFAULT]
        # # set SPLASH [tsv::get application SPLASH]
        # # set GID [tsv::get application GID]



        # set result [open "$PROBLEMTYPEPATH\\result_new.dat" "w"]
        # puts $result "AQUIlllll"
        # #puts $result $format
        # #puts $result $error
        # close $result

        # global GIDDEFAULT PROBLEMTYPEPATH
        # if { [ winfo exist $SPLASH] } {
        #     destroy $SPLASH
        #     update
        # }
        # set PROBLEMTYPEPATH "C:\\Program Files\\GiD\\GiD 12.0\\problemtypes\\FEMIX.gid"
        # toplevel $SPLASH
        # set im [image create photo -file [file join $PROBLEMTYPEPATH Images wait_window.png]]
        # set x [expr [winfo screenwidth $SPLASH]/2-[image width $im]/2]
        # set y [expr [winfo screenheight .$SPLASH]/2-[image height $im]/2]
        # wm geom $SPLASH +$x+$y
        # wm transient $SPLASH $GID
        # wm overrideredirect $SPLASH 1
        # pack [label $SPLASH.l -image $im -relief ridge -bd 2]
        # #bind .splash <1> "destroy .splash"
        # raise $SPLASH $GID
        # # grab .splash
        # # focus .splash
        # tkwait visibility $SPLASH
        # update
    # }]

}

proc GetStringValue { str } {

    if { [string is double $str] } {
        return $str
    }
    return 0
}

# proc LineSpringsConditions { } {

#     set LineSpringsConditions [lsort -integer -index 0 [GiD_Info Conditions _Line_Springs Mesh]]
# }